﻿using DomainModel.Content;
using System;
using System.Collections.Generic;

namespace DomainModel.Account
{
    public class User : BaseEntity
    {
        public User()
        {
            Roles = new List<Role>();
        }

        public const int EmailLength = 256;
        public const int PasswordLength = 64;

        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Phone { get; set; }

        public string PasswordQuestion { get; set; }
        public string PasswordAnswer { get; set; }
        public string Comment { get; set; }

        public bool IsApproved { get; set; }
        public bool IsLockedOut { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime? LastActivityDate { get; set; }
        public DateTime? LastLoginDate { get; set; }

        public DateTime? LastPasswordChangedDate { get; set; }
        public DateTime? LastLockoutDate { get; set; }
        public DateTime? LastPasswordFailureDate { get; set; }

        public int PasswordFailuresSinceLastSuccess { get; set; }

        public Guid? PasswordRecoveryToken { get; set; }
        public DateTime? PasswordRecoveryTokenExpirationDate { get; set; }

        public IList<Role> Roles { get; set; }
        public bool IsNotify { get; set; }
        public List<Widget> Widgets { get; set; }
    }
}