﻿using System;
using System.Collections.Generic;
using DomainModel.Content;

namespace DomainModel.Navigation
{
    public class MenuItem : BaseEntity
    {
        public string Name { get; set; }

        public string CustomUrl { get; set; }

        public MenuItemType Type { get; set; }

        public bool IsHidden { get; set; }

        public int? PageId { get; set; }
        public Page Page { get; set; }

        public int? ParentId { get; set; }
        public MenuItem Parent { get; set; }

        public IList<MenuItem> Childs { get; set; }

        public int? MenuId { get; set; }
        public Menu Menu { get; set; }

        public DateTime Created { get; set; }
        public DateTime? Updated { get; set; }
    }
}