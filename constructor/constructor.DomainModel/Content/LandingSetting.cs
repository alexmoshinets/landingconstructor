﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainModel.Content
{
    public class LandingSetting: BaseEntity
    {        
        public string SettingsJson { get; set; }        
        public int LandingId { get; set; }
        public Landing Landing { get; set; }        
    }
}
