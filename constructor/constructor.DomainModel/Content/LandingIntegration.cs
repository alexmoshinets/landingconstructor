﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainModel.Content
{
    public class LandingIntegration: BaseEntity
    {
        public string FormId { get; set; }        
        public string SettingsJson { get; set; }
        public int TypeService { get; set; }

        public int LandingId { get; set; }
        public Landing Landing { get; set; }
        public bool IsRedirect { get; set; }
        public string RedirectUrl { get; set; }
        public string AlertMessage { get; set; }
    }
}
