﻿using System;
using System.Collections.Generic;

namespace DomainModel.Content
{
    public class Sections : BaseEntity
    {       
        public string StringKey { get; set; }
        public string Name { get; set; }
        public string SectionContent { get; set; }
        public string Styles { get; set; }
        public string Image { get; set; }
        public bool IsPublic { get; set; }
        public int CategoryId { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Updated { get; set; }
        //public virtual SectionsCategory Category { get; set; }
    }
}
