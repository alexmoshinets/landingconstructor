﻿using System;

namespace DomainModel.Content
{
    public class Photo : BaseEntity
    {
        public string MediumUrl { get; set; }
        public string OriginalUrl { get; set; }

        public DateTime Created { get; set; }

        public int AlbumId { get; set; }
        public PhotoAlbum Album { get; set; }
    }
}