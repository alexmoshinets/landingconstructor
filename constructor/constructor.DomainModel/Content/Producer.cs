﻿using System;

namespace DomainModel.Content
{
    public class Producer : BaseEntity
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string FullDescription { get; set; }
        public string Image { get; set; }
        public string Url { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Updated { get; set; }
    }
}