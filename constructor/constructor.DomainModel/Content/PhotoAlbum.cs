﻿using System;
using System.Collections.Generic;

namespace DomainModel.Content
{
    public class PhotoAlbum : BaseEntity
    {
        public PhotoAlbum()
        {
            Photos = new List<Photo>();
        }

        public bool IsHidden { get; set; }

        public string StringKey { get; set; }

        public DateTime Date { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public string MetaTitle { get; set; }
        public string MetaKeywords { get; set; }
        public string MetaDescription { get; set; }

        public IList<Photo> Photos { get; set; }

        public DateTime Created { get; set; }
        public DateTime? Updated { get; set; }
    }
}