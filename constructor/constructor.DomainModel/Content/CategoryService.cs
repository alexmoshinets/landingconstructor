﻿using System;
using System.Collections.Generic;

namespace DomainModel.Content
{
    public class CategoryService : BaseEntity
    {
        public CategoryService()
        {
            Services = new List<Servise>();
        }
        public List<Servise> Services { get; set; }
        public string StringKey { get; set; }
        public bool IsHidden { get; set; }
        public string Title { get; set; }
        public string MetaTitle { get; set; }
        public string Description { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeywords { get; set; }
        public string Image { get; set; }

    }
}