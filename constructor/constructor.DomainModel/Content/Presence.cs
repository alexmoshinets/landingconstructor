﻿using System;

namespace DomainModel.Content
{
    public class Presence : BaseEntity
    {
        public string Title { get; set; }
    }
}
