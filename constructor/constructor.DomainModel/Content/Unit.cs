﻿using System;

namespace DomainModel.Content
{
    public class Unit : BaseEntity
    {
        public string Title { get; set; }
    }
}
