﻿using DomainModel.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainModel.Content
{
    public class Widget
    {
        public int Id { get; set; }        
        public string Title { get; set; }
        public DateTime CreateDate { get; set; }       
        public string Script { get; set; }
        public string Description { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
    }
}
