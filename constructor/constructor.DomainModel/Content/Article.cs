﻿using System;

namespace DomainModel.Content
{
    public class Article : BaseEntity
    {
        public string StringKey { get; set; }

        public DateTime Date { get; set; }

        public string Title { get; set; }

        public string Annonse { get; set; }
        public string Description { get; set; }

        public string MetaTitle { get; set; }
        public string MetaKeywords { get; set; }
        public string MetaDescription { get; set; }

        public bool IsHidden { get; set; }
        public string Image { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Updated { get; set; }

        public int CategoryId { get; set; }
        public ArticleCategory Category { get; set; }
    }
}