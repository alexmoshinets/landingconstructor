﻿using System;

namespace DomainModel.Content
{
    public class Product : BaseEntity
    {
        public bool IsHidden { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public string Image { get; set; }
        public string StringKey { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Updated { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
    }
}