﻿using System;
using System.Collections.Generic;


namespace DomainModel.Content
{
    public class Category: BaseEntity
    {
        public Category()
        {
            Products = new List<Product>();
        }
        public string StringKey { get; set; }
        public bool IsHidden { get; set; }
        public string Description { get; set; }
        public int? CategoryId { get; set; }
        public Category Parent { get; set; }
        public IList<Category> Childs { get; set; }
        public string Image { get; set; }
        public string Title { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Updated { get; set; }
        public IList<Product> Products { get; set; }
    }
}
