﻿using System;
using System.Collections.Generic;
using DomainModel.Navigation;

namespace DomainModel.Content
{
    public class Page : BaseEntity
    {
        public Page()
        {
            MenuItems = new List<MenuItem>();
        }

        public string StringKey { get; set; }

        public string Name { get; set; }
        public string Content { get; set; }

        public string BottomContent { get; set; }
        public bool IsSection { get; set; }

        public string MetaTitle { get; set; }
        public string MetaKeywords { get; set; }
        public string MetaDescription { get; set; }

        public DateTime Created { get; set; }
        public DateTime? Updated { get; set; }

        public IList<MenuItem> MenuItems { get; set; }
    }
}