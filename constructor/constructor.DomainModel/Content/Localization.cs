﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using OrangeJetpack.Localization;

namespace DomainModel.Content
{
    public class Localization : ILocalizable
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LocalizationId { get; set; }

        [Required]
        public string StringKey { get; set; }
        [Required, Localized]
        public string Name { get; set; }
    }
}
