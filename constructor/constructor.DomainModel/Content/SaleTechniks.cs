﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainModel.Content
{
    public class SaleTechniks: BaseEntity
    {
        public bool IsHidden { get; set; }
        public string StringKey { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public int Price { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Updated { get; set; }
    }
}
