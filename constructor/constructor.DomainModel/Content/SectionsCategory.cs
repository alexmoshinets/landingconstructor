﻿using System;
using System.Collections.Generic;

namespace DomainModel.Content
{
    public class SectionsCategory : BaseEntity
    {        
        public string StringKey { get; set; }
        public string Name { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Updated { get; set; }

    }
}
