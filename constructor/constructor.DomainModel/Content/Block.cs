﻿using System;

namespace DomainModel.Content
{
    public class Block : BaseEntity
    {
        public string StringKey { get; set; }

        public bool IsStatic { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public bool IsHidden { get; set; }

        public bool IsContent { get; set; }

        public int? BlockListId { get; set; }
        public BlockList BlockList { get; set; }

        public DateTime Created { get; set; }
        public DateTime? Updated { get; set; }
    }
}