﻿using System;
using System.Collections.Generic;

namespace DomainModel.Content
{
    public class Landing : BaseEntity
    {
        public Landing()
        {
            LandingIntegrations = new List<LandingIntegration>();
            LandingSettings = new List<LandingSetting>();
        }
        public string Url { get; set; }
        public bool IsTemplate { get; set; }
        public string StringKey { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeyWords { get; set; }
        public string SiteContent { get; set; }
        public string FullSiteContent { get; set; }
        public string Styles { get; set; }
        public string Scripts { get; set; }
        public string LastLoadScripts { get; set; }
        public string Links { get; set; }
        public string PageLoader { get; set; }
        public string Type { get; set; }
        public int? UserId { get; set; }
        public bool IsPublic { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Updated { get; set; }
        public List<LandingIntegration> LandingIntegrations { get; set; }
        public List<LandingSetting> LandingSettings { get; set; }
        public string Email { get; set; }
    }
}
