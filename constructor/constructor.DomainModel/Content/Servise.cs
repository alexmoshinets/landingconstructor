﻿using System;

namespace DomainModel.Content
{
    public class Servise : BaseEntity
    {
        public bool IsHidden { get; set; }
        public string StringKey { get; set; }
        public string Title { get; set; }
        public string MetaTitle { get; set; }
        public string Description { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeywords { get; set; }
        public string FullDescription { get; set; }
        public string Image { get; set; }
        public double Price { get; set; }
        public bool IsPresence { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Updated { get; set; }
        public int CategoryId { get; set; }
        public CategoryService Category { get; set; }
    }
}