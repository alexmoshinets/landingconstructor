﻿namespace DomainModel
{
    public abstract class BaseEntity
    {
        public const int NameLength = 256;
        public const int HandleLength = 256;

        public virtual int Id { get; set; }
    }
}