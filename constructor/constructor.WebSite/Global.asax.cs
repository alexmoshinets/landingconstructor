﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using WebSite.App_Start;

namespace WebSite
{
    // Примечание: Инструкции по включению классического режима IIS6 или IIS7 
    // см. по ссылке http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AutofacConfig.SetupContainer();
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            if (Request.Url == null) return;

            bool bResponse = false;
            string sUrlTo = string.Empty;
            string sUrlFrom = Request.Url.ToString().ToLower();

            if (sUrlFrom.Contains("www."))
            {
                sUrlTo = Request.Url.ToString().Replace("www.", string.Empty);
                bResponse = true;
            }
            if (sUrlFrom[sUrlFrom.Length - 1] == '/')
            {
                int count = 0;
                for (int i = 0; i < sUrlFrom.Length; i++)
                    if (sUrlFrom[i] == '/')
                        count++;
                if (count > 3)
                {
                    sUrlTo = Request.Url.ToString().Remove((Request.Url.ToString().Length - 1));
                    bResponse = true;
                }
            }

            if (bResponse)
            {
                Response.Clear();
                Response.StatusCode = 301;
                Response.StatusDescription = "Moved Permanently";
                Response.AddHeader("Location", sUrlTo);
                Response.End();
            }
        }

    }
}