﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DomainModel.Content;
using DomainModel.Navigation;
using Interfaces.Content;
using Interfaces.Common;
using Interfaces.Navigation;
using WebSite.Framework;
using WebSite.Models.Common;

namespace WebSite.Controllers
{
    public class CommonController : Controller
    {
        private readonly IMenuItemService _menuItemService;
        private readonly WorkContext _workContext;
        private readonly ISettingService _settingService;
        private readonly IMessageService _messageService;

        public CommonController(WorkContext workContext, IMenuItemService menuItemService, ISettingService settingService, IMessageService messageSevice)
        {
            _workContext = workContext;
            _menuItemService = menuItemService;
            _settingService = settingService;
            _messageService = messageSevice;
        }

        [ChildActionOnly]
        public ActionResult TopMenu(string currentController)
        {
            var menuItems = _menuItemService.GetByMenuKey("TopMenu", true);

            var model = menuItems.Select(_ => MenuItemViewModel.FromDomainModel(_, this)).ToList();

            return PartialView(model);
        }

        [ChildActionOnly]
        public ActionResult SideMenu(string currentController)
        {
            var menuItems = _menuItemService.GetByMenuKey("SideMenu", true);

            var model = menuItems.Select(_ => MenuItemViewModel.FromDomainModel(_, this)).ToList();

            return PartialView(model);
        }
        

        [ChildActionOnly]
        public ActionResult FooterMenu(string currentController)
        {
            var menuItems = _menuItemService.GetByMenuKey("FooterMenu", true);

            var model = menuItems.Select(_ => MenuItemViewModel.FromDomainModel(_, this)).ToList();

            return PartialView(model);
        }

        [ChildActionOnly]
        public ActionResult LoginPhoto()
        {
            var settings = _workContext.Settings;
            var setting = settings.FirstOrDefault(_ => _.StringKey.Equals("PhotoTimestamp"));
            if (setting != null)
            {
                ViewBag.Timestamp = setting.Value;
            }
            return PartialView();
        }

        [ChildActionOnly]
        public ActionResult SideAds()
        {
            var settings = _workContext.Settings;
            var setting = settings.FirstOrDefault(_ => _.StringKey.Equals("SideAdsTimestamp"));
            if (setting != null)
            {
                ViewBag.Timestamp = setting.Value;
            }
            return PartialView();
        }

        [ChildActionOnly]
        public ActionResult Logo()
        {
            var settings = _workContext.Settings;
            var setting = settings.FirstOrDefault(_ => _.StringKey.Equals("LogoTimestamp"));
            if (setting != null)
            {
                ViewBag.Timestamp = setting.Value;
            }
            return PartialView();
        }

        [ChildActionOnly]
        public ActionResult Map()
        {
            var map = _settingService.GetByKey("Map");
            var model = new MapViewModel();
            if (map != null)
            {
                model.Content = map.Value;
            }else{
                model.Content = "";
            }
            return View(model);
        }

        [ChildActionOnly]
        public ActionResult Copyright()
        {
            var copyright = _settingService.GetByKey("copyright");           
            var model = new MapViewModel();
            if (copyright != null) model.Content = copyright.Value; else model.Content = "";
            
            return PartialView(model);
        }

        [HttpPost]
        public string SendMessage(string name, string email, string message)
        {
            var cEmail = _settingService.GetByKey("Email");
            var textBody = "<table width='100%' border='0' cellspacing='1' cellpadding='1'>" +
                                                   "<tr>" +
                                                   "<td> Сообщение от: " + name + "</td>" +
                                                   "</tr>" +
                                                   "<tr>" +
                                                   "<td>Содержание:" + message + "</td>" +
                                                   "</tr>" +
                                                   "</table>";
                
                
                //"Обратная связь сайта caneximtrading.com <br/> <br/> Сообщение от: "
                //+ name + "<br/>Сообщение:" + message;
            _messageService.Send(cEmail.Value, email, textBody);
            return "ок!";
        }


        [HttpPost]
        public string СhangeLocalization(string lang)
        {
            if (lang == "ru")
            {
                Session["lang"] = "ru";
            }
            else
            {
                Session["lang"] = "en";
            }
            HttpCookie cooki = new HttpCookie("lang", Session["lang"].ToString());
            Response.SetCookie(cooki);
            return "ok!";
        }

        //public ActionResult Error404()
        //{
        //    return View("404");
        //}
    }
}