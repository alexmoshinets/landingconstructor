﻿using System.Web.Mvc;

namespace WebSite.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ViewResult Index()
        {
            return View();
        }

        // GET: Opportunities
        //[Route("~/Opportunities")]
        public ViewResult Opportunities()
        {
            return View();
        }

        // GET: Templates
        //[Route("~/Templates")]
        public ViewResult Templates()
        {
            return View();
        }

        // GET: Prices
        //[Route("~/Prices")]
        public ViewResult Prices()
        {
            return View();
        }

        // GET: Blog
        //[Route("~/Blog")]
        public ViewResult Blog()
        {
            return View();
        }

        // GET: Сontact
        //[Route("~/Contact")]
        public ViewResult Contact()
        {
            return View();
        }
    }
}