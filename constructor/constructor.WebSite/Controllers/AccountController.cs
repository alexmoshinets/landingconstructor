﻿using System;
using System.Web.Mvc;
using System.Web.Security;
using DomainModel.Account;
using Interfaces.Account;
using Interfaces.Common;
using WebSite.Models.Account;
using WebSite.Controllers;
using WebSite.Framework.Providers;

namespace WebSite.Controllers
{
    public class AccountController : BaseController
    {
        private readonly ILoggerService _loggerService;
        private readonly IMessageService _messageService;
        private readonly IUserService _userService;
        public AccountController(ILoggerService loggerService, IMessageService messageService, IUserService userService)
        {
            _loggerService = loggerService;
            _messageService = messageService;
            _userService = userService;
        }

        [HttpGet]
        public ActionResult LogIn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LogIn(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var userName = Membership.GetUserNameByEmail(model.Email);

                if (WebSecurity.Login(userName, model.Password, model.RememberMe))
                {
                    if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/") && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                    {
                        return Redirect(returnUrl);
                    }
                    return RedirectToAction("Index", "Home");
                }
                ModelState.AddModelError("", "Имя пользователя или пароль не является корректным.");
            }

            return View(model);
        }

        [HttpGet]
        // GET: /Account/Register
        public ViewResult Register()
        {
            return View();
        }
        [HttpPost]
        // POST: /Account/Register
        public ActionResult Register(RegisterViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var newUser = new User()
                {
                    Email = model.Email,
                    Password = model.Password,
                    UserName = model.Name,
                    PasswordQuestion = "",
                    PasswordAnswer = "",
                    IsApproved = false,
                    CreateDate = DateTime.Now
                };
                _userService.CreateUser(newUser);
                var userName = Membership.GetUserNameByEmail(model.Email);

                if (WebSecurity.Login(userName, model.Password, model.RememberMe))
                {
                    if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/") && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                    {
                        return Redirect(returnUrl);
                    }
                    return RedirectToAction("Index", "Home");
                }
                ModelState.AddModelError("", "Имя пользователя или пароль не является корректным.");
            }

            return View(model);
        }

        [HttpGet]
        public ActionResult Recovery()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Recovery(RecoveryViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = _userService.GetByEmail(model.Email);
                if (user != null)
                {
                    user = _userService.Update(user.Id, u =>
                    {
                        u.PasswordRecoveryToken = Guid.NewGuid();
                        u.PasswordRecoveryTokenExpirationDate = DateTime.UtcNow.AddHours(6);
                    });
                    var result = _messageService.Send(user.Email, "Смена пароля", "Для того чтоб сменить пароль перейдите по этой ссылке: " + Url.Action("Reset", "Account", new { token = user.PasswordRecoveryToken }, "http"));
                    if (result)
                    {
                        SuccessMessage("На ваш почтовый ящик было выслано сообщение, выполните инструкции для смены пароля.");
                    }
                    else
                    {
                        ErrorMessage("Во время отправки сообщения произошла ошибка, попробуйте повторить операцию позже или связаться с администратором.");
                    }
                }
                else
                {
                    ModelState.AddModelError("Email", "Не найден пользователь с таким емаил");
                }
            }

            return View(model);
        }


        [HttpGet]
        public ActionResult Reset(Guid? token)
        {
            if (!token.HasValue)
            {
                ErrorMessage("Невалидный код востановления, попробуйте ввести емаил заново");
                return RedirectToAction("Recovery", "Account");
            }
            var model = new ResetViewModel();
            model.Token = token.Value;
            return View(model);
        }

        [HttpPost]
        public ActionResult Reset(ResetViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = _userService.GetByPasswordRecoveryToken(model.Token);
                if (user == null)
                {
                    ErrorMessage("Не найден код сброса");
                    return RedirectToAction("Recovery");
                }

                if (user.PasswordRecoveryTokenExpirationDate < DateTime.UtcNow)
                {
                    ErrorMessage("Верификационный код устарел");
                    return RedirectToAction("Recovery");
                }

                var result = WebSecurity.ResetPassword(user.UserName, model.Password);
                if (!result)
                {
                    ModelState.AddModelError("Password", "Ошибка сброса пароля");
                }

                SuccessMessage("Пароль был успешно изменен, вы можете попробовать выполнить вход.");
                return RedirectToAction("LogIn");
            }

            return View(model);
        }

        #region Helpers

        //private IAuthenticationManager AuthenticationManager
        //{
        //    get
        //    {
        //        return HttpContext.GetOwinContext().Authentication;
        //    }
        //}

        //private void AddErrors(IdentityResult result)
        //{
        //    foreach (var error in result.Errors)
        //    {
        //        ModelState.AddModelError("", error);
        //    }
        //}

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return RedirectToAction("Index", "Home");
        }

        #endregion
        //// GET: /Account/LogOff
        //[AllowAnonymous]
        //public ActionResult LogOff()
        //{
        //    AuthenticationManager.SignOut(new[] { DefaultAuthenticationTypes.ApplicationCookie });

        //    return RedirectToAction("Login");
        //}
    }
}