﻿using System.Web;
using System.Web.Mvc;
using DomainModel.Content;
using Interfaces.Content;
using WebSite.Models.Content;

namespace WebSite.Controllers
{
    public class PageController : Controller
    {
        private readonly IPageService _pageService;

        public PageController(IPageService pageService)
        {
            _pageService = pageService;
        }

        //
        // GET: /Page/
        public ActionResult Index(string stringKey)
        {
            if (Session["lang"] == null)
            {
                HttpCookie cookie = Request.Cookies["lang"];
                if (cookie != null)
                {
                    Session["lang"] = cookie.Value;
                }
                else
                {
                    HttpCookie cooki = new HttpCookie("lang", "ru");
                    Response.SetCookie(cooki);
                    Session["lang"] = "ru";
                }
            }
            var page = _pageService.GetByKey(stringKey);
            if (page == null)
            {
                return HttpNotFound();
            }

            var model = PageViewModel.FromDomainModel(page);
            return View(model);
        }
    }
}