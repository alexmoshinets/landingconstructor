﻿using System.Web.Mvc;
using WebSite.Framework;

namespace WebSite.Controllers
{
    public class BaseController : Controller
    {
        protected void SuccessMessage(string message)
        {
            TempData[Constants.NotificationsSuccess] = message;
        }

        protected void ErrorMessage(string message)
        {
            TempData[Constants.NotificationsError] = message;
        }
    }
}