﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace WebSite.Framework.Helpers
{
    public static class DescriptionEnum
    {
        public static string GetDescription(this Enum value)
        {
            string output = null;
            var type = value.GetType();

            var fi = type.GetField(value.ToString());
            var attrs = fi.GetCustomAttributes(typeof(DescriptionAttribute), false) as DescriptionAttribute[];
            if (attrs != null && attrs.Length > 0)
            {
                output = attrs[0].Description;
            }

            return output;
        }
    }
}