﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace WebSite.Framework.Helpers
{
    public static class FileHelper
    {
        public static string GetFileSize(long lenght)
        {
            string[] sizes = { "B", "KB", "MB", "GB" };
            var order = 0;
            double len = lenght;
            while (len >= 1024 && order + 1 < sizes.Length)
            {
                order++;
                len = len / 1024;
            }

            return String.Format("{0:0.##} {1}", len, sizes[order]);
        }

        public static IEnumerable<FileInfo> GetFilesByExtensions(this DirectoryInfo dir, params string[] extensions)
        {
            if (extensions == null) return null;
            var files = dir.EnumerateFiles();
            return
                files.Where(
                    f => extensions.Contains(f.Extension.ToLower()) || extensions.Contains(f.Extension.ToLower()));
        }
    }
}