﻿using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using DomainModel.Content;
using Interfaces.Content;

namespace WebSite.Framework
{
    
    public class WorkContext
    {
        private readonly HttpContextBase _httpContextBase;
        private readonly ISettingService _settingService;
       
        private IList<Setting> _settings;

        public WorkContext(HttpContextBase httpContextBase, ISettingService settingService)
        {
            _httpContextBase = httpContextBase;
            _settingService = settingService;
        }

        public IList<Setting> Settings
        {
            get
            {
                if (_settings == null)
                {
                    _settings = _settingService.GetAll();
                }
                return _settings;
            }
        }

        public static string FilesPath
        {
            get { return "/Content/Files/"; }
        }

        public string FilesAbsPath
        {
            get { return _httpContextBase.Server.MapPath(FilesPath); }
        }

        public static string SliderPath
        {
            get { return FilesPath + "Slider/"; }
        }

        public static string PartnerPath
        {
            get { return FilesPath + "Partner/"; }
        }

        public static string ArticlePath
        {
            get { return FilesPath + "Article/"; }
        }
        
        public static string SectionPath
        {
            get { return FilesPath + "Section/"; }
        }

        public static string ServicesPath
        {
            get { return FilesPath + "Services/"; }
        }

        public static string ProducerPath
        {
            get { return FilesPath + "Producer/"; }
        }

        public static string ProductPath
        {
            get { return FilesPath + "Product/"; }
        }

        public string SliderAbsPath
        {
            get { return _httpContextBase.Server.MapPath(SliderPath); }
        }

        public static string PhotoPath
        {
            get { return FilesPath + "Photo/"; }
        }

        public string PhotoAbsPath
        {
            get { return _httpContextBase.Server.MapPath(PhotoPath); }
        }

        public static string CategoriesPath
        {
            get { return FilesPath + "Categories/"; }
        }

        public string CategoriesAbsPath
        {
            get { return _httpContextBase.Server.MapPath(CategoriesPath); }
        }
        public static string UserImagePath
        {
            get { return "/Areas/Cabinet/Gallery/UserFiles/"; }
        }

        public string UserImageAbsPath
        {
            get { return _httpContextBase.Server.MapPath(UserImagePath); }
        }
    }
}