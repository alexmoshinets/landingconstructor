﻿using System;
using System.Collections.Generic;
using System.Configuration.Provider;
using System.Linq;
using System.Web.Security;
using Interfaces.Account;
using Interfaces.Common;
using Services.Account;
using Services.Common;

namespace WebSite.Framework.Providers
{
    public class CustomRoleProvider : RoleProvider
    {
        private readonly ILoggerService _loggerService;
        private readonly IRoleService _roleService;
        private readonly IUserService _userService;

        public CustomRoleProvider()
        {
            // todo: решить вопрос привязки интерфейса через аутофак
            _roleService = new RoleService();
            _userService = new UserService();
            _loggerService = new LoggerService();
        }

        /// <summary>
        ///     Получает или задает имя приложения, для которого будут сохраняться и извлекаться сведения о роли.
        /// </summary>
        /// <returns>
        ///     Имя приложения, для которого будут сохраняться и извлекаться сведения о роли.
        /// </returns>
        public override string ApplicationName { get; set; }

        /// <summary>
        ///     Возвращает значение, указывающее связан ли указанный пользователь с указанной ролью для настроенного
        ///     applicationName.
        /// </summary>
        /// <returns>
        ///     Значение true, если указанный пользователь связан с указанной ролью для настроенного applicationName; в противном
        ///     случае — false.
        /// </returns>
        /// <param name="userName">Имя пользователя для поиска.</param>
        /// <param name="roleName">Роль, в которой следует выполнить поиск.</param>
        public override bool IsUserInRole(string userName, string roleName)
        {
            var result = false;
            try
            {
                result = _roleService.IsUserInRole(userName, roleName);
            }
            catch (Exception ex)
            {
                _loggerService.Error("Проблема получения роли для пользователя", ex);
            }
            return result;
        }

        /// <summary>
        ///     Возвращает список ролей, с которыми связан указанный пользователей для настроенного applicationName.
        /// </summary>
        /// <returns>
        ///     Массив строк, содержащий имена всех ролей, с которыми связан указанный пользователь для настроенного
        ///     applicationName.
        /// </returns>
        /// <param name="userName">Имя пользователя, для которого нужно возвратить список ролей.</param>
        public override string[] GetRolesForUser(string userName)
        {
            var result = new List<string>();
            try
            {
                result = _roleService.GetAllByUserName(userName).Select(_ => _.Name).ToList();
            }
            catch (Exception ex)
            {
                _loggerService.Error("Проблема получения роли для пользователя", ex);
            }
            return result.ToArray();
        }

        /// <summary>
        ///     Добавляет новую роль к источнику данных для настроенного приложения applicationName.
        /// </summary>
        /// <param name="roleName">Имя создаваемой роли.</param>
        public override void CreateRole(string roleName)
        {
            if (roleName.Contains(","))
            {
                throw new ArgumentException("Role names cannot contain commas.");
            }

            if (RoleExists(roleName))
            {
                throw new ProviderException("Role name already exists.");
            }

            try
            {
                _roleService.Create(roleName);
            }
            catch (Exception ex)
            {
                _loggerService.Error("Проблема создания роли", ex);
            }
        }

        /// <summary>
        ///     Удаляет роль из источника данных для настроенного приложения applicationName.
        /// </summary>
        /// <returns>
        ///     Значение true, если роль успешно удалена; в противном случае — значение false.
        /// </returns>
        /// <param name="roleName">Имя удаляемой роли.</param>
        /// <param name="throwOnPopulatedRole">
        ///     Если true, то выдайте исключение, если <paramref name="roleName" /> имеет один или
        ///     более членов и не удаляйте <paramref name="roleName" />.
        /// </param>
        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            if (!RoleExists(roleName))
            {
                throw new ProviderException("Role does not exist.");
            }

            if (throwOnPopulatedRole && GetUsersInRole(roleName).Length > 0)
            {
                throw new ProviderException("Cannot delete a populated role.");
            }

            var result = false;
            try
            {
                result = _roleService.Delete(roleName);
            }
            catch (Exception ex)
            {
                _loggerService.Error("Проблема удаления роли", ex);
            }
            return result;
        }

        /// <summary>
        ///     Возвращает значение, указывающее, существует ли указанная роль в источнике данных ролей для настроенного
        ///     applicationName.
        /// </summary>
        /// <returns>
        ///     true, если имя роли уже существует в источнике данных для настроенного applicationName; в противном случае — false.
        /// </returns>
        /// <param name="roleName">Имя роли, которую необходимо найти в источнике данных.</param>
        public override bool RoleExists(string roleName)
        {
            var result = false;
            try
            {
                result = _roleService.IsRoleExist(roleName);
            }
            catch (Exception ex)
            {
                _loggerService.Error("Проблема получения роли для пользователя", ex);
            }
            return result;
        }

        /// <summary>
        ///     Добавляет указанные имена пользователей к указанным ролям для установленного приложения applicationName.
        /// </summary>
        /// <param name="userNames">Массив строк имен пользователей для добавления в указанные роли. </param>
        /// <param name="roleNames">Массив строк имен пользователей, в который добавляются указанные имена пользователей.</param>
        public override void AddUsersToRoles(string[] userNames, string[] roleNames)
        {
            if (roleNames.Any(rolename => !RoleExists(rolename)))
            {
                throw new ProviderException("Role name not found.");
            }

            foreach (var username in userNames)
            {
                if (username.Contains(","))
                {
                    throw new ArgumentException("User names cannot contain commas.");
                }

                if (roleNames.Any(rolename => IsUserInRole(username, rolename)))
                {
                    throw new ProviderException("User is already in role.");
                }
            }

            try
            {
                _roleService.AddUsersToRoles(userNames, roleNames);
            }
            catch (Exception ex)
            {
                _loggerService.Error("Проблема добавления ролей юзерам", ex);
            }
        }

        /// <summary>
        ///     Удаляет указанные имена пользователей из указанных ролей для установленного приложения applicationName.
        /// </summary>
        /// <param name="userNames">Массив строк имен пользователей для удаления из указанных ролей. </param>
        /// <param name="roleNames">Массив строк имен ролей, из которых необходимо удалить указанные имена пользователей.</param>
        public override void RemoveUsersFromRoles(string[] userNames, string[] roleNames)
        {
            if (roleNames.Any(rolename => !RoleExists(rolename)))
            {
                throw new ProviderException("Role name not found.");
            }

            if (userNames.Any(username => roleNames.Any(rolename => !IsUserInRole(username, rolename))))
            {
                throw new ProviderException("User is not in role.");
            }

            try
            {
                _roleService.RemoveUsersFromRoles(userNames, roleNames);
            }
            catch (Exception ex)
            {
                _loggerService.Error("Проблема удаления ролей у юзеров", ex);
            }
        }

        /// <summary>
        ///     Возвращает список пользователей, связанных с указанной ролью, для настроенного applicationName.
        /// </summary>
        /// <returns>
        ///     Массив строк, содержащий имена всех пользователей, которые являются членами указанной роли для настроенного
        ///     applicationName.
        /// </returns>
        /// <param name="roleName">Имя роли, для которой необходимо возвратить список пользователей.</param>
        public override string[] GetUsersInRole(string roleName)
        {
            var result = new List<string>();
            try
            {
                result = _userService.GetAllByRoleName(roleName).Select(_ => _.UserName).ToList();
            }
            catch (Exception ex)
            {
                _loggerService.Error("Проблема получения всех юзеров по роли", ex);
            }
            return result.ToArray();
        }

        /// <summary>
        ///     Возвращает список всех ролей для настроенного applicationName.
        /// </summary>
        /// <returns>
        ///     Массив строк, содержащий имена всех ролей, сохраненных в источнике данных для настроенного applicationName.
        /// </returns>
        public override string[] GetAllRoles()
        {
            var result = new List<string>();
            try
            {
                result = _roleService.GetAll().Select(_ => _.Name).ToList();
            }
            catch (Exception ex)
            {
                _loggerService.Error("Проблема получения всех ролей", ex);
            }
            return result.ToArray();
        }

        /// <summary>
        ///     Возвращает массив имен пользователей в роли, у которых имена совпадают с указанными именами пользователей.
        /// </summary>
        /// <returns>
        ///     Массив строк, содержащий имена всех пользователей, имена которых совпадают с <paramref name="usernameToMatch" />, и
        ///     которые являются членами указанной роли.
        /// </returns>
        /// <param name="roleName">Роль, в которой следует выполнить поиск.</param>
        /// <param name="usernameToMatch">Имя пользователя для поиска.</param>
        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            var result = new List<string>();
            try
            {
                result =
                    _userService.GetAllByRoleNameAndUserNameMatch(roleName, usernameToMatch)
                        .Select(_ => _.UserName)
                        .ToList();
            }
            catch (Exception ex)
            {
                _loggerService.Error("Проблема получения всех юзеров по роли", ex);
            }
            return result.ToArray();
        }
    }
}