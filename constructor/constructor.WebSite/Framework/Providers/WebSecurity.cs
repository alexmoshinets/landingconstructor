﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Security;

namespace WebSite.Framework.Providers
{
    public class WebSecurity
    {
        public static HttpContextBase Context
        {
            get { return new HttpContextWrapper(HttpContext.Current); }
        }

        public static HttpRequestBase Request
        {
            get { return Context.Request; }
        }

        public static HttpResponseBase Response
        {
            get { return Context.Response; }
        }

        public static IPrincipal User
        {
            get { return Context.User; }
        }

        public static bool IsAuthenticated
        {
            get { return User.Identity.IsAuthenticated; }
        }

        public static MembershipCreateStatus Register(string userName, string password, string email, bool isApproved)
        {
            MembershipCreateStatus createStatus;
            var membershipUser = Membership.CreateUser(userName, password, email, null, null, isApproved,
                null, out createStatus);

            if (createStatus == MembershipCreateStatus.Success && membershipUser != null)
            {
                if (isApproved)
                {
                    FormsAuthentication.SetAuthCookie(userName, false);
                }
            }

            return createStatus;
        }

        public static bool Login(string username, string password, bool persistCookie = false)
        {
            var success = Membership.ValidateUser(username, password);
            if (success)
            {
                FormsAuthentication.SetAuthCookie(username, persistCookie);
            }
            return success;
        }

        public static void Logout()
        {
            FormsAuthentication.SignOut();
        }

        public static MembershipUser GetUser(string username)
        {
            return Membership.GetUser(username);
        }

        public static bool ChangePassword(string userName, string currentPassword, string newPassword)
        {
            var success = false;

            var currentUser = Membership.GetUser(userName, true);
            if (currentUser != null) success = currentUser.ChangePassword(currentPassword, newPassword);

            return success;
        }

        public static bool ResetPassword(string userName, string newPassword)
        {
            var success = false;

            var currentUser = Membership.GetUser(userName, true);
            if (currentUser != null)
            {
                var currentPassword = currentUser.ResetPassword();
                success = currentUser.ChangePassword(currentPassword, newPassword);
            }

            return success;
        }

        public static bool DeleteUser(string username)
        {
            return Membership.DeleteUser(username);
        }

        public static int GetUserId(string userName)
        {
            var user = Membership.GetUser(userName);
            Debug.Assert(user != null, "user != null");
            Debug.Assert(user.ProviderUserKey != null, "user.ProviderUserKey != null");
            return (int)user.ProviderUserKey;
        }

        public static List<MembershipUser> FindUsersByEmail(string email, int pageIndex, int pageSize)
        {
            int totalRecords;
            return
                Membership.FindUsersByEmail(email, pageIndex, pageSize, out totalRecords)
                    .Cast<MembershipUser>()
                    .ToList();
        }

        public static List<MembershipUser> FindUsersByName(string username, int pageIndex, int pageSize)
        {
            int totalRecords;
            return
                Membership.FindUsersByName(username, pageIndex, pageSize, out totalRecords)
                    .Cast<MembershipUser>()
                    .ToList();
        }

        public static List<MembershipUser> GetAllUsers(int pageIndex, int pageSize)
        {
            int totalRecords;
            return Membership.GetAllUsers(pageIndex, pageSize, out totalRecords).Cast<MembershipUser>().ToList();
        }
    }
}