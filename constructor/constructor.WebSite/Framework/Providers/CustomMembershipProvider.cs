﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Configuration.Provider;
using System.Security.Cryptography;
using System.Text;
using System.Web.Configuration;
using System.Web.Hosting;
using System.Web.Security;
using DomainModel.Account;
using Interfaces.Account;
using Interfaces.Common;
using Services.Account;
using Services.Common;

namespace WebSite.Framework.Providers
{
    public class CustomMembershipProvider : MembershipProvider
    {
        private readonly ILoggerService _loggerService;
        private readonly IUserService _usersService;
        private string _applicationName;
        private bool _enablePasswordReset;
        private bool _enablePasswordRetrieval;
        private MachineKeySection _machineKey;
        private int _maxInvalidPasswordAttempts;
        private int _minRequiredNonalphanumericCharacters;
        private int _minRequiredPasswordLength;
        private int _passwordAttemptWindow;
        private MembershipPasswordFormat _passwordFormat;
        private string _passwordStrengthRegularExpression;
        private bool _requiresQuestionAndAnswer;
        private bool _requiresUniqueEmail;


        public CustomMembershipProvider()
        {
            // todo: решить вопрос привязки интерфейса через аутофак
            _usersService = new UserService();
            _loggerService = new LoggerService();
        }

        public override void Initialize(string name, NameValueCollection config)
        {
            if (config == null)
                throw new ArgumentNullException("config");

            if (name.Length == 0) name = "CustomMembershipProvider";

            if (String.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", "Custom Membership Provider");
            }

            base.Initialize(name, config);

            _applicationName = GetConfigValue(config["applicationName"], HostingEnvironment.ApplicationVirtualPath);
            _maxInvalidPasswordAttempts = Convert.ToInt32(GetConfigValue(config["maxInvalidPasswordAttempts"], "5"));
            _passwordAttemptWindow = Convert.ToInt32(GetConfigValue(config["passwordAttemptWindow"], "10"));
            _minRequiredNonalphanumericCharacters =
                Convert.ToInt32(GetConfigValue(config["minRequiredNonalphanumericCharacters"], "1"));
            _minRequiredPasswordLength = Convert.ToInt32(GetConfigValue(config["minRequiredPasswordLength"], "6"));
            _enablePasswordReset = Convert.ToBoolean(GetConfigValue(config["enablePasswordReset"], "true"));
            _passwordStrengthRegularExpression =
                Convert.ToString(GetConfigValue(config["passwordStrengthRegularExpression"], ""));

            _enablePasswordRetrieval = Convert.ToBoolean(GetConfigValue(config["enablePasswordRetrieval"], "true"));
            _requiresQuestionAndAnswer = Convert.ToBoolean(GetConfigValue(config["requiresQuestionAndAnswer"], "false"));
            _requiresUniqueEmail = Convert.ToBoolean(GetConfigValue(config["requiresUniqueEmail"], "true"));

            var tempFormat = config["passwordFormat"] ?? "Clear";
            switch (tempFormat)
            {
                case "Hashed":
                    _passwordFormat = MembershipPasswordFormat.Hashed;
                    break;
                case "Encrypted":
                    _passwordFormat = MembershipPasswordFormat.Encrypted;
                    break;
                case "Clear":
                    _passwordFormat = MembershipPasswordFormat.Clear;
                    break;
                default:
                    throw new ProviderException("Password format not supported.");
            }

            // Get encryption and decryption key information from the configuration.
            var cfg = WebConfigurationManager.OpenWebConfiguration(HostingEnvironment.ApplicationVirtualPath);
            _machineKey = (MachineKeySection)cfg.GetSection("system.web/machineKey");

            if (_machineKey.ValidationKey.Contains("AutoGenerate"))
                if (PasswordFormat != MembershipPasswordFormat.Clear)
                    throw new ProviderException("Hashed or Encrypted passwords " +
                                                "are not supported with auto-generated keys.");
        }

        private string GetConfigValue(string configValue, string defaultValue)
        {
            return (string.IsNullOrEmpty(configValue)) ? defaultValue : configValue;
        }

        //
        // CheckPassword
        //   Compares password values based on the MembershipPasswordFormat.
        //
        private bool CheckPassword(string password, string dbpassword)
        {
            var pass1 = password;
            var pass2 = dbpassword;

            switch (PasswordFormat)
            {
                case MembershipPasswordFormat.Encrypted:
                    pass2 = UnEncodePassword(dbpassword);
                    break;
                case MembershipPasswordFormat.Hashed:
                    pass1 = EncodePassword(password);
                    break;
                default:
                    break;
            }

            return pass1 == pass2;
        }

        //
        // EncodePassword
        //   Encrypts, Hashes, or leaves the password clear based on the PasswordFormat.
        //
        private string EncodePassword(string password)
        {
            var encodedPassword = password;

            switch (PasswordFormat)
            {
                case MembershipPasswordFormat.Clear:
                    break;
                case MembershipPasswordFormat.Encrypted:
                    encodedPassword = Convert.ToBase64String(EncryptPassword(Encoding.Unicode.GetBytes(password)));
                    break;
                case MembershipPasswordFormat.Hashed:
                    var hash = new HMACSHA1();
                    hash.Key = HexToByte(_machineKey.ValidationKey);
                    encodedPassword = Convert.ToBase64String(hash.ComputeHash(Encoding.Unicode.GetBytes(password)));
                    break;
                default:
                    throw new ProviderException("Unsupported password format.");
            }

            return encodedPassword;
        }


        //
        // UnEncodePassword
        //   Decrypts or leaves the password clear based on the PasswordFormat.
        //
        private string UnEncodePassword(string encodedPassword)
        {
            var password = encodedPassword;

            switch (PasswordFormat)
            {
                case MembershipPasswordFormat.Clear:
                    break;
                case MembershipPasswordFormat.Encrypted:
                    password = Encoding.Unicode.GetString(DecryptPassword(Convert.FromBase64String(password)));
                    break;
                case MembershipPasswordFormat.Hashed:
                    throw new ProviderException("Cannot unencode a hashed password.");
                default:
                    throw new ProviderException("Unsupported password format.");
            }

            return password;
        }

        //
        // HexToByte
        //   Converts a hexadecimal string to a byte array. Used to convert encryption
        // key values from the configuration.
        //
        private byte[] HexToByte(string hexString)
        {
            var returnBytes = new byte[hexString.Length / 2];
            for (var i = 0; i < returnBytes.Length; i++)
                returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
            return returnBytes;
        }

        #region Properties

        /// <summary>
        ///     Показывает, настроена ли в поставщике участия возможность извлечения пользователями собственных паролей.
        /// </summary>
        /// <returns>
        ///     Значение true, если в поставщике участия настроена возможность извлечения пароля, в противном случае — значение
        ///     false.Значение по умолчанию — false.
        /// </returns>
        public override bool EnablePasswordRetrieval
        {
            get { return _enablePasswordRetrieval; }
        }

        /// <summary>
        ///     Показывает, настроена ли в поставщике участия возможность сброса пользователями собственных паролей.
        /// </summary>
        /// <returns>
        ///     Значение true, если поставщик участия поддерживает сброс пароля; в противном случае — значение false.Значение по
        ///     умолчанию — true.
        /// </returns>
        public override bool EnablePasswordReset
        {
            get { return _enablePasswordReset; }
        }

        /// <summary>
        ///     Возвращает значение, показывающее, настроен ли поставщик участия, чтобы запрашивать у пользователя ответ на
        ///     контрольный вопрос для изменения или извлечения пароля.
        /// </summary>
        /// <returns>
        ///     Значение true, если требуется ответ на контрольный вопрос для изменения или извлечения пароля; в противном случае —
        ///     значение false.Значение по умолчанию — true.
        /// </returns>
        public override bool RequiresQuestionAndAnswer
        {
            get { return _requiresQuestionAndAnswer; }
        }

        /// <summary>
        ///     Имя приложения, использующего пользовательского поставщика участия.
        /// </summary>
        /// <returns>
        ///     Имя приложения, использующего пользовательского поставщика участия.
        /// </returns>
        public override string ApplicationName
        {
            get { return _applicationName; }
            set { _applicationName = value; }
        }

        /// <summary>
        ///     Возвращает количество попыток ввода недопустимых пароля или контрольного ответа для пароля, после которых
        ///     пользователь членства блокируется.
        /// </summary>
        /// <returns>
        ///     Количество попыток ввода недопустимых пароля или контрольного ответа для пароля, после которых пользователь
        ///     членства блокируется.
        /// </returns>
        public override int MaxInvalidPasswordAttempts
        {
            get { return _maxInvalidPasswordAttempts; }
        }

        /// <summary>
        ///     Возвращает количество минут, в пределах которого допускается ввод пароля или контрольного ответа для пароля. По
        ///     истечение данного промежутка времени пользователь членства блокируется.
        /// </summary>
        /// <returns>
        ///     Количество минут, в пределах которого допускается ввод пароля или контрольного ответа для пароля. По истечение
        ///     данного промежутка времени пользователь членства блокируется.
        /// </returns>
        public override int PasswordAttemptWindow
        {
            get { return _passwordAttemptWindow; }
        }

        /// <summary>
        ///     Возвращает значение, показывающее, настроен ли поставщик участия, чтобы требовать уникальный адрес электронной
        ///     почты для каждого имени пользователя.
        /// </summary>
        /// <returns>
        ///     Значение true, если поставщик участия требует уникального адреса электронной почты; в противном случае — значение
        ///     false.Значение по умолчанию — true.
        /// </returns>
        public override bool RequiresUniqueEmail
        {
            get { return _requiresUniqueEmail; }
        }

        /// <summary>
        ///     Возвращает значение, показывающее формат хранения паролей в хранилище данных членства.
        /// </summary>
        /// <returns>
        ///     Одно из значений <see cref="T:System.Web.Security.MembershipPasswordFormat" />, показывающее формат хранения
        ///     паролей в хранилище данных.
        /// </returns>
        public override MembershipPasswordFormat PasswordFormat
        {
            get { return _passwordFormat; }
        }

        /// <summary>
        ///     Возвращает минимально допустимую длину пароля.
        /// </summary>
        /// <returns>
        ///     Минимально допустимая длина пароля.
        /// </returns>
        public override int MinRequiredPasswordLength
        {
            get { return _minRequiredPasswordLength; }
        }

        /// <summary>
        ///     Возвращает минимальное количество специальных знаков, которые должны присутствовать в допустимом пароле.
        /// </summary>
        /// <returns>
        ///     Минимальное количество специальных знаков, которые должны присутствовать в допустимом пароле.
        /// </returns>
        public override int MinRequiredNonAlphanumericCharacters
        {
            get { return _minRequiredNonalphanumericCharacters; }
        }

        /// <summary>
        ///     Возвращает регулярное выражение, используемое для оценки пароля.
        /// </summary>
        /// <returns>
        ///     Регулярное выражение, используемое для оценки пароля.
        /// </returns>
        public override string PasswordStrengthRegularExpression
        {
            get { return _passwordStrengthRegularExpression; }
        }

        #endregion

        #region Functions

        /// <summary>
        ///     Добавляет нового пользователя членства в источник данных.
        /// </summary>
        /// <returns>
        ///     Объект <see cref="T:System.Web.Security.MembershipUser" />, заполненный информацией для вновь созданного
        ///     пользователя.
        /// </returns>
        /// <param name="username">Имя для нового пользователя. </param>
        /// <param name="password">Пароль для нового пользователя. </param>
        /// <param name="email">Адрес электронной почты для нового пользователя.</param>
        /// <param name="passwordQuestion">Контрольный вопрос для пароля нового пользователя.</param>
        /// <param name="passwordAnswer">Контрольный ответ для пароля нового пользователя.</param>
        /// <param name="isApproved">Одобрена ли проверка нового пользователя.</param>
        /// <param name="providerUserKey">Уникальный идентификатор из источника данных членства для пользователя.</param>
        /// <param name="status">
        ///     Значение перечисления <see cref="T:System.Web.Security.MembershipCreateStatus" />, показывающее
        ///     успешно ли создан пользователь.
        /// </param>
        public override MembershipUser CreateUser(string username, string password, string email,
            string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey,
            out MembershipCreateStatus status)
        {
            if (string.IsNullOrEmpty(username))
            {
                status = MembershipCreateStatus.InvalidUserName;
                return null;
            }
            if (string.IsNullOrEmpty(password))
            {
                status = MembershipCreateStatus.InvalidPassword;
                return null;
            }
            if (string.IsNullOrEmpty(email))
            {
                status = MembershipCreateStatus.InvalidEmail;
                return null;
            }

            var hashedPassword = EncodePassword(password);

            var args = new ValidatePasswordEventArgs(username, password, true);
            OnValidatingPassword(args);

            if (args.Cancel || hashedPassword.Length > 128)
            {
                status = MembershipCreateStatus.InvalidPassword;
                return null;
            }

            if (RequiresUniqueEmail && GetUserNameByEmail(email) != "")
            {
                status = MembershipCreateStatus.DuplicateEmail;
                return null;
            }

            var u = GetUser(username, false);
            if (u != null)
            {
                status = MembershipCreateStatus.DuplicateUserName;
                return null;
            }

            var newUser = _usersService.Create(username, hashedPassword, email, passwordQuestion, passwordAnswer,
                isApproved, DateTime.UtcNow);
            status = MembershipCreateStatus.Success;

            return new MembershipUser(Membership.Provider.Name, newUser.UserName, newUser.Id, newUser.Email,
                passwordQuestion, null, newUser.IsApproved, newUser.IsLockedOut, newUser.CreateDate,
                newUser.LastLoginDate.GetValueOrDefault(), newUser.LastActivityDate.GetValueOrDefault(),
                newUser.LastPasswordChangedDate.GetValueOrDefault(), newUser.LastLockoutDate.GetValueOrDefault());
        }

        /// <summary>
        ///     Проверяет существование указанного имени пользователя и пароля в источнике данных.
        /// </summary>
        /// <returns>
        ///     Значение true, если указанные имя пользователя и пароль действительны; в противном случае — значение false.
        /// </returns>
        /// <param name="username">Имя пользователя для проверки. </param>
        /// <param name="password">Пароль заданного пользователя. </param>
        public override bool ValidateUser(string username, string password)
        {
            if (string.IsNullOrEmpty(username))
            {
                return false;
            }
            if (string.IsNullOrEmpty(password))
            {
                return false;
            }

            var user = _usersService.GetByName(username);

            if (user == null)
            {
                return false;
            }
            if (!user.IsApproved)
            {
                return false;
            }
            if (user.IsLockedOut)
            {
                return false;
            }

            var verificationSucceeded = CheckPassword(user.Password, password);

            UpdateFailureCount(username, verificationSucceeded);

            return verificationSucceeded;
        }

        private void UpdateFailureCount(string username, bool verificationSucceeded)
        {
            var user = _usersService.GetByName(username);

            if (verificationSucceeded)
            {
                user.PasswordFailuresSinceLastSuccess = 0;
                user.LastLoginDate = DateTime.UtcNow;
                user.LastActivityDate = DateTime.UtcNow;
            }
            else
            {
                var failures = user.PasswordFailuresSinceLastSuccess;
                if (failures < MaxInvalidPasswordAttempts)
                {
                    user.PasswordFailuresSinceLastSuccess += 1;
                    user.LastPasswordFailureDate = DateTime.UtcNow;
                }
                else if (failures >= MaxInvalidPasswordAttempts)
                {
                    user.LastPasswordFailureDate = DateTime.UtcNow;
                    user.LastLockoutDate = DateTime.UtcNow;
                    user.IsLockedOut = true;
                }
            }

            _usersService.Update(user.Id, u =>
            {
                u.PasswordFailuresSinceLastSuccess = user.PasswordFailuresSinceLastSuccess;
                u.LastLoginDate = user.LastLoginDate;
                u.LastActivityDate = user.LastActivityDate;
                u.LastPasswordFailureDate = user.LastPasswordFailureDate;
                u.IsLockedOut = user.IsLockedOut;
            });
        }

        /// <summary>
        ///     Возвращает информацию пользователя из источника данных, основываясь на уникальном идентификаторе для пользователя
        ///     членства.Предоставляет параметр для обновления метки даты и времени последней операции пользователя.
        /// </summary>
        /// <returns>
        ///     Объект <see cref="T:System.Web.Security.MembershipUser" />, заполненный информацией конкретного пользователя из
        ///     источника данных.
        /// </returns>
        /// <param name="providerUserKey">
        ///     Уникальный идентификатор пользователя членства, для которого необходимо получить
        ///     информацию.
        /// </param>
        /// <param name="userIsOnline">
        ///     Значение true, чтобы обновить метку даты и времени последней операции пользователя. Значение
        ///     false, чтобы вернуть информацию пользователя без обновления метки даты и времени последней операции пользователя.
        /// </param>
        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            if (!(providerUserKey is int)) return null;

            var user = _usersService.GetById((int)providerUserKey);
            if (user == null) return null;

            if (userIsOnline)
            {
                _usersService.Update(user.Id, u => { u.LastActivityDate = DateTime.UtcNow; });
            }
            return new MembershipUser(Membership.Provider.Name, user.UserName, user.Id, user.Email, null, null,
                user.IsApproved, user.IsLockedOut, user.CreateDate, user.LastLoginDate.GetValueOrDefault(),
                user.LastActivityDate.GetValueOrDefault(), user.LastPasswordChangedDate.GetValueOrDefault(),
                user.LastLockoutDate.GetValueOrDefault());
        }

        /// <summary>
        ///     Возвращает информацию из источника данных для пользователя.Предоставляет параметр для обновления метки даты и
        ///     времени последней операции пользователя.
        /// </summary>
        /// <returns>
        ///     Объект <see cref="T:System.Web.Security.MembershipUser" />, заполненный информацией конкретного пользователя из
        ///     источника данных.
        /// </returns>
        /// <param name="username">Имя пользователя, для которого нужно получить информацию. </param>
        /// <param name="userIsOnline">
        ///     Значение true, чтобы обновить метку даты и времени последней операции пользователя. Значение
        ///     false, чтобы вернуть информацию пользователя без обновления метки даты и времени последней операции пользователя.
        /// </param>
        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            if (string.IsNullOrEmpty(username)) return null;

            var user = _usersService.GetByName(username);
            if (user == null) return null;

            if (userIsOnline)
            {
                _usersService.Update(user.Id, u => { u.LastActivityDate = DateTime.UtcNow; });
            }
            return new MembershipUser(Membership.Provider.Name, user.UserName, user.Id, user.Email, null, null,
                user.IsApproved, user.IsLockedOut, user.CreateDate, user.LastLoginDate.GetValueOrDefault(),
                user.LastActivityDate.GetValueOrDefault(), user.LastPasswordChangedDate.GetValueOrDefault(),
                user.LastLockoutDate.GetValueOrDefault());
        }

        /// <summary>
        ///     Возвращает имя пользователя, связанное с указанным адресом электронной почты.
        /// </summary>
        /// <returns>
        ///     Имя пользователя, связанное с указанным адресом электронной почты.Если совпадения не найдены, то возвращается
        ///     значение null.
        /// </returns>
        /// <param name="email">Адрес электронной почты для поиска. </param>
        public override string GetUserNameByEmail(string email)
        {
            var result = string.Empty;
            var dbuser = _usersService.GetByEmail(email);
            if (dbuser != null)
            {
                result = dbuser.UserName;
            }
            return result;
        }

        /// <summary>
        ///     Обновляет информацию о пользователе в источнике данных.
        /// </summary>
        /// <param name="user">
        ///     Объект <see cref="T:System.Web.Security.MembershipUser" />, который представляет пользователя для
        ///     обновления и обновленную информацию для пользователя.
        /// </param>
        public override void UpdateUser(MembershipUser user)
        {
            try
            {
                _usersService.Update(user.UserName, u =>
                {
                    u.Email = user.Email;
                    u.Comment = user.Comment;
                    u.IsApproved = user.IsApproved;
                });
            }
            catch (Exception ex)
            {
                _loggerService.Error("Не удалось обновить пользователя", ex);
            }
        }

        /// <summary>
        ///     Обрабатывает запрос на обновление пароля для пользователя членства.
        /// </summary>
        /// <returns>
        ///     Значение true, если пароль был успешно обновлен; в противном случае — false.
        /// </returns>
        /// <param name="username">Пользователь, для которого обновляется пароль. </param>
        /// <param name="oldPassword">Текущий пароль заданного пользователя. </param>
        /// <param name="newPassword">Новый пароль заданного пользователя. </param>
        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            if (!ValidateUser(username, oldPassword)) return false;

            var args = new ValidatePasswordEventArgs(username, newPassword, true);

            OnValidatingPassword(args);

            if (args.Cancel)
                if (args.FailureInformation != null) throw args.FailureInformation;
                else
                    throw new MembershipPasswordException(
                        "Change password canceled due to new password validation failure.");

            try
            {
                var user = _usersService.Update(username, u =>
                {
                    u.Password = EncodePassword(newPassword);
                    u.LastPasswordChangedDate = DateTime.UtcNow;
                });
                if (user != null)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                _loggerService.Error("Проблема смены пароля", ex);
            }

            return false;
        }

        /// <summary>
        ///     Заменяет пароль пользователя на новый, автоматически сгенерированный пароль.
        /// </summary>
        /// <returns>
        ///     Новый пароль заданного пользователя.
        /// </returns>
        /// <param name="username">Пользователь, для которого сбрасывается пароль. </param>
        /// <param name="answer">Контрольный ответ для пароля заданного пользователя. </param>
        public override string ResetPassword(string username, string answer)
        {
            if (!EnablePasswordReset)
            {
                throw new NotSupportedException("Password reset is not enabled.");
            }

            if (answer == null && RequiresQuestionAndAnswer)
            {
                UpdateFailureCount(username, false);

                throw new ProviderException("Password answer required for password reset.");
            }

            var newPassword = Membership.GeneratePassword(_minRequiredPasswordLength,
                MinRequiredNonAlphanumericCharacters);

            var args = new ValidatePasswordEventArgs(username, newPassword, true);

            OnValidatingPassword(args);

            if (args.Cancel)
                if (args.FailureInformation != null)
                    throw args.FailureInformation;
                else
                    throw new MembershipPasswordException("Reset password canceled due to password validation failure.");

            var user = _usersService.Update(username, u =>
            {
                u.Password = EncodePassword(newPassword);
                u.LastPasswordChangedDate = DateTime.UtcNow;
                u.PasswordRecoveryToken = null;
                u.PasswordRecoveryTokenExpirationDate = null;
            });
            if (user != null)
            {
                return newPassword;
            }

            throw new MembershipPasswordException("User not found, or user is locked out. Password not Reset.");
        }

        /// <summary>
        ///     Снимает блокировку так, что пользователь членства может быть проверен.
        /// </summary>
        /// <returns>
        ///     Значение true, если пользователь членства успешно разблокирован; в противном случае — значение false.
        /// </returns>
        /// <param name="userName">Пользователь членства, для которого нужно снять блокировку.</param>
        public override bool UnlockUser(string userName)
        {
            try
            {
                var user = _usersService.Update(userName, u =>
                {
                    u.IsLockedOut = false;
                    u.LastLockoutDate = DateTime.UtcNow;
                });
                if (user != null)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                _loggerService.Error("Проблема смены пароля", ex);
            }

            return false;
        }

        /// <summary>
        ///     Удаляет пользователя из источника данных членства.
        /// </summary>
        /// <returns>
        ///     Значение true, если пользователь успешно удален; в противном случае — значение false.
        /// </returns>
        /// <param name="username">Имя пользователя для удаления.</param>
        /// <param name="deleteAllRelatedData">
        ///     Значение true, чтобы удалить связанные с пользователем данные из базы данных;
        ///     значение false, чтобы оставить связанные с пользователем данные в базе данных.
        /// </param>
        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            try
            {
                return _usersService.Delete(username);
            }
            catch (Exception ex)
            {
                _loggerService.Error("Проблема удаления пользователя", ex);
            }

            return false;
        }

        #endregion

        #region Not Supported

        /// <summary>
        ///     Обрабатывает запрос на обновление контрольного вопроса и ответа при вводе пароля для пользователя членства.
        /// </summary>
        /// <returns>
        ///     Значение true, если контрольный вопрос и ответ для пароля успешно обновлены; в противном случае — значение false.
        /// </returns>
        /// <param name="username">Пользователь, для которого изменяется контрольный вопрос и ответ. </param>
        /// <param name="password">Пароль заданного пользователя. </param>
        /// <param name="newPasswordQuestion">Новый контрольный вопрос заданного пользователя. </param>
        /// <param name="newPasswordAnswer">Новый контрольный ответ заданного пользователя. </param>
        public override bool ChangePasswordQuestionAndAnswer(string username, string password,
            string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Возвращает из источника данных пароль для указанного имени пользователя.
        /// </summary>
        /// <returns>
        ///     Пароль для заданного имени пользователя.
        /// </returns>
        /// <param name="username">Пользователь, для которого извлекается пароль. </param>
        /// <param name="answer">Контрольный ответ для пароля пользователя. </param>
        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        ///     Возвращает коллекцию всех пользователей в источнике данных на страницах данных.
        /// </summary>
        /// <returns>
        ///     Коллекция <see cref="T:System.Web.Security.MembershipUserCollection" />, содержащая страницу, которая включает
        ///     определяемое параметром <paramref name="pageSize" /> количество объектов
        ///     <see cref="T:System.Web.Security.MembershipUser" />, начиная со страницы, заданной параметром
        ///     <paramref name="pageIndex" />.
        /// </returns>
        /// <param name="pageIndex">
        ///     Индекс возвращаемой страницы результатов.Параметр <paramref name="pageIndex" /> отсчитывается с
        ///     нуля.
        /// </param>
        /// <param name="pageSize">Размер возвращаемой страницы результатов.</param>
        /// <param name="totalRecords">Общее количество пользователей, для которых выявлены совпадения.</param>
        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Получает количество пользователей, осуществляющих текущий доступ к приложению.
        /// </summary>
        /// <returns>
        ///     Количество пользователей, осуществляющих текущий доступ к приложению.
        /// </returns>
        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Возвращает коллекцию пользователей членства, у которых часть имени совпадает с указанным значением.
        /// </summary>
        /// <returns>
        ///     Коллекция <see cref="T:System.Web.Security.MembershipUserCollection" />, содержащая страницу, которая включает
        ///     определяемое параметром <paramref name="pageSize" /> количество объектов
        ///     <see cref="T:System.Web.Security.MembershipUser" />, начиная со страницы, заданной параметром
        ///     <paramref name="pageIndex" />.
        /// </returns>
        /// <param name="usernameToMatch">Имя пользователя для поиска.</param>
        /// <param name="pageIndex">
        ///     Индекс возвращаемой страницы результатов.Параметр <paramref name="pageIndex" /> отсчитывается с
        ///     нуля.
        /// </param>
        /// <param name="pageSize">Размер возвращаемой страницы результатов.</param>
        /// <param name="totalRecords">Общее количество пользователей, для которых выявлены совпадения.</param>
        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize,
            out int totalRecords)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Возвращает коллекцию пользователей членства, адрес электронной почты которых содержит часть, совпадающую с
        ///     указанным значением.
        /// </summary>
        /// <returns>
        ///     Коллекция <see cref="T:System.Web.Security.MembershipUserCollection" />, содержащая страницу, которая включает
        ///     определяемое параметром <paramref name="pageSize" /> количество объектов
        ///     <see cref="T:System.Web.Security.MembershipUser" />, начиная со страницы, заданной параметром
        ///     <paramref name="pageIndex" />.
        /// </returns>
        /// <param name="emailToMatch">Адрес электронной почты для поиска.</param>
        /// <param name="pageIndex">
        ///     Индекс возвращаемой страницы результатов.Параметр <paramref name="pageIndex" /> отсчитывается с
        ///     нуля.
        /// </param>
        /// <param name="pageSize">Размер возвращаемой страницы результатов.</param>
        /// <param name="totalRecords">Общее количество пользователей, для которых выявлены совпадения.</param>
        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize,
            out int totalRecords)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}