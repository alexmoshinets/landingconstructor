﻿namespace WebSite.Framework
{
    public class Constants
    {
        public const string NotificationsSuccess = "NotificationsSuccess";
        public const string NotificationsError = "NotificationsError";
    }
}