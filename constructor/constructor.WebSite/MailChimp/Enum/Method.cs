﻿namespace WebSite.MailChimp.Enum
{
    public enum Method
    {
        Get,
        Post,
        Put,
        Patch,
        Delete
    }
}
