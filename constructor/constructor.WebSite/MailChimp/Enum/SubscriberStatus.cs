﻿namespace WebSite.MailChimp.Enum
{
    public enum SubscriberStatus
    {
        subscribed,
        unsubscribed,
        cleaned,
        pending
    }
}
