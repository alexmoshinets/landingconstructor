﻿namespace WebSite.MailChimp.Enum
{
    public enum CampaignType
    {
        regular,
        plaintext,
        absplit,
        rss,
        variate
    }
}
