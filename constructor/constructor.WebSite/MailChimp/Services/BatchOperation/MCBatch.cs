﻿using System.Threading.Tasks;
using WebSite.MailChimp.Domain.BatchOperation;
using WebSite.MailChimp.Enum;
using WebSite.MailChimp.Helper;

namespace WebSite.MailChimp.Services.BatchOperation
{
    internal class MCBatch
    {
        /// <summary>
        /// Start a batch operation
        /// <param name="bundle"></param>
        /// </summary>
        internal async Task<dynamic> PostBatchOperationAsync(RootBatch bundle)
        {
            string endpoint = Authenticate.EndPoint(TargetTypes.batches, SubTargetType.not_applicable, SubTargetType.not_applicable);

            return await BaseOperation.PostAsync<RootBatch>(endpoint, bundle);
        }

        /// <summary>
        /// Get the status of a batch operation request
        /// <param name="batchId">The unique id for the batch operation</param>
        /// </summary>
       internal async Task<RootBatch> GetBatchReportById(string batchId)
       {
           string endpoint = Authenticate.EndPoint(TargetTypes.batches, SubTargetType.not_applicable, SubTargetType.not_applicable, batchId);

           return await BaseOperation.GetAsync<RootBatch>(endpoint);
       }




        /// <summary>
        /// Get a list of batch requests
        /// </summary>
        //internal async Task<T> GetAllBatchReport()
        //{
        //    string endpoint = Authenticate.EndPoint(TargetTypes.batches, SubTargetType.not_applicable, SubTargetType.not_applicable);

        //    return await BaseOperation.GetAsync<>(endpoint);
        //}
    }
}
