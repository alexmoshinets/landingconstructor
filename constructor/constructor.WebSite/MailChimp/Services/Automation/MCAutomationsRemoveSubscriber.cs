﻿using System.Threading.Tasks;
using WebSite.MailChimp.Domain.Automations;
using WebSite.MailChimp.Enum;
using WebSite.MailChimp.Helper;

namespace WebSite.MailChimp.Services.Automation
{
    // ========================================================================
    // AUTHOR      : Shahriar Hossain
    // PURPOSE     : Remove subscribers from an Automation workflow.
    // ========================================================================
    internal class MCAutomationsRemoveSubscriber
    {
        /// <summary>
        /// View all subscribers removed from a workflow
        /// <param name="workflow_id">Unique id for the Automation workflow</param>
        /// </summary>
        internal async Task<RemovedSubscriber> GetRemovedSubscriberListAsync(string workflow_id)
        {
            string endpoint = Authenticate.EndPoint(TargetTypes.automations, SubTargetType.removed_subscribers, SubTargetType.not_applicable, workflow_id);

            return await BaseOperation.GetAsync<RemovedSubscriber>(endpoint);
        }

    }
}
