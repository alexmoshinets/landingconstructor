﻿using System;
using System.Threading.Tasks;
using WebSite.MailChimp.Enum;
using WebSite.MailChimp.Helper;
using WebSite.MailChimp.Domain.Campaigns;

namespace WebSite.MailChimp.Services.Campaigns
{
    // ==============================================
    // AUTHOR      : Shahriar Hossain
    // PURPOSE     : Schedule Campaign blast time
    // ==============================================

    internal class MCCampaignSchedule
    {
        /// <summary>
        /// Schedule Campaign Blast time
        /// <param name="campaignId">Unique id for the campaign</param>
        /// <param name="dateTime">Schedule time in UTC format</param>
        /// </summary>
        internal async Task<dynamic> ScheduleCampaignAsync(string campaignId, DateTime dateTime)
        {
            string endpoint = Authenticate.LegacyEndPoint(TargetTypes.campaigns, SubTargetType.schedule);

            Schedule content = new Schedule()
            {
                apikey = Authenticate.FeatchApiKey(),
                cid = campaignId,
                schedule_time = dateTime               
            };

            return await BaseOperation.PostAsync<Schedule>(endpoint, content);
        }
    }
}
