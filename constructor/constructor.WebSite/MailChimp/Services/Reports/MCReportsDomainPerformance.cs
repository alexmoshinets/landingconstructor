﻿using System.Threading.Tasks;
using WebSite.MailChimp.Domain.Reports;
using WebSite.MailChimp.Enum;
using WebSite.MailChimp.Helper;

namespace WebSite.MailChimp.Services.Reports
{
    // ============================================================================
    // AUTHOR      : Shahriar Hossain
    // PURPOSE     : Get statistics for the top-performing domains from a campaign. 
    // ============================================================================

    internal class MCReportsDomainPerformance
    {
        /// <summary>
        /// Return statistics for the top-performing domains from a campaign.
        /// <param name="campaignId">Unique id for campaign</param>
        /// </summary>
        internal async Task<DomainPerformance> GetDomainPerformanceAsync(string campaignId)
        {
            string endpoint = Authenticate.EndPoint(TargetTypes.reports, SubTargetType.domain_performance, SubTargetType.not_applicable, campaignId);

            return await BaseOperation.GetAsync<DomainPerformance>(endpoint);
        }
    }
}
