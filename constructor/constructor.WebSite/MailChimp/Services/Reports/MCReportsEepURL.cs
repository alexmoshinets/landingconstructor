﻿using System.Threading.Tasks;
using WebSite.MailChimp.Domain.Reports;
using WebSite.MailChimp.Enum;
using WebSite.MailChimp.Helper;

namespace WebSite.MailChimp.Services.Reports
{
    // ===================================================================================
    // AUTHOR      : Shahriar Hossain
    // PURPOSE     : Get a summary of social activity for the campaign, tracked by EepURL. 
    // ====================================================================================

    internal class MCReportsEepURL
    {
        /// <summary>
        /// Return a summary of social activity for the campaign, tracked by EepURL.
        /// <param name="campaignId">Unique id for campaign</param>
        /// </summary>
        internal async Task<Eepurl> GetEepUrlActivityAsync(string campaignId)
        {
            string endpoint = Authenticate.EndPoint(TargetTypes.reports, SubTargetType.eepurl, SubTargetType.not_applicable, campaignId);

            return await BaseOperation.GetAsync<Eepurl>(endpoint);
        }
    }
}
