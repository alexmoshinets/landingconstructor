﻿using System.Collections.Generic;
using WebSite.MailChimp.Domain.Reports;

namespace WebSite.MailChimp.Domain.Conversations
{
    public class RootConversation
    {
        public List<Conversation> conversations { get; set; }
        public int total_items { get; set; }
        public List<Link2> _links { get; set; }
    }
}
