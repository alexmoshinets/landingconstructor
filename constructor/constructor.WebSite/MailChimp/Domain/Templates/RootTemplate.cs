﻿using System.Collections.Generic;
using WebSite.MailChimp.Domain.Reports;

namespace WebSite.MailChimp.Domain.Templates
{
    public class RootTemplate
    {
            public List<Template> templates { get; set; }
            public List<Link2> _links { get; set; }
            public int total_items { get; set; }
    }
}
