﻿using System.Collections.Generic;
using WebSite.MailChimp.Domain.Reports;

namespace WebSite.MailChimp.Domain.CampaignFolder
{
    public class CampaignFolder
    {
            public string id { get; set; }
            public string name { get; set; }
            public int count { get; set; }
            public List<Link> _links { get; set; }
    }
}
