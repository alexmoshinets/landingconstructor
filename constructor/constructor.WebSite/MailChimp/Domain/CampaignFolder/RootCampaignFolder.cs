﻿using System.Collections.Generic;
using WebSite.MailChimp.Domain.Reports;

namespace WebSite.MailChimp.Domain.CampaignFolder
{
    public class RootCampaignFolder
    {
        public List<CampaignFolder> folders { get; set; }
        public int total_items { get; set; }
        public List<Link2> _links { get; set; }
    }
}
