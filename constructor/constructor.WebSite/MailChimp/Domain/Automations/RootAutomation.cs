﻿using System.Collections.Generic;
using WebSite.MailChimp.Domain.Reports;

namespace WebSite.MailChimp.Domain.Automations
{
    public class RootAutomation
    {
        public List<MCAutomation> automations { get; set; }
        public int total_items { get; set; }
        public List<Link2> _links { get; set; }
    }
}
