﻿using System.Collections.Generic;

namespace WebSite.MailChimp.Domain.Automations
{
    public class Runtime
    {
        public List<string> days { get; set; }
        public Hours hours { get; set; }
    }
}
