﻿using System.Collections.Generic;
using WebSite.MailChimp.Domain.Campaigns;

namespace WebSite.MailChimp.Domain.Lists
{
    public class Options
    {
        public int size { get; set; }
        public string match { get; set; }
        public List<Condition> conditions { get; set; }
    }
}
