﻿using System.Collections.Generic;
using WebSite.MailChimp.Domain.Reports;

namespace WebSite.MailChimp.Domain.Lists
{
    public class RootListsClient
    {
        public List<MCClient> clients { get; set; }
        public string list_id { get; set; }
        public int total_items { get; set; }
        public List<Link> _links { get; set; }
    }
}
