﻿namespace WebSite.MailChimp.Domain.Lists
{
    public class Stats
    {
        public double avg_open_rate { get; set; }
        public double avg_click_rate { get; set; }
    }
}
