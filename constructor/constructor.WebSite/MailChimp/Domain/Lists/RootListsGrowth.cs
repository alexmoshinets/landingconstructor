﻿using System.Collections.Generic;
using WebSite.MailChimp.Domain.Reports;

namespace WebSite.MailChimp.Domain.Lists
{
    public class RootListsGrowth
    {
        public string list_id { get; set; }
        public string month { get; set; }
        public int existing { get; set; }
        public int imports { get; set; }
        public int optins { get; set; }
        public List<Link> _links { get; set; }
    }
}
