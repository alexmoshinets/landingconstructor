﻿namespace WebSite.MailChimp.Domain.Lists
{
    public class MCClient
    {
        public string client { get; set; }
        public int members { get; set; }
        public string list_id { get; set; }
    }
}
