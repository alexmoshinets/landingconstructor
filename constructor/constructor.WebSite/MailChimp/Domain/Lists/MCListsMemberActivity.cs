﻿namespace WebSite.MailChimp.Domain.Lists
{
    public class MCListsMemberActivity
    {
        public string action { get; set; }
        public string timestamp { get; set; }
        public string campaign_id { get; set; }
        public string title { get; set; }
        public string type { get; set; }
    }
}
