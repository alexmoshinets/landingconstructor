﻿using System.Collections.Generic;
using WebSite.MailChimp.Domain.Reports;

namespace WebSite.MailChimp.Domain.Campaigns
{
    public class RootCampaign
    {
        public List<Campaign> campaigns { get; set; }
        public int total_items { get; set; }
        public List<Link2> _links { get; set; }
    }
}
