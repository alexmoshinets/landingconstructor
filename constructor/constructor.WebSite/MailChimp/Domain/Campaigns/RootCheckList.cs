﻿using System.Collections.Generic;
using WebSite.MailChimp.Domain.Reports;

namespace WebSite.MailChimp.Domain.Campaigns
{
    public class RootCheckList
    {
        public bool is_ready { get; set; }
        public List<Item> items { get; set; }
        public List<Link> _links { get; set; }
    }
}
