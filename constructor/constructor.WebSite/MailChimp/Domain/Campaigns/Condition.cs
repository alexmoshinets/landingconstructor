﻿namespace WebSite.MailChimp.Domain.Campaigns
{
    public class Condition
    {
        public string field { get; set; }
        public string op { get; set; }
        public int value { get; set; }
    }
}
