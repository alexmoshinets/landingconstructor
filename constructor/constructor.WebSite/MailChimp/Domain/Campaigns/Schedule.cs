﻿using System;

namespace WebSite.MailChimp.Domain.Campaigns
{
    public class Schedule
    {
        public string apikey { get; set; }
        public string cid { get; set; }
        public DateTime schedule_time { get; set; }
        public DateTime? schedule_time_b { get; set; }
    }    
}
