﻿using System.Collections.Generic;

namespace WebSite.MailChimp.Domain.BatchOperation
{
    public class MultiOperation
    {
        public List<RootBatch> batches { get; set; }
        public int totalItems {get; set; }
    }
}
