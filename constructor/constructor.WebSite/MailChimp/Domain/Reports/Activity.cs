﻿namespace WebSite.MailChimp.Domain.Reports
{
    public class Activity
    {
        public string action { get; set; }
        public string timestamp { get; set; }
        public string ip { get; set; }
    }
}
