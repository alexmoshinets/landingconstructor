﻿namespace WebSite.MailChimp.Domain.Reports
{
    public class Forwards
    {
        public int forwards_count { get; set; }
        public int forwards_opens { get; set; }
    }
}
