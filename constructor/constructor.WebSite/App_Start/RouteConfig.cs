﻿using System.Web.Mvc;
using System.Web.Routing;

namespace WebSite
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            //routes.MapRoute(
            //    "default",
            //    "{controller}/{action}/{id}",
            //    new { action = "Index", id = UrlParameter.Optional },
            //    new[] { "WebSite.Areas.Cabinet.Controllers" }
            //);

            routes.MapRoute("Home", "", new { controller = "Home", action = "Index" },
                new[] { "WebSite.Controllers" }
                );

            routes.MapRoute("Templates", "Templates", new { controller = "Home", action = "Templates" },
                new[] { "WebSite.Controllers" }
                );

            routes.MapRoute("Prices", "Prices", new { controller = "Home", action = "Prices" },
                new[] { "WebSite.Controllers" }
                );

            routes.MapRoute("Blog", "Blog", new { controller = "Home", action = "Blog" },
                new[] { "WebSite.Controllers" }
                );

            routes.MapRoute("Contact", "Contact", new { controller = "Home", action = "Contact" },
                new[] { "WebSite.Controllers" }
                );

            routes.MapRoute("Opportunities", "Opportunities", new { controller = "Home", action = "Opportunities" },
                new[] { "WebSite.Controllers" }
                );


            //routes.MapRoute("Register", "Cabinet/Register", new { Areas = "Cabinet", controller = "Account", action = "Register" },
            //    new[] { "WebSite.Controllers" }
            //    );

            //routes.MapRoute("LogIn", "Cabinet/LogIn", new {Areas="Cabinet",  controller = "Account", action = "LogIn" },
            //    new[] { "WebSite.Controllers" }
            //    );

            //routes.MapRoute("SendForm", "SendForm", new {Areas="Cabinet",  controller = "Common", action = "SendForm" },
            //    new[] { "WebSite.Controllers" }
            //    );


            routes.MapRoute("Page", "{stringKey}", new { controller = "Page", action = "Index" },
                new[] { "WebSite.Controllers" }
                );
        }
    }
}