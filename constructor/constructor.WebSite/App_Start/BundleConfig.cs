﻿using System.Web.Optimization;

namespace WebSite
{
    public class BundleConfig
    {
        // Additional info http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(
                new ScriptBundle("~/bundles/js")
                    .Include("~/assets/plugins/jquery/jquery-{version}.js")
                    .Include("~/assets/plugins/bootstrap/js/bootstrap.js")
                    .Include("~/assets/plugins/jquery/jquery-migrate-1.2.1.min.js")
                    .Include("~/assets/plugins/back-to-top.js")
                    .Include("~/assets/plugins/flexslider/jquery.flexslider-min.js")
                    .Include("~/assets/plugins/parallax-slider/js/jquery.cslider.js")
                    .Include("~/assets/app/app.js")
                    .Include("~/assets/app/pages/index.js")
            );

            bundles.Add(
                new ScriptBundle("~/bundles/modernizr")
                    .Include("~/assets/plugins/modernizr-{version}.js")
            );

            bundles.Add(
                new StyleBundle("~/assets/distrib/css/css")
                    .Include("~/assets/distrib/css/bootstrap.css")
                    .Include("~/assets/distrib/css/line-icons.css")
                    .Include("~/assets/distrib/css/font-awesome.css")
                    .Include("~/assets/distrib/css/style.css")
                    .Include("~/assets/distrib/css/app.css")
                    .Include("~/assets/distrib/css/ie8.css")
                    .Include("~/assets/distrib/css/plugins.css")
                    .Include("~/assets/distrib/css/default.css")
                    .Include("~/assets/distrib/css/plugins/animate.css")
                    .Include("~/assets/distrib/css/plugins/box-shadows.css")
                    .Include("~/assets/distrib/css/flexslider.css")
                    .Include("~/assets/distrib/css/parallax-slider.css")
                    .Include("~/assets/distrib/css/custom.css")
            );

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/angularjs").Include(
                        "~/Scripts/angular.js",
                        "~/Scripts/angular-route.js",
                        "~/Scripts/angular-cookies.js"));

            bundles.Add(new ScriptBundle("~/bundles/lodash").Include(
                        "~/Scripts/lodash.js"));

            bundles.Add(new ScriptBundle("~/bundles/mousetrap").Include(
                        "~/Scripts/mousetrap.js"));

            bundles.Add(new ScriptBundle("~/bundles/moment").Include(
                        "~/Scripts/moment.js"));

            bundles.Add(new ScriptBundle("~/bundles/appScripts").Include(
                        "~/Scripts/AppScripts/Editor.js",
                        "~/Scripts/AppScripts/ru.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/Css/framework.css",
                      "~/Content/Css/editor.css",
                      "~/Content/Css/jquery.fileupload-ui.css",
                      "~/Content/Css/stuff.css",
                      "~/Content/Css/style.css"));

            // Additional info http://go.microsoft.com/fwlink/?LinkId=301862
            BundleTable.EnableOptimizations = true;
        }
    }
}