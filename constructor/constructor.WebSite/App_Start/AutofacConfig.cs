﻿using System.Reflection;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Interfaces.Account;
using Interfaces.Common;
using Interfaces.Content;
using Interfaces.Navigation;
using Services.Account;
using Services.Common;
using Services.Content;
using Services.Navigation;
using WebSite.Framework;

namespace WebSite
{
    public class AutofacConfig
    {
        public static void SetupContainer()
        {
            // Setup IoC container
            var builder = new ContainerBuilder();

            builder.RegisterControllers(Assembly.GetExecutingAssembly());

            //Инициализация классов HttpContextBase, HttpRequestBase, HttpResponseBase, HttpServerUtilityBase, HttpSessionStateBase,
            //HttpApplicationStateBase, HttpBrowserCapabilitiesBase, HttpCachePolicyBase, VirtualPathProvider происходит регистрацией встроенного модуля autofac.
            builder.RegisterModule(new AutofacWebTypesModule());

            // регистрация контекста
            builder.RegisterType<WorkContext>().InstancePerHttpRequest();

            // регистрация сервисов
            builder.RegisterType<LoggerService>().As<ILoggerService>().InstancePerHttpRequest();
            builder.RegisterType<MessageService>().As<IMessageService>().InstancePerHttpRequest();
            builder.RegisterType<ImageResizeService>().As<IImageResizeService>().InstancePerHttpRequest();

            builder.RegisterType<UserService>().As<IUserService>().InstancePerHttpRequest();
            builder.RegisterType<RoleService>().As<IRoleService>().InstancePerHttpRequest();

            builder.RegisterType<SettingService>().As<ISettingService>().InstancePerHttpRequest();

            builder.RegisterType<MenuService>().As<IMenuService>().InstancePerHttpRequest();
            builder.RegisterType<MenuItemService>().As<IMenuItemService>().InstancePerHttpRequest();

            builder.RegisterType<PageService>().As<IPageService>().InstancePerHttpRequest();

            builder.RegisterType<ArticleCategoryService>().As<IArticleCategoryService>().InstancePerHttpRequest();
            builder.RegisterType<ArticleService>().As<IArticleService>().InstancePerHttpRequest();

            builder.RegisterType<BlockListService>().As<IBlockListService>().InstancePerHttpRequest();
            builder.RegisterType<BlockService>().As<IBlockService>().InstancePerHttpRequest();

            builder.RegisterType<SliderService>().As<ISliderService>().InstancePerHttpRequest();

            builder.RegisterType<SectionsService>().As<ISectionsService>().InstancePerHttpRequest();
            builder.RegisterType<SectionsCategoryService>().As<ISectionsCategoryService>().InstancePerHttpRequest();
           // builder.RegisterType<ProducerService>().As<IProducerService>().InstancePerHttpRequest();

            //builder.RegisterType<ProductService>().As<IProductService>().InstancePerHttpRequest();
            builder.RegisterType<ProductService>().As<IProductService>().InstancePerHttpRequest();
            builder.RegisterType<LocalizationService>().As<ILocalizationService>().InstancePerHttpRequest();


            builder.RegisterType<UnitsService>().As<IUnitsService>().InstancePerHttpRequest();
            builder.RegisterType<CategoryService>().As<ICategoryService>().InstancePerHttpRequest();
            builder.RegisterType<LandingService>().As<ILandingService>().InstancePerHttpRequest();
            builder.RegisterType<LandingIntegrationService>().As<ILandingIntegrationService>().InstancePerRequest();

            builder.RegisterType<LandingSettingsService>().As<ILandingSettingsService>().InstancePerRequest();
            builder.RegisterType<WidgetService>().As<IWidgetService>().InstancePerRequest();

            // инъекция в фильтры (классы производные от ActionFilterAttribute)
            builder.RegisterFilterProvider();

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}