﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebSite.Areas.Admin.Models.Account
{
    public class ResetViewModel
    {
        [Required(ErrorMessage = "Поле {0} обязательно для заполнения")]
        [DataType(DataType.Password)]
        [Display(Name = "Новый пароль", Description = "Введите новый пароль")]
        public string Password { get; set; }

        public Guid Token { get; set; }
    }
}