﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web;
using DomainModel.Content;
using System.Web.Mvc;

namespace WebSite.Areas.Admin.Models.Content
{
    public class ProducerViewModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Заголовок")]
        public string Title { get; set; }


        [Required]
        [StringLength(150, MinimumLength = 10)]
        [Display(Name = "Краткое описание")]

        public string Description { get; set; }

        [Required]
        [AllowHtml]
        [Display(Name = "Полное описание")]
        public string FullDescription { get; set; }

        public string Image { get; set; }

        [Display(Name = "Загрузить новый", Description = "")]
        public HttpPostedFileBase File { get; set; }

        [Display(Name = "Ссылка")]
        public string Url { get; set; }

        public DateTime Created { get; set; }
        public DateTime? Updated { get; set; }

        public static ProducerViewModel FromDomainModel(Producer producer)
        {
            return new ProducerViewModel
            {
                Id = producer.Id,
                Title = producer.Title,
                Description = producer.Description,
                FullDescription = producer.FullDescription,
                Image = producer.Image,
                Url = producer.Url,
                Created = producer.Created,
                Updated = producer.Updated
            };
        }
    }
}