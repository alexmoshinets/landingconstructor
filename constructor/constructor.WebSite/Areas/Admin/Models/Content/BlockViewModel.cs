﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using DomainModel.Content;

namespace WebSite.Areas.Admin.Models.Content
{
    public class BlockViewModel
    {
        public static BlockViewModel FromDomainModel(Block block)
        {
            return new BlockViewModel
            {
                Id = block.Id,
                Title = block.Title,
                Description = block.Description,

                IsHidden = block.IsHidden,
                IsContent = block.IsContent,
                IsStatic = block.IsStatic,

                BlockListId = block.BlockListId,
                Created = block.Created,
                Updated = block.Updated
            };
        }

        public int Id { get; set; }

        [Required]
        [Display(Name = "Заголовок")]
        public string Title { get; set; }

        [Required]
        [AllowHtml]
        [Display(Name = "Контент")]
        public string Description { get; set; }

        [Display(Name = "Скрыть?")]
        public bool IsHidden { get; set; }

        [Display(Name = "Только контент")]
        public bool IsContent { get; set; }

        public bool IsStatic { get; set; }

        [Display(Name = "Панель блоков")]
        public int? BlockListId { get; set; }

        public DateTime Created { get; set; }
        public DateTime? Updated { get; set; }

        public IList<SelectListItem> BlockListAll { get; set; }
    }
}