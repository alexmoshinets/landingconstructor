﻿using System.Collections.Generic;

namespace WebSite.Areas.Admin.Models.Content
{
    public class PageListViewModel
    {
        public PageListViewModel()
        {
            Sections = new List<PageViewModel>();
            Pages = new List<PageViewModel>();
        }

        public IList<PageViewModel> Sections { get; set; }
        public IList<PageViewModel> Pages { get; set; }
    }
}