﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using DomainModel.Content;

namespace WebSite.Areas.Admin.Models.Content
{
    public class ArticleCategoryViewModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Ссылка")]
        public string StringKey { get; set; }

        [Required]
        [Display(Name = "Название")]
        public string Name { get; set; }

        [Display(Name = "Meta Title")]
        public string MetaTitle { get; set; }

        [Display(Name = "Meta Keywords")]
        public string MetaKeywords { get; set; }

        [Display(Name = "Meta Description")]
        public string MetaDescription { get; set; }

        public DateTime Created { get; set; }
        public DateTime? Updated { get; set; }

        public IList<ArticleViewModel> Articles { get; set; }

        public static ArticleCategoryViewModel FromDomainModel(ArticleCategory article)
        {
            var result = new ArticleCategoryViewModel
            {
                Id = article.Id,
                StringKey = article.StringKey,
                Name = article.Name,
                MetaTitle = article.MetaTitle,
                MetaKeywords = article.MetaKeywords,
                MetaDescription = article.MetaDescription,
                Created = article.Created,
                Updated = article.Updated
            };
            if (article.Articles != null)
            {
                result.Articles = article.Articles.Select(ArticleViewModel.FromDomainModel).ToList();
            }

            return result;
        }
    }
}