﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DomainModel.Content;
using OrangeJetpack.Localization;

namespace WebSite.Areas.Admin.Models.Content
{
    public class CompanyNameViewModel
    {
        [Display(Name="Название организации")]
        public String Name { get; set; }
        [Display(Name = "Мобильный телефон")]
        public string Tel { get; set; }
        [Display(Name = "Рабочий телефон")]
        public string WorkTel { get; set; }
        [Display(Name = "Адрес")]
        public string Address { get; set; }
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Display(Name = "Сайт")]
        public string WebSite { get; set; }

    }
     
}