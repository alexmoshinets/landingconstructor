﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using DomainModel.Content;
using System.Web.Mvc;

namespace WebSite.Areas.Admin.Models.Content
{
    public class PageProductsViewModel
    {
        [Display(Name = "Сортировка по разделу:")]
        public int Section { get; set; }
        public IList<ProductViewModel> ListProducts { get; set; }
        public IList<SelectListItem> SectionAll { get; set; }
    }
}