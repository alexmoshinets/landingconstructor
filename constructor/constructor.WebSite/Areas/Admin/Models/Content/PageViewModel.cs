﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using DomainModel.Content;

namespace WebSite.Areas.Admin.Models.Content
{
    public class PageViewModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Название")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Ссылка")]
        public string StringKey { get; set; }

        [AllowHtml]
        [Display(Name = "Контент")]
        public string Content { get; set; }

        [AllowHtml]
        [Display(Name = "Контент снизу страницы")]
        public string BottomContent { get; set; }
        public bool IsSection { get; set; }

        public DateTime Created { get; set; }

        public DateTime? Updated { get; set; }

        [Display(Name = "Meta Title")]
        public string MetaTitle { get; set; }

        [Display(Name = "Meta Keywords")]
        public string MetaKeywords { get; set; }

        [Display(Name = "Meta Description")]
        public string MetaDescription { get; set; }

        public static PageViewModel FromDomainModel(Page page)
        {
            var result = new PageViewModel
            {
                Id = page.Id,
                Name = page.Name,
                StringKey = page.StringKey,
                Content = page.Content,
                BottomContent = page.BottomContent,
                IsSection = page.IsSection,
                Created = page.Created,
                Updated = page.Updated,
                MetaTitle = page.MetaTitle,
                MetaKeywords = page.MetaKeywords,
                MetaDescription = page.MetaDescription
            };

            return result;
        }
    }
}