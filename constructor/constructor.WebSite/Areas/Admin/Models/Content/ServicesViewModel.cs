﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using DomainModel.Content;

namespace WebSite.Areas.Admin.Models.Content
{
    public class ServicesViewModel
    {
        public int Id { get; set; }
        [Display(Name="Скрыть?")]
        public bool IsHidden { get; set; }
        [Display(Name = "Ссылка")]
        public string StringKey { get; set; }
        [Display(Name = "Название")]
        public string Title { get; set; }
         [Display(Name = "Краткое описание")]
        public string Description { get; set; }
        public string MetaTitle { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeywords { get; set; }
        [Display(Name = "Описание")]
        public string FullDescription { get; set; }
        public string Image { get; set; }
        [Display(Name = "Стоимость")]
        public double Price { get; set; }
        [Display(Name = "Наличие")]
        public bool IsPresence { get; set; }

        public DateTime Created { get; set; }
        public DateTime? Updated { get; set; }
        [Display(Name = "Картинка")]
        public HttpPostedFileBase File { get; set; }

        [Required]
        [Display(Name = "Категория")]
        public int CategoryId { get; set; }

        public CategoryServicesViewModel Category { get; set; }
        public IList<SelectListItem> CategoryAll { get; set; }

        public static ServicesViewModel FromDomainModel(DomainModel.Content.Servise services)
        {
            return new ServicesViewModel
            {
                Id = services.Id,
                IsHidden = services.IsHidden,
                StringKey = services.StringKey,
                Title = services.Title,
                Description = services.Description,
                MetaTitle = services.MetaTitle,
                MetaDescription = services.MetaDescription,
                MetaKeywords = services.MetaKeywords,
                FullDescription = services.FullDescription,
                Image = services.Image,
                Price = services.Price,
                IsPresence = services.IsPresence,
                Created = services.Created,
                Updated = services.Updated
            };
        }
    }
}