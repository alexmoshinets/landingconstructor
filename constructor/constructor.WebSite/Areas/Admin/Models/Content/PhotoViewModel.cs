﻿using DomainModel.Content;

namespace WebSite.Areas.Admin.Models.Content
{
    public class PhotoViewModel
    {
        public int Id { get; set; }

        public string MediumUrl { get; set; }
        public string OriginalUrl { get; set; }

        public static PhotoViewModel FromDomainModel(Photo photo)
        {
            return new PhotoViewModel
            {
                Id = photo.Id,
                MediumUrl = photo.MediumUrl,
                OriginalUrl = photo.OriginalUrl
            };
        }
    }
}