﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web;
using DomainModel.Content;
using System.Web.Mvc;

namespace WebSite.Areas.Admin.Models.Content
{
    public class SliderViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Скрыть?")]
        public bool IsHidden { get; set; }

        //[Required]
        [AllowHtml]
        [Display(Name = "Заголовок")]
        public string Title { get; set; }

        public string Image { get; set; }

        [Display(Name = "Загрузить новый", Description = "")]
        public HttpPostedFileBase File { get; set; }

        //[Required]
        [Display(Name = "Ссылка")]
        public string Url { get; set; }

        public DateTime Created { get; set; }
        public DateTime? Updated { get; set; }

        public static SliderViewModel FromDomainModel(Slider slider)
        {
            return new SliderViewModel
            {
                Id = slider.Id,
                IsHidden = slider.IsHidden,
                Title = slider.Title,
                Image = slider.Image,
                Url = slider.Url,
                Created = slider.Created,
                Updated = slider.Updated
            };
        }
    }
}