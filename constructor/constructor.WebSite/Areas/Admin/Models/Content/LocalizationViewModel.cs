﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DomainModel.Content;

namespace WebSite.Areas.Admin.Models.Content
{
    public class LocalizationViewModel
    {
        public int LocalizationId { get; set; }

        [Required]
        [Display (Name="Ключ")]
        public string StringKey { get; set; }

        [Display(Name = "Перевод")]
        public string Name { get; set; }

        public static LocalizationViewModel FromDomainModel(Localization localization)
        {
            var result = new LocalizationViewModel
            {
                LocalizationId = localization.LocalizationId,
                Name = localization.Name,
                StringKey = localization.StringKey
            };
            return result;
        }

    }
}