﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DomainModel.Content;
using System.Web.Mvc;

namespace WebSite.Areas.Admin.Models.Content
{
    public class SaleTechnikViewModel
    {
        public int Id { get; set; }
        [Display(Name = "Скрыть?")]
        public bool IsHidden { get; set; }

        [Display (Name = "Ссылка")]
        public string StringKey { get; set; }

        [Required]
        [Display(Name = "Наименование техники")]
        public string Name { get; set; }

        [Required]
        [AllowHtml]
        [Display(Name = "Описание техники")]
        public string Description { get; set; }

        public string Image { get; set; }
                [Display(Name = "Изображение")]

        public HttpPostedFileBase File { get; set; }

        [Display(Name = "Стоимость")]
        public int Price { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Updated { get; set; }

        public static SaleTechnikViewModel FromDomainModel(SaleTechniks technik)
        {
            var result = new SaleTechnikViewModel
            {
                Id = technik.Id,
                
                Name = technik.Name,
                StringKey = technik.StringKey,
                Description = technik.Description,
                Image = technik.Image,
                Price = technik.Price,
                Created = technik.Created,
                Updated = technik.Updated                
            };

            return result;
        }
    }
}