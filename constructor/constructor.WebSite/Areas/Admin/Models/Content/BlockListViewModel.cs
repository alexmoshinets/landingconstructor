﻿using System.Collections.Generic;
using System.Linq;
using DomainModel.Content;

namespace WebSite.Areas.Admin.Models.Content
{
    public class BlockListViewModel
    {
        public static BlockListViewModel FromDomainModel(BlockList blockList)
        {
            var result = new BlockListViewModel
            {
                Id = blockList.Id,
                StringKey = blockList.StringKey,
                Name = blockList.Name,
            };

            if (blockList.Blocks != null)
            {
                result.Blocks = blockList.Blocks.Select(BlockViewModel.FromDomainModel).ToList();
            }
            return result;
        }

        public int Id { get; set; }

        public string StringKey { get; set; }

        public string Name { get; set; }

        public IList<BlockViewModel> Blocks { get; set; }
    }
}