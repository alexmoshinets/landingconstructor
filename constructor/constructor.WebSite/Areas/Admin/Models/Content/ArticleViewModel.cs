﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using DomainModel.Content;
using System.Web;

namespace WebSite.Areas.Admin.Models.Content
{
    public class ArticleViewModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Ссылка")]
        public string StringKey { get; set; }

        [Required]
        [Display(Name = "Дата")]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        [Required]
        [Display(Name = "Заголовок")]
        public string Title { get; set; }

        [Required]
        [AllowHtml]
        [Display(Name = "Анонс")]
        public string Annonse { get; set; }

        [Required]
        [AllowHtml]
        [Display(Name = "Новость")]
        public string Description { get; set; }

        [Display(Name = "Meta Title")]
        public string MetaTitle { get; set; }

        [Display(Name = "Meta Keywords")]
        public string MetaKeywords { get; set; }

        [Display(Name = "Meta Description")]
        public string MetaDescription { get; set; }

        [Display(Name = "Скрыть?")]
        public bool IsHidden { get; set; }

        public DateTime Created { get; set; }
        public DateTime? Updated { get; set; }

        [Required]
        [Display(Name = "Категория")]
        public int CategoryId { get; set; }

        public ArticleCategoryViewModel Category { get; set; }
        public IList<SelectListItem> CategoryAll { get; set; }

        public string Image { get; set; }

        [Display(Name = "Новое изображение", Description = "")]
        public HttpPostedFileBase File { get; set; }

        public static ArticleViewModel FromDomainModel(Article article)
        {
            var result = new ArticleViewModel
            {
                Id = article.Id,
                StringKey = article.StringKey,
                Date = article.Date,
                Title = article.Title,
                Annonse = article.Annonse,
                Description = article.Description,
                MetaTitle = article.MetaTitle,
                MetaKeywords = article.MetaKeywords,
                MetaDescription = article.MetaDescription,
                IsHidden = article.IsHidden,
                Created = article.Created,
                Updated = article.Updated,
                CategoryId = article.CategoryId,
                Image = article.Image
            };
            //if (article.Category != null)
            //{
            //    result.Category = ArticleCategoryViewModel.FromDomainModel(article.Category);
            //}

            return result;
        }
    }
}