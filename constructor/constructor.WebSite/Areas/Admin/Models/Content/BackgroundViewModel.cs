﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebSite.Areas.Admin.Models.Content
{
    public class BackgroundViewModel
    {
        [Display(Name = "Загрузить новый", Description = "")]
        public HttpPostedFileBase File { get; set; }
    }
}