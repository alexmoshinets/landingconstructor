﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using DomainModel.Content;

namespace WebSite.Areas.Admin.Models.Content
{
    public class PhotoAlbumViewModel
    {
        public PhotoAlbumViewModel()
        {
            Date = DateTime.Now;
            Photos = new List<PhotoViewModel>();
        }

        public int Id { get; set; }

        [Display(Name = "Скрыть?")]
        public bool IsHidden { get; set; }

        [Required]
        [Display(Name = "Ссылка")]
        public string StringKey { get; set; }

        [Required]
        [Display(Name = "Дата")]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        [Required]
        [Display(Name = "Название")]
        public string Name { get; set; }

        [Required]
        [AllowHtml]
        [Display(Name = "Описание")]
        public string Description { get; set; }

        public PhotoViewModel Photo { get; set; }

        [Display(Name = "Meta Title")]
        public string MetaTitle { get; set; }

        [Display(Name = "Meta Keywords")]
        public string MetaKeywords { get; set; }

        [Display(Name = "Meta Description")]
        public string MetaDescription { get; set; }


        public IList<PhotoViewModel> Photos { get; set; }

        public DateTime Created { get; set; }
        public DateTime? Updated { get; set; }

        public string AuthToken { get; set; }

        public static PhotoAlbumViewModel FromDomainModel(PhotoAlbum photoAlbum)
        {
            var result = new PhotoAlbumViewModel
            {
                Id = photoAlbum.Id,
                IsHidden = photoAlbum.IsHidden,
                StringKey = photoAlbum.StringKey,
                Date = photoAlbum.Date,
                Name = photoAlbum.Name,
                Description = photoAlbum.Description,
                MetaTitle = photoAlbum.MetaTitle,
                MetaKeywords = photoAlbum.MetaKeywords,
                MetaDescription = photoAlbum.MetaDescription,
                Created = photoAlbum.Created,
                Updated = photoAlbum.Updated
            };

            if (photoAlbum.Photos != null && photoAlbum.Photos.Count > 0)
            {
                result.Photos = photoAlbum.Photos.Select(PhotoViewModel.FromDomainModel).ToList();
                result.Photo = result.Photos.First();
            }
            else
            {
                result.Photo = new PhotoViewModel { OriginalUrl = "no-image.png", MediumUrl = "no-image.png" };
            }

            return result;
        }
    }
}