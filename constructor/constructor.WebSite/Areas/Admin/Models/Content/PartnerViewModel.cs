﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web;
using DomainModel.Content;
using System.Web.Mvc;

namespace WebSite.Areas.Admin.Models.Content
{
    public class PartnerViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Скрыть?")]
        public bool IsHidden { get; set; }

        [Required]
        [Display(Name = "Заголовок")]
        public string Title { get; set; }

        [Required]
        [AllowHtml]
        [Display(Name = "Описание")]
        public string Description { get; set; }
        public string Image { get; set; }

        [Display(Name = "Загрузить новый", Description = "")]
        public HttpPostedFileBase File { get; set; }

        [Required]
        [Display(Name = "Ссылка")]
        public string Url { get; set; }

        public DateTime Created { get; set; }
        public DateTime? Updated { get; set; }

        public static PartnerViewModel FromDomainModel(Partner partner)
        {
            return new PartnerViewModel
            {
                Id = partner.Id,
                IsHidden = partner.IsHidden,
                Title = partner.Title,
                Description = partner.Description,
                Image = partner.Image,
                Url = partner.Url,
                Created = partner.Created,
                Updated = partner.Updated
            };
        }
    }
}