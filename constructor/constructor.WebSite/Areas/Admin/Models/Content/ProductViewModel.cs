﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using DomainModel.Content;
using System.Web.Mvc;

namespace WebSite.Areas.Admin.Models.Content
{
    public class ProductViewModel
    {
        public ProductViewModel()
        {
            Categories = new List<SelectListItem>();
        }
        public int Id { get; set; }

        [Required]
        [Display(Name = "Название")]
        public string Title { get; set; }

        [AllowHtml]
        [Required]
        [Display(Name = "Описание")]
        public string Description { get; set; }

        [Display(Name = "Ссылка")]
        public string StringKey { get; set; }
        [Display(Name = "Скрыть?")]
        public bool IsHidden { get; set; }

        [Display(Name = "Цена")]
        public double Price { get; set; }

        public string Image { get; set; }

        
        [Display(Name = "Загрузить новый", Description = "")]
        public HttpPostedFileBase File { get; set; }

        public DateTime Created { get; set; }
        public DateTime? Updated { get; set; }

        [Display(Name = "Категория")]
        public string CategoryId { get; set; }

        public CategoryViewModel Category { get; set; }
        public IList<SelectListItem> Categories { get; set; }
        public static ProductViewModel FromDomainModel(Product product)
        {
            var result =  new ProductViewModel
            {
                Id = product.Id,
                IsHidden = product.IsHidden,
                Title = product.Title,
                Description = product.Description,
                Image = product.Image,
                StringKey = product.StringKey,
                CategoryId = product.CategoryId.ToString(),
                Price = product.Price,
                Created = product.Created,
                Updated = product.Updated
            };
            if(product.Category != null){
                result.Category = CategoryViewModel.FromDomainModel(product.Category);
            }
            return result;
        }        
    }
    public class ProductsViewModel
    {
        public ProductsViewModel()
        {
            Categories = new List<SelectListItem>();
        }
        public int Id { get; set; }

        [Required]
        [Display(Name = "Название")]
        public string Title { get; set; }

        [AllowHtml]
        [Required]
        [Display(Name = "Описание")]
        public string Description { get; set; }

        [Display(Name = "Ссылка")]
        public string StringKey { get; set; }
        [Display(Name = "Скрыть?")]
        public bool IsHidden { get; set; }

        [Display(Name = "Цена")]
        public double Price { get; set; }

        public string Image { get; set; }


        [Display(Name = "Загрузить новый", Description = "")]
        public HttpPostedFileBase File { get; set; }

        public DateTime Created { get; set; }
        public DateTime? Updated { get; set; }

        [Display(Name = "Категория")]
        public string CategoryId { get; set; }

        public CategoryViewModel Category { get; set; }
        public IList<SelectListItem> Categories { get; set; }
        public static ProductsViewModel FromDomainModel(Product product)
        {
            var result = new ProductsViewModel
            {
                Id = product.Id,
                IsHidden = product.IsHidden,
                Title = product.Title,
                Description = product.Description,
                Image = product.Image,
                StringKey = product.StringKey,
                CategoryId = product.CategoryId.ToString(),
                Price = product.Price,
                Created = product.Created,
                Updated = product.Updated
            };

            return result;
        }
    }
}