﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DomainModel.Content;
using System.Web.Mvc;

namespace WebSite.Areas.Admin.Models.Content
{
    public class CategoryViewModel
    {
        public CategoryViewModel()
        {
            Products = new List<ProductsViewModel>();
        }
        public int Id { get; set; }

        [Display(Name = "Ссылка")]
        public string StringKey { get; set; }

        [Display(Name = "Скрыть?")]
        public bool IsHidden { get; set; }

        [Required]
        [Display(Name = "Название")]
        public string Title { get; set; }

        [AllowHtml]
        [Required]
        [Display(Name = "Описание")]
        public string Description { get; set; }

        public string Image { get; set; }
        [Display(Name = "Выберите картинку")]
        public HttpPostedFileBase File { get; set; }

        public int? CategoryId { get; set; }
        public CategoryViewModel Parent { get; set; }
        public IList<CategoryViewModel> Childs { get; set; }

        public IList<ProductsViewModel> Products { get; set; }

        public DateTime Created { get; set; }
        public DateTime? Updated { get; set; }

        public static CategoryViewModel FromDomainModel(Category category)
        {
            var result = new CategoryViewModel
            {
                Id = category.Id,
                IsHidden = category.IsHidden,
                StringKey = category.StringKey,
                Title = category.Title,
                Description = category.Description,
                Image = category.Image,
                Created = category.Created,
                Updated = category.Updated
            };

            if (category.Products.Count > 0) {
                result.Products = category.Products.Select(ProductsViewModel.FromDomainModel).ToList();
            }

            return result;
        }
    }
}