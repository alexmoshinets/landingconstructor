﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using DomainModel.Navigation;
using WebSite.Areas.Admin.Framework.HtmlHelpers;
using WebSite.Areas.Admin.Models.Content;

namespace WebSite.Areas.Admin.Models.Navigation
{
    public class MenuItemViewModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Название")]
        public string Name { get; set; }

        [Display(Name = "Скрыть?")]
        public bool IsHidden { get; set; }

        [Required]
        [Display(Name = "Тип пункта меню")]
        public MenuItemType Type { get; set; }

        public IList<SelectListItem> TypeAll { get; set; }

        [Display(Name = "Ссылка на внешний ресурс")]
        public string CustomUrl { get; set; }

        [Display(Name = "Существующая страница")]
        public int? PageId { get; set; }

        public PageViewModel Page { get; set; }
        public IList<SelectListItem> PageAll { get; set; }

        [Display(Name = "Меню")]
        public int? MenuId { get; set; }

        public IList<SelectListItem> MenuAll { get; set; }

        public int? ParentId { get; set; }
        public IList<MenuItemViewModel> Childs { get; set; }

        public DateTime Created { get; set; }
        public DateTime? Updated { get; set; }

        public static MenuItemViewModel FromDomainModel(MenuItem menuItem)
        {
            var result = new MenuItemViewModel
            {
                Id = menuItem.Id,
                Name = menuItem.Name,
                IsHidden = menuItem.IsHidden,
                Type = menuItem.Type,
                CustomUrl = menuItem.CustomUrl,
                PageId = menuItem.PageId,
                MenuId = menuItem.MenuId,
                ParentId = menuItem.ParentId,
                Created = menuItem.Created,
                Updated = menuItem.Updated
            };
            if (menuItem.Page != null)
            {
                result.Page = PageViewModel.FromDomainModel(menuItem.Page);
                result.Page.Name = LH.GLSS(result.Page.Name);
            }
            if (menuItem.Childs != null)
            {
                result.Childs = menuItem.Childs.Select(FromDomainModel).ToList();
            }

            return result;
        }
    }
}