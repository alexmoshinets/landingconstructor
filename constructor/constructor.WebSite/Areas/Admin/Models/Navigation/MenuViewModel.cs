﻿using System.Collections.Generic;
using System.Linq;
using DomainModel.Navigation;

namespace WebSite.Areas.Admin.Models.Navigation
{
    public class MenuViewModel
    {
        public int Id { get; set; }
        public string StringKey { get; set; }
        public string Name { get; set; }

        public IList<MenuItemViewModel> Items { get; set; }

        public static MenuViewModel FromDomainModel(Menu menu)
        {
            var result = new MenuViewModel
            {
                Id = menu.Id,
                StringKey = menu.StringKey,
                Name = menu.Name
            };
            if (menu.Items != null)
            {
                result.Items = menu.Items.Select(MenuItemViewModel.FromDomainModel).ToList();
            }

            return result;
        }
    }
}