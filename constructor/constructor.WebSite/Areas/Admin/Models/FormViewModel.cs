﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebSite.Areas.Admin.Models
{
    public class FormViewModel
    {
        public string FormId { get; set; }
        public int LandingId { get; set; }
        public int TypeIntegration { get; set; }
        public IDictionary<string, string> Fields { get; set; }
        public IDictionary<string, string> FieldsEmail { get; set; }

        [Display(Name = "Источник трафика")]
        public string utm_source { get; set; }

        [Display(Name = "Канал кампании")]
        public string utm_medium { get; set; }

        [Display(Name = "Название кампании")]

        public string utm_campaign { get; set; }
        [Display(Name = "Объявление")]
        public string utm_content { get; set; }

        [Display(Name = "Ключевое слово")]
        public string utm_term { get; set; }

        public string FName { get; set; }
        public string LName { get; set; }
        public string OName { get; set; }
        public string Email { get; set; }
    }
}