﻿using Newtonsoft.Json;
using Spoofi.AmoCrmIntegration.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSite.Areas.Admin.Models.AmoCrm
{
    public class CrmTag: IAmoCrmEntity
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}