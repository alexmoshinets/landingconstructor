﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace WebSite.Areas.Admin.Models.AmoCrm
{
    public enum AmoCrmErrors
    {
        [Description("Неизвестная ошибка. Проверьте конфигурацию.")]
        Unknown
    }
}