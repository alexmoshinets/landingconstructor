﻿using Newtonsoft.Json;
using Spoofi.AmoCrmIntegration.AmoCrmEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSite.Areas.Admin.Models.AmoCrm
{
    public class CrmCustomFieldModel
    {
        public CrmCustomFieldModel()
        {
            Values = new List<CrmCustomFieldValueModel>();
        }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("values")]
        public List<CrmCustomFieldValueModel> Values { get; set; }

        public static CrmCustomFieldModel FromDomainModel(CrmCustomField field)
        {
            var result = new CrmCustomFieldModel()
            {
                Name = field.Name,
                Id = field.Id,
                Code = field.Code
            };

            if (field.Values != null && field.Values.Count > 0)
            {
                result.Values = field.Values.Select(CrmCustomFieldValueModel.FromDomainModel).ToList();
            }

            return result;
        }

    }

    public class CrmCustomFieldValueModel
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("enum")]
        public string @Enum { get; set; }

        [JsonProperty("last_modified")]
        public long LastModified { get; set; }

        public static CrmCustomFieldValueModel FromDomainModel(CrmCustomFieldValue valueentity)
        {
            return new CrmCustomFieldValueModel()
            {
                Enum = valueentity.Enum,
                Id = valueentity.Id,
                LastModified = valueentity.LastModified,
                Value = valueentity.Value
            };
        }
    }
}