﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using WebSite.Areas.Admin.Models.AmoCrm;
using WebSite.Framework.Helpers;

namespace WebSite.Areas.Admin.Models.AmoCrm
{
    [Serializable]
    public class AmoCrmException : ApplicationException
    {
        public AmoCrmException()
        {
        }

        public AmoCrmException(string message) : base(message)
        {
        }

        public AmoCrmException(AmoCrmErrors error) : base(error.GetDescription())
        {
        }

        public AmoCrmException(string message, Exception inner) : base(message, inner)
        {
        }

        protected AmoCrmException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}