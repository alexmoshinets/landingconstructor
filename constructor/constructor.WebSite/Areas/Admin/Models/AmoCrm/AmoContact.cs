﻿using Newtonsoft.Json;
using Spoofi.AmoCrmIntegration.AmoCrmEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSite.Areas.Admin.Models.AmoCrm
{
    public class AmoContact
    {
        public AmoContact()
        {
            CustomFields = new List<CrmCustomFieldModel>();
        }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("last_modified")]
        public long LastModifiedTimestamp { get; set; }

        public DateTime LastModified
        {
            get { return LastModifiedTimestamp.GetDateTime(); }
            set { LastModifiedTimestamp = value.GetTimestamp(); }
        }

        [JsonProperty("account_id")]
        public long AccountId { get; set; }

        [JsonProperty("responsible_user_id")]
        public long ResponsibleUserId { get; set; }

        [JsonProperty("date_create")]
        public long DateCreateTimestamp { get; set; }

        public DateTime DateCreate
        {
            get { return DateCreateTimestamp.GetDateTime(); }
            set { DateCreateTimestamp = value.GetTimestamp(); }
        }

        [JsonProperty("created_user_id")]
        public long CreatedUserId { get; set; }

        [JsonProperty("linked_leads_id")]
        public List<long> LinkedLeadsId { get; set; }

        [JsonProperty("tags")]
        public List<CrmTag> Tags { get; set; }

        [JsonProperty("Type")]
        public string Type { get; set; }

        [JsonProperty("custom_fields")]
        public List<CrmCustomFieldModel> CustomFields { get; set; }

        public static AmoContact FromDomainModel(CrmContact contact)
        {
            var result = new AmoContact()
            {
                Id = contact.Id,
                Name = contact.Name,
                LastModifiedTimestamp = contact.LastModifiedTimestamp,
                LastModified = contact.LastModified,
                AccountId = contact.AccountId,
                ResponsibleUserId = contact.ResponsibleUserId,
                DateCreateTimestamp = contact.DateCreateTimestamp,
                DateCreate = contact.DateCreate,
                CreatedUserId = contact.CreatedUserId,
                Type = contact.Type
            };

            if (contact.CustomFields != null && contact.CustomFields.Count > 0)
            {
                result.CustomFields = contact.CustomFields.Select(CrmCustomFieldModel.FromDomainModel).ToList();
            }

            return result;
        }

    }
}