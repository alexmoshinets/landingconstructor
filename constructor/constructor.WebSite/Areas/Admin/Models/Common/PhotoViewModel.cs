﻿using System.ComponentModel.DataAnnotations;
using System.Web;

namespace WebSite.Areas.Admin.Models.Common
{
    public class PhotoViewModel
    {
        [Required]
        [Display(Name = "Загрузить новое", Description = "Только .jpg (Максимальный размер: 1MB)")]
        public HttpPostedFileBase File { get; set; }

        public string ImageUrl { get; set; }
    }
}