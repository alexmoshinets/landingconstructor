﻿using System;

namespace WebSite.Areas.Admin.Models.Common
{
    public class FileBrowserViewModel
    {
        public string Icon { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string Size { get; set; }
        public DateTime DateAdd { get; set; }
    }
}