﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using DomainModel.Content;

namespace WebSite.Areas.Admin.Models.Common
{
    public class FooterViewModel
    {
        [AllowHtml]
        [Display(Name = "Надпись в подвале")]
        public string Content { get; set; }
    }
}