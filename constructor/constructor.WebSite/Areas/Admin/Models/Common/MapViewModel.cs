﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using DomainModel.Content;

namespace WebSite.Areas.Admin.Models.Common
{
    public class MapViewModel
    {
        [AllowHtml]
        [Display(Name = "Карта")]
        public string Content { get; set; }
    }
}