﻿using System.ComponentModel.DataAnnotations;
using System.Web;

namespace WebSite.Areas.Admin.Models.Common
{
    public class LogoViewModel
    {
        [Required]
        [Display(Name = "Загрузить новый", Description = "Только .png (Максимальный размер: 1MB)")]
        public HttpPostedFileBase File { get; set; }

        public string ImageUrl { get; set; }
    }
}