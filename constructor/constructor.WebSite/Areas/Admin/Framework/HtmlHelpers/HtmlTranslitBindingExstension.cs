﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using System.Web.Mvc;

namespace WebSite.Areas.Admin.Framework.HtmlHelpers
{
    public static class HtmlTranslitBindingExstension
    {
        public enum TranslitType
        {
            Translit,
            Original
        }

        public static MvcHtmlString TranslitBind<TModel, TProperty>(this HtmlHelper<TModel> html,
            Expression<Func<TModel, TProperty>> src, Expression<Func<TModel, TProperty>> dest, TranslitType type)
        {
            var propSrc = GetPropertyInfo(src);
            var propDest = GetPropertyInfo(dest);

            var format = "<script type=\"text/javascript\">" +
                            "$(function () {{" +
                            "translitBind(\"{0}\", \"{1}\", {2});" +
                            "}});" +
                            "</script>";

            var result = String.Format(format, propSrc.Name, propDest.Name,
                type == TranslitType.Original ? "true" : "false");
            return new MvcHtmlString(result);
        }

        private static PropertyInfo GetPropertyInfo<TSource, TProperty>(
            Expression<Func<TSource, TProperty>> propertyLambda)
        {
            var type = typeof(TSource);

            var member = propertyLambda.Body as MemberExpression;
            if (member == null)
                throw new ArgumentException(string.Format("Expression '{0}' refers to a method, not a property.",
                    propertyLambda));

            var propInfo = member.Member as PropertyInfo;
            if (propInfo == null)
                throw new ArgumentException(string.Format("Expression '{0}' refers to a field, not a property.",
                    propertyLambda));

            if (type != propInfo.ReflectedType && !type.IsSubclassOf(propInfo.ReflectedType))
                throw new ArgumentException(
                    string.Format("Expresion '{0}' refers to a property that is not from type {1}.", propertyLambda,
                        type));

            return propInfo;
        }

    }
}