﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OrangeJetpack.Localization;
using Services;
using WebSite.Models.Content;

namespace WebSite.Areas.Admin.Framework.HtmlHelpers
{
    public static class LH
    {
        static DataContext db = new DataContext();
        public static string GLSS(string str, string lang = "ru")
        {
            if (string.IsNullOrEmpty(str))
            {
                str = "[{\"k\":\"en\",\"v\":\"\"},{\"k\":\"ru\",\"v\":\"\"}]";
            }
            var list = LocalizedContent.Deserialize("[{\"k\":\"en\",\"v\":\"\"},{\"k\":\"ru\",\"v\":\"\"}]");
            try
            {
                list = LocalizedContent.Deserialize(str);
            }
            catch(Exception ex)
            {
                list = LocalizedContent.Deserialize("[{\"k\":\"en\",\"v\":\"\"},{\"k\":\"ru\",\"v\":\"\"}]");
            }
            foreach (var item in list.Where(item => item.Key == lang))
            {
                return item.Value;
            }
            return "";
        }

        public static string GLSK(string key, string lang = "ru")
        {
            var lString = db.Localizations.FirstOrDefault(_ => _.StringKey.Equals(key));
            if (lString != null)
            {
                  return GLSS(lString.Name, lang);
            }
            return "";
        }

        public static LocalizedContent[] Normalization(LocalizedContent[] list)
        {
            foreach (var item in list)
            {
                item.Value = string.IsNullOrEmpty(item.Value) ? "&nbsp;" : item.Value.Replace("\"", "&quot;");
                
            }
            return list;
        }

        public static LocalizedContent[] DeNormalization(LocalizedContent[] list)
        {
            foreach (var item in list)
            {
                item.Value = item.Value.Replace("&quot;", "\"");
            }
            return list;
        }
        public static string DNS(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                str = " ";
            }
            return str.Replace("&quot;", "\"");
        }
    }
}