﻿using Spoofi.AmoCrmIntegration;
using Spoofi.AmoCrmIntegration.Dtos.Request;
using Spoofi.AmoCrmIntegration.Interface;
using Spoofi.AmoCrmIntegration.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebSite.Areas.Admin.Models.AmoCrm;

namespace WebSite.Areas.Admin.Service
{
    public class AmoCrmServiceIntegration
    {
        private const string ContactName = "Геннадий";
        private const int ContactId = 65682868;
        private const int ResponsibleUserId = 144078;
        private readonly IAmoCrmService _service;

        public AmoCrmServiceIntegration(string subdomain, string login, string hash)
        {
            _service = new AmoCrmService(new AmoCrmConfig(subdomain, login, hash));
        }

        public void Send(AmoContact model)
        {
            var newContact = new AddOrUpdateCrmContact
            {
                Name = model.Name,
                //CustomFields = new List<AddContactCustomField>()
                //{
                //    new AddContactCustomField()
                //    {
                //        Id = 1229414,
                //        Values = new List<AddContactCustomFieldValues>()
                //        {
                //            new AddContactCustomFieldValues() {Value = "текстище" }
                //        }
                //    }
                //}
            };
            newContact.CustomFields = new List<AddContactCustomField>();
            foreach (var field in model.CustomFields)
            {
                newContact.CustomFields.Add(new AddContactCustomField()
                {
                    Id = field.Id,
                    Values = new List<AddContactCustomFieldValues>()
                    {
                        new AddContactCustomFieldValues() { Value = field.Values.First().Value, Enum = "WORK" }
                    }
                });
            }

            var t = _service.GetAccountInfo();

            var result = _service.AddOrUpdateContact(new AddOrUpdateCrmContacts
            {
                Add = new List<AddOrUpdateCrmContact> { newContact }
            });                        
        }
    }

    public static class TestCrmConfig
    {
        private static readonly AmoCrmConfig Config = new AmoCrmConfig("new57fe8b988be85", "aleksandr.moshinec1@yandex.ru", "21737e33d766f8b43905445f106ce2ef");

        public static AmoCrmConfig Get()
        {
            return Config;
        }
    }
}