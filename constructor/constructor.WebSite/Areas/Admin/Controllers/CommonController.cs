﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using DomainModel.Content;
using Interfaces.Content;
using OrangeJetpack.Localization;
using Services;
using WebSite.Areas.Admin.Framework.HtmlHelpers;
using WebSite.Areas.Admin.Models.Common;
using WebSite.Areas.Admin.Models.Content;
using WebSite.Framework;
using PhotoViewModel = WebSite.Areas.Admin.Models.Common.PhotoViewModel;

namespace WebSite.Areas.Admin.Controllers
{
    public class CommonController : AdminBaseController
    {
        private readonly ISettingService _settingService;


        public CommonController( ISettingService settingService){
            _settingService = settingService;
        }

        [ChildActionOnly]
        public ActionResult TopMenu()
        {
            var currentController =
                ControllerContext.ParentActionViewContext.Controller.ValueProvider.GetValue("controller")
                    .RawValue.ToString();
            var currentAction =
                ControllerContext.ParentActionViewContext.Controller.ValueProvider.GetValue("action")
                    .RawValue.ToString();

            var model = new List<MenuItemViewModel>
            {
                //new MenuItemViewModel
                //{
                //    Title = "Главная",
                //    Controller = "Home"
                //},                
                new MenuItemViewModel
                {
                    Title = "Общее",
                    Childs = new List<MenuItemViewModel>
                    {
                        //new MenuItemViewModel {Title = "Слоган", Controller = "Common", Action = "Slogan"},
                        new MenuItemViewModel {Title = "Логотип", Controller = "Common", Action = "Logo"},
                        //new MenuItemViewModel {Title = "Партнеры", Controller = "Partners", Action = "Index"},
                        new MenuItemViewModel {Title = "Слайдер", Controller = "Slider"},
                        //new MenuItemViewModel {Title = "Фоновая картинка", Controller = "Background"},
                        new MenuItemViewModel {Title = "Контактрые данные", Controller = "Common", Action ="EditCompamyName"},
                        new MenuItemViewModel {Title = "Подвал", Controller = "Common", Action ="EditCopyright"},
                        new MenuItemViewModel {Title = "Карта", Controller = "Common", Action ="Map"}
                    }
                },
                new MenuItemViewModel
                {
                    Title = "Меню",
                    Childs = new List<MenuItemViewModel>
                    {
                        new MenuItemViewModel {Title = "Список", Controller = "Menu"},
                        new MenuItemViewModel {Title = "Добавить", Controller = "Menu", Action = "Add"}
                    }
                },
                new MenuItemViewModel
                {
                    Title = "Страницы",
                    Childs = new List<MenuItemViewModel>
                    {
                        new MenuItemViewModel {Title = "Список", Controller = "Page"},
                        new MenuItemViewModel {Title = "Добавить", Controller = "Page", Action = "Add"}
                    }
                },
                //new MenuItemViewModel
                //{
                //    Title = "Блоки",
                //    Childs = new List<MenuItemViewModel>
                //    {
                //        new MenuItemViewModel {Title = "Список", Controller = "Block"},
                //        new MenuItemViewModel {Title = "Добавить", Controller = "Block", Action = "Add"}
                //    }
                //},
                //new MenuItemViewModel
                //{
                //    Title = "Статьи",
                //    Childs = new List<MenuItemViewModel>
                //    {
                //        new MenuItemViewModel {Title = "Список", Controller = "Article"},
                //        new MenuItemViewModel {Title = "Добавить статью", Controller = "Article", Action = "Add"},
                //        new MenuItemViewModel
                //        {
                //            Title = "Добавить категорию",
                //            Controller = "Article",
                //            Action = "AddCategory"
                //        }
                //    }
                //},
                new MenuItemViewModel
                {
                    Title = "Каталог продукции",
                    Controller = "Product"
                },
                new MenuItemViewModel
                {
                    Title = "Категории",
                    Controller = "Category"
                },
                new MenuItemViewModel
                {
                    Title = "Локализация",
                    Controller = "Localization"
                }
            };

            foreach (var item in model)
            {
                if (item.Controller != null &&
                    item.Controller.Equals(currentController, StringComparison.InvariantCultureIgnoreCase)
                    ||
                    item.Childs.Any(
                        _ =>
                            _.Controller != null &&
                            _.Controller.Equals(currentController, StringComparison.InvariantCultureIgnoreCase)))
                {
                    item.IsActive = true;
                }
                if (item.Childs.Count > 0)
                {
                    foreach (var child in item.Childs)
                    {
                        if (child.Controller != null &&
                            child.Controller.Equals(currentController, StringComparison.InvariantCultureIgnoreCase)
                            && child.Action.Equals(currentAction, StringComparison.InvariantCultureIgnoreCase))
                        {
                            child.IsActive = true;
                        }
                    }
                }
            }

            return PartialView(model);
        }

        [ChildActionOnly]
        public ActionResult UserMenu()
        {
            return PartialView();
        }

        [ChildActionOnly]
        public ActionResult SideMenu()
        {
            return PartialView();
        }

        [ChildActionOnly]
        public ActionResult PageHeader(string title)
        {
            ViewBag.Title = title;
            ViewBag.Date = DateTime.Now;
            return PartialView();
        }

        [HttpGet]
        public ActionResult Map()
        {
            var map = _settingService.GetByKey("Map");
            var model = new MapViewModel();
            if (map == null)
            {
                var set = new Setting
                {
                    StringKey = "Map",
                    Value = ""
                };
                _settingService.Add(set);
                model.Content = set.Value;
            }
            else
            {
                model.Content = map.Value;
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Map(MapViewModel model)
        {
            if (ModelState.IsValid)
            {
                _settingService.Update("Map", map => { map.Value = model.Content; });
                SuccessMessage("Карта успешно изменена.");
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult Logo()
        {
            var model = new LogoViewModel();
            var setting = _settingService.GetByKey("LogoTimestamp");
            if (setting != null)
            {
                model.ImageUrl = setting.Value;
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Logo(LogoViewModel model)
        {
            if (ModelState.IsValid)
            {
                var path = Server.MapPath(WorkContext.FilesPath + "bg_logo.png");
                var newPath = Server.MapPath(WorkContext.FilesPath + "bg_logo_old.png");
                System.IO.File.Delete(newPath);
                System.IO.File.Move(path, newPath);

                model.File.SaveAs(path);

                _settingService.Update("LogoTimestamp",
                    s => { s.Value = DateTime.Now.Ticks.ToString(CultureInfo.InvariantCulture); });

                SuccessMessage("Логотип успешно изменен.");
            }
            var setting = _settingService.GetByKey("LogoTimestamp");
            if (setting != null)
            {
                model.ImageUrl = setting.Value;
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult Photo()
        {
            var model = new PhotoViewModel();
            var setting = _settingService.GetByKey("PhotoTimestamp");
            if (setting != null)
            {
                model.ImageUrl = setting.Value;
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Photo(PhotoViewModel model)
        {
            if (ModelState.IsValid)
            {
                var path = Server.MapPath(WorkContext.FilesPath + "photo.jpg");
                var newPath = Server.MapPath(WorkContext.FilesPath + "photo_old.jpg");
                System.IO.File.Delete(newPath);
                System.IO.File.Move(path, newPath);

                model.File.SaveAs(path);

                _settingService.Update("PhotoTimestamp",
                    s => { s.Value = DateTime.Now.Ticks.ToString(CultureInfo.InvariantCulture); });

                SuccessMessage("Логотип успешно изменен.");
            }
            var setting = _settingService.GetByKey("PhotoTimestamp");
            if (setting != null)
            {
                model.ImageUrl = setting.Value;
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult SideAds()
        {
            var model = new SideAdsViewModel();
            var setting = _settingService.GetByKey("SideAdsTimestamp");
            if (setting != null)
            {
                model.ImageUrl = WorkContext.FilesPath + "sideAdds.png?" + setting.Value;
                model.IsHidden = string.IsNullOrWhiteSpace(setting.Value);
            }
            else
            {
                model.IsHidden = true;
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult SideAds(SideAdsViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.File != null)
                {
                    var path = Server.MapPath(WorkContext.FilesPath + "sideAdds.png");
                    var newPath = Server.MapPath(WorkContext.FilesPath + "sideAdds_old.png");
                    System.IO.File.Delete(newPath);
                    System.IO.File.Move(path, newPath);

                    model.File.SaveAs(path);
                }

                _settingService.Update("SideAdsTimestamp",
                    s =>
                    {
                        s.Value = model.IsHidden ? null : DateTime.Now.Ticks.ToString(CultureInfo.InvariantCulture);
                    });

                SuccessMessage("Баннер успешно изменен.");
            }
            var setting = _settingService.GetByKey("SideAdsTimestamp");
            if (setting != null)
            {
                model.ImageUrl = WorkContext.FilesPath + "sideAdds.png?" + setting.Value;
                model.IsHidden = string.IsNullOrWhiteSpace(setting.Value);
            }
            else
            {
                model.IsHidden = true;
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult EditCompamyName()
        {
            var companyName = _settingService.GetByKey("companyName");
            var Tel = _settingService.GetByKey("Tel");
            var WorkTel = _settingService.GetByKey("WorkTel");
            var Email = _settingService.GetByKey("Email");
            var WebSite = _settingService.GetByKey("WebSite");
            var Address = _settingService.GetByKey("Address");
            var model = new CompanyNameViewModel();

            if (companyName!=null&&companyName.Value != null)
            {
                model.Name = companyName.Value;
            }
            else
            {
                model.Name = "[{\"k\":\"en\",\"v\":\"\"},{\"k\":\"ru\",\"v\":\"\"}]";
            }
            if (Address != null && Address.Value != null)
            {
                model.Address = Address.Value;
            }
            else
            {
                model.Address = "[{\"k\":\"en\",\"v\":\"\"},{\"k\":\"ru\",\"v\":\"\"}]";
            }
            if (Tel != null) model.Tel = Tel.Value;
            if (WorkTel != null) model.WorkTel = WorkTel.Value;
            if (Email != null) model.Email = Email.Value;
            if (WebSite != null) model.WebSite = WebSite.Value;
            return View(model);
        }

        [HttpPost]
        public ActionResult EditCompamyName(CompanyNameViewModel companyName, LocalizedContent[] name, LocalizedContent[] address)
        {
            name = LH.Normalization(name);
            address = LH.Normalization(address);
            if (companyName != null)
            {
                companyName.Name = name.Serialize();
                companyName.Address = address.Serialize();

                _settingService.Update("WorkTel", setting =>
                {
                    setting.Value = companyName.WorkTel;
                });
                _settingService.Update("Email", setting =>
                {
                    setting.Value = companyName.Email;
                });
                _settingService.Update("WebSite", setting =>
                {
                    setting.Value = companyName.WebSite;
                });
                _settingService.Update("companyName", setting =>
                {
                    setting.Value = name.Serialize();                    
                });
                _settingService.Update("Tel", setting =>
                {
                    setting.Value = companyName.Tel;
                });
                _settingService.Update("Address", setting =>
                {
                    setting.Value = address.Serialize();
                });
            }
            return View(companyName);
        }

        [HttpGet]
        public ActionResult EditCopyright()
        {
            var copyright = _settingService.GetByKey("copyright");
            var model = new FooterViewModel();
            if (copyright != null && copyright.Value != null)
                model.Content = copyright.Value;
            else
            {
                model.Content = "[{\"k\":\"en\",\"v\":\"\"},{\"k\":\"ru\",\"v\":\"\"}]";
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult EditCopyright(FooterViewModel model, LocalizedContent[] content)
        {
            content = LH.Normalization(content);
            if (model != null)
            {
                model.Content = content.Serialize();
                _settingService.Update("copyright", copyright =>
                {
                    copyright.Value = model.Content;
                });                
            }
            return View(model);
        }
    }
}