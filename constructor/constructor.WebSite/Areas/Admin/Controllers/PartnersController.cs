﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DomainModel.Content;
using Interfaces.Content;
using WebSite.Areas.Admin.Models.Content;
using WebSite.Framework;
using System.IO;

namespace WebSite.Areas.Admin.Controllers
{
    public class PartnersController : Controller
    {
        private readonly IPartnersService _partnersService;

        public PartnersController(IPartnersService partnersService)
        {
            _partnersService = partnersService;
        }
        public ActionResult Index()
        {
            var partners = _partnersService.GetAll();
            var model = partners.Select(PartnerViewModel.FromDomainModel).ToList();

            return View(model);
        }

        [HttpGet]
        public ActionResult Add()
        {
            var model = new PartnerViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Add(PartnerViewModel model)
        {
            if (ModelState.IsValid)
            {

                var fileName = Guid.NewGuid() + Path.GetExtension(model.File.FileName); ;
                var path = Server.MapPath(WorkContext.PartnerPath + fileName);
                model.File.SaveAs(path);

                var partner = new Partner
                {
                    IsHidden = model.IsHidden,
                    Title = model.Title,
                    Description=model.Description,
                    Image = fileName,
                    Url = model.Url,
                    Created = DateTime.UtcNow
                };
                _partnersService.Add(partner);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var partner = _partnersService.GetById(id);
            if (partner == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            var model = PartnerViewModel.FromDomainModel(partner);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(int id, PartnerViewModel model)
        {
            if (ModelState.IsValid)
            {
                _partnersService.Update(id, page =>
                {
                    page.IsHidden = model.IsHidden;
                    page.Title = model.Title;
                    page.Description = model.Description;
                    if (model.File != null)
                    {
                        var fileName = model.File.FileName;
                        var path = Server.MapPath(WorkContext.PartnerPath + fileName);
                        model.File.SaveAs(path);
                        page.Image = fileName;
                    }

                    page.Url = model.Url;

                    page.Updated = DateTime.UtcNow;
                });
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            _partnersService.Delete(id);
            return RedirectToAction("Index");
        }
    }
}