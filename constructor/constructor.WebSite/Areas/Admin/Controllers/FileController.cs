﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using WebSite.Areas.Admin.Models.Common;
using WebSite.Framework;
using WebSite.Framework.Helpers;

namespace WebSite.Areas.Admin.Controllers
{
    public class FileController : Controller
    {
        private readonly VirtualPathProvider _virtualPathProvider;


        public FileController(VirtualPathProvider virtualPathProvider)
        {
            _virtualPathProvider = virtualPathProvider;
        }

        public ActionResult Browser()
        {
            var model = new List<FileBrowserViewModel>();

            var filesPath = Server.MapPath(WorkContext.FilesPath);
            if (!Directory.Exists(filesPath))
            {
                Directory.CreateDirectory(filesPath);
            }

            var files = Directory.GetFiles(filesPath);
            foreach (var file in files)
            {
                var info = new FileInfo(file);
                model.Add(new FileBrowserViewModel
                {
                    Icon =
                        VirtualPathUtility.ToAbsolute(_virtualPathProvider.CombineVirtualPaths(WorkContext.FilesPath,
                            info.Name)),
                    Url =
                        VirtualPathUtility.ToAbsolute(_virtualPathProvider.CombineVirtualPaths(WorkContext.FilesPath,
                            info.Name)),
                    Name = info.Name,
                    Size = FileHelper.GetFileSize(info.Length),
                    DateAdd = info.CreationTime
                });
            }

            return View(model.OrderBy(_ => _.DateAdd).ToList());
        }

        public ActionResult Uploader(HttpPostedFileBase upload, string ckEditorFuncNum)
        {
            var filesPath = Server.MapPath(WorkContext.FilesPath);
            if (!Directory.Exists(filesPath))
            {
                Directory.CreateDirectory(filesPath);
            }

            upload.SaveAs(Path.Combine(filesPath, upload.FileName));

            var url =
                VirtualPathUtility.ToAbsolute(_virtualPathProvider.CombineVirtualPaths(WorkContext.FilesPath,
                    upload.FileName));

            return
                Content(
                    string.Format(
                        "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction({0}, '{1}');</script>",
                        ckEditorFuncNum, url));
        }
    }
}