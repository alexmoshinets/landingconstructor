﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Interfaces.Content;
using WebSite.Areas.Admin.Models.Content;
using DomainModel.Content;
using System.IO;
using OrangeJetpack.Localization;
using WebSite.Areas.Admin.Framework.HtmlHelpers;
using WebSite.Framework;

namespace WebSite.Areas.Admin.Controllers
{
    public class CategoryController : Controller
    {
        private readonly ICategoryService _categryService;

        public CategoryController(ICategoryService categoryService)
        {
            _categryService = categoryService;
        }
        //
        // GET: /Admin/Category/
        public ActionResult Index()
        {
            var categories = _categryService.GetAll().Where(_ => _.CategoryId == null);
            var model = new List<CategoryViewModel>();
            model = categories.Select(CategoryViewModel.FromDomainModel).ToList();
            return View(model);
        }

        [HttpGet]
        public ActionResult Add()
        {
            var model = new CategoryViewModel();
            model.Title = "[{\"k\":\"en\",\"v\":\"\"},{\"k\":\"ru\",\"v\":\"\"}]";
            model.Description = "[{\"k\":\"en\",\"v\":\"\"},{\"k\":\"ru\",\"v\":\"\"}]";
            return View(model);
        }


        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Add(CategoryViewModel model, LocalizedContent[] title, LocalizedContent[] description)
        {
            title = LH.Normalization(title);
            description = LH.Normalization(description);
            model.Title = title.Serialize();
            model.Description = description.Serialize();
            if (model.Title != null && model.Description != null)
            {

                var fileName = "no-image.jpg";
                if (model.File != null)
                {
                    if (!Directory.Exists(WorkContext.CategoriesPath))
                    {
                        Directory.CreateDirectory(WorkContext.CategoriesPath);
                    }

                    fileName = Guid.NewGuid() + Path.GetExtension(model.File.FileName);
                    var path = Server.MapPath(WorkContext.CategoriesPath + fileName);
                    model.File.SaveAs(path);
                }

                var category = new Category
                {
                    IsHidden = model.IsHidden,
                    StringKey = model.StringKey,
                    Title = model.Title,
                    Description = model.Description,
                    Image = fileName,
                    Created = DateTime.UtcNow
                };
                _categryService.Add(category);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var category = _categryService.GetById(id);
            var childs = _categryService.GetAll().Where(_ => _.CategoryId == id);
            var model = CategoryViewModel.FromDomainModel(category);
            model.Childs = childs.Select(CategoryViewModel.FromDomainModel).ToList();
            return View(model);
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Edit(int id, CategoryViewModel model, LocalizedContent[] title, LocalizedContent[] description)
        {
            title = LH.Normalization(title);
            description = LH.Normalization(description); 
            model.Title = title.Serialize();
            model.Description = description.Serialize();
            if (model.Title != null && model.Description != null)
            {
                if (model.File != null)
                {
                    if (!Directory.Exists(WorkContext.CategoriesPath))
                    {
                        Directory.CreateDirectory(WorkContext.CategoriesPath);
                    }

                    model.Image = Guid.NewGuid() + Path.GetExtension(model.File.FileName);
                    var path = Server.MapPath(WorkContext.CategoriesPath + model.Image);
                    model.File.SaveAs(path);
                }

                _categryService.Update(id, category =>
                {
                    category.IsHidden = model.IsHidden;
                    category.StringKey = model.StringKey;
                    category.Title = model.Title;
                    category.Description = model.Description;
                    category.Image = model.Image;
                    category.Updated = DateTime.UtcNow;
                });
                if (model.CategoryId != null)
                {
                    return RedirectToAction("Edit", new{id = model.CategoryId});
                }
                else
                {
                    return RedirectToAction("Index");
                }

            }
            return View(model);
        }

        [HttpGet]
        public ActionResult AddSubCategory(int id)
        {
            var model = new CategoryViewModel();
            model.CategoryId = id;
            model.Title = "[{\"k\":\"en\",\"v\":\"\"},{\"k\":\"ru\",\"v\":\"\"}]";
            model.Description = "[{\"k\":\"en\",\"v\":\"\"},{\"k\":\"ru\",\"v\":\"\"}]";
            return View(model);
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult AddSubCategory(CategoryViewModel model, LocalizedContent[] title, LocalizedContent[] description)
        {
            title = LH.Normalization(title);
            description = LH.Normalization(description);
            model.Title = title.Serialize();
            model.Description = description.Serialize();
            if (model.Title != null && model.Description != null)
            {
                var fileName = "no-image.jpg";
                if (model.File != null)
                {
                    if (!Directory.Exists(WorkContext.CategoriesPath))
                    {
                        Directory.CreateDirectory(WorkContext.CategoriesPath);
                    }

                    fileName = Guid.NewGuid() + Path.GetExtension(model.File.FileName);
                    var path = Server.MapPath(WorkContext.CategoriesPath + fileName);
                    model.File.SaveAs(path);
                }

                var category = new Category
                {
                    IsHidden = model.IsHidden,
                    StringKey = model.StringKey,
                    Title = model.Title,
                    Description = model.Description,
                    Image = fileName,
                    CategoryId = model.CategoryId,
                    Created = DateTime.UtcNow
                };
                _categryService.Add(category);
                return RedirectToAction("Edit", new { id = model.CategoryId });
            }
            return View(model);
        }
        public ActionResult Delete(int id)
        {
            var filename = Server.MapPath(WorkContext.CategoriesPath + _categryService.Delete(id).ToString());
            if (filename != null && System.IO.File.Exists(filename))
            {
                System.IO.File.Delete(filename);
            }

            return RedirectToAction("Index");
        }
    }
}