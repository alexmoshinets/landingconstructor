﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DomainModel.Content;
using Interfaces.Common;
using Interfaces.Content;
using WebSite.Areas.Admin.Models.Content;
using WebSite.Framework;

namespace WebSite.Areas.Admin.Controllers
{
    public class PhotoController : AdminBaseController
    {
        public static readonly string[] ImageExtensions = { ".jpg", ".gif", ".png", ".bmp", ".jpeg" };
        private readonly IImageResizeService _imageResizeService;
        private readonly IPhotoAlbumService _photoAlbumService;
        private readonly IPhotoService _photoService;

        public PhotoController(IPhotoAlbumService photoAlbumService, IImageResizeService imageResizeService,
            IPhotoService photoService)
        {
            _photoAlbumService = photoAlbumService;
            _imageResizeService = imageResizeService;
            _photoService = photoService;
        }

        public ActionResult Index()
        {
            var albums = _photoAlbumService.GetAll(false);
            var model = albums.Select(PhotoAlbumViewModel.FromDomainModel).ToList();

            return View(model);
        }

        [HttpGet]
        public ActionResult Add()
        {
            var model = new PhotoAlbumViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Add(PhotoAlbumViewModel model)
        {
            if (ModelState.IsValid)
            {
                var album = new PhotoAlbum
                {
                    IsHidden = true,
                    Name = model.Name,
                    StringKey = model.StringKey,
                    Date = model.Date,
                    Description = model.Description,
                    MetaTitle = model.MetaTitle,
                    MetaKeywords = model.MetaKeywords,
                    MetaDescription = model.MetaDescription,
                    Created = DateTime.UtcNow
                };
                _photoAlbumService.Add(album);
                return RedirectToAction("Edit", new { id = album.Id });
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var album = _photoAlbumService.GetById(id);
            if (album == null)
            {
                return HttpNotFound();
            }

            var authCookie = FormsAuthentication.GetAuthCookie(User.Identity.Name, true);
            authCookie.Expires = DateTime.Now.AddHours(3);
            var authToken = authCookie.Value;

            var model = PhotoAlbumViewModel.FromDomainModel(album);
            model.AuthToken = authToken;
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(int id, PhotoAlbumViewModel model)
        {
            if (ModelState.IsValid)
            {
                _photoAlbumService.Update(id, album =>
                {
                    album.IsHidden = model.IsHidden;
                    album.Name = model.Name;
                    album.StringKey = model.StringKey;
                    album.Date = model.Date;

                    album.Description = model.Description;

                    album.MetaTitle = model.MetaTitle;
                    album.MetaKeywords = model.MetaKeywords;
                    album.MetaDescription = model.MetaDescription;

                    album.Updated = DateTime.UtcNow;
                });
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            _photoAlbumService.Delete(id);
            return RedirectToAction("Index");
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Upload(int albumId, HttpPostedFileBase fileData, string authToken)
        {
            var ticket = FormsAuthentication.Decrypt(authToken);
            if (ticket == null)
                return null;

            var identity = new FormsIdentity(ticket);
            if (!identity.IsAuthenticated)
                return null;

            var album = _photoAlbumService.GetById(albumId);
            if (album == null)
            {
                return new JsonResult
                {
                    Data = "Ok",
                    JsonRequestBehavior = JsonRequestBehavior.DenyGet,
                    ContentType = "application/json",
                    ContentEncoding = Encoding.UTF8
                };
            }

            var filesPath = Server.MapPath(WorkContext.PhotoPath);

            var extension = Path.GetExtension(fileData.FileName);
            if (extension != null && ImageExtensions.Contains(extension))
            {
                var albumDir = albumId.ToString(CultureInfo.InvariantCulture);

                var fileName = Path.Combine(albumDir, Guid.NewGuid() + extension);

                var photoAlbumPath = Path.Combine(filesPath, albumDir);
                if (!Directory.Exists(photoAlbumPath))
                {
                    Directory.CreateDirectory(photoAlbumPath);
                }
                fileData.SaveAs(Path.Combine(filesPath, fileName));

                var mediumImage = _imageResizeService.ResizePhoto(fileData, 280, 90);
                var mediumImageName = fileName.Replace(".", "_medium.");
                mediumImage.Save(Path.Combine(filesPath, mediumImageName));

                var photo = new Photo { Created = DateTime.UtcNow, OriginalUrl = fileName, MediumUrl = mediumImageName };

                _photoAlbumService.Update(album.Id, photoAlbum => { photoAlbum.Photos.Add(photo); });

                var model = PhotoViewModel.FromDomainModel(photo);
                return PartialView("_PhotoItem", model);
            }

            return null;
        }

        [HttpGet]
        public JsonResult DeletePhoto(int id)
        {
            var photo = _photoService.GetById(id);
            if (photo != null)
            {
                var filesPath = Server.MapPath(WorkContext.PhotoPath);
                var mediumPhotoPath = Path.Combine(filesPath, photo.MediumUrl);
                System.IO.File.Delete(mediumPhotoPath);
                var oroginalPhotoPath = Path.Combine(filesPath, photo.OriginalUrl);
                System.IO.File.Delete(oroginalPhotoPath);
                _photoService.Delete(id);
            }

            return new JsonResult
            {
                Data = "Ok",
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                ContentType = "application/json",
                ContentEncoding = Encoding.UTF8
            };
        }
    }
}