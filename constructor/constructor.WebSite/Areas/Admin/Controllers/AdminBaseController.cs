﻿using System.Web.Mvc;
using WebSite.Framework;

namespace WebSite.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]
    public class AdminBaseController : Controller
    {
        protected void SuccessMessage(string message)
        {
            TempData[Constants.NotificationsSuccess] = message;
        }

        protected void ErrorMessage(string message)
        {
            TempData[Constants.NotificationsError] = message;
        }
    }
}