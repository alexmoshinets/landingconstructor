﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DomainModel.Content;
using Interfaces.Content;
using OrangeJetpack.Localization;
using WebSite.Areas.Admin.Framework.HtmlHelpers;
using WebSite.Areas.Admin.Models.Content;

namespace WebSite.Areas.Admin.Controllers
{
    public class PageController : AdminBaseController
    {
        private readonly IPageService _pageService;

        public PageController(IPageService pageService)
        {
            _pageService = pageService;
        }

        //
        // GET: /Admin/Page/
        public ActionResult Index()
        {
            var pages = _pageService.GetAll();

            var model = new PageListViewModel();
            model.Sections = pages.Where(_ => _.IsSection).Select(PageViewModel.FromDomainModel).ToList();
            model.Pages = pages.Where(_ => !_.IsSection).Select(PageViewModel.FromDomainModel).ToList();


            return View(model);
        }

        [HttpGet]
        public ActionResult Add()
        {
            var model = new PageViewModel
            {
                Name = "[{\"k\":\"en\",\"v\":\"\"},{\"k\":\"ru\",\"v\":\"\"}]",
                Content = "[{\"k\":\"en\",\"v\":\"\"},{\"k\":\"ru\",\"v\":\"\"}]",
                BottomContent = "[{\"k\":\"en\",\"v\":\"\"},{\"k\":\"ru\",\"v\":\"\"}]",
                MetaDescription = "[{\"k\":\"en\",\"v\":\"\"},{\"k\":\"ru\",\"v\":\"\"}]",
                MetaTitle = "[{\"k\":\"en\",\"v\":\"\"},{\"k\":\"ru\",\"v\":\"\"}]",
                MetaKeywords = "[{\"k\":\"en\",\"v\":\"\"},{\"k\":\"ru\",\"v\":\"\"}]"
            };
            return View(model);
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Add(PageViewModel model, LocalizedContent[] name, LocalizedContent[] content, LocalizedContent[] bottomcontent, LocalizedContent[] metadescription, LocalizedContent[] metatitle, LocalizedContent[] metakeywords)
        {
            name = LH.Normalization(name);
            content = LH.Normalization(content);
            metadescription = LH.Normalization(metadescription);
            bottomcontent = LH.Normalization(bottomcontent);
            metatitle = LH.Normalization(metatitle);
            metakeywords = LH.Normalization(metakeywords);
            model.Name = name.Serialize();
            model.Content = content.Serialize();
            model.BottomContent = bottomcontent.Serialize();
            model.MetaDescription = metadescription.Serialize();
            model.MetaTitle = metatitle.Serialize();
            model.MetaKeywords = metakeywords.Serialize();
            if (model.Name!=null&&model.StringKey!=null)
            {
                var page = new Page
                {
                    Name = model.Name,
                    StringKey = model.StringKey,
                    Content = model.Content,
                    BottomContent = model.BottomContent,
                    MetaTitle = model.MetaTitle,
                    MetaKeywords = model.MetaKeywords,
                    MetaDescription = model.MetaDescription,
                    Created = DateTime.UtcNow
                };
                _pageService.Add(page);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var page = _pageService.GetById(id);
            if (page == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            var model = PageViewModel.FromDomainModel(page);
            return View(model);
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Edit(int id, PageViewModel model, LocalizedContent[] name, LocalizedContent[] content, LocalizedContent[] bottomcontent, LocalizedContent[] metadescription, LocalizedContent[] metatitle, LocalizedContent[] metakeywords)
        {
            name = LH.Normalization(name);
            content = LH.Normalization(content);
            metadescription = LH.Normalization(metadescription);
            bottomcontent = LH.Normalization(bottomcontent);
            metatitle = LH.Normalization(metatitle);
            metakeywords = LH.Normalization(metakeywords);
            content = LH.Normalization(content);
            bottomcontent = LH.Normalization(bottomcontent);
            model.Name = name.Serialize();
            model.Content = content.Serialize();
            model.BottomContent = bottomcontent.Serialize();
            model.MetaDescription = metadescription.Serialize();
            model.MetaTitle = metatitle.Serialize();
            model.MetaKeywords = metakeywords.Serialize();
            if (model.Name != null && model.StringKey != null)
            {
                _pageService.Update(id, page =>
                {
                    page.Name = model.Name;

                    if (!page.IsSection)
                    {
                        page.StringKey = model.StringKey;
                    }

                    page.Content = model.Content;
                    page.BottomContent = model.BottomContent;
                    page.MetaTitle = model.MetaTitle;
                    page.MetaKeywords = model.MetaKeywords;
                    page.MetaDescription = model.MetaDescription;

                    page.Updated = DateTime.UtcNow;
                });
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            _pageService.Delete(id);
            return RedirectToAction("Index");
        }
    }
}