﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DomainModel.Content;
using Interfaces.Content;
using OrangeJetpack.Localization;
using WebSite.Areas.Admin.Models.Content;

namespace WebSite.Areas.Admin.Controllers
{
    public class LocalizationController : Controller
    {

         private readonly ILocalizationService _localizationService;


        public LocalizationController(ILocalizationService localizationService)
        {
            _localizationService = localizationService;

        }
        //
        // GET: /Admin/Localization/
        public ActionResult Index()
        {
            var localSrtings = _localizationService.GetAll().ToList();
            var model = localSrtings.Select(LocalizationViewModel.FromDomainModel).ToList();
            return View(model);
        }

        [HttpGet]
        public ActionResult Add()
        {
            var model = new LocalizationViewModel();
            model.Name = "[{\"k\":\"en\",\"v\":\"\"},{\"k\":\"ru\",\"v\":\"\"}]";
            return View(model);
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Add(LocalizationViewModel model, LocalizedContent[] name)
        {
            model.Name = name.Serialize();
            if (model.StringKey != null && model.Name != null)
            {
                var localString = new Localization()
                {
                    StringKey = model.StringKey,
                    Name = model.Name
                };
                _localizationService.Add(localString);
                return RedirectToAction("Index");

            }
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var localString = _localizationService.GetById(id);
            var model = LocalizationViewModel.FromDomainModel(localString);
            return View(model);
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Edit(LocalizationViewModel model, int id, LocalizedContent[] name)
        {
            model.Name = name.Serialize();
            if (model.Name != null)
            {
                _localizationService.Update(id, localiz =>
                {
                    localiz.Name = model.Name;
                    localiz.StringKey = model.StringKey;
                });
                return RedirectToAction("Index");
            }
            return View(model);
        }
    }
}