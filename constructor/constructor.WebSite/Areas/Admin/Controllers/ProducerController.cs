﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DomainModel.Content;
using Interfaces.Content;
using WebSite.Areas.Admin.Models.Content;
using WebSite.Areas.Admin.Models.Common;
using WebSite.Framework;
using System.Web;

namespace WebSite.Areas.Admin.Controllers
{
    public class ProducerController : AdminBaseController
    {
        private readonly IProducerService _ProducerService;

        public ProducerController(IProducerService ProducerService)
        {
            _ProducerService = ProducerService;
        }

        public ActionResult Index()
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            var Producers = _ProducerService.GetAll();
            var model = Producers.Select(ProducerViewModel.FromDomainModel).ToList();

            return View(model);
        }

        [HttpGet]
        public ActionResult Add()
        {
            var model = new ProducerViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Add(ProducerViewModel model)
        {
            if (ModelState.IsValid)
            {
                var fileName = Guid.NewGuid() + ".jpg";
                var path = Server.MapPath(WorkContext.ProducerPath + fileName);
                model.File.SaveAs(path);

                var Producer = new Producer
                {
                    Title = model.Title,
                    Description = model.Description,
                    FullDescription = model.FullDescription,
                    Image = fileName,
                    Url = Transliteration.Front(model.Title),
                    Created = DateTime.UtcNow
                };

                if (_ProducerService.ExistUrl(Producer.Url))
                {
                    Random rnd = new Random();
                    Producer = new Producer
                    {
                        Title = model.Title,
                        Description = model.Description,
                        FullDescription = model.FullDescription,
                        Image = fileName,
                        Url = Transliteration.Front(model.Title) + rnd.Next(0, 999).ToString(),
                        Created = DateTime.UtcNow
                    };
                }
                _ProducerService.Add(Producer);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var Producer = _ProducerService.GetById(id);
            if (Producer == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            var model = ProducerViewModel.FromDomainModel(Producer);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(int id, ProducerViewModel model)
        {
            if (ModelState.IsValid)
            {
                _ProducerService.Update(id, page =>
                {
                    page.Title = model.Title;
                    page.Description = model.Description;
                    page.FullDescription = model.FullDescription;
                    page.Image = model.Image;
                    if (model.File != null)
                    {
                        var fileName = model.Image;
                        var path = Server.MapPath(WorkContext.ProducerPath + fileName);
                        model.File.SaveAs(path);
                        page.Image = fileName;
                    }
                    page.Url = model.Url;
                    page.Updated = DateTime.UtcNow;
                });
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            _ProducerService.Delete(id);
            _ProducerService.DeleteProductsAll(id);
            return RedirectToAction("Index");
        }
    }
}