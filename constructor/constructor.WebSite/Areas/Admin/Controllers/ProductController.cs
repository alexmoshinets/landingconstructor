﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DomainModel.Content;
using Interfaces.Content;
using WebSite.Areas.Admin.Models.Content;
using WebSite.Areas.Admin.Models.Common;
using WebSite.Framework;
using System.Globalization;
using System.IO;
using OrangeJetpack.Localization;
using WebSite.Areas.Admin.Framework.HtmlHelpers;

namespace WebSite.Areas.Admin.Controllers
{
    public class ProductController : AdminBaseController
    {
        private readonly IProductService _productionService;
        private readonly ICategoryService _categoryService;

        public ProductController(IProductService productionService, ICategoryService categoryService)
        { 
            _productionService = productionService;
            _categoryService = categoryService;

        }

        [HttpGet]
        public ActionResult Index()
        {
            var categories = _categoryService.GetAll();
            var model = categories.Select(CategoryViewModel.FromDomainModel).ToList();
            return View(model);
        }


        [HttpGet]
        public ActionResult Add(int id)
        {
            var model = new ProductViewModel();
            var categories = _categoryService.GetAll();
            foreach(var item in categories){
                model.Categories.Add(new SelectListItem{ Text = LH.GLSS(item.Title), Value = item.Id.ToString() });
            }
            model.CategoryId = id.ToString();
            model.Title = "[{\"k\":\"en\",\"v\":\"\"},{\"k\":\"ru\",\"v\":\"\"}]";
            model.Description = "[{\"k\":\"en\",\"v\":\"\"},{\"k\":\"ru\",\"v\":\"\"}]";
            return View(model);
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Add(ProductViewModel model, LocalizedContent[] title, LocalizedContent[] description)
        {
            title = LH.Normalization(title);
            description = LH.Normalization(description);
            model.Title = title.Serialize();
            model.Description = description.Serialize();
            if (model.Title != null && model.Description != null)
            {

                var fileName= "no-image.jpg";
                if (model.File != null)
                {
                    fileName = Guid.NewGuid() + Path.GetExtension(model.File.FileName);
                    var path = Server.MapPath(WorkContext.ProductPath + fileName);
                    model.File.SaveAs(path);
                }
                var product = new Product
                {
                    Title = model.Title,
                    Description = model.Description,
                    Image = fileName,
                    CategoryId = Int32.Parse(model.CategoryId),
                    StringKey = model.StringKey,
                    IsHidden = model.IsHidden,
                    Price = model.Price,
                    Created = DateTime.UtcNow
                };
                _productionService.Add(product);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var product = _productionService.GetById(id);
            if (product == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            var model = ProductViewModel.FromDomainModel(product);
            var categories = _categoryService.GetAll();
            foreach (var item in categories)
            {
                if (item.Id.ToString() == model.CategoryId)
                {
                    model.Categories.Add(new SelectListItem { Text = LH.GLSS(item.Title), Value = item.Id.ToString() , Selected=true});
                }
                else {
                    model.Categories.Add(new SelectListItem { Text = LH.GLSS(item.Title), Value = item.Id.ToString() });
                }
            }         
            return View(model);
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Edit(int id, ProductViewModel model, LocalizedContent[] title, LocalizedContent[] description)
        {
            title = LH.Normalization(title);
            description = LH.Normalization(description);
            description = LH.Normalization(description);
            model.Title = title.Serialize();
            model.Description = description.Serialize();
            if (model.Title != null && model.Description != null)
            {
                _productionService.Update(id, product =>
                {
                    product.Title = model.Title;
                    product.Description = model.Description;
                    product.StringKey = model.StringKey;
                    product.IsHidden = model.IsHidden;
                    product.Price = model.Price;
                    product.Image = model.Image;
                    if (model.File != null)
                    {
                        var fileName = model.Image;
                        if (fileName == "no-image.jpg")
                            fileName = Guid.NewGuid() +Path.GetExtension(model.File.FileName); ;
                        var path = Server.MapPath(WorkContext.ProductPath + fileName);
                        model.File.SaveAs(path);
                        product.Image = fileName;
                    }
                    product.Updated = DateTime.UtcNow;
                });
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            var filename = Server.MapPath(WorkContext.ProductPath + _productionService.Delete(id).ToString());
            if (filename != null && System.IO.File.Exists(filename))
            {
                System.IO.File.Delete(filename);
            }

            return RedirectToAction("Index");
        }

    }

}