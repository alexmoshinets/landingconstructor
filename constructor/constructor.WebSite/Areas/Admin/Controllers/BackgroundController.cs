﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebSite.Areas.Admin.Models.Content;
using WebSite.Framework;

namespace WebSite.Areas.Admin.Controllers
{
    public class BackgroundController : Controller
    {
        //
        // GET: /Admin/Background/
        public ActionResult Index()
        {
            var model = new BackgroundViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult EditBackground(BackgroundViewModel model)
        {
            if (model.File != null)
            {
                var path = Server.MapPath(WorkContext.FilesPath + "background.png");
                model.File.SaveAs(path);                                              
            }
            return RedirectToAction("Index");
        }
	}
}