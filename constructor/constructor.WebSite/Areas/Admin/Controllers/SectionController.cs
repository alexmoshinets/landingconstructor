﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DomainModel.Content;
using Interfaces.Content;
using WebSite.Areas.Admin.Models.Content;
using WebSite.Areas.Admin.Models.Common;
using WebSite.Framework;
using Interfaces.Navigation;
using DomainModel.Navigation;
using System.Web;

namespace WebSite.Areas.Admin.Controllers
{
    public class SectionController : AdminBaseController
    {
        private readonly ISectionService _sectionService;
        private readonly IMenuItemService _menuItemService;
        private readonly IMenuService _menuService;

        public SectionController(ISectionService sectionService, IMenuItemService menuItemService, IMenuService menuService)
        {
            _sectionService = sectionService;
            _menuItemService = menuItemService;
            _menuService = menuService;
        }

        public ActionResult Index()
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            var sections = _sectionService.GetAll();
            var model = sections.Select(SectionViewModel.FromDomainModel).ToList();

            return View(model);
        }

        [HttpGet]
        public ActionResult Add()
        {
            var model = new SectionViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Add(SectionViewModel model)
        {
            if (ModelState.IsValid)
            {
                var fileName = Guid.NewGuid() + ".jpg";
                var path = Server.MapPath(WorkContext.SectionPath + fileName);
                model.File.SaveAs(path);

                var section = new CategoryServices
                {
                    IsHidden = model.IsHidden,
                    Title = model.Title,
                    Description = model.Description,
                    FullDescription = model.FullDescription,
                    Image = fileName,
                    Url = Transliteration.Front(model.Title),
                    Created = DateTime.UtcNow
                };

                //var menu = _menuItemService.Ge("TopMenu");
                //var item_menu = new MenuItem
                //{
                //    Name = section.Title,
                //    MenuId = topMenu.Id,
                //    CustomUrl = "/Section/" + section.Url,
                //    Created = DateTime.UtcNow
                //};


                _sectionService.Add(section);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var section = _sectionService.GetById(id);
            if (section == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            var model = SectionViewModel.FromDomainModel(section);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(int id, SectionViewModel model)
        {
            if (ModelState.IsValid)
            {
                _sectionService.Update(id, page =>
                {
                    page.IsHidden = model.IsHidden;
                    page.Title = model.Title;
                    page.Description = model.Description;
                    page.FullDescription = model.FullDescription;
                    page.Image = model.Image;
                    if (model.File != null)
                    {
                        var fileName = model.Image;
                        var path = Server.MapPath(WorkContext.SectionPath + fileName);
                        model.File.SaveAs(path);
                        page.Image = fileName;
                    }
                    page.Url = model.Url;
                    page.Updated = DateTime.UtcNow;
                });
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            _sectionService.Delete(id);
            _sectionService.DeleteProductsAll(id);
            return RedirectToAction("Index");
        }
    }
}