﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using DomainModel.Content;
using Interfaces.Content;
using WebSite.Areas.Admin.Models.Content;
using WebSite.Framework;

namespace WebSite.Areas.Admin.Controllers
{
    public class ServicesController : AdminBaseController
    {
        private readonly ICategoryServicesService _categoryServicesService;
        private readonly IServicesService _servicesService;

        public ServicesController(IServicesService servicesService, ICategoryServicesService servicesCategoryService)
        {
            _servicesService = servicesService;
            _categoryServicesService = servicesCategoryService;
        }

        public ActionResult Index()
        {
            var categories = _categoryServicesService.GetAll();
            var model = categories.Select(CategoryServicesViewModel.FromDomainModel).ToList();
            return View(model);
        }

        [HttpGet]
        public ActionResult Add(int? id)
        {
            var model = new ServicesViewModel();
            if (id.HasValue)
            {
                model.CategoryId = id.Value;
            }
            model.Created = DateTime.Now;
            model.IsPresence = true;
            UpdateViewModel(model);
            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Add(ServicesViewModel model)
        {
            if (ModelState.IsValid)
            {
                var fileName = Guid.NewGuid() + Path.GetExtension(model.File.FileName);
                var path = Server.MapPath(WorkContext.ServicesPath + fileName);
                if (model.File != null)
                    model.File.SaveAs(path);
                else fileName = "no-image.jpg";

                var service = new Servise
                {
                    IsHidden = model.IsHidden,
                    Title = model.Title,
                    StringKey = model.StringKey,
                    Description = model.Description,
                    FullDescription = model.FullDescription,
                    Image = fileName,
                    Price = model.Price,
                    CategoryId = model.CategoryId,
                    IsPresence = model.IsPresence,
                    Created = DateTime.Now
                };
                _servicesService.Add(service);
                return RedirectToAction("Index");
            }
            UpdateViewModel(model);
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var services = _servicesService.GetById(id);
            if (services == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            var model = ServicesViewModel.FromDomainModel(services);
            model.CategoryId = services.CategoryId;
            UpdateViewModel(model);
            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(int id, ServicesViewModel model)
        {
            if (ModelState.IsValid)
            {
                _servicesService.Update(id, services =>
                {
                    services.IsHidden = model.IsHidden;
                    services.StringKey = model.StringKey;
                    services.Title = model.Title;
                    services.Description = model.Description;
                    services.FullDescription = model.FullDescription;
                    services.Price = model.Price;
                    services.IsPresence = model.IsPresence;
                    services.Updated = DateTime.Now;
                    if (model.File != null)
                    {
                        var fileName = model.Image;
                        if(fileName == "no-image.jpg")
                            fileName = Guid.NewGuid() + Path.GetExtension(model.File.FileName);
                        var path = Server.MapPath(WorkContext.ServicesPath + fileName);
                        model.File.SaveAs(path);
                        services.Image = fileName;
                    }
                });
                return RedirectToAction("Index");
            }
            UpdateViewModel(model);
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            var result = _servicesService.Delete(id);
            if (result !=null)
            {
                SuccessMessage("Статья успешно удалена.");
            }
            else
            {
                ErrorMessage("Не удалось удалить статью, статья не была найдена.");
            }
            return RedirectToAction("Index");
        }

        private void UpdateViewModel(ServicesViewModel model)
        {
            model.CategoryAll = _categoryServicesService.GetAll(_ => new { _.Id, _.Title })
                .Select(
                    _ =>
                        new SelectListItem
                        {
                            Value = _.Id.ToString(CultureInfo.InvariantCulture),
                            Text = _.Title,
                            Selected = (model.CategoryId == _.Id)
                        }).ToList();
        }

        [HttpGet]
        public ActionResult AddCategory()
        {
            var model = new CategoryServicesViewModel();
            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddCategory(CategoryServicesViewModel model)
        {
            if (ModelState.IsValid)
            {
                var services = new CategoryService
                {
                    StringKey = model.StringKey,
                    Title = model.Title,
                    Description = model.Description,
                    Image = model.Image

                };
                if (model.File != null)
                {
                    var fileName = Guid.NewGuid() + Path.GetExtension(model.File.FileName);
                    var path = Server.MapPath(WorkContext.ServicesPath + fileName);
                    model.File.SaveAs(path);
                    services.Image = fileName;
                }
                _categoryServicesService.Add(services);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult EditCategory(int id)
        {
            var services = _categoryServicesService.GetById(id);
            if (services == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            var model = CategoryServicesViewModel.FromDomainModel(services);
            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditCategory(int id, CategoryServicesViewModel model)
        {
            if (ModelState.IsValid)
            {
                _categoryServicesService.Update(id, category =>
                {
                    category.StringKey = model.StringKey;
                    category.Title = model.Title;

                    category.Description = model.Description;
                    if (model.File != null)
                    {
                        var fileName = model.Image ?? Guid.NewGuid() + Path.GetExtension(model.File.FileName);
                        var path = Server.MapPath(WorkContext.ServicesPath + fileName);
                        model.File.SaveAs(path);
                        category.Image = fileName;
                    }
                });
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult DeleteCategory(int id)
        {
            var category = _categoryServicesService.GetById(id);
            if (category != null)
            {
                if (category.Services.Count == 0)
                {
                    var result = _categoryServicesService.Delete(id);
                    if (result)
                    {
                        SuccessMessage("Категория успешно удалена.");
                    }
                    else
                    {
                        ErrorMessage("Не удалось удалить категорию, категория не была найдена.");
                    }
                }
                else
                {
                    ErrorMessage("У данной категории есть статьи, вы должны удалить сперва все статьи.");
                }
            }
            else
            {
                ErrorMessage("Не удалось удалить категорию, категория не была найдена.");
            }
            return RedirectToAction("Index");
        }
    }
}