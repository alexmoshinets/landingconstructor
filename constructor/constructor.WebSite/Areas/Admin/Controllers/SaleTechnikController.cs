﻿using System;
using System.Collections.Generic;
using System.Linq;
using DomainModel.Content;
using Interfaces.Content;
using System.Web;
using System.Web.Mvc;
using WebSite.Areas.Admin.Models.Content;
using WebSite.Framework;
using System.IO;



namespace WebSite.Areas.Admin.Controllers
{
    public class SaleTechnikController : Controller
    {
        private readonly ISaleTechnikService _saleTechnikService;

        public SaleTechnikController(ISaleTechnikService saleTechnikService)
        { 
            _saleTechnikService = saleTechnikService;

        }
        //
        // GET: /Admin/SaleTechnik/
        public ActionResult Index()
        {
            var techniks = _saleTechnikService.GetAll();
            var model = new List<SaleTechnikViewModel>();
            if (techniks != null)
                model = techniks.Select(SaleTechnikViewModel.FromDomainModel).ToList();
            return View(model);
        }
        [HttpGet]
        public ActionResult Add()
        {
            var model = new SaleTechnikViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Add(SaleTechnikViewModel model)
        {
            if (ModelState.IsValid)
            {

                var fileName = Guid.NewGuid() + Path.GetExtension(model.File.FileName);
                var path = Server.MapPath(WorkContext.SaleTechnikPath + fileName);
                if (model.File != null)
                    model.File.SaveAs(path);
                else fileName = "no-image.jpg";

                var technik = new SaleTechniks
                {
                    IsHidden = model.IsHidden,
                    Name = model.Name,
                    Description = model.Description,
                    Image = fileName,
                    Price = model.Price,
                    StringKey = model.StringKey,
                    Created = DateTime.UtcNow
                };
                _saleTechnikService.Add(technik);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var technik = _saleTechnikService.GetById(id);
            if (technik == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            var model = SaleTechnikViewModel.FromDomainModel(technik);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(int id, SaleTechnikViewModel model)
        {
            if (ModelState.IsValid)
            {
                _saleTechnikService.Update(id, technik =>
                {
                    technik.IsHidden = model.IsHidden;
                    technik.Name = model.Name;
                    technik.Price = model.Price;
                    technik.Description = model.Description;
                    if (model.File != null)
                    {
                        var fileName = model.File.FileName;
                        if (fileName == "no-image.jpg")
                            fileName = Guid.NewGuid() + Path.GetExtension(model.File.FileName);
                        var path = Server.MapPath(WorkContext.SaleTechnikPath + fileName);
                        model.File.SaveAs(path);
                        technik.Image = fileName;
                    }

                    technik.StringKey = model.StringKey;
                    technik.Updated = DateTime.UtcNow;
                });
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            _saleTechnikService.Delete(id);
            return RedirectToAction("Index");
        }
	}
}