﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using DomainModel.Content;
using Interfaces.Content;
using WebSite.Areas.Admin.Models.Content;
using OrangeJetpack.Localization;

namespace WebSite.Areas.Admin.Controllers
{
    public class BlockController : AdminBaseController
    {
        private readonly IBlockListService _blockListService;
        private readonly IBlockService _blockService;

        public BlockController(IBlockListService blockListService, IBlockService blockService)
        {
            _blockListService = blockListService;
            _blockService = blockService;
        }

        public ActionResult Index()
        {
            var grouping = _blockService.GetAll().GroupBy(_ => _.BlockList);
            var blockLists = grouping.Select(it => it.Key ?? new BlockList { StringKey = "unbindBlock", Name = "Статические блоки", Blocks = it.ToList() }).ToList();

            var model = blockLists.Select(BlockListViewModel.FromDomainModel).ToList();

            return View(model);
        }

        [HttpGet]
        public ActionResult Add(int? id)
        {
            var model = new BlockViewModel();
            if (id.HasValue)
            {
                model.BlockListId = id.Value;
            }
            UpdateViewModel(model);
            return View(model);
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Add(BlockViewModel model)
        {
            if (!model.IsStatic && !model.BlockListId.HasValue)
            {
                ModelState.AddModelError("BlockListId", "Выберите панель блоков");
            }

            if (ModelState.IsValid)
            {
                var menuItem = new Block
                {
                    IsHidden = model.IsHidden,
                    IsContent = model.IsContent,

                    Title = model.Title,
                    Description = model.Description,

                    BlockListId = model.BlockListId,

                    Created = DateTime.UtcNow
                };
                _blockService.Add(menuItem);
                return RedirectToAction("Index");
            }
            UpdateViewModel(model);
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var item = _blockService.GetById(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            var model = BlockViewModel.FromDomainModel(item);
            UpdateViewModel(model);
            var re = LocalizedContent.Deserialize(model.Description);
            return View(model);
        }


        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Edit(int id, BlockViewModel model, LocalizedContent[] title, LocalizedContent[] description)
        {
            model.Title = title.Serialize();
            model.Description = description.Serialize();
            if (model.Title!=null)
            {
                _blockService.Update(id, _ =>
                {
                    _.IsHidden = model.IsHidden;
                    _.IsContent = model.IsContent;

                    _.Title = model.Title;
                    _.Description = model.Description;

                    _.BlockListId = model.BlockListId;

                    _.Updated = DateTime.UtcNow;
                });
                return RedirectToAction("Index");
            }
            UpdateViewModel(model);
            return View(model);
        }

        private void UpdateViewModel(BlockViewModel model)
        {
            model.BlockListAll = _blockListService.GetAll(_ => new { _.Id, _.Name })
                .Select(_ => new SelectListItem { Value = _.Id.ToString(CultureInfo.InvariantCulture), Text = _.Name, Selected = (model.BlockListId == _.Id) }).ToList();
        }

        public ActionResult Delete(int id)
        {
            _blockService.Delete(id);
            return RedirectToAction("Index");
        }
    }
}