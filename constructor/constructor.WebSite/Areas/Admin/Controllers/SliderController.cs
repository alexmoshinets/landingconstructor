﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DomainModel.Content;
using Interfaces.Content;
using WebSite.Areas.Admin.Models.Content;
using WebSite.Framework;
using System.Web;

namespace WebSite.Areas.Admin.Controllers
{
    public class SliderController : AdminBaseController
    {
        private readonly ISliderService _sliderService;

        public SliderController(ISliderService sliderService)
        {
            _sliderService = sliderService;
        }

        public ActionResult Index()
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            var sliders = _sliderService.GetAll();
            var model = sliders.Select(SliderViewModel.FromDomainModel).ToList();

            return View(model);
        }

        [HttpGet]
        public ActionResult Add()
        {
            var model = new SliderViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Add(SliderViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.File != null)
                {
                    var fileName = model.File.FileName;
                    var path = Server.MapPath(WorkContext.SliderPath + fileName);
                    model.File.SaveAs(path);

                    var slider = new Slider
                    {
                        IsHidden = model.IsHidden,
                        Title = model.Title,
                        Image = fileName,
                        Url = model.Url,
                        Created = DateTime.UtcNow
                    };
                    _sliderService.Add(slider);
                    return RedirectToAction("Index");
                }
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var slider = _sliderService.GetById(id);
            if (slider == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            var model = SliderViewModel.FromDomainModel(slider);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(int id, SliderViewModel model)
        {
            if (ModelState.IsValid)
            {
                _sliderService.Update(id, page =>
                {
                    page.IsHidden = model.IsHidden;
                    page.Title = model.Title;

                    if (model.File != null)
                    {
                        var fileName = model.File.FileName;
                        var path = Server.MapPath(WorkContext.SliderPath + fileName);
                        model.File.SaveAs(path);
                        page.Image = fileName;
                    }

                    page.Url = model.Url;

                    page.Updated = DateTime.UtcNow;
                });
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            _sliderService.Delete(id);
            return RedirectToAction("Index");
        }
    }
}