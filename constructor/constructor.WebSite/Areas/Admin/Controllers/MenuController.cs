﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using DomainModel.Navigation;
using Interfaces.Content;
using Interfaces.Navigation;
using OrangeJetpack.Localization;
using WebSite.Areas.Admin.Models.Navigation;
using WebSite.Framework.Helpers;
using WebSite.Areas.Admin.Framework.HtmlHelpers;


namespace WebSite.Areas.Admin.Controllers
{
    public class MenuController : AdminBaseController
    {
        private readonly IMenuItemService _menuItemService;
        private readonly IPageService _pageService;
        private readonly IMenuService _menuService;

        public MenuController(IMenuItemService menuItemService, IPageService pageService, IMenuService menuService)
        {
            _menuItemService = menuItemService;
            _pageService = pageService;
            _menuService = menuService;
        }

        public ActionResult Index()
        {
            var menuItemsGrouping = _menuItemService.GetAll().GroupBy(_ => _.Menu);
            var menus = new List<Menu>();
            foreach (var item in menuItemsGrouping)
            {
                if (item.Key != null)
                {
                    menus.Add(item.Key);
                }
                else
                {
                    var menu = new Menu { Name = "Не привязанные", Items = item.Where(_ => !_.ParentId.HasValue).ToList() };
                    menus.Add(menu);
                }
            }
            var model = menus.Select(MenuViewModel.FromDomainModel).ToList();
            return View(model);
        }

        [HttpGet]
        public ActionResult Add(int? id, int? parentId)
        {
            var model = new MenuItemViewModel();

            model.MenuId = id;
            model.ParentId = parentId;
            model.Name = "[{\"k\":\"en\",\"v\":\"\"},{\"k\":\"ru\",\"v\":\"\"}]";
            UpdateViewModel(model);
            return View(model);
        }


        [HttpPost]
        public ActionResult Add(MenuItemViewModel model, LocalizedContent[] name)
        {
            name = LH.Normalization(name);
            model.Name = name.Serialize();
            if (model.Name!=null)
            {
                if ((!model.MenuId.HasValue && !model.ParentId.HasValue) || (model.MenuId.HasValue && model.ParentId.HasValue))
                    return HttpNotFound();

                if (model.Type == MenuItemType.Page && !model.PageId.HasValue)
                {
                    ErrorMessage("Укажите страницу");
                    UpdateViewModel(model);
                    return View(model);
                }

                var menuItem = new MenuItem
                {
                    Name = model.Name,

                    IsHidden = model.IsHidden,

                    Type = model.Type,
                    CustomUrl = model.CustomUrl,
                    PageId = model.PageId,

                    MenuId = model.MenuId,
                    ParentId = model.ParentId,

                    Created = DateTime.UtcNow
                };
                _menuItemService.Add(menuItem);

                if (model.ParentId.HasValue)
                    return RedirectToAction("Edit", new { id = model.ParentId.Value });

                return RedirectToAction("Index");
            }

            UpdateViewModel(model);
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var item = _menuItemService.GetById(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            var model = MenuItemViewModel.FromDomainModel(item);
            UpdateViewModel(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(int id, MenuItemViewModel model, LocalizedContent[] name)
        {
            name = LH.Normalization(name);
            model.Name = name.Serialize();
            if (model.Name != null)
            {
                if ((!model.MenuId.HasValue && !model.ParentId.HasValue) || (model.MenuId.HasValue && model.ParentId.HasValue))
                    return HttpNotFound();

                if (model.Type == MenuItemType.Page && !model.PageId.HasValue)
                {
                    ErrorMessage("Укажите страницу");
                    UpdateViewModel(model);
                    return View(model);
                }

                _menuItemService.Update(id, _ =>
                {
                    _.Name = model.Name;

                    _.IsHidden = model.IsHidden;

                    _.Type = model.Type;
                    _.CustomUrl = model.CustomUrl;
                    _.PageId = model.PageId;

                    _.MenuId = model.MenuId;
                    _.ParentId = model.ParentId;

                    _.Updated = DateTime.UtcNow;
                });

                if (model.ParentId.HasValue)
                    return RedirectToAction("Edit", new { id = model.ParentId.Value });

                return RedirectToAction("Index");
            }
            UpdateViewModel(model);
            return View(model);
        }

        private void UpdateViewModel(MenuItemViewModel model)
        {
            model.TypeAll = EnumEditorHtmlHelper.CreateSelectList(typeof(MenuItemType), model.Type.ToString());
            model.PageAll = _pageService.GetAll(_ => new { _.Id, _.Name })
                .Select(_ => new SelectListItem { Value = _.Id.ToString(CultureInfo.InvariantCulture), Text = _.Name, Selected = (model.PageId.HasValue && model.PageId == _.Id) }).ToList();
            model.MenuAll = _menuService.GetAll(_ => new { _.Id, _.Name })
                .Select(_ => new SelectListItem { Value = _.Id.ToString(CultureInfo.InvariantCulture), Text = _.Name, Selected = (model.MenuId.HasValue && model.MenuId == _.Id) }).ToList();
            foreach (var item in model.PageAll)
            {
                item.Text = LH.GLSS(item.Text);
            }
        }

        public ActionResult Delete(int id)
        {
            var item = _menuItemService.GetById(id);

            if (item == null)
            {
                ErrorMessage("Пункт меню не найден");
                return RedirectToAction("Index");
            }

            if (item.Childs != null && item.Childs.Count > 0)
            {
                ErrorMessage("У пункта меню \"" + item.Name + "\" есть подменю, сначала удалите их");
            }
            else
            {
                _menuItemService.Delete(id);
                SuccessMessage("Пункт меню \"" + item.Name + "\" удалён");
            }

            if (item.ParentId.HasValue)
                return RedirectToAction("Edit", new { id = item.ParentId.Value });

            return RedirectToAction("Index");
        }
    }
}