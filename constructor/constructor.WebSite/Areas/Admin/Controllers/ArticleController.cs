﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using DomainModel.Content;
using Interfaces.Content;
using WebSite.Areas.Admin.Models.Content;
using WebSite.Framework;

namespace WebSite.Areas.Admin.Controllers
{
    public class ArticleController : AdminBaseController
    {
        private readonly IArticleCategoryService _articleCategoryService;
        private readonly IArticleService _articleService;

        public ArticleController(IArticleService articleService, IArticleCategoryService articleCategoryService)
        {
            _articleService = articleService;
            _articleCategoryService = articleCategoryService;
        }

        public ActionResult Index()
        {
            var categories = _articleCategoryService.GetAll();
            var model = categories.Select(ArticleCategoryViewModel.FromDomainModel).ToList();
            return View(model);
        }

        [HttpGet]
        public ActionResult Add(int? id)
        {
            var model = new ArticleViewModel();
            if (id.HasValue)
            {
                model.CategoryId = id.Value;
            }
            model.Date = DateTime.Now;
            UpdateViewModel(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult Add(ArticleViewModel model)
        {
            if (ModelState.IsValid)
            {
                var fileName = Guid.NewGuid() + ".jpg";
                var path = Server.MapPath(WorkContext.ArticlePath + fileName);
                if(model.File !=null)
                model.File.SaveAs(path);
                else
                {
                    fileName = "no-image.jpg";
                }

                var article = new Article
                {
                    StringKey = model.StringKey,
                    Date = model.Date,
                    Title = model.Title,
                    Annonse = model.Annonse,
                    Description = model.Description,
                    MetaTitle = model.MetaTitle,
                    MetaKeywords = model.MetaKeywords,
                    MetaDescription = model.MetaDescription,
                    IsHidden = model.IsHidden,
                    CategoryId = model.CategoryId,
                    Created = DateTime.UtcNow,
                    Image= fileName

                };
                _articleService.Add(article);
                return RedirectToAction("Index");
            }
            UpdateViewModel(model);
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var article = _articleService.GetById(id);
            if (article == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            var model = ArticleViewModel.FromDomainModel(article);
            UpdateViewModel(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(int id, ArticleViewModel model)
        {
            if (ModelState.IsValid)
            {
                _articleService.Update(id, article =>
                {
                    article.StringKey = model.StringKey;

                    article.Date = model.Date;

                    article.Title = model.Title;

                    article.Annonse = model.Annonse;
                    article.Description = model.Description;

                    article.MetaTitle = model.MetaTitle;
                    article.MetaKeywords = model.MetaKeywords;
                    article.MetaDescription = model.MetaDescription;

                    article.IsHidden = model.IsHidden;

                    article.CategoryId = model.CategoryId;

                    article.Updated = DateTime.UtcNow;


                    article.Image = model.Image;
                    if (model.File != null)
                    {
                        var fileName = model.Image;
                        if (fileName == "no-image.jpg")
                            fileName = Guid.NewGuid() + ".jpg";
                        var path = Server.MapPath(WorkContext.ArticlePath + fileName);
                        model.File.SaveAs(path);
                        article.Image = fileName;
                    }
                });
                return RedirectToAction("Index");
            }
            UpdateViewModel(model);
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            var result = _articleService.Delete(id);
            if (result)
            {
                SuccessMessage("Статья успешно удалена.");
            }
            else
            {
                ErrorMessage("Не удалось удалить статью, статья не была найдена.");
            }
            return RedirectToAction("Index");
        }

        private void UpdateViewModel(ArticleViewModel model)
        {
            model.CategoryAll = _articleCategoryService.GetAll(_ => new { _.Id, _.Name })
                .Select(
                    _ =>
                        new SelectListItem
                        {
                            Value = _.Id.ToString(CultureInfo.InvariantCulture),
                            Text = _.Name,
                            Selected = (model.CategoryId == _.Id)
                        }).ToList();
        }

        [HttpGet]
        public ActionResult AddCategory()
        {
            var model = new ArticleCategoryViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult AddCategory(ArticleCategoryViewModel model)
        {
            if (ModelState.IsValid)
            {
                var article = new ArticleCategory
                {
                    StringKey = model.StringKey,
                    Name = model.Name,
                    MetaTitle = model.MetaTitle,
                    MetaKeywords = model.MetaKeywords,
                    MetaDescription = model.MetaDescription,
                    Created = DateTime.UtcNow
                };
                _articleCategoryService.Add(article);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult EditCategory(int id)
        {
            var article = _articleCategoryService.GetById(id);
            if (article == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            var model = ArticleCategoryViewModel.FromDomainModel(article);
            return View(model);
        }

        [HttpPost]
        public ActionResult EditCategory(int id, ArticleCategoryViewModel model)
        {
            if (ModelState.IsValid)
            {
                _articleCategoryService.Update(id, category =>
                {
                    category.StringKey = model.StringKey;
                    category.Name = model.Name;

                    category.MetaTitle = model.MetaTitle;
                    category.MetaKeywords = model.MetaKeywords;
                    category.MetaDescription = model.MetaDescription;

                    category.Updated = DateTime.UtcNow;
                });
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult DeleteCategory(int id)
        {
            var category = _articleCategoryService.GetById(id);
            if (category != null)
            {
                if (category.Articles.Count == 0)
                {
                    var result = _articleCategoryService.Delete(id);
                    if (result)
                    {
                        SuccessMessage("Категория успешно удалена.");
                    }
                    else
                    {
                        ErrorMessage("Не удалось удалить категорию, категория не была найдена.");
                    }
                }
                else
                {
                    ErrorMessage("У данной категории есть статьи, вы должны удалить сперва все статьи.");
                }
            }
            else
            {
                ErrorMessage("Не удалось удалить категорию, категория не была найдена.");
            }
            return RedirectToAction("Index");
        }
    }
}