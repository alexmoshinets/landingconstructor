﻿function initDeleteForGalery() {
    $(".del-gallery-pic").unbind("click");

    $(".del-gallery-pic").click(function(e) {
        e.preventDefault();
        var url = $(this).attr("href");
        var $el = $(this);
        $.get(url, function() {
            var $parent = $el.parents("li");
            $parent.fadeOut(400, function() {
                $parent.remove();

                $(".gallery-dynamic").masonry('reload');
            });
        });
    });
}

function initColorboxForGalery() {
    if ($(".colorbox-image").length > 0) {
        $(".colorbox-image").colorbox.remove();

        $(".colorbox-image").colorbox({
            maxWidth: "90%",
            maxHeight: "90%",
            rel: $(this).attr("rel")
        });
    }
}

$(function() {
    initDeleteForGalery();
    initColorboxForGalery();

    $("#main .btn:has(.icon-trash)").click(function() {
        var same = this;
        bootbox.animate(false);
        var string_delete = document.getElementById("string-delete").value
        bootbox.confirm(string_delete, "Отменить", "Да, удалить", function (r) {
            if (r) {
                location.href = same.href;
            }
        });
        return false;
    });
});



(function($) {
    $.extend($.validator.methods, {
        date: function(value, element) {
            return this.optional(element) || /^\d\d?\.\d\d?\.\d\d\d?\d?$/.test(value);
        },
        number: function(value, element) {
            return this.optional(element) || /^-?(?:\d+|\d{1,3}(?: \d{3})+)(?:,\d+)?$/.test(value);
        }
    });
}(jQuery));

function getTranslit(text) {
    var transl = new Array();

    transl['А'] = 'A';
    transl['а'] = 'a';
    transl['Б'] = 'B';
    transl['б'] = 'b';
    transl['В'] = 'V';
    transl['в'] = 'v';
    transl['Г'] = 'G';
    transl['г'] = 'g';
    transl['Д'] = 'D';
    transl['д'] = 'd';
    transl['Е'] = 'E';
    transl['е'] = 'e';
    transl['Ё'] = 'Yo';
    transl['ё'] = 'yo';
    transl['Ж'] = 'Zh';
    transl['ж'] = 'zh';
    transl['З'] = 'Z';
    transl['з'] = 'z';
    transl['И'] = 'I';
    transl['и'] = 'i';
    transl['Й'] = 'J';
    transl['й'] = 'j';
    transl['К'] = 'K';
    transl['к'] = 'k';
    transl['Л'] = 'L';
    transl['л'] = 'l';
    transl['М'] = 'M';
    transl['м'] = 'm';
    transl['Н'] = 'N';
    transl['н'] = 'n';
    transl['О'] = 'O';
    transl['о'] = 'o';
    transl['П'] = 'P';
    transl['п'] = 'p';
    transl['Р'] = 'R';
    transl['р'] = 'r';
    transl['С'] = 'S';
    transl['с'] = 's';
    transl['Т'] = 'T';
    transl['т'] = 't';
    transl['У'] = 'U';
    transl['у'] = 'u';
    transl['Ф'] = 'F';
    transl['ф'] = 'f';
    transl['Х'] = 'X';
    transl['х'] = 'x';
    transl['Ц'] = 'C';
    transl['ц'] = 'c';
    transl['Ч'] = 'Ch';
    transl['ч'] = 'ch';
    transl['Ш'] = 'Sh';
    transl['ш'] = 'sh';
    transl['Щ'] = 'Shh';
    transl['щ'] = 'shh';
    transl['Ъ'] = '';
    transl['ъ'] = '';
    transl['Ы'] = 'Y';
    transl['ы'] = 'y';
    transl['Ь'] = '';
    transl['ь'] = '';
    transl['Э'] = 'E';
    transl['э'] = 'e';
    transl['Ю'] = 'Yu';
    transl['ю'] = 'yu';
    transl['Я'] = 'Ya';
    transl['я'] = 'ya';
    transl['0'] = '0';
    transl['1'] = '1';
    transl['2'] = '2';
    transl['3'] = '3';
    transl['4'] = '4';
    transl['5'] = '5';
    transl['6'] = '6';
    transl['7'] = '7';
    transl['8'] = '8';
    transl['9'] = '9';
    transl[' '] = '-';
    transl['-'] = '-';
    transl['A'] = 'A';
    transl['a'] = 'a';
    transl['B'] = 'B';
    transl['b'] = 'b';
    transl['C'] = 'C';
    transl['c'] = 'c';
    transl['D'] = 'D';
    transl['d'] = 'd';
    transl['E'] = 'E';
    transl['e'] = 'e';
    transl['F'] = 'F';
    transl['f'] = 'f';
    transl['G'] = 'G';
    transl['g'] = 'g';
    transl['H'] = 'H';
    transl['h'] = 'h';
    transl['I'] = 'I';
    transl['i'] = 'i';
    transl['J'] = 'J';
    transl['j'] = 'j';
    transl['K'] = 'K';
    transl['k'] = 'k';
    transl['L'] = 'L';
    transl['l'] = 'l';
    transl['M'] = 'M';
    transl['m'] = 'm';
    transl['N'] = 'N';
    transl['n'] = 'n';
    transl['O'] = 'O';
    transl['o'] = 'o';
    transl['P'] = 'P';
    transl['p'] = 'p';
    transl['Q'] = 'Q';
    transl['q'] = 'q';
    transl['R'] = 'R';
    transl['r'] = 'r';
    transl['S'] = 'S';
    transl['s'] = 's';
    transl['T'] = 'T';
    transl['t'] = 't';
    transl['U'] = 'U';
    transl['u'] = 'u';
    transl['V'] = 'V';
    transl['v'] = 'v';
    transl['W'] = 'W';
    transl['w'] = 'w';
    transl['X'] = 'X';
    transl['x'] = 'x';
    transl['Y'] = 'Y';
    transl['y'] = 'y';
    transl['Z'] = 'Z';
    transl['z'] = 'z';

    var result = '';
    var i;

    for (i = 0; i < text.length; i++) {
        if (transl[text[i]] !== undefined) {
            if (!(result.substr(result.length - 1, 1) == '-' && transl[text[i]] == '-')) {
                result += transl[text[i]];
            }
        }
    }
    if (result == '-')
        result = '';

    if (result.substr(0, 1) == '-')
        result = result.substr(1, result.length);

    if (result.substr(result.length - 1, 1) == '-')
        result = result.substr(0, result.length - 1);

    return result;
}

function translitBind(idSrc, idDest, original) {
    var src = $("#" + idSrc).first();
    var dest = $("#" + idDest).first();

    src.keyup(function() {
        var text = src.val();
        var translit = text;

        if (original == false)
            translit = getTranslit(text);

        dest.val(translit);
    });
}