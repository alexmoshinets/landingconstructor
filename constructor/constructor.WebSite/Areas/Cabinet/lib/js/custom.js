$(function () {
    //$("#country_id").selectbox({
    //    onOpen: function (inst) {
    //        //console.log("open", inst);
    //    },
    //    onClose: function (inst) {
    //        //console.log("close", inst);
    //    },
    //    onChange: function (val, inst) {
    //        $('.active').css('font-family', $('#country_id').val());

    //        if ($('.active').attr('type') == 'button ') { saveButtonOption(); }

    //        if ($('.active').attr('type') == 'timer') {
    //            $('.active .timeTo').css('font-family', $('#country_id').val());
    //        }
    //        groupProperty('font-family', $('#country_id').val());
    //        saveStep();
    //    },
    //    effect: "slide"
    //});

    //$("#country_id_2").selectbox({
    //    onOpen: function (inst) {
    //        //console.log("open", inst);
    //    },
    //    onClose: function (inst) {
    //        //console.log("close", inst);
    //    },
    //    onChange: function (val, inst) {
    //        var style = $('#country_id_2').val();
    //        $('#country_id_2').children().each(function () {
    //            if ($(this).text() != style) {
    //                $(this).removeAttr('selected');
    //            }
    //        });
    //        //$('.active').css('border-style', $('#country_id_2').next().find('.sbSelector').text());
    //        $('.active').css('border-top', parseInt($('.active').css('border-top-width')) + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
    //        groupProperty('border-top', parseInt($('.active').css('border-top-width')) + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
    //        $('.active').css('border-right', parseInt($('.active').css('border-right-width')) + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
    //        groupProperty('border-right', parseInt($('.active').css('border-right-width')) + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
    //        $('.active').css('border-bottom', parseInt($('.active').css('border-bottom-width')) + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
    //        groupProperty('border-bottom', parseInt($('.active').css('border-bottom-width')) + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
    //        $('.active').css('border-left', parseInt($('.active').css('border-left  -width')) + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
    //        groupProperty('border-left', parseInt($('.active').css('border-left  -width')) + 'px ' + style+ ' ' + $('#border_color>div>div>div').css('background-color'));

    //        saveStep();
    //    },
    //    effect: "slide"
    //});

    $("#background_repeat").selectbox({
        onOpen: function (inst) {
            //console.log("open", inst);
        },
        onClose: function (inst) {
            //console.log("close", inst);
        },
        onChange: function (val, inst) {
            var repeat = $('#background_repeat').next().children('.sbSelector').text();
            $('#background_repeat').children().each(function () {
                if ($(this).text() != repeat) {
                    $(this).removeAttr('selected');
                }
            });
            $('.active').css('background-repeat', $('#background_repeat').val());
            if($('.active').attr('type') == 'btn')
            {
                saveButtonOption();
            }            
            saveStep();
        },
        effect: "slide"
    });

    $("#background_option").selectbox({
        onOpen: function (inst) {
            //console.log("open", inst);
        },
        onClose: function (inst) {
            //console.log("close", inst);
        },
        onChange: function (val, inst) {
            var option = $('#background_option').next().children('.sbSelector').text();
            $('#background_option').children().each(function () {
                if ($(this).text() != 'option') {
                    $(this).removeAttr('selected');
                }
            });
            $('.active').css('background-size', $('#background_option').val());
            if ($('.active').attr('type') == 'btn') {
                saveButtonOption();
            }
            saveStep();
        },
        effect: "slide"
    });

    //$("#lighting").selectbox({
    //    onOpen: function (inst) {
    //        //console.log("open", inst);
    //    },
    //    onClose: function (inst) {
    //        //console.log("close", inst);
    //    },
    //    onChange: function (val, inst) {
    //        var ligthing = $('#lighting').next().children('.sbSelector').text();
    //        $('#ligthing').children().each(function () {
    //            if ($(this).text() != ligthing) {
    //                $(this).removeAttr('selected');
    //            }
    //        });

    //        $('.active').css('box-shadow', $('#lighting').val() + ' 0px 0px ' + $('#blurrines_lighting').val() + 'px ' + $('#block_lightings>div>div>div').css('background-color'));
    //        saveStep();
    //    },
    //    effect: "slide"
    //});

    $("#select_animate").selectbox({
        onOpen: function (inst) {
            //console.log("open", inst);
        },
        onClose: function (inst) {
            //console.log("close", inst);
        },
        onChange: function (val, inst) {
            var anim = '';
            var animate = $('#select_animate').val();
            $('#select_animate').children().each(function () {
                if ($(this).val() != animate) {
                    $(this).removeAttr('selected');
                }
            });
            var activeAnimate = $('.active').attr('animate');
            $('.active').removeClass(activeAnimate);
            if (activeAnimate != '' && activeAnimate != undefined) {
                console.log('animate1=' + animate);
                var arrayActiveAnimate = activeAnimate.split(' ');

                if (animate == 'glade' || animate == 'fade' || animate == 'space' || animate == 'bounce' || animate == 'bar') {
                    if (arrayActiveAnimate.length > 2) {
                        if (arrayActiveAnimate[2].indexOf('d') >= 0) {
                            anim = 'ianimate ' + animate + ' ' + arrayActiveAnimate[2];
                        } else {
                            anim = 'ianimate ' + animate + ' d1';
                        }
                    } else {
                        anim = 'ianimate ' + animate + ' d1';
                    }
                    $('#delay-animate').show();
                    $('#duration-animate').hide();
                    $('#appearance-animate').hide();
                } else if (animate == 'flash' || animate == 'swing' || animate == 'pulse' || animate == 'rotate') {
                    if (arrayActiveAnimate.length > 2) {
                        if (arrayActiveAnimate[2].indexOf('s') >= 0) {
                            anim = 'ianimate ' + animate + ' ' + arrayActiveAnimate[2];
                        } else {
                            anim = 'ianimate ' + animate + ' s1';
                        }
                    } else {
                        anim = 'ianimate ' + animate + ' s1';
                    }
                    $('#duration-animate').show();
                    $('#delay-animate').hide();
                    $('#appearance-animate').hide();
                } else if (animate == 'blow' || animate == 'magnet' || animate == 'alert' || animate == 'drop'
                || animate == 'band' || animate == 'blur' || animate == 'tada' || animate == 'shake'
                || animate == 'hang' || animate == 'flip' || animate == 'hang') {
                    $('#appearance-animate').show();
                    $("#select_animate").val();
                    anim = 'ianimate ' + animate + " infinity";
                    $('#duration-animate').hide();
                    $('#appearance-animate').hide();
                    $('#delay-animate').hide();
            }else{
                    $('#appearance-animate').show();
                    $("#select_animate").val();
                    anim = 'ianimate ' + animate + $('#appearance_animate').val();
                    $('#duration-animate').hide();
                    $('#delay-animate').hide();
                    $('#appearance-animate').show();
                }
                console.log('anim=' + anim);
                $('.active').attr('animate', anim);
                $('.active').addClass(anim);
            } else {
                console.log('animate2=' + animate);
                if (animate == 'glade' || animate == 'fade' || animate == 'space' || animate == 'bounce' || animate == 'bar') {
                    anim = 'ianimate ' + animate + ' d1';
                    $('#delay-animate').show();
                    $('#duration-animate').hide();
                    $('#appearance-animate').hide();
                } else if (animate == 'flash' || animate == 'swing' || animate == 'pulse' || animate == 'rotate') {
                    anim = 'ianimate ' + animate + ' s1';
                    $('#duration-animate').show();
                    $('#delay-animate').hide();
                    $('#appearance-animate').hide();
                } else if (animate == 'blow' || animate == 'magnet' || animate == 'alert' || animate == 'drop'
                    || animate == 'band' || animate == 'blur' || animate == 'tada' || animate == 'shake'
                    || animate == 'hang' || animate == 'flip' || animate == 'hang') {
                    anim = 'ianimate ' + animate + " infinity";
                    console.log('ianimate='+anim);
                    $('#duration-animate').hide();
                    $('#delay-animate').hide();
                    $('#appearance-animate').hide();
                } else {
                    anim = 'ianimate ' + animate + $('#appearance_animate').val();
                    $('#duration-animate').hide();
                    $('#delay-animate').hide();
                    $('#appearance-animate').show();
                }
                console.log('anim=' + anim);
                $('.active').attr('animate', anim);
                $('.active').addClass(anim);
            }
            console.log('animanim=' + anim);
            $('iframe#animateIframe').contents().find('#Animate').val(anim);
            $('iframe#animateIframe').contents().find('#animateButton').first().click();

            saveStep();
        },
        effect: "slide"
    });

    $("#delay_animate").selectbox({
        onOpen: function (inst) {
            //console.log("open", inst);
        },
        onClose: function (inst) {
            //console.log("close", inst);
        },
        onChange: function (val, inst) {
            var animateDelay = $('#delay_animate').val();
            var activeAnimate = $('.active').attr('animate');
            $('#delay_animate').children().each(function () {
                if ($(this).text() != animateDelay) {
                    $(this).removeAttr('selected');
                }
            });

            $('.active').removeClass(activeAnimate);

            try {
                var arrayActiveAnimate = activeAnimate.split(' ');
                var anim = arrayActiveAnimate[0] + ' ' + arrayActiveAnimate[1] + ' ' + animateDelay;
                $('.active').addClass(anim);
                $('.active').attr('animate', anim);
                $('iframe#animateIframe').contents().find('#Animate').val(anim);
                $('iframe#animateIframe').contents().find('#animateButton').first().click();
            } catch (ex) {

            }
            saveStep();
        },
        effect: "slide"
    });

    $("#duration_animate").selectbox({
        onOpen: function (inst) {
            //console.log("open", inst);
        },
        onClose: function (inst) {
            //console.log("close", inst);
        },
        onChange: function (val, inst) {
            var animateDuration = $('#duration_animate').val();
            var activeAnimate = $('.active').attr('animate');
            $('#duration_animate').children().each(function () {
                if ($(this).val() != animateDuration) {
                    $(this).removeAttr('selected');
                }
            });

            $('.active').removeClass(activeAnimate);

            try {
                var arrayActiveAnimate = activeAnimate.split(' ');
                var anim = arrayActiveAnimate[0] + ' ' + arrayActiveAnimate[1] + ' ' + animateDuration;
                $('.active').addClass(anim);
                $('.active').attr('animate', anim);
                $('iframe#animateIframe').contents().find('#Animate').val(anim);
                $('iframe#animateIframe').contents().find('#animateButton').first().click();
            } catch (ex) {

            }
            saveStep();
        },
        effect: "slide"
    });

    $("#appearance_animate").selectbox({
        onOpen: function (inst) {
            //console.log("open", inst);
        },
        onClose: function (inst) {
            //console.log("close", inst);
        },
        onChange: function (val, inst) {
            var animateAppearance = $('#appearance_animate').val();
            var activeAnimate = $('.active').attr('animate');
            $('#appearance_animate').children().each(function () {
                if ($(this).val() != animateAppearance) {
                    $(this).removeAttr('selected');
                }
            });

            $('.active').removeClass(activeAnimate);

            var anim = 'ianimate ' + $('#select_animate').val() + $('#appearance_animate').val();
            $('.active').addClass(anim);
            $('.active').attr('animate', anim);
            $('iframe#animateIframe').contents().find('#Animate').val(anim);
            $('iframe#animateIframe').contents().find('#animateButton').first().click();
            saveStep();
        },
        effect: "slide"
    });

    $("#litebox_animate").selectbox({
        onOpen: function (inst) {
            //console.log("open", inst);
        },
        onClose: function (inst) {
            //console.log("close", inst);
        },
        onChange: function (val, inst) {
            var animate =$("#litebox_animate").val();
            $('.active').attr('w-animate', animate);
            $('#litebox_animate').children().each(function () {
                if ($(this).val() != animate) {
                    $(this).removeAttr('selected');
                }
            });
            saveStep();
        },
        effect: "slide"
    });

    $(".nova").selectbox({
        effect: "fade"
    });
    $("#vehicle_id").selectbox({
        speed: 400
    });
    $("#btnOpen").click(function (e) {
        $("#language").selectbox('open');
    });
    $("#btnClose").click(function (e) {
        $("#language").selectbox('close');
    });
    $("#btnAttach").click(function (e) {
        $("#language").selectbox('attach');
    });
    $("#btnDetach").click(function (e) {
        $("#language").selectbox('detach');
    });
    $("#btnEnable").click(function (e) {
        $("#language").selectbox('enable');
    });
    $("#btnDisable").click(function (e) {
        $("#language").selectbox('disable');
    });
});