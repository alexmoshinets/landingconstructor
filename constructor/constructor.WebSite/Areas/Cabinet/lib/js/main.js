﻿//пусть боженька простит меня за ту порнографию, что здесь изложена, аминь
var interval = null;
var resized = false;

var currentNumber = 1;
var totalNumbers = 1;
var landingBackground = false;
var createPage = true;
var stopRecursoin = 0;

$(function () {
    $(document).mousedown(function (event) {
        // Проверяем нажата ли именно правая кнопка мыши:        
        if (event.which === 3 && ($(event.target).closest("#move_block").length)) {            
            $('#contextMenu').css('top', event.clientY + 'px');
            $('#contextMenu').css('left', event.clientX + 'px');
            $('#contextMenu').show();
        }
    })
    $("#lighting").on('change', function () {
        var ligthing = $('#lighting option:selected').val();
        //$('#lighting option').removeAttr('selected');
        //$('#lighting').find('[value="' + ligthing + '"]').attr('selected', 'selected');
        //$('#lighting').val(style);
        $('.active').css('box-shadow', ligthing + ' 0px 0px ' + $('#blurrines_lighting').val() + 'px ' + $('#block_lightings>div>div>div').css('background-color'));
        saveStep();
    });

    $("#textarea_text_element").on('change', function () {
        $('.active>a').text($("#textarea_text_element").val());
        $('.active>a').css('opacity', 0);
        //$('#textarea_text_element').hide();
        saveStep();
    });
    $("#textarea_text_element").keypress(function (key) {
        var char = getChar(key);       
        $('.active>a').text($("#textarea_text_element").val() + char);
        correctSizeTextElement();
        //resizeArea();
    });

    function getChar(event) {
        if (event.which == null) { // IE
            if (event.keyCode < 32) return null; // спец. символ
            return String.fromCharCode(event.keyCode)
        }

        if (event.which != 0 && event.charCode != 0) { // все кроме IE
            if (event.which < 32) return null; // спец. символ
            return String.fromCharCode(event.which); // остальные
        }

        return null; // спец. символ
    }

    $("#edit_background_color").on('change', function () {
        $('#content').css('background-color', $('#edit_background_color>div>div>div').css('background-color'));
        if ($('.active').attr('type') == 'button' || $('.active').attr('type') == 'btn' || $('.active').attr('type') == 'submit') { saveButtonOption(); }
        saveStep();
    });

    $("#edit_background_color_content").on('change', function () {
        $('#content_standard').css('background-color', $('#edit_background_color_content>div>div>div').css('background-color'));
        if ($('.active').attr('type') == 'button' || $('.active').attr('type') == 'btn' || $('.active').attr('type') == 'submit') { saveButtonOption(); }
        saveStep();
    });

    $("#edit_background_color_grad1").on('change', function () {
        try {
            if ($('.active').css('background').indexOf('linear') >= 0) {
                var param = $('.active').css('background').replace('gradient(', '$').split('$')[1];
                var position = param.split(',')[0];
                var color1 = $('#edit_background_color_grad1>div>div>div').css('background-color');
                var color2 = param.replace('), ', '$').split('$')[1].replace('))', '$').split('$')[0] + ')';
                switch (position) {
                    case 'to right bottom':
                        $('.active').css('background', 'linear-gradient(' + position + ', ' + color1 + ', ' + color2 + ')');
                        break;
                    case 'to left bottom':
                        $('.active').css('background', 'linear-gradient(' + position + ', ' + color1 + ', ' + color2 + ')');
                        break;
                    case 'to right':
                        $('.active').css('background', 'linear-gradient(' + position + ', ' + color1 + ', ' + color2 + ')');
                        break;
                    case 'to left':
                        $('.active').css('background', 'linear-gradient(' + position + ', ' + color1 + ', ' + color2 + ')');
                        break;
                    case 'to right top':
                        $('.active').css('background', 'linear-gradient(' + position + ', ' + color1 + ', ' + color2 + ')');
                        break;
                    case 'to top':
                        $('.active').css('background', 'linear-gradient(' + position + ', ' + color1 + ', ' + color2 + ')');
                        break;
                    case 'to left top':
                        $('.active').css('background', 'linear-gradient(' + position + ', ' + color1 + ', ' + color2 + ')');
                        break;
                    default:
                        $('.active').css('background', 'linear-gradient(' + color1 + ', ' + color2 + ')');
                        break;
                }
            } else {
                var colors1 = $('#edit_background_color_grad1>div>div>div').css('background-color');
                $('.active').css('background', colors1);
            }
        } catch (ex) {
            var colors2 = $('#edit_background_color_grad1>div>div>div').css('background-color');
            $('.active').css('background', colors2);
        }
        if ($('.active').attr('type') == 'button' || $('.active').attr('type') == 'btn' || $('.active').attr('type') == 'submit') { saveButtonOption(); }
        saveStep();
    });

    $("#edit_background_color_grad2").on('change', function () {
        try {
            if ($('.active').css('background').indexOf('linear') >= 0) {
                var position = $('.active').css('background-position');
                var temp1 = $('.active').css('background').replace('linear-gradient(', '$').split('$');
                var positions = temp1[1].split(',')[0];
                var color1 = $('#edit_background_color_grad1>div>div>div').css('background-color');
                var color2 = $('#edit_background_color_grad2>div>div>div').css('background-color');
                switch (positions) {
                    case 'to right bottom':
                        $('.active').css('background', 'linear-gradient(to right bottom, ' + color1 + ', ' + color2 + ')');
                        break;
                    case '100% 100%':
                        $('.active').css('background', 'linear-gradient(to right bottom, ' + color1 + ', ' + color2 + ')');
                        break;
                    case 'to left bottom':
                        $('.active').css('background', 'linear-gradient(to left bottom, ' + color1 + ', ' + color2 + ')');
                        break;
                    case '0% 100%':
                        $('.active').css('background', 'linear-gradient(to left bottom, ' + color1 + ', ' + color2 + ')');
                        break;
                    case 'to right':
                        $('.active').css('background', 'linear-gradient(to right, ' + color1 + ', ' + color2 + ')');
                        break;
                    case '100% 50%':
                        $('.active').css('background', 'linear-gradient(to right, ' + color1 + ', ' + color2 + ')');
                        break;
                    case 'to left':
                        $('.active').css('background', 'linear-gradient(to left, ' + color1 + ', ' + color2 + ')');
                        break;
                    case '0% 50%':
                        $('.active').css('background', 'linear-gradient(to left, ' + color1 + ', ' + color2 + ')');
                        break;
                    case 'to right top':
                        $('.active').css('background', 'linear-gradient(to right top, ' + color1 + ', ' + color2 + ')');
                        break;
                    case '100% 0%':
                        $('.active').css('background', 'linear-gradient(to right top, ' + color1 + ', ' + color2 + ')');
                        break;
                    case 'to top':
                        $('.active').css('background', 'linear-gradient(to top, ' + color1 + ', ' + color2 + ')');
                        break;
                    case '50% 0%':
                        $('.active').css('background', 'linear-gradient(to top, ' + color1 + ', ' + color2 + ')');
                        break;
                    case 'to left top':
                        $('.active').css('background', 'linear-gradient(to left top, ' + color1 + ', ' + color2 + ')');
                        break;
                    case '0% 0%':
                        $('.active').css('background', 'linear-gradient(to left top, ' + color1 + ', ' + color2 + ')');
                        break;
                    default:
                        $('.active').css('background', 'linear-gradient(' + color1 + ', ' + color2 + ')');
                        break;
                }
            } else {
                var colors2 = $('#edit_background_color_grad2>div>div>div').css('background-color');
                $('.active').css('background', colors2);
            }
        } catch (ex) {
            var colors2 = $('#edit_background_color_grad2>div>div>div').css('background-color');
            $('.active').css('background', colors2);
        }
        if ($('.active').attr('type') == 'button' || $('.active').attr('type') == 'btn' || $('.active').attr('type') == 'submit') { saveButtonOption(); }
        saveStep();
    });

    $("#border_color").on('change', function () {
        var style = $('#country_id_2 option:selected').val();
        $('.active').css('border', $('#thickness_border').val() + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
        groupProperty('border', $('#thickness_border').val() + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
        if ($('.active').attr('type') == 'button' || $('.active').attr('type') == 'btn' || $('.active').attr('type') == 'submit') { saveButtonOption(); }
        saveStep();
    });


    $("#color_shadow").on('change', function () {
        if ($('.active').attr('type') == 'text') {
            $('.active').children().first().css('text-shadow', $('#x').val() + 'px ' + $('#y').val() + 'px ' + $('#blurrines').val() + 'px ' + $('#color_shadow>div>div>div').css('background-color'));
        } else {
            $('.active span').css('text-shadow', $('#x').val() + 'px ' + $('#y').val() + 'px ' + $('#blurrines').val() + 'px ' + $('#color_shadow>div>div>div').css('background-color'));
        }
        if ($('.active').attr('type') == 'button' || $('.active').attr('type') == 'btn' || $('.active').attr('type') == 'submit') { saveButtonOption(); }
        saveStep();
    });

    $("#block_shadows").on('change', function () {
        $('.active').css('box-shadow', $('#x_shadow').val() + 'px ' + $('#y_shadow').val() + 'px ' + $('#blurrines_shadow').val() + 'px ' + $('#block_shadows>div>div>div').css('background-color'));
        if ($('.active').attr('type') == 'button' || $('.active').attr('type') == 'btn' || $('.active').attr('type') == 'submit') { saveButtonOption(); }
        saveStep();
    });

    $("#block_lightings").on('change', function () {
        $('.active').css('box-shadow', $('#lighting').val() + ' 0px 0px ' + $('#blurrines_lighting').val() + 'px ' + $('#block_lightings>div>div>div').css('background-color'));
        if ($('.active').attr('type') == 'button' || $('.active').attr('type') == 'btn' || $('.active').attr('type') == 'submit') { saveButtonOption(); }
        saveStep();
    });


    $("#country_id_2").on('change', function () {
        var style = $('#country_id_2 option:selected').val();
        //$('.active').css('border', $('#thickness_border').val() + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));

        $('.active').css('border-top', $('#b_top').val() + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
        groupProperty('border-top', $('#b_top').val() + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
        $('.active').css('border-right', $('#b_right').val() + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
        groupProperty('border-right', $('#b_right').val() + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
        $('.active').css('border-bottom', $('#b_bottom').val() + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
        groupProperty('border-bottom', $('#b_bottom').val() + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
        $('.active').css('border-left', $('#b_left').val() + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
        groupProperty('border-left', $('#b_left').val() + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));

        if ($('.active').attr('type') == 'text') {
            correctEditText();
        }        
        saveStep();
    });

    $("#rotation_val").keypress(function (key) {
        if ((key.charCode < 48 || key.charCode > 57)) return false;
    });

    $("#rotation_val").change(function () {
        if ($("#rotation_val").val() == '') {
            $("#rotation_val").val('0');
        }
    });

    $("#page_content_height_val").keypress(function (key) {
        if (((key.charCode < 48 || key.charCode > 57) && key.charCode !== 46) ||
            ($("#page_content_height_val").val().indexOf('.') >= 0 && key.charCode === 46)) return false;
    });

    $("#page_content_height_val").change(function () {
        if ($("#page_content_height_val").val() == '') {
            $("#page_content_height_val").val('300');
        }
    });

    $("#page_content_width_val").keypress(function (key) {
        if (((key.charCode < 48 || key.charCode > 57) && key.charCode !== 46) ||
            ($("#page_content_width_val").val().indexOf('.') >= 0 && key.charCode === 46)) return false;
        $('#content_width_val').val($('#page_content_width_val').val());
    });

    $("#page_content_width_val").change(function () {
        if ($("#page_content_width_val").val() == '') {
            if ($("#page_content_width_val").next().text() == '%') {
                $("#page_content_width_val").val('95');
            } else {
                $("#page_content_width_val").val('960');
            }
        }
        $("#content_width_val").val($("#page_content_width_val").val());
        $("#content_width_val").next().text($("#page_content_width_val").next().text());
    });

    $("#page_content_column_val").keypress(function (key) {
        if (((key.charCode < 48 || key.charCode > 57) && key.charCode !== 46) ||
            ($("#page_content_column_val").val().indexOf('.') >= 0 && key.charCode === 46)) return false;
    });

    $("#page_content_column_val").change(function () {
        if ($("#page_content_column_val").val() == '') {
            $("#page_content_column_val").val('1');
        }
    });

    $("#font_s").keyup(function () {
        var units = 'px';
        if ($("#font_s").next().text() == '%') {
            units = '%';
        }
        $('.active').css('font-size', $("#font_s").val() + units);
        $("#panel_font_s").val($("#font_s").val());
        $("#panel_font_s").next().text(units);
        if ($('.active').attr('type') == 'timer') {
            $('.active  div').css('font-size', $("#font_s").val() + units);
            $('.active  span').css('font-size', $("#font_s").val() + units);
        }
        correctSizeTextElement();
        groupProperty('font-size', $("#font_s").val() + units);
    });
    $("#font_s").keypress(function (key) {
        if (((key.charCode < 48 || key.charCode > 57) && key.charCode !== 46) ||
            ($("#font_s").val().indexOf('.') >= 0 && key.charCode === 46)) return false;
        $("#panel_font_s").val($("#font_s").val());
        correctSizeTextElement();
    });
    $("#font_s").change(function () {
        if ($("#font_s").val() == '') {
            $("#panel_font_s").val(parseInt($('.active').css('font-size')));
            $("#font_s").val(parseInt($('.active').css('font-size')));
            correctSizeTextElement();
        }
    });

    $("#panel_font_s").keyup(function () {
        var units = 'px';
        if ($("#panel_font_s").next().text() == '%') {
            units = '%';
        }
        $('.active').css('font-size', $("#panel_font_s").val() + units);
        $("#font_s").val($("#panel_font_s").val());
        $("#font_s").next().text(units);
        if ($('.active').attr('type') == 'timer') {
            $('.active  div').css('font-size', $("#panel_font_s").val() + units);
            $('.active  span').css('font-size', $("#panel_font_s").val() + units);
        }
        correctSizeTextElement();
        groupProperty('font-size', $("#panel_font_s").val() + units);
    });
    $("#panel_font_s").keypress(function (key) {
        if (((key.charCode < 48 || key.charCode > 57) && key.charCode !== 46) ||
            ($("#panel_font_s").val().indexOf('.') >= 0 && key.charCode === 46)) return false;
        $("#font_s").val($("#panel_font_s").val());
        correctSizeTextElement();
    });
    $("#panel_font_s").change(function () {
        if ($("#panel_font_s").val() == '') {
            $("#panel_font_s").val(parseInt($('.active').css('font-size')));
            $("#font_s").val(parseInt($('.active').css('font-size')));
        }
        correctSizeTextElement();
    });

    $("#letter_s").keypress(function (key) {
        if (((key.charCode < 48 || key.charCode > 57) && key.charCode !== 46) ||
            ($("#letter_s").val().indexOf('.') >= 0 && key.charCode === 46)) return false;
    });
    $("#letter_s").keyup(function () {
        $('.active').css('letter-spacing', $("#letter_s").val() + 'px');
        correctSizeTextElement();
        groupProperty('letter-spacing', $("#letter_s").val() + 'px');
        saveStep();
    });
    $("#letter_s").on('change', function () {
        if ($("#letter_s").val() == '') {
            $("#letter_s").val(parseInt($('.active').css('letter-spacing')));
            correctSizeTextElement();
        }
    });

    $("#line_h").keypress(function (key) {
        if (((key.charCode < 48 || key.charCode > 57) && key.charCode !== 46) ||
            ($("#line_h").val().indexOf('.') >= 0 && key.charCode === 46)) return false;
    });
    $("#line_h").keyup(function () {
        var units = 'px';
        if ($("#line_h").next().text() == '%') {
            units = '%';
        }
        $('.active').css('line-height', $("#line_h").val() + units);
        correctSizeTextElement();
        groupProperty('line-height', $("#line_h").val() + units);
        saveStep();
    });
    $("#line_h").change(function () {
        if ($("#line_h").val() == '') {
            $("#line_h").val(parseInt($('.active').css('line-height')));
            correctSizeTextElement();
        }
    });

    $("#text_i").keypress(function (key) {
        if (((key.charCode < 48 || key.charCode > 57) && key.charCode !== 46) ||
            ($("#text_i").val().indexOf('.') >= 0 && key.charCode === 46)) return false;
    });
    $("#text_i").keyup(function () {
        var units = 'px';
        if ($("#text_i").next().text() == '%') {
            units = '%';
        }
        $('.active').css('text-indent', $("#text_i").val() + units);
        correctSizeTextElement()
        groupProperty('text-indent', $("#text_i").val() + units);
        saveStep();
    });
    $("#text_i").change(function () {
        if ($("#text_i").val() == '') {
            $("#text_i").val(parseInt($('.active').css('text-indent')));
            correctSizeTextElement()
        }
    });

    $("#padding_a").keypress(function (key) {
        if (((key.charCode < 48 || key.charCode > 57) && key.charCode !== 46) ||
            ($("#padding_a").val().indexOf('.') >= 0 && key.charCode === 46)) return false;
    });
    $("#padding_a").keyup(function () {
        var units = 'px';
        if ($("#padding_a").next().text() == '%') {
            units = '%';
        }
        $('.active').css('padding', $("#padding_a").val() + units);
        groupProperty('padding', $("#padding_a").val() + units);
        correctSizeTextElement()
        correctPosition();
        saveStep();
    });
    $("#padding_a").change(function () {
        if ($("#padding_a").val() == '') {
            $("#padding_a").val(parseInt($('.active').css('padding')));
            correctSizeTextElement()
        }
    });

    $("#padding_l").keypress(function (key) {
        if (((key.charCode < 48 || key.charCode > 57) && key.charCode !== 46) ||
            ($("#padding_l").val().indexOf('.') >= 0 && key.charCode === 46)) return false;
    });

    $("#padding_l").keyup(function () {
        var units = 'px';
        if ($("#padding_l").next().text() == '%') {
            units = '%';
        }
        $('.active').css('padding-left', $("#padding_l").val() + units);
        correctSizeTextElement()
        groupProperty('padding-left', $("#padding_l").val() + units);
        saveStep();
    });
    $("#padding_l").change(function () {
        if ($("#padding_l").val() == '') {
            $("#padding_l").val(parseInt($('.active').css('padding-left')));
            correctSizeTextElement()
        }
    });

    $("#padding_r").keypress(function (key) {
        if (((key.charCode < 48 || key.charCode > 57) && key.charCode !== 46) ||
            ($("#padding_r").val().indexOf('.') >= 0 && key.charCode === 46)) return false;
    });
    $("#padding_r").keyup(function () {
        var units = 'px';
        if ($("#padding_r").next().text() == '%') {
            units = '%';
        }
        $('.active').css('padding-right', $("#padding_r").val() + units);
        correctSizeTextElement()
        groupProperty('padding-right', $("#padding_r").val() + units);
        saveStep();
    });
    $("#padding_r").change(function () {
        if ($("#padding_r").val() == '') {
            $("#padding_r").val(parseInt($('.active').css('padding-right')));
            correctSizeTextElement()
        }
    });

    $("#padding_t").keypress(function (key) {
        if (((key.charCode < 48 || key.charCode > 57) && key.charCode !== 46) ||
            ($("#padding_t").val().indexOf('.') >= 0 && key.charCode === 46)) return false;
    });
    $("#padding_t").keyup(function () {
        var units = 'px';
        if ($("#padding_t").next().text() == '%') {
            units = '%';
        }
        $('.active').css('padding-top', $("#padding_t").val() + units);
        correctSizeTextElement()
        groupProperty('padding-top', $("#padding_t").val() + units);
        saveStep();
    });
    $("#padding_t").change(function () {
        if ($("#padding_t").val() == '') {
            $("#padding_t").val(parseInt($('.active').css('padding-top')));
            correctSizeTextElement()
        }
    });

    $("#padding_b").keypress(function (key) {
        if (((key.charCode < 48 || key.charCode > 57) && key.charCode !== 46) ||
            ($("#padding_b").val().indexOf('.') >= 0 && key.charCode === 46)) return false;
    });

    $("#padding_b").keyup(function () {
        var units = 'px';
        if ($("#padding_b").next().text() == '%') {
            units = '%';
        }
        $('.active').css('padding-bottom', $("#padding_b").val() + units);
        correctSizeTextElement()
        groupProperty('padding-bottom', $("#padding_b").val() + units);
        saveStep();
    });
    $("#padding_b").change(function () {
        if ($("#padding_b").val() == '') {
            $("#padding_b").val(parseInt($('.active').css('padding-bottom')));
            correctSizeTextElement()
        }
    });

    $("#thickness_border").keypress(function (key) {
        if (((key.charCode < 48 || key.charCode > 57) && key.charCode !== 46) ||
            ($("#thickness_border").val().indexOf('.') >= 0 && key.charCode === 46)) return false;
    });
    $("#thickness_border").keyup(function () {
        $('.active').css('border', $("#thickness_border").val() + 'px');
        groupProperty('border', $("#thickness_border").val() + 'px');
        saveStep();
    });
    $("#thickness_border").change(function () {
        if ($("#thickness_border").val() == '') {
            $("#thickness_border").val(parseInt($('.active').css('border')));
        }
    });

    $("#b_top").keypress(function (key) {
        if (((key.charCode < 48 || key.charCode > 57) && key.charCode !== 46) ||
            ($("#b_top").val().indexOf('.') >= 0 && key.charCode === 46)) return false;
    });
    $("#b_top").keyup(function () {
        $('.active').css('border-top-width', $("#b_top").val() + 'px');
        groupProperty('border-top-width', $("#b_top").val() + 'px');
        saveStep();
    });
    $("#b_top").change(function () {
        if ($("#b_top").val() == '') {
            $("#b_top").val(parseInt($('.active').css('border-top-width')));
        }
    });

    $("#b_left").keypress(function (key) {
        if (((key.charCode < 48 || key.charCode > 57) && key.charCode !== 46) ||
            ($("#b_left").val().indexOf('.') >= 0 && key.charCode === 46)) return false;
    });
    $("#b_left").keyup(function () {
        $('.active').css('border-left-width', $("#b_left").val() + 'px');
        groupProperty('border-left-width', $("#b_left").val() + 'px');
        saveStep();
    });
    $("#b_left").change(function () {
        if ($("#b_left").val() == '') {
            $("#b_left").val(parseInt($('.active').css('border-left-width')));
        }
    });

    $("#b_right").keypress(function (key) {
        if (((key.charCode < 48 || key.charCode > 57) && key.charCode !== 46) ||
            ($("#b_right").val().indexOf('.') >= 0 && key.charCode === 46)) return false;
    });
    $("#b_right").keyup(function () {
        $('.active').css('border-right-width', $("#b_right").val() + 'px');
        groupProperty('border-right-width', $("#b_right").val() + 'px');
        saveStep();
    });
    $("#b_right").change(function () {
        if ($("#b_right").val() == '') {
            $("#b_right").val(parseInt($('.active').css('border-right-width')));
        }
    });

    $("#b_bottom").keypress(function (key) {
        if (((key.charCode < 48 || key.charCode > 57) && key.charCode !== 46) ||
            ($("#b_bottom").val().indexOf('.') >= 0 && key.charCode === 46)) return false;
    });
    $("#b_bottom").keyup(function () {
        $('.active').css('border-bottom-width', $("#b_bottom").val() + 'px');
        groupProperty('border-bottom-width', $("#b_bottom").val() + 'px');
        saveStep();
    });
    $("#b_bottom").change(function () {
        if ($("#b_bottom").val() == '') {
            $("#b_bottom").val(parseInt($('.active').css('border-bottom-width')));
        }
    });

    $("#border_radiusi").keypress(function (key) {
        if (((key.charCode < 48 || key.charCode > 57) && key.charCode !== 46) ||
            ($("#border_radiusi").val().indexOf('.') >= 0 && key.charCode === 46)) return false;
    });
    $("#border_radiusi").keyup(function () {
        $('.active').css('border-radius', $("#border_radiusi").val() + 'px');
        groupProperty('border-radius', $("#border_radiusi").val() + 'px');
        saveStep();
    });
    $("#border_radiusi").change(function () {
        if ($("#border_radiusi").val() == '') {
            $("#border_radiusi").val(parseInt($('.active').css('border-radius')));
        }
    });

    $("#br_t_l").keypress(function (key) {
        if (((key.charCode < 48 || key.charCode > 57) && key.charCode !== 46) ||
            ($("#br_t_l").val().indexOf('.') >= 0 && key.charCode === 46)) return false;
    });
    $("#br_t_l").keyup(function () {
        $('.active').css('border-top-left-radius', $("#br_t_l").val() + 'px');
        groupProperty('border-top-left-radius', $("#br_t_l").val() + 'px');
        saveStep();
    });
    $("#br_t_l").change(function () {
        if ($("#br_t_l").val() == '') {
            $("#br_t_l").val(parseInt($('.active').css('border-top-left-radius')));
        }
    });

    $("#br_t_r").keypress(function (key) {
        if (((key.charCode < 48 || key.charCode > 57) && key.charCode !== 46) ||
            ($("#br_t_r").val().indexOf('.') >= 0 && key.charCode === 46)) return false;
    });
    $("#br_t_r").keyup(function () {
        $('.active').css('border-top-right-radius', $("#br_t_r").val() + 'px');
        groupProperty('border-top-right-radius', $("#br_t_r").val() + 'px');
        saveStep();
    });
    $("#br_t_r").change(function () {
        if ($("#br_t_r").val() == '') {
            $("#br_t_r").val(parseInt($('.active').css('border-top-right-radius')));
        }
    });

    $("#br_b_l").keypress(function (key) {
        if (((key.charCode < 48 || key.charCode > 57) && key.charCode !== 46) ||
            ($("#br_b_l").val().indexOf('.') >= 0 && key.charCode === 46)) return false;
    });
    $("#br_b_l").keyup(function () {
        $('.active').css('border-bottom-left-radius', $("#br_b_l").val() + 'px');
        groupProperty('border-bottom-left-radius', $("#br_b_l").val() + 'px');
        saveStep();
    });
    $("#br_b_l").change(function () {
        if ($("#br_b_l").val() == '') {
            $("#br_b_l").val(parseInt($('.active').css('border-bottom-left-radius')));
        }
    });

    $("#br_b_r").keypress(function (key) {
        if (((key.charCode < 48 || key.charCode > 57) && key.charCode !== 46) ||
            ($("#br_b_r").val().indexOf('.') >= 0 && key.charCode === 46)) return false;
    });
    $("#br_b_r").keyup(function () {
        $('.active').css('border-bottom-right-radius', $("#br_b_r").val() + 'px');
        groupProperty('border-bottom-right-radius', $("#br_b_r").val() + k + 'px');
        saveStep();
    });
    $("#br_b_r").change(function () {
        if ($("#br_b_r").val() == '') {
            $("#br_b_r").val(parseInt($('.active').css('border-bottom-right-radius')));
        }
    });

    $("#blurrines").keypress(function (key) {
        if (((key.charCode < 48 || key.charCode > 57) && key.charCode !== 46) ||
            ($("#blurrines").val().indexOf('.') >= 0 && key.charCode === 46)) return false;
    });
    $("#blurrines").keyup(function () {
        $('.active').children().first().css('text-shadow', $('#x').val() + 'px ' + $('#y').val() + 'px ' + $('#blurrines').val() + 'px ' + $('#color_shadow>div>div>div').css('background-color'));
        saveStep();
    });

    $("#x").keypress(function (key) {
        if (((key.charCode < 48 || key.charCode > 57) && key.charCode !== 46) ||
            ($("#x").val().indexOf('.') >= 0 && key.charCode === 46)) return false;
    });

    $("#x").keyup(function () {
        $('.active').children().first().css('text-shadow', $('#x').val() + 'px ' + $('#y').val() + 'px ' + $('#blurrines').val() + 'px ' + $('#color_shadow>div>div>div').css('background-color'));
        saveStep();
    });

    $("#y").keypress(function (key) {
        if (((key.charCode < 48 || key.charCode > 57) && key.charCode !== 46) ||
            ($("#y").val().indexOf('.') >= 0 && key.charCode === 46)) return false;
    });

    $("#y").keyup(function () {
        $('.active').children().first().css('text-shadow', $('#x').val() + 'px ' + $('#y').val() + 'px ' + $('#blurrines').val() + 'px ' + $('#color_shadow>div>div>div').css('background-color'));
        saveStep();
    });

    $("#blurrines_shadow").keypress(function (key) {
        if (((key.charCode < 48 || key.charCode > 57) && key.charCode !== 46) ||
            ($("#blurrines_shadow").val().indexOf('.') >= 0 && key.charCode === 46)) return false;
    });

    $("#blurrines_shadow").keyup(function () {
        $('.active').css('box-shadow', $('#x_shadow').val() + 'px ' + $('#y_shadow').val() + 'px ' + $('#blurrines_shadow').val() + 'px ' + $('#block_shadows>div>div>div').css('background-color'));
        saveStep();
    });

    $("#x_shadow").keypress(function (key) {
        if (((key.charCode < 48 || key.charCode > 57) && key.charCode !== 46) ||
            ($("#x_shadow").val().indexOf('.') >= 0 && key.charCode === 46)) return false;
    });

    $("#x_shadow").keyup(function () {
        $('.active').css('box-shadow', $('#x_shadow').val() + 'px ' + $('#y_shadow').val() + 'px ' + $('#blurrines_shadow').val() + 'px ' + $('#block_shadows>div>div>div').css('background-color'));
        saveStep();
    });

    $("#y_shadow").keypress(function (key) {
        if (((key.charCode < 48 || key.charCode > 57) && key.charCode !== 46) ||
            ($("#y_shadow").val().indexOf('.') >= 0 && key.charCode === 46)) return false;
    });

    $("#y_shadow").keyup(function () {
        $('.active').css('box-shadow', $('#x_shadow').val() + 'px ' + $('#y_shadow').val() + 'px ' + $('#blurrines_shadow').val() + 'px ' + $('#block_shadows>div>div>div').css('background-color'));
        saveStep();
    });

    $("#section_width_val").keypress(function (key) {
        if (((key.charCode < 48 || key.charCode > 57) && key.charCode !== 46) ||
            ($("#section_width_val").val().indexOf('.') >= 0 && key.charCode === 46)) return false;
        $("#panel_section_width_val").val($("#section_width_val").val());
    });

    $("#section_width_val").keyup(function () {
        var units = 'px';
        if ($("#section_width_val").next().text() == '%') {
            units = '%';
        }
        if ($("#section_width_val").val() == '') {
            $("#section_width_val").val(parseInt($('.active').css('width')));
        }
        $('.active').css('width', $('#section_width_val').val() + units);
        $("#panel_section_width_val").val($("#section_width_val").val());
        groupProperty('width', $("#section_width_val").val() + units);
        setPositionResizeBlockSection();
        saveStep();
    });

    $("#panel_section_width_val").keypress(function (key) {
        if (((key.charCode < 48 || key.charCode > 57) && key.charCode !== 46) ||
            ($("#panel_section_width_val").val().indexOf('.') >= 0 && key.charCode === 46)) return false;
        $("#section_width_val").val($("#panel_section_width_val").val());
    });

    $("#panel_section_width_val").keyup(function () {
        var units = 'px';
        if ($("#panel_section_width_val").next().text() == '%') {
            units = '%';
        }
        if ($("#panel_section_width_val").val() == '') {
            $("#panel_section_width_val").val(parseInt($('.active').css('width')));
        }
        $('.active').css('width', $('#panel_section_width_val').val() + units);
        $("#section_width_val").val($("#panel_section_width_val").val());
        groupProperty('width', $("#panel_section_width_val").val() + units);
        setPositionResizeBlockSection();
        saveStep();
    });

    $("#section_height_val").keypress(function (key) {
        if (((key.charCode < 48 || key.charCode > 57) && key.charCode !== 46) ||
            ($("#section_height_val").val().indexOf('.') >= 0 && key.charCode === 46)) return false;
        $('#panel_section_height_val').val($("#section_height_val").val());
    });
    $("#section_height_val").keyup(function () {
        var units = 'px';
        if ($("#section_height_val").next().text() == '%') {
            units = '%';
        }
        $('.active').css('height', $('#section_height_val').val() + units);
        $('#panel_section_height_val').val($("#section_height_val").val());
        groupProperty('height', $("#section_height_val").val() + units);
        try {
            correctSectionHeight("");
        } catch (ex) {

        }
        setPositionResizeBlockSection();
        saveStep();
    });
    $("#section_height_val").change(function () {
        if ($("#section_height_val").val() == '') {
            $("#section_height_val").val(parseInt($('.active').css('height')));
            $('#panel_section_height_val').val($("#section_height_val").val());
        }
    });

    $("#panel_section_height_val").keypress(function (key) {
        if (((key.charCode < 48 || key.charCode > 57) && key.charCode !== 46) ||
            ($("#panel_section_height_val").val().indexOf('.') >= 0 && key.charCode === 46)) return false;
        $('#section_height_val').val($("#panel_section_height_val").val());
    });
    $("#panel_section_height_val").keyup(function () {
        var units = 'px';
        if ($("#panel_section_height_val").next().text() == '%') {
            units = '%';
        }
        $('.active').css('height', $('#panel_section_height_val').val() + units);
        $('#section_height_val').val($("#panel_section_height_val").val());
        groupProperty('height', $("#panel_section_height_val").val() + units);
        try {
            correctSectionHeight("");
        } catch (ex) {

        }
        setPositionResizeBlockSection();
        saveStep();
    });
    $("#panel_section_height_val").change(function () {
        if ($("#panel_section_height_val").val() == '') {
            $("#panel_section_height_val").val(parseInt($('.active').css('height')));
            $('#section_height_val').val($("#panel_section_height_val").val());
        }
    });

    $("#section_top_val").keypress(function (key) {
        if (((key.charCode < 48 || key.charCode > 57) && key.charCode !== 46) ||
            ($("#section_top_val").val().indexOf('.') >= 0 && key.charCode === 46)) return false;
    });
    $("#section_top_val").keyup(function () {
        var units = 'px';
        if ($("#section_top_val").next().text() == '%') {
            units = '%';
        }
        $('.active').css('top', $('#section_top_val').val() + units);
        saveStep();
    });
    $("#section_top_val").change(function () {
        if ($("#section_top_val").val() == '') {
            $("#section_top_val").val(parseInt($('.active').css('top')));
        }
    });

    $("#width_l_val").keypress(function (key) {
        if (((key.charCode < 48 || key.charCode > 57) && key.charCode !== 46) ||
            ($("#width_l_val").val().indexOf('.') >= 0 && key.charCode === 46)) return false;

        saveStep();
    });
    $("#height_l_val").keypress(function (key) {
        if (((key.charCode < 48 || key.charCode > 57) && key.charCode !== 46) ||
            ($("#height_l_val").val().indexOf('.') >= 0 && key.charCode === 46)) return false;

        saveStep();
    });


    $("#section_left_val").keypress(function (key) {
        if (((key.charCode < 48 || key.charCode > 57) && key.charCode !== 46) ||
            ($("#section_left_val").val().indexOf('.') >= 0 && key.charCode === 46)) return false;
    });
    $("#section_left_val").keyup(function () {
        var units = 'px';
        if ($("#section_left_val").next().text() == '%') {
            units = '%';
        }
        $('.active').css('left', $('#section_left_val').val() + units);
        checkPositionCheckbox();
        saveStep();
    });
    $("#section_left_val").change(function () {
        if ($("#section_left_val").val() == '') {
            $("#section_left_val").val(parseInt($('.active').css('left')));
        }
        checkPositionCheckbox();
    });

    function getKeyByCode(code) {
        var key = '';
        switch (code) {
            case 46:
                key = '.';
                break;
            case 48:
                key = '0';
                break;
            case 49:
                key = '1';
                break;
            case 50:
                key = '2';
                break;
            case 51:
                key = '3';
                break;
            case 52:
                key = '4';
                break;
            case 53:
                key = '5';
                break;
            case 54:
                key = '6';
                break;
            case 55:
                key = '7';
                break;
            case 56:
                key = '8';
                break;
            case 57:
                key = '9';
                break;
        }
        return key;
    }

    $('.sellect_sections').draggable({
        distance: 20,
        helper: "clone",
        start: function () {

        },
        drag: function () {
            var elem = $('.ui-draggable-dragging').offset();
            var el = document.elementFromPoint(elem.left - 1, elem.top - 1);

            var sdf = $(el).closest('.section');
            $('.pasteSection').remove();
            var div = document.createElement('div');
            $(div).addClass('pasteSection');
            $(div).attr('X', elem.left - 1);
            $(div).attr('Y', elem.top - 1);
            if ($(sdf).attr('id') != undefined) {
                var posTop = $(sdf).offset().top;
                var posBottom = posTop + $(sdf).height();
                var posReal = elem.top - 1;
                if ((posBottom - posTop) / 2 > (posReal - posTop)) {
                    $(sdf).before(div);
                } else {
                    $(sdf).after(div);
                }
            } else if ($(el).closest('#content') != undefined) {
                $('#content_standard').append(div);
            }
        },
        stop: function (event) {
            //console.log('добавили секцию');
            var elem = $('.ui-draggable-dragging').offset();
            var el = document.elementFromPoint(elem.left - 1, elem.top - 1);
            var content = $(this).attr('rel');
            var sdf = $(el).closest('#content');
            if ($(sdf).attr('id') != undefined) {
                try {
                    var element = document.elementFromPoint(parseInt(x), parseInt(y));
                    if ($('.pasteSection').length) {
                        $('.pasteSection').after($.parseHTML(reHtmlEntities(content)));
                    } else if ($(element).closest('#content') != undefined) {
                        $('#content_standard').append($.parseHTML(reHtmlEntities(content)));
                    }
                } catch (ex) {
                    $('#content_standard').append($.parseHTML(reHtmlEntities(content)));
                }
                $('#content_standard').find('.section').each(function () {
                    if ($(this).attr('id') != 'sectionContent') {
                        $(this).css('width', $('#content_width_val').val() + $('#content_width_val').next().text());
                    }
                });
            } else {

            }
            $('.pasteSection').remove();
            saveStep();
        }
    });



    $('#anchor').draggable({
        distance: 20,
        helper: "clone",
        start: function () {

        },
        stop: function (event) {
            //console.log('добавление якоря');
            $('#set_name_anchor>.litebox_buttons').children().each(function () {
                var elem = $('.ui-draggable-dragging').offset();
                var el = document.elementFromPoint(elem.left - 1, elem.top - 1);
                var element;
                if ($(el).attr('class') == undefined) {
                    element = $(el).closest('.column');
                } else if ($(el).attr('class').indexOf('column') < 0) {
                    element = $(el).closest('.column');
                } else {
                    element = $(el).attr('id');
                }
                try {
                    var test = $('#' + element).attr('id');
                } catch (ex) {
                    return;
                }
                if (element != '') {
                    if ($(this).attr('onclick').indexOf('create') >= 0) {
                        //console.log('меняем функцию');
                        var x = event.clientX;
                        var y = event.clientY;
                        var rand = Math.round((Math.random() * 100000));
                        $(this).attr('onclick', "createAnchor('" + x + "', '" + y + "', '" + element + "', '" + rand + "')");
                        $('#anchor_name').val('якорь' + rand);
                        showLitebox('set_name_anchor');
                    }
                }
            });
        }
    });

    $('#block').draggable({
        distance: 20,
        helper: "clone",
        start: function () {

        },
        stop: function (event) {
            //console.log('добавили пустой блок');
            var elem = $('.ui-draggable-dragging').offset();
            var el = document.elementFromPoint(elem.left - 1, elem.top - 1);
            var element;
            if ($(el).attr('class') == undefined) {
                element = $(el).closest('.column');
            } else if ($(el).attr('class').indexOf('column') < 0) {
                element = $(el).closest('.column');
            } else {
                element = $(el).attr('id');
            }
            try {
                var test = $('#' + element).attr('id');
            } catch (ex) {
                return;
            }
            if (element != '') {
                var rand = Math.round((Math.random() * 100000));
                var idEl = "block_" + rand;
                var div = document.createElement('div');
                div.id = idEl;
                div.className = "ui-widget-content";
                $('#' + element).append(div);
                //var top = (((event.clientY - $('#' + element).offset().top) / $('#' + element).height()) * 100).toFixed(1);
                //var left = (((event.clientX - $('#' + element).offset().left) / $('#' + element).width()) * 100).toFixed(1);
                //var width = ((200 / $('#' + element).width()) * 100).toFixed(1);
                //var height = ((150 / $('#' + element).height()) * 100).toFixed(1);
                //$('#' + idEl).attr('style', ' font-family: Arial; background: rgba(1, 166, 249, 0.8); border: none; border-radius: 0px 0px 0px 0px; line-height: 1; font-size: 16px; color: #000000; position: absolute; width: ' + width + '%; height: ' + height + '%; top:' + top + '%; left: ' + left + '%; z-index:' + counter);
                var top = (event.clientY - $('#' + element).offset().top).toFixed(0);
                var left = (event.clientX - $('#' + element).offset().left).toFixed(0);
                $('#' + idEl).attr('style', ' font-family: Arial; background: rgba(1, 166, 249, 0.8); border: none; border-radius: 0px 0px 0px 0px; line-height: 1; font-size: 16px; color: #000000; position: absolute; width: 200px; height: 150px; top:' + top + 'px; left: ' + left + 'px; z-index:' + counter);
                $('#' + idEl).attr('type', 'block');
                $('#' + idEl).attr('m-title', 'блок_' + rand);
                correctSectionHeight(idEl);
                reload_right_panel();
                counter++;
            }
            saveStep();
        }
    });

    $('#map').draggable({
        distance: 20,
        helper: "clone",
        start: function () {

        },
        stop: function (event) {
            //console.log('добавили карту яндекса');
            var elem = $('.ui-draggable-dragging').offset();
            var el = document.elementFromPoint(elem.left - 1, elem.top - 1);
            var element;
            if ($(el).attr('class') == undefined) {
                element = $(el).closest('.column');
            } else if ($(el).attr('class').indexOf('column') < 0) {
                element = $(el).closest('.column');
            } else {
                element = $(el).attr('id');
            }
            try {
                var test = $('#' + element).attr('id');
            } catch (ex) {
                return;
            }
            if (element != '') {
                var rand = Math.round((Math.random() * 100000));
                var idEl = "map_" + rand;
                var div = document.createElement('div');
                div.id = idEl;
                div.className = "ui-widget-content anchor";

                var img = document.createElement('img');
                $(img).attr('src', '/Areas/Cabinet/Images/map.png');
                $(img).css('width', '100%');
                $(img).css('height', '100%');
                $(img).attr('script', "var map; ymaps.ready(function(){map = new ymaps.Map('map_id', {center: [55.76, 37.64],zoom: 10});});");
                $(div).append(img);

                $('#' + element).append(div);
                var top = (event.clientY - $('#' + element).offset().top).toFixed(0);
                var left = (event.clientX - $('#' + element).offset().left).toFixed(0);
                //var top = (((event.clientY - $('#' + element).offset().top) / $('#' + element).height()) * 100).toFixed(1);
                var leftP = (((event.clientX - $('#' + element).offset().left) / $('#' + element).width()) * 100).toFixed(1);
                var width = ((200 / $('#' + element).width()) * 100).toFixed(1);
                //var height = ((110 / $('#' + element).height()) * 100).toFixed(1);
                //if (parseFloat(top) + parseFloat(height) > 100) {
                //    top = 100 - parseFloat(height);
                //}
                if (parseFloat(leftP) + parseFloat(width) > 100) {
                    left = ((100 - parseFloat(width)) / 100) * $('#' + element).width();
                }
                //$('#' + idEl).attr('style', ' font-family: Arial; overflow: hidden; position: absolute; width: ' + width + '%; height: ' + height + '%; top:' + top + '%; left: ' + left + '%; z-index:' + counter);
                $('#' + idEl).attr('style', ' font-family: Arial; overflow: hidden; position: absolute; width: 200; height: 110; top:' + top + 'px; left: ' + left + 'px; z-index:' + counter);
                $('#' + idEl).attr('type', 'map');
                $('#' + idEl).attr('m-title', 'карта_' + rand);
                correctSectionHeight(idEl);
                reload_right_panel();
                counter++;
            }
            saveStep();
        }
    });

    $('#form').draggable({
        distance: 20,
        helper: "clone",
        start: function () {

        },
        stop: function (event) {
            //console.log('добавили форму');
            var elem = $('.ui-draggable-dragging').offset();
            var el = document.elementFromPoint(elem.left - 1, elem.top - 1);
            var element;
            if ($(el).attr('class') == undefined) {
                element = $(el).closest('.column');
            } else if ($(el).attr('class').indexOf('column') < 0) {
                element = $(el).closest('.column');
            } else {
                element = $(el).attr('id');
            }
            try {
                var test = $('#' + element).attr('id');
            } catch (ex) {
                return;
            }
            if (element != '') {
                var rand = Math.round((Math.random() * 100000));
                var idEl = "form_" + rand;
                var div = document.createElement('div');
                div.id = idEl;
                div.className = "ui-widget-content";

                var divlab = document.createElement('div');
                $(divlab).attr('style', 'position: absolute; left: 20px; top: 20px; height: 25px; width: 80%;');
                $(divlab).attr('type', 'title');
                var callBack = document.createElement('label');
                $(callBack).text('Обратная связь');
                $(callBack).attr('style', 'color: black; font-size: 14px;  font-family: Arial; text-align: center; display: inline-block; height: 25px; width: 100%;');
                $(divlab).append(callBack);
                $(div).append(divlab);

                var div1 = document.createElement('div');
                $(div1).attr('style', 'width: 80%; overflow: hidden; position: absolute; left: 20px; top: 45px;');
                $(div1).attr('class', 'containerInput');
                var label1 = document.createElement('label');
                $(label1).attr('style', 'color: #000000; display: block; width: 100%; font-size: 11px;');
                $(label1).attr('class', idEl + '_label');
                $(label1).text('Фамилия:');
                $(div1).append(label1);
                var input1 = document.createElement('input');
                $(input1).attr('style', 'width: 100%; height: 20px; border: 1px solid #aaa;');
                $(input1).attr('type', 'text');
                $(input1).attr('disabled', 'disabled');
                $(input1).attr('class', idEl + '_param');
                $(input1).attr('id', idEl + '_param_' + 1);
                $(input1).attr('name', 'parametrs');
                $(input1).attr('placeholder', 'Фамилия');
                $(div1).append(input1);
                var validate1 = document.createElement('span');
                $(validate1).attr('style', 'font-size: 10px; color: red; width: 100%; height: 15px; display: none;');
                $(validate1).attr('id', idEl + '_param_' + 1 + '_valid');
                $(validate1).attr('class', idEl + '_param_valid');
                $(validate1).text('* данные не прошли проверку');
                $(div1).append(validate1);
                $(div).append(div1);

                var div2 = document.createElement('div');
                $(div2).attr('class', 'containerInput');
                $(div2).attr('style', 'width: 80%;  overflow: hidden; position: absolute; left: 20px; top: 95px;');
                var label2 = document.createElement('label');
                $(label2).attr('style', 'color: #000000; display: block; width: 100%; font-size: 11px;');
                $(label2).attr('class', idEl + '_label');
                $(label2).text('Имя:');
                $(div2).append(label2);
                var input2 = document.createElement('input');
                $(input2).attr('style', 'width: 100%; height: 20px; border: 1px solid #aaa;');
                $(input2).attr('type', 'text');
                $(input2).attr('disabled', 'disabled');
                $(input2).attr('class', idEl + '_param');
                $(input2).attr('id', idEl + '_param_' + 2);
                $(input2).attr('name', 'parametrs');
                $(input2).attr('placeholder', 'Имя');
                $(div2).append(input2);
                var validate2 = document.createElement('span');
                $(validate2).attr('style', 'font-size: 10px; color: red; width: 100%;height: 15px; display: none;');
                $(validate2).attr('id', idEl + '_param_' + 2 + '_valid');
                $(validate2).attr('class', idEl + '_param_valid');
                $(validate2).text('* данные не прошли проверку');
                $(div2).append(validate2);
                $(div).append(div2);

                var div3 = document.createElement('div');
                $(div3).attr('class', 'containerInput');
                $(div3).attr('style', 'width: 80%; overflow: hidden; left: 20px; top: 145px; position: absolute;');
                var label3 = document.createElement('label');
                $(label3).attr('style', 'color: #000000; display: block; width: 100%; font-size: 11px;');
                $(label3).attr('class', idEl + '_label');
                $(label3).text('Почта:');
                $(div3).append(label3);
                var input3 = document.createElement('input');
                $(input3).attr('style', 'width: 100%; height: 20px; border: 1px solid #aaa;');
                $(input3).attr('type', 'email');
                $(input3).attr('disabled', 'disabled');
                $(input3).attr('class', idEl + '_param');
                $(input3).attr('id', idEl + '_param_' + 3);
                $(input3).attr('name', 'parametrs');
                $(input3).attr('placeholder', 'Адрес эелктронной почты');
                $(div3).append(input3);
                var validate3 = document.createElement('span');
                $(validate3).attr('style', 'font-size: 10px; color: red; width: 100%; height: 15px; display: none;');
                $(validate3).attr('id', idEl + '_param_' + 3 + '_valid');
                $(validate3).attr('class', idEl + '_param_valid');
                $(validate3).text('* данные не прошли проверку');
                $(div3).append(validate3);
                $(div).append(div3);

                var div4 = document.createElement('div');
                $(div4).attr('class', 'containerInput');
                $(div4).attr('style', 'width: 80%; overflow: hidden;  position: absolute; left: 20px; top: 195px;');
                var label4 = document.createElement('label');
                $(label4).attr('style', 'color: #000000; display: block; width: 100%; font-size: 11px;');
                $(label4).attr('class', idEl + '_label');
                $(label4).text('Сообщение:');
                $(div4).append(label4);
                var input4 = document.createElement('textarea');
                $(input4).attr('style', 'width: 100%; height: 70px; overflow-y: auto; border: 1px solid #aaa;');
                $(input4).attr('placeholder', 'Текст сообщения');
                $(input4).attr('disabled', 'disabled');
                $(input4).attr('class', idEl + '_param');
                $(input4).attr('id', idEl + '_param_' + 4);
                $(input4).attr('name', 'parametrs');
                $(div4).append(input4);
                var validate4 = document.createElement('span');
                $(validate4).attr('style', 'font-size: 10px; color: red; width: 100%; height: 15px; display: none;');
                $(validate4).attr('id', idEl + '_param_' + 4 + '_valid');
                $(validate4).attr('class', idEl + '_param_valid');
                $(validate4).text('* данные не прошли проверку');
                $(div4).append(validate4);
                $(div).append(div4);

                var button = document.createElement('div');
                $(button).attr('type', 'submit');
                $(button).attr('id', idEl + '_submit');
                $(button).attr('onclick', 'submitForm("' + idEl + '")');
                $(button).attr('style', 'text-align: center; display: block; left: 50px; top: 295px; font-family: Arial; color: white; position: absolute; background: linear-gradient(rgba(1, 166, 249, 0.8), rgb(37, 136, 147)); width: 100px; height: 20px;');
                var buttonText = document.createElement('p');
                $(buttonText).text('Отправить');
                $(button).append(buttonText);
                $(div).append(button);

                var buttonStyles = document.createElement('p');
                $(buttonStyles).attr('rel', idEl + '_submit');
                $(buttonStyles).text('#' + idEl + '_submit{text-align: center; display: block; font-family: Arial; color: white; position: absolute; background: linear-gradient(rgba(1, 166, 249, 0.8), rgb(37, 136, 147));  width:100px; height:20px; top:295px; left:50.5px;}#' + idEl + '_submit:hover{text-align: center; display: block;   font-family: Arial; color: white; position: absolute; background: linear-gradient(rgba(1, 108, 249, 0.8), rgb(37, 84, 147));  width:100px;height:20px;top:295px;left:50px;}#' + idEl + '_submit:active{text-align: center; display: block; font-family: Arial; color: white; position: absolute; background: linear-gradient(rgba(249, 1, 1, 0.8), rgb(147, 37, 48));    width:100px;height:20px;top:295px;left:50px;}');
                $('#styles').append(buttonStyles);

                $('#' + element).append(div);
                var top = (event.clientY - $('#' + element).offset().top).toFixed(0);
                var left = (event.clientX - $('#' + element).offset().left).toFixed(0);
                //var top = (((event.clientY - $('#' + element).offset().top) / $('#' + element).height()) * 100).toFixed(1);
                var leftP = (((event.clientX - $('#' + element).offset().left) / $('#' + element).width()) * 100).toFixed(1);
                var width = ((200 / $('#' + element).width()) * 100).toFixed(1);
                //if (parseFloat(top) + parseFloat(height) > 100) {
                //    top = 100 - parseFloat(height);
                //}
                if (parseFloat(leftP) + parseFloat(width) > 100) {
                    left = ((100 - parseFloat(width)) / 100) * $('#' + element).width();
                }
                $('#' + idEl).attr('style', 'overflow: hidden;  font-family: Arial; position: absolute; width: 200px; height: 340px; top:' + top + 'px; left: ' + left + 'px; z-index:' + counter);
                $('#' + idEl).attr('type', 'form');
                $('#' + idEl).attr('novalidate', 'novalidate');
                $('#' + idEl).attr('m-title', 'форма_' + rand);
                correctSectionHeight(idEl);
                reload_right_panel();
                counter++;
            }
            saveStep();
        }
    });

    $('#text').draggable({
        distance: 20,
        helper: "clone",
        start: function () {

        },
        stop: function (event) {
            //console.log('добавили текст');
            var elem = $('.ui-draggable-dragging').offset();
            var el = document.elementFromPoint(elem.left - 1, elem.top - 1);
            var element;
            if ($(el).attr('class') == undefined) {
                element = $(el).closest('.column');
            } else if ($(el).attr('class').indexOf('column') < 0) {
                element = $(el).closest('.column');
            } else {
                element = $(el).attr('id');
            }
            try {
                var test = $('#' + element).attr('id');
            } catch (ex) {
                return;
            }

            if (element != '') {
                var rand = Math.round((Math.random() * 100000));
                var idEl = "text_" + rand;
                var div = document.createElement('div');
                div.id = idEl;
                div.className = "ui-widget-content";
                var a = document.createElement('a');
                a.innerHTML = 'Ваш текст';
                $(a).attr('href', '#');
                $(a).attr('style', 'cursor: default; text-decoration: none; width: 150px; height: 25px; font-family: Arial; color: rgba(0,0,0,1); text-shadow: none; box-shadow: none;');
                div.appendChild(a);

                $('#' + element).append(div);
                var top = (event.clientY - $('#' + element).offset().top).toFixed(0);
                var left = (event.clientX - $('#' + element).offset().left).toFixed(0);
                //var top = (((event.clientY - $('#' + element).offset().top) / $('#' + element).height()) * 100).toFixed(1);
                var leftP = (((event.clientX - $('#' + element).offset().left) / $('#' + element).width()) * 100).toFixed(1);
                var width = ((150 / $('#' + element).width()) * 100).toFixed(1);
                //var height = ((25 / $('#' + element).height()) * 100).toFixed(1);

                //if (parseFloat(top) + parseFloat(height) > 100) {
                //    top = 100 - parseFloat(height);
                //}
                //if (parseFloat(left) + parseFloat(width) > 100) {
                //    left = 100 - parseFloat(width);
                //}
                if (parseFloat(leftP) + parseFloat(width) > 100) {
                    left = ((100 - parseFloat(width)) / 100) * $('#' + element).width();
                }
                //$('#' + idEl).attr('style', 'border: none; background: rgba(0,0,0,0);  font-family: Arial; border-radius: 0px 0px 0px 0px; line-height: 1; font-size: 16px; color: #000000; position: absolute; width: ' + width + '%; height: ' + height + '%; top:' + top + '%; left: ' + left + '%; z-index:' + counter);
                $('#' + idEl).attr('style', 'border: none; background: rgba(0,0,0,0);  font-family: Arial; border-radius: 0px 0px 0px 0px; line-height: 1; font-size: 16px; color: #000000; position: absolute; width: 150px; height: 25px; top:' + top + 'px; left: ' + left + 'px; z-index:' + counter);
                $('#' + idEl).attr('type', 'text');
                $('#' + idEl).attr('m-title', 'текст_' + rand);
                correctSectionHeight(idEl);
                reload_right_panel();
                counter++;
            }
            saveStep();
        }
    });

    $('#button').draggable({
        distance: 20,
        helper: "clone",
        start: function () {

        },
        stop: function (event) {
            //console.log('добавили кнопку');
            var elem = $('.ui-draggable-dragging').offset();
            var el = document.elementFromPoint(elem.left - 1, elem.top - 1);
            var element;
            if ($(el).attr('class') == undefined) {
                element = $(el).closest('.column');
            } else if ($(el).attr('class').indexOf('column') < 0) {
                element = $(el).closest('.column');
            } else {
                element = $(el).attr('id');
            }
            try {
                var test = $('#' + element).attr('id');
            } catch (ex) {
                return;
            }

            if (element != '') {
                var rand = Math.round((Math.random() * 100000));
                var idEl = "button_" + rand;
                var div = document.createElement('div');
                div.id = idEl;
                div.className = "ui-widget-content";
                var a = document.createElement('a');
                a.innerHTML = 'Текст кнопки';
                $(a).attr('href', '#');
                $(a).attr('style', 'cursor: default; text-decoration: none; color: rgb(255,255,255); font-family: Arial; text-shadow: none; box-shadow: none;');
                div.appendChild(a);
                $('#' + element).append(div);
                var top = (event.clientY - $('#' + element).offset().top).toFixed(0);
                var left = (event.clientX - $('#' + element).offset().left).toFixed(0);
                //var top = (((event.clientY - $('#' + element).offset().top) / $('#' + element).height()) * 100).toFixed(1);
                var leftP = (((event.clientX - $('#' + element).offset().left) / $('#' + element).width()) * 100).toFixed(1);
                var width = ((150 / $('#' + element).width()) * 100).toFixed(1);
                var height = ((25 / $('#' + element).height()) * 100).toFixed(1);
                //if (parseFloat(top) + parseFloat(height) > 100) {
                //    top = 100 - parseFloat(height);
                //}
                //if (parseFloat(left) + parseFloat(width) > 100) {
                //    left = 100 - parseFloat(width);
                //}
                if (parseFloat(leftP) + parseFloat(width) > 100) {
                    left = ((100 - parseFloat(width)) / 100) * $('#' + element).width();
                }
                //$('#' + idEl).attr('style', 'cursor: pointer; color: rgb(255,255,255); text-align: center; border: 1px solid rgb(0, 0, 0, 0); border-radius: 2px; line-height: 25px; font-size: 16px; font-family: Magneto; position: absolute; padding: 0px; box-shadow: rgba(30, 37, 35, 0.811765) 1px 1px 4px; text-shadow: rgba(30, 37, 35, 0.811765) 3px 4px 3px; background: linear-gradient(rgba(1, 166, 249, 0.8), rgb(37, 136, 147)); width: ' + width + '%; height: ' + height + '%; top:' + top + '%; left: ' + left + '%; z-index:' + counter);
                $('#' + idEl).attr('style', 'cursor: pointer; color: rgb(255,255,255); text-align: center; border: 1px solid rgb(0, 0, 0, 0); border-radius: 2px; line-height: 25px; font-size: 16px; font-family: Magneto; position: absolute; padding: 0px; box-shadow: rgba(30, 37, 35, 0.811765) 1px 1px 4px; text-shadow: rgba(30, 37, 35, 0.811765) 3px 4px 3px; background: linear-gradient(rgba(1, 166, 249, 0.8), rgb(37, 136, 147)); width: 150px; height: 25px; top:' + top + 'px; left: ' + left + 'px; z-index:' + counter);
                $('#' + idEl).attr('type', 'button');
                $('#' + idEl).attr('m-title', 'кнопка_' + rand);
                $('#' + idEl).attr('option', 'standard');

                var buttonStyles = document.createElement('p');
                $(buttonStyles).attr('rel', idEl);
                $(buttonStyles).text('#' + idEl + '{text-align: center; display: block; font-size: 16px;  border: 1px solid rgb(0, 0, 0, 0); border-radius: 2px; line-height: 25px; font-family: Magneto; color: white; position: absolute; padding: 0px; box-shadow: rgba(30, 37, 35, 0.811765) 1px 1px 4px; text-shadow: rgba(30, 37, 35, 0.811765) 3px 4px 3px; background: linear-gradient(rgba(1, 166, 249, 0.8), rgb(37, 136, 147));  width:150px; height:25px; top:' + top + 'px; left:' + left + 'px;z-index:' + counter + '}#' + idEl + ':hover{text-align: center; display: block; font-size: 16px;  border: 1px solid rgb(0, 0, 0, 0); border-radius: 2px; line-height: 25px; font-family: Magneto; color: white; position: absolute; padding: 0px; box-shadow: rgba(30, 37, 35, 0.811765) 1px 1px 4px; text-shadow: rgba(30, 37, 35, 0.811765) 3px 4px 3px; background: linear-gradient(rgba(1, 108, 249, 0.8), rgb(37, 84, 147));  width: 150px;height: 25px;top:' + top + 'px;left:' + left + 'px;z-index:' + counter + '}#' + idEl + ':active{text-align: center; display: block; font-size: 16px;  border: 1px solid rgb(0, 0, 0, 0); border-radius: 2px; line-height: 25px; font-family: Magneto; color: white; position: absolute; padding: 0px; box-shadow: rgba(30, 37, 35, 0.811765) 1px 1px 4px; text-shadow: rgba(30, 37, 35, 0.811765) 3px 4px 3px; background: linear-gradient(rgba(249, 1, 1, 0.8), rgb(147, 37, 48)); width: 150px;height: 25px;top:' + top + 'px;left:' + left + 'px; z-index:' + counter + '}');
                $('#styles').append(buttonStyles);
                correctSectionHeight(idEl);
                reload_right_panel();
                counter++;
            }
            saveStep();
        }
    });

    $('#image').draggable({
        distance: 20,
        helper: "clone",
        start: function () {

        },
        stop: function (event) {
            //console.log('добавили изображение');
            var elem = $('.ui-draggable-dragging').offset();
            var el = document.elementFromPoint(elem.left - 1, elem.top - 1);
            var element;
            stopRecursoin = 0;
            if ($(el).attr('class') == undefined) {
                element = $(el).closest('.column');
            } else if ($(el).attr('class').indexOf('column') < 0) {
                element = $(el).closest('.column');
            } else {
                element = $(el).attr('id');
            }
            try {
                var test = $('#' + element).attr('id');
            } catch (ex) {
                return;
            }
            if (element != '') {
                var rand = Math.round((Math.random() * 100000))
                var idEl = "image_" + rand;
                var div = document.createElement('div');
                div.id = idEl;
                div.className = "ui-widget-content";

                var a = document.createElement('a');
                $(a).attr('style', 'width: 100%; height: 100%');

                var img = document.createElement('img');
                a.appendChild(img);
                div.appendChild(a);
                $('#' + element).append(div);
                var top = (event.clientY - $('#' + element).offset().top).toFixed(0);
                var left = (event.clientX - $('#' + element).offset().left).toFixed(0);
                //var top = (((event.clientY - $('#' + element).offset().top) / $('#' + element).height()) * 100).toFixed(1);
                var leftP = (((event.clientX - $('#' + element).offset().left) / $('#' + element).width()) * 100).toFixed(1);
                var width = ((200 / $('#' + element).width()) * 100).toFixed(1);
                //var height = ((150 / $('#' + element).height()) * 100).toFixed(1);
                //if (parseFloat(top) + parseFloat(height) > 100) {
                //    top = 100 - parseFloat(height);
                //}
                //if (parseFloat(left) + parseFloat(width) > 100) {
                //    left = 100 - parseFloat(width);
                //}
                if (parseFloat(leftP) + parseFloat(width) > 100) {
                    left = ((100 - parseFloat(width)) / 100) * $('#' + element).width();
                }
                $('#' + idEl).attr('style', 'overflow: hidden; position: absolute; font-family: Arial; width: 200px; height: 150px; top:' + top + 'px; left: ' + left + 'px; z-index:' + counter);
                $('#' + idEl).attr('type', 'image');
                $('#' + idEl + '>a>img').attr('src', '/Areas/Cabinet/Images/no-image.png');
                $('#' + idEl + '>a>img').attr('style', 'width: 100%;');
                $('#' + idEl).attr('m-title', 'картинка_' + rand);
                correctSectionHeight(idEl);
                reload_right_panel();
                counter++;
            }
            saveStep();
        }
    });

    $('#icons').draggable({
        distance: 20,
        helper: "clone",
        start: function () {

        },
        stop: function (event) {
            //console.log('добавили иконку');
            var elem = $('.ui-draggable-dragging').offset();
            var el = document.elementFromPoint(elem.left - 1, elem.top - 1);
            var element;
            stopRecursoin = 0;
            if ($(el).attr('class') == undefined) {
                element = $(el).closest('.column');
            } else if ($(el).attr('class').indexOf('column') < 0) {
                element = $(el).closest('.column');
            } else {
                element = $(el).attr('id');
            }
            try {
                var test = $('#' + element).attr('id');
            } catch (ex) {
                return;
            }
            if (element != '') {
                var rand = Math.round((Math.random() * 100000))
                var idEl = "icon_" + rand;
                var div = document.createElement('div');
                div.id = idEl;
                div.className = "ui-widget-content";
                var a = document.createElement('a');
                div.appendChild(a);
                $('#' + element).append(div);
                var top = (event.clientY - $('#' + element).offset().top).toFixed(0);
                var left = (event.clientX - $('#' + element).offset().left).toFixed(0);
                //var top = (((event.clientY - $('#' + element).offset().top) / $('#' + element).height()) * 100).toFixed(1);
                var leftP = (((event.clientX - $('#' + element).offset().left) / $('#' + element).width()) * 100).toFixed(1);
                var width = ((40 / $('#' + element).width()) * 100).toFixed(1);
                //var height = ((40 / $('#' + element).height()) * 100).toFixed(1);
                //if (parseFloat(top) + parseFloat(height) > 100) {
                //    top = 100 - parseFloat(height);
                //}
                //if (parseFloat(left) + parseFloat(width) > 100) {
                //    left = 100 - parseFloat(width);
                //}
                if (parseFloat(leftP) + parseFloat(width) > 100) {
                    left = ((100 - parseFloat(width)) / 100) * $('#' + element).width();
                }
                $('#' + idEl).attr('style', 'color: black; background: rgba(0,0,0,0); font-family: Arial; width: 40px; height: 40px; position: absolute; top:' + top + 'px; left: ' + left + 'px; z-index:' + counter);
                $('#' + idEl).attr('type', 'icon');
                $('#' + idEl + '>a').attr('class', 'fa fa-smile-o fa-3x');
                $('#' + idEl).attr('m-title', 'иконка_' + rand);
                correctSectionHeight(idEl);
                reload_right_panel();
                counter++;
            }
            saveStep();
        }
    });

    $('#video').draggable({
        distance: 20,
        helper: "clone",
        start: function () {

        },
        stop: function (event) {
            //console.log('добавили видео');
            var elem = $('.ui-draggable-dragging').offset();
            var el = document.elementFromPoint(elem.left - 1, elem.top - 1);
            var element;
            stopRecursoin = 0;
            if ($(el).attr('class') == undefined) {
                element = $(el).closest('.column');
            } else if ($(el).attr('class').indexOf('column') < 0) {
                element = $(el).closest('.column');
            } else {
                element = $(el).attr('id');
            }
            //if (element != '') {
            //    var idEl = "video_" + Math.round((Math.random() * 100000));
            //    var div = document.createElement('div');
            //    div.id = idEl;
            //    div.className = "ui-widget-content";
            //    var iframe = document.createElement('iframe');
            //        $(iframe).css('width', '100%');
            //        $(iframe).css('height', '100%');
            //        $(iframe).css('border', 'none');
            //        $(iframe).attr('src', '/Cabinet/Editor/UserVideo?videoUrl=http://cv.lp100plus.ru/Content/images/AdobeMuse.mp4&posterUrl=//img.youtube.com/vi/ZyYkuAUvYOw/default.jpg');
            //        div.appendChild(iframe);
            //        $('#' + element).append(div);
            //        var top = (((event.clientY - $('#' + element).offset().top) / $('#' + element).height()) * 100).toFixed(1);
            //        var left = (((event.clientX - $('#' + element).offset().left) / $('#' + element).width()) * 100).toFixed(1);
            //        var width = ((210 / $('#' + element).width()) * 100).toFixed(1);
            //        var height = ((157 / $('#' + element).height()) * 100).toFixed(1);
            //        if (parseFloat(top) + parseFloat(height) > 100) {
            //            top = 100 - parseFloat(height);
            //        }
            //        if (parseFloat(left) + parseFloat(width) > 100) {
            //            left = 100 - parseFloat(width);
            //        }
            //        $('#' + idEl).attr('style', 'position: absolute; font-family: Arial; width:' + width + '%; height: ' + height + '%; top:' + top + '%; left: ' + left + '%; z-index:' + counter);
            //        $('#' + idEl).attr('type', 'video');
            //        $('#' + idEl).attr('m-title', idEl);
            //}

            try {
                var test = $('#' + element).attr('id');
            } catch (ex) {
                return;
            }
            if (element != '') {
                var rand = Math.round((Math.random() * 100000));
                var idEl = "video_" + rand;
                var div = document.createElement('div');
                div.id = idEl;
                div.className = "ui-widget-content";
                var iframe = document.createElement('img');
                $(iframe).css('width', '100%');
                $(iframe).css('height', '100%');
                $(iframe).attr('src', '//img.youtube.com/vi/ZyYkuAUvYOw/default.jpg');
                $(iframe).attr('rel', 'https://youtube.com/embed/ZyYkuAUvYOw?autoplay=0?autoplay=0&loop=0&showinfo=0&theme=light&color=red&controls=0&modestbranding=&start=0&fs=0&iv_load_policy=3&wmode=transparent&rel=0');

                div.appendChild(iframe);
                $('#' + element).append(div);
                var top = (event.clientY - $('#' + element).offset().top).toFixed(0);
                var left = (event.clientX - $('#' + element).offset().left).toFixed(0);
                //var top = (((event.clientY - $('#' + element).offset().top) / $('#' + element).height()) * 100).toFixed(1);
                var leftP = (((event.clientX - $('#' + element).offset().left) / $('#' + element).width()) * 100).toFixed(1);
                var width = ((210 / $('#' + element).width()) * 100).toFixed(1);
                //var height = ((157 / $('#' + element).height()) * 100).toFixed(1);
                //if (parseFloat(top) + parseFloat(height) > 100) {
                //    top = 100 - parseFloat(height);
                //}
                //if (parseFloat(left) + parseFloat(width) > 100) {
                //    left = 100 - parseFloat(width);
                //}
                if (parseFloat(leftP) + parseFloat(width) > 100) {
                    left = ((100 - parseFloat(width)) / 100) * $('#' + element).width();
                }
                $('#' + idEl).attr('style', 'position: absolute; font-family: Arial; width: 210px; height: 157px; top:' + top + 'px; left: ' + left + 'px; z-index:' + counter);
                $('#' + idEl).attr('type', 'video');
                $('#' + idEl).attr('m-title', 'видео_' + rand);

                correctSectionHeight(idEl);
                reload_right_panel();
                counter++;
            }
            saveStep();
        }
    });

    $('#timer').draggable({
        distance: 20,
        helper: "clone",
        start: function () {

        },
        stop: function (event) {
            //console.log('добавили таймер');
            var elem = $('.ui-draggable-dragging').offset();
            var el = document.elementFromPoint(elem.left - 1, elem.top - 1);
            var element;
            stopRecursoin = 0;
            if ($(el).attr('class') == undefined) {
                element = $(el).closest('.column');
            } else if ($(el).attr('class').indexOf('column') < 0) {
                element = $(el).closest('.column');
            } else {
                element = $(el).attr('id');
            }
            try {
                var test = $('#' + element).attr('id');
            } catch (ex) {
                return;
            }
            if (element != '') {
                var t = new Date((new Date()).getTime() + 6000 * 60 * 24 * 2);
                var time = Math.round((Math.random() * 100000));
                var idEl = "timer_" + time;
                var div = document.createElement('div');
                div.id = idEl;
                $(div).attr('m-title', 'таймер_' + time);
                $(div).attr('type', 'timer');
                div.className = "ui-widget-content";
                $(div).attr('timer', t.toString());
                var a = document.createElement('a');
                var timer = document.createElement('div');
                $(timer).attr('id', 'countdown-' + time);
                $(timer).addClass('timeTo timeTo-white');
                var script = document.createElement('script');
                $(script).text("/*" + idEl + "*/ $(function(){$('#countdown-" + time + "').timeTo(new Date('" + t.toString() + "'));});");
                $(script).attr('type', 'text/javascript');
                $(a).append(timer);
                $(div).append(a);


                $('#' + element).append(div);
                $(a).append(script);
                var top = (event.clientY - $('#' + element).offset().top).toFixed(0);
                var left = (event.clientX - $('#' + element).offset().left).toFixed(0);
                //var top = (((event.clientY - $('#' + element).offset().top) / $('#' + element).height()) * 100).toFixed(1);
                var leftP = (((event.clientX - $('#' + element).offset().left) / $('#' + element).width()) * 100).toFixed(1);
                var width = ((173 / $('#' + element).width()) * 100).toFixed(1);
                //var height = ((30 / $('#' + element).height()) * 100).toFixed(1);
                //if (parseFloat(top) + parseFloat(height) > 100) {
                //    top = 100 - parseFloat(height);
                //}
                //if (parseFloat(left) + parseFloat(width) > 100) {
                //    left = 100 - parseFloat(width);
                //}
                if (parseFloat(leftP) + parseFloat(width) > 100) {
                    left = ((100 - parseFloat(width)) / 100) * $('#' + element).width();
                }
                $('#' + idEl).attr('style', 'font-family: Tahoma, Verdana, Aial, sans-serif; font-size: 28px; line-height: 23px; position: absolute; width: 173px; height: 30px; top:' + top + 'px; left: ' + left + 'px; z-index:' + counter);
                correctSectionHeight(idEl);
                reload_right_panel();
                counter++;
            }
            saveStep();
        }
    });

    $('#circular_timer').draggable({
        distance: 20,
        helper: "clone",
        start: function () {

        },
        stop: function (event) {
            //console.log('добавили круговой таймер');
            var elem = $('.ui-draggable-dragging').offset();
            var el = document.elementFromPoint(elem.left - 1, elem.top - 1);
            var element;
            stopRecursoin = 0;
            if ($(el).attr('class') == undefined) {
                element = $(el).closest('.column');
            } else if ($(el).attr('class').indexOf('column') < 0) {
                element = $(el).closest('.column');
            } else {
                element = $(el).attr('id');
            }
            try {
                var test = $('#' + element).attr('id');
            } catch (ex) {
                return;
            }
            if (element != '') {
                var t = new Date((new Date())).getFullYear() + '-' + (parseInt(new Date((new Date())).getMonth()) + 1) + '-' + new Date((new Date())).getDate() + ' ' + new Date((new Date())).getHours() + ':' + new Date((new Date())).getMinutes() + ':' + new Date((new Date())).getSeconds();
                var time = Math.round((Math.random() * 100000));
                var idEl = "timer_" + time;
                var div = document.createElement('div');
                div.id = idEl;
                $(div).attr('m-title', 'таймер_' + time);
                $(div).attr('type', 'circular_timer');
                div.className = "ui-widget-content clearfix borderbox";
                //$(div).attr('timer', t.toString());

                var div1 = document.createElement('div');
                $(div1).attr('class', 'size_fixed colelem');
                div1.id = 'u' + time;

                var div2 = document.createElement('div');
                $(div2).attr('data-date', t);
                div2.id = 'countDown_' + time;
                $(div2).attr('style', 'margin:0 auto;');

                var timeCircles = document.createElement('div');
                $(timeCircles).attr('class', 'time_circles');

                var textDiv_Days = document.createElement('div');
                $(textDiv_Days).attr('class', 'textDiv_Days');
                $(textDiv_Days).attr('style', 'top: 18px; padding-top: 10px; left: 0px; width: 70px; height: 70px; border:7px solid #c4c4c4; border-top:7px solid #5f5f5f; border-right:7px solid #5f5f5f; border-radius: 150px;');

                var textDiv_Days_h4 = document.createElement('h4');
                $(textDiv_Days_h4).text('Дни');
                $(textDiv_Days_h4).attr('style', 'font-size: 7px; line-height: 7px;');

                var textDiv_Days_span = document.createElement('span');
                $(textDiv_Days_span).text('0');
                $(textDiv_Days_span).attr('style', 'font-size: 21px; line-height: 7px;');

                var textDiv_Hours = document.createElement('div');
                $(textDiv_Hours).attr('class', 'textDiv_Hours');
                $(textDiv_Hours).attr('style', 'top: 18px; padding-top: 10px; left: 85px; width: 70px; height: 70px; border:7px solid #c4c4c4; border-top:7px solid #5f5f5f; border-right:7px solid #5f5f5f; border-radius: 150px;');

                var textDiv_Hours_h4 = document.createElement('h4');
                $(textDiv_Hours_h4).text('Часы');
                $(textDiv_Hours_h4).attr('style', 'font-size: 7px; line-height: 7px;');

                var textDiv_Hours_span = document.createElement('span');
                $(textDiv_Hours_span).text('0');
                $(textDiv_Hours_span).attr('style', 'font-size: 21px; line-height: 7px;');

                var textDiv_Minutes = document.createElement('div');
                $(textDiv_Minutes).attr('class', 'textDiv_Minutes');
                $(textDiv_Minutes).attr('style', 'top: 18px; padding-top: 10px; left: 170px; width: 70px; height: 70px; border:7px solid #c4c4c4; border-top:7px solid #5f5f5f; border-right:7px solid #5f5f5f; border-radius: 150px;');

                var textDiv_Minutes_h4 = document.createElement('h4');
                $(textDiv_Minutes_h4).text('Минуты');
                $(textDiv_Minutes_h4).attr('style', 'font-size: 7px; line-height: 7px;');

                var textDiv_Minutes_span = document.createElement('span');
                $(textDiv_Minutes_span).text('0');
                $(textDiv_Minutes_span).attr('style', 'font-size: 21px; line-height: 7px;');

                var textDiv_Seconds = document.createElement('div');
                $(textDiv_Seconds).attr('class', 'textDiv_Seconds');
                $(textDiv_Seconds).attr('style', 'top: 18px; padding-top: 10px; left: 255px; width: 70px; height: 70px; border:7px solid #c4c4c4; border-top:7px solid #5f5f5f; border-right:7px solid #5f5f5f; border-radius: 150px;');

                var textDiv_Seconds_h4 = document.createElement('h4');
                $(textDiv_Seconds_h4).text('Секунды');
                $(textDiv_Seconds_h4).attr('style', 'font-size: 7px; line-height: 7px;');

                var textDiv_Seconds_span = document.createElement('span');
                $(textDiv_Seconds_span).text('0');
                $(textDiv_Seconds_span).attr('style', 'font-size: 21px; line-height: 7px;');

                var verticalspacer = document.createElement('div');
                $('#verticalspacer').attr('class', 'verticalspacer');
                $('#verticalspacer').attr('data-offset-top', '416');
                $('#verticalspacer').attr('data-content-above-spacer', '416');
                $('#verticalspacer').attr('data-content-below-spacer', '84');

                $('#' + element).append(div);
                $(div).append(div1);
                $(div).append(verticalspacer);
                $(div1).append(div2);
                $(div2).append(timeCircles);
                $(timeCircles).append(textDiv_Days);
                $(textDiv_Days).append(textDiv_Days_h4);
                $(textDiv_Days).append(textDiv_Days_span);
                $(timeCircles).append(textDiv_Hours);
                $(textDiv_Hours).append(textDiv_Hours_h4);
                $(textDiv_Hours).append(textDiv_Hours_span);
                $(timeCircles).append(textDiv_Minutes);
                $(textDiv_Minutes).append(textDiv_Minutes_h4);
                $(textDiv_Minutes).append(textDiv_Minutes_span);
                $(timeCircles).append(textDiv_Seconds);
                $(textDiv_Seconds).append(textDiv_Seconds_h4);
                $(textDiv_Seconds).append(textDiv_Seconds_span);


                var top = (event.clientY - $('#' + element).offset().top).toFixed(0);
                var left = (event.clientX - $('#' + element).offset().left).toFixed(0);
                var leftP = (((event.clientX - $('#' + element).offset().left) / $('#' + element).width()) * 100).toFixed(1);
                var width = ((173 / $('#' + element).width()) * 100).toFixed(1);
                if (parseFloat(leftP) + parseFloat(width) > 100) {
                    left = ((100 - parseFloat(width)) / 100) * $('#' + element).width();
                }
                $('#' + idEl).attr('style', 'font-family: Tahoma, Verdana, Aial, sans-serif; background: rgba(0,0,0,0); font-size: 28px; line-height: 23px; position: absolute; width: 400px; height: 100px; top:' + top + 'px; left: ' + left + 'px; z-index:' + counter);
                correctSectionHeight(idEl);
                reload_right_panel();
                counter++;

                var link = document.createElement('link');
                $(link).attr('href', '/Areas/Cabinet/lib/css/circular_timer.css');
                $(link).attr('rel', 'stylesheet');
                $('head').append(link);

                var link1 = document.createElement('link');
                $(link1).attr('href', '/Areas/Cabinet/lib/css/TimeCircles.css');
                $(link1).attr('rel', 'stylesheet');
                $('head').append(link1);

                var script1 = document.createElement('p');
                $(script1).attr('rel', idEl);
                $(script1).text('/*' + idEl + ', lastload*/ try{$("#countDown_' + time + '").TimeCircles({"animation": "ticks", "bg_width": 1,' +
                    ' "fg_width": 0.06, "circle_bg_color": "#5F5F5F", "time": { "Days": { "text": "Дни",' +
                    ' "color": "#C4C4C4", "show": true},"Hours": {"text": "Часы", "color": "#C4C4C4",' +
                    '"show": true}, "Minutes": { "text": "Минуты", "color": "#C4C4C4", "show": true},' +
                    '"Seconds": {"text": "Секунды", "color": "#C4C4C4", "show": true}}});}catch(ex){console.log(ex)}');
                $('#scripts').append(script1);
            }
            saveStep();
        }
    });

    //widget тут добавление
    $('.widget').draggable({
        distance: 20,
        helper: "clone",
        start: function () {

        },
        stop: function (event) {
            var title = $(this).find('.widget_title').clone();
            var id = $(this).find('.widget_id').text();
            var description = $(this).find('.widget_descripotion').clone();
            var script = $(this).find('.widget_script').text();
            var userId = $(this).find('.widget_userId').clone();
            var create = $(this).find('.widget_create').clone();

            if ($('#widget_' + id).attr('id') != undefined) {
                alert('Этот вижет уже добавлен! Мы для Вас сделали его активным на странице.');
                select('widget_' + id);
                return;
            }

            //console.log('добавили таймер');
            var elem = $('.ui-draggable-dragging').offset();
            var el = document.elementFromPoint(elem.left - 1, elem.top - 1);
            var element;
            stopRecursoin = 0;
            if ($(el).attr('class') == undefined) {
                element = $(el).closest('.column');
            } else if ($(el).attr('class').indexOf('column') < 0) {
                element = $(el).closest('.column');
            } else {
                element = $(el).attr('id');
            }
            try {
                var test = $('#' + element).attr('id');
            } catch (ex) {
                return;
            }
            if (element != '') {
                //widget тут начало добавления
                var idEl = 'widget_' + id;
                var div = document.createElement('div');
                div.id = idEl;
                $(div).attr('m-title', 'виджет #' + id);
                $(div).attr('type', 'widget');
                div.className = "ui-widget-content";

                $(div).append(title);
                $(div).append(description);
                $(div).append(userId);
                $(div).append(create);

                $('#' + element).append(div);

                var script1 = document.createElement('div');
                $(script1).attr('rel', idEl);
                $(script1).text('/*' + idEl + ', lastload*/ try{' + script + '}catch(ex){console.log(ex)}');
                $('#scripts').append(script1);
                //widget тут конец добавленя

                var top = (event.clientY - $('#' + element).offset().top).toFixed(0);
                var left = (event.clientX - $('#' + element).offset().left).toFixed(0);
                //var top = (((event.clientY - $('#' + element).offset().top) / $('#' + element).height()) * 100).toFixed(1);
                var leftP = (((event.clientX - $('#' + element).offset().left) / $('#' + element).width()) * 100).toFixed(1);
                var width = ((173 / $('#' + element).width()) * 100).toFixed(1);
                //var height = ((30 / $('#' + element).height()) * 100).toFixed(1);
                //if (parseFloat(top) + parseFloat(height) > 100) {
                //    top = 100 - parseFloat(height);
                //}
                //if (parseFloat(left) + parseFloat(width) > 100) {
                //    left = 100 - parseFloat(width);
                //}
                if (parseFloat(leftP) + parseFloat(width) > 100) {
                    left = ((100 - parseFloat(width)) / 100) * $('#' + element).width();
                }
                $('#' + idEl).attr('style', 'font-family: Tahoma, Verdana, Aial, sans-serif; font-size: 12px; line-height: 23px; position: absolute; width: 100px; height: 20px; top:' + top + 'px; left: ' + left + 'px; z-index:' + counter);
                correctSectionHeight(idEl);
                reload_right_panel();
                counter++;
            }
            saveStep();
        }
    });

    $('#section').draggable({
        distance: 20,
        helper: "clone",
        start: function () {

        },
        drag: function () {
            var elem = $('.ui-draggable-dragging').offset();
            var el = document.elementFromPoint(elem.left - 1, elem.top - 1);

            var sdf = $(el).closest('.section');
            $('.pasteSection').remove();
            var div = document.createElement('div');
            $(div).addClass('pasteSection');
            $(div).attr('X', elem.left - 1);
            $(div).attr('Y', elem.top - 1);
            if ($(sdf).attr('id') != undefined) {
                var posTop = $(sdf).offset().top;
                var posBottom = posTop + $(sdf).height();
                var posReal = elem.top - 1;
                if ((posBottom - posTop) / 2 > (posReal - posTop)) {
                    $(sdf).before(div);
                } else {
                    $(sdf).after(div);
                }
            } else if ($(el).closest('#content') != undefined) {
                $('#content_standard').append(div);
            }
        },
        stop: function (event) {
            //console.log('добавили секцию');
            var elem = $('.ui-draggable-dragging').offset();
            var el = document.elementFromPoint(elem.left - 1, elem.top - 1);

            var sdf = $(el).closest('#content');
            if ($(sdf).attr('id') != undefined) {
                openWondowAddSection(event.clientX, event.clientY);
            } else {
                $('.pasteSection').remove();
            }
            saveStep();
        }
    });

    $('#input').draggable({
        distance: 20,
        helper: "clone",
        start: function () {

        },
        drag: function () {

        },
        stop: function (event) {
            addFormElement(event, 'text')
        }
    });

    $('#textarea').draggable({
        distance: 20,
        helper: "clone",
        start: function () {

        },
        drag: function () {

        },
        stop: function (event) {
            addFormElement(event, 'textarea')
        }
    });

    $('#e-mail').draggable({
        distance: 20,
        helper: "clone",
        start: function () {

        },
        drag: function () {

        },
        stop: function (event) {
            addFormElement(event, 'e-mail')
        }
    });
    $('#url').draggable({
        distance: 20,
        helper: "clone",
        start: function () {

        },
        drag: function () {

        },
        stop: function (event) {
            addFormElement(event, 'url')
        }
    });
    $('#phone').draggable({
        distance: 20,
        helper: "clone",
        start: function () {

        },
        drag: function () {

        },
        stop: function (event) {
            addFormElement(event, 'phone')
        }
    });
    $('#file').draggable({
        distance: 20,
        helper: "clone",
        start: function () {

        },
        drag: function () {

        },
        stop: function (event) {
            addFormElement(event, 'file')
        }
    });

    function addFormElement(ev, typeOfElement) {
        //console.log('добавили поле ввода');
        var elem = $('.ui-draggable-dragging').offset();
        var el = document.elementFromPoint(elem.left - 1, elem.top - 1);
        var element;
        if ($(el).attr('class') == undefined) {
            element = $(el).closest('.containerForm');
        } else if ($(el).attr('class').indexOf('containerForm') < 0) {
            element = $(el).closest('.containerForm');
        } else if ($(el).attr('class').indexOf('containerForm') >= 0) {
            element = $(el).attr('class');
        } else {
            element = $(el).attr('class');
        }
        try {
            var test = $('#' + element).attr('id');
        } catch (ex) {
            return;
        }
        var formId = $('.tab_active').parent().attr('rel');
        if (element != "" && element != undefined) {
            var containerStyle = '';
            var styleInput = '';
            var textarea;
            var ident = '';
            if (typeOfElement == "textarea") {
                ident = "textarea";
            } else {
                ident = "input";
            }
            $('.' + formId + '_param').each(function () {
                if ($(this).is(ident)) {
                    textarea = this;
                }
            });
            if ($(textarea).attr('style') != undefined) {
                styleInput = $(textarea).attr('style');
                containerStyle = $(textarea).parent().attr('style');
            } else if ($('.' + formId + '_param').first().length > 0) {
                styleInput = $('.' + formId + '_param').first().attr('style');
                containerStyle = $('.' + formId + '_param').first().parent().attr('style');
            } else {
                styleInput = 'width: 100%; height: 20px; border: 1px solid #aaa;';
                containerStyle = 'position: absolute;  overflow: hidden; width: 150px;';
            }
            var styleLabel = '';
            if ($('.' + formId + '_label').first().length > 0) {
                styleLabel = $('.' + formId + '_label').first().attr('style');
            } else {
                styleLabel = 'font-size: 11px; color: #000000;';
            }
            var styleValid = '';
            if ($('.' + formId + '_param_valid').first().length > 0) {
                styleValid = $('.' + formId + '_param_valid').first().attr('style');
            } else {
                styleValid = 'font-size: 10px; color: red; width: 100%; height: 15px;';
            }


            var container = document.createElement('div');
            $(container).attr('style', containerStyle);
            $(container).attr('class', 'containerInput');
            var label = document.createElement('label');
            $(label).attr('style', styleLabel);
            $(label).attr('class', formId + '_label editable_element');
            $(label).text('Название');

            var input
            if (typeOfElement == "textarea") {
                input = document.createElement('textarea');
            } else {
                input = document.createElement('input');
            }

            $(input).attr('type', typeOfElement);
            $(input).attr('disabled', 'disabled');
            $(input).attr('placeholder', 'Подсказка');
            $(input).attr('class', formId + '_param editable_element');
            var count = $('.containerForm').children('.containerInput').length + 1;
            $(input).attr('id', formId + '_param_' + count);
            $(input).attr('name', 'parametrs');
            $(input).attr('placeholder', 'Подсказка');
            $(input).attr('style', styleInput);
            if (typeOfElement == "file") {
                $(input).css('border', 'none');
            }
            var valid = document.createElement('span');
            $(valid).attr('class', formId + '_param_valid editable_element');
            $(valid).text('* данные не прошли проверку');
            $(valid).attr('style', styleValid);
            $(container).append(label);
            $(container).append(input);
            $(container).append(valid);

            var top = (((ev.clientY - $('.' + element).offset().top) / $('.' + element).height()) * 100).toFixed(1);
            var left = (((ev.clientX - $('.' + element).offset().left) / $('.' + element).width()) * 100).toFixed(1);
            var width = ((150 / $('.' + element).width()) * 100).toFixed(1);
            var height = ((25 / $('.' + element).height()) * 100).toFixed(1);
            if (parseFloat(top) + parseFloat(height) > 100) {
                top = 100 - parseFloat(height);
            }
            if (parseFloat(left) + parseFloat(width) > 100) {
                left = 100 - parseFloat(width);
            }

            $(container).css('top', top + '%');
            $(container).css('left', left + '%');
            $('.containerForm').append(container);

            reload_right_panel();
            counter++;
        }
    }

    $(document).ready(function () {
        $('#content').scroll(function () {
            $('.hover_border').remove();
            $('.type_element').remove();
        });
    });

    $('.section').droppable({
        activeClass: "active"
    });

    $("#content_width_val").change(function () {
        if ($('#content_width_val').next().text() == '%') {
            $('#content_width_val').val(parseFloat($('#content_width_val').val()));
            $('#content_standard .section').each(function () {
                var width = getStyle($(this), 'width');
                var itemId = $(this).attr('id');
                var widthC = getStyle($('#' + itemId + '>.container_section'), 'width');
                if (width.replace(' ', '') == '100%') {
                    $('#' + itemId + '>.container_section').css('width', $('#content_width_val').val() + '%');
                }
                if (widthC.replace(' ', '') == '100%') {
                    $(this).css('width', $('#content_width_val').val() + '%');
                }
            });
            $('#pages .section').each(function () {
                if ($(this).attr('class').indexOf('sectionContent') < 0) {
                    var width = getStyle($(this), 'width');
                    var itemId = $(this).attr('id');
                    var widthC = getStyle($('#' + itemId + '>.container_section'), 'width');
                    if (width.replace(' ', '') == '100%') {
                        $('#' + itemId + '>.container_section').css('width', $('#content_width_val').val() + '%');
                    }
                    if (widthC.replace(' ', '') == '100%') {
                        $(this).css('width', $('#content_width_val').val() + '%');
                    }
                }
            });
        } else {
            $('#content_width_val').val(parseInt($('#content_width_val').val()));
            $('#content_standard .section').each(function () {
                var width = getStyle($(this), 'width');
                var itemId = $(this).attr('id');
                var widthC = getStyle($('#' + itemId + '>.container_section'), 'width');
                if (width.replace(' ', '') == '100%') {
                    $('#' + itemId + '>.container_section').css('width', $('#content_width_val').val() + 'px');
                }
                if (widthC.replace(' ', '') == '100%') {
                    $(this).css('width', $('#content_width_val').val() + 'px');
                }
            });
            $('#pages .section').each(function () {
                if ($(this).attr('class').indexOf('sectionContent') < 0) {
                    var width = getStyle($(this), 'width');
                    var itemId = $(this).attr('id');
                    var widthC = getStyle($('#' + itemId + '>.container_section'), 'width');
                    if (width.replace(' ', '') == '100%') {
                        $('#' + itemId + '>.container_section').css('width', $('#content_width_val').val() + 'px');
                    }
                    if (widthC.replace(' ', '') == '100%') {
                        $(this).css('width', $('#content_width_val').val() + 'px');
                    }
                }
            });
        }
        saveStep();
    });

    $("#content_width_val").keypress(function (key) {
        if (((key.charCode < 48 || key.charCode > 57) && key.charCode !== 46) ||
            ($("#content_width_val").val().indexOf('.') >= 0 && key.charCode === 46)) return false;
        //$("#content_width_val").val($("#content_width_val").val() + getKeyByCode(key.charCode));
        if ($('#content_width_val').next().text() == '%') {
            $('#content_standard .section').each(function () {
                var width = getStyle($(this), 'width');
                var itemId = $(this).attr('id');
                var widthC = getStyle($('#' + itemId + '>.container_section'), 'width');
                if (width.replace(' ', '') == '100%') {
                    $('#' + itemId + '>.container_section').css('width', $('#content_width_val').val() + '%');
                }
                if (widthC.replace(' ', '') == '100%') {
                    $(this).css('width', $('#content_width_val').val() + '%');
                }
            });
            $('#pages .section').each(function () {
                if ($(this).attr('class').indexOf('sectionContent') < 0) {
                    var width = getStyle($(this), 'width');
                    var itemId = $(this).attr('id');
                    var widthC = getStyle($('#' + itemId + '>.container_section'), 'width');
                    if (width.replace(' ', '') == '100%') {
                        $('#' + itemId + '>.container_section').css('width', $('#content_width_val').val() + '%');
                    }
                    if (widthC.replace(' ', '') == '100%') {
                        $(this).css('width', $('#content_width_val').val() + '%');
                    }
                }
            });
        } else {
            $('#content_standard .section').each(function () {
                var width = getStyle($(this), 'width');
                var itemId = $(this).attr('id');
                var widthC = getStyle($('#' + itemId + '>.container_section'), 'width');
                if (width.replace(' ', '') == '100%') {
                    $('#' + itemId + '>.container_section').css('width', $('#content_width_val').val() + 'px');
                }
                if (widthC.replace(' ', '') == '100%') {
                    $(this).css('width', $('#content_width_val').val() + 'px');
                }
            });
            $('#pages .section').each(function () {
                if ($(this).attr('class').indexOf('sectionContent') < 0) {
                    var width = getStyle($(this), 'width');
                    var itemId = $(this).attr('id');
                    var widthC = getStyle($('#' + itemId + '>.container_section'), 'width');
                    if (width.replace(' ', '') == '100%') {
                        $('#' + itemId + '>.container_section').css('width', $('#content_width_val').val() + 'px');
                    }
                    if (widthC.replace(' ', '') == '100%') {
                        $(this).css('width', $('#content_width_val').val() + 'px');
                    }
                }
            });
        }
        saveStep();
    });

    ///контекстное меню
    document.body.oncontextmenu = function (e) {
        return false;
    };

    $('#input_image_load').on('change', function (e) {

        var files = e.target.files;

        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            // Если в файле содержится изображение
            if (/image.*/.test(file.type)) {
                // узнаём информацию о нём
                var data = [file.name, file.type, file.size];
                var fr = new FileReader();
                // считываем его в строку base64
                fr.readAsDataURL(file);
                // как только файл загружен
                fr.onload = (function (file, data) {
                    return function (e) {
                        var img = new Image(),
                          s, td;
                        img.src = e.target.result;

                        /*
                         * и как только загружено изображение
                         * добавляем в информацию о файле html-код первьюшки
                         */

                        if (img.complete) {
                            img = makePreview(img, 128);
                            var codeImage = img.src;
                            $.ajax({
                                url: "/Cabinet/Editor/SaveImageByByteArray",
                                data: {
                                    codeImage: codeImage
                                },
                                async: true,
                                success: function (data) {
                                    $('#image_from_gallery').html();
                                    $('#image_from_gallery').append("<img>");
                                    $('#image_from_gallery>img').attr('src', data);
                                },
                                error: function (data) {
                                    alert('Все плохо');
                                },
                                type: "POST"
                            });
                        } else {
                            img.onload = function () {
                                img = makePreview(img, 128);
                                var codeImage = img.src.replace('base64,', '$')[1];
                                $.ajax({
                                    url: "/Cabinet/Editor/SaveImageByByteArray",
                                    data: {
                                        codeImage: codeImage
                                    },
                                    async: true,
                                    success: function (data) {
                                        $('#image_from_gallery').html();
                                        $('#image_from_gallery').append("<img>");
                                        $('#image_from_gallery>img').attr('src', data);
                                    },
                                    error: function (data) {
                                        alert('Все плохо');
                                    },
                                    type: "POST"
                                });
                            }
                        }

                    }
                })(file, data);
                // Если файл не изображение
            } else {
                // то вместо превью выводим соответствующую надпись
                data = [file.name, file.type, file.size, 'Файл не является изображением'];
            }
        }

        function makePreview(image, a) {
            var img = image,
              w = img.width, h = img.height,
              s = w / h;

            if (w > a && h > a) {
                if (img.width > img.height) {
                    img.width = a;
                    img.height = a / s;
                } else {
                    img.height = a;
                    img.width = a * s;
                }
            }

            return img;
        }
    });


    $(document).mouseover(function (event) {
        if ($('.edit_form').length) { return; }
        var elem = document.elementFromPoint(event.clientX, event.clientY);
        var act = $(elem).closest('.ui-widget-content');
        if ($(act).attr('id') != undefined && $(act).attr('class').indexOf('active') < 0) {


            $('.type_element').remove();
            $('.hover_border').remove();

            var width = parseFloat($(act).css('width')) + 2;
            var height = parseFloat($(act).css('height')) + 3;
            var top = parseInt($(act).css('top')) + $(act).parent().offset().top - 1;
            var left = parseInt($(act).css('left')) + $(act).parent().offset().left - 1;
            var zindex = parseInt($(act).css('z-index')) - 1;
            var rotate = parseInt(getStyle($(act), 'transform').replace('rotate(', '').replace('deg)', ''));
            if (isNaN(rotate)) {
                rotate = 0;
            }
            if ($(act).attr('id') != 'remove_form_element') {
                var border = document.createElement('div');
                $(border).addClass('hover_border');
                $(border).attr('style', 'z-index: ' + zindex + '; position: absolute; transform: rotate(' + rotate + 'deg); width:' + width + 'px; height:' + height + 'px; top:' + top + 'px; left:' + left + 'px; border: 1px dashed #428bca;');
                $('body').append(border);

            }
            var type = document.createElement('div');
            $(type).addClass('type_element');

            //$(type).attr('style', 'z-index:' + (zindex + 1) + '; color: white; position: absolute; width: 50px; height: 0px; background: rgba(42, 139, 202, 0.6); text-align: center; top:' + (top + 1) + 'px; left:' + (left + 1) + 'px;');
            $(type).attr('style', 'z-index:' + (zindex + 1) + '; color: white; position: absolute; width: 50px; height: 0px; background: rgba(42, 139, 202, 0.6); text-align: center; top:0; left:0;');
            switch ($(act).attr('type')) {
                case 'text':
                    $(type).text('текст');
                    break;
                case 'button':
                    $(type).text('кнопка');
                    break;
                case 'image':
                    $(type).text('изобр.');
                    break;
                case 'icon':
                    $(type).text('иконка');
                    break;
                case 'video':
                    $(type).text('видео');
                    break;
                case 'timer':
                    $(type).text('таймер');
                    break;
                case 'anchor':
                    $(type).text('якорь');
                    break;
                case 'section':
                    $(type).text('секция');
                    break;
                case 'map':
                    $(type).text('карта');
                    break;
                case 'form':
                    $(type).text('форма');
                    break;
                case 'block':
                    $(type).text('блок');
                    break;
            }
            $(type).attr('rel', $(act).attr('id'));

            if ($(act).attr('id').indexOf('btn_close') < 0 && $(act).attr('type') != 'anchor') {
                $('.hover_border').append(type);
            }

            $('.type_element').animate({
                height: 17
            }, 100);
        }
    });
    $(document).mouseout(function (event) {
        if (($(event.target).closest(".type_element").length) || ($(event.target).closest(".hover_border").length) || ($(event.target).attr('class') == undefined && $(event.target).attr('id') == undefined)) {
            return;
        }

        var elem = document.elementFromPoint(event.clientX, event.clientY);
        if ($(elem).closest('.section').attr('id') != undefined && $(elem).attr('class') != 'hover_border') {
            $('.type_element').remove();
            $('.hover_border').remove();
        }
    });


    $(document).mouseup(function (e) {
        var container = $(".rename_input");
        if (container.has(e.target).length === 0) {
            var id = $(".rename_input").attr('name');
            if ($('#' + id).attr('type') == 'anchor') {
                $('#' + id + '>a').attr('name', $(".rename_input").val());
            }

            $('#' + id).attr('m-title', $(".rename_input").val());
            $('#' + id + '>a').attr('name', $(".rename_input").val());

            $(".rename_input").prev().text($(".rename_input").val());
            $(".rename_input").prev().show();
            $(".rename_input").remove();
        }
    });

    $(document).click(function (event) {
        if (!$(event.target).closest("#contextMenu").length) {
            $('#contextMenu').hide();
        }        
        if ($(event.target).closest("#textarea_text_element").length) {
            return;
        }

        //close border selectlist
        //try {
        //    var temp = $('#country_id_2').next().attr('id');
        //    if ((!$(event.target).closest('#' + temp).length)) {
        //        $("#country_id_2").selectbox('close');
        //    }
        //} catch (ex) {
        //    //console.log('Error: ' + ex);
        //}

        //close image reiteration selectlist
        try {
            var temp3 = $('#background_repeat').next().attr('id');
            if ((!$(event.target).closest('#' + temp3).length)) {
                $("#background_repeat").selectbox('close');
            }
        } catch (ex3) {
            //console.log('Error: ' + ex3);
        }

        //close image option selectlist
        try {
            var temp4 = $('#background_option').next().attr('id');
            if ((!$(event.target).closest('#' + temp4).length)) {
                $("#background_option").selectbox('close');
            }
        } catch (ex4) {
            //console.log('Error: ' + ex4);
        }

        //close animate option selectlist
        try {
            var temp5 = $('#select_animate').next().attr('id');
            if ((!$(event.target).closest('#' + temp5).length)) {
                $("#select_animate").selectbox('close');
            }
        } catch (ex5) {
            //console.log('Error: ' + ex5);
        }

        //close delay animate option selectlist
        try {
            var temp6 = $('#delay_animate').next().attr('id');
            if ((!$(event.target).closest('#' + temp6).length)) {
                $("#delay_animate").selectbox('close');
            }
        } catch (ex6) {
            //console.log('Error: ' + ex6);
        }

        //close duration animate option selectlist
        try {
            var temp7 = $('#duration_animate').next().attr('id');
            if ((!$(event.target).closest('#' + temp7).length)) {
                $("#duration_animate").selectbox('close');
            }
        } catch (ex7) {
            //console.log('Error: ' + ex7);
        }

        if (resized == true) {
            return;
        }

        if (!$(event.target).closest(".option_el").length && !$(event.target).closest(".name-of-param").length
            && !$(event.target).closest(".cp-color-picker").length) {
            $(".option_el").each(function () {
                if ($(this).css('position') == 'fixed') {
                    $(this).hide();
                }
            });
        }

        if (!$(event.target).closest("#unit").length && !$(event.target).closest(".units").length) {
            $('#unit').remove();
        }

        if ($('.active').attr('type') == 'button' || $('.active').attr('type') == 'btn') {
            saveButtonOption();
        }

        if (!$(event.target).closest("#option_panel_content>div>.option_el").length && !$(event.target).closest("#option_panel_content>div").length
            && !$(event.target).closest("#unit").length) {
            $("#option_panel_content>div>.option_el").hide();
            $('.name-of-param').css('background-color', 'rgba(0,0,0,0)');
        }
        if (!$(event.target).closest("#panel_align").length) {
            $("#panel_align_menu").hide();
        }
        if (!$(event.target).closest("#panel_background").length && !$(event.target).closest(".cp-color-picker").length) {
            $("#panel_background_menu").hide();
        }

        if ((!$(event.target).closest("#sub_element_menu").length) && (!$(event.target).closest(".element_catalog").length)
            && (!$(event.target).closest("#sub_element_sub_menu").length) && (!$(event.target).closest(".inactivebackground").length)
            && (!$(event.target).closest("#zoom_section").length && (!$(event.target).closest("#litebox_close_button").length))) {
            //$('#left_panel_content>.element_catalog').css('background-color', '#636363');
            $('#sub_element_menu').animate({
                width: '0px'
            }, 200);
            $('#sub_element_sub_menu').animate({
                width: '0px'
            }, 200);
            $('.element_menus').hide();
            $('.element_catalog').css('background-color', 'rgba(0,0,0,0)');
            $('.category-menu').css('background-color', '#444');
            $('.zoom').remove();
        }

        if (($(event.target).closest("#workspace").length) && (!$(event.target).closest(".section").length)
             && (!$(event.target).closest("#active_menu").length)
            && (!$(event.target).closest("#delete_element").length) && (!$(event.target).closest("#additionalBlocks").length)) {
            //unselectEl();
        }

        if ((!$(event.target).closest("#landing-parameters").length) && (!$(event.target).closest(".cp-color-picker").length)) {
            $('#parameters_con').hide();
        }

        if ((!$(event.target).closest("#general_menu").length) && (!$(event.target).closest("#general_menu_bnt").length)) {
            hide_menu();
        }

        try {
            if ($(event.target).attr('id') == 'move_form_element' || $(event.target).attr('id') == 'remove_form_element') {
                return;
            }
        } catch (ex31) {
        }

        var evClass = false;
        try {
            if ($(event.target).attr('class').indexOf('containerForm') >= 0)
                evClass = true;
        } catch (ex44) {

        }
        if ($(event.target).closest(".editable_element").length || evClass) {
            //console.log('клик по редактируемому элементу формы');
            selectFormElement($(event.target));
            return;
        }


        if ($(event.target).closest("#workspace").length) {
            $('#active_form').remove();
            $('#formpar_active').remove();
            $('#move_form_element').hide();
            $('#edit_form_element').remove();
            $('#remove_form_element').remove();
        }




        if (($(event.target).closest(".ui-widget-content").length || $(event.target).closest(".section").length || $(event.target).closest(".type_element").length)
            && !$(event.target).closest("#resize").length && !$(event.target).closest("#resize_section").length && !$(event.target).closest("#textarea_text_element").length) {
            if ($(event.target).closest(".ui-widget-content").attr('id') != undefined) {
                clearInterval(interval);
                try {
                    if ((!$(event.target).closest('#resize').length) && !$(event.target).closest('#resize_section').length) {
                        hideTextarea();
                        $(".active").removeClass("active");
                        if ($('#' + $(event.target).closest(".ui-widget-content").attr('id')).attr('class').indexOf('column') < 0) {
                            $('#' + $(event.target).closest(".ui-widget-content").attr('id')).addClass('active');
                        }
                    }
                } catch (ex) {
                    hideTextarea();
                    $(".active").removeClass("active");                    
                }
                select($(event.target).closest(".ui-widget-content").attr('id'));
            } else {
                if ($(event.target).closest(".section").attr('id') != 'sectionContent') {
                    clearInterval(interval);
                    try {
                        if ((!$(event.target).closest('#resize').length) && !$(event.target).closest('#resize_section').length) {
                            hideTextarea();
                            $(".active").removeClass("active");
                            if ($('#' + $(event.target).closest(".section").attr('id')).attr('class').indexOf('column') < 0) {
                                $('#' + $(event.target).closest(".section").attr('id')).addClass('active');
                            }
                        }
                    } catch (ex) {
                        hideTextarea();
                        $(".active").removeClass("active");
                    }
                    select($(event.target).closest(".section").attr('id'));
                }
            }
            if ($(event.target).attr('class') == 'type_element') {
                clearInterval(interval);
                try {
                    if ((!$(event.target).closest('#resize').length) && !$(event.target).closest('#resize_section').length) {
                        hideTextarea();
                        $(".active").removeClass("active");
                        if ($('#' + $(event.target).attr('rel')).attr('class').indexOf('column') < 0) {
                            $('#' + $(event.target).attr('rel')).addClass('active');
                        }
                    }
                } catch (ex) {
                    hideTextarea();
                    $(".active").removeClass("active");
                }
                select($(event.target).attr('rel'));
            }
            return;
        }
        
        $('div[type = "text"]>a').show();

        if (($(event.target).closest(".active").length) || ($(event.target).closest("#active").length)
            || ($(event.target).closest("#general_panel").length) || ($(event.target).closest("#left_panel").length)
            || ($(event.target).closest("#right_panel").length) || ($(event.target).closest("#load_image").length)
            || ($(event.target).closest("#load_icon_litebox").length) || ($(event.target).closest("#additionalBlocks").length)
            || ($(event.target).closest(".cp-xy-slider").length) || ($(event.target).closest(".cp-color-picker").length)
            || ($(event.target).closest("#edit_video").length) || ($(event.target).closest("#timer_option_litebox").length
            || ($(event.target).closest("#editTextElement").length)) || ($(event.target).closest("#unit").length)
            || ($(event.target).closest("#delete_element").length) || ($(event.target).closest("#resize").length)
            || ($(event.target).closest("#map_option_window").length) || ($(event.target).closest("#form_option_window").length)
            || ($(event.target).closest("#resize_section").length) || ($(event.target).closest(".litebox").length)
            || ($(event.target).closest("#save_section_litebox").length) || ($(event.target).closest("#option_panel").length)
            || ($(event.target).closest("#circular_timer_option_window").length) || ($(event.target).closest("#textarea_text_element").length)
            || ($(event.target).closest("#contextMenu").length)) {
            return;
        }
        if ((!$(event.target).closest('#resize').length) && (!$(event.target).closest('#resize_section').length)
            && (!$(event.target).closest('.lite-box').length) && (!$(event.target).closest('.inactivebackground').length)) {
            $('#view-type-block').text('Нет выделения');
            hideTextarea();
            $(".active").removeClass("active");
            $("#active").hide();
            $('#move_block').hide();
            $('#resize_section').hide();
            $('#move_text').hide();
            $('#move-text').hide();
            $('#edit_element').hide();
            $('#option_element').hide();
            $('#remove_element').hide();

            $('#button-option').hide();
            $('#animation-option').hide();
            $('#border-option').hide();
            $('#effect-option').hide();
            $('#url-option').hide();
            $('#text-options').hide();
            $('#background-option').hide();
            $('#size-option').hide();
            $('.inactive-option').show();
        }

        $('#information_if_element').hide();
        $('#button_information_if_element').hide();
        event.stopPropagation();
        clearInterval(interval);
    });
    $(function () {
        $("#rotation_val").keydown(function (event) {
            if (event.keyCode === 38) {
                btn_next_rotation();
            }
            if (event.keyCode === 40) {
                btn_prev_rotation();
            }
        });

        $("#page_content_height_val").keydown(function (event) {
            if (event.keyCode === 38) {
                button_next_height_page();
            }
            if (event.keyCode === 40) {
                button_prev_height_page();
            }
        });

        $("#page_content_width_val").keydown(function (event) {
            if (event.keyCode === 38) {
                button_next_width_page();
            }
            if (event.keyCode === 40) {
                button_prev_width_page();
            }
        });

        $("#page_content_column_val").keydown(function (event) {
            if (event.keyCode === 38) {
                count_column_next();
            }
            if (event.keyCode === 40) {
                count_column_prev();
            }
        });

        $("#font_s").keydown(function (event) {
            if (event.keyCode === 38) {
                button_next_font_size();
            }
            if (event.keyCode === 40) {
                button_prev_font_size();
            }
            saveStep();
        });
        $("#panel_font_s").keydown(function (event) {
            if (event.keyCode === 38) {
                button_next_font_size();
            }
            if (event.keyCode === 40) {
                button_prev_font_size();
            }
            saveStep();
        });
        $("#letter_s").keydown(function (event) {
            if (event.keyCode === 38) {
                button_next_letter_spacing();
            }
            if (event.keyCode === 40) {
                button_prev_letter_spacing();
            }
            saveStep();
        });
        $("#line_h").keydown(function (event) {
            if (event.keyCode === 38) {
                button_next_line_height();
            }
            if (event.keyCode === 40) {
                button_prev_line_height();
            }
            saveStep();
        });
        $("#text_i").keydown(function (event) {
            if (event.keyCode === 38) {
                button_next_text_indent();
            }
            if (event.keyCode === 40) {
                button_prev_text_indent();
            }
            saveStep();
        });
        $("#padding_a").keydown(function (event) {
            if (event.keyCode === 38) {
                button_next_padding_all();
            }
            if (event.keyCode === 40) {
                button_prev_padding_all();
            }
            saveStep();
        });
        $("#padding_l").keydown(function (event) {
            if (event.keyCode === 38) {
                button_next_padding_left();
            }
            if (event.keyCode === 40) {
                button_prev_padding_left();
            }
            saveStep();
        });
        $("#padding_r").keydown(function (event) {
            if (event.keyCode === 38) {
                button_next_padding_right();
            }
            if (event.keyCode === 40) {
                button_prev_padding_right();
            }
            saveStep();
        });
        $("#padding_t").keydown(function (event) {
            if (event.keyCode === 38) {
                button_next_padding_top();
            }
            if (event.keyCode === 40) {
                button_prev_padding_top();
            }
            saveStep();
        });
        $("#padding_b").keydown(function (event) {
            if (event.keyCode === 38) {
                button_next_padding_bottom();
            }
            if (event.keyCode === 40) {
                button_prev_padding_bottom();
            }
            saveStep();
        });
        $("#thickness_border").keydown(function (event) {
            if (event.keyCode === 38) {
                button_next_thickness_border();
            }
            if (event.keyCode === 40) {
                button_prev_thickness_border();
            }
            saveStep();
        });
        $("#b_top").keydown(function (event) {
            if (event.keyCode === 38) {
                button_next_border_top();
            }
            if (event.keyCode === 40) {
                button_prev_border_top();
            }
            saveStep();
        });
        $("#b_left").keydown(function (event) {
            if (event.keyCode === 38) {
                button_next_border_left();
            }
            if (event.keyCode === 40) {
                button_prev_border_left();
            }
            saveStep();
        });
        $("#b_right").keydown(function (event) {
            if (event.keyCode === 38) {
                button_next_border_right();
            }
            if (event.keyCode === 40) {
                button_prev_border_right();
            }
            saveStep();
        });
        $("#b_bottom").keydown(function (event) {
            if (event.keyCode === 38) {
                button_next_border_bottom();
            }
            if (event.keyCode === 40) {
                button_prev_border_bottom();
            }
            saveStep();
        });
        $("#border_radiusi").keydown(function (event) {
            if (event.keyCode === 38) {
                button_next_border_radius();
            }
            if (event.keyCode === 40) {
                button_prev_border_radius();
            }
            saveStep();
        });
        $("#br_t_l").keydown(function (event) {
            if (event.keyCode === 38) {
                button_next_br_top_left();
            }
            if (event.keyCode === 40) {
                button_prev_br_top_left();
            }
            saveStep();
        });
        $("#br_t_r").keydown(function (event) {
            if (event.keyCode === 38) {
                button_next_br_top_right();
            }
            if (event.keyCode === 40) {
                button_prev_br_top_right();
            }
            saveStep();
        });
        $("#br_b_l").keydown(function (event) {
            if (event.keyCode === 38) {
                button_next_br_bottom_left();
            }
            if (event.keyCode === 40) {
                button_prev_br_bottom_left();
            }
            saveStep();
        });
        $("#br_b_r").keydown(function (event) {
            if (event.keyCode === 38) {
                button_next_br_bottom_right();
            }
            if (event.keyCode === 40) {
                button_prev_br_bottom_right();
            }
            saveStep();
        });
        $("#blurrines").keydown(function (event) {
            if (event.keyCode === 38) {
                button_next_blurriness();
            }
            if (event.keyCode === 40) {
                button_prev_blurriness();
            }
            saveStep();
        });
        $("#x").keydown(function (event) {
            if (event.keyCode === 38) {
                button_next_x();
            }
            if (event.keyCode === 40) {
                button_prev_x();
            }
            saveStep();
        });
        $("#y").keydown(function (event) {
            if (event.keyCode === 38) {
                button_next_y();
            }
            if (event.keyCode === 40) {
                button_prev_y();
            }
            saveStep();
        });
        $("#blurrines_shadow").keydown(function (event) {
            if (event.keyCode === 38) {
                button_next_blurriness_shadow();
            }
            if (event.keyCode === 40) {
                button_prev_blurriness_shadow();
            }
            saveStep();
        });
        $("#x_shadow").keydown(function (event) {
            if (event.keyCode === 38) {
                button_next_x_shadow();
            }
            if (event.keyCode === 40) {
                button_prev_x_shadow();
            }
            saveStep();
        });
        $("#y_shadow").keydown(function (event) {
            if (event.keyCode === 38) {
                button_next_y_shadow();
            }
            if (event.keyCode === 40) {
                button_prev_y_shadow();
            }
            saveStep();
        });
        $("#blurrines_lighting").keydown(function (event) {
            if (event.keyCode === 38) {
                button_next_blurriness_lighting();
            }
            if (event.keyCode === 40) {
                button_prev_blurriness_lighting();
            }
            saveStep();
        });
        $("#section_width_val").keydown(function (event) {
            if (event.keyCode === 38) {
                btn_next_section_width();
            }
            if (event.keyCode === 40) {
                btn_prev_section_width();
            }
            saveStep();
        });
        $("#panel_section_width_val").keydown(function (event) {
            if (event.keyCode === 38) {
                btn_next_section_width();
            }
            if (event.keyCode === 40) {
                btn_prev_section_width();
            }
            saveStep();
        });
        $("#section_height_val").keydown(function (event) {
            if (event.keyCode === 38) {
                btn_next_section_height();
            }
            if (event.keyCode === 40) {
                btn_prev_section_height();
            }
            saveStep();
        });
        $("#panel_section_height_val").keydown(function (event) {
            if (event.keyCode === 38) {
                btn_next_section_height();
            }
            if (event.keyCode === 40) {
                btn_prev_section_height();
            }
            saveStep();
        });
        $("#section_top_val").keydown(function (event) {
            if (event.keyCode === 38) {
                btn_next_section_top();
            }
            if (event.keyCode === 40) {
                btn_prev_section_top();
            }
            saveStep();
        });
        $("#width_l_val").keydown(function (event) {
            if (event.keyCode === 38) {
                btnNextWidthL();
            }
            if (event.keyCode === 40) {
                btnPrevWidthL();
            }
            saveStep();
        });
        $("#height_l_val").keydown(function (event) {
            if (event.keyCode === 38) {
                btnNextHeightL();
            }
            if (event.keyCode === 40) {
                btnPrevHeightL();
            }
            saveStep();
        });
        $("#section_left_val").keydown(function (event) {
            if (event.keyCode === 38) {
                btn_next_section_left();
            }
            if (event.keyCode === 40) {
                btn_prev_section_left();
            }
            saveStep();
        });
    });

    var tempZindex = 1;

    $('#move_block').draggable({
        distance: 20,
        containment: "#content_standard",
        scroll: false,
        start: function () {
            // что то при старте

        },
        drag: function (event, ui) {
            //функция срабатывает при перемещении
            if ((ui.offset.left > $('#left_panel').width()) && (ui.offset.top > 70)) {
                $("#active").offset({ left: ui.offset.left });
                var left;
                if (getStyle($('.active'), 'left').indexOf('%') >= 0) {
                    left = (((ui.offset.left - $('.active').parent().offset().left) / $('.active').parent().width()) * 100).toFixed(1) + '%';
                } else {
                    left = (ui.offset.left - $('.active').parent().offset().left).toFixed(0) + 'px';
                }

                $(".active").css('left', left);
                if (parseInt($(".active").css('z-index') != 999999)) {
                    tempZindex = parseInt($(".active").css('z-index'));
                }
                $(".active").css('z-index', '999999');
                //$("#active").offset({ top: ui.offset.top - 25 });
                var top;
                if (getStyle($('.active'), 'top').indexOf('%') >= 0) {
                    if ($('#content').scrollTop() > $('.active').closest('.section').position().top) {
                        top = ((($('#move_block').offset().top - 105 + $('#content').scrollTop() - $('.active').closest('.section').position().top) / $('.active').parent().height()) * 100).toFixed(1) + '%';
                    } else {
                        top = (((($('#move_block').offset().top - 105) - Math.abs($('.active').closest('.section').position().top - $('#content').scrollTop())) / $('.active').parent().height()) * 100).toFixed(1) + '%';
                    }
                } else {
                    if ($('#content').scrollTop() > $('.active').closest('.section').position().top) {
                        top = ($('#move_block').offset().top - 105 + $('#content').scrollTop() - $('.active').closest('.section').position().top).toFixed(0) + 'px';
                    } else {
                        top = (($('#move_block').offset().top - 105) - Math.abs($('.active').closest('.section').position().top - $('#content').scrollTop())).toFixed(0) + 'px';
                    }
                }
                if ($(".active").attr('id').indexOf('btn_close') >= 0 || $('.active').closest('.section').attr('id').indexOf('litebox') >= 0) {
                    $(".active").css('top', parseInt($('#move_block').css('top')) - 20 + 4);
                    $(".active").css('left', parseInt($(".active").css('left')) + 2);
                } else {
                    $(".active").css('top', top);
                }
                //$('#active').css('top', parseFloat($('#move_block').css('top')) + 2);


                var offsetEl = parseInt($('.active').css('left')) + $('.active').parent().offset().left;
                var offsetCs = $('#content_standard').offset();
                var scroll = $('#content_standard').scrollTop();
                var top111 = scroll + $('.active').offset().top - offsetCs.top;
                var left111 = offsetEl - offsetCs.left;

                if (left111 != undefined) {
                    $('#active').css('left', left111 - 1);
                }
                if ($('.active').attr('id').indexOf('btn_close') >= 0 || $('.active').closest('.section').attr('id').indexOf('litebox') >= 0) {
                    $('#active').css('top', $('.active').offset().top - $('#content_standard').offset().top + $('#content_standard').scrollTop());
                } else {
                    $('#active').css('top', $('.active').closest('.section').position().top + parseInt($('.active').css('top')));
                }


                $('#resize').css('top', 0);
                setPositionLeftToElementButtons();

                $('#lineGorisontal').remove();
                $('#lineVertical').remove();
                var widthLineG = parseInt($("#active").css('left'));
                var heightLineV = parseInt($("#active").css('top'));

                var lineGorisontal = document.createElement('div');
                $(lineGorisontal).attr('style', 'position: absolute; font-family: Arial; font-size: 10px; border-top: 1px dotted #428bca; background: rgba(0,0,0,0); color: #428bca; height: 10px; width:' + widthLineG + 'px; text-align: right; top: ' + heightLineV + 'px; left: 0px;');
                $(lineGorisontal).attr('id', 'lineGorisontal');
                $(lineGorisontal).text(widthLineG);
                $('#content').append(lineGorisontal);

                var lineVertical = document.createElement('div');
                if ($('.active').attr('type') == 'anchor') {
                    heightLineV = heightLineV - $('#name_block').height();
                }
                $(lineVertical).attr('style', 'padding: 0; margin: 0; position: absolute; display: table; border-left: 1px dotted #428bca; background: rgba(0,0,0,0); color: #428bca; height: ' + heightLineV + 'px; width:50px; text-align: left; top: 0px; left: ' + widthLineG + 'px;');
                $(lineVertical).attr('id', 'lineVertical');
                var p = document.createElement('p');
                $(p).attr('style', 'padding: 0; margin: 0; display: table-cell; vertical-align: bottom; font-family: Arial; font-size: 10px;  background: rgba(0,0,0,0); color: #428bca;')
                $(p).text(heightLineV);
                $(lineVertical).append(p);
                $('#content').append(lineVertical);
            }
        },
        stop: function (event, ui) {
            //console.log('переместили элемент');
            // что то при окончании перемещения
            $('#lineGorisontal').remove();
            $('#lineVertical').remove();
            var pos = $('#move_block').offset();
            var el = document.elementFromPoint(pos.left - 6, pos.top - 6);
            var elId = '';
            if ($(el).attr('class') == undefined) {
                elId = $(el).closest('.column').attr('id');
            } else {
                if ($(el).attr('class').indexOf('column') >= 0) {
                    elId = $(el).attr('id');
                } else {
                    elId = $(el).closest('.column').attr('id');
                }
            }
            console.log(elId);
            if (elId != undefined) {
                var topElId = $('#' + elId).offset().top;
                var leftElId = $('#' + elId).offset().left;

                var par = $('.active').parent();
                var widthElement = $('.active').width();
                var heightElement = $('.active').height();
                var parId = $(par).attr('id');
                if (elId != parId && elId != undefined) {
                    $('#' + elId).append($('.active'));
                    $('#' + parId + '>.active').remove();
                    var position = $('#active').position();
                    var parent = $('#' + elId).position();
                    var heightEl = (((heightElement + parseFloat($('.active').css('padding-top')) + parseFloat($('.active').css('padding-bottom'))) / $('#' + elId).height()) * 100).toFixed(1);
                    var top;
                    if (getStyle($('.active'), 'top').indexOf('%') >= 0) {
                        top = ((pos.top - topElId) / $('#' + elId).height()) * 100 + '%';
                    } else {
                        top = (pos.top - topElId) + 'px';
                    }
                    var left;
                    if (getStyle($('.active'), 'left').indexOf('%') >= 0) {
                        left = ((pos.left - leftElId) / $('#' + elId).width()) * 100 + '%';
                    } else {
                        left = (pos.left - leftElId) + 'px';
                    }
                    if ($(".active").attr('id').indexOf('btn_close') >= 0 || $('.active').closest('.section').attr('id').indexOf('litebox') >= 0) {
                        $(".active").css('top', parseInt($('#move_block').css('top')) - 20 + 4);
                        $(".active").css('left', $(".active").css('left'));
                    } else {
                        $('.active').css('top', top);
                        $('.active').css('left', left);
                    }
                    try{
                        correctSectionHeight("");
                    }catch(ex){
                    
                    }                    
                    $('#move_block').css('left', $('.active').offset().left - ($('#left_panel').width() + parseInt($('#left_panel').css('border-left')) + parseInt($('#left_panel').css('border-right'))));
                    $('#move_block').css('top', $('.active').closest('.section').position().top + parseInt($('.active').css('top')));
                    $('#move_text').css('left', $('#move-text').offset().left - $('#workspace').offset().left);
                    $('#move_text').css('top', $('#move-text').offset().top - $('#workspace').offset().top - 15);
                    $('#name_block').css('top', -($('#name_block').height()));
                    $('#active').css('top', $('#move_block').css('top'));
                    $('#active').css('left', $('#move_block').css('left'));
                    $('#resize').css('top', 0);
                    setPositionLeftToElementButtons();
                } else {
                    var positions = $('#active').position();
                    var parents = $('#' + parId).position();
                    var tops;
                    if (getStyle($('.active'), 'top').indexOf('%') >= 0) {
                        if ($('#content').scrollTop() > $('.active').closest('.section').position().top) {
                            tops = ((($('#move_block').offset().top - 105 + $('#content').scrollTop() - $('.active').closest('.section').position().top) / $('.active').parent().height()) * 100).toFixed(1) + '%';
                        } else {
                            tops = (((($('#move_block').offset().top - 105) - Math.abs($('.active').closest('.section').position().top - $('#content').scrollTop())) / $('.active').parent().height()) * 100).toFixed(1) + '%';
                        }
                    } else {
                        if ($('#content').scrollTop() > $('.active').closest('.section').position().top) {
                            tops = ($('#move_block').offset().top - 105 + $('#content').scrollTop() - $('.active').closest('.section').position().top).toFixed(0) + 'px';
                        } else {
                            tops = (($('#move_block').offset().top - 105) - Math.abs($('.active').closest('.section').position().top - $('#content').scrollTop())).toFixed(0) + 'px';
                        }
                    }
                    var lefts;
                    if (getStyle($('.active'), 'left').indexOf('%') >= 0) {
                        if ((pos.left - 6 + parseFloat($('#' + elId + '>.active').css('width'))) > ($('#' + elId + '>.active').parent().offset().left + parseFloat($('#' + elId + '>.active').parent().css('width')))) {
                            lefts = (((parseFloat($('#' + elId + '>.active').parent().css('width')) - (parseFloat($('#' + elId + '>.active').css('width')))) / $('#' + elId + '>.active').parent().width()) * 100).toFixed(1) + '%';
                        }
                        if (pos.left - 6 < $('#' + elId + '>.active').closest('.section').offset().left) {
                            lefts = 0 + '%';
                        }
                    } else {
                        if ((pos.left - 6 + parseFloat($('#' + elId + '>.active').css('width'))) > ($('#' + elId + '>.active').parent().offset().left + parseFloat($('#' + elId + '>.active').parent().css('width')))) {
                            lefts = (parseFloat($('#' + elId + '>.active').parent().css('width')) - (parseFloat($('#' + elId + '>.active').css('width')))).toFixed(1) + 'px';
                        }
                        if (pos.left - 6 < $('#' + elId + '>.active').closest('.section').offset().left) {
                            lefts = 0 + 'px';
                        }
                    }
                    var widthElem = ((widthElement / $('.active').parent().width()) * 100).toFixed(1);
                    var heightElem = (((heightElement + parseFloat($('.active').css('padding-top')) + parseFloat($('.active').css('padding-bottom'))) / $('.active').parent().height()) * 100).toFixed(1);

                    if ($(".active").attr('id').indexOf('btn_close') >= 0 || $('.active').closest('.section').attr('id').indexOf('litebox') >= 0) {
                        $(".active").css('top', parseInt($('#move_block').css('top')) - 20 + 4);
                        $(".active").css('left', $(".active").css('left'));
                    } else {
                        $('.active').css('top', tops);
                        $('.active').css('left', lefts);
                    }
                    try {
                        correctSectionHeight("");
                    } catch (ex) {

                    }
                    $('#active').css('left', $('.active').offset().left - ($('#left_panel').width() + parseInt($('#left_panel').css('border-left')) + parseInt($('#left_panel').css('border-right'))));
                    $('#move_block').css('left', $('.active').offset().left - ($('#left_panel').width() + parseInt($('#left_panel').css('border-left')) + parseInt($('#left_panel').css('border-right'))));
                    $('#move_block').css('top', $('.active').closest('.section').position().top + parseInt($('.active').css('top')));
                    $('#name_block').css('top', -($('#name_block').height()));
                    $('#active').css('top', $('#move_block').css('top'));
                    $('#resize').css('top', 0);
                    setPositionLeftToElementButtons();
                }
                $(".active").css('z-index', tempZindex);
            }
            return;
            saveStep();
        }
    });

    function correctSectionHeight(id) {
        //console.log('корректируем высоту секции согласно содержимого');
        if (id != '' && id != undefined) {
            var realSectionHeight = parseFloat($('#' + id).closest('.section').css('height'));
            var neededSectionHeight = $('#' + id).height() + parseFloat($('#' + id).css('top')) + parseInt($('#' + id).css('border-top')) + parseInt($('#' + id).css('border-bottom'));
            if (realSectionHeight < neededSectionHeight) {
                $('#' + id).closest('.section').css('height', neededSectionHeight + 'px')
            }
        } else {
            var realSectionHeight = parseFloat($('.active').closest('.section').css('height'));
            var top = getStyle($('.active'), 'top');
            var tops;
            if (top.indexOf('%') >= 0) {
                tops = $('.active').closest('.section').height() * (parseFloat(top) / 100);
            } else {
                tops = getStyle($('.active'), 'top');
            }
            var neededSectionHeight = parseFloat(tops) + $('.active').height() + parseInt($('.active').css('border-top')) + parseInt($('.active').css('border-bottom'));
            if (realSectionHeight < neededSectionHeight) {
                if (top.indexOf('%') >= 0) {
                    $('.active').css('top', ($('.active').closest('.section').height() - $('.active').height()) / $('.active').closest('.section').height() * 100) + '%';
                }
                $('.active').closest('.section').css('height', neededSectionHeight + 'px');
            }
        }
    }

    $('#move_text').draggable({
        distance: 20,
        containment: "#content_standard",
        scroll: false,
        start: function () {
            // что то при старте

        },
        drag: function (event, ui) {
            //функция срабатывает при перемещении
            $("#active").offset({ left: ui.offset.left - $('.active').width() - 3 });
            var left;
            if (getStyle($('.active'), 'left').indexOf('%') >= 0) {
                left = ((((ui.offset.left - $('.active').parent().offset().left) / $('.active').parent().width()) * 100) - (($('.active').width()) / $('.active').parent().width()) * 100).toFixed(1) + '%';
            } else {
                left = (ui.offset.left - $('.active').parent().offset().left - $('.active').width()).toFixed(0) + 'px';
            }


            if (parseInt($(".active").css('z-index') != 999999)) {
                tempZindex = parseInt($(".active").css('z-index'));
            }
            $(".active").css('z-index', '999999');
            $("#active").offset({ top: ui.offset.top });
            var top;
            if (getStyle($('.active'), 'top').indexOf('%') >= 0) {
                if ($('#content').scrollTop() > $('.active').closest('.section').position().top) {
                    top = ((($('#move_text').offset().top - 105 + $('#content').scrollTop() - $('.active').closest('.section').position().top) / $('.active').parent().height()) * 100).toFixed(1) + '%';
                } else {
                    top = (((($('#move_text').offset().top - 105) - Math.abs($('.active').closest('.section').position().top - $('#content').scrollTop())) / $('.active').parent().height()) * 100).toFixed(1) + '%';
                }
            } else {
                if ($('#content').scrollTop() > $('.active').closest('.section').position().top) {
                    top = ($('#move_text').offset().top - 105 + $('#content').scrollTop() - $('.active').closest('.section').position().top).toFixed(0) + 'px';
                } else {
                    top = (($('#move_text').offset().top - 105) - Math.abs($('.active').closest('.section').position().top - $('#content').scrollTop())).toFixed(0) + 'px';
                }
            }
            $('.active').css('top', top);
            $('.active').css('left', left);

            //$('#lineGorisontal').remove();
            //$('#lineVertical').remove();
            //var widthLineG = parseInt($("#active").css('left'));
            //var heightLineV = parseInt($("#active").css('top'));

            //var lineGorisontal = document.createElement('div');
            //$(lineGorisontal).attr('style', 'position: absolute; font-family: Arial; font-size: 10px; border-top: 1px dotted #428bca; background: rgba(0,0,0,0); color: #428bca; height: 10px; width:' + widthLineG + 'px; text-align: right; top: ' + heightLineV + 'px; left: 0px;');
            //$(lineGorisontal).attr('id', 'lineGorisontal');
            //$(lineGorisontal).text(widthLineG);
            //$('#content').append(lineGorisontal);

            //var lineVertical = document.createElement('div');
            //if ($('.active').attr('type') == 'anchor') {
            //    heightLineV = heightLineV - $('#name_block').height();
            //}
            //$(lineVertical).attr('style', 'padding: 0; margin: 0; position: absolute; display: table; border-left: 1px dotted #428bca; background: rgba(0,0,0,0); color: #428bca; height: ' + heightLineV + 'px; width:50px; text-align: left; top: 0px; left: ' + widthLineG + 'px;');
            //$(lineVertical).attr('id', 'lineVertical');
            //var p = document.createElement('p');
            //$(p).attr('style', 'padding: 0; margin: 0; display: table-cell; vertical-align: bottom; font-family: Arial; font-size: 10px;  background: rgba(0,0,0,0); color: #428bca;')
            //$(p).text(heightLineV);
            //$(lineVertical).append(p);
            //$('#content').append(lineVertical);
        },
        stop: function (event, ui) {
            //console.log('переместили текстовый элемент');
            $('#lineGorisontal').remove();
            $('#lineVertical').remove();
            $("#active").offset({ left: ui.offset.left - $('.active').width() - 3 });
            var left;
            if (getStyle($('.active'), 'left').indexOf('%') >= 0) {
                left = ((((ui.offset.left - $('.active').parent().offset().left) / $('.active').parent().width()) * 100) - (($('.active').width()) / $('.active').parent().width()) * 100).toFixed(1) + '%';
            } else {
                left = (ui.offset.left - $('.active').parent().offset().left - $('.active').width()).toFixed(0) + 'px';
            }


            if (parseInt($(".active").css('z-index') != 999999)) {
                tempZindex = parseInt($(".active").css('z-index'));
            }
            $(".active").css('z-index', '999999');
            $("#active").offset({ top: ui.offset.top });
            var top;
            if (getStyle($('.active'), 'top').indexOf('%') >= 0) {
                if ($('#content').scrollTop() > $('.active').closest('.section').position().top) {
                    top = ((($('#move_text').offset().top - 105 + $('#content').scrollTop() - $('.active').closest('.section').position().top) / $('.active').parent().height()) * 100).toFixed(1) + '%';
                } else {
                    top = (((($('#move_text').offset().top - 105) - Math.abs($('.active').closest('.section').position().top - $('#content').scrollTop())) / $('.active').parent().height()) * 100).toFixed(1) + '%';
                }
            } else {
                if ($('#content').scrollTop() > $('.active').closest('.section').position().top) {
                    top = ($('#move_text').offset().top - 105 + $('#content').scrollTop() - $('.active').closest('.section').position().top).toFixed(0) + 'px';
                } else {
                    top = (($('#move_text').offset().top - 105) - Math.abs($('.active').closest('.section').position().top - $('#content').scrollTop())).toFixed(0) + 'px';
                }
            }
            $('.active').css('top', top);
            $('.active').css('left', left);

            var pos = $('#active').offset();
            var el = document.elementFromPoint(pos.left - 6, pos.top - 6);
            var elId = '';
            if ($(el).attr('class') == undefined) {
                elId = $(el).closest('.column').attr('id');
            } else {
                if ($(el).attr('class').indexOf('column') >= 0) {
                    elId = $(el).attr('id');
                } else {
                    elId = $(el).closest('.column').attr('id');
                }
            }

            var topElId = $('#' + elId).offset().top;
            var leftElId = $('#' + elId).offset().left;

            var par = $('.active').parent();
            var widthElement = $('.active').width();
            var heightElement = $('.active').height();
            var parId = $(par).attr('id');
            if (elId != parId && elId != undefined) {
                $('#' + elId).append($('.active'));
                $('#' + parId + '>.active').remove();
                var position = $('#active').position();
                var parent = $('#' + elId).position();
                var heightEl = (((heightElement + parseFloat($('.active').css('padding-top')) + parseFloat($('.active').css('padding-bottom'))) / $('#' + elId).height()) * 100).toFixed(1);
                var top;
                if (getStyle($('.active'), 'top').indexOf('%') >= 0) {
                    top = ((pos.top - topElId) / $('#' + elId).height()) * 100 + '%';
                } else {
                    top = (pos.top - topElId) + 'px';
                }
                var left;
                if (getStyle($('.active'), 'left').indexOf('%') >= 0) {
                    left = ((pos.left - leftElId) / $('#' + elId).width()) * 100 + '%';
                } else {
                    left = (pos.left - leftElId) + 'px';
                }
                $('.active').css('top', top);
                $('.active').css('left', left);
                $('#move_text').css('left', $('#move-text').offset().left - $('#workspace').offset().left);
                $('#move_text').css('top', $('#move-text').offset().top - $('#workspace').offset().top - 15);
                $('#name_block').css('top', -($('#name_block').height()));
                $('#active').css('top', $('#move_text').css('top'));
                $('#active').css('left', $('#move_text').css('left') - $('.active').width() - 3);
                $('#resize').css('top', 0);
            }
        }
    });

    $('#move_form_element').draggable({
        distance: 20,
        containment: "#content_standard",
        scroll: false,
        start: function () {
            // что то при старте

        },
        drag: function (event, ui) {
            //функция срабатывает при перемещении
            if ($('.active').attr('type') != 'submit') {
                var left = parseInt($('#move_form_element').css('left')) - ($('.edit_form').offset().left - ($('#left_panel').width() + parseInt($('#left_panel').css('border-left')) + parseInt($('#left_panel').css('border-right')))) - $('.active').parent().width() - 2;
                var top = parseInt($('#move_form_element').css('top'));
                $('#active_form').css('left', (left + $('.edit_form').offset().left - ($('#left_panel').width() + parseInt($('#left_panel').css('border-left')) + parseInt($('#left_panel').css('border-right')))) + 'px');
                $('#active_form').css('top', ($('.active').offset().top - 105 + $('#content').scrollTop()) + 'px');
                $('.active').parent().css('left', left + 'px');
                $('.active').parent().css('top', top + 'px');
                $('#formpar_active').css('left', (left + $('.edit_form').offset().left - ($('#left_panel').width() + parseInt($('#left_panel').css('border-left')) + parseInt($('#left_panel').css('border-right')))) + 'px');
                $('#formpar_active').css('top', ($('.active').parent().offset().top - 105 + $('#content').scrollTop()) + 'px');
                $('#edit_form_element').css('left', $('#move_form_element').css('left'));
                $('#edit_form_element').css('top', (parseInt($('#move_form_element').css('top')) + $('#move_form_element').height()) + 'px');
                $('#remove_form_element').css('left', $('#move_form_element').css('left'));
                $('#remove_form_element').css('top', (parseInt($('#move_form_element').css('top')) + $('#move_form_element').height() + $('#edit_form_element').height()) + 'px');
            } else {
                var left = parseInt($('#move_form_element').css('left')) - ($('.edit_form').offset().left - ($('#left_panel').width() + parseInt($('#left_panel').css('border-left')) + parseInt($('#left_panel').css('border-right')))) - $('.active').width() - 2;
                var top = parseInt($('#move_form_element').css('top'));
                $('#active_form').css('left', (left + $('.edit_form').offset().left - ($('#left_panel').width() + parseInt($('#left_panel').css('border-left')) + parseInt($('#left_panel').css('border-right')))) + 'px');
                $('#active_form').css('top', ($('.active').offset().top - 105 + $('#content').scrollTop()) + 'px');
                $('.active').css('left', left + 'px');
                $('.active').css('top', top + 'px');
                $('#edit_form_element').css('left', $('#move_form_element').css('left'));
                $('#edit_form_element').css('top', (parseInt($('#move_form_element').css('top')) + $('#move_form_element').height()) + 'px');
                $('#formpar_active').remove();
                $('#remove_form_element').remove();
            }
        },
        stop: function (event, ui) {
            //console.log('переместили элемент формы');

        }
    });

    $("#resize").resizable({
        //Other options
        create: function (event, ui) { },
        start: function (event, ui) {
            resized = true;
        },
        stop: function (event, ui) {
            $('#widthElement').remove();
            $('#heightElement').remove();
            saveStep();
            resized = false;
        },
        resize: function (event, ui) {
            //console.log('изменили размер элемента');
            var widthActiv = 0;
            if (getStyle($(".active"), 'width').indexOf('%') >= 0) {
                widthActiv = ((($('#resize').width()  - 1) / $('.active').parent().width()) * 100).toFixed(1);
                $(".active").css('width', widthActiv + '%');
            } else {
                widthActiv = $('#resize').width() - 1;
                $(".active").css('width', widthActiv + 'px');
            }

            var heightActiv = 0;
            if (getStyle($(".active"), 'height').indexOf('%') >= 0) {
                heightActiv = ((($('#resize').height() - 1) / $('.active').parent().height()) * 100).toFixed(1);
                $(".active").css('height', heightActiv + '%');
            } else {
                heightActiv = $('#resize').height() - 1;
                $(".active").css('height', heightActiv + 'px');
            }

            $('#move_block').css('width', $('#active').width() - 10);
            $('#move_block').css('height', $('#active').height() - 10);
            var parRightBorder = $('.active').closest('.section').offset().left + $('.active').closest('.section').width();
            var elRightBorder = $('.active').offset().left + $('.active').width();
            var parBottomBorder = $('.active').closest('.section').offset().top + $('.active').closest('.section').height();
            var elBottomBorder = $('.active').offset().top + $('.active').height();
            if ($('.active').attr('type') != 'btn') {
                if (parRightBorder < elRightBorder) {
                    $('.active').css('width', ((($('.active').width() - (elRightBorder - parRightBorder - 2)) / $('.active').parent().width()) * 100).toFixed(1) + '%');
                    $('#move_block').css('width', $('#active').width() - 10);
                    $('.active').children().css('width', '100%');
                }
                if (parBottomBorder < elBottomBorder) {
                    $('.active').css('height', ((($('.active').height() - (elBottomBorder - parBottomBorder - 2)) / $('.active').parent().height()) * 100).toFixed(1) + '%');
                    $('#move_block').css('height', $('#active').height() - 10);
                }
            }

            $('#widthElement').remove();
            $('#heightElement').remove();

            var wTop = $('.active').height() + parseInt($('#active').css('top')) - 8;
            var wLeft = $('.active').width() + parseInt($('#active').css('left')) + 4;
            var hTop = $('.active').height() + parseInt($('#active').css('top')) + 2;
            var hLeft = $('.active').width() + parseInt($('#active').css('left')) - 50;

            var widthEl = document.createElement('div');
            $(widthEl).attr('style', 'font-family: Arial; font-size: 10px; background: rgba(0,0,0,0); color: #428bca; width: 50px; height: 12px; position: absolute; top: ' + wTop + 'px; left: ' + wLeft + 'px;');
            $(widthEl).attr('id', 'widthElement');
            $(widthEl).text(getStyle($('.active'), 'width'));
            $('#content').append(widthEl);

            var heightEl = document.createElement('div');
            $(heightEl).attr('style', 'font-family: Arial; font-size: 10px; background: rgba(0,0,0,0); color: #428bca; text-align: right; width: 50px; height: 12px; position: absolute; top: ' + hTop + 'px; left: ' + hLeft + 'px;');
            $(heightEl).attr('id', 'heightElement');
            $(heightEl).text(getStyle($('.active'), 'height'));
            $('#content').append(heightEl);
            //saveStep();
        }
    });

    $("#resize_section").resizable({
        handles: 's',
        //Other options
        create: function (event, ui) { },
        start: function (event, ui) {

        },
        stop: function (event, ui) {
            //console.log('stop resized section');

        },
        resize: function (event, ui) {
            //console.log('resize section');
            var height = $("#resize_section").height();
            $('.active').css('height', height);
        }
    });

    $('#content').resize(function () {
        //console.log('content resize');
    });

    $(window).resize(function () {
        //console.log('изменили размер окна');
        $('#active_menu').width($('#type_block').width() + $('#edit_block').width() + $('#del_block').width());
        if ($('.active').attr('type') == 'anchor') {
            $('#active_menu').width($('#type_block').width() + $('#del_block').width());
        }
        $('#active').width($('#resize').width());
        //try {
        //    if ($('.active').attr('class').indexOf('ui-widget-content') >= 0) {
        //        $('.active').css('width', (($('#resize').width() / $('.active').parent().width()) * 100).toFixed(1) + '%');
        //    }
        //} catch (ex22) {

        //}
        setPositionLeftToElementButtons();

        if ($('#information_if_element').css('display') != 'none') {
            var total = 0; // всего пунктов меню
            var hide = 0; // скрытых пунктов меню
            var heightViewBlock = 0; // высота не скрытых пунктов меню
            //$('#woc_column_2').children('.woc').each(function () {
            //    total += 1;
            //    if ($(this).css('display') == 'none') {
            //        hide += 1;
            //    } else {
            //        heightViewBlock += $(this).height() + parseInt($(this).css('margin-top')) + parseInt($(this).css('margin-bottom')) + parseInt($(this).css('border-top')) + parseInt($(this).css('border-bottom'));
            //    }
            //});
            if (total != hide) {

                $('#information_if_element').height($('#right_panel_content').height() - heightViewBlock - parseInt($('#information_if_element').css('padding-top')) - parseInt($('#information_if_element').css('padding-bottom')) - 30 - 30);
                if ($('#information_if_element').height() < 60) {
                    $('#information_if_element').css('opacity', '0');
                } else {
                    $('#information_if_element').css('opacity', '1');
                }
                $('#information_if_element').show();
                $('#information_if_element').css('margin-top', '30px');
                $('#button_information_if_element').hide();
            }
        }

    });

    var el = document.getElementById('content_standard');
    el.addEventListener('click', function (event) {

        reload_right_panel();
        counter++;
    });
    var top = $('#resize').css('top');
    var left = $('#resize').css('left');
    if (top != undefined) {
        $('#resize_all').css('top', parseInt(top.replace('px', '')) + $('#resize').height());
        $('#resize_gor').css('top', parseInt(top.replace('px', '')) + $('#resize').height() / 2);
        $('#resize_ver').css('top', parseInt(top.replace('px', '')) + $('#resize').height());
    }
    if (left != undefined) {
        $('#resize_all').css('left', parseInt(left.replace('px', '')) + $('#resize').width());
        $('#resize_gor').css('left', parseInt(left.replace('px', '')) + $('#resize').width());
        $('#resize_ver').css('left', parseInt(left.replace('px', '')) + $('#resize').width() / 2);
    }

    loadPages();
});

//загрузка вкладок страниц согласно имеющихся страниц
function loadPages(param) {
    //console.log('загрузили вкладки страниц согласно имеющимся страницам');
    $("#page_tabs").html('');
    var countPages = 0;
    $('#pages').children().each(function () {
        var del = document.createElement('div');
        $(del).attr('class', 'del_page fa fa-trash-o');
        $(del).attr('title', 'delete');
        $(del).attr('onclick', 'openWindowDelPage(this)');

        var div = document.createElement('div');
        if (countPages == 1) {
            $(div).attr('class', 'page_tab tab_active');
        } else {
            $(div).attr('class', 'page_tab');
        }
        $(div).text($(this).attr('name'));
        if ($(this).attr('type') === 'page' || $(this).attr('type') === 'litebox') {
            div.appendChild(del);
        }
        var href = document.createElement('a');
        $(href).attr('href', '#');
        $(href).attr('onclick', 'selectTab(this)');
        $(href).attr('title', $(this).attr('name'));
        $(href).attr('rel', $(this).attr('id'));

        href.appendChild(div);
        $("#page_tabs").append(href);
        countPages++;
    });
    reload_right_panel();

    var i = document.createElement('i');
    $(i).attr('class', 'fa fa-plus');
    $(i).attr('style', 'font-size: 0.8em; line-height: 1.7;');
    $(i).attr('title', 'add page');
    var div = document.createElement('div');
    $(div).attr('class', 'add_page_tab');
    div.appendChild(i);
    var href = document.createElement('a');
    $(href).attr('href', '#');
    $(href).attr('onclick', 'openWindowAddPage()');
    href.appendChild(div);
    $("#page_tabs").append(href);
}

//удаление вспомогательных блоков из страниц
function removeAdditionalBlocks() {
    //console.log('удаляем лишние блоки управления');
    $('#pages').find('#additionalBlocks').each(function () {
        $(this).remove();
    });
}

//вызываем окно создания страницы
function openWindowAddPage() {
    //console.log('вызываем окно создания страницы');
    showLitebox('addPage_litebox');
}

//закрытие окна создания страницы
function closeWindowAddPage() {
    //console.log('закрываем окна создания страницы');
    $('#addPage_Name').val('');
    $('#addPage_Url').val('');
    hideLitebox();
}


//добавление страницы
function addPage() {
    //console.log('добавляем страницу или вспл. окно');
    if (createPage) {
        var errorUrl = false;
        //проверка на наличие такой страницы
        $('#pages').children().each(function () {
            if ($('#addPage_Url').val().toLowerCase() === $(this).attr('id').toLowerCase()) {
                errorUrl = true;
                return;
            }
        });

        if ($('#addPage_Url').val() == '' || $('#addPage_Name').val() === '' || errorUrl) {
            //подсветка поля с ссылкой не подходяшей для создания страницы
            if ($('#addPage_Url').val() === '' || errorUrl) {
                $("#attention_addPage_url").animate({
                    opacity: 1
                }, 200, function () {
                    $("#attention_addPage_url").animate({
                        opacity: 0
                    }, 200, function () {
                        $("#attention_addPage_url").animate({
                            opacity: 1
                        }, 200, function () {
                            $("#attention_addPage_url").animate({
                                opacity: 0
                            }, 200, function () {
                                $("#attention_addPage_url").animate({
                                    opacity: 1
                                }, 200, function () {
                                    $("#attention_addPage_url").animate({
                                        opacity: 0
                                    }, 2500);
                                });
                            });
                        });
                    });
                });
            }
            //подсветка пустого поля названия страницы 
            if ($('#addPage_Name').val() === '') {
                $("#attention_addPage_name").animate({
                    opacity: 1
                }, 200, function () {
                    $("#attention_addPage_name").animate({
                        opacity: 0
                    }, 200, function () {
                        $("#attention_addPage_name").animate({
                            opacity: 1
                        }, 200, function () {
                            $("#attention_addPage_name").animate({
                                opacity: 0
                            }, 200, function () {
                                $("#attention_addPage_name").animate({
                                    opacity: 1
                                }, 200, function () {
                                    $("#attention_addPage_name").animate({
                                        opacity: 0
                                    }, 2500);
                                });
                            });
                        });
                    });
                });
            }
        } else {
            //созданиие страницы
            var div = document.createElement('div');
            $(div).attr('id', $('#addPage_Url').val());
            $(div).attr('type', 'page');
            $(div).attr('name', $('#addPage_Name').val());
            $(div).attr('desc', '');
            $(div).attr('key', '');
            var divC = document.createElement('div');
            $(divC).attr('class', 'page_content_standard');
            div.appendChild(divC);
            $('#pages').append(div);
            loadPages();//-
            closeWindowAddPage();
            $('#page_tabs').children().each(function () {
                if ($(this).attr('rel') === $(div).attr('id')) {
                    $(this).click();
                    return;
                }
            });
        }
    } else {
        if ($('#addLitebox_name').val() === '') {
            $("#attention_addLitebox_name").animate({
                opacity: 1
            }, 200, function () {
                $("#attention_addLitebox_name").animate({
                    opacity: 0
                }, 200, function () {
                    $("#attention_addLitebox_name").animate({
                        opacity: 1
                    }, 200, function () {
                        $("#attention_addLitebox_name").animate({
                            opacity: 0
                        }, 200, function () {
                            $("#attention_addLitebox_name").animate({
                                opacity: 1
                            }, 200, function () {
                                $("#attention_addLitebox_name").animate({
                                    opacity: 0
                                }, 2500);
                            });
                        });
                    });
                });
            });
            return;
        }
        //созданиие litebox
        var litebox = document.createElement('div');
        var random = Math.round(Math.random() * 10000);
        $(litebox).attr('id', 'litebox_' + random + '_pag');
        $(litebox).attr('type', 'litebox');
        $(litebox).attr('name', $('#addLitebox_name').val());
        $(litebox).attr('desc', '');
        $(litebox).attr('key', '');
        var liteboxC = document.createElement('div');
        $(liteboxC).attr('class', 'page_content_standard');
        litebox.appendChild(liteboxC);
        $('#pages').append(litebox);
        loadPages();
        closeWindowAddPage();
        $('#page_tabs').children().each(function () {
            if ($(this).attr('rel') === $(litebox).attr('id')) {
                $(this).click();
                return;
            }
        });
    }
    $('#addLitebox_name').val('');
    $('#width_l_val').val('600');
    $('#height_l_val').val('400');

    saveStep();
}

//вызов окна удаления страницы
function openWindowDelPage(el) {
    //console.log('вызываем окно удаления страницы');
    showLitebox('delete_page');
}

//удаление страницы
function delPage() {
    //console.log('удаляем страницу');
    $('#' + $('.tab_active').parent().attr('rel')).remove();
    loadPages();
    $('#content_standard').find('.section').each(function () {
        $(this).remove();
    });
    $('#pages>#Home>.page_content_standard').children().each(function () {
        $('#content_standard').append($(this));
    });
    hideLitebox();
    saveStep();
}

//переключение вкладок страниц
function selectTab(tab) {
    //console.log('переключаем вкладки страниц');
    saveChangesForm();
    hideTextarea();
    $(".active").removeClass("active");
    $("#active").hide();
    $('#move_block').hide();
    $('#move_text').hide();
    $('#move-text').hide();
    $('#edit_element').hide();
    $('#option_element').hide();
    $('#remove_element').hide();
    $('#resize_section').hide();
    $('#' + $('.tab_active').parent().attr('rel') + '>.page_content_standard').html($('#content_standard').html());
    removeAdditionalBlocks();
    $('.tab_active').removeClass('tab_active');
    $(tab).children().addClass('tab_active');

    $('#page_tabs').children().each(function () {
        if ($(this).attr('onclick') == '' || $(this).attr('onclick') == undefined) {
            $(this).remove();
        }
    });

    if ($('.tab_active').parent().attr('rel').indexOf('litebox') >= 0) {
        $('#section').hide();
        $('#sections').hide();
    } else {
        $('#section').show();
        $('#sections').show();
    }
    $('#text').show();
    $('#image').show();
    $('#button').show();
    $('#icons').show();
    $('#video').show();
    $('#map').show();
    $('#form').show();
    $('#timers').show();
    $('#anchor').show();
    $('#block').show();
    $('#widgets').show();

    $('#input').hide();
    $('#textarea').hide();
    $('#e-mail').hide();
    $('#url').hide();
    $('#phone').hide();
    $('#file').hide();


    //удаление секций из окна редактора
    $('#content_standard').find('.section').each(function () {
        $(this).remove();
    });

    //загрузка страницы в окно редактора
    $('#' + $(tab).attr('rel') + '>.page_content_standard').find('.section').each(function () {
        $('#content_standard').append(this);
    });

    //Layout
    if ($(tab).attr('rel') == 'Layout' && $('#content_standard>.section').attr('class') == undefined) {
        addSection('sectionContent');
    }

    //если страница пустая, то добавляем секцию
    if ($('#content_standard>.section').attr('class') == undefined) {
        if ($(tab).attr('rel').indexOf('litebox') >= 0) {
            addSection('1');
        } else {
            addSection('2');
        }
    }
    reload_right_panel();
}

var element = '';

function add(el) {
    element = el;
}


function select(id, event) {
    console.log('выбираем элемент');
    clearInterval(interval);

    $('#active').css('transform', 'none');
    hideTextarea();    
    $('#edit_block').hide();
    $('#active_menu').width($('#type_block').width() + $('#del_block').width());
    $('#active').width($('#type_block').width() + $('#del_block').width());
    setPositionLeftToElementButtons();

    $('.hover_border').remove();
    $('.type_element').remove();

    clearInterval(interval);
    $('#section_width').removeClass('inactive');
    $('#section_width_val').removeClass('inactive');
    $('#btn_prev_section_width').removeClass('inactive');
    $('#btn_next_section_width').removeClass('inactive');
    $('#panel_section_width').removeClass('inactive');
    $('#panel_section_width_val').removeClass('inactive');
    $('#panel_btn_prev_section_width').removeClass('inactive');
    $('#panel_btn_next_section_width').removeClass('inactive');
    $('.section').removeClass('ui-resizable');

    backgroundTypeDefinition($('.active'));

    $('#active_menu').hide();
    $('#name_block').hide();
    $('#lighting_block').show();
    $('#select-animate').show();
    $('#delay-animate').show();
    $('#duration-animate').show();
    $('#appearance-animate').show();
    $('#preview-animate').show();
    $('#litebox-animate').hide();
    $('#move_text').hide();
    $('#move-text').hide();
    $('#edit_element').hide();
    $('#positionElement').show();
    $('#rotate').show();
    $('#max_width').show();

    $('#button-option').hide();
    $('#animation-option').hide();
    $('#border-option').hide();
    $('#effect-option').hide();
    $('#url-option').hide();
    $('#text-options').hide();
    $('#background-option').hide();
    $('#size-option').hide();
    $('#move_block').show();
    $('#woc_text').addClass('disabled');
    $('#woc_background').addClass('disabled');
    $('#woc_border').addClass('disabled');
    $('#woc_effect').addClass('disabled');
    $('#woc_url').addClass('disabled');
    $('#woc_param').addClass('disabled');
    $('#woc_animate').addClass('disabled');
    $('#woc_button').addClass('disabled');

    $('.inactive-option').css('display', 'block');
    if (id != undefined) {
        var el = id.split('_')[0];
        switch (el) {
            case 'text':
                $('#move_block').show();
                $('#option_element').hide();
                setTextOption();
                setBackgroundOption();
                setBorderOption();
                setEffectOption();
                setUrlOption();
                setSizeOption();
                setAnimationOption();
                clearUrlOrtion();
                break;
            case 'image':
                setBackgroundOption();
                setBorderOption();
                setEffectOption();
                setUrlOption();
                setSizeOption();
                setAnimationOption();
                clearUrlOrtion();
                setButtonOption();
                break;
            case 'block':
                setBackgroundOption();
                setBorderOption();
                setEffectOption();
                setSizeOption();

                $('#edit_block').hide();
                $('#active_menu').width($('#type_block').width() + $('#del_block').width());
                $('#active').width($('#type_block').width() + $('#del_block').width());
                $('#option_element').hide();
                $('#remove_element').show();
                break;
            case 'button':
                setTextOption();
                setBackgroundOption();
                setBorderOption();
                setEffectOption();
                setUrlOption();
                setSizeOption();
                setAnimationOption();
                setButtonOption();
                clearUrlOrtion();
                break;
            case 'icon':
                setTextOption();
                setBackgroundOption();
                setBorderOption();
                setEffectOption();
                setUrlOption();
                setSizeOption();
                setAnimationOption();

                clearUrlOrtion();
                break;
            case 'video':
                setBorderOption();
                setEffectOption();
                setSizeOption();

                break;
            case 'map':
                setBorderOption();
                setEffectOption();
                setSizeOption();
                break;
            case 'form':
                $('#edit_element').show();
                $('#option_element').hide();
                setBackgroundOption();
                setBorderOption();
                setEffectOption();
                setSizeOption();

                break;
            case 'timer':
                if ($('.active').attr('type') != "circular_timer") {
                    setTextOption();
                    setBackgroundOption();
                    setBorderOption();
                    setEffectOption();
                    setSizeOption();
                    setAnimationOption();

                    clearUrlOrtion();
                }
                break;
            case 'anchor':
                $('#edit_block').hide();
                $('#active_menu').width($('#type_block').width() + $('#del_block').width());
                $('#active').width($('#type_block').width() + $('#del_block').width());
                $('#option_element').hide();
                $('#remove_element').show();
                $('#name_block').show();
                $('#name_block').text($('.active').attr('m-title'));
                break;
            case 'section':
                setBackgroundOption();
                setSizeOption();

                $('#active_menu').show();
                $('#option_element').hide();

                $('#section_width').addClass('inactive');
                $('#section_width_val').addClass('inactive');
                $('#btn_prev_section_width').addClass('inactive');
                $('#btn_next_section_width').addClass('inactive');
                $('#panel_section_width').addClass('inactive');
                $('#panel_section_width_val').addClass('inactive');
                $('#panel_btn_prev_section_width').addClass('inactive');
                $('#panel_btn_next_section_width').addClass('inactive');

                $('.img_align').css('border', '0.5px solid rgba(0,0,0,0)');
                $('.img_align').css('background', 'rgba(0,0,0,0)');
                $('#img_lt').attr('src', '/Areas/Cabinet/Content/images/lt.png');
                $('#img_t').attr('src', '/Areas/Cabinet/Content/images/t.png');
                $('#img_rt').attr('src', '/Areas/Cabinet/Content/images/rt.png');
                $('#img_l').attr('src', '/Areas/Cabinet/Content/images/l.png');
                $('#img_c').attr('src', '/Areas/Cabinet/Content/images/c.png');
                $('#img_r').attr('src', '/Areas/Cabinet/Content/images/r.png');
                $('#img_lb').attr('src', '/Areas/Cabinet/Content/images/lb.png');
                $('#img_b').attr('src', '/Areas/Cabinet/Content/images/b.png');
                $('#img_rb').attr('src', '/Areas/Cabinet/Content/images/rb.png');

                if ($('.active').width() > $('.active').children().width()) {
                    $('#max_width>label>input').prop('checked', true);
                } else {
                    $('#max_width>label>input').prop('checked', false);
                }
                switch ($('.active').css('background-position')) {
                    case '0% 0%':
                        $('#img_lt').parent().css('border', '0.5px solid #428bca');
                        $('#img_lt').parent().css('background', '#2d2d2d');
                        $('#img_lt').attr('src', '/Areas/Cabinet/Content/images/lth.png');
                        break;
                    case '0% 50%':
                        $('#img_t').parent().css('border', '0.5px solid #428bca');
                        $('#img_t').parent().css('background', '#2d2d2d');
                        $('#img_t').attr('src', '/Areas/Cabinet/Content/images/th.png');
                        break;
                    case '0% 100%':
                        $('#img_rt').parent().css('border', '0.5px solid #428bca');
                        $('#img_rt').parent().css('background', '#2d2d2d');
                        $('#img_rt').attr('src', '/Areas/Cabinet/Content/images/rth.png');
                        break;
                    case '50% 0%':
                        $('#img_l').parent().css('border', '0.5px solid #428bca');
                        $('#img_l').parent().css('background', '#2d2d2d');
                        $('#img_l').attr('src', '/Areas/Cabinet/Content/images/lh.png');
                        break;
                    case '50% 50%':
                        $('#img_c').parent().css('border', '0.5px solid #428bca');
                        $('#img_c').parent().css('background', '#2d2d2d');
                        $('#img_c').attr('src', '/Areas/Cabinet/Content/images/ch.png');
                        break;
                    case '50% 100%':
                        $('#img_r').parent().css('border', '0.5px solid #428bca');
                        $('#img_r').parent().css('background', '#2d2d2d');
                        $('#img_r').attr('src', '/Areas/Cabinet/Content/images/rh.png');
                        break;
                    case '100% 0%':
                        $('#img_lb').parent().css('border', '0.5px solid #428bca');
                        $('#img_lb').parent().css('background', '#2d2d2d');
                        $('#img_lb').attr('src', '/Areas/Cabinet/Content/images/lbh.png');
                        break;
                    case '100% 50%':
                        $('#img_b').parent().css('border', '0.5px solid #428bca');
                        $('#img_b').parent().css('background', '#2d2d2d');
                        $('#img_b').attr('src', '/Areas/Cabinet/Content/images/bh.png');
                        break;
                    case '100% 100%':
                        $('#img_rb').parent().css('border', '0.5px solid #428bca');
                        $('#img_rb').parent().css('background', '#2d2d2d');
                        $('#img_rb').attr('src', '/Areas/Cabinet/Content/images/rbh.png');
                        break;
                }
                break;
            case 'litebox':
                setBackgroundOption();
                setSizeOption();
                setAnimationOption();

                $('#active_menu').show();
                $('#option_element').hide();
                $('#select-animate').hide();
                $('#delay-animate').hide();
                $('#duration-animate').hide();
                $('#appearance-animate').hide();
                $('#preview-animate').hide();
                $('#litebox-animate').show();

                $('#section_width').addClass('inactive');
                $('#section_width_val').addClass('inactive');
                $('#btn_prev_section_width').addClass('inactive');
                $('#btn_next_section_width').addClass('inactive');
                $('#panel_section_width').addClass('inactive');
                $('#panel_section_width_val').addClass('inactive');
                $('#panel_btn_prev_section_width').addClass('inactive');
                $('#panel_btn_next_section_width').addClass('inactive');

                $('.img_align').css('border', '0.5px solid rgba(0,0,0,0)');
                $('.img_align').css('background', 'rgba(0,0,0,0)');
                $('#img_lt').attr('src', '/Areas/Cabinet/Content/images/lt.png');
                $('#img_t').attr('src', '/Areas/Cabinet/Content/images/t.png');
                $('#img_rt').attr('src', '/Areas/Cabinet/Content/images/rt.png');
                $('#img_l').attr('src', '/Areas/Cabinet/Content/images/l.png');
                $('#img_c').attr('src', '/Areas/Cabinet/Content/images/c.png');
                $('#img_r').attr('src', '/Areas/Cabinet/Content/images/r.png');
                $('#img_lb').attr('src', '/Areas/Cabinet/Content/images/lb.png');
                $('#img_b').attr('src', '/Areas/Cabinet/Content/images/b.png');
                $('#img_rb').attr('src', '/Areas/Cabinet/Content/images/rb.png');

                if ($('.active').width() > $('.active').children().width()) {
                    $('#max_width>label>input').prop('checked', true);
                } else {
                    $('#max_width>label>input').prop('checked', false);
                }
                switch ($('.active').css('background-position')) {
                    case '0% 0%':
                        $('#img_lt').parent().css('border', '0.5px solid #428bca');
                        $('#img_lt').parent().css('background', '#2d2d2d');
                        $('#img_lt').attr('src', '/Areas/Cabinet/Content/images/lth.png');
                        break;
                    case '0% 50%':
                        $('#img_t').parent().css('border', '0.5px solid #428bca');
                        $('#img_t').parent().css('background', '#2d2d2d');
                        $('#img_t').attr('src', '/Areas/Cabinet/Content/images/th.png');
                        break;
                    case '0% 100%':
                        $('#img_rt').parent().css('border', '0.5px solid #428bca');
                        $('#img_rt').parent().css('background', '#2d2d2d');
                        $('#img_rt').attr('src', '/Areas/Cabinet/Content/images/rth.png');
                        break;
                    case '50% 0%':
                        $('#img_l').parent().css('border', '0.5px solid #428bca');
                        $('#img_l').parent().css('background', '#2d2d2d');
                        $('#img_l').attr('src', '/Areas/Cabinet/Content/images/lh.png');
                        break;
                    case '50% 50%':
                        $('#img_c').parent().css('border', '0.5px solid #428bca');
                        $('#img_c').parent().css('background', '#2d2d2d');
                        $('#img_c').attr('src', '/Areas/Cabinet/Content/images/ch.png');
                        break;
                    case '50% 100%':
                        $('#img_r').parent().css('border', '0.5px solid #428bca');
                        $('#img_r').parent().css('background', '#2d2d2d');
                        $('#img_r').attr('src', '/Areas/Cabinet/Content/images/rh.png');
                        break;
                    case '100% 0%':
                        $('#img_lb').parent().css('border', '0.5px solid #428bca');
                        $('#img_lb').parent().css('background', '#2d2d2d');
                        $('#img_lb').attr('src', '/Areas/Cabinet/Content/images/lbh.png');
                        break;
                    case '100% 50%':
                        $('#img_b').parent().css('border', '0.5px solid #428bca');
                        $('#img_b').parent().css('background', '#2d2d2d');
                        $('#img_b').attr('src', '/Areas/Cabinet/Content/images/bh.png');
                        break;
                    case '100% 100%':
                        $('#img_rb').parent().css('border', '0.5px solid #428bca');
                        $('#img_rb').parent().css('background', '#2d2d2d');
                        $('#img_rb').attr('src', '/Areas/Cabinet/Content/images/rbh.png');
                        break;
                }
                break;

            case 'btn':
                setTextOption();
                setBackgroundOption();
                setBorderOption();
                setEffectOption();
                setSizeOption();
                setAnimationOption();
                setButtonOption();

                $('#lighting_block').hide();
                break;
            default:

        }


        if ($('.active').css('text-decoration') != undefined && $('.active').css('text-decoration') == 'underline') {
            $('#sizeAndDecor>#c').css('background-color', '#515151');
        } else {
            $('#sizeAndDecor>#c').css('background-color', '#2d2d2d');
        }

        if ($('.active').css('font-weight') != undefined && $('.active').css('font-weight') == 'bold') {
            $('#sizeAndDecor>#b').css('background-color', '#515151');
        } else {
            $('#sizeAndDecor>#b').css('background-color', '#2d2d2d');
        }

        if ($('.active').css('font-style') != undefined && $('.active').css('font-style') == 'italic') {
            $('#sizeAndDecor>#i').css('background-color', '#515151');
        } else {
            $('#sizeAndDecor>#i').css('background-color', '#2d2d2d');
        }

        if ($('.active').css('line-height') != undefined) {
            $('#line-h').val(parseInt($('.active').css('line-height')));
        }
        switch ($('#' + id).attr('type')) {
            case 'text':
                $('#type_block').text('текст');
                $('#view-type-block').text('Текст');
                break;
            case 'button':
                $('#type_block').text('кнопка');
                $('#view-type-block').text('Кнопка');
                break;
            case 'image':
                $('#type_block').text('изобр.');
                $('#view-type-block').text('Изображение');
                break;
            case 'icon':
                $('#type_block').text('иконка');
                $('#view-type-block').text('Иконка');
                break;
            case 'video':
                $('#type_block').text('видео');
                $('#view-type-block').text('Видео');
                break;
            case 'timer':
                $('#type_block').text('таймер');
                $('#view-type-block').text('Таймер');
                break;
            case 'circular_timer':
                $('#type_block').text('таймер');
                $('#view-type-block').text('Таймер');
                break;
            case 'anchor':
                $('#type_block').text('якорь');
                $('#view-type-block').text('Якорь');
                break;
            case 'section':
                $('#type_block').text('секция');
                $('#view-type-block').text('Секция');
                break;
            case 'block':
                $('#type_block').text('блок');
                $('#view-type-block').text('Блок');
                break;
            case 'form':
                $('#type_block').text('форма');
                $('#view-type-block').text('Форма');
                break;
            case 'map':
                $('#type_block').text('карта');
                $('#view-type-block').text('Карта');
                break;
            case 'sectionLitebox':
                $('#active_menu').hide();
                break;
        }
        $('#active').show();

        $('#resize').show();
        $('#resize_section').hide();
        if ($('.active').attr('type') == 'anchor' || $('.active').attr('type') == 'section' || $('.active').attr('class').indexOf('column') >= 0) {
            $('#option_element').hide();
            if ($('.active').attr('type') == 'sectionLitebox' || $('.active').attr('type') == 'section' || $('.active').attr('class').indexOf('column') >= 0 || $('.active').attr('id').indexOf('btn_close') >= 0) {
                $('#remove_element').hide();
            } else {
                $('#remove_element').show();
            }
        }

        $('#active').css('z-index', $('.active').css('z-index') - 1);
        if ($('.active').is(":hidden")) {
            $('#active').hide();
            $('#move_block').hide();
            $('#option_element').hide();
            $('#remove_element').hide();

        }


        $('#section_height_val').val($('.active').height());
        $('#panel_section_height_val').val($('#section_height_val').val());
        if (id.indexOf('section') >= 0) {
            $('#section_height_val').next().text('px');
            $('#panel_section_height_val').next().text('px');
            if (getStyle($('.active'), 'width').indexOf('%') >= 0) {
                $('#section_width_val').next().text('%');
                $('#panel_section_width_val').next().text('%');
            } else {
                $('#section_width_val').next().text('px');
                $('#panel_section_width_val').next().text('px');
            }
        }

        if (el != 'section') {
            if ($('.active').is(":hidden")) {
                $('#active').hide();
                $('#move_block').hide();
                $('#option_element').hide();
                $('#remove_element').hide();
            } else {
                if ($('.active').attr('type') == 'block' || $('.active').attr('type') == 'anchor' || $('.active').attr('type') == 'section' || $('.active').attr('type') == 'form' || $('.active').attr('class').indexOf('column') >= 0) {

                    $('#option_element').hide();
                    if ($('.active').attr('type') == 'section' || $('.active').attr('type') == 'sectionLitebox' || $('.active').attr('id').indexOf('btn_close') >= 0) {
                        $('#remove_element').hide();
                        if ($('.active').attr('type') != 'text') {
                            $('#option_element').show();
                        }
                    } else {
                        $('#remove_element').show();
                    }
                } else {
                    if ($('.active').attr('type') != 'text') {
                        $('#option_element').show();
                    }
                    $('#remove_element').show();
                }
                $('#active').show();
                if (id.split('_')[0] != 'text') {
                    $('#move_block').show();
                }
            }

            $('#active').width($('.active').width());
            $('#active').height($('.active').height());
            var offsetEl = parseInt($('#' + id).css('left')) + $('#' + id).parent().offset().left;
            var offsetCs = $('#content_standard').offset();
            var scroll = $('#content_standard').scrollTop();
            var top = scroll + $('#' + id).offset().top - offsetCs.top;
            var left = offsetEl - offsetCs.left;

            if (left != undefined) {
                $('#active').css('left', left);
            }
            if ($('.active').attr('id').indexOf('btn_close') >= 0 || $('.active').closest('.section').attr('id').indexOf('litebox') >= 0) {
                $('#active').css('top', $('#' + id).offset().top - $('#content_standard').offset().top + $('#content_standard').scrollTop());
            } else {
                $('#active').css('top', $('.active').closest('.section').position().top + parseInt($('.active').css('top')));
            }

            $('#resize').css('top', 0);

            if ($('.active').offset().top - $('#content_standard').offset().top <= 26) {
                //$('#active_menu').css('top', $('.active').offset().top - $('#content_standard').offset().top);
                $('#active_menu').css('position', 'absolute');
                $('#active_menu').css('z-index', parseInt($('.active').css('z-index')) + 1);
                $('#active').css('z-index', parseInt($('.active').css('z-index')) + 1);
            } else {
                $('#active_menu').css('position', 'relative');
                $('#active_menu').css('z-index', parseInt($('.active').css('z-index')) + 1);
                $('#active').css('z-index', parseInt($('.active').css('z-index')) + 1);
            }
        } else {
            if ($('.active').width() == $('#content').width()) {
                $('#max_width>label>input').prop('checked', true);
            } else {
                $('#max_width>label>input').prop('checked', false);
            }
            $('#resize').hide();
            $('#textarea_text_element').hide();
            hideTextarea();
            setPositionResizeBlockSection();
            $('#move_block').hide();
            $('#active').css('left', 0);
            var sect = $('#' + id);
            var offset = sect.offset();
            $('#active').offset({ top: offset.top, left: offset.left });
            $('#resize_section').css('left', parseInt($('#active').css('left')) - 1);
            $('#section_background>div>div>div>div').css('background', $('.active').css('background'));


            $('.active').addClass('ui-resizable');
        }
        if (el == 'litebox') {
            $('#resize').hide();
            hideTextarea();
            $('#resize_section').hide();
            $('#move_block').hide();
            $('#active').css('left', 0);
        }

        var par = $('#' + id).parent().offset();
        var posCs = $('#content_standard').offset();
        var thissLeft = parseInt($('#' + id).css('left')) + $('#' + id).parent().offset().left;
        var thissTop = parseInt($('#' + id).css('top')) + $('#' + id).parent().offset().top;
        var tops = thissTop - posCs.top;
        var lefts = thissLeft - posCs.left;
        var scrol = $('#content_standard').scrollTop();


        if (lefts != undefined) {
            $('#move_block').css('left', lefts);
            $('#move_text').css('left', $('#move-text').offset().left - $('#workspace').offset().left);

        }
        if (tops != undefined) {
            $('#move_block').css('top', scrol + tops + 3);
            $('#move_text').css('top', $('#move-text').offset().top - $('#workspace').offset().top - 15);
            $('#name_block').css('top', -($('#name_block').height()));
        }
        $('#move_block').width(parseFloat($('.active').css('width')) - 6);
        $('#move_block').height(parseFloat($('.active').css('height')) - 6);

        $('#resize').width($('#' + id).width() + parseInt($('#' + id).css('border-left-width')) + parseInt($('#' + id).css('border-right-width')) + parseInt($('#' + id).css('padding-left')) + parseInt($('#' + id).css('padding-right')));
        $('#resize').height($('#' + id).height() + parseInt($('#' + id).css('border-top-width')) + parseInt($('#' + id).css('border-bottom-width')) + parseInt($('#' + id).css('padding-top')) + parseInt($('#' + id).css('padding-bottom')));
        setPositionLeftToElementButtons();
    }
    if ($('.active').attr('id') != undefined)
        if ($('.active').attr('type') == 'sectionLitebox' || $('.active').attr('id').indexOf('btn_close') >= 0) {
            $('#remove_element').hide();
            if ($('.active').attr('id').indexOf('btn_close') >= 0) {
                $('#option_element').show();
            } else {
                $('#option_element').hide();
            }
        }

    var total = 0;
    var hide = 0;
    var heightViewBlock = 0;
    $('#woc_column_2').children('.woc').each(function () {
        total += 1;
        if ($(this).css('display') == 'none') {
            hide += 1;
        } else {
            heightViewBlock += $(this).height() + parseInt($(this).css('margin-top')) + parseInt($(this).css('margin-bottom')) + parseInt($(this).css('border-top')) + parseInt($(this).css('border-bottom'));
        }
    });
    if (total != hide) {
        $('#information_if_element').height($('#right_panel_content').height() - heightViewBlock - parseInt($('#information_if_element').css('padding-top')) - parseInt($('#information_if_element').css('padding-bottom')) - 30 - 30);
        $('#info_more>a').attr('onclick', 'showLitebox("information_litebox", "' + $('.active').attr('type') + '")');
        $('#button_information_if_element').attr('onclick', 'showLitebox("information_litebox", "' + $('.active').attr('type') + '")');
        $('#info_content').html('<i class="fa fa-exclamation-circle"></i>&nbsp;' + $('#' + $('.active').attr('type') + '_info').text());
        $('#information_if_element').css('margin-top', '30px');
        if ($('#information_if_element').height() < 60) {
            $('#information_if_element').css('opacity', '0');
        } else {
            $('#information_if_element').css('opacity', '1');
        }
        $('#information_if_element').show();
        $('#button_information_if_element').hide();
    }
}


function btn_next_tranform() {
    var tranform = parseFloat($('#tranform_val').val());
    tranform += 0.1;
    $('#tranform_val').val(tranform.toFixed(1));
    $('.active').css('-webkit-transition', '' + tranform.toFixed(1) + 's');
    $('.active').css('-moz-transition', '' + tranform.toFixed(1) + 's');
    $('.active').css('-o-transition', '' + tranform.toFixed(1) + 's');
    $('.active').css('transition', '' + tranform.toFixed(1) + 's');
    if ($('.active').attr('type') != 'image') {
        saveButtonOption();
    } else {
        saveCssImage();
    }
}

function btn_prev_tranform() {
    var tranform = parseFloat($('#tranform_val').val());
    if (tranform > 0) {
        tranform = tranform - 0.1;
        $('#tranform_val').val(tranform.toFixed(1));
        $('.active').css('-webkit-transition', '' + tranform.toFixed(1) + 's');
        $('.active').css('-moz-transition', '' + tranform.toFixed(1) + 's');
        $('.active').css('-o-transition', '' + tranform.toFixed(1) + 's');
        $('.active').css('transition', '' + tranform.toFixed(1) + 's');
    }
    if ($('.active').attr('type') != 'image') {
        saveButtonOption();
    } else {
        saveCssImage();
    }
}

function button_next_background_position_x() {
    var position_x = parseInt($('#background_position_x_val').val());
    position_x++;
    $('#background_position_x_val').val(position_x);
    $('.active').css('background-position-x', position_x);
}

function button_prev_background_position_x() {
    var position_x = parseInt($('#background_position_x_val').val());
    position_x--;
    $('#background_position_x_val').val(position_x);
    $('.active').css('background-position-x', position_x);
}

function button_next_background_position_y() {
    var position_y = parseInt($('#background_position_y_val').val());
    position_y++;
    $('#background_position_y_val').val(position_y);
    $('.active').css('background-position-y', position_y);
}

function button_prev_background_position_y() {
    var position_y = parseInt($('#background_position_y_val').val());
    position_y--;
    $('#background_position_y_val').val(position_y);
    $('.active').css('background-position-y', position_y);
}

function setPositionResizeBlockSection() {
    var sectionTop = $('.active').position().top;
    var sectionLeft = $('.active').offset().left - ($('#left_panel').width() + parseInt($('#left_panel').css('border-left')) + parseInt($('#left_panel').css('border-right'))) - $('#ruler_ver').width();
    var sectionHeight = $('.active').height();
    var sectionWidth = $('.active').width();
    $('#resize_section').css('top', sectionTop);
    $('#resize_section').css('left', sectionLeft);
    $('#resize_section').width(sectionWidth);
    $('#resize_section').height(sectionHeight);
    $('#resize_section').show();
}

function backgroundTypeDefinition(element) {
    var style = element.css('background');
    if (element.css('background-image') != '' && element.css('background-image') != undefined)
        if (element.css('background-image').indexOf('gradient') >= 0) {
            $('#checkbox_gradient>input').prop('checked', true);
            $('#panel_checkbox_gradient>input').prop('checked', true);
            $('#checkbox_ordinary>input').prop('checked', false);
            $('#panel_checkbox_ordinary>input').prop('checked', false);
            $('#background_color_gradient').show();
            $('#panel_background_color_gradient').show();
            $('#background_color_ordinary').hide();
            $('#panel_background_color_ordinary').hide();
            $('#section_background_position').hide();
            $('#background-position').hide();
            $('#position-background').hide();
            $('#section_background').hide();
            if (style.indexOf('linear') >= 0) {
                var param = style.replace('gradient(', '$').split('$')[1];
                var position = param.split(',')[0];
                var color1 = 'rgb' + param.replace('),', '$').split('$')[0].replace('rgb', '$').split('$')[1] + ')';
                var color2 = param.replace('), ', '$').split('$')[1].replace('))', '$').split('$')[0] + ')';
                $('#edit_background_color_grad1>div>div>div').css('background-color', color1);
                $('#panel_edit_background_color_grad1>div>div>div').css('background-color', color1);
                $('#edit_background_color_grad2>div>div>div').css('background-color', color2);
                $('#panel_edit_background_color_grad2>div>div>div').css('background-color', color2);
                $('.g_position').css('border', '0.5px solid rgba(0, 0, 0, 0)');
                switch (position) {
                    case 'to right bottom':
                        $('#b_tl').parent().css('border', '0.5px solid rgb(66, 139, 202)');
                        $('#panel_b_tl').parent().css('border', '0.5px solid rgb(66, 139, 202)');
                        break;
                    case 'to left bottom':
                        $('#b_tr').parent().css('border', '0.5px solid rgb(66, 139, 202)');
                        $('#panel_b_tr').parent().css('border', '0.5px solid rgb(66, 139, 202)');
                        break;
                    case 'to right':
                        $('#b_l').parent().css('border', '0.5px solid rgb(66, 139, 202)');
                        $('#panel_b_l').parent().css('border', '0.5px solid rgb(66, 139, 202)');
                        break;
                    case 'to left':
                        $('#b_r').parent().css('border', '0.5px solid rgb(66, 139, 202)');
                        $('#panel_b_r').parent().css('border', '0.5px solid rgb(66, 139, 202)');
                        break;
                    case 'to right top':
                        $('#b_bl').parent().css('border', '0.5px solid rgb(66, 139, 202)');
                        $('#panel_b_bl').parent().css('border', '0.5px solid rgb(66, 139, 202)');
                        break;
                    case 'to top':
                        $('#b_b').parent().css('border', '0.5px solid rgb(66, 139, 202)');
                        $('#panel_b_b').parent().css('border', '0.5px solid rgb(66, 139, 202)');
                        break;
                    case 'to left top':
                        $('#b_br').parent().css('border', '0.5px solid rgb(66, 139, 202)');
                        $('#panel_b_br').parent().css('border', '0.5px solid rgb(66, 139, 202)');
                        break;
                    default:
                        $('#b_t').parent().css('border', '0.5px solid rgb(66, 139, 202)');
                        $('#panel_b_t').parent().css('border', '0.5px solid rgb(66, 139, 202)');
                        break;
                }
            } else {
                try {
                    var params = style.replace('radial-gradient(', '$').split('$')[1].replace('))', '$').split('$')[0] + ')';
                    var colors1 = params.replace('), ', '$').split('$')[0] + ')';
                    var colors2 = params.replace('), ', '$').split('$')[1];
                    $('#edit_background_color_grad1>div>div>div').css('background-color', colors1);
                    $('#panel_edit_background_color_grad1>div>div>div').css('background-color', colors1);
                    $('#edit_background_color_grad2>div>div>div').css('background-color', colors2);
                    $('#panel_edit_background_color_grad2>div>div>div').css('background-color', colors2);
                    $('.g_position').css('border', '0.5px solid rgba(0, 0, 0, 0)');
                    $('#b_c').parent().css('border', '0.5px solid rgb(66, 139, 202)');
                    $('#panel_b_c').parent().css('border', '0.5px solid rgb(66, 139, 202)');
                } catch (ex) {
                    //console.log('Error: ' + ex);
                }
            }
        } else if (element.css('background-image') != undefined && element.css('background-image').indexOf('url') >= 0) {
            $('#background_image_section_del').show();
            $('#section_background_position').show();
            $('#position-background').show();
            $('#background-position').show();
            $('#section_background').show();
            $('#checkbox_gradient>input').prop('checked', false);
            $('#panel_checkbox_gradient>input').prop('checked', false);
            $('#checkbox_ordinary>input').prop('checked', true);
            $('#panel_checkbox_ordinary>input').prop('checked', true);
            $('#background_color_gradient').hide();
            $('#panel_background_color_gradient').hide();
            $('#background_color_ordinary').show();
            $('#panel_background_color_ordinary').show();
        } else {
            style = element.css('background-color');
            $('#background_image_section_del').hide();
            $('#section_background_position').hide();
            $('#position-background').hide();
            $('#background-position').hide();
            $('#section_background').hide();
            $('#checkbox_gradient>input').prop('checked', false);
            $('#panel_checkbox_gradient>input').prop('checked', false);
            $('#checkbox_ordinary>input').prop('checked', true);
            $('#panel_checkbox_ordinary>input').prop('checked', true);
            $('#background_color_gradient').hide();
            $('#panel_background_color_gradient').hide();
            $('#background_color_ordinary').show();
            $('#panel_background_color_ordinary').show();
            $('#edit_background_color_text>div>div>div').css('background-color', style);
            $('#panel_edit_background_color_text>div>div>div').css('background-color', style);
        }
    $('#panel__background').css('background-color', $('.active').css('background-color'));
}

function reload_right_panel() {
    //console.log('обновляем правую панель элементов');
    $('#layers_con').empty();
    var sections = 0;
    var columns = 0;
    var blocks = 0;
    var rel = $('.tab_active').parent().attr('rel');
    if (rel == undefined) {
        rel = "";
    }
    if (rel.indexOf('litebox') < 0) {
        $('#content_standard').children(".section").each(function () {
            if ($(this).attr('class').indexOf('sectionContent') < 0) {
                sections++;
                right_panel_list_element(this);
                if ($(this).attr('option') != 0) {
                    $('#content_standard #' + $(this).attr('id') + '>.container_section').children().each(function () {
                        columns++;
                        right_panel_list_element(this);
                        if ($(this).attr('option') != 0) {
                            $(this).children('.ui-widget-content').each(function () {
                                blocks++;
                                right_panel_list_element(this);
                            });
                        }
                    });
                }
            }
        });
    } else {
        $('#content_standard').children(".section").first().find('.column').children('.ui-widget-content').each(function () {
            right_panel_list_element(this);
        });
    }

    if ($('#content_standard').children(".section").length < 1) {
        $('#layers_con').text('На странице нет элементов');
    }
}

function right_panel_list_element(el) {
    //console.log('создаем список элементов в правом окне');
    var div = document.createElement('div');
    div.tagName = $(el).attr('id');
    var p = document.createElement('p');
    if ($(el).attr('id') != undefined) {
        if ($(el).attr('type') == 'btn') {
            return;
        }
        p.textContent = $(el).attr('m-title');
        if ($(el).attr('id').split('_')[0] != 'section') {
            $(div).css('width', '96%');
            $(div).css('float', 'right');
        }
        if ($(el).attr('class').indexOf('column') >= 0) {
            p.textContent = 'Колонка';
            $(div).css('width', '98%');
            $(div).css('float', 'right');
            $(div).removeAttr('onclick');
        }
    }

    $(p).css('padding-left', '5px');
    var spanP = document.createElement('span');
    $(spanP).css('width', '45%');
    $(spanP).attr('onclick', 'sellElement(this, "' + $(el).attr('id') + '")');

    var showTitle = document.createElement('div');
    $(showTitle).attr('style', 'float: left; width: 10%;');
    $(showTitle).attr('title', 'Отображение подсказки');
    var check = document.createElement('i');
    if ($(el).attr('m-title') == $(el).attr('title')) {
        $(check).attr('class', 'fa fa-check-circle-o');
    } else {
        $(check).attr('class', 'fa fa-circle-o');
    }

    $(check).attr('style', 'width: 100%; height: 100%; font-size: 12px;');
    $(check).attr('onclick', 'showTitle(this, "' + $(el).attr('id') + '")');
    $(showTitle).append(check);

    var divH = document.createElement('div');
    var iH = document.createElement('i');
    if (($(el).attr('id') != undefined) && ($(el).attr('id').split('_')[0] != 'section')) {
        if ($(el).attr('class').indexOf('column') >= 0) {
            if ($(el).attr('option') != '0') {
                iH.className = 'fa fa-chevron-down';
            } else {
                iH.className = 'fa fa-chevron-right';
            }
            $(divH).attr('onclick', 'collapseOrExpandEl(this, "' + $(el).attr('id') + '")');
        } else {
            if ($(el).css('display') === 'none') {
                iH.className = 'fa fa-eye-slash';
            } else {
                iH.className = 'fa fa-eye';
            }
            $(divH).attr('onclick', 'hiddeAndShowEl(this, "' + $(el).attr('id') + '")');
        }
    } else {
        if ($(el).attr('option') != '0') {
            iH.className = 'fa fa-chevron-down';
        } else {
            iH.className = 'fa fa-chevron-right';
        }
        $(divH).attr('onclick', 'collapseOrExpandEl(this, "' + $(el).attr('id') + '")');
    }
    $(divH).className = 'hideAndShowEl';
    $(divH).attr('style', 'width:10%; font-size: 12px;');

    var divI = document.createElement('div');
    divI.className = 'sellElement';
    $(divI).attr('onclick', 'sellElement(this, "' + $(el).attr('id') + '")');
    $(divI).attr('style', 'width:10%; font-size: 12px;');

    var i = document.createElement('i');
    if ($(el).attr('id') != undefined) {
        switch ($(el).attr('id').split('_')[0]) {
            case 'text':
                i.className = 'fa fa-font';
                break;
            case 'button':
                i.className = 'fa fa-keyboard-o';
                break;
            case 'image':
                i.className = 'fa fa-picture-o';
                break;
            case 'icon':
                i.className = 'fa fa-smile-o';
                break;
            case 'video':
                i.className = 'fa fa-video-camera';
                break;
            case 'timer':
                i.className = 'fa fa-clock-o';
                break;
            case 'anchor':
                i.className = 'fa fa-anchor';
                break;
            case 'section':
                i.className = 'fa fa-newspaper-o';
                break;
            default: i.className = 'fa fa-columns';
        }
    }


    var spanBtns = document.createElement('span');
    var spanBtnRename = document.createElement('span');
    var spanBtnEdit = document.createElement('span');
    var spanBtnDel = document.createElement('span');
    $(spanBtns).attr('class', 'btns_el');
    var iBtnRename = document.createElement('i');
    iBtnRename.className = 'fa fa-refresh';
    $(iBtnRename).attr('title', 'Rename');
    $(iBtnRename).attr('onclick', 'renameEl("' + $(el).attr('id') + '", this)');
    var iBtnEdit = document.createElement('i');
    iBtnEdit.className = 'fa fa-pencil-square-o';
    $(iBtnEdit).attr('title', 'Edit');
    $(iBtnEdit).attr('onclick', 'editEl("' + $(el).attr('id') + '")');
    var iBtnDel = document.createElement('i');
    iBtnDel.className = 'fa fa-trash-o';
    $(iBtnDel).attr('title', 'Delete');
    $(iBtnDel).attr('onclick', 'deleteEl()');

    spanBtnRename.appendChild(iBtnRename);
    spanBtnEdit.appendChild(iBtnEdit);
    spanBtnDel.appendChild(iBtnDel);
    spanBtns.appendChild(spanBtnRename);
    spanBtns.appendChild(spanBtnEdit);
    spanBtns.appendChild(spanBtnDel);
    divH.appendChild(iH);
    div.appendChild(divH);
    divI.appendChild(i);
    div.appendChild(divI);
    if ($(el).attr('class').indexOf('column') < 0) {
        div.appendChild(showTitle);
    }
    spanP.appendChild(p);
    div.appendChild(spanP);
    if ($(el).attr('class').indexOf('column') < 0) {
        div.appendChild(spanBtns);
    }
    $('#layers_con').append(div);
}

function right_panel(el) {
    //console.log('нажатие на кнопку в правой панели');

    switch (el) {
        case 'widget_options':
            var total = 0;
            var hide = 0;
            $('#woc_column_2').children('.woc').each(function () {
                total += 1;
                if ($(this).css('display') == 'none') {
                    hide += 1;
                }
            });
            if (total == hide) {
                //$('#sel_element').remove();
                //var e = document.createElement('div');
                //var top = $('#woc_column_2').height() / 2 - 7;
                //$(e).attr('style', 'width: 100%; text-align: center; font-size: 14px; height: 15px; position: relative; top: ' + top + 'px;');
                //$(e).text('Выберите элемент');
                //$(e).attr('id', 'sel_element');
                //$('#woc_column_2').append(e);
                $('#information_if_element').hide();
                $('#button_information_if_element').hide();
            }
            break;
        case 'layers':
            reload_right_panel();

            break;
            //case 'parameters':

            //    break;
    }
    $('.right_panel_menu').css('background', '#444');
    $('#' + el).css('background', '#333');
    //$('.right_panel').hide();
    $('#' + el + '_con').show();
}

function button_next_circles_width() {
    var value = parseFloat($('#circles_width_background_val').val()).toFixed(1);
    value = 1 * value + 0.1;
    $('#circles_width_background_val').val(value.toFixed(1));
}

function button_prev_circles_width() {
    var value = parseFloat($('#circles_width_background_val').val()).toFixed(1);
    if (value > 0) {
        value = 1 * value - 0.1;
        $('#circles_width_background_val').val(value.toFixed(1));
    }
}

function button_next_circles_width_foreground() {
    var value = parseFloat($('#circles_width_foreground_val').val()).toFixed(2);
    value = 1 * value + 0.01;
    $('#circles_width_foreground_val').val(value.toFixed(2));
}

function button_prev_circles_width_foreground() {
    var value = parseFloat($('#circles_width_foreground_val').val()).toFixed(2);
    if (value > 0) {
        value = value * 1 - 0.01;
        $('#circles_width_foreground_val').val(value.toFixed(2));
    }
}

function button_next_font_size() {
    if (parseInt($('#font_s').val()) < 199) {
        var font_size = '';
        $('#font_s').val(parseInt($('#font_s').val()) + 1);
        $('#panel_font_s').val(parseInt($('#font_s').val()));
        try {
            var fontSize = getStyle($('.active'), 'font-size');
            if (fontSize.indexOf('%') >= 0) {
                $('.active').css('font-size', parseInt($('#font_s').val()) + '%');
                if ($('.active').attr('type') == 'timer') {
                    $('.active li').css('font-size', parseInt($('#font_s').val()) + '%');
                    $('.active span').css('font-size', parseInt($('#font_s').val()) + '%');
                    $('.active>div>div').css('width', (parseInt($('.active').css('font-size')) / 0.93 + 'px'));
                    $('.active>div>div').css('height', (parseInt($('.active').css('font-size')) / 0.75 + 'px'));
                    var lineH = 100 + parseInt($('#font_s').val()) - 18;
                    $('.active>.timeTo').css('line-height', lineH + '%');
                    $('.active>.timeTo>span').css('line-height', lineH + '%');
                }
                font_size = parseInt($('#font_s').val()) + '%';
            } else if (fontSize.indexOf('px') >= 0) {
                $('.active').css('font-size', parseInt($('#font_s').val()) + 'px');
                if ($('.active').attr('type') == 'timer') {
                    $('.active li').css('font-size', parseInt($('#font_s').val()) + 'px');
                    $('.active span').css('font-size', parseInt($('#font_s').val()) + 'px');
                    $('.active>div>div').css('width', (parseInt($('.active').css('font-size')) / 0.93 + 'px'));
                    $('.active>div>div').css('height', (parseInt($('.active').css('font-size')) / 0.75 + 'px'));
                    var lineH2 = 100 + parseInt($('#font_s').val()) - 18;
                    $('.active>.timeTo').css('line-height', lineH2 + '%');
                    $('.active>.timeTo>span').css('line-height', lineH2 + '%');
                }
                font_size = parseInt($('#font_s').val()) + 'px';
            }
            if ($('.active').attr('type') == 'button' || $('.active').attr('type') == 'btn') {
                saveButtonOption();
            }
        } catch (ex) {
            $('.active').css('font-size', parseInt($('#font_s').val()) + 'px');
            if ($('.active').attr('type') == 'button' || $('.active').attr('type') == 'btn') {
                saveButtonOption();
            }
            if ($('.active').attr('type') == 'timer') {
                $('.active li').css('font-size', parseInt($('#font_s').val()) + 'px');
                $('.active span').css('font-size', parseInt($('#font_s').val()) + 'px');
                $('.active>div>div').css('width', (parseInt($('.active').css('font-size')) / 0.93 + 'px'));
                $('.active>div>div').css('height', (parseInt($('.active').css('font-size')) / 0.75 + 'px'));
                var lineH3 = 100 + parseInt($('#font_s').val()) - 18;
                $('.active>.timeTo').css('line-height', lineH3 + '%');
                $('.active>.timeTo>span').css('line-height', lineH3 + '%');
            }
            font_size = parseInt($('#font_s').val()) + 'px';
        }
        $('#textarea_text_element').css('font-size', $('.active').css('font-size'));
        groupProperty('font-size', font_size);
    }
    correctSizeTextElement();
    saveStep();
}

function button_prev_font_size() {
    if (parseInt($('#font_s').val()) > 1) {
        var font_size = '';
        $('#font_s').val(parseInt($('#font_s').val()) - 1);
        $('#panel_font_s').val(parseInt($('#font_s').val()));
        try {
            var fontSize = getStyle($('.active'), 'font-size');
            if (fontSize.indexOf('%') >= 0) {
                $('.active').css('font-size', parseInt($('#font_s').val()) + '%');
                if ($('.active').attr('type') == 'timer') {
                    $('.active li').css('font-size', parseInt($('#font_s').val()) + '%');
                    $('.active span').css('font-size', parseInt($('#font_s').val()) + '%');
                    $('.active>div>div').css('width', (parseInt($('.active').css('font-size')) / 0.93 + 'px'));
                    $('.active>div>div').css('height', (parseInt($('.active').css('font-size')) / 0.75 + 'px'));
                    var lineH = 100 + parseInt($('#font_s').val()) - 18;
                    $('.active>.timeTo').css('line-height', lineH + '%');
                    $('.active>.timeTo>span').css('line-height', lineH + '%');
                }
                font_size = parseInt($('#font_s').val()) + '%';
            } else if (fontSize.indexOf('px') >= 0) {
                $('.active').css('font-size', parseInt($('#font_s').val()) + 'px');
                if ($('.active').attr('type') == 'timer') {
                    $('.active li').css('font-size', parseInt($('#font_s').val()) + 'px');
                    $('.active span').css('font-size', parseInt($('#font_s').val()) + 'px');
                    $('.active>div>div').css('width', (parseInt($('.active').css('font-size')) / 0.93 + 'px'));
                    $('.active>div>div').css('height', (parseInt($('.active').css('font-size')) / 0.75 + 'px'));
                    var lineH2 = 100 + parseInt($('#font_s').val()) - 18;
                    $('.active>.timeTo').css('line-height', lineH2 + '%');
                    $('.active>.timeTo>span').css('line-height', lineH2 + '%');
                }
                font_size = parseInt($('#font_s').val()) + 'px';
            }
        } catch (ex) {
            $('.active').css('font-size', parseInt($('#font_s').val()) + 'px');
            if ($('.active').attr('type') == 'timer') {
                $('.active li').css('font-size', parseInt($('#font_s').val()) + 'px');
                $('.active span').css('font-size', parseInt($('#font_s').val()) + 'px');
                $('.active>div>div').css('width', (parseInt($('.active').css('font-size')) / 0.93 + 'px'));
                $('.active>div>div').css('height', (parseInt($('.active').css('font-size')) / 0.75 + 'px'));
                var lineH3 = 100 + parseInt($('#font_s').val()) - 18;
                $('.active>.timeTo').css('line-height', lineH3 + '%');
                $('.active>.timeTo>span').css('line-height', lineH3 + '%');
            }
            font_size = parseInt($('#font_s').val()) + '%';
        }
        $('#textarea_text_element').css('font-size', $('.active').css('font-size'));
        groupProperty('font-size', font_size);
    }
    correctSizeTextElement();
    saveStep();
}

function button_next_padding_all() {
    if (parseInt($('#padding_a').val()) < 199) {
        $('#padding_a').val(parseInt($('#padding_a').val()) + 1);
        $('.active').css('padding', parseInt($('#padding_a').val()) + 'px');
        $('#resize').width(parseFloat($('.active').css('width')));
        $('#resize').height(parseFloat($('.active').css('height')));
        setPositionLeftToElementButtons();
        $('#move_block').width(parseFloat($('.active').css('width')) - 6);
        $('#move_block').height(parseFloat($('.active').css('height')) - 6);
        var top = (parseFloat($('.active').css('top')) / parseFloat($('.active').parent().css('height'))) * 100;
        var left = (parseFloat($('.active').css('left')) / parseFloat($('.active').parent().css('width'))) * 100;
        var width = (parseFloat($('.active').css('width')) / parseFloat($('.active').parent().css('width'))) * 100;
        var height = (parseFloat($('.active').css('height')) / parseFloat($('.active').parent().css('height'))) * 100;
        if (top + height > 100) {
            $('.active').css('top', 100 - height + '%');
            $('#active').css('top', $('.active').parent().position().top + parseFloat($('.active').css('top')));
            $('#move_block').css('top', $('.active').parent().position().top + parseFloat($('.active').css('top')));
            $('#move_text').css('top', $('#move-text').offset().top - $('#workspace').offset().top - 15);
            $('#name_block').css('top', -($('#name_block').height()));
        }
        if (left + width > 100) {
            $('.active').css('left', 100 - width + '%');
            $('#active').css('left', $('.active').offset().left - $('#content_standard').offset().left);
            $('#move_block').css('left', $('.active').offset().left - $('#content_standard').offset().left);
            $('#move_text').css('left', $('#move-text').offset().left - $('#workspace').offset().left);
        }
        correctSizeTextElement()
        groupProperty('padding', parseInt($('#padding_a').val()) + 'px');
        saveStep();
    }
}

function button_prev_padding_all() {
    if (parseInt($('#padding_a').val()) >= 1) {
        $('#padding_a').val(parseInt($('#padding_a').val()) - 1);
        $('.active').css('padding', parseInt($('#padding_a').val()) + 'px');
        $('#resize').width(parseFloat($('.active').css('width')));
        $('#resize').height(parseFloat($('.active').css('height')));
        setPositionLeftToElementButtons();
        $('#move_block').width(parseFloat($('.active').css('width')) - 6);
        $('#move_block').height(parseFloat($('.active').css('height')) - 6);
        correctSizeTextElement()
        groupProperty('padding', parseInt($('#padding_a').val()) + 'px');
        saveStep();
    }
}

function setPositionLeftToElementButtons() {
    var left = $('.active').width() + parseInt($('.active').css('padding-left')) + parseInt($('.active').css('padding-right')) + parseInt($('.active').css('border-left-width')) + parseInt($('.active').css('border-right-width')) + 1;
    $('#move-text').css('left', left);
    $('#edit_element').css('left', left);
    $('#option_element').css('left', left);
    $('#remove_element').css('left', left);
}

function button_next_padding_left() {
    if (parseInt($('#padding_l').val()) < 199) {
        $('#padding_l').val(parseInt($('#padding_l').val()) + 1);
        $('.active').css('padding-left', parseInt($('#padding_l').val()) + 'px');
        $('#resize').width(parseFloat($('.active').css('width')));
        $('#resize').height(parseFloat($('.active').css('height')));
        setPositionLeftToElementButtons();
        $('#move_block').width(parseFloat($('.active').css('width')) - 6);
        $('#move_block').height(parseFloat($('.active').css('height')) - 6);
        correctSizeTextElement()
        groupProperty('padding-left', parseInt($('#padding_l').val()) + 'px');
        saveStep();
    }
}

function button_prev_padding_left() {
    if (parseInt($('#padding_l').val()) >= 1) {
        $('#padding_l').val(parseInt($('#padding_l').val()) - 1);
        $('.active').css('padding-left', parseInt($('#padding_l').val()) + 'px');
        $('#resize').width(parseFloat($('.active').css('width')));
        $('#resize').height(parseFloat($('.active').css('height')));
        setPositionLeftToElementButtons();
        $('#move_block').width(parseFloat($('.active').css('width')) - 6);
        $('#move_block').height(parseFloat($('.active').css('height')) - 6);
        correctSizeTextElement()
        groupProperty('padding-left', parseInt($('#padding_l').val()) + 'px');
        saveStep();
    }
}

function button_next_padding_right() {
    if (parseInt($('#padding_r').val()) < 199) {
        $('#padding_r').val(parseInt($('#padding_r').val()) + 1);
        $('.active').css('padding-right', parseInt($('#padding_r').val()) + 'px');
        correctSizeTextElement()
        groupProperty('padding-right', parseInt($('#padding_r').val()) + 'px');
        saveStep();
    }
}

function button_prev_padding_right() {
    if (parseInt($('#padding_r').val()) >= 1) {
        $('#padding_r').val(parseInt($('#padding_r').val()) - 1);
        $('.active').css('padding-right', parseInt($('#padding_r').val()) + 'px');
        $('#resize').width(parseFloat($('.active').css('width')));
        $('#resize').height(parseFloat($('.active').css('height')));
        setPositionLeftToElementButtons();
        $('#move_block').width(parseFloat($('.active').css('width')) - 6);
        $('#move_block').height(parseFloat($('.active').css('height')) - 6);
        correctSizeTextElement()
        groupProperty('padding-right', parseInt($('#padding_r').val()) + 'px');
        saveStep();
    }
}

function button_next_padding_top() {
    if (parseInt($('#padding_t').val()) < 199) {
        $('#padding_t').val(parseInt($('#padding_t').val()) + 1);
        $('.active').css('padding-top', parseInt($('#padding_t').val()) + 'px');
        $('#resize').width(parseFloat($('.active').css('width')));
        $('#resize').height(parseFloat($('.active').css('height')));
        setPositionLeftToElementButtons();
        $('#move_block').width(parseFloat($('.active').css('width')) - 6);
        $('#move_block').height(parseFloat($('.active').css('height')) - 6);
        correctSizeTextElement()
        groupProperty('padding-top', parseInt($('#padding_t').val()) + 'px');
        saveStep();
    }
}

function button_prev_padding_top() {
    if (parseInt($('#padding_t').val()) >= 1) {
        $('#padding_t').val(parseInt($('#padding_t').val()) - 1);
        $('.active').css('padding-top', parseInt($('#padding_t').val()) + 'px');
        correctSizeTextElement();
        groupProperty('padding-top', parseInt($('#padding_t').val()) + 'px');
        saveStep();
    }
}

function button_next_padding_bottom() {
    if (parseInt($('#padding_b').val()) < 199) {
        $('#padding_b').val(parseInt($('#padding_b').val()) + 1);
        $('.active').css('padding-bottom', parseInt($('#padding_b').val()) + 'px');
        correctSizeTextElement()
        groupProperty('padding-bottom', parseInt($('#padding_b').val()) + 'px');
        saveStep();
    }
}

function button_prev_padding_bottom() {
    if (parseInt($('#padding_b').val()) >= 1) {
        $('#padding_b').val(parseInt($('#padding_b').val()) - 1);
        $('.active').css('padding-bottom', parseInt($('#padding_b').val()) + 'px');
        correctSizeTextElement()
        groupProperty('padding-bottom', parseInt($('#padding_b').val()) + 'px');
        saveStep();
    }
}

function button_next_line_height() {
    if (parseFloat($('#line_h').val()) < 150) {
        var line_h = '';
        if (getStyle($('.active'), 'line-height').indexOf('%') >= 0) {
            $('#line_h').val(parseFloat($('#line_h').val()) + 0.1);
            line_h = $('#line_h').val() + '%';
            $('.active').css('line-height', line_h);
            $('.active>a').css('line-height', line_h);
        } else {
            $('#line_h').val(parseInt($('#line_h').val()) + 1);
            line_h = $('#line_h').val() + 'px';
            $('.active').css('line-height', line_h);
            $('.active>a').css('line-height', line_h);
        }
        correctSizeTextElement()
        groupProperty('line-height', line_h);
        saveStep();
    }
}

function button_prev_line_height() {
    if (parseFloat($('#line_h').val()) > 0.1) {
        var line_h = '';
        if (getStyle($('.active'), 'line-height').indexOf('%') >= 0) {
            $('#line_h').val(parseFloat($('#line_h').val()) - 0.1);
            line_h = $('#line_h').val() + '%';
            $('.active').css('line-height', line_h);
            $('.active>a').css('line-height', line_h);
        } else {
            $('#line_h').val(parseInt($('#line_h').val()) - 1);
            line_h = $('#line_h').val() + 'px';
            $('.active').css('line-height', line_h);
            $('.active>a').css('line-height', line_h);
        }
        correctSizeTextElement();
        groupProperty('line-height', line_h);
        saveStep();
    }
}

function button_next_letter_spacing() {
    if (parseFloat($('#letter_s').val()) < 50) {
        $('#letter_s').val((parseFloat($('#letter_s').val()) + 0.1).toFixed(1));
        $('.active').css('letter-spacing', parseFloat($('#letter_s').val()) + 'px');
        groupProperty('letter-spacing', parseFloat($('#letter_s').val()) + 'px');
        correctSizeTextElement();
        correctPosition();
        saveStep();
    }
}

function button_prev_letter_spacing() {
    var letter_s = '';
    if (parseFloat($('#letter_s').val()) > 0.2) {
        $('#letter_s').val((parseFloat($('#letter_s').val()) - 0.1).toFixed(1));
        letter_s = parseFloat($('#letter_s').val()) + 'px';
        $('.active').css('letter-spacing', letter_s);
    } else {
        letter_s = '0px';
        $('#letter_s').val(0);
        $('.active').css('letter-spacing', letter_s);
    }
    correctSizeTextElement()
    groupProperty('letter-spacing', letter_s);
    correctPosition();
    saveStep();
}

function button_next_text_indent() {
    if (parseInt($('#text_i').val()) < 50) {
        var text_i = '';
        var textI = getStyle($('.active'), 'text-indent');
        if (textI.indexOf('%') >= 0) {
            $('#text_i').val(parseFloat($('#text_i').val()) + 1);
            text_i = parseInt($('#text_i').val()) + '%';
            $('.active').css('text-indent', text_i);
        } else {
            $('#text_i').val(parseFloat($('#text_i').val()) + 1);
            text_i = parseInt($('#text_i').val()) + 'px';
            $('.active').css('text-indent', text_i);
        }
        correctSizeTextElement()
        groupProperty('text-indent', text_i);
        saveStep();
    }
}

function button_prev_text_indent() {
    if (parseInt($('#text_i').val()) > 0) {
        var text_i = '';
        var textI = getStyle($('.active'), 'text-indent');
        if (textI.indexOf('%') >= 0) {
            $('#text_i').val(parseFloat($('#text_i').val()) - 1);
            text_i = parseInt($('#text_i').val()) + '%';
            $('.active').css('text-indent', parseInt($('#text_i').val()) + '%');
        } else {
            $('#text_i').val(parseInt($('#text_i').val()) - 1);
            text_i = parseInt($('#text_i').val()) + 'px';
            $('.active').css('text-indent', parseInt($('#text_i').val()) + 'px');
        }
        correctSizeTextElement()
        groupProperty('text-indent', text_i);
        saveStep();
    }
}

function button_next_blurriness() {
    if (parseInt($('#blurrines').val()) < 50) {
        $('#blurrines').val(parseInt($('#blurrines').val()) + 1);

        var el = $('.active').attr('id').split('_')[0];
        if (el === 'text') {
            $('.active').children().first().css('text-shadow', $('#x').val() + 'px ' + $('#y').val() + 'px ' + $('#blurrines').val() + 'px ' + $('#color_shadow>div>div>div').css('background-color'));
        } else {
            $('.active span').css('text-shadow', $('#x').val() + 'px ' + $('#y').val() + 'px ' + $('#blurrines').val() + 'px ' + $('#color_shadow>div>div>div').css('background-color'));
        }
        saveStep();
    }
}

function button_prev_blurriness() {
    if (parseInt($('#blurrines').val()) > 1) {
        $('#blurrines').val(parseInt($('#blurrines').val()) - 1);
        var el = $('.active').attr('id').split('_')[0];
        if (el === 'text') {
            $('.active').children().first().css('text-shadow', $('#x').val() + 'px ' + $('#y').val() + 'px ' + $('#blurrines').val() + 'px ' + $('#color_shadow>div>div>div').css('background-color'));
        } else {
            $('.active span').css('text-shadow', $('#x').val() + 'px ' + $('#y').val() + 'px ' + $('#blurrines').val() + 'px ' + $('#color_shadow>div>div>div').css('background-color'));
        }
        saveStep();
    }
}


function button_next_blurriness_shadow() {
    if (parseInt($('#blurrines_shadow').val()) < 50) {
        $('#blurrines_shadow').val(parseInt($('#blurrines_shadow').val()) + 1);

        $('.active').css('box-shadow', $('#x_shadow').val() + 'px ' + $('#y_shadow').val() + 'px ' + $('#blurrines_shadow').val() + 'px ' + $('#block_shadows>div>div>div').css('background-color'));

        saveStep();
    }
}

function button_prev_blurriness_shadow() {
    if (parseInt($('#blurrines_shadow').val()) > 1) {
        $('#blurrines_shadow').val(parseInt($('#blurrines_shadow').val()) - 1);
        $('.active').css('box-shadow', $('#x_shadow').val() + 'px ' + $('#y_shadow').val() + 'px ' + $('#blurrines_shadow').val() + 'px ' + $('#block_shadows>div>div>div').css('background-color'));
        saveStep();
    }
}

function button_next_x() {
    if (parseInt($('#x').val()) < 50) {
        $('#x').val(parseInt($('#x').val()) + 1);
        var el = $('.active').attr('id').split('_')[0];
        if (el === 'text') {
            $('.active').children().first().css('text-shadow', $('#x').val() + 'px ' + $('#y').val() + 'px ' + $('#blurrines').val() + 'px ' + $('#color_shadow>div>div>div').css('background-color'));
        } else {
            $('.active span').css('text-shadow', $('#x').val() + 'px ' + $('#y').val() + 'px ' + $('#blurrines').val() + 'px ' + $('#color_shadow>div>div>div').css('background-color'));
        }
        saveStep();
    }
}

function button_prev_x() {
    if (parseInt($('#x').val()) > -50) {
        $('#x').val(parseInt($('#x').val()) - 1);
        var el = $('.active').attr('id').split('_')[0];
        if (el === 'text') {
            $('.active').children().first().css('text-shadow', $('#x').val() + 'px ' + $('#y').val() + 'px ' + $('#blurrines').val() + 'px ' + $('#color_shadow>div>div>div').css('background-color'));
        } else {
            $('.active span').css('text-shadow', $('#x').val() + 'px ' + $('#y').val() + 'px ' + $('#blurrines').val() + 'px ' + $('#color_shadow>div>div>div').css('background-color'));
        }
        saveStep();
    }
}

function button_next_x_shadow() {
    if (parseInt($('#x_shadow').val()) < 50) {
        $('#x_shadow').val(parseInt($('#x_shadow').val()) + 1);
        $('.active').css('box-shadow', $('#x_shadow').val() + 'px ' + $('#y_shadow').val() + 'px ' + $('#blurrines_shadow').val() + 'px ' + $('#block_shadows>div>div>div').css('background-color'));
        saveStep();
    }
}

function button_prev_x_shadow() {
    if (parseInt($('#x_shadow').val()) > -50) {
        $('#x_shadow').val(parseInt($('#x_shadow').val()) - 1);

        $('.active').css('box-shadow', $('#x_shadow').val() + 'px ' + $('#y_shadow').val() + 'px ' + $('#blurrines_shadow').val() + 'px ' + $('#block_shadows>div>div>div').css('background-color'));
        saveStep();
    }
}

function button_next_y() {
    if (parseInt($('#y').val()) < 50) {
        $('#y').val(parseInt($('#y').val()) + 1);
        var el = $('.active').attr('id').split('_')[0];
        if (el === 'text') {
            $('.active').children().first().css('text-shadow', $('#x').val() + 'px ' + $('#y').val() + 'px ' + $('#blurrines').val() + 'px ' + $('#color_shadow>div>div>div').css('background-color'));
        } else {
            $('.active span').css('text-shadow', $('#x').val() + 'px ' + $('#y').val() + 'px ' + $('#blurrines').val() + 'px ' + $('#color_shadow>div>div>div').css('background-color'));
        }
        saveStep();
    }
}

function button_prev_y() {
    if (parseInt($('#y').val()) > -50) {
        $('#y').val(parseInt($('#y').val()) - 1);
        var el = $('.active').attr('id').split('_')[0];
        if (el === 'text') {
            $('.active').children().first().css('text-shadow', $('#x').val() + 'px ' + $('#y').val() + 'px ' + $('#blurrines').val() + 'px ' + $('#color_shadow>div>div>div').css('background-color'));
        } else {
            $('.active span').css('text-shadow', $('#x').val() + 'px ' + $('#y').val() + 'px ' + $('#blurrines').val() + 'px ' + $('#color_shadow>div>div>div').css('background-color'));
        }
        saveStep(); ////console.log('saveStep - ' + 134);
    }
}

function button_next_y_shadow() {
    if (parseInt($('#y_shadow').val()) < 50) {
        $('#y_shadow').val(parseInt($('#y_shadow').val()) + 1);
        $('.active').css('box-shadow', $('#x_shadow').val() + 'px ' + $('#y_shadow').val() + 'px ' + $('#blurrines_shadow').val() + 'px ' + $('#block_shadows>div>div>div').css('background-color'));
        saveStep();
    }
}

function button_prev_y_shadow() {
    if (parseInt($('#y_shadow').val()) > -50) {
        $('#y_shadow').val(parseInt($('#y_shadow').val()) - 1);
        $('.active').css('box-shadow', $('#x_shadow').val() + 'px ' + $('#y_shadow').val() + 'px ' + $('#blurrines_shadow').val() + 'px ' + $('#block_shadows>div>div>div').css('background-color'));
        saveStep();
    }
}

function button_next_blurriness_lighting() {
    if (parseInt($('#blurrines_lighting').val()) < 50) {
        $('#blurrines_lighting').val(parseInt($('#blurrines_lighting').val()) + 1);
        $('.active').css('box-shadow', $('#lighting').val() + ' 0px 0px ' + $('#blurrines_lighting').val() + 'px ' + $('#block_lightings>div>div>div').css('background-color'));
        saveStep();
    }
}

function button_prev_blurriness_lighting() {
    if (parseInt($('#blurrines_lighting').val()) > 0) {
        $('#blurrines_lighting').val(parseInt($('#blurrines_lighting').val()) - 1);
        $('.active').css('box-shadow', $('#lighting').val() + ' 0px 0px ' + $('#blurrines_lighting').val() + 'px ' + $('#block_lightings>div>div>div').css('background-color'));
        saveStep(); 
    }
}

function button_next_blurriness_lighting_text() {
    if (parseInt($('#blurrines_lighting_text').val()) < 50) {
        $('#blurrines_lighting_text').val(parseInt($('#blurrines_lighting_text').val()) + 1);
        $('.active').css('text-shadow', '0px 0px ' + $('#blurrines_lighting_text').val() + 'px ' + $('#text_lightings>div>div>div').css('background-color'));
        saveStep(); 
    }
}

function button_prev_blurriness_lighting_text() {
    if (parseInt($('#blurrines_lighting_text').val()) > 0) {
        $('#blurrines_lighting_text').val(parseInt($('#blurrines_lighting_text').val()) - 1);
        $('.active').css('text-shadow', '0px 0px ' + $('#blurrines_lighting_text').val() + 'px ' + $('#text_lightings>div>div>div').css('background-color'));
        saveStep(); 
    }
}

function button_next_width() {
    if (parseInt($('#content_width_val').val()) < 2500) {
        if ($('#content_width_val').next().text() == '%') {
            $('#content_width_val').val(parseFloat($('#content_width_val').val()) + 1);
            $('#content_standard .section').each(function () {
                var width = getStyle($(this), 'width');
                var itemId = $(this).attr('id');
                var widthC = getStyle($('#' + itemId + '>.container_section'), 'width');
                if (width.replace(' ', '') == '100%') {
                    $('#' + itemId + '>.container_section').css('width', $('#content_width_val').val() + '%');
                }
                if (widthC.replace(' ', '') == '100%') {
                    $(this).css('width', $('#content_width_val').val() + '%');
                }
            });
            $('#pages .section').each(function () {
                if ($(this).attr('class').indexOf('sectionContent') < 0) {
                    var width = getStyle($(this), 'width');
                    var itemId = $(this).attr('id');
                    var widthC = getStyle($('#' + itemId + '>.container_section'), 'width');
                    if (width.replace(' ', '') == '100%') {
                        $('#' + itemId + '>.container_section').css('width', $('#content_width_val').val() + '%');
                    }
                    if (widthC.replace(' ', '') == '100%') {
                        $(this).css('width', $('#content_width_val').val() + '%');
                    }
                }
            });
        } else {
            $('#content_width_val').val(parseInt($('#content_width_val').val()) + 1);
            $('#content_standard .section').each(function () {
                var width = getStyle($(this), 'width');
                var itemId = $(this).attr('id');
                var widthC = getStyle($('#' + itemId + '>.container_section'), 'width');
                if (width.replace(' ', '') == '100%') {
                    $('#' + itemId + '>.container_section').css('width', $('#content_width_val').val() + 'px');
                }
                if (widthC.replace(' ', '') == '100%') {
                    $(this).css('width', $('#content_width_val').val() + 'px');
                }
            });
            $('#pages .section').each(function () {
                if ($(this).attr('class').indexOf('sectionContent') < 0) {
                    var width = getStyle($(this), 'width');
                    var itemId = $(this).attr('id');
                    var widthC = getStyle($('#' + itemId + '>.container_section'), 'width');
                    if (width.replace(' ', '') == '100%') {
                        $('#' + itemId + '>.container_section').css('width', $('#content_width_val').val() + 'px');
                    }
                    if (widthC.replace(' ', '') == '100%') {
                        $(this).css('width', $('#content_width_val').val() + 'px');
                    }
                }
            });
        }

    }
    saveStep();
}

function button_prev_width_page() {
    var value = parseInt($('#page_content_width_val').val());
    if (value > 0) {
        $('#page_content_width_val').val(value - 1);
        $('#content_width_val').val(value - 1);
        $('#content_width_val').next().text($('#page_content_width_val').next().text());
    }
}

function button_next_width_page() {
    var value = parseInt($('#page_content_width_val').val());
    $('#page_content_width_val').val(value + 1);
    $('#content_width_val').val(value + 1);
    $('#content_width_val').next().text($('#page_content_width_val').next().text());
}

function button_prev_height_page() {
    var value = parseInt($('#page_content_height_val').val());
    if (value > 0) {
        $('#page_content_height_val').val(value - 1);
    }
}

function button_next_height_page() {
    var value = parseInt($('#page_content_height_val').val());
    $('#page_content_height_val').val(value + 1);
}

function button_prev_width() {
    if (parseInt($('#content_width_val').val()) > 0) {
        if ($('#content_width_val').next().text() == '%') {
            $('#content_width_val').val(parseFloat($('#content_width_val').val()) - 1);
            $('#content_standard .section').each(function () {
                var width = getStyle($(this), 'width');
                var itemId = $(this).attr('id');
                var widthC = getStyle($('#' + itemId + '>.container_section'), 'width');
                if (width.replace(' ', '') == '100%') {
                    $('#' + itemId + '>.container_section').css('width', $('#content_width_val').val() + '%');
                }
                if (widthC.replace(' ', '') == '100%') {
                    $(this).css('width', $('#content_width_val').val() + '%');
                }
            });
            $('#pages .section').each(function () {
                if ($(this).attr('class').indexOf('sectionContent') < 0) {
                    var width = getStyle($(this), 'width');
                    var itemId = $(this).attr('id');
                    var widthC = getStyle($('#' + itemId + '>.container_section'), 'width');
                    if (width.replace(' ', '') == '100%') {
                        $('#' + itemId + '>.container_section').css('width', $('#content_width_val').val() + '%');
                    }
                    if (widthC.replace(' ', '') == '100%') {
                        $(this).css('width', $('#content_width_val').val() + '%');
                    }
                }
            });
        } else {
            $('#content_width_val').val(parseInt($('#content_width_val').val() - 1));
            $('#content_standard .section').each(function () {
                var width = getStyle($(this), 'width');
                var itemId = $(this).attr('id');
                var widthC = getStyle($('#' + itemId + '>.container_section'), 'width');
                if (width.replace(' ', '') == '100%') {
                    $('#' + itemId + '>.container_section').css('width', $('#content_width_val').val() + 'px');
                }
                if (widthC.replace(' ', '') == '100%') {
                    $(this).css('width', $('#content_width_val').val() + 'px');
                }
            });
            $('#pages .section').each(function () {
                if ($(this).attr('class').indexOf('sectionContent') < 0) {
                    var width = getStyle($(this), 'width');
                    var itemId = $(this).attr('id');
                    var widthC = getStyle($('#' + itemId + '>.container_section'), 'width');
                    if (width.replace(' ', '') == '100%') {
                        $('#' + itemId + '>.container_section').css('width', $('#content_width_val').val() + 'px');
                    }
                    if (widthC.replace(' ', '') == '100%') {
                        $(this).css('width', $('#content_width_val').val() + 'px');
                    }
                }
            });
        }

    }
    saveStep();
}

function button_next_thickness_border() {
    if (parseInt($('#thickness_border').val()) < 20) {
        var style = $('#country_id_2 option:selected').val();
        $('#thickness_border').val(parseInt($('#thickness_border').val()) + 1);
        groupProperty('border', $('#thickness_border').val() + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
        $('.active').css('border', $('#thickness_border').val() + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
        if ($('.active').attr('type') == 'text') {
            correctEditText();
        }
        saveStep();
    }
}

function button_prev_thickness_border() {
    if (parseInt($('#thickness_border').val()) > 0) {
        var style = $('#country_id_2 option:selected').val();
        $('#thickness_border').val(parseInt($('#thickness_border').val()) - 1);
        groupProperty('border', $('#thickness_border').val() + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
        $('.active').css('border', $('#thickness_border').val() + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
        if ($('.active').attr('type') == 'text') {
            correctEditText();
        }
        saveStep();
    }
}

function button_next_border_top() {
    if (parseInt($('#b_top').val()) < 20) {
        $('#b_top').val(parseInt($('#b_top').val()) * 1 + 1);
        var style = $('#country_id_2 option:selected').val();
        $('.active').css('border-top', $('#b_top').val() + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
        groupProperty('border-top', $('#b_top').val() + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
        if ($('.active').attr('type') == 'text') {
            correctEditText();
        }
        saveStep();
    }
}

function button_prev_border_top() {
    if (parseInt($('#b_top').val()) > 0) {
        $('#b_top').val(parseInt($('#b_top').val()) * 1 - 1);
        var style = $('#country_id_2 option:selected').val();
        $('.active').css('border-top', $('#b_top').val() + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
        groupProperty('border-top', $('#b_top').val() + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
        if ($('.active').attr('type') == 'text') {
            correctEditText();
        }
        saveStep();
    }
}

function button_next_border_left() {
    if (parseInt($('#b_left').val()) < 20) {
        $('#b_left').val(parseInt($('#b_left').val()) * 1 + 1);
        var style = $('#country_id_2 option:selected').val();
        $('.active').css('border-left', $('#b_left').val() + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
        groupProperty('border-left', $('#b_left').val() + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
        if ($('.active').attr('type') == 'text') {
            correctEditText();
        }
        saveStep();
    }
}

function button_prev_border_left() {
    if (parseInt($('#b_left').val()) > 0) {
        $('#b_left').val(parseInt($('#b_left').val()) * 1 - 1);
        var style = $('#country_id_2 option:selected').val();
        $('.active').css('border-left', $('#b_left').val() + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
        groupProperty('border-left', $('#b_left').val() + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
        if ($('.active').attr('type') == 'text') {
            correctEditText();
        }
        saveStep();
    }
}

function button_next_border_right() {
    if (parseInt($('#b_right').val()) < 20) {
        $('#b_right').val(parseInt($('#b_right').val()) * 1 + 1);
        var style = $('#country_id_2 option:selected').val();
        $('.active').css('border-right', $('#b_right').val() + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
        groupProperty('border-right', $('#b_right').val() + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
        if ($('.active').attr('type') == 'text') {
            correctEditText();
        }
        saveStep();
    }
}

function button_prev_border_right() {
    if (parseInt($('#b_right').val()) > 0) {
        $('#b_right').val(parseInt($('#b_right').val()) * 1 - 1);
        var style = $('#country_id_2 option:selected').val();
        $('.active').css('border-right', $('#b_right').val() + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
        groupProperty('border-right', $('#b_right').val() + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
        if ($('.active').attr('type') == 'text') {
            correctEditText();
        }
        saveStep();;
    }
}

function button_next_border_bottom() {
    if (parseInt($('#b_bottom').val()) < 20) {
        $('#b_bottom').val(parseInt($('#b_bottom').val()) * 1 + 1);
        var style = $('#country_id_2 option:selected').val();
        $('.active').css('border-bottom', $('#b_bottom').val() + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
        groupProperty('border-bottom', $('#b_bottom').val() + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
        if ($('.active').attr('type') == 'text') {
            correctEditText();
        }
        saveStep();
    }
}

function button_prev_border_bottom() {
    if (parseInt($('#b_bottom').val()) > 0) {
        $('#b_bottom').val(parseInt($('#b_bottom').val()) * 1 - 1);
        var style = $('#country_id_2 option:selected').val();
        $('.active').css('border-bottom', $('#b_bottom').val() + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
        groupProperty('border-bottom', $('#b_bottom').val() + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
        if ($('.active').attr('type') == 'text') {
            correctEditText();
        }
        saveStep();
    }
}

function button_next_border_radius() {
    if (parseInt($('#border_radiusi').val()) < 200) {
        $('#border_radiusi').val(parseInt($('#border_radiusi').val()) + 1);
        $('.active').css('border-radius', $('#border_radiusi').val() + 'px');
        groupProperty('border-radius', $('#border_radiusi').val() + 'px');
        saveStep();
    }
}

function button_prev_border_radius() {
    if (parseInt($('#border_radiusi').val()) > 0) {
        $('#border_radiusi').val(parseInt($('#border_radiusi').val()) - 1);
        $('.active').css('border-radius', $('#border_radiusi').val() + 'px');
        groupProperty('border-radius', $('#border_radiusi').val() + 'px');
        saveStep();
    }
}

function button_next_br_top_left() {
    if (parseInt($('#br_t_l').val()) < 200) {
        $('#br_t_l').val(parseInt($('#br_t_l').val()) + 1);
        $('.active').css('border-top-left-radius', $('#br_t_l').val() + 'px');
        groupProperty('border-top-left-radius', $('#br_t_l').val() + 'px');
        saveStep();
    }
}

function button_prev_br_top_left() {
    if (parseInt($('#br_t_l').val()) > 0) {
        $('#br_t_l').val(parseInt($('#br_t_l').val()) - 1);
        $('.active').css('border-top-left-radius', $('#br_t_l').val() + 'px');
        groupProperty('border-top-left-radius', $('#br_t_l').val() + 'px');
        saveStep();
    }
}

function button_next_br_top_right() {
    if (parseInt($('#br_t_r').val()) < 200) {
        $('#br_t_r').val(parseInt($('#br_t_r').val()) + 1);
        $('.active').css('border-top-right-radius', $('#br_t_r').val() + 'px');
        groupProperty('border-top-right-radius', $('#br_t_r').val() + 'px');
        saveStep();
    }
}

function button_prev_br_top_right() {
    if (parseInt($('#br_t_r').val()) > 0) {
        $('#br_t_r').val(parseInt($('#br_t_r').val()) - 1);
        $('.active').css('border-top-right-radius', $('#br_t_r').val() + 'px');
        groupProperty('border-top-right-radius', $('#br_t_r').val() + 'px');
        saveStep();
    }
}

function button_next_br_bottom_right() {
    if (parseInt($('#br_b_r').val()) < 200) {
        $('#br_b_r').val(parseInt($('#br_b_r').val()) + 1);
        groupProperty('border-bottom-right-radius', $('#br_b_r').val() + 'px');
        $('.active').css('border-bottom-right-radius', $('#br_b_r').val() + 'px');
        saveStep();
    }
}

function button_prev_br_bottom_right() {
    if (parseInt($('#br_b_r').val()) > 0) {
        $('#br_b_r').val(parseInt($('#br_b_r').val()) - 1);
        groupProperty('border-bottom-right-radius', $('#br_b_r').val() + 'px');
        $('.active').css('border-bottom-right-radius', $('#br_b_r').val() + 'px');
        saveStep(); ////console.log('saveStep - ' + 156);
    }
}

function button_next_br_bottom_left() {
    if (parseInt($('#br_b_l').val()) < 200) {
        $('#br_b_l').val(parseInt($('#br_b_l').val()) + 1);
        groupProperty('border-bottom-left-radius', $('#br_b_l').val() + 'px');
        $('.active').css('border-bottom-left-radius', $('#br_b_l').val() + 'px');
        saveStep(); ////console.log('saveStep - ' + 157);
    }
}

function button_prev_br_bottom_left() {
    if (parseInt($('#br_b_l').val()) > 0) {
        $('#br_b_l').val(parseInt($('#br_b_l').val()) - 1);
        groupProperty('border-bottom-left-radius', $('#br_b_l').val() + 'px');
        $('.active').css('border-bottom-left-radius', $('#br_b_l').val() + 'px');
        saveStep(); ////console.log('saveStep - ' + 158);
    }
}



function editElement() {
    //console.log('редактируем элемент');
    var type = $('.active').attr('type');
    switch (type) {
        case 'text':
            $('#move_block').hide();
            $('#textarea_text_element').val($('.active>a').text());
            $('#textarea_text_element').text($('.active>a').text());
            
            correctSizeTextElement();
            $('.active>a').css('opacity', 0);
            $('#textarea_text_element').show();
            $('#textarea_text_element').attr('autofocus', 'autofocus');
            $('#textarea_text_element').focus();

            var height = getStyle($('.active'), 'height');
            if (height.indexOf('%') > -1) {
                $('.active').css('height', ((($('#textarea_text_element').height() + parseInt($('.active').css('border-top-width')) + parseInt($('.active').css('border-bottom-width'))) / $('.active').parent().height()) * 100) + '%');
            } else {
                $('.active').css('height', ($('#textarea_text_element').height() + parseInt($('.active').css('border-bottom-width')) + parseInt($('.active').css('border-top-width'))) + 'px');
            }
            break;
        case 'button':
            $('#edit_text').val($('.active').children().first().text());
            if ($('.active>a').attr('href').indexOf('@Url.Action') < 0) {
                $('#text_href').val($('.active>a').attr('href'));
            } else {
                $('#text_href').val('#');
            }

            showLitebox('editTextElement');

            $('#ListPages').html('');
            var optB = document.createElement('option');
            $(optB).attr('value', '');
            $(optB).text('-  Выберите  -');
            $(optB).attr('selected', 'selected');
            $('#ListPages').append(optB);
            $('#pages').children().each(function () {
                if ($(this).attr('type') != 'layout') {
                    var optionB = document.createElement('option');
                    $(optionB).attr('value', $(this).attr('id'));
                    $(optionB).text($(this).attr('name') + '(' + $(this).attr('id') + ')');
                    $('#ListPages').append(optionB);
                }
            });
            break;
        case 'image':
            openWindowLoadImage();
            break;
        case 'icon':
            try {
                var key = '';
                var i = 1;
                $('[class = "' + $('.active>a').attr('class') + '"]').each(function () {
                    i++;
                    if ($(this).parent().attr('class') == 'icon_gall') {
                        key = $(this).parent().attr('id').split('_')[1];
                        $(this).click();
                        return;
                    }
                });
                $('#types_of_galleries_icon').children().each(function () {
                    if ($(this).attr('onclick').indexOf(key) >= 0) {
                        $(this).click();
                        return;
                    }
                });
            } catch (ex) {

            }
            $('#icons_gallery ' + $('.active').attr('class')).first().click();
            showLitebox('load_icon_litebox');
            break;
        case 'video':
            $('#user_poster_url').val();
            $('#user_poster_url').val();
            $('#user_poster_url').val();

            if (videoOption('controls') == 1) {
                $('#controls').prop('checked', true);
            } else {
                $('#controls').prop('checked', false);
            }
            if (videoOption('fs') == 1) {
                $('#fs').prop('checked', true);
            } else {
                $('#fs').prop('checked', false);
            }
            if (videoOption('showinfo') == 1) {
                $('#showinfo').prop('checked', true);
            } else {
                $('#showinfo').prop('checked', false);
            }
            if (videoOption('autoplay') == 1) {
                $('#autoplay').prop('checked', true);
            } else {
                $('#autoplay').prop('checked', false);
            }
            if (videoOption('rel') == 1) {
                $('#rel').prop('checked', true);
            } else {
                $('#rel').prop('checked', false);
            }
            if (videoOption('loop') == 1) {
                $('#loop').prop('checked', true);
            } else {
                $('#loop').prop('checked', false);
            }
            var video = $('.active>img').attr('rel');
            if (video.indexOf('youtu.be') >= 0 || video.indexOf('youtube.com') >= 0 || video.indexOf('vimeo.com') >= 0) {
                var videoid = video.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/embed\/)([^\s&]+)/);
                if (videoid != null) {
                    videoid[1] = videoid[1].split('?')[0];
                    $('#video_url').val('https://youtube.com/embed/' + videoid[1]);
                    $('#controls').parent().show();
                    $('#fs').parent().show();
                    $('#showinfo').parent().show();
                    $('#rel').parent().show();
                } else {
                    videoid = video.match(/(?:https?:\/{2})?(?:w{3}\.)?(?:player\.)?vimeo?\.?com(?:\/video)?\/([^\s?]+)/);
                    if (videoid != null) {
                        $('#video_url').val('https://vimeo.com/' + videoid[1]);
                        $('#controls').parent().hide();
                        $('#fs').parent().hide();
                        $('#showinfo').parent().hide();
                        $('#rel').parent().hide();
                    } else {
                        //console.log("The vimeo url is not valid.");
                    }

                }
                selNetworkUrlVideo();
            } else {
                $('#user_video_url').val($('.active>img').attr('rel'));
                if ($('.active>img').attr('src') != '/Areas/Cabinet/Images/video_poster.jpg') {
                    $('#user_poster_url').val($('.active>img').attr('src'));
                } else {
                    $('#user_poster_url').val('');
                }
                selUserUrlVideo();
            }
            showLitebox('edit_video');
            break;
        case 'map':
            $('#map_option_window>script').remove();
            $('#map_id').html('');
            var script = document.createElement('script');
            $(script).text($('.active>img').attr('script'));
            $(script).attr('type', 'text/javascript');
            $('#map_option_window').append(script);

            showLitebox('map_option_window');
            break;
        case 'form':
            //console.log('открываем окно редактирования свойств формы');
            $('#left_panel_content').css('top', '0px');
            var id = $('.active').attr('id');
            var htmlEl = $('.active').html();
            var widthEl = $('.active').css('width');
            var heightEl = getStyle($('.active'), 'height');
            var pwEl = parseInt($('.active').css('padding-left')) + parseInt($('.active').css('padding-right'));
            var phEl = parseInt($('.active').css('padding-top')) + parseInt($('.active').css('padding-bottom'));
            var styleEl = $('.active').attr('style');

            hideTextarea();
            $(".active").removeClass("active");
            $("#active").hide();
            $('#move_block').hide();
            $('#move_text').hide();
            $('#move-text').hide();
            $('#edit_element').hide();
            $('#option_element').hide();
            $('#remove_element').hide();
            $('#resize_section').hide();
            $('#' + $('.tab_active').parent().attr('rel') + '>.page_content_standard').html($('#content_standard').html());
            removeAdditionalBlocks();
            $('.tab_active').removeClass('tab_active');

            //удаление секций из окна редактора
            $('#content_standard').find('.section').each(function () {
                $(this).remove();
            });

            var div = document.createElement('div');
            $(div).attr('class', 'page_tab tab_active');
            $(div).text('Форма');
            var href = document.createElement('a');
            $(href).attr('href', '#');
            $(href).attr('title', 'Редактирование формы');
            $(href).attr('rel', id);
            href.appendChild(div);
            $(".add_page_tab").parent().before(href);

            //var optionButton = document.createElement('span');
            //$(optionButton).attr('style', 'position: absolute; top:0px; right: -20px; width: 19px; height: 19px; text-align: center; font-size: 6px;');
            //$(optionButton).attr('onclick', 'editForm()');
            //$(optionButton).html('<i class="glyphicon glyphicon-cog sell"></i>');
            var genBlock = document.createElement('div');
            //$(genBlock).append(optionButton);
            $(genBlock).addClass('edit_form');
            $(genBlock).width(parseInt(widthEl));
            $(genBlock).css('height', heightEl);
            $(genBlock).css('position', 'relative');
            $(genBlock).css('margin', '0 auto');
            $(genBlock).css('background', 'white');
            var form = document.createElement('div');
            $(form).attr('style', styleEl);
            $(form).attr('class', 'containerForm');
            $(form).css('width', '100%');
            $(form).css('height', '100%');
            $(form).css('top', 0);
            $(form).css('left', 0);
            $(form).html(htmlEl);
            $(genBlock).append(form);
            $('#content_standard').append(genBlock);
            $(form).children().each(function () {
                if ($(this).children().length == 0) {
                    $(this).addClass('editable_element');
                } else {
                    $(this).children().each(function () {
                        $(this).addClass('editable_element');
                    });
                }

            });

            $('.containerForm>div>span').css('display', 'block');

            $('#section').hide();
            $('#sections').hide();
            $('#text').hide();
            $('#image').hide();
            $('#button').hide();
            $('#icons').hide();
            $('#video').hide();
            $('#map').hide();
            $('#form').hide();
            $('#timers').hide();
            $('#anchor').hide();
            $('#block').hide();
            $('#widgets').hide();

            $('#input').show();
            $('#textarea').show();
            $('#e-mail').show();
            $('#url').show();
            $('#phone').show();
            $('#file').show();
            //correctionFormSize();
            break;
        case 'timer':
            showLitebox('#timer_option_litebox');
            var time = $('.active').attr('timer');
            var data = new Date(time);
            $('#data_time').val(data.getDate() + '-' + (data.getMonth() + 1) + '-' + data.getFullYear() + ' ' + data.getHours() + ':' + data.getMinutes());
            break;
        case 'circular_timer':
            showLitebox('circular_timer_litebox', '');
            var time = $('.active>div>div').attr('data-date');
            var arrayDate = time.split(' ')[0].split('-');
            var arrayTime = time.split(' ')[1].split(':');

            $('#circular_data_time').val(arrayDate[2] + '-' + arrayDate[1] + '-' + arrayDate[0] + ' ' + arrayTime[0] + ':' + arrayTime[1] + ':' + arrayTime[2]);
            $('#days_text').val($('.active .textDiv_Days>h4').text());
            $('#hours_text').val($('.active .textDiv_Hours>h4').text());
            $('#minutes_text').val($('.active .textDiv_Minutes>h4').text());
            $('#seconds_text').val($('.active .textDiv_Seconds>h4').text());
            $('#circular_timer_background_color>div>div>div').css('background-color', $('.textDiv_Days').css('border-bottom-color'));
            $('#circular_timer_background_color_day>div>div>div').css('background-color', $('.textDiv_Days').css('border-top-color'));
            $('#circular_timer_background_color_hour>div>div>div').css('background-color', $('.textDiv_Hours').css('border-top-color'));
            $('#circular_timer_background_color_minute>div>div>div').css('background-color', $('.textDiv_Minutes').css('border-top-color'));
            $('#circular_timer_background_color_second>div>div>div').css('background-color', $('.textDiv_Seconds').css('border-top-color'));
            try {
                var script = $('#scripts').find('[rel = "' + $('.active').attr('id') + '"]').html();
                var circlesWidth = parseFloat(script.replace('"bg_width":', '┤').split('┤')[1].split(',')[0]).toFixed(1);
                $('#circles_width_background_val').val(circlesWidth);
                $('.active .time_circles>div').css('width', 70 * circlesWidth);
                $('.active .time_circles>div').css('height', 70 * circlesWidth);
            } catch (ex) {
                $('#circles_width_background_val').val(1);
                $('.active .time_circles>div').css('width', 70);
                $('.active .time_circles>div').css('height', 70);
            }

            try {
                var script = $('#scripts').find('[rel = "' + $('.active').attr('id') + '"]').html();
                var circlesWidth = parseFloat(script.replace('"fg_width":', '┤').split('┤')[1].split(',')[0]).toFixed(2);
                $('#circles_width_foreground_val').val(circlesWidth);
                $('.active .time_circles>div').css('border-width', (circlesWidth * 116) + 'px');
            } catch (ex) {
                $('#circles_width_foreground_val').val(1);
                $('.active .time_circles>div').css('border-width', 7 + 'px');
            }
            break;
        case 'btn':
            $('#edit_text').val($('.active').text());

            showLitebox('editTextElement');
            $('#edit_text').focus();
            break;
        case 'anchor':

            break;
        case 'widget':
            //widget тут открытие окна
            showLitebox('widget_litebox', $('.active').attr('type'));
            break;
        default:
    }
}

function videoOption(parametr) {
    //console.log('videoOption');
    try {
        var url = $('.active>img').attr('rel');
        var array = url.split('/');
        var str = array[array.length - 1];
        str = str.replace(parametr + '=', '/').split('/')[1];
        return parseInt(str.charAt(0));
    } catch (ex) {
        return 0;
    }
}

function openWindowLoadImage(param) {
    //console.log('открываем окно загрузки изображения');
    if (param == 'landingBackground') {
        landingBackground = true;
    }
    loadCatalogs('user');
    loadCatalogs('gallery');
    $('#Catalog').parent().parent().hide();
    $('#image_from_gallery').hide();
    $('#download_image_from_gallery').show();
    $('#download_image_from_gallery img').remove();
    $('#current_category').text('Загрузка изображения');
    $('#save_image_1').hide();
    $('#image_url input').val('');
    $('#image_url #Catalog').parent().parent().hide();
    $('#save_image_2').hide();
    $("#image_file input").val('');
    showLitebox('load_image');
}

function getMountName(m) {
    var month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    return month[m];
}

function getDayName(m) {
    var day = ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'];
    return day[m];
}

function saveTimerOption() {
    //console.log('устанавливаем настройки таймера');
    var t = $('#data_time').val();
    var jear = parseInt(t.split(' ')[0].split('-')[2]);
    var mount = parseInt(t.split(' ')[0].split('-')[1]);
    var date = parseInt(t.split(' ')[0].split('-')[0]);
    var hour = parseInt(t.split(' ')[1].split(':')[0]);
    var min = parseInt(t.split(' ')[1].split(':')[1]);
    var time = new Date(jear, mount - 1, date, hour, min).getTime();
    var day = (new Date()).getDay();
    var dateFormat = getDayName(day) + ' ' + getMountName(mount) + ' ' + date + ' ' + jear + ' ' + hour + ':' + min;

    var id = $('.active').attr('id');
    var num = id.split('_')[1];
    $('.active').empty();

    var timer = document.createElement('div');
    $(timer).attr('id', 'countdown-' + num);
    $(timer).addClass('timeTo timeTo-white');
    $('.active').append(timer);
    var script = document.createElement('script');
    $(script).text("/*" + id + ", lastload*/ try{$('#countdown-" + num + "').timeTo(new Date('" + dateFormat + "'));}catch(ex){console.log(ex)}");
    $(script).attr('type', 'text/javascript');
    $('.active').append(script);

    hideLitebox();
    saveStep();
}

function createCss() {
    //console.log('создаем СCS свойства таймера');
    var style = document.createElement('style');
    $(style).text(".inn{background-color:rgb(255, 255, 255)!important;color:rgba(150, 197, 187, 0.8)!important;font-size:70px;} .flip-clock-label{color:black;} .flip-clock-wrapper ul{width:60px;height:90px;} .ffflip-clock-divider{width:0px;height:90px;} .flip-clock-wrapper ul li a div.up:after{top:44px;} .flip-clock-wrapper ul li{line-height:87px;}.flip-clock-divider:first-child {width: 0!important; }");
    $(".active").attr('text', "rgb(255, 255, 255)$rgba(150, 197, 187, 0.8)$70px$black$60px$90px$0px$90px$44px$87px");
    $(style).attr('id', 'timerStyle');
    $('head').append(style);
    saveStep();
}

function saveCssToTimer() {
    //console.log('сохраняем СCS свойства таймера');
    var strr = $('.active').attr('text');
    var str = strr.split('$');
    str[0] = $('#edit_text_background_color>div>div').css('background-color');
    str[1] = $('#edit_text_color>div>div').css('background-color');
    $('#timerStyle').text(".inn{background-color:" + str[0] + "!important;color:" + str[1] + "!important;font-size:" + str[2] + "!important;} .flip-clock-label{color:" + str[3] + "!important;} .ffflip-clock-wrapper ul{width:" + str[4] + "!important;height:" + str[5] + "!important;} .flip-clock-divider{width:" + str[6] + "!important;height:" + str[7] + "!important;} .flip-clock-wrapper ul li a div.up:after{top:" + str[8] + "!important;} .flip-clock-wrapper ul li{line-height:" + str[9] + "!important;}.flip-clock-divider:first-child {width: 0!important; }");
    $('.active').attr('text', str[0] + "$" + str[1] + "$" + str[2] + "$" + str[3] + "$" + str[4] + "$" + str[5] + "$" + str[6] + "$" + str[7] + "$" + str[8] + "$" + str[9]);
    saveStep();
}

function getGallery(el, gallery, type) {
    //console.log('получаем галереи изображений');
    $('.type_of_gallery').css('background', 'rgba(0,0,0,0)');
    $(el).css('background', '#444');
    $('#image_from_gallery').empty();
    $('#image_from_gallery_b').empty();
    $('#download_image_from_gallery').hide();
    $('#image_from_gallery').show();
    $('#image_from_gallery_b').show();
    if (type == 'user') {
        $('#current_category').text('Мои коллекции > ' + gallery);
        $('#current_category_b').text('Мои коллекции > ' + gallery);
    } else {
        $('#current_category').text('Коллекции изображений > ' + gallery);
        $('#current_category_b').text('Коллекции изображений > ' + gallery);
    }
    $.post('/Cabinet/Editor/LoadImages', { patch: gallery, type: type }, function (data) {
        for (var i = 0; i <= data.res.length - 1; i++) {
            if (type != 'user') {
                if (data.res[i].indexOf('_min.') >= 0) {
                    var img = document.createElement('img');
                    $(img).attr('src', data.res[i]);
                    $(img).attr('onclick', 'selectImage(this)');
                    $('#image_from_gallery').append(img);
                    $('#image_from_gallery_b').append(img);
                }
            } else {
                var imgM = document.createElement('img');
                $(imgM).attr('src', data.res[i]);
                $(imgM).attr('onclick', 'selectImage(this)');
                $('#image_from_gallery').append(imgM);
                $('#image_from_gallery_b').append(imgM);
            }
        }
    });
}

function getGalleryBackgroundSection(gallery) {
    //console.log('получаем галереи изображений для секций');
    $('#image_from_gallery_section').empty();

    $.post('/Cabinet/Editor/LoadImages', { patch: gallery }, function (data) {
        for (var i = 1; i <= data.res.length - 1; i++) {
            if (gallery != 'Main') {
                if (data.res[i].indexOf('_min.') >= 0) {
                    var img = document.createElement('img');
                    $(img).attr('src', data.res[i]);
                    $(img).attr('onclick', 'selectImage(this)');
                    $('#image_from_gallery_section').append(img);
                }
            } else {
                var imgM = document.createElement('img');
                $(imgM).attr('src', data.res[i]);
                $(imgM).attr('onclick', 'selectImage(this)');
                $('#image_from_gallery_section').append(imgM);
            }

        }
    });
}

function getGalleryBackground(gallery) {
    //console.log('получаем галереи изображений фонов???');
    $('#image_from_gallery_background').empty();
    $.ajax({
        url: "/Cabinet/Editor/LoadImages",
        data: {
            patch: gallery
        },
        async: true,
        typedata: "json",
        success: function (data) {
            for (var i = 1; i <= data.res.length - 1; i++) {
                var img = document.createElement('img');
                $(img).attr('src', data.res[i]);
                $(img).attr('onclick', 'selectImage(this)');
                $('#image_from_gallery_background').append(img);
            }
        },
        error: function (data) {
            //alert('Все плохо');
        },
        type: "POST"
    });
}

function selectImage(el) {
    //console.log('выбираем изображение');
    $('#image_from_gallery').find('.selected').removeClass('selected');
    $(el).addClass('selected');
}

function selImage() {
    //console.log('выбираем изображение 2 ???');
    if (landingBackground) {
        $('#content').css('background-image', 'url(' + $('.selected').attr('src') + ')');
        $('.selected').removeClass('selected');
    } else {
        try {
            if ($('.active').attr('type') == 'image') {
                $('.active img').attr('src', $('.selected').attr('src').replace('_min', ''));
            } else {
                $('.active').css('background', 'url(' + $('.selected').attr('src').replace('_min', '') + ')');
            }

        } catch (ex) {
            if ($('.active').attr('type') == 'image') {
                $('.active img').attr('src', $('.selected').attr('src'));
            } else {
                $('.active').css('background', 'url(' + $('.selected').attr('src') + ')');
            }
        }

        if ($('.selected').attr('src') != undefined && $('.selected').attr('src') != '') {
            $('#section_background_position').show();
            $('#position-background').show();
            $('#background-position').show();
            $('#section_background').show();
            $('#background_image_section_del').show();
        }
    }

    saveStep();
    hideLitebox();
}

function selBackgroundImage() {
    //console.log('выбираем изображение 3 ???');
    $('#content').css('background-image', 'url(' + $('.selected').attr('src') + ')');
    $('.selected').removeClass('selected');
    hideLitebox();
    saveStep(); ////console.log('saveStep - ' + 164);
}

function delElement() {
    //console.log('удаляем элемент');
    if ($('.active').attr('type') == 'button') {
        var id = $('.active').attr('id');
        $('#styles').find('[rel = "' + id + '"]').remove();
        $('#scripts').find('[rel = "' + id + '"]').remove();
    }
    $("div.active").remove();
    $('#active').hide();
    $('#move_block').hide();
    $('#move_text').hide();
    $('#move-text').hide();
    $('#edit_element').hide();
    $('#option_element').hide();
    $('#remove_element').hide();

    hideLitebox();
    reload_right_panel();
    //unselectEl();
    saveStep(); ////console.log('saveStep - ' + 166);
}

function openDelWindow(id) {
    if (id != undefined && id != '') {
        $('#delete_element').children('.litebox_buttons').children('.yes').attr('onclick', 'removeFormElement("' + id + '")');
    } else {
        $('#delete_element').children('.litebox_buttons').children('.yes').attr('onclick', 'delElement()');
    }
    //console.log('открыаем окно удаления элемента');
    showLitebox('delete_element');
}

function loadImage(el) {
    //console.log('загружаем изображение');
    $('.type_of_gallery').css('background', 'rgba(0,0,0,0)');
    $('#image_from_gallery').hide();
    $('#image_from_gallery_b').hide();
    $('#download_image_from_gallery').show();
    $('#download_image_from_gallery_b').show();
    $('#current_category').text('Загрузка изображения');
    $('#current_category_b').text('Загрузка изображения');
}

function addUrl() {
    //console.log('добавляем ссылку изображения');
    $('#image_from_gallery').html();
    $('#image_from_gallery').append('<p>');
    $('#image_from_gallery>p').text("Ссылка:");
    $('#image_from_gallery>p').attr('style', 'margin: 10px 10px 10px 40px; font-size: 17px;');
    $('#image_from_gallery').append('<input/>');
    $('#image_from_gallery>input').attr('style', 'margin: 10px 10px 10px 40px; float: left; width:400px;');
    $('#image_from_gallery').append('<button>');
    $('#image_from_gallery>button').text("Показать");
    $('#image_from_gallery>button').attr('style', 'margin: 10px 10px 10px 10px; font-size: 17px; float: left; width:100px;');
    $('#image_from_gallery>button').attr('onclick', 'viewImage()');
    $('#image_from_gallery').append('<img>');
    $('#image_from_gallery>img').attr('style', 'margin: 10px 40px; max-width:520px; max-height: 425px;');
    $('#image_from_gallery>img').attr('class', 'selected');
}

function addBackgroundUrl() {
    //console.log('добавляем ссылку изображения 2 ???');
    $('#image_from_gallery_background').html();
    $('#image_from_gallery_background').append('<p>');
    $('#image_from_gallery_background>p').text("Ссылка:");
    $('#image_from_gallery_background>p').attr('style', 'margin: 10px 10px 10px 40px; font-size: 17px;');
    $('#image_from_gallery_background').append('<input/>');
    $('#image_from_gallery_background>input').attr('style', 'margin: 10px 10px 10px 40px; float: left; width:400px;');
    $('#image_from_gallery_background').append('<button>');
    $('#image_from_gallery_background>button').text("Показать");
    $('#image_from_gallery_background>button').attr('style', 'margin: 10px 10px 10px 10px; font-size: 17px; float: left; width:100px;');
    $('#image_from_gallery_background>button').attr('onclick', 'viewImageBackground()');
    $('#image_from_gallery_background').append('<img>');
    $('#image_from_gallery_background>img').attr('style', 'margin: 10px 40px; max-width:520px; max-height: 425px;');
    $('#image_from_gallery_background>img').attr('class', 'selected');
}

function addBackgroundUrlSection() {
    //console.log('добавляем ссылку изображения секции');
    $('#image_from_gallery_section').html('');
    $('#image_from_gallery_section').append('<p>');
    $('#image_from_gallery_section>p').text("Ссылка:");
    $('#image_from_gallery_section>p').attr('style', 'margin: 10px 10px 10px 40px; font-size: 17px;');
    $('#image_from_gallery_section').append('<input/>');
    $('#image_from_gallery_section>input').attr('style', 'margin: 10px 10px 10px 40px; float: left; width:400px;');
    $('#image_from_gallery_section').append('<button>');
    $('#image_from_gallery_section>button').text("Показать");
    $('#image_from_gallery_section>button').attr('style', 'margin: 10px 10px 10px 10px; font-size: 17px; float: left; width:100px;');
    $('#image_from_gallery_section>button').attr('onclick', 'viewImageBackgroundSection()');
    $('#image_from_gallery_section').append('<img>');
    $('#image_from_gallery_section>img').attr('style', 'margin: 10px 40px; max-width:520px; max-height: 425px;');
    $('#image_from_gallery_section>img').attr('class', 'selected');
}

function viewImage() {
    //console.log('показываем изображение');
    $('#image_from_gallery>img').attr('src', $('#image_from_gallery>input').val());
    $('#image_from_gallery>img').attr('class', 'selected');
};
function viewImageBackground() {
    //console.log('показываем изображение 2 ???');
    $('#image_from_gallery_background>img').attr('src', $('#image_from_gallery_background>input').val());
    $('#image_from_gallery_background>img').attr('class', 'selected');
};
function viewImageBackgroundSection() {
    //console.log('показываем изображение секции');
    $('#image_from_gallery_section>img').attr('src', $('#image_from_gallery_section>input').val());
    $('#image_from_gallery_section>img').attr('class', 'selected');
};

function b() {
    var b = '';
    if ($('.active').css('font-weight') === 'bold') {
        b = 'normal';
        $('.active').css('font-weight', b);
        $('.active>div').css('font-weight', b);
        $('#b').css('background-color', '#2d2d2d');
        $('#bb').css('background-color', '#2d2d2d');
    } else {
        b = 'bold';
        $('.active').css('font-weight', b);
        $('.active>div').css('font-weight', b);
        $('#b').css('background-color', '#515151');
        $('#bb').css('background-color', '#515151');
    }
    correctSizeTextElement()
    groupProperty('font-weight', b);
    saveStep();
}

function u() {
    var u = '';
    if ($('.active').css('text-decoration') === 'underline') {
        u = 'none';
        $('.active').css('text-decoration', u);
        $('.active>div').css('text-decoration', u);
        $('#c').css('background-color', '#2d2d2d');
        $('#cc').css('background-color', '#2d2d2d');
    } else {
        u = 'underline';
        $('.active').css('text-decoration', u);
        $('.active>div').css('text-decoration', u);
        $('#c').css('background-color', '#515151');
        $('#cc').css('background-color', '#515151');
    }
    correctSizeTextElement()
    groupProperty('text-decoration', u);
    saveStep();
}

function i() {
    var i = '';
    if ($('.active').css('font-style') === 'italic') {
        i = 'normal';
        $('.active').css('font-style', 'normal');
        $('.active>div').css('font-style', 'normal');
        $('#i').css('background-color', '#2d2d2d');
        $('#ii').css('background-color', '#2d2d2d');
    } else {
        i = 'italic';
        $('.active').css('font-style', 'italic');
        $('.active>div').css('font-style', 'italic');
        $('#i').css('background-color', '#515151');
        $('#ii').css('background-color', '#515151');
    }
    correctSizeTextElement()
    groupProperty('font-style', i);
    saveStep();
}

//function text_align(param) {
//    $('.font').css('background-color', '#2d2d2d');
//    $('#' + param).css('background-color', '#515151');
//    $('#panel_' + param).css('background-color', '#515151');
//    $('.active').css('text-align', param);
//    groupProperty('text-align', param);
//    saveStep();
//}

//function text_align_vertical(param) {
//    $('.font-vert').css('background-color', '#2d2d2d');
//    $('#v_' + param).css('background-color', '#515151');
//    $('#panel_v_' + param).css('background-color', '#515151');
//    $('.active').css('display', 'table');
//    if ($('.active').is('label') && $('.active').parent().attr('type') == 'title') {
//        $('.active').parent().css('display', 'table');
//        $('.active').css('display', 'table-cell');
//        $('.active').css('vertical-align', param);

//    } else {
//        $('.active').children().first().css('display', 'table-cell');
//        $('.active').children().first().css('vertical-align', param);
//    }
//    groupProperty('vertical-align', param);
//    saveStep();
//}

function getIconGallery(el, param) {
    //console.log('получаем галерею иконок');
    $('.sel').removeClass('sel');
    $(el).addClass('sel');
    $('.icon_gall').hide();
    $('#icons_' + param).show();
}

function selIcon(el) {
    //console.log('выбираем иконку');
    $('.sell').removeClass('sell');
    $(el).addClass('sell');
}

function getIcon() {
    //console.log('применяем выбранную иконку');
    $('.active a').attr('class', $('.sell').attr('class'));
    $('.active a').removeClass('sell');
    hideLitebox();
    saveStep(); ////console.log('saveStep - ' + 171);
}

function sellElement(el, id) {
    //console.log('выбираем элемент ??? sellElement');
    $('#layers_con').find('span').css('background-color', '#555');
    $(el).find('span').css('background-color', '#444');
    select(id);
}

function hiddeAndShowEl(el, id) {
    //console.log('скрываем или отображем элемент');
    hideTextarea();
    $('.active').removeClass('active');
    $('#active').hide();
    $('#move_block').hide();
    $('#move_text').hide();
    $('#move-text').hide();
    $('#edit_element').hide();
    $('#option_element').hide();
    $('#remove_element').hide();
    if ($(el).find('i').attr('class') === 'fa fa-eye') {
        $(el).find('i').attr('class', 'fa fa-eye-slash');
        $('#' + id).css('opacity', '0');
    } else {
        $(el).find('i').attr('class', 'fa fa-eye');
        $('#' + id).css('opacity', '1');
    }
    saveStep(); ////console.log('saveStep - ' + 172);
}

function renameEl(id, el) {
    //console.log('переименовываем элемент');
    $(el).parent().parent().prev().children().hide();
    var input = document.createElement('input');
    $(input).val($('#' + id).attr('m-title'));
    $(input).attr('class', 'rename_input');
    $(el).parent().parent().prev().append(input);
    $('.rename_input').attr('name', id);
    $('.rename_input').focus();
}

function editEl(id) {
    //console.log('редактируем элемент editEl');
    select(id);
    editElement();
}

function deleteEl(id) {
    //console.log('??? deleteEl');
    if (id != '' && id != undefined) {
        openDelWindow(id);
    } else {
        openDelWindow();
    }
}

function background_image() {
    //console.log('нажимаем на кнопку редактирования изображения');
    editBackground();
    $('#background_color').hide();
    $('#background_image').show();
}

function background_image_del() {
    //console.log('нажимаем на кнопку удаления изображения');
    background_image
    $('#background_color').show();
    $('#background_image').hide();
    $('#content').css('background-image', 'none');
}


function edit_background_color() {
    //console.log('редактируем цвет текста');
    clearInterval(interval);
    interval = setInterval(function () {
        $('#content').css('background-color', $('#edit_background_color>div>div>div').css('background-color'));
        if ($('.active').attr('type') == 'button' || $('.active').attr('type') == 'btn' || $('.active').attr('type') == 'submit') { saveButtonOption(); }
    }, 75);
}

function edit_background_color_section() {
    //console.log('редактируем цвет фона');
    clearInterval(interval);
    interval = setInterval(function () {
        $('.active').css('background-color', $('#edit_background_color_section>div>div>div').css('background-color'));
        if ($('.active').attr('type') == 'button' || $('.active').attr('type') == 'btn' || $('.active').attr('type') == 'submit') { saveButtonOption(); }
    }, 75);
}


function edit_background_color_content() {
    //console.log('редактируем цвет фона рабочей области');
    clearInterval(interval);
    interval = setInterval(function () {
        $('#content_standard').css('background-color', $('#edit_background_color_content>div>div>div').css('background-color'));
        if ($('.active').attr('type') == 'button' || $('.active').attr('type') == 'btn' || $('.active').attr('type') == 'submit') { saveButtonOption(); }
    }, 75);
}

function edit_text_shadow_color() {
    //console.log('редактируем цвет тени текста');
    clearInterval(interval);
    interval = setInterval(function () {
        if ($('.active').attr('type') == 'text') {
            $('.active').children().first().css('text-shadow', $('#x').val() + 'px ' + $('#y').val() + 'px ' + $('#blurrines').val() + 'px ' + $('#color_shadow>div>div>div').css('background-color'));
        } else {
            $('.active span').css('text-shadow', $('#x').val() + 'px ' + $('#y').val() + 'px ' + $('#blurrines').val() + 'px ' + $('#color_shadow>div>div>div').css('background-color'));
        }
        if ($('.active').attr('type') == 'button' || $('.active').attr('type') == 'btn' || $('.active').attr('type') == 'submit') { saveButtonOption(); }
    }, 75);
}

function edit_block_shadow_color() {
    //console.log('редактируем цвет тениблока');
    clearInterval(interval);
    interval = setInterval(function () {
        $('.active').css('box-shadow', $('#x_shadow').val() + 'px ' + $('#y_shadow').val() + 'px ' + $('#blurrines_shadow').val() + 'px ' + $('#block_shadows>div>div>div').css('background-color'));
        if ($('.active').attr('type') == 'button' || $('.active').attr('type') == 'btn' || $('.active').attr('type') == 'submit') { saveButtonOption(); }
    }, 75);
}
function edit_border_color() {
    //console.log('редактируем цвет рамки');
    clearInterval(interval);
    interval = setInterval(function () {
        var style = $('#country_id_2 option:selected').val();
        $('.active').css('border', $('#thickness_border').val() + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
        groupProperty('border', $('#thickness_border').val() + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
        if ($('.active').attr('type') == 'button' || $('.active').attr('type') == 'btn' || $('.active').attr('type') == 'submit') { saveButtonOption(); }
    }, 75);
}

function edit_block_lighting_color() {
    //console.log('редактируем цвет свечения');
    clearInterval(interval);
    interval = setInterval(function () {
        $('.active').css('box-shadow', $('#lighting').val() + ' 0px 0px ' + $('#blurrines_lighting').val() + 'px ' + $('#block_lightings>div>div>div').css('background-color'));
        if ($('.active').attr('type') == 'button' || $('.active').attr('type') == 'btn' || $('.active').attr('type') == 'submit') { saveButtonOption(); }
    }, 75);
}

function edit_text_lighting_color() {
    //console.log('редактируем цвет свечения');
    clearInterval(interval);
    interval = setInterval(function () {
        $('.active>a').css('text-shadow', '0px 0px ' + $('#blurrines_lighting_text').val() + 'px ' + $('#text_lightings>div>div>div').css('background-color'));
        if ($('.active').attr('type') == 'button' || $('.active').attr('type') == 'btn' || $('.active').attr('type') == 'submit') { saveButtonOption(); }
    }, 75);
}

function hide_left_menu() {
    $('#workspace').click();
    if ($('#left_panel').width() > 60) {
        $('#l-p-change-of-size>i').attr('class', 'fa fa-angle-double-right');
        $('#sub_element_menu').css('left', 39);
        $('#left_panel').animate({
            width: 40
        }, 200);
        var ww = $('#center_block').width() + 30;
        $('#center_block').animate({
            width: ww
        }, 200);
        $('#page_tabs').animate({
            width: ww
        }, 200);
        $('#left_panel_content').animate({
            width: 50
        }, 200);
    } else {
        $('#l-p-change-of-size>i').attr('class', 'fa fa-angle-double-left');
        $('#sub_element_menu').css('left', 69);
        $('#left_panel').animate({
            width: 70
        }, 200);
        var ww = $('#center_block').width() - 30;
        $('#center_block').animate({
            width: ww
        }, 200);
        $('#page_tabs').animate({
            width: ww
        }, 200);
        $('#left_panel_content').animate({
            width: 80
        }, 200);
    }

}

function hide_right_menu() {
    //console.log('сворачиваем правое меню');
    hideTextarea();
    $(".active").removeClass("active");
    $("#active").hide();
    $('#move_block').hide();
    $('#move_text').hide();
    $('#move-text').hide();
    $('#edit_element').hide();
    $('#option_element').hide();
    $('#remove_element').hide();
    $('#resize_section').hide();
    if ($('#right_panel').width() > 200) {
        $('#hide_right_menu>i').attr('class', 'fa fa-angle-double-left');
        $('#woc_column_2').show();
        $('#woc_column_1').css('overflow-y', 'hidden');
        $('#woc_column_1').css('width', '0');
        $('.option_el').hide();
        $('.option_el').css('position', 'absolute');
        $('.option_el>.woc_panel>i').attr('class', 'fa fa-angle-double-right');
        $('.woc').css('background', 'none');
        $('#right_panel').animate({
            width: 43
        }, 200);
        $('#widget_options').animate({
            width: 43
        }, 200);
        var ww = $('#center_block').width() + 217;
        $('#center_block').animate({
            width: ww
        }, 200);
        $('#page_tabs').animate({
            width: ww
        }, 200);
        $('#widget_options').css('float', 'none');
    } else {
        $('.option_el>.woc_panel').show();
        $('#hide_right_menu>i').attr('class', 'fa fa-angle-double-right');
        $('#woc_column_2').hide();
        $('#woc_column_1').css('overflow-y', 'scroll');
        $('#woc_column_1').css('width', '100%');
        $('.option_el').show();
        $('.option_el').css('position', 'relative');
        $('.option_el').css('right', 'auto');
        $('.option_el').css('top', '0');
        $('.option_el').css('left', '0');
        $('.woc_content').hide();
        $('#right_panel').animate({
            width: 260
        }, 200);
        $('#widget_options').animate({
            width: 80
        }, 200);
        var ww = $('#center_block').width() - 217;
        $('#center_block').animate({
            width: ww
        }, 200);
        $('#page_tabs').animate({
            width: ww
        }, 200);
        $('#widget_options').css('float', 'left');
        $('#layers').css('float', 'left');
    }
}

function edit_text_background_color() {
    //console.log('редактируем цвет фона 2');
    clearInterval(interval);
    interval = setInterval(function () {
        $('#panel_edit_background_color_text>div>div>div').css('background-color', $('#edit_background_color_text>div>div>div').css('background-color'));
        if ($('.active').attr('type') == 'timer') {
            saveCssToTimer();
        } else {
            $('.active').css('background-color', $('#edit_background_color_text>div>div>div').css('background-color'));
        }
        if ($('.active').attr('type') == 'button' || $('.active').attr('type') == 'btn' || $('.active').attr('type') == 'submit') { saveButtonOption(); }
    }, 75);
}

function panel_edit_text_background_color() {
    //console.log('редактируем цвет фона 2');
    clearInterval(interval);
    interval = setInterval(function () {
        $('#edit_background_color_text>div>div>div').css('background-color', $('#panel_edit_background_color_text>div>div>div').css('background-color'));
        if ($('.active').attr('type') == 'timer') {
            saveCssToTimer();
        } else {
            $('.active').css('background-color', $('#panel_edit_background_color_text>div>div>div').css('background-color'));
        }
        if ($('.active').attr('type') == 'button' || $('.active').attr('type') == 'btn' || $('.active').attr('type') == 'submit') { saveButtonOption(); }
    }, 75);
}

function edit_text_color() {
    //console.log('редактируем цвет текста 2');
    clearInterval(interval);
    interval = setInterval(function () {
        $('#panel_edit_color_text>div>div>div').css('background-color', $('#edit_color_text>div>div>div').css('background-color'));
        if ($('.active').attr('type') == 'timer') {
            $('.active span').css('color', $('#edit_color_text>div>div>div').css('background-color'));
            $('.active div').css('color', $('#edit_color_text>div>div>div').css('background-color'));
        } else {
            $('.active').children().first().css('color', $('#edit_color_text>div>div>div').css('background-color'));
        }
        groupProperty('color', $('#edit_color_text>div>div>div').css('background-color'));
        if ($('.active').attr('type') == 'button' || $('.active').attr('type') == 'btn' || $('.active').attr('type') == 'submit') { saveButtonOption(); }
    }, 75);
}

function panel_edit_text_color() {
    //console.log('редактируем цвет текста 2');
    clearInterval(interval);
    interval = setInterval(function () {
        $('#edit_color_text>div>div>div').css('background-color', $('#panel_edit_color_text>div>div>div').css('background-color'));
        if ($('.active').attr('type') == 'timer') {
            $('.active span').css('color', $('#edit_color_text>div>div>div').css('background-color'));
            $('.active div').css('color', $('#edit_color_text>div>div>div').css('background-color'));
        } else {
            $('.active').children().first().css('color', $('#edit_color_text>div>div>div').css('background-color'));
        }
        groupProperty('color', $('#edit_color_text>div>div>div').css('background-color'));
        if ($('.active').attr('type') == 'button' || $('.active').attr('type') == 'btn' || $('.active').attr('type') == 'submit') { saveButtonOption(); }
    }, 75);
}

function additionally() {
    //console.log('заполняем форму с заголовками');
    showLitebox('additionally_window');
    $('#header').val($('#' + $('.tab_active').parent().attr('rel')).attr('name'));
    $('#description').val($('#' + $('.tab_active').parent().attr('rel')).attr('desc'));
    $('#keywords').val($('#' + $('.tab_active').parent().attr('rel')).attr('key'));
}

function saveAdditionally() {
    //console.log('сохраняем форму с заголовками');
    $('#' + $('.tab_active').parent().attr('rel')).attr('name', $('#header').val());
    $('#' + $('.tab_active').parent().attr('rel')).attr('desc', $('#description').val());
    $('#' + $('.tab_active').parent().attr('rel')).attr('key', $('#keywords').val());
    hideLitebox();
    saveStep(); ////console.log('saveStep - ' + 173);
}

function saveVideo() {
    //console.log('сохраняем видео');
    //console.log($('#vy>i').attr('class'));
    if ($('#vy>i').attr('class') == 'fa fa-check-square-o') {
        var autoplay = 0;
        var loop = 0;
        var showinfo = 0;
        var controls = 0;
        var fs = 0;
        var rel = 0;

        if ($('#autoplay').prop('checked')) {
            autoplay = 1;
        }
        if ($('#loop').prop('checked')) {
            loop = 1;
        }
        if ($('#showinfo').prop('checked')) {
            showinfo = 1;
        }
        if ($('#controls').prop('checked')) {
            controls = 1;
        }
        if ($('#fs').prop('checked')) {
            fs = 1;
        }
        if ($('#rel').prop('checked')) {
            rel = 1;
        }

        var width = $('.active').children().width();
        var height = $('.active').children().height();
        var video = $('#video_url').val();
        var videoid = video.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/embed\/)([^\s&]+)/);
        if (videoid != null) {
            videoid[1] = videoid[1].split('?')[0];
            $('.active').children().remove();
            var str = '';
            str = 'https://youtube.com/embed/' + videoid[1] + '?autoplay=' + autoplay + '&amp;loop=' + loop + '&amp;showinfo=' + showinfo + '&amp;theme=light&amp;color=red&amp;controls=' + controls + '&amp;modestbranding=&amp;start=0&amp;fs=' + fs + '&amp;iv_load_policy=3&amp;wmode=transparent&amp;rel=' + rel;
            $('.active').append('<img width="' + width + '" height="' + height + '" rel="' + str + '" src="//img.youtube.com/vi/' + videoid[1] + '/default.jpg"/>');
            hideLitebox();
            saveStep(); ////console.log('saveStep - ' + 1740);
        } else {
            videoid = video.match(/(?:https?:\/{2})?(?:w{3}\.)?(?:player\.)?vimeo?\.?com(?:\/video)?\/([^\s?]+)/);
            if (videoid != null) {
                videoid[1] = videoid[1].split('?')[0];
                $('.active').children().remove();
                var str = '';
                str = 'https://player.vimeo.com/video/' + videoid[1] + '?autoplay=' + autoplay + '&amp;loop=' + loop + '&amp;showinfo=' + showinfo + '&amp;theme=light&amp;color=red&amp;controls=' + controls + '&amp;modestbranding=&amp;start=0&amp;fs=' + fs + '&amp;iv_load_policy=3&amp;wmode=transparent&amp;rel=' + rel;

                var thumbnailSrc = '';
                $.ajax({
                    type: 'GET',
                    url: 'http://vimeo.com/api/v2/video/' + videoid[1] + '.json',
                    jsonp: 'callback',
                    dataType: 'jsonp',
                    success: function (data) {
                        thumbnailSrc = data[0].thumbnail_large;
                        $('.active').append('<img width="' + width + '" height="' + height + '" rel="' + str + '" src="' + thumbnailSrc + '"/>');
                    }
                });

                hideLitebox();
                saveStep(); ////console.log('saveStep - ' + 1741);
            } else {
                var col = 1.0;
                clearInterval(interval);
                var interval = setInterval(function () {
                    $('#video_url').css('border-color', 'rgba(250, 0, 0, ' + col + ')');
                    col = col - 0.01;
                    if (col <= 0) {
                        $('#video_url').css('border-color', 'initial');
                        clearInterval(interval);
                    }
                }, 10);
                return;
            }
        }
    } else {
        var videoUrl = $('#user_video_url').val();
        var posterUrl = $('#user_poster_url').val();
        if (videoUrl != '') {
            if (posterUrl != '') {
                $('.active>img').attr('src', posterUrl);
            } else {
                $('.active>img').attr('src', '/Areas/Cabinet/Images/video_poster.jpg');
            }
            $('.active>img').attr('rel', videoUrl);
            hideLitebox();
        }
    }
}

function timer_sel(el, param) {
    //console.log('выбираем таймер ???');
    $('#templates_timers>div').css('border', '1px solid rgba(66, 139, 202, 0)');
    $(el).css('border', '1px solid rgba(66, 139, 202, 1)');
}

function savePages(bozo) {
    $('.ui-widget-content>a').css('opacity', 1);
    $('#frame-for-screen').width($(window).width());
    //ccccccccc
    //console.log(downimg($('#frame-for-screen')));
    //сохранение формы, если она есть
    if ($('.edit_form').length) {
        $('#page_tabs').find('[rel="Home"]').first().click();
    }
    //console.log('сохраняем страницы');

    //серый фон
    $('#background_modal').css('top', '0px');
    $('#background_modal').css('left', '0px');
    $('#background_modal').width($(window).width());
    $('#background_modal').height($(window).height());
    //надпись сохранение
    var div = document.createElement('div');
    $(div).attr('id', 'info_save');
    var top = ($(window).height() - 50) / 2;
    var left = ($(window).width() - 200) / 2;
    $(div).attr('style', 'left: ' + left + 'px; top: ' + top + 'px; color: #428bca; font-size: 23px; border: 1px solid rgba(0,0,0,0); border-radius: 5px; background: white; position: absolute; width: 200px; height: 50px; z-index: 100000; text-align: center; line-height: 2;');
    $(div).text('Сохранение...');

    $('body').append(div);

    //сохраняем текущую страницу
    $('#' + $('.tab_active').parent().attr('rel') + '>.page_content_standard').html($('#content_standard').html());
    //удаляем лишние блоки
    removeAdditionalBlocks();
    var pageKeys = [];
    var index = 0;
    var url = "";
    var styles = '';
    var scripts = '';
    var scriptsLast = '';
    var pages = [];
    var links = "";

    $('#pages').children().each(function () {
        pageKeys[index] = $(this).attr('id');
        pages[index] = [];
        url = $('#website_info').attr('rel');
        var pageLoader = '';
        if (pageKeys[index] == 'Layout') {
            $('#styles').children().each(function () {
                styles = styles + '$' + $(this).text();
            });
            $('#scripts').children().each(function () {
                try {
                    var tempScript = $(this).text();
                    var param = tempScript.split('*')[1].split(',')[1].replace(' ', '');
                    if (param == 'lastload') {
                        scriptsLast = scriptsLast + '$$$' + $(this).text();
                    } else {
                        scripts = scripts + '$$$' + $(this).text();
                    }
                } catch (ex) {
                    scripts = scripts + '$$$' + $(this).text();
                }

            });
            var arrayLinks = $('.added_fonts');
            for (var r = 0; r < arrayLinks.length; r++) {
                links = links + ';' + arrayLinks[r].tagName + " href='" + $(arrayLinks[r]).attr('href') + "' class='" + $(arrayLinks[r]).attr('class') + "' rel='" + $(arrayLinks[r]).attr('rel') + "' name='" + $(arrayLinks[r]).attr('name') + "'";
            }


            if ($('#page_loader').attr('option') == 'checked') {
                pageLoader = $('#color_load_line>div>div>div').css('background-color') +
                    ';' +
                    $('#color_load_background>div>div>div').css('background-color');
            } else {
                pageLoader = '';
            }
        }
        pages[index] = $(this).attr('name') + '<' + $(this).attr('id') + '<' + $(this).attr('type') + '<' + $(this).attr('desc') + '<' + $(this).attr('key') + '<' + htmlEntities($('#' + $(this).attr('id') + '>.page_content_standard').html()) + '<' + $('#website_info').attr('title') + '<' + url + '<' + styles + '<' + links + '<' + pageLoader + '<' + htmlEntities(scripts) + '<' + htmlEntities(scriptsLast);
        styles = '';
        scripts = '';
        scriptsLast = '';
        pageLoader = '';
        index++;
    });
    $.ajax({
        url: "/Cabinet/Editor/SavePages",
        data: {
            page: pages,
            bozo: bozo
        },
        async: true,
        typedata: "json",
        success: function (data) {
            if (data === 'ok!') {
                $('#info_save').text('Сохранено!');
                $('#info_save').css('color', '#22b662');
                setTimeout(function () {
                    $('#info_save').remove();
                    $('#background_modal').css('top', '0px');
                    $('#background_modal').css('left', '0px');
                    $('#background_modal').css('width', '0px');
                    $('#background_modal').css('height', '0px');
                }, 1000);
            } else {
                alert('Error :(');
            }
        },
        error: function (data) {
            alert('Error :(');
        },
        type: "POST"
    });

    $.ajax({
        url: "/Cabinet/Editor/DelExcessPages",
        data: { pageKeys: pageKeys, url: url },
        async: true,
        typedata: "json",
        success: function (data) {
            //alert('good');
        },
        error: function (data) {
            //alert('bad');
        },
        type: "POST"
    });

    //открываем получившиеся страницы
    //for (var i = 0; i < pageKeys.length; i++) {        
    //    if (pageKeys[i] != 'Layout') {
    //        var frame = document.createElement('iframe');
    //        if (pageKeys[i] == 'Home') {
    //            $(frame).attr('src', '/Cabinet/Preview/saveFullContent?url=' + url);                
    //        } else {
    //            $(frame).attr('src', '/Cabinet/Preview/saveFullContent?url=' + url + '&page=' + pageKeys[i]);
    //        }
    //        $(frame).attr('style', 'display: none;');
    //        if ($('iframe[src = "' + $(frame).attr('src') + '"]').length > 0) {
    //            //$('iframe[src = "' + $(frame).attr('src') + '"]').contentWindow.location.reload(true);
    //        } else {
    //            $('body').append(frame);
    //        }           

    //    }
    //}
}

function saveStep(parametr) {
    //сохраняем шаг
    correctPosition();
    noRepeatID();
    if ($('.active').attr('type') == 'button' || $('.active').attr('type') == 'btn' || $('.active').attr('type') == 'submit') { saveButtonOption(); }

    $('#' + $('.tab_active').parent().attr('rel') + '>.page_content_standard').html($('#content_standard').html());
    var currentTab = $('.tab_active').parent().attr('rel');
    removeAdditionalBlocks();
    var pageKeys = [];
    var index = 0;
    var url = "";
    var styles = '';
    var pages = [];
    $('#pages').children().each(function () {
        pageKeys[index] = $(this).attr('id');
        pages[index] = [];
        url = $('#website_info').attr('rel');
        if (pageKeys[index] == 'Layout') {
            $('#styles').children().each(function () {
                styles = styles + '$' + $(this).text();
            });
        }
        pages[index] = $(this).attr('name') + '<' + $(this).attr('id') + '<' + $(this).attr('type') + '<' + $(this).attr('desc') + '<' + $(this).attr('key') + '<' + htmlEntities($('#' + $(this).attr('id') + '>.page_content_standard').html()) + '<' + $('#website_info').attr('title') + '<' + url + '<' + styles;
        styles = '';
        index++;
    });
    if (parametr != 'first') {
        parametr == '';
    }
    var urlLanding = $('#website_info').attr('rel');
    $.ajax({
        url: "/Cabinet/Editor/SaveStep",
        data: {
            page: pages,
            currentTab: currentTab,
            currentNumber: currentNumber,
            url: urlLanding,
            parametr: parametr
        },
        async: true,
        typedata: "json",
        success: function (data) {
            $('#reply').attr('onclick', 'step("back")');
            $('#reply>a').css('color', '#428bca');
            $('#share').removeAttr('onclick');
            $('#share>a').css('color', '#444');
            currentNumber = data.current;
            totalNumbers = data.total;
            if (currentNumber == 1) {
                $('#reply').removeAttr('onclick');
                $('#reply>a').css('color', '#444');
            }
        },
        type: "POST"
    });
    $('#' + $('.tab_active').parent().attr('rel') + '>.page_content_standard').html('');
}

function step(parametr) {
    //console.log('нажимаем на кнопку назад или вперед');
    var ref = '';
    if (parametr == 'back') {
        ref = '/Cabinet/Editor/StepBack';
    } else {
        ref = '/Cabinet/Editor/StepNext';
    }
    $.ajax({
        url: ref,
        data: {
            currentNumber: currentNumber
        },
        async: true,
        success: function (data) {
            if (data.res == "") return;
            $('#pages').html('');
            $('#website_info').attr('rel', data.res[0].Url);
            $('#website_info').attr('Title', data.res[0].Name);
            counter = data.res.length;
            for (var i = 0; i < data.res.length; i++) {
                var div = document.createElement('div');
                $(div).attr('id', data.res[i].StringKey.toString());
                $(div).attr('type', data.res[i].Type);
                $(div).attr('name', data.res[i].Title);
                $(div).attr('desc', data.res[i].MetaDescription);
                $(div).attr('key', data.res[i].MetaKeyWords);

                if (!$('#styles').length) {
                    var divS = document.createElement('div');
                    $(divS).css('display', 'none');
                    $('#content').apend(divS);
                }

                if (data.res[i].Styles != null) {
                    var array = data.res[i].Styles.split('$');
                    for (var j = 1; j <= array.length; j++) {
                        try {
                            var idButton = array[j].replace('#', '').split('{')[0];
                            var child = document.createElement('p');
                            $(child).attr('rel', idButton);
                            $(child).text(array[j]);
                            $('#styles').append(child);

                        } catch (ex) {
                            //console.log(ex);
                        }

                    }
                }
                var divC = document.createElement('div');
                $(divC).attr('class', 'page_content_standard');
                $(divC).html($.parseHTML(reHtmlEntities(data.res[i].SiteContent)));
                div.appendChild(divC);
                $('#pages').append(div);
            }
            $('#content_standard').children().each(function () {
                if ($(this).attr('id') != 'additionalBlocks') {
                    $(this).remove();
                }
            });
            loadPages(data.currentTab.toLowerCase());
            $('#pages>#Home>.page_content_standard').children().each(function () {
                $('#content_standard').append($(this));
            });

            hideTextarea();
            $('.active').removeClass('active');
            currentNumber = parseInt(data.current);
            totalNumbers = parseInt(data.total);

            if (currentNumber == totalNumbers) {
                $('#share>a').css('color', '#444');
                $('#share').removeAttr('onclick');
                if (totalNumbers == 1) {
                    $('#reply>a').css('color', '#444');
                    $('#reply').removeAttr('onclick');
                }
            } else if (currentNumber < totalNumbers) {
                $('#share>a').css('color', '#428bca');
                $('#share').attr('onclick', 'step("next")');
            }
            if (currentNumber >= 1) {
                $('#reply>a').css('color', '#428bca');
                $('#reply').attr('onclick', 'step("back")');
            }
            if (currentNumber == 1 || (currentNumber == 2 && data.res[0].Url == '')) {
                $('#reply>a').css('color', '#444');
                $('#reply').removeAttr('onclick');
            }
            $('[rel = "' + data.currentTab + '"]')[0].click();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
        },
        type: "POST"
    });
}

function reHtmlEntities(str) {
    var string = String(str).replace(/&amp;/g, '&').replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&quot;/g, '"').replace(/\(\"/g, '(\'').replace(/\( \"/g, '(\'').replace(/\"\)/g, '\')');
    return delSpaces(string);
}

function htmlEntities(str) {
    var string = String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/\n/g, ' ');
    return delSpaces(string);
}


function delSpaces(str) {
    var string = str.replace('  ', ' ');
    if (string.indexOf('  ') != -1) {
        string = delSpaces(string);
    }
    return string;
}

function create() {
    //console.log('создаем новый лендинг');
    if ($('#website_url').val() != '' && $('#website_name').val() != '') {
        $('#create').click();//???
        hideLitebox();
        $('#website_info').attr('rel', $('#website_url').val());
        $('#website_info').attr('title', $('#website_name').val());
    } else {
        if ($('#website_url').val() == '') {
            $("#attention_website_url").animate({
                opacity: 1
            }, 200, function () {
                $("#attention_website_url").animate({
                    opacity: 0
                }, 200, function () {
                    $("#attention_website_url").animate({
                        opacity: 1
                    }, 200, function () {
                        $("#attention_website_url").animate({
                            opacity: 0
                        }, 200, function () {
                            $("#attention_website_url").animate({
                                opacity: 1
                            }, 200, function () {
                                $("#attention_website_url").animate({
                                    opacity: 0
                                }, 2500);
                            });
                        });
                    });
                });
            });
        }
        if ($('#website_name').val() == '') {
            $("#attention_website_name").animate({
                opacity: 1
            }, 200, function () {
                $("#attention_website_name").animate({
                    opacity: 0
                }, 200, function () {
                    $("#attention_website_name").animate({
                        opacity: 1
                    }, 200, function () {
                        $("#attention_website_name").animate({
                            opacity: 0
                        }, 200, function () {
                            $("#attention_website_name").animate({
                                opacity: 1
                            }, 200, function () {
                                $("#attention_website_name").animate({
                                    opacity: 0
                                }, 2500);
                            });
                        });
                    });
                });
            });
        }
    }
    saveStep('first');
}
function closeWindow() {
    //console.log('закрываем окно ??? closeWindow');
    $('#exit_btn_hidden')[0].click();
}

function saveAndExitWindowOpen() {
    //console.log('сохраняем и закрываем лендинг');
    if ($('.edit_form').length) {
        $('#page_tabs').find('[rel="Home"]').first().click();
    }


    hide_menu();
    showLitebox('saveAndExitWindow');
}

function saveAndExit() {
    //console.log('сохраняем и закрываем лендинг 2');
    savePages();
    hideLitebox();
    closeWindow();
}


function openWondowAddSection(x, y) {
    //console.log('открываем окно сыбора кол-ва колонок секции');
    showLitebox('addSection');
}

function closeAddSectionWondow() {
    //console.log('закрываем окно сыбора кол-ва колонок секции');
    $('.pasteSection').remove();
    hideLitebox();

}

function addSection(column) {
    //console.log('добавляем секцию');
    var x = $('.pasteSection').attr('X');
    var y = $('.pasteSection').attr('Y');

    var section = document.createElement('div');
    var random = Math.round(Math.random() * 10000);
    $(section).attr('id', 'section_' + random);
    $(section).addClass('section');
    var oldSection = $('.container_section');

    var contentSection = document.createElement('div');
    $(contentSection).attr('id', 'content_section_' + random);
    $(contentSection).addClass('container_section');
    //var width1 = 0;
    //var width2 = 0;

    //var percent = '';

    //width11 = getStyle($('.section').first(), 'width');
    //width22 = getStyle($('.section').first().children().first(), 'width');

    //if (width11.indexOf('px') >= 0) {
    //    try {
    //        width11 = $('.section').first().attr('style').replace('width:', '$').split('$')[1].split(';')[0];
    //    } catch (ex123) {

    //    }
    //}

    //if (width22.indexOf('px') >= 0) {
    //    try {
    //        width22 = $('.section').first().children().first().attr('style').replace('width:', '$').split('$')[1].split(';')[0];
    //    } catch (ex1234) {

    //    }
    //}

    //width1 = parseInt(width11);
    //width2 = parseInt(width22);

    //if (width1 > width2) {
    //    percent = width2 + '%';
    //} else {
    //    percent = width1 + '%';
    //}
    //if (!$('.section').length) {
    //    percent = '95%';
    //}
    var heightSection;
    if ($('#page_content_height_val').val() != '') {
        heightSection = $('#page_content_height_val').val();
        $('#page_content_height_val').val('');
    } else {
        heightSection = 300;
    }

    $(section).attr('style', 'width: ' + $('#content_width_val').val() + $('#content_width_val').next().text() + '; height: ' + heightSection + 'px; position:relative; margin: 0 auto; background: rgb(255, 255, 255);');
    $(contentSection).attr('style', 'width: 100%; height: 100%; text-align: left; margin: 0 auto;');

    switch (column) {
        case '1':
            var width = '';
            var height = '300px';
            var style = 'height: ' + height + '; position: relative; display: inline-block;';
            if ($('.tab_active').parent().attr('rel').indexOf('litebox') >= 0) {
                height = $('#height_l_val').val() + $('#height_l_val').next().text();
                width = $('#width_l_val').val() + $('#width_l_val').next().text();
                style = 'width:' + width + '; height: ' + height + '; position: relative; display: inline-block;';
                $(section).attr('id', $('.tab_active').parent().attr('rel').replace('_pag', ''));
                $(section).css('width', width);
                $(section).css('height', height);
                $(section).css('top', '20px');
                $(section).attr('option', $('#edit_background_color_ll>div>div').css('background-color'));
                $(contentSection).css('width', width);
                $(contentSection).css('height', height);
                var btnClose = document.createElement('div');
                $(btnClose).attr('class', 'btn_close ui-widget-content');
                $(btnClose).attr('id', 'btn_close_' + random);
                $(btnClose).attr('type', 'btn');
                $(btnClose).attr('onclick', 'hideLitebox()');
                $(btnClose).attr('style', "position: absolute; cursor: pointer; background-image: url('/Areas/Cabinet/Images/btn_close.png'); background-size: 100%; background-color: rgba(255,255,255,0);  z-index: 1; top: -13px; right: -13px; width: 25px; height: 25px;");
                $(section).append(btnClose);

            }
            var column11 = document.createElement('div');
            $(column11).addClass('column1 column');
            $(column11).attr('id', 'c' + Math.round((Math.random() * 100000)));
            $(contentSection).append(column11);
            break;
        case '2':
            var column21 = document.createElement('div');
            var column22 = document.createElement('div');
            $(column21).addClass('column2 column');
            $(column22).addClass('column2 column');
            $(column21).attr('id', 'c' + Math.round((Math.random() * 100000)));
            $(column22).attr('id', 'c' + Math.round((Math.random() * 100000)));
            $(contentSection).append(column21);
            $(contentSection).append(column22);
            break;
        case '3':
            var column31 = document.createElement('div');
            var column32 = document.createElement('div');
            var column33 = document.createElement('div');
            $(column31).addClass('column3 column');
            $(column32).addClass('column3 column');
            $(column33).addClass('column3 column');
            $(column31).attr('id', 'c' + Math.round((Math.random() * 10000)));
            $(column32).attr('id', 'c' + Math.round((Math.random() * 10000)));
            $(column33).attr('id', 'c' + Math.round((Math.random() * 10000)));
            $(contentSection).append(column31);
            $(contentSection).append(column32);
            $(contentSection).append(column33);
            break;
        case '4':
            var column41 = document.createElement('div');
            var column42 = document.createElement('div');
            var column43 = document.createElement('div');
            var column44 = document.createElement('div');
            $(column41).addClass('column4 column');
            $(column42).addClass('column4 column');
            $(column43).addClass('column4 column');
            $(column44).addClass('column4 column');
            $(column41).attr('id', 'c' + Math.round((Math.random() * 10000)));
            $(column42).attr('id', 'c' + Math.round((Math.random() * 10000)));
            $(column43).attr('id', 'c' + Math.round((Math.random() * 10000)));
            $(column44).attr('id', 'c' + Math.round((Math.random() * 10000)));
            $(contentSection).append(column41);
            $(contentSection).append(column42);
            $(contentSection).append(column43);
            $(contentSection).append(column44);
            break;
        case 'sectionContent':
            var td = document.createElement('td');
            $(td).attr('align', 'center');
            $(td).attr('valign', 'middle');
            $(td).attr('style', 'font-size: 30px; color: #aaa;');
            $(td).text('Содержание страниц');
            var tr = document.createElement('tr');
            var tab = document.createElement('table');
            $(tab).attr('style', 'width: 100%; height: 100%;');
            var sec = document.createElement('div');
            $(sec).addClass('sectionContent section');
            $(sec).attr('id', 'sectionContent');
            tr.appendChild(td);
            tab.appendChild(tr);
            sec.appendChild(tab);
            $('#content_standard').append(sec);
            break;
        default:
            var column5 = document.createElement('div');
            $(column5).addClass('column1 column');
            $(contentSection).append(column5);
            break;
    }
    if (column != 'sectionContent') {
        if ($('.tab_active').parent().first().attr('rel').indexOf('litebox') >= 0) {
            $(section).attr('type', 'sectionLitebox');
            $(section).attr('m-title', $('.tab_active').parent().first().attr('rel'));
            $(section).append(contentSection);
            $('#content_standard').append(section);
        } else {
            $(section).attr('type', 'section');
            $(section).attr('m-title', 'секция_' + random);
            $(contentSection).css('height', '100%');
            $(section).append(contentSection);
            try {
                var el = document.elementFromPoint(parseInt(x), parseInt(y));
                if ($('.pasteSection').length) {
                    $('.pasteSection').after(section);
                } else if ($(el).closest('#content') != undefined) {
                    $('#content_standard').append(section);
                }
            } catch (ex) {
                $('#content_standard').append(section);
            }
        }

    }
    closeAddSectionWondow();
    if ($('#website_info').attr('rel') == '' || $('#website_info').attr('rel') == undefined) return;
    saveStep();
}

function btn_prev_section_width() {
    if ($('.active').attr('class').indexOf('section') < 0) {
        if ($('#section_width_val').val() > 0) {
            var widthW = getStyle($('.active'), 'width');
            if ($('.active').attr('class').indexOf('containerForm') >= 0) {
                widthW = getStyle($('.active').parent(), 'width');
            }
            if ($('.active').attr('class').indexOf('editable_element') >= 0) {
                if ($('.active').attr('type') == 'submit' || $('.active').attr('type') == 'title') {
                    widthW = getStyle($('.active'), 'width');
                } else {
                    widthW = getStyle($('.active').parent(), 'width');
                }
            }
            if (widthW.indexOf('%') >= 0) {
                $('#section_width_val').val(parseFloat(widthW) - 1);
                $('#panel_section_width_val').val($('#section_width_val').val());
                if ($('.active').attr('class').indexOf('editable_element') >= 0 || $('.active').attr('class').indexOf('containerForm') >= 0) {
                    if ($('.active').attr('type') == 'submit' || $('.active').attr('type') == 'title') {
                        $('.active').css('width', $('#section_width_val').val() + '%');
                    } else {
                        $('.active').parent().css('width', $('#section_width_val').val() + '%');
                    }
                } else {
                    $('.active').css('width', $('#section_width_val').val() + '%');
                }
            } else {
                $('#section_width_val').val(parseFloat(widthW) - 1);
                $('#panel_section_width_val').val($('#section_width_val').val());
                if ($('.active').attr('class').indexOf('editable_element') >= 0 || $('.active').attr('class').indexOf('containerForm') >= 0) {
                    if ($('.active').attr('type') == 'submit' || $('.active').attr('type') == 'title') {
                        $('.active').css('width', $('#section_width_val').val() + 'px');
                    } else {
                        $('.active').parent().css('width', $('#section_width_val').val() + 'px');
                    }
                } else {
                    $('.active').css('width', $('#section_width_val').val() + 'px');
                }
            }
            if ($('.active').closest('.section').length) {
                $('#resize').width(parseFloat($('.active').css('width')) + parseInt($(".active").css('border-left-width')) + parseInt($(".active").css('border-right-width')) + 1);
                $('#move_block').width(parseFloat($('.active').css('width')) + parseInt($(".active").css('border-left-width')) + parseInt($(".active").css('border-right-width')));
                setPositionLeftToElementButtons();
            }
        }
        groupProperty('width', parseInt($('#section_width_val').val()) + $('#section_width_val').next().text());
        correctPosition();
        setPositionResizeBlockSection();
        saveStep();
    }
}

function btn_next_section_width() {
    if ($('.active').attr('class').indexOf('section') < 0) {
        var persent = ((parseFloat($('.active').css('left')) + parseFloat($('.active').css('width'))) / parseFloat($('.active').parent().css('width'))) * 100;
        var widthW = getStyle($('.active'), 'width');
        if ($('.active').attr('class').indexOf('containerForm') >= 0) {
            widthW = getStyle($('.active').parent(), 'width');
        }
        if ($('.active').attr('class').indexOf('editable_element') >= 0) {
            if ($('.active').attr('type') == 'submit' || $('.active').attr('type') == 'title') {
                widthW = getStyle($('.active'), 'width');
            } else {
                widthW = getStyle($('.active').parent(), 'width');
            }
        }
        var leftL = getStyle($('.active'), 'left');
        if (persent < 99.8) {
            if (widthW.indexOf('%') >= 0) {
                $('#section_width_val').val(parseFloat(widthW) + 1);
                $('#panel_section_width_val').val($('#section_width_val').val());
                if ($('.active').attr('class').indexOf('editable_element') >= 0 || $('.active').attr('class').indexOf('containerForm') >= 0) {
                    if ($('.active').attr('type') == 'submit' || $('.active').attr('type') == 'title') {
                        $('.active').css('width', $('#section_width_val').val() + '%');
                    } else {
                        $('.active').parent().css('width', $('#section_width_val').val() + '%');
                    }
                } else {
                    $('.active').css('width', $('#section_width_val').val() + '%');
                }
            } else {
                $('#section_width_val').val(parseFloat(widthW) + 1);
                $('#panel_section_width_val').val($('#section_width_val').val());
                if ($('.active').attr('class').indexOf('editable_element') >= 0 || $('.active').attr('class').indexOf('containerForm') >= 0) {
                    if ($('.active').attr('type') == 'submit' || $('.active').attr('type') == 'title') {
                        $('.active').css('width', $('#section_width_val').val() + 'px');
                    } else {
                        $('.active').parent().css('width', $('#section_width_val').val() + 'px');
                    }
                } else {
                    $('.active').css('width', $('#section_width_val').val() + 'px');
                }
            }
            $('#resize').width(parseFloat($('.active').css('width')) + parseInt($(".active").css('border-left-width')) + parseInt($(".active").css('border-right-width')));
            $('#move_block').width(parseFloat($('.active').css('width')) + parseInt($(".active").css('border-left-width')) + parseInt($(".active").css('border-right-width')));
            setPositionLeftToElementButtons();
        } else {
            if (widthW.indexOf('%') >= 0) {
                $('#section_width_val').val(parseFloat(widthW) + 1);
                $('#panel_section_width_val').val($('#section_width_val').val());
                if ($('.active').attr('class').indexOf('editable_element') >= 0 || $('.active').attr('class').indexOf('containerForm') >= 0) {
                    if ($('.active').attr('type') == 'submit' || $('.active').attr('type') == 'title') {
                        $('.active').css('width', $('#section_width_val').val() + '%');
                    } else {
                        $('.active').parent().css('width', $('#section_width_val').val() + '%');
                    }
                } else {
                    $('.active').css('width', $('#section_width_val').val() + '%');
                }
            } else {
                $('#section_width_val').val(parseFloat(widthW) + 1);
                $('#panel_section_width_val').val($('#section_width_val').val());
                if ($('.active').attr('class').indexOf('editable_element') >= 0 || $('.active').attr('class').indexOf('containerForm') >= 0) {
                    if ($('.active').attr('type') == 'submit' || $('.active').attr('type') == 'title') {
                        $('.active').css('width', $('#section_width_val').val() + 'px');
                    } else {
                        $('.active').parent().css('width', $('#section_width_val').val() + 'px');
                    }
                } else {
                    $('.active').css('width', $('#section_width_val').val() + 'px');
                }
            }
            var width = ((parseFloat($('.active').css('width')) / parseFloat($('.active').parent().css('width'))) * 100).toFixed(1);
            var left = 0;
            if (leftL.indexOf('%') >= 0) {
                left = 100 - width;
                $('.active').css('left', left + '%');
            } else {
                left = parseFloat($('.active').parent().css('width')) - parseFloat($('.active').css('width'));
                $('.active').css('left', left + 'px');
            }
            if ($('.active').closest('.section').length) {
                $('#active').css('left', $('.active').offset().left - ($('#left_panel').width() + parseInt($('#left_panel').css('border-left')) + parseInt($('#left_panel').css('border-right'))));
                $('#move_block').css('left', $('.active').closest('.section').position().left + $('.active').parent().position().left + parseFloat($('.active').css('left')) + 5);
                $('#move_text').css('left', $('#move-text').offset().left - $('#workspace').offset().left);
                $('#move_text').css('top', $('#move-text').offset().top - $('#workspace').offset().top - 15);
                $('#resize').width($('.active').width() + parseInt($(".active").css('border-left-width')) + parseInt($(".active").css('border-right-width')) + parseInt($(".active").css('padding-left')) + parseInt($(".active").css('padding-right')) + 1);
                $('#move_block').width($('.active').width() + parseInt($(".active").css('border-left-width')) + parseInt($(".active").css('border-right-width')) + parseInt($(".active").css('padding-left')) + parseInt($(".active").css('padding-right')) - 10);
                setPositionLeftToElementButtons();
            }
        }

        groupProperty('width', parseInt($('#section_width_val').val()) + $('#section_width_val').next().text());

        correctFormSize();
        correctPosition();
        setPositionResizeBlockSection();
        saveStep();
    }
}

function correctFormSize() {
    //только для окна редактирования форм
    if ($('.edit_form').length) {
        var formHeight = parseInt($('.edit_form').css('height'));
        var elTop = parseInt($('.active').parent().css('top'));
        var elHeight = parseInt($('.active').parent().css('height'));
        var formWidth = parseInt($('.edit_form').css('width'));
        var elLeft = parseInt($('.active').parent().css('left'));
        var elWidth = parseInt($('.active').css('width'));
        if (formWidth < elLeft + elWidth) {
            $('.edit_form').width(elLeft + elWidth);
        }
        //console.log('formHeight=' + formHeight);
        //console.log('elTop=' + elTop);
        //console.log('elHeight=' + elHeight);
        if (formHeight < elTop + elHeight) {
            $('.edit_form').height(elTop + elHeight);
        }
        if ($('.form_option_parametr input').prop('checked')) {
            $('.containerInput').each(function () {
                var thisElLeft = parseInt($(this).css('left'));
                var thisElWidth = parseInt($(this).css('width'))
                if (formWidth < thisElLeft + thisElWidth) {
                    $('.edit_form').width(thisElLeft + thisElWidth);
                }
                var thisElTop = parseInt($(this).css('top'));
                var thisElHeight = parseInt($(this).css('height'));
                if (formHeight < thisElTop + thisElHeight) {
                    $('.edit_form').height(thisElTop + thisElHeight);
                }
            });
        }
    }
}

function btn_prev_section_height() {
    if ($('#section_height_val').val() > 0) {
        if ($('.active').attr('class').indexOf('section') < 0) {
            var height = getStyle($('.active'), 'height');
            if (height.indexOf('%') >= 0) {
                $('#section_height_val').val(parseFloat(height) - 1);
                $('#panel_section_height_val').val($('#section_height_val').val());
                $('.active').css('height', $('#section_height_val').val() + '%');
            } else {
                $('#section_height_val').val(parseInt(height) - 1);
                $('#panel_section_height_val').val($('#section_height_val').val());
                $('.active').css('height', $('#section_height_val').val() + 'px');
            }
            try {
                correctSectionHeight("");
            } catch (ex) {

            }
        } else {
            $('#section_height_val').val(parseInt($('#section_height_val').val()) - 1);
            $('#panel_section_height_val').val($('#section_height_val').val());
            $('.active').css('height', parseInt($('#section_height_val').val()));
            //$('.active .column').height(parseInt($('#section_height_val').val()));
        }
        $('#move_block').css('height', parseFloat($('.active').css('height')) - 6);
        $('#resize').height(parseInt($('.active').height()) + parseInt($(".active").css('border-top-width')) + parseInt($(".active").css('border-bottom-width')) + parseInt($(".active").css('padding-top')) + parseInt($(".active").css('padding-bottom')));
        groupProperty('height', parseInt($('#section_height_val').val()) + $('#section_height_val').next().text());
        correctPosition();
        setPositionResizeBlockSection();
        saveStep();
    }
}

function btn_next_section_height() {
    var persent = ((parseFloat($('.active').css('top')) + parseFloat($('.active').css('height'))) / parseFloat($('.active').parent().css('height'))) * 100;
    if ($('.active').attr('class').indexOf('section') < 0) {
        if (persent < 99.8) {
            var height = getStyle($('.active'), 'height');
            //console.log('===height===' + height);
            if (height.indexOf('%') >= 0) {
                $('#section_height_val').val((parseInt(height) + 1));
                $('#panel_section_height_val').val($('#section_height_val').val());
                $('.active').css('height', parseInt($('#section_height_val').val()) + '%');
            } else {
                $('#section_height_val').val((parseInt(height) + 1));
                $('#panel_section_height_val').val($('#section_height_val').val());
                $('.active').css('height', parseInt($('#section_height_val').val()) + 'px');
            }
            $('#move_block').css('height', parseInt($('.active').css('height')) - 6);
        } else {
            $('#section_height_val').val((parseInt($('#section_height_val').val()) + 1));
            $('#panel_section_height_val').val($('#section_height_val').val());
            if ($('#section_height_val').next().text() == '%') {
                $('.active').css('height', (parseInt($('#section_height_val').val())) + '%');
            } else {
                $('.active').css('height', (parseInt($('#section_height_val').val())) + 'px');
            }

            var padding = ((parseFloat($('.active').css('padding-top')) + parseFloat($('.active').css('padding-bottom'))) / parseFloat($('.active').parent().css('height'))) * 100;
            var top = 100 - (parseFloat($('#section_height_val').val()) + padding);
            var heights = getStyle($('.active'), 'height');
            //console.log('===heights===' + heights);
            if (heights.indexOf('%') >= 0) {
                top = (100 - parseFloat(heights)) + '%';
            } else {
                top = (parseFloat($('.active').parent().css('width')) - parseFloat($('.active').css('width'))) + 'px';
            }
            $('.active').css('top', top);
            $('#section_top_val').val(top);
            if ($('.active').attr('class').indexOf('editable_element') < 0) {
                $('#active').css('top', $('.active').closest('.section').position().top + $('.active').css('top'));
                $('#move_block').css('top', $('.active').closest('.section').position().top + $('.active').css('top') + 5);
                $('#name_block').css('top', -($('#name_block').height()));
            }
        }
    } else {
        $('#section_height_val').val(parseInt($('#section_height_val').val()) + 1);
        $('#panel_section_height_val').val($('#section_height_val').val());
        //$('.active .column').height(parseInt($('#section_height_val').val()));
        $('.active').height(parseInt($('#section_height_val').val()));
    }

    $('#resize').height(parseInt($('.active').height()) + parseInt($(".active").css('border-top-width')) + parseInt($(".active").css('border-bottom-width')) + parseInt($(".active").css('padding-top')) + parseInt($(".active").css('padding-bottom')));
    groupProperty('height', parseInt($('#section_height_val').val()) + $('#section_height_val').next().text());
    correctFormSize();
    correctPosition();
    setPositionResizeBlockSection();
    saveStep();
}

function btn_prev_section_top() {
    var minTop = 0;
    if ($('.active').attr('type') == 'btn') {
        minTop = -20;
    }
    if (parseFloat($('#section_top_val').val()) > minTop) {
        var top = getStyle($('.active'), 'top');
        if (top.indexOf('%') >= 0) {
            $('#section_top_val').val(parseFloat(top) - 0.1);
            $('.active').css('top', $('#section_top_val').val() + '%');
        } else {
            $('#section_top_val').val(parseFloat(top) - 1);
            $('.active').css('top', $('#section_top_val').val() + 'px');
        }
        try {
            $('#active').css('top', $('.active').closest('.section').position().top + parseInt($('.active').css('top')));
            $('#move_block').css('top', $('.active').closest('.section').position().top + parseInt($('.active').css('top')));
            $('#name_block').css('top', -($('#name_block').height()));
        } catch (ex) {
        }
        correctPosition();
        saveStep();
    }
}

function btn_next_section_top() {
    var persent = ((parseFloat($('.active').css('height')) + parseFloat($('.active').css('top'))) / $('.active').parent().height()) * 100;
    //console.log('persent=' + persent);
    if (persent < 100) {
        var top = getStyle($('.active'), 'top');
        if (top.indexOf('%') >= 0) {
            $('#section_top_val').val(parseFloat(top) + 0.1);
            $('.active').css('top', $('#section_top_val').val() + '%');
        } else {
            $('#section_top_val').val(parseFloat(top) + 1);
            $('.active').css('top', $('#section_top_val').val() + 'px');
        }
        try {
            $('#active').css('top', $('.active').closest('.section').position().top + parseInt($('.active').css('top')));
            $('#move_block').css('top', $('.active').closest('.section').position().top + parseInt($('.active').css('top')));
            $('#name_block').css('top', -($('#name_block').height()));
        } catch (ex) {
        }
        correctPosition();
        saveStep();
    }
}

function btn_prev_section_left() {
    if (parseFloat($('#section_left_val').val()) > 0) {
        var leftL = getStyle($('.active'), 'left');

        if (leftL.indexOf('%') >= 0) {
            $('#section_left_val').val(parseFloat(leftL) - 0.1);
            $('.active').css('left', $('#section_left_val').val() + '%');
        } else {
            $('#section_left_val').val(parseFloat(leftL) - 1);
            $('.active').css('left', $('#section_left_val').val() + 'px');
        }

        var offsetEl = $('.active').offset();
        var offsetCs = $('#content_standard').offset();
        var left = offsetEl.left - offsetCs.left;

        $('#active').css('left', left);
        $('#move_block').css('left', left);
        $('#move_text').css('left', $('#move-text').offset().left - $('#workspace').offset().left);
        $('#move_text').css('top', $('#move-text').offset().top - $('#workspace').offset().top - 15);
        checkPositionCheckbox();
        saveStep();
    }
}

function btn_next_section_left() {
    var persent = ((parseFloat($('.active').css('left')) + parseFloat($('.active').css('width'))) / $('.active').parent().width()) * 100;
    if (persent < 100) {
        var leftL = getStyle($('.active'), 'left');

        if (leftL.indexOf('%') >= 0) {
            $('#section_left_val').val(parseFloat(leftL) + 0.1);
            $('.active').css('left', $('#section_left_val').val() + '%');
        } else {
            $('#section_left_val').val(parseFloat(leftL) + 1);
            $('.active').css('left', $('#section_left_val').val() + 'px');
        }
        var offsetEl = $('.active').offset();
        var offsetCs = $('#content_standard').offset();
        var left = offsetEl.left - offsetCs.left;

        $('#active').css('left', left);
        $('#move_block').css('left', left);
        $('#move_text').css('left', $('#move-text').offset().left - $('#workspace').offset().left);
        $('#move_text').css('top', $('#move-text').offset().top - $('#workspace').offset().top - 15);
        checkPositionCheckbox();
        saveStep();
    }
}

function maxWidth(elem) {
    if (!$(elem).prop("checked")) {
        if ($('.active').attr('class').indexOf('section') < 0) {
            $('.active').width($('.active').children().width());
        } else {
            var sectionWidth1 = $('.active').width();
            var containerWidth1 = $('.active>.container_section').width();
            var persent1 = Math.round((containerWidth1 / sectionWidth1) * 100);
            $('.active>.container_section').css('width', '100%');
            $('.active').css('width', persent1 + '%');
            $('.active').css('left', 0 + '%');
        }
    } else {
        if ($('.active').attr('class').indexOf('section') < 0) {
            $('.active').css('width', '100%');
            $('.active').css('left', '0%');
        } else {
            var contentWidth2 = $('#content_standard').width();
            var sectionWidth2 = $('.active').width();
            var persent2 = Math.round((sectionWidth2 / contentWidth2) * 100);
            $('.active').css('width', '100%');
            $('.active>.container_section').css('width', persent2 + '%');
            $('.active').css('left', '0%');
        }
    }
    $('#active').css('left', $('.active').offset().left - $('#content_standard').offset().left);
    $('#resize').css('width', parseFloat($('.active').css('width')));
    setPositionLeftToElementButtons();
    $('#move_block').css('width', parseFloat($('.active').css('width')) - 10);
    $('#move_block').css('left', '5px');
    $('#move_text').css('left', $('#move-text').offset().left - $('#workspace').offset().left);
    $('#move_text').css('top', $('#move-text').offset().top - $('#workspace').offset().top - 15);
    $('#remove_element').css('left', $('.active').width() + parseInt($('.active').css('padding-left')) + parseInt($('.active').css('padding-right')) + parseInt($('.active').css('border-left-width')) + parseInt($('.active').css('border-right-width')) + 3);
    saveStep(); ////console.log('saveStep - ' + 184);
}

function background_image_section_del() {
    $('#section_background_position').hide();
    $('#background-position').hide();
    $('#position-background').hide();
    $('#section_background').show();
    $('.active').css('background-image', 'none');
    $('#background_image_section_del').hide();
    saveStep(); ////console.log('saveStep - ' + 185);
}

function imagePosition(align) {
    $('#img_lt').attr('src', '/Areas/Cabinet/Content/images/lt.png');
    $('#img_t').attr('src', '/Areas/Cabinet/Content/images/t.png');
    $('#img_rt').attr('src', '/Areas/Cabinet/Content/images/rt.png');
    $('#img_l').attr('src', '/Areas/Cabinet/Content/images/l.png');
    $('#img_c').attr('src', '/Areas/Cabinet/Content/images/c.png');
    $('#img_r').attr('src', '/Areas/Cabinet/Content/images/r.png');
    $('#img_lb').attr('src', '/Areas/Cabinet/Content/images/lb.png');
    $('#img_b').attr('src', '/Areas/Cabinet/Content/images/b.png');
    $('#img_rb').attr('src', '/Areas/Cabinet/Content/images/rb.png');

    $('.img_align').css('border', '0.5px solid rgba(0,0,0,0)');
    $('.img_align').css('background', 'rgba(0,0,0,0)');
    switch (align) {
        case 'lt':
            $('.active').css('background-position', 'top left');
            break;
        case 't':
            $('.active').css('background-position', 'top center');
            break;
        case 'rt':
            $('.active').css('background-position', 'top right');
            break;
        case 'l':
            $('.active').css('background-position', 'center left');
            break;
        case 'c':
            $('.active').css('background-position', 'center');
            break;
        case 'r':
            $('.active').css('background-position', 'center right');
            break;
        case 'lb':
            $('.active').css('background-position', 'bottom left');
            break;
        case 'b':
            $('.active').css('background-position', 'bottom center');
            break;
        case 'rb':
            $('.active').css('background-position', 'bottom right');
            break;
    }
    $('#img_' + align).parent().css('background', '#2d2d2d');
    $('#img_' + align).parent().css('border', '0.5px solid #428bca');
    $('#img_' + align).attr('src', '/Areas/Cabinet/Content/images/' + align + 'h.png');
    saveStep(); ////console.log('saveStep - ' + 186);
}

function backgroundAttachment(elem) {
    if (!$(elem).prop("checked")) {
        $('.active').css('background-attachment', 'scroll');
    } else {
        $('.active').css('background-attachment', 'fixed');
    }
    saveStep(); ////console.log('saveStep - ' + 187);
}

function show_menu() {
    //console.log('показать меню');
    if ($('.edit_form').length) {
        $('#page_tabs').find('[rel="Home"]').first().click();
    }
    $('#general_menu').animate({
        height: 6 * 35
    }, 300);
}

function hide_menu() {
    //console.log('скрыть меню');
    $('#general_menu').animate({
        height: 0
    }, 300);
}

function previewLanding() {
    //console.log('предпросмотр лендинга');
    if ($('.edit_form').length) {
        $('#page_tabs').find('[rel="Home"]').first().click();
    }
    var url = $('#website_info').attr('rel');
    $('#previewLanding').attr('href', '/Cabinet/Preview?url=' + url);
    $('#previewLanding')[0].click();
    hide_menu();
}

$(function () {
    $('.element').hover(function () {
        var top = $(this).offset().top + 20;
        var left = $(this).offset().left + 30;
        var title = document.createElement('div');
        $(title).attr('class', 'title_element');
        $(title).attr('style', 'max-height: 50px; width: auto; opacity: 0; padding: 2px 5px; background: white; border: 1px solid #d3d3d3; box-shadow: 2px 2px 2px #555; position: absolute; z-index: 9999; top:' + top + 'px; left:' + left + 'px;');
        $(title).text($(this).attr('data-title'));
        $('body').append(title);
        $(title).animate({
            opacity: 1
        }, 300);
    },
        function () {
            $('.title_element').remove();
        }
    );

    $('#open_landing').hover(function () {
        var height = $('.opening_menu').length * 35;
        var top = $('#open_landing').offset().top;
        var left = $('#open_landing').offset().left + $('#general_menu').width();
        $('#open_landing_menu').css('top', top);
        $('#open_landing_menu').css('left', left);
        $('#open_landing_menu').animate({
            height: height
        }, 200);
    },
        function (event) {
            var el = document.elementFromPoint(event.clientX, event.clientY);
            var elem = $(el).closest('#open_landing_menu');
            if ($(elem).attr('id') == undefined) {
                $('#open_landing_menu').animate({
                    height: 0
                }, 200);
            }
        });

    $('#open_landing_menu').hover(
        function () { },
        function () {
            $('#open_landing_menu').animate({
                height: 0
            }, 200);
        }
        );


    $('.sellect_sections').hover(
        function () {
            $('.zoom').remove();
            var parentTop = $(this).position().top;
            var parentLeft = $(this).position().left;
            var parentWidth = $(this).width();
            var zoom = document.createElement('div');
            $(zoom).attr('class', 'zoom');
            $(zoom).attr('onclick', 'viewSection("' + $(this).attr('src') + '", ' + ($(this).width() / $(this).height()) + ')');
            $(zoom).attr('style', 'z-index: 1001; width: 15px; background: rgba(94, 93, 93, 0.6); height: 15px; color: #428bca; cursor:pointer; position: absolute; top:' + (parentTop + 2) + 'px; left: ' + (parentLeft + parentWidth - 2) + 'px;');
            $(zoom).attr('rel', $(this).attr('src'));
            var i = document.createElement('i');
            $(i).addClass('fa fa-search');
            $(zoom).append(i);
            $('#sub_menu_sections').append(zoom);

        },
        function (event) {

        }
        );
});

function viewSection(src, k) {
    var coef = parseFloat(k);
    $('#zoom_section').remove();
    var width = $(window).width() / 2;
    var zoom = document.createElement('div');
    $(zoom).attr('id', 'zoom_section');
    $(zoom).attr('style', 'width: ' + width + 'px; height: ' + (width / coef) + 'px;')
    var img = document.createElement('img');
    $(img).attr('src', src);
    $(img).attr('style', 'width: 100%;');
    $(zoom).append(img);
    $('.litebox').append(zoom);
    showLitebox('zoom_section');
}

function save_ETE(element) {
    //console.log('сохраняем текст');
    if (element == 'submit') {
        $('.edit_form').find('[type="submit"]').text($('#edit_text').val());
    } if (element == 'title') {
        $('.edit_form').find('[type="title"]').children().first().text($('#edit_text').val());
    } else {
        if ($('.active').attr('type') == 'btn') {
            $('.active').text($('#edit_text').val());
        } else {
            $('.active').children().first().text($('#edit_text').val());
        }
    }
    hideLitebox();
    saveStep();
}

function borderRadius(id) {
    if ($('#' + id).attr('src').indexOf(id) >= 0) {
        $('#' + id).attr('src', '/Areas/Cabinet/Content/images/' + id[0] + 'r' + id[1] + '.png');
    } else {
        $('#' + id).attr('src', '/Areas/Cabinet/Content/images/' + id + '.png');
    }
    var b1 = 0, b2 = 0, b3 = 0, b4 = 0;
    if ($('#b1').attr('src').indexOf('b1') < 0) {
        b1 = parseInt($('#border_radiusi').val());
    }
    if ($('#b2').attr('src').indexOf('b2') < 0) {
        b2 = parseInt($('#border_radiusi').val());
    }
    if ($('#b3').attr('src').indexOf('b3') < 0) {
        b3 = parseInt($('#border_radiusi').val());
    }
    if ($('#b4').attr('src').indexOf('b4') < 0) {
        b4 = parseInt($('#border_radiusi').val());
    }
    $('.active').css('border-radius', b1 + 'px ' + b2 + 'px ' + b3 + 'px ' + b4 + 'px');
    saveStep(); ////console.log('saveStep - ' + 189);
}

function woc(parametr, el, option) {
    if ($(el).attr('class').indexOf('disabled') > -1) {
        return;
    }
    $('.name-of-param').css('background-color', 'rgba(0,0,0,0)');
    switch (parametr) {
        case 'animate':

            break;
        case 'text':
            fontTab('');
            break;
        case 'button':

            break;
        case 'background':
            backgroundTab('');
            break;
        case 'borders':
            borderTab('');
            break;
        case 'shadow':
            effectTab('');
            break;
        case 'url':

            break;
        case 'param':

            break;
        case 'integration':

            break;
        case 'layer':
            reload_right_panel();
            break;
    }
    if (option == 'right_panel') {
        $('.option_el').hide();
        $('.option_el>.woc_panel').show();
        $('.option_el>.woc_content').show();
        $('.woc').css('background', 'none');
        $('#woc-' + parametr).show();
        $('#woc-' + parametr).css('left', 'auto');
        var elTop = $(el).offset().top - 70;
        var windowHeight = $(window).height();
        var elHeight = $('#woc-' + parametr).height();
        if (elHeight + elTop > windowHeight - 85) {
            if (elHeight < windowHeight - 85) {
                elTop = windowHeight - elHeight - 85;
            } else {
                elTop = 0;
            }
        }
        $('#woc-' + parametr).css('top', elTop);
        $('#woc-' + parametr).css('right', 42);
        $('#woc-' + parametr).css('position', 'absolute');
        $(el).css('background-color', '#212121');
    } else {
        $(el).css('background-color', 'rgb(45, 45, 45)');
        $('.woc').css('background-color', 'rgba(0, 0, 0,0)');
        $('.woc_panel').hide();
        $('.woc_content').show();
        $('.option_el').hide();
        if ($('#right_panel').width() > 200) {
            hide_right_menu();
        }
        var blockId = '';

        switch (parametr) {
            case 'animate':
                blockId = 'animation-option';
                break;
            case 'text':
                blockId = 'text-options';
                break;
            case 'button':
                blockId = 'button-option';
                break;
            case 'background':
                blockId = 'background-option';
                break;
            case 'borders':
                blockId = 'border-option';
                break;
            case 'shadow':
                blockId = 'effect-option';
                break;
            case 'url':
                blockId = 'url-option';
                break;
            case 'size':
                blockId = 'size-option';
                break;
        }
        var left = $(el).offset().left;
        var top = $(el).offset().top + $(el).height() + 9;
        var zIndex = parseInt($('#option_panel').css('z-index'));
        $('#woc-' + parametr).css('position', 'fixed');
        $('#woc-' + parametr).css('z-index', (zIndex + 1));
        $('#woc-' + parametr).css('display', 'block');
        $('#woc-' + parametr).css('left', left);
        $('#woc-' + parametr).css('top', top);
    }



}

function chain(parametr) {
    if (parametr == 'apart') {
        $('#padding-all').hide();
        $('#padding-apart').show();
        $('#padding_l').val($('#padding_a').val());
        $('#padding_r').val($('#padding_a').val());
        $('#padding_t').val($('#padding_a').val());
        $('#padding_b').val($('#padding_a').val());
        $('.active').css('padding', $('#padding_t').val() + 'px ' + $('#padding_r').val() + 'px ' + $('#padding_b').val() + 'px ' + $('#padding_l').val() + 'px');
    } else {
        $('#padding-all').show();
        $('#padding-apart').hide();
        $('#padding_a').val($('#padding_l').val());
        $('.active').css('padding', $('#padding_a').val() + 'px');
    }
    saveStep();
}

function fill(parametr) {
    switch (parametr) {
        case 'ordinary':
            $('.active').css('background', 'rgba(0,0,0,0)');
            $('#panel__background').css('background-color', 'rgba(0,0,0,0)');
            $('#background_color_ordinary').show();
            $('#panel_background_color_ordinary').show();
            $('#background_color_gradient').hide();
            $('#panel_background_color_gradient').hide();
            $('.active').css('background-color', $('#edit_background_color_grad1>div>div>div').css('background-color'));
            $('#edit_text_background_color>div>div').css('background-color', $('#edit_background_color_gradient1>div>div').css('background-color'));
            $('#panel_edit_text_background_color>div>div').css('background-color', $('#edit_background_color_gradient1>div>div').css('background-color'));
            $('#checkbox_gradient>input').prop('checked', false);
            $('#panel_checkbox_gradient>input').prop('checked', false);
            $('#checkbox_ordinary>input').prop('checked', true);
            $('#panel_checkbox_ordinary>input').prop('checked', true);
            clearInterval(interval);
            break;
        case 'gradient':
            $('#background_color_ordinary').hide();
            $('#panel_background_color_ordinary').hide();
            $('#background_color_gradient').show();
            $('#panel_background_color_gradient').show();
            $('#edit_background_color_gradient1>div>div').css('background-color', $('#edit_text_background_color>div>div').css('background-color'));
            $('#panel_edit_background_color_gradient1>div>div').css('background-color', $('#edit_text_background_color>div>div').css('background-color'));
            $('#edit_background_color_gradient2>div>div').css('background-color', 'rgba(0,0,0,1)');
            $('#panel_edit_background_color_gradient2>div>div').css('background-color', 'rgba(0,0,0,1)');
            $('.active').css('background', 'linear-gradient(' + $('#edit_text_background_color>div>div').css('background-color') + ', ' + 'rgba(0,0,0,1))');
            $('#panel__background').css('background', 'linear-gradient(' + $('#edit_text_background_color>div>div').css('background-color') + ', ' + 'rgba(0,0,0,1))');
            $('#checkbox_ordinary>input').prop('checked', false);
            $('#panel_checkbox_ordinary>input').prop('checked', false);
            $('#checkbox_gradient>input').prop('checked', true);
            $('#panel_checkbox_gradient>input').prop('checked', true);
            clearInterval(interval);
            break;
        default:
            break;
    }

    saveStep();
}

function gradient_position(parametr) {
    $('.g_position').css('border', '0.5px solid rgba(0, 0, 0, 0)');
    $('#' + parametr).parent().css('border', '0.5px solid rgb(66, 139, 202)');
    switch (parametr) {
        case 'b_tl':
            $('.active').css('background', 'linear-gradient(to bottom right, ' + $('#edit_background_color_grad1>div>div>div').css('background-color') + ', ' + $('#edit_background_color_grad2>div>div>div').css('background-color') + ')');
            break;
        case 'b_t':
            $('.active').css('background', 'linear-gradient(to bottom, ' + $('#edit_background_color_grad1>div>div>div').css('background-color') + ', ' + $('#edit_background_color_grad2>div>div>div').css('background-color') + ')');
            break;
        case 'b_tr':
            $('.active').css('background', 'linear-gradient(to bottom left, ' + $('#edit_background_color_grad1>div>div>div').css('background-color') + ', ' + $('#edit_background_color_grad2>div>div>div').css('background-color') + ')');
            break;
        case 'b_l':
            $('.active').css('background', 'linear-gradient(to right, ' + $('#edit_background_color_grad1>div>div>div').css('background-color') + ', ' + $('#edit_background_color_grad2>div>div>div').css('background-color') + ')');
            break;
        case 'b_c':
            $('.active').css('background', 'radial-gradient(' + $('#edit_background_color_grad1>div>div>div').css('background-color') + ', ' + $('#edit_background_color_grad2>div>div>div').css('background-color') + ')');
            break;
        case 'b_r':
            $('.active').css('background', 'linear-gradient(to left, ' + $('#edit_background_color_grad1>div>div>div').css('background-color') + ', ' + $('#edit_background_color_grad2>div>div>div').css('background-color') + ')');
            break;
        case 'b_bl':
            $('.active').css('background', 'linear-gradient(to top right, ' + $('#edit_background_color_grad1>div>div>div').css('background-color') + ', ' + $('#edit_background_color_grad2>div>div>div').css('background-color') + ')');
            break;
        case 'b_b':
            $('.active').css('background', 'linear-gradient(to top, ' + $('#edit_background_color_grad1>div>div>div').css('background-color') + ', ' + $('#edit_background_color_grad2>div>div>div').css('background-color') + ')');
            break;
        case 'b_br':
            $('.active').css('background', 'linear-gradient(to top left, ' + $('#edit_background_color_grad1>div>div>div').css('background-color') + ', ' + $('#edit_background_color_grad2>div>div>div').css('background-color') + ')');
            break;
        default:
            break;
    }
    saveStep();
}

function edit_text_background_color_grad1() {
    //console.log('редактируем 1 цвет фона градиента');
    clearInterval(interval);
    interval = setInterval(function () {
        $('#panel_edit_background_color_grad1>div>div>div').css('background-color', $('#edit_background_color_grad1>div>div>div').css('background-color'));
        try {
            if ($('.active').css('background').indexOf('linear') >= 0) {
                var param = $('.active').css('background').replace('gradient(', '$').split('$')[1];
                var position = param.split(',')[0];
                var color1 = $('#edit_background_color_grad1>div>div>div').css('background-color');
                var color2 = param.replace('), ', '$').split('$')[1].replace('))', '$').split('$')[0] + ')';
                switch (position) {
                    case 'to right bottom':
                        $('.active').css('background', 'linear-gradient(' + position + ', ' + color1 + ', ' + color2 + ')');
                        break;
                    case 'to left bottom':
                        $('.active').css('background', 'linear-gradient(' + position + ', ' + color1 + ', ' + color2 + ')');
                        break;
                    case 'to right':
                        $('.active').css('background', 'linear-gradient(' + position + ', ' + color1 + ', ' + color2 + ')');
                        break;
                    case 'to left':
                        $('.active').css('background', 'linear-gradient(' + position + ', ' + color1 + ', ' + color2 + ')');
                        break;
                    case 'to right top':
                        $('.active').css('background', 'linear-gradient(' + position + ', ' + color1 + ', ' + color2 + ')');
                        break;
                    case 'to top':
                        $('.active').css('background', 'linear-gradient(' + position + ', ' + color1 + ', ' + color2 + ')');
                        break;
                    case 'to left top':
                        $('.active').css('background', 'linear-gradient(' + position + ', ' + color1 + ', ' + color2 + ')');
                        break;
                    default:
                        $('.active').css('background', 'linear-gradient(' + color1 + ', ' + color2 + ')');
                        break;
                }
            } else {
                var colors1 = $('#edit_background_color_grad1>div>div>div').css('background-color');
                $('.active').css('background', colors1);
            }
        } catch (ex) {
            var colors2 = $('#edit_background_color_grad1>div>div>div').css('background-color');
            $('.active').css('background', colors2);
        }
        if ($('.active').attr('type') == 'button' || $('.active').attr('type') == 'btn' || $('.active').attr('type') == 'submit') { saveButtonOption(); }
    }, 75);
    saveStep();
}

function panel_edit_text_background_color_grad1() {
    //console.log('редактируем 1 цвет фона градиента');
    clearInterval(interval);
    interval = setInterval(function () {
        $('#edit_background_color_grad1>div>div>div').css('background-color', $('#panel_edit_background_color_grad1>div>div>div').css('background-color'));
        try {
            if ($('.active').css('background').indexOf('linear') >= 0) {
                var param = $('.active').css('background').replace('gradient(', '$').split('$')[1];
                var position = param.split(',')[0];
                var color1 = $('#edit_background_color_grad1>div>div>div').css('background-color');
                var color2 = param.replace('), ', '$').split('$')[1].replace('))', '$').split('$')[0] + ')';
                switch (position) {
                    case 'to right bottom':
                        $('.active').css('background', 'linear-gradient(' + position + ', ' + color1 + ', ' + color2 + ')');
                        break;
                    case 'to left bottom':
                        $('.active').css('background', 'linear-gradient(' + position + ', ' + color1 + ', ' + color2 + ')');
                        break;
                    case 'to right':
                        $('.active').css('background', 'linear-gradient(' + position + ', ' + color1 + ', ' + color2 + ')');
                        break;
                    case 'to left':
                        $('.active').css('background', 'linear-gradient(' + position + ', ' + color1 + ', ' + color2 + ')');
                        break;
                    case 'to right top':
                        $('.active').css('background', 'linear-gradient(' + position + ', ' + color1 + ', ' + color2 + ')');
                        break;
                    case 'to top':
                        $('.active').css('background', 'linear-gradient(' + position + ', ' + color1 + ', ' + color2 + ')');
                        break;
                    case 'to left top':
                        $('.active').css('background', 'linear-gradient(' + position + ', ' + color1 + ', ' + color2 + ')');
                        break;
                    default:
                        $('.active').css('background', 'linear-gradient(' + color1 + ', ' + color2 + ')');
                        break;
                }
            } else {
                var colors1 = $('#edit_background_color_grad1>div>div>div').css('background-color');
                $('.active').css('background', colors1);
            }
        } catch (ex) {
            var colors2 = $('#edit_background_color_grad1>div>div>div').css('background-color');
            $('.active').css('background', colors2);
        }
        if ($('.active').attr('type') == 'button' || $('.active').attr('type') == 'btn' || $('.active').attr('type') == 'submit') { saveButtonOption(); }
    }, 75);
    saveStep();
}

//function backgroundTypeDefinition(style) {
//    if (style != undefined && style.indexOf('gradient') >= 0) {
//        $('#checkbox_gradient>input').prop('checked', true);
//        $('#checkbox_ordinary>input').prop('checked', false);
//        $('#background_color_gradient').show();
//        $('#background_color_ordinary').hide();
//        $('#section_backgroung_position').hide();
//        $('#section_backgroung').hide();
//        if (style.indexOf('linear') >= 0) {
//            var param = style.replace('gradient(', '$').split('$')[1];
//            var position = param.split(',')[0];
//            var color1 = 'rgb' + param.replace('),', '$').split('$')[0].replace('rgb', '$').split('$')[1] + ')';
//            var color2 = param.replace('), ', '$').split('$')[1].replace('))', '$').split('$')[0] + ')';
//            $('#edit_background_color_grad1>div>div>div').css('background-color', color1);
//            $('#edit_background_color_grad2>div>div>div').css('background-color', color2);
//            $('.g_position').css('border', '0.5px solid rgba(0, 0, 0, 0)');
//            switch (position) {
//                case 'to right bottom':
//                    $('#b_tl').parent().css('border', '0.5px solid rgb(66, 139, 202)');
//                    break;
//                case 'to left bottom':
//                    $('#b_tr').parent().css('border', '0.5px solid rgb(66, 139, 202)');
//                    break;
//                case 'to right':
//                    $('#b_l').parent().css('border', '0.5px solid rgb(66, 139, 202)');
//                    break;
//                case 'to left':
//                    $('#b_r').parent().css('border', '0.5px solid rgb(66, 139, 202)');
//                    break;
//                case 'to right top':
//                    $('#b_bl').parent().css('border', '0.5px solid rgb(66, 139, 202)');
//                    break;
//                case 'to top':
//                    $('#b_b').parent().css('border', '0.5px solid rgb(66, 139, 202)');
//                    break;
//                case 'to left top':
//                    $('#b_br').parent().css('border', '0.5px solid rgb(66, 139, 202)');
//                    break;
//                default:
//                    $('#b_t').parent().css('border', '0.5px solid rgb(66, 139, 202)');
//                    break;
//            }
//        } else {
//            try {
//                var params = style.replace('radial-gradient(', '$').split('$')[1].replace('))', '$').split('$')[0] + ')';
//                var colors1 = params.replace('), ', '$').split('$')[0] + ')';
//                var colors2 = params.replace('), ', '$').split('$')[1];
//                $('#edit_background_color_grad1>div>div>div').css('background-color', colors1);
//                $('#edit_background_color_grad2>div>div>div').css('background-color', colors2);
//                $('.g_position').css('border', '0.5px solid rgba(0, 0, 0, 0)');
//                $('#b_c').parent().css('border', '0.5px solid rgb(66, 139, 202)');
//            } catch (ex) {
//                console.log('Error: ' + ex);
//            }
//        }
//    } else if ($('.active').css('background-image') != undefined && $('.active').css('background-image').indexOf('url') >= 0) {
//        $('#background_image_section_del').show();
//        $('#section_backgroung_position').show();
//        $('#section_backgroung').show();
//        $('#checkbox_gradient>input').prop('checked', false);
//        $('#checkbox_ordinary>input').prop('checked', true);
//        $('#background_color_gradient').hide();
//        $('#background_color_ordinary').show();
//    } else {
//        $('#background_image_section_del').hide();
//        $('#section_backgroung_position').hide();
//        $('#section_backgroung').hide();
//        $('#checkbox_gradient>input').prop('checked', false);
//        $('#checkbox_ordinary>input').prop('checked', true);
//        $('#background_color_gradient').hide();
//        $('#background_color_ordinary').show();
//    }
//}

function edit_text_background_color_grad2() {
    //console.log('редактируем 2 цвет фона градиента');
    clearInterval(interval);
    interval = setInterval(function () {
        try {
            $('#panel_edit_background_color_grad2>div>div>div').css('background-color', $('#edit_background_color_grad2>div>div>div').css('background-color'));
            if ($('.active').css('background').indexOf('linear') >= 0) {
                var position = $('.active').css('background-position');
                var temp1 = $('.active').css('background').replace('linear-gradient(', '$').split('$');
                var positions = temp1[1].split(',')[0];
                var color1 = $('#edit_background_color_grad1>div>div>div').css('background-color');
                var color2 = $('#edit_background_color_grad2>div>div>div').css('background-color');
                switch (positions) {
                    case 'to right bottom':
                        $('.active').css('background', 'linear-gradient(to right bottom, ' + color1 + ', ' + color2 + ')');
                        break;
                    case '100% 100%':
                        $('.active').css('background', 'linear-gradient(to right bottom, ' + color1 + ', ' + color2 + ')');
                        break;
                    case 'to left bottom':
                        $('.active').css('background', 'linear-gradient(to left bottom, ' + color1 + ', ' + color2 + ')');
                        break;
                    case '0% 100%':
                        $('.active').css('background', 'linear-gradient(to left bottom, ' + color1 + ', ' + color2 + ')');
                        break;
                    case 'to right':
                        $('.active').css('background', 'linear-gradient(to right, ' + color1 + ', ' + color2 + ')');
                        break;
                    case '100% 50%':
                        $('.active').css('background', 'linear-gradient(to right, ' + color1 + ', ' + color2 + ')');
                        break;
                    case 'to left':
                        $('.active').css('background', 'linear-gradient(to left, ' + color1 + ', ' + color2 + ')');
                        break;
                    case '0% 50%':
                        $('.active').css('background', 'linear-gradient(to left, ' + color1 + ', ' + color2 + ')');
                        break;
                    case 'to right top':
                        $('.active').css('background', 'linear-gradient(to right top, ' + color1 + ', ' + color2 + ')');
                        break;
                    case '100% 0%':
                        $('.active').css('background', 'linear-gradient(to right top, ' + color1 + ', ' + color2 + ')');
                        break;
                    case 'to top':
                        $('.active').css('background', 'linear-gradient(to top, ' + color1 + ', ' + color2 + ')');
                        break;
                    case '50% 0%':
                        $('.active').css('background', 'linear-gradient(to top, ' + color1 + ', ' + color2 + ')');
                        break;
                    case 'to left top':
                        $('.active').css('background', 'linear-gradient(to left top, ' + color1 + ', ' + color2 + ')');
                        break;
                    case '0% 0%':
                        $('.active').css('background', 'linear-gradient(to left top, ' + color1 + ', ' + color2 + ')');
                        break;
                    default:
                        $('.active').css('background', 'linear-gradient(' + color1 + ', ' + color2 + ')');
                        break;
                }
            } else {
                var colors2 = $('#edit_background_color_grad2>div>div>div').css('background-color');
                $('.active').css('background', colors2);
            }
        } catch (ex) {
            var colors2 = $('#edit_background_color_grad2>div>div>div').css('background-color');
            $('.active').css('background', colors2);
        }
        if ($('.active').attr('type') == 'button' || $('.active').attr('type') == 'btn' || $('.active').attr('type') == 'submit') { saveButtonOption(); }
    }, 75);
    saveStep();
}

function panel_edit_text_background_color_grad2() {
    //console.log('редактируем 2 цвет фона градиента');
    clearInterval(interval);
    interval = setInterval(function () {
        $('#edit_background_color_grad2>div>div>div').css('background-color', $('#panel_edit_background_color_grad2>div>div>div').css('background-color'));
        try {
            if ($('.active').css('background').indexOf('linear') >= 0) {
                var position = $('.active').css('background-position');
                var temp1 = $('.active').css('background').replace('linear-gradient(', '$').split('$');
                var positions = temp1[1].split(',')[0];
                var color1 = $('#edit_background_color_grad1>div>div>div').css('background-color');
                var color2 = $('#edit_background_color_grad2>div>div>div').css('background-color');
                switch (positions) {
                    case 'to right bottom':
                        $('.active').css('background', 'linear-gradient(to right bottom, ' + color1 + ', ' + color2 + ')');
                        break;
                    case '100% 100%':
                        $('.active').css('background', 'linear-gradient(to right bottom, ' + color1 + ', ' + color2 + ')');
                        break;
                    case 'to left bottom':
                        $('.active').css('background', 'linear-gradient(to left bottom, ' + color1 + ', ' + color2 + ')');
                        break;
                    case '0% 100%':
                        $('.active').css('background', 'linear-gradient(to left bottom, ' + color1 + ', ' + color2 + ')');
                        break;
                    case 'to right':
                        $('.active').css('background', 'linear-gradient(to right, ' + color1 + ', ' + color2 + ')');
                        break;
                    case '100% 50%':
                        $('.active').css('background', 'linear-gradient(to right, ' + color1 + ', ' + color2 + ')');
                        break;
                    case 'to left':
                        $('.active').css('background', 'linear-gradient(to left, ' + color1 + ', ' + color2 + ')');
                        break;
                    case '0% 50%':
                        $('.active').css('background', 'linear-gradient(to left, ' + color1 + ', ' + color2 + ')');
                        break;
                    case 'to right top':
                        $('.active').css('background', 'linear-gradient(to right top, ' + color1 + ', ' + color2 + ')');
                        break;
                    case '100% 0%':
                        $('.active').css('background', 'linear-gradient(to right top, ' + color1 + ', ' + color2 + ')');
                        break;
                    case 'to top':
                        $('.active').css('background', 'linear-gradient(to top, ' + color1 + ', ' + color2 + ')');
                        break;
                    case '50% 0%':
                        $('.active').css('background', 'linear-gradient(to top, ' + color1 + ', ' + color2 + ')');
                        break;
                    case 'to left top':
                        $('.active').css('background', 'linear-gradient(to left top, ' + color1 + ', ' + color2 + ')');
                        break;
                    case '0% 0%':
                        $('.active').css('background', 'linear-gradient(to left top, ' + color1 + ', ' + color2 + ')');
                        break;
                    default:
                        $('.active').css('background', 'linear-gradient(' + color1 + ', ' + color2 + ')');
                        break;
                }
            } else {
                var colors2 = $('#edit_background_color_grad2>div>div>div').css('background-color');
                $('.active').css('background', colors2);
            }
        } catch (ex) {
            var colors2 = $('#edit_background_color_grad2>div>div>div').css('background-color');
            $('.active').css('background', colors2);
        }
        if ($('.active').attr('type') == 'button' || $('.active').attr('type') == 'btn' || $('.active').attr('type') == 'submit') { saveButtonOption(); }
    }, 75);
    saveStep();
}

function clearColor() {
    //console.log('очищаем цвет фона');
    $('#edit_background_color_text>div>div>div').css('background-color', 'rgba(255,255,255,0)');
    $('#panel_edit_background_color_text>div>div>div').css('background-color', 'rgba(255,255,255,0)');
    $('.active').css('background-color', 'rgba(255,255,255,0)');
    saveStep();
}

function clearGradient() {
    //console.log('очищаем градиент');
    $('#checkbox_ordinary').children().first().click();
    $('#panel_checkbox_ordinary').children().first().click();
}

function clearBorderColor() {
    //console.log('очищаем цвет рамки');
    $('.active').css('border-color', 'rgba(255, 255, 255, 0)');
    groupProperty('border-color', 'rgba(255, 255, 255, 0)');
    $('#border_color>div>div>div').css('background-color', 'rgba(255, 255, 255, 0)');
    saveStep();
}

function border_to_chain() {
    var style = $('#country_id_2 option:selected').val();
    $('.active').css('border', $('#b_top').val() + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
    $('#thickness_border').val($('#b_top').val());
    $('#border_apart').show();
    $('#border_chain').hide();
    saveStep();
}

function border_to_apart() {
    $('#b_top').val($('#thickness_border').val());
    $('#b_left').val($('#thickness_border').val());
    $('#b_right').val($('#thickness_border').val());
    $('#b_bottom').val($('#thickness_border').val());
    var style = $('#country_id_2 option:selected').val();
    $('.active').css('border-top', $('#b_top').val() + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
    $('.active').css('border-left', $('#b_left').val() + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
    $('.active').css('border-right', $('#b_right').val() + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
    $('.active').css('border-bottom', $('#b_bottom').val() + 'px ' + style + ' ' + $('#border_color>div>div>div').css('background-color'));
    $('#border_apart').hide();
    $('#border_chain').show();
    saveStep();
}

function border_radius_to_chain() {
    $('#borders_radius_chain').show();
    $('#borders_radius_apart').hide();
    $('#border_radiusi').val($('#br_t_l').val());
    $('.active').css('border-radius', $('#border_radiusi').val() + 'px');
    saveStep();
}

function border_radius_to_apart() {
    $('#borders_radius_chain').hide();
    $('#borders_radius_apart').show();
    $('#br_t_l').val($('#border_radiusi').val());
    $('#br_t_r').val($('#border_radiusi').val());
    $('#br_b_l').val($('#border_radiusi').val());
    $('#br_b_r').val($('#border_radiusi').val());
    $('.active').css('border-radius', $('#border_radiusi').val() + 'px');
    saveStep();
}

function clearBlockShadowColor() {
    //console.log('очищаем цвет тени блока');
    $('#block_shadows>div>div>div').css('background-color', 'rgba(255,255,255,0)');
    $('#blurrines_shadow').val('0');
    $('#x_shadow').val('0');
    $('#y_shadow').val('0');
    $('.active').css('box-shadow', 'none');
    saveStep();
}

function clearBlockLightingColor() {
    //console.log('очищаем цвет свечения блока');
    $('#block_lightings>div>div>div').css('background-color', 'rgba(255, 255,255,0)');
    $('#blurrines_lighting').val('0');
    $('#lighting').val('');
    $('.active').css('box-shadow', 'none');
    saveStep();
}

function clearTextLightingColor() {
    //console.log('очищаем цвет свечения текста');
    $('#text_lightings>div>div>div').css('background-color', 'rgba(255, 255,255,0)');
    $('#blurrines_lighting_text').val('0');
    $('.active').css('text-shadow', 'none');
    saveStep();
}


function clearTextShadowColor() {
    //console.log('очищаем цвет тени текста');
    $('#color_shadow>div>div>div').css('background-color', 'rgba(255,255,255,0)');
    $('#blurrines').val('0');
    $('#x').val('0');
    $('#y').val('0');
    $('.active').children().first().css('text-shadow', 'none');
    saveStep();
}

$(function () {

    $("#web_url").on('change', function () {
        $('.active>a').attr('href', $("#web_url").val());
        if ($("#web_url").val() == '') {
            $('.active>a').attr('href', '#');
        }
        $('.active>a').attr('type', 'web');
        $("#web_url").prev().children().first().click();
        $('#list_pages>option').removeAttr('selected');
        $('#list_pages').children().first().attr('selected', 'selected');
        $('#list_anchor>option').removeAttr('selected');
        $('#list_anchor').children().first().attr('selected', 'selected');
        saveStep();
    });

    $("#list_pages").on('change', function () {
        $('.active>a').attr('href', '#' + $("#list_pages").val());
        $('.active>a').attr('type', 'page');
        $("#list_pages").prev().children().first().click();
        $('#list_anchor>option').removeAttr('selected');
        $('#list_liteboxes>option').removeAttr('selected');
        var el = $('#list_anchor').children().first();
        $(el).attr('selected', 'selected');
        var el1 = $('#list_liteboxes').children().first();
        $(el1).attr('selected', 'selected');
        $("#web_url").val('');
        saveStep();
    });

    $("#list_anchor").on('change', function () {
        $('.active>a').attr('href', '#' + $("#list_anchor").val());
        $('#' + $("#list_anchor").val()).attr('data-anchor', '#' + $("#list_anchor").val());
        $('.active>a').attr('type', 'anchor');
        $("#list_anchor").prev().children().first().click();
        $('#list_pages>option').removeAttr('selected');
        $('#list_liteboxes>option').removeAttr('selected');
        var el = $('#list_pages').children().first();
        $(el).attr('selected', 'selected');
        var el1 = $('#list_liteboxes').children().first();
        $(el1).attr('selected', 'selected');
        $("#web_url").val('');
        saveStep();
    });
    $("#list_liteboxes").on('change', function () {
        $('.active>a').attr('href', '#' + $("#list_liteboxes").val());
        $('.active>a').attr('type', 'litebox');
        $("#list_liteboxes").prev().children().first().click();
        $('#list_pages>option').removeAttr('selected');
        $('#list_anchor>option').removeAttr('selected');
        var el = $('#list_pages').children().first();
        $(el).attr('selected', 'selected');
        var el1 = $('#list_anchor').children().first();
        $(el1).attr('selected', 'selected');
        $("#web_url").val('');
        saveStep();
    });
});


function savePagesToHDD() {
    //console.log('сохраняем страницы на жесткий диск');
    if ($('.edit_form').length) {
        $('#page_tabs').find('[rel="Home"]').first().click();
    }
    var css = '';
    var filename = $('#website_info').attr('rel');
    var countPages = $('#pages').children().length;
    var pages = new Array();
    for (var i = 0; i < countPages; i++) {
        pages[i] = new Array();
    }

    $('#tempBlock').remove();
    var tempBlock = document.createElement('div');
    $(tempBlock).attr('id', 'tempBlock');
    $(tempBlock).css('display', 'none');
    $('body').append(tempBlock);
    $('#tempBlock').html($('#pages').html());

    var html = document.createElement('html');
    var head = document.createElement('head');
    var body = document.createElement('body');
    var layout = document.createElement('div');
    $(layout).attr('id', 'pageLayout');
    $(html).append(head);
    $(html).append(body);
    $(layout).append(html);
    $('#tempBlock').append(layout);
    $('#tempBlock>#pageLayout>html>body').html($('#tempBlock>#Layout>.page_content_standard').html());
    $('#tempBlock>#Layout').remove();

    var link1 = document.createElement('link');
    $(link1).attr('href', 'css/site.css');
    $(link1).attr('type', 'text/css');
    $(link1).attr('rel', 'stylesheet');
    $('#tempBlock>#pageLayout>html>head').append(link1);

    var link2 = document.createElement('link');
    $(link2).attr('href', 'css/style.css');
    $(link2).attr('type', 'text/css');
    $(link2).attr('rel', 'stylesheet');
    $('#tempBlock>#pageLayout>html>head').append(link2);

    var title = document.createElement('title');
    $(title).text('Home');
    $('#tempBlock>#pageLayout>html>head').append(title);

    var desc = document.createElement('meta');
    $(desc).attr('id', 'desc');
    $(desc).attr('name', 'description');
    $('#tempBlock>#pageLayout>html>head').append(desc);

    var keys = document.createElement('meta');
    $(keys).attr('id', 'keys');
    $(keys).attr('name', 'keywords');
    $('#tempBlock>#pageLayout>html>head').append(keys);

    $('#tempBlock>#' + $('.tab_active').parent().attr('rel')).html($('#content_standard').html());
    $('#tempBlock #additionalBlocks').remove();

    var index = 0;

    var img = findImages($('#tempBlock'));
    var count = 0;
    var ind = 0;
    $('#tempBlock img').each(function () {
        count++;
    });
    $('#tempBlock img').each(function () {

        var img = $(this).attr('src');
        if (img != '' && img != undefined) {
            var elemId = $(this).closest('.ui-widget-content').attr('id');
            $.ajax({
                url: "/Cabinet/Editor/GetImageBase64Data",
                data: {
                    image: img
                },
                async: true,
                typedata: 'json',
                success: function (data) {
                    $('#tempBlock #' + elemId + ' img').attr('src', data);
                    ind++;
                },
                error: function (data) {
                    alert('Все плохо');
                },
                type: "POST"
            });
        }
    });
    clearInterval(interval);
    var interval = setInterval(function () {
        if (count === ind) {
            $('#tempBlock').children().each(function () {
                $(this).children().each(function () {
                    css = findByAttrId(this);
                });
                if ($(this).attr('id') != 'pageLayout' && $(this).attr('id') != undefined) {
                    pages[index] = $(this).attr('id');
                    $('#tempBlock>#pageLayout #sectionContent').html($(this).html());
                    var text = $(this).attr('name');
                    $('#desc').attr('content', $(this).attr('desc'));
                    $('#keys').attr('content', $(this).attr('key'));
                    pages[index + 1] = $('#tempBlock>#pageLayout').html();
                    index = index + 2;
                }
            });
            if (css != undefined) {
                css = css.replace(/\undefined/g, '');
            } else {
                css = '';
            }
            $.ajax({
                url: "/Cabinet/Editor/SaveProject",
                data: {
                    filename: filename,
                    css: css,
                    pages: pages
                },
                async: false,
                typedata: 'json',
                success: function (data) {
                    clearInterval(interval);
                },
                error: function (data) {
                    alert('Все плохо');
                    clearInterval(interval);
                },
                type: "POST"
            });
        }
    }, 100);

}

function findByAttrId(element) {
    //console.log('собираем все свойства CSS для дальнейшего сохранения на диск');
    if ($(element).attr('id') != undefined) {
        var cs = '';
        var id = $(element).attr('id');
        var style = $(element).attr('style');
        cs = '#' + id + '{' + style + '}';
        $('#tempBlock #' + id).children().each(function () {
            cs = cs + findByAttrId(this);
        });
        return cs;
    } else {
        var cs = '';
        $(element).children().each(function () {
            cs = cs + findByAttrId(this);
        });
        return cs;
    }
}

function findImages(element) {
    //console.log('собираем все свойства изображения для дальнейшего сохранения на диск');
    var img = '';
    if ($(element).css('background').indexOf('url(') >= 0) {
        img = $(element).css('background').replace(/'/g, '').replace('url(', '').replace(')', '');
        $.ajax({
            url: "/Cabinet/Editor/GetImageBase64Data",
            data: {
                image: img
            },
            async: false,
            typedata: 'json',
            success: function (data) {
                $(element).css('background', 'url(' + data + ')');
            },
            error: function (data) {
                alert('Все плохо');
            },
            type: "POST"
        });
    }
    if ($(element).css('background-image').indexOf('url(') >= 0) {
        img = $(element).css('background-image').replace(/'/g, '').replace('url(', '').replace(')', '');
        $.ajax({
            url: "/Cabinet/Editor/GetImageBase64Data",
            data: {
                image: img
            },
            async: false,
            typedata: 'json',
            success: function (data) {
                $(element).css('background', 'url(' + data + ')');
            },
            error: function (data) {
                alert('Все плохо');
            },
            type: "POST"
        });
    }
    $(element).children().each(function () {
        img = findImages(this);
    });
    return 'ok!';
}

//удаление элемента
$(document).keydown(function (eventObject) {
    if (eventObject.which === 46 && $("div").is(".active")) {
        if ((parseFloat($('#editTextElement').css('top')) < 1) && (parseFloat($('#load_image').css('top')) < 1)
            && (parseFloat($('#load_icon_litebox').css('top')) < 1) && (parseFloat($('#edit_video').css('top')) < 1)
            && (parseFloat($('#timer_option_litebox').css('top')) < 1) && (!$('#font_s').is(":focus"))
            && (!$('#line_h').is(":focus")) && (!$('#letter_s').is(":focus"))
            && (!$('#text_i').is(":focus")) && (!$('#padding_a').is(":focus"))
            && (!$('#padding_l').is(":focus")) && (!$('#padding_r').is(":focus"))
            && (!$('#padding_t').is(":focus")) && (!$('#padding_b').is(":focus"))
            && (!$('#thickness_border').is(":focus")) && (!$('#b_top').is(":focus"))
            && (!$('#b_bottom').is(":focus")) && (!$('#b_left').is(":focus"))
            && (!$('#b_right').is(":focus")) && (!$('#border_radiusi').is(":focus"))
            && (!$('#br_t_l').is(":focus")) && (!$('#br_t_r').is(":focus"))
            && (!$('#br_b_l').is(":focus")) && (!$('#br_b_r').is(":focus"))
            && (!$('#blurrines').is(":focus")) && (!$('#x').is(":focus"))
            && (!$('#y').is(":focus")) && (!$('#blurrines_shadow').is(":focus"))
            && (!$('#x_shadow').is(":focus")) && (!$('#y_shadow').is(":focus"))
            && (!$('#web_url').is(":focus")) && (!$('#section_width_val').is(":focus"))
            && (!$('#section_height_val').is(":focus")) && (!$('#section_top_val').is(":focus"))
            && (!$('#section_left_val').is(":focus")) && (!$('#content_width_val').is(":focus"))
            && (!$('#header').is(":focus")) && (!$('#description').is(":focus"))
            && (!$('#keywords').is(":focus")) && (!$('#addPage_Name').is(":focus"))
            && (!$('#addPage_Url').is(":focus")) && (!$('.rename_input').is(":focus"))
            && (!$('#textarea_text_element').is(":focus"))) {
            openDelWindow();
        }
    }
});

//редактирование элемента
$(function () {
    $('#icons_gallery i').dblclick(function () {
        getIcon();
    });

    $('#move_block').dblclick(function () {
        editElement();
    });
});

function selUnit(el) {
    var valId = $(el).prev().attr('id');
    var div = document.createElement('div');
    var top = $('#right_panel_content').offset().top;
    var height = $('#right_panel_content').css('height');
    var topEl = $(el).parent().offset().top;
    var tops = -11;
    if (topEl - top < 11) {
        tops = -(topEl - top);
    }
    if (top + height - topEl < 33) {
        tops = -(44 - (top + height - topEl));
    }

    if ($(el).prev().attr('id') == 'panel_font_s') {
        tops = -11;
    }

    if (($(el).prev().attr('id') == 'section_height_val' || $(el).prev().attr('id') == 'panel_section_height_val') && ($('.active').attr('class').indexOf('section') > -1 || $('.active').attr('class').indexOf('containerForm') > -1)) {
        return;
    }
    if (($(el).prev().attr('id') == 'section_width_val' || $(el).prev().attr('id') == 'panel_section_width_val') && ($('.active').attr('class').indexOf('section') >= 0 || $('.active').attr('class').indexOf('containerForm') >= 0) || $(this).prev().attr('id') == 'page_content_height_val') {
        return;
    }

    $(div).attr('style', 'position: absolute; z-index: 1000; width: 18px; height: 36px; top: ' + tops + '; left: -1px;');
    if ($(el).prev().attr('id') == 'width_l_val' || $(el).prev().attr('id') == 'height_l_val') {
        $(div).css('background-color', '#D8D8D8');
    }
    $(div).attr('id', 'unit');
    var px = document.createElement('div');
    $(px).text('px');
    $(px).attr('style', 'text-align: center; height: 18px; width: 18px; ');
    $(px).attr('onclick', 'units(' + valId + ', "px")');
    var persent = document.createElement('div');
    $(persent).text('%');
    $(persent).attr('style', 'text-align: center; height: 18px; width: 18px; ');
    $(persent).attr('onclick', 'units(' + valId + ', "%")');
    div.appendChild(px);
    div.appendChild(persent);
    $(el).append(div);
};

function units(inputId, unit) {
    var oldUnit = $(inputId).next().text();
    $(inputId).find('#unit').hide();
    $(inputId).next().text(unit);
    if ((oldUnit != unit) && (unit == 'px')) {
        var fontSizes = parseFloat($('.active').css('font-size'));
        var paddingss = getStyle($('.active'), 'padding');
        var paddingTops = '';
        var paddingRights = '';
        var paddingBottoms = '';
        var paddingLefts = '';
        switch (paddingss.length) {
            case 1:
                paddingTops = paddingss[0];
                paddingRights = paddingss[0];
                paddingBottoms = paddingss[0];
                paddingLefts = paddingss[0];
                break;
            case 2:
                paddingTops = paddingss[0];
                paddingRights = paddingss[1];
                paddingBottoms = paddingss[0];
                paddingLefts = paddingss[1];
                break;
            case 3:
                paddingTops = paddingss[0];
                paddingRights = paddingss[1];
                paddingBottoms = paddingss[2];
                paddingLefts = paddingss[1];
                break;
            case 4:
                paddingTops = paddingss[0];
                paddingRights = paddingss[1];
                paddingBottoms = paddingss[2];
                paddingLefts = paddingss[3];
                break;
        }
        switch ($(inputId).attr('id')) {
            case 'font_s':
                $(inputId).val(Math.abs(fontSizes));
                $('#panel_font_s').val(Math.abs(fontSizes));
                $('.active').css('font-size', fontSizes + 'px');
                break;
            case 'panel_font_s':
                $(inputId).val(Math.abs(fontSizes));
                $('#font_s').val(Math.abs(fontSizes));
                $('.active').css('font-size', fontSizes + 'px');
                break;
            case 'line_h':
                var lineHeights = parseFloat($('.active').css('line-height'));
                $(inputId).val(lineHeights);
                $('.active').css('line-height', $(inputId).val() + 'px');
                correctSizeTextElement();
                break;
            case 'text_i':
                var textIndent = parseFloat($('.active').css('text-indent'));
                $(inputId).val(textIndent);
                $('.active').css('text-indent', $(inputId).val() + 'px');
                correctSizeTextElement()
                break;
            case 'padding_a':
                var paddingA = parseFloat($('.active').css('padding'));
                $(inputId).val(paddingA);
                $('.active').css('padding', $(inputId).val() + 'px');
                correctSizeTextElement()
                break;
            case 'padding_l':
                var left = Math.abs(parseFloat($('.active').css('padding-left')));
                paddingLefts = left + 'px';
                $(inputId).val(left);
                $('.active').css('padding', paddingTops + ' ' + paddingRights + ' ' + paddingBottoms + ' ' + paddingLefts);
                correctSizeTextElement()
                break;
            case 'padding_r':
                var right = Math.abs(parseFloat($('.active').css('padding-left')));
                paddingRights = right + 'px';
                $(inputId).val(right);
                $('.active').css('padding', paddingTops + ' ' + paddingRights + ' ' + paddingBottoms + ' ' + paddingLefts);
                correctSizeTextElement()
                break;
            case 'padding_t':
                var top = Math.abs(parseFloat($('.active').css('padding-left')));
                paddingTops = top + 'px';
                $(inputId).val(top);
                $('.active').css('padding', paddingTops + ' ' + paddingRights + ' ' + paddingBottoms + ' ' + paddingLefts);
                correctSizeTextElement()
                break;
            case 'padding_b':
                var bottom = Math.abs(parseFloat($('.active').css('padding-left')));
                paddingBottoms = bottom + 'px';
                $(inputId).val(bottom);
                $('.active').css('padding', paddingTops + ' ' + paddingRights + ' ' + paddingBottoms + ' ' + paddingLefts);
                correctSizeTextElement()
                break;
            case 'section_width_val':
                var width = parseInt($('.active').css('width'));
                if ($('.active').parent().attr('class').indexOf('containerInput') >= 0) {
                    $('.active').parent().css('width', width + 'px');
                } else {
                    $('.active').css('width', width + 'px');
                }
                $(inputId).val(width);
                $('#panel_section_width_val').val(height);
                break;
            case 'panel_section_width_val':
                var width = parseInt($('.active').css('width'));
                if ($('.active').parent().attr('class').indexOf('containerInput') >= 0) {
                    $('.active').parent().css('width', width + 'px');
                } else {
                    $('.active').css('width', width + 'px');
                }
                $(inputId).val(width);
                $('#section_width_val').val(height);
                break;
            case 'section_height_val':
                var height = parseInt($('.active').css('height'));
                $(inputId).val(height);
                $('#panel_section_height_val').val(height);
                $('.active').css('height', height + 'px');
                break;
            case 'panel_section_height_val':
                var height = parseInt($('.active').css('height'));
                $(inputId).val(height);
                $('#section_height_val').val(height);
                $('.active').css('height', height + 'px');
                break;
            case 'section_top_val':
                var tops = parseInt($('.active').css('top'));
                $(inputId).val(tops);
                $('.active').css('top', tops + 'px');
                break;
            case 'section_left_val':
                var lefts = parseInt($('.active').css('left'));
                $(inputId).val(lefts);
                $('.active').css('left', lefts + 'px');
                break;
            case 'page_content_width_val':
                $(inputId).val(960);
                $('.active').css('width', '960px');
                $('#content_width_val').val(960);
                $('#content_width_val').next().text('px');
                break;
            case 'content_width_val':
                var widthS = getStyle($('#content_standard>.section').first(), 'width');
                var widthCc = getStyle($('#content_standard>.section').first().find('.container_section').first(), 'width');
                var w = '';
                if (widthS.replace(' ', '') == '100%') {
                    if (widthCc.indexOf('%') >= 0) {
                        w = '%';
                    }
                }
                if (widthCc.replace(' ', '') == '100%') {
                    if (widthS.indexOf('%') >= 0) {
                        w = '%';
                    }
                }
                if (w == '%') {
                    var widthWww = Math.abs($('#content_standard').width() * (parseFloat($('#content_width_val').val()) / 100));
                    $('#content_width_val').val(widthWww);
                    $('#content_width_val').next().text('px');
                    $('#content_standard .section').each(function () {
                        if ($(this).attr('class').indexOf('sectionContent') < 0) {
                            var width = getStyle($(this), 'width');
                            var itemId = $(this).attr('id');
                            var widthC = getStyle($('#' + itemId + '>.container_section'), 'width');
                            if (width.replace(' ', '') == '100%') {
                                $('#' + itemId + '>.container_section').css('width', Math.abs($('#content_width_val').val()) + 'px');
                            }
                            if (widthC.replace(' ', '') == '100%') {
                                $(this).css('width', Math.abs($('#content_width_val').val()) + 'px');
                            }
                        }
                    });
                    $('#pages .section').each(function () {
                        if ($(this).attr('class').indexOf('sectionContent ') < 0) {
                            var width = getStyle($(this), 'width');
                            var itemId = $(this).attr('id');
                            var widthC = getStyle($('#' + itemId + '>.container_section'), 'width');
                            if (width.replace(' ', '') == '100%') {
                                $('#' + itemId + '>.container_section').css('width', Math.abs($('#content_width_val').val()) + 'px');
                            }
                            if (widthC.replace(' ', '') == '100%') {
                                $(this).css('width', Math.abs($('#content_width_val').val()) + 'px');
                            }
                        }
                    });
                }
                break;
        }
    }
    if ((oldUnit != unit) && (unit == '%')) {
        var parentFontSize = parseFloat($('.active').parent().css('font-size'));
        var fontSize = parseFloat($('.active').css('font-size'));
        var parentWidth = $('.active').width();

        var paddings = getStyle($('.active'), 'padding');
        var paddingTop = '';
        var paddingRight = '';
        var paddingBottom = '';
        var paddingLeft = '';
        switch (paddings.length) {
            case 2:
                paddingTop = paddings[1];
                paddingRight = paddings[1];
                paddingBottom = paddings[1];
                paddingLeft = paddings[1];
                break;
            case 3:
                paddingTop = paddings[1];
                paddingRight = paddings[2];
                paddingBottom = paddings[1];
                paddingLeft = paddings[2];
                break;
            case 4:
                paddingTop = paddings[1];
                paddingRight = paddings[2];
                paddingBottom = paddings[3];
                paddingLeft = paddings[2];
                break;
            case 5:
                paddingTop = paddings[1];
                paddingRight = paddings[2];
                paddingBottom = paddings[3];
                paddingLeft = paddings[4];
                break;
        }

        switch ($(inputId).attr('id')) {
            case 'font_s':
                $(inputId).val(Math.abs((fontSize / parentFontSize) * 100).toFixed(1));
                $('#panel_font_s').val(Math.abs((fontSize / parentFontSize) * 100).toFixed(1));
                $('.active').css('font-size', $(inputId).val() + '%');
                break;
            case 'panel_font_s':
                $(inputId).val(Math.abs((fontSize / parentFontSize) * 100).toFixed(1));
                $('#font_s').val(Math.abs((fontSize / parentFontSize) * 100).toFixed(1));
                $('.active').css('font-size', $(inputId).val() + '%');
                break;
            case 'line_h':
                var lineHeight = parseFloat($('.active').css('line-height')).toFixed(1);
                $(inputId).val(((lineHeight / fontSize) * 100).toFixed(1));
                $('.active').css('line-height', $(inputId).val() + '%');
                correctSizeTextElement();
                break;
            case 'text_i':
                var textIndents = parseFloat($('.active').css('text-indent')).toFixed(1);
                $(inputId).val(((textIndents / parentWidth) * 100).toFixed(1));
                $('.active').css('text-indent', $(inputId).val() + '%');
                correctSizeTextElement()
                break;
            case 'padding_a':
                var paddingAs = parseFloat($('.active').css('padding')).toFixed(1);
                $(inputId).val(((paddingAs / parentWidth) * 100).toFixed(1));
                $('.active').css('padding', $(inputId).val() + '%');
                correctSizeTextElement()
                break;
            case 'padding_l':
                paddingLeft = Math.abs((parseFloat(paddingLeft) / parentWidth) * 100).toFixed(1) + '%';
                $(inputId).val(Math.abs((parseFloat(paddingLeft) / parentWidth) * 100).toFixed(1));
                $('.active').css('padding', paddingTop + ' ' + paddingRight + ' ' + paddingBottom + ' ' + paddingLeft);
                correctSizeTextElement()
                break;
            case 'padding_r':
                paddingRight = Math.abs((parseFloat(paddingRight) / parentWidth) * 100).toFixed(1) + '%';
                $(inputId).val(Math.abs((parseFloat(paddingRight) / parentWidth) * 100).toFixed(1));
                $('.active').css('padding', paddingTop + ' ' + paddingRight + ' ' + paddingBottom + ' ' + paddingLeft);
                correctSizeTextElement()
                break;
            case 'padding_t':
                paddingTop = Math.abs((parseFloat(paddingTop) / parentWidth) * 100).toFixed(1) + '%';
                $(inputId).val(Math.abs((parseFloat(paddingTop) / parentWidth) * 100).toFixed(1));
                $('.active').css('padding', paddingTop + ' ' + paddingRight + ' ' + paddingBottom + ' ' + paddingLeft);
                correctSizeTextElement()
                break;
            case 'padding_b':
                paddingBottom = Math.abs((parseFloat(paddingBottom) / parentWidth) * 100).toFixed(1) + '%';
                $(inputId).val(Math.abs((parseFloat(paddingBottom) / parentWidth) * 100).toFixed(1));
                $('.active').css('padding', paddingTop + ' ' + paddingRight + ' ' + paddingBottom + ' ' + paddingLeft);
                correctSizeTextElement()
                break;
            case 'section_width_val':
                var parentW = $('.active').parent().width();
                var widths = parseFloat($('.active').css('width'));
                $(inputId).val(Math.abs((widths / parentW) * 100).toFixed(1));
                $('#panel_section_width_val').val(Math.abs((widths / parentW) * 100).toFixed(1));
                if ($('.active').parent().attr('class').indexOf('containerInput') >= 0) {
                    $('.active').parent().css('width', Math.abs((widths / parentW) * 100).toFixed(1) + '%');
                } else {
                    $('.active').css('width', Math.abs((widths / parentW) * 100).toFixed(1) + '%');
                }
                break;
            case 'panel_section_width_val':
                var parentW = $('.active').parent().width();
                var widths = parseFloat($('.active').css('width'));
                $(inputId).val(Math.abs((widths / parentW) * 100).toFixed(1));
                $('#section_width_val').val(Math.abs((widths / parentW) * 100).toFixed(1));
                if ($('.active').parent().attr('class').indexOf('containerInput') >= 0) {
                    $('.active').parent().css('width', Math.abs((widths / parentW) * 100).toFixed(1) + '%');
                } else {
                    $('.active').css('width', Math.abs((widths / parentW) * 100).toFixed(1) + '%');
                }
                break;
            case 'section_height_val':
                var parentH = $('.active').parent().height();
                var heightss = parseFloat($('.active').css('height'));
                $(inputId).val(Math.abs((heightss / parentH) * 100).toFixed(1));
                $('#panel_section_height_val').val(Math.abs((heightss / parentH) * 100).toFixed(1));
                $('.active').css('height', Math.abs((heightss / parentH) * 100).toFixed(1) + '%');
                break;
            case 'panel_section_height_val':
                var parentH = $('.active').parent().height();
                var heightss = parseFloat($('.active').css('height'));
                $(inputId).val(Math.abs((heightss / parentH) * 100).toFixed(1));
                $('#section_height_val').val(Math.abs((heightss / parentH) * 100).toFixed(1));
                $('.active').css('height', Math.abs((heightss / parentH) * 100).toFixed(1) + '%');
                break;
            case 'section_top_val':
                var parentT = $('.active').parent().height();
                var topss = parseFloat($('.active').css('top'));
                $(inputId).val(Math.abs((topss / parentT) * 100).toFixed(1));
                $('.active').css('top', Math.abs((topss / parentT) * 100).toFixed(1) + '%');
                break;
            case 'section_left_val':
                var parentL = $('.active').parent().width();
                var leftss = parseFloat($('.active').css('left'));
                $(inputId).val(Math.abs((leftss / parentL) * 100).toFixed(1));
                $('.active').css('left', Math.abs((leftss / parentL) * 100).toFixed(1) + '%');
                checkPositionCheckbox();
                break;
            case 'page_content_width_val':
                $(inputId).val(95);
                $('.active').css('width', '95%');
                $('#content_width_val').val(95);
                $('#content_width_val').next().text('%');
                break;
            case 'content_width_val':
                var widthSr = getStyle($('#content_standard>.section').first(), 'width');
                var widthCr = getStyle($('#content_standard>.section').first().find('.container_section').first(), 'width');
                var w = '';
                if (widthSr.replace(' ', '') == '100%') {
                    if (widthCr.indexOf('px') >= 0) {
                        w = 'px';
                    }
                }
                if (widthCr.replace(' ', '') == '100%') {
                    if (widthSr.indexOf('px') >= 0) {
                        w = 'px';
                    }
                }
                if (w == 'px') {
                    var widthWw = ((parseFloat($('#content_width_val').val()) / $('#content_standard').width()) * 100).toFixed(1);
                    $('#content_width_val').val(widthWw);
                    $('#content_width_val').next().text('%');
                    $('#content_standard .section').each(function () {
                        var width = getStyle($(this), 'width');
                        var itemId = $(this).attr('id');
                        var widthC = getStyle($('#' + itemId + '>.container_section'), 'width');
                        if (width.replace(' ', '') == '100%') {
                            $('#' + itemId + '>.container_section').css('width', $('#content_width_val').val() + '%');
                        }
                        if (widthC.replace(' ', '') == '100%') {
                            $(this).css('width', $('#content_width_val').val() + '%');
                        }
                    });
                    $('#pages .section').each(function () {
                        if ($(this).attr('class').indexOf('sectionContent') < 0) {
                            var width = getStyle($(this), 'width');
                            var itemId = $(this).attr('id');
                            var widthC = getStyle($('#' + itemId + '>.container_section'), 'width');
                            if (width.replace(' ', '') == '100%') {
                                $('#' + itemId + '>.container_section').css('width', $('#content_width_val').val() + '%');
                            }
                            if (widthC.replace(' ', '') == '100%') {
                                $(this).css('width', $('#content_width_val').val() + '%');
                            }
                        }
                    });
                }
                break;
        }

    }
    $('#unit').remove();
    saveStep();
}

function collapseOrExpandEl(el, id) {
    if ($('#' + id).attr('option') != 0) {
        $('#' + id).attr('option', '0');

    } else {
        $('#' + id).attr('option', '1');
    }

    right_panel('layers');
    saveStep();
}

function setButtonStyles(id, option) {

    var styles = $('#styles').find('[rel = "' + id + '"]').text();
    try {
        //console.log('устанавливаем CSS свойства кнопки согласно опции');
        var tempB = '';
        var stylesS = styles.replace('#' + id + '{', '').split('}')[0];
        tempB = styles.replace('#' + id + ':hover{', '$').split('$')[1];
        var stylesH = tempB.split('}')[0];
        tempB = styles.replace('#' + id + ':active{', '$').split('$')[1];
        var stylesA = tempB.split('}')[0];
        switch (option) {
            case 'standard':
                $('.active').attr('style', stylesS);
                break;
            case 'hover':
                $('.active').attr('style', stylesH);
                break;
            case 'active':
                $('.active').attr('style', stylesA);
                break;
        }

    } catch (ex) {
        //console.log('корректируем значения CSS свойства кнопки согласно опции');
        var s = document.createElement('p');
        var styleActive = $('.active').attr('style');
        $(s).text('#' + id + '{' + styleActive + '}#' + id + ':hover{' + styleActive + '}#' + id + ':active{' + styleActive + '}');
        $(s).attr('rel', id);
        $('#styles').append(s);
        //console.log('устанавливаем CSS свойства кнопки согласно опции');
        var tempB = '';
        styles = $('#styles').find('[rel = "' + id + '"]').text();
        var stylesS = styles.replace('#' + id + '{', '').split('}')[0];
        tempB = styles.replace('#' + id + ':hover{', '$').split('$')[1];
        var stylesH = tempB.split('}')[0];
        tempB = styles.replace('#' + id + ':active{', '$').split('$')[1];
        var stylesA = tempB.split('}')[0];
        switch (option) {
            case 'standard':
                $('.active').attr('style', stylesS);
                break;
            case 'hover':
                $('.active').attr('style', stylesH);
                break;
            case 'active':
                $('.active').attr('style', stylesA);
                break;
        }
    }
}

function buttonSelectOption(option) {
    //console.log('выбираем опцию для кнопки');
    clearInterval(interval);
    var idButton = $('.active').attr('id');
    $('.button_op').css('background', 'rgba(0,0,0,0)');
    $('#button_' + option).css('background', 'rgba(182, 160, 243, 0.1)');
    $('.active').attr('option', option);
    setButtonStyles(idButton, option);
    useButtonOption();
}

function saveButtonOption() {
    //console.log('saveButtonOption');
    var id = $('.active').attr('id');
    var option = $('.active').attr('option');

    var styles = $('#styles').find('[rel = "' + id + '"]').text();
    try {
        var tempB = '';
        var stylesS = styles.replace('#' + id + '{', '').split('}')[0];
        tempB = styles.replace('#' + id + ':hover{', '$').split('$')[1];
        var stylesH = tempB.split('}')[0];
        tempB = styles.replace('#' + id + ':active{', '$').split('$')[1];
        var stylesA = tempB.split('}')[0];
        switch (option) {
            case 'standard':
                stylesS = $('.active').attr('style');
                break;
            case 'hover':
                stylesH = $('.active').attr('style');
                break;
            case 'active':
                stylesA = $('.active').attr('style');
                break;
        }
        try {
            var width = stylesS.replace('width:', '$').split('$')[1].split(';')[0];
            var height = stylesS.replace('height:', '$').split('$')[1].split(';')[0];
            var top = stylesS.replace('top:', '$').split('$')[1].split(';')[0];
            var left = stylesS.replace('left:', '$').split('$')[1].split(';')[0];
            stylesS = stylesS.replace('width:' + width + ';', '') + 'width:' + $('.active').css('width') + ';';
            stylesS = stylesS.replace('height:' + height + ';', '') + 'height:' + $('.active').css('height') + ';';
            stylesS = stylesS.replace('top:' + top + ';', '') + 'top:' + $('.active').css('top') + ';';
            stylesS = stylesS.replace('left:' + left + ';', '') + 'left:' + $('.active').css('left') + ';';
            width = stylesH.replace('width:', '$').split('$')[1].split(';')[0];
            height = stylesH.replace('height:', '$').split('$')[1].split(';')[0];
            top = stylesH.replace('top:', '$').split('$')[1].split(';')[0];
            left = stylesH.replace('left:', '$').split('$')[1].split(';')[0];
            stylesH = stylesH.replace('width:' + width + ';', '') + 'width:' + $('.active').css('width') + ';';
            stylesH = stylesH.replace('height:' + height + ';', '') + 'height:' + $('.active').css('height') + ';';
            stylesH = stylesH.replace('top:' + top + ';', '') + 'top:' + $('.active').css('top') + ';';
            stylesH = stylesH.replace('left:' + left + ';', '') + 'left:' + $('.active').css('left') + ';';
            width = stylesA.replace('width:', '$').split('$')[1].split(';')[0];
            height = stylesA.replace('height:', '$').split('$')[1].split(';')[0];
            top = stylesA.replace('top:', '$').split('$')[1].split(';')[0];
            left = stylesA.replace('left:', '$').split('$')[1].split(';')[0];
            stylesA = stylesA.replace('width:' + width + ';', '') + 'width:' + $('.active').css('width') + ';';
            stylesA = stylesA.replace('height:' + height + ';', '') + 'height:' + $('.active').css('height') + ';';
            stylesA = stylesA.replace('top:' + top + ';', '') + 'top:' + $('.active').css('top') + ';';
            stylesA = stylesA.replace('left:' + left + ';', '') + 'left:' + $('.active').css('left') + ';';
        } catch (ex1) {
            //console.log(ex1);
        }


        $('#styles').find('[rel = "' + id + '"]').text('#' + id + '{' + stylesS + '}#' + id + ':hover{' + stylesH + '}#' + id + ':active{' + stylesA + '}');
    } catch (ex) {
        //console.log(ex)
        var s = document.createElement('p');
        $(s).attr('rel', id);
        if ($('#styles').find('[rel = "' + id + '"]').lenght == 0) {
            $('#styles').append(s);
        }
        var styleActive = $('.active').attr('style');
        $(s).text('#' + id + '{' + styleActive + '}#' + id + ':hover{' + styleActive + '}#' + id + ':active{' + styleActive + '}');
        $('#styles').find('[rel = "' + id + '"]').text($(s).text());
    }

    useButtonOption();
}

function useButtonOption() {
    //console.log('useButtonOption');
    var id = $('.active').attr('id');
    if ($('#styles').find('[rel = "' + id + '"]').text() == '' || $('#styles').find('[rel = "' + id + '"]').text() == undefined) {
        var s = document.createElement('p');
        $(s).attr('rel', id);
        $('#styles').append(s);
        var styleActive = $('.active').attr('style');
        $(s).text('#' + id + '{' + styleActive + '}#' + id + ':hover{' + styleActive + '}#' + id + ':active{' + styleActive + '}');
        $('#styles').find('[rel = "' + id + '"]').text($(s).text());
    }
    var styles = $('#styles').find('[rel = "' + id + '"]').text();
    if (styles != '') {
        var standardStyles, hoverStyles, activeStyles;

        standardStyles = styles.replace('#' + id + '{', '').split('}')[0];

        hoverStyles = styles.replace('#' + id + ':hover{', '$').split('$')[1];
        hoverStyles = hoverStyles.split('}')[0];

        activeStyles = styles.replace('#' + id + ':active{', '$').split('$')[1];
        activeStyles = activeStyles.split('}')[0];

        var arrayStulesS = standardStyles.split(';');

        var isUrl = false;
        for (var i = 0; i < arrayStulesS.length - 1; i++) {
            var style = arrayStulesS[i].split(':')[0].replace(' ', '');
            try {
                var valueStyle = arrayStulesS[i].replace('http://', '$').replace('localhost:', '#').split(':')[1].replace('$', 'http://').replace('#', 'localhost:');
                switch (style) {
                    case 'font-size':
                        if (valueStyle.indexOf('px') >= 0) {
                            if (parseInt(valueStyle) < 21) {
                                $('#button-standard').css(style, valueStyle);
                            } else {
                                $('#button-standard').css(style, '20px');
                            }
                            break;
                        } else {
                            if (parseInt(valueStyle) < 143) {
                                $('#button-standard').css(style, valueStyle);
                            } else {
                                $('#button-standard').css(style, '20px');
                            }
                        }
                        break;
                    case 'color':
                        $('#button-standard').css(style, valueStyle);
                        break;
                    case 'font-family':
                        $('#button-standard').css(style, valueStyle);
                        break;
                    case 'font-weight':
                        $('#button-standard').css(style, valueStyle);
                        break;
                    case 'text-decoration':
                        $('#button-standard').css(style, valueStyle);
                        break;
                    case 'font-style':
                        $('#button-standard').css(style, valueStyle);
                        break;
                    case 'text-align':
                        $('#button-standard').css(style, valueStyle);
                        break;
                    case 'letter-spacing':
                        $('#button-standard').css(style, valueStyle);
                        break;
                    case 'background':
                        $('#button-standard').css(style, valueStyle);
                        if (valueStyle.indexOf('url') >= 0) {
                            isUrl = true;
                        }
                        break;
                    case 'background-color':
                        $('#button-standard').css(style, valueStyle);
                        if (valueStyle.indexOf('url') >= 0) {
                            isUrl = true;
                        }
                        break;
                    case 'background-image':
                        $('#button-standard').css(style, valueStyle);
                        if (valueStyle.indexOf('url') >= 0) {
                            isUrl = true;
                        }
                        break;
                }
            } catch (ex) {
                //console.log(ex);
            }
        }
        if (isUrl) {
            $('#button-standard>span').hide();
        } else {
            $('#button-standard>span').show();
        }
        isUrl = false;
        var arrayStulesH = hoverStyles.split(';');
        for (var iH = 0; iH < arrayStulesH.length - 1; iH++) {
            try {
                var styleH = arrayStulesH[iH].split(':')[0].replace(' ', '');
                var valueStyleH = arrayStulesH[iH].replace('http://', '$').replace('localhost:', '#').split(':')[1].replace('$', 'http://').replace('#', 'localhost:');
                switch (styleH) {
                    case 'font-size':
                        if (valueStyleH.indexOf('px') >= 0) {
                            if (parseInt(valueStyleH) < 21) {
                                $('#button-hover').css(styleH, valueStyleH);
                            } else {
                                $('#button-hover').css(styleH, '20px');
                            }
                            break;
                        } else {
                            if (parseInt(valueStyleH) < 143) {
                                $('#button-hover').css(styleH, valueStyleH);
                            } else {
                                $('#button-hover').css(styleH, '20px');
                            }
                        }
                        break;
                    case 'color':
                        $('#button-hover').css(styleH, valueStyleH);
                        break;
                    case 'font-family':
                        $('#button-hover').css(styleH, valueStyleH);
                        break;
                    case 'font-weight':
                        $('#button-hover').css(styleH, valueStyleH);
                        break;
                    case 'text-decoration':
                        $('#button-hover').css(styleH, valueStyleH);
                        break;
                    case 'font-style':
                        $('#button-hover').css(styleH, valueStyleH);
                        break;
                    case 'text-align':
                        $('#button-hover').css(styleH, valueStyleH);
                        break;
                    case 'letter-spacing':
                        $('#button-hover').css(styleH, valueStyleH);
                        break;
                    case 'background':
                        $('#button-hover').css(styleH, valueStyleH);
                        if (valueStyleH.indexOf('url') >= 0) {
                            isUrl = true;
                        }
                        break;
                    case 'background-color':
                        $('#button-hover').css(styleH, valueStyleH);
                        if (valueStyleH.indexOf('url') >= 0) {
                            isUrl = true;
                        }
                        break;
                    case 'background-image':
                        $('#button-hover').css(styleH, valueStyleH);
                        if (valueStyleH.indexOf('url') >= 0) {
                            isUrl = true;
                        }
                        break;
                }
            } catch (ex) {
                //console.log(ex);
            }
        }
        if (isUrl) {
            $('#button-hover>span').hide();
        } else {
            $('#button-hover>span').show();
        }

        isUrl = false;
        var arrayStulesA = activeStyles.split(';');
        for (var iA = 0; iA < arrayStulesA.length - 1; iA++) {
            try {
                var styleA = arrayStulesH[iA].split(':')[0].replace(' ', '');
                var valueStyleA = arrayStulesA[iA].replace('http://', '$').replace('localhost:', '#').split(':')[1].replace('$', 'http://').replace('#', 'localhost:');
                switch (styleA) {
                    case 'font-size':
                        if (valueStyleA.indexOf('px') >= 0) {
                            if (parseInt(valueStyleA) < 21) {
                                $('#button-active').css(styleA, valueStyleA);
                            } else {
                                $('#button-active').css(styleA, '20px');
                            }
                            break;
                        } else {
                            if (parseInt(valueStyleA) < 143) {
                                $('#button-active').css(styleA, valueStyleA);
                            } else {
                                $('#button-active').css(styleA, '20px');
                            }
                        }
                    case 'color':
                        $('#button-active').css(styleA, valueStyleA);
                        break;
                    case 'font-family':
                        $('#button-active').css(styleA, valueStyleA);
                        break;
                    case 'font-weight':
                        $('#button-active').css(styleA, valueStyleA);
                        break;
                    case 'text-decoration':
                        $('#button-active').css(styleA, valueStyleA);
                        break;
                    case 'font-style':
                        $('#button-active').css(styleA, valueStyleA);
                        break;
                    case 'text-align':
                        $('#button-active').css(styleA, valueStyleA);
                        break;
                    case 'letter-spacing':
                        $('#button-active').css(styleA, valueStyleA);
                        break;
                    case 'background':
                        $('#button-active').css(styleA, valueStyleA);
                        if (valueStyleA.indexOf('url') >= 0) {
                            isUrl = true;
                        }
                        break;
                    case 'background-color':
                        $('#button-active').css(styleA, valueStyleA);
                        if (valueStyleA.indexOf('url') >= 0) {
                            isUrl = true;
                        }
                        break;
                    case 'background-image':
                        $('#button-active').css(styleA, valueStyleA);
                        if (valueStyleA.indexOf('url') >= 0) {
                            isUrl = true;
                        }
                        break;
                }
            } catch (ex) {
                //console.log(ex);
            }
        }
        if (isUrl) {
            $('#button-active>span').hide();
        } else {
            $('#button-active>span').show();
        }

        //border
        var border = $('.active').css('border');
        if (border != undefined) {
            $('#button-standard').css('border', border);
            $('#button-hover').css('border', border);
            $('#button-active').css('border', border);
        }
        //border-radius
        var borderRadius = $('.active').css('border-radius');
        if (borderRadius != undefined) {
            $('#button-standard').css('border-radius', borderRadius);
            $('#button-hover').css('border-radius', borderRadius);
            $('#button-active').css('border-radius', borderRadius);
        }
        //box-shadow
        var boxShadow = $('.active').css('box-shadow');
        if (boxShadow != undefined) {
            $('#button-standard').css('box-shadow', boxShadow);
            $('#button-hover').css('box-shadow', boxShadow);
            $('#button-active').css('box-shadow', boxShadow);
        }
        //text-shadow
        var textShadow = $('.active').css('text-shadow');
        if (textShadow != undefined) {
            $('#button-standard').css('text-shadow', textShadow);
            $('#button-hover').css('text-shadow', textShadow);
            $('#button-active').css('text-shadow', textShadow);
        }
    }
    //saveStep();
    $('#tranform_val').val(parseFloat($('.active').css('transition-duration')));
}

function viewImageByUrl() {
    //console.log('показываем изображение по ссылке');
    var url = $('#image_url input').first().val();
    var urlB = $('#image_url_b input').first().val();
    if (url == '' || urlB == '') {
        return;
    }
    $('#save_image_2').css('top', $('#image_url').position().top + 74);
    $('#download_image_from_gallery>img').remove();
    $('#download_image_from_gallery_b>img').remove();
    var img = document.createElement('img');
    $(img).attr('src', url);
    $('#download_image_from_gallery').append(img);
    $('#download_image_from_gallery_b').append(img);
}

$(function () {
    $('#image_url input').on('change', function () {
        if ($('#image_url input').val() != '') {
            $('#save_image_2').show();
            $('#save_image_2').css('top', $('#image_url').position().top + 74);
            $('#image_url #Catalog').parent().parent().show();
        }
    });

    $('#image_file #Gatalog').on('change', function () {
    });

    $('#add_catalog>input').on('change', function () {
        var name = $('#add_catalog>input').val();
        $.post('/Cabinet/Editor/AddCatalogs', { name: name }, function () {
            loadCatalogs('user');
            $('#add_catalog>input').val('');
            $('#add_catalog').hide();
            $.ajax({
                url: '/Cabinet/Editor/LoadImageByFile',
                cache: false,
                success: function (html) {
                    $('#image_file').html(html);
                }
            });
            $.ajax({
                url: '/Cabinet/Editor/LoadImageByUrl',
                cache: false,
                success: function (html) {
                    $('#image_url>span').html(html);
                    $('#image_url #Catalog').parent().parent().hide();
                }
            });
        });
    });
});

function addCatalog(element) {
    //console.log('добавляем каталог');
    $('#add_catalog').show();
}

function loadCatalogs(type) {
    //console.log('загружаем каталоги');
    $.post('/Cabinet/Editor/GetCatalogs', { type: type }, function (data) {
        if (type == 'user') {
            $('#main_gallery>.galleries>div').remove();
        } else {
            $('#other_gallery>.galleries').html('');
        }
        for (var i = 0; i <= data.res.length - 1; i++) {
            var div = document.createElement('div');
            $(div).attr('class', 'type_of_gallery');
            $(div).attr('onclick', 'getGallery(this, "' + data.res[i] + '", "' + type + '")');
            $(div).html('<i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>&nbsp;&nbsp;' + data.res[i]);
            if (type == 'user') {
                $('#add_catalog').before(div);
            } else {
                $('#other_gallery>.galleries').append(div);
            }
        }
    });
}

function saveImageByUrl() {
    //console.log('сохраняем изображение по ссылке');
    var url = $('#image_url #UrlImage').val();
    var catalog = $('#image_url #Catalog').val();
    $.post('/Cabinet/Editor/LoadImageByUrl', { url: url, catalog: catalog });
    $('#save_image_2').hide();
    $('#image_url #Catalog').parent().parent().hide();
    $('#download_image_from_gallery img').remove();
    $('#image_url #Url').val('');
}

function clearUrlOrtion() {
    //console.log('очищаем настройку ссылки элемента ');
    $('#woc-url > label > input').prop('checked', false);
    $('#web_url').val('');
    $('#list_pages').children().each(function () {
        $(this).prop('selected', false);
    });
    $('#list_pages').children().first().prop('selected', true);
    $('#list_anchor').children().each(function () {
        $(this).prop('selected', false);
    });
    $('#list_anchor').children().first().prop('selected', true);
}

function saveMap() {
    //console.log('сохраняем карту');
    var center = map.getCenter();
    var array = center.toString().split(',');
    center = parseFloat(array[0]).toFixed(6) + ',' + parseFloat(array[1]).toFixed(6);
    var zoom = map.getZoom();
    var type = map.getType();

    $('.active>img').attr('script', "var map; ymaps.ready(function () { map = new ymaps.Map('map_id', { center: [" + center + "], zoom: " + zoom + ", type: '" + type + "'}); });");
    switch (type) {
        case 'yandex#map':
            $('.active>img').attr('src', '//static-maps.yandex.ru/1.x/?ll=' + array[1] + ',' + array[0] + '&l=map&z=' + zoom);
            break;
        case 'yandex#satellite':
            $('.active>img').attr('src', '//static-maps.yandex.ru/1.x/?ll=' + array[1] + ',' + array[0] + '&l=sat&z=' + zoom);
            break;
        case 'yandex#hybrid':
            $('.active>img').attr('src', '//static-maps.yandex.ru/1.x/?ll=' + array[1] + ',' + array[0] + '&l=sat, skl&z=' + zoom);
            break;
        default:
            $('.active>img').attr('src', '//static-maps.yandex.ru/1.x/?ll=' + array[1] + ',' + array[0] + '&l=map&z=' + zoom);
    }
    hideLitebox();
    saveStep();
}

//function addField() {
//    //console.log('добавляем новый элемент формы');
//    var count = parseInt($('#form_elements table>tbody').children().last().children().first().text()) + 1;
//    var tr = document.createElement('tr');
//    var idForm = $('#form_elements').children().first().next().find('tbody').children().first().next().attr('id').replace('_param_', '$').split('$')[0];
//    var countInput = $('#form_elements').children().first().next().find('tbody').children().length;
//    $(tr).attr('id', idForm + '_param_' + countInput + '_tr');
//    var td1 = document.createElement('td');
//    $(td1).attr('style', 'width: 5%; height: 60px;');
//    $(td1).text(count);

//    var td2 = document.createElement('td');
//    $(td2).attr('style', 'width: 30%; height: 60px;');
//    var input = document.createElement('input');
//    $(input).attr('placeholder', 'Название');
//    $(input).attr('style', 'width: 95%;');
//    $(input).val($(this).find('label').text());
//    $(td2).append(input);
//    $(tr).append(td2);

//    var td3 = document.createElement('td');
//    $(td3).attr('style', 'width: 15%; height: 60px;');

//    var select = document.createElement('select');
//    var option = document.createElement('option');
//    $(option).attr('value', 'text');
//    $(option).text('текст');
//    var option11 = document.createElement('option');
//    $(option11).attr('value', 'textarea');
//    $(option11).text('текстовое поле');
//    var option1 = document.createElement('option');
//    $(option1).attr('value', 'email');
//    $(option1).text('почта');
//    var option2 = document.createElement('option');
//    $(option2).attr('value', 'tel');
//    $(option2).text('телефон');
//    var option3 = document.createElement('option');
//    $(option3).attr('value', 'url');
//    $(option3).text('ссылка');
//    var option4 = document.createElement('option');
//    $(option4).attr('value', 'file');
//    $(option4).text('файл');
//    $(select).append(option);
//    $(select).append(option11);
//    $(select).append(option1);
//    $(select).append(option2);
//    $(select).append(option3);
//    $(select).append(option4);
//    $(td3).append(select);
//    $(select).val($(this).find('input').attr('type'));

//    var td4 = document.createElement('td');
//    $(td4).attr('style', 'width: 30%; height: 60px;');
//    var input3 = document.createElement('input');
//    $(input3).attr('style', 'width: 95%;');
//    $(input3).attr('placeholder', 'Подсказка');
//    $(input3).val($(this).find('input').attr('placeholder'));
//    $(td4).append(input3);

//    var id = $('.active').attr('id');
//    var temp = $('#form_elements').find('tbody').children().last().children().last().prev().find('input').attr('id');
//    var td5 = document.createElement('td');
//    $(td5).attr('style', 'width: 20%; height: 60px; position: relative;');
//    var input5 = document.createElement('input');
//    $(input5).attr('style', 'width: 95%; border: 1px solid grey;');
//    $(input5).attr('id', idForm + '_param_' + countInput + 'validvalid');

//    $(input5).val('* данные не прошли проверку');
//    $(td5).append(input5);

//    var td6 = document.createElement('td');
//    $(td6).attr('style', 'width: 5%; position: relative; font-size: 16px; text-align: center; color: red; cursor: pointer;');
//    $(td6).html('<i class="fa fa-trash-o btn btn-red" onclick="delField(this)"></i>');

//    $(tr).append(td1);
//    $(tr).append(td2);
//    $(tr).append(td3);
//    $(tr).append(td4);
//    $(tr).append(td5);
//    $(tr).append(td6);

//    $('#form_elements table>tbody').append(tr);
//    $(input).focus();

//    //scroll to bottom
//    var objDiv = document.getElementById("form_elements");
//    objDiv.scrollTop = objDiv.scrollHeight;
//}

function saveForm(id) {
    //console.log('сохраняем элемент формы');

    var label = $('#form_elements table>tbody').children().last().children().first().children().val();
    var placeholder = $('#form_elements table>tbody').children().last().children().first().next().children().val();
    var valid = $('#form_elements table>tbody').children().last().children().first().next().next().children().val();
    var intfield = $('#form_elements table>tbody').children().last().children().last().children().val();
    $('#' + id).parent().children('label').text(label);
    $('#' + id).parent().children().first().next().attr('placeholder', placeholder);
    $('#' + id).parent().children().first().next().attr('integrationfield', intfield);
    $('#' + id).parent().children('span').text(valid);
    //var totalHeight = 0;
    //var div = document.createElement('div');
    //$(div).attr('style', $('.containerForm').attr('style'));

    //var label = document.createElement('label');
    //$(label).attr('style', $('.containerForm').children().first().attr('style'));
    //$(label).text($('#form_elements').children().first().find('input').val());
    //$(label).attr('class', 'editable_element');
    //$(div).append(label);
    //var i = 0;
    //var idForm = $('.containerForm').children().first().next().children().first().next().attr('id').replace('_param_', '$').split('$')[0];
    ////$('#' + idForm).html('');
    ////$('#' + idForm).append(label);
    //var styleDiv = $('.containerForm').children('div').first().attr('style');
    //var styleLabel = $('.containerForm').children('div').first().children().first().attr('style');
    //var styleInput = $('.containerForm').find('input').first().attr('style');
    //var styleTexearea = '';
    //try {
    //    styleTexearea = $('.containerForm').find('textarea').first().attr('style');
    //} catch (ex) {
    //    styleTexearea = styleInput;
    //}
    //var styleValid = $('.containerForm').children('div').first().children().last().attr('style');


    //$('#form_elements table>tbody').children().each(function () {
    //    if (i > 0) {
    //        var div1 = document.createElement('div');
    //        var label1 = document.createElement('label');
    //        $(label1).css('margin', '0');
    //        $(label1).attr('class', idForm + '_label editable_element');
    //        $(label1).attr('style', styleLabel);
    //        $(label1).text($(this).children().first().next().find('input').val());
    //        $(div1).append(label1);

    //        if ($(this).children().first().next().next().find('select').val() !== 'textarea') {
    //            var input1 = document.createElement('input');
    //            $(input1).attr('class', idForm + '_param editable_element');
    //            $(input1).attr('id', idForm + '_param_' + i);
    //            if ($(this).children().first().next().next().find('select').val() === 'file') {
    //                $(input1).attr('name', 'files');
    //            } else {
    //                $(input1).attr('name', 'parametrs');
    //            }
    //            //переделать высоту
    //            //$(input1).attr('style', 'width: 100%; height: ' + height + 'px;');
    //            $(input1).attr('type', $(this).children().first().next().next().find('select').val());
    //            $(input1).attr('disabled', 'disabled');
    //            $(input1).attr('style', styleInput);
    //            try {
    //                if ($('.edit_form').find($(this).attr('id').replace('_tr', '')).length > 0) {
    //                    $(input1).css('width', getStyle($('.edit_form').find('#' + idForm + '_param_' + i), 'width'));
    //                    $(input1).css('height', getStyle($('.edit_form').find('#' + idForm + '_param_' + i), 'height'));
    //                }
    //            } catch (ex) {

    //            }
    //            $(input1).attr('placeholder', $(this).children().first().next().next().next().find('input').val());
    //            $(div1).append(input1);

    //            var valid = document.createElement('span');
    //            $(valid).text($(this).children().last().prev().find('input').val());
    //            $(valid).attr('class', idForm + '_param_valid' + ' editable_element');
    //            $(valid).attr('style', styleValid);
    //            $(div1).append(valid);
    //        } else {
    //            var textarea = document.createElement('textarea');
    //            $(textarea).attr('class', idForm + '_param editable_element');
    //            $(textarea).attr('id', idForm + '_param_' + i);
    //            $(textarea).attr('name', 'parametrs');
    //            $(textarea).attr('style', styleTexearea);
    //            try {
    //                if ($('.edit_form').find($(this).attr('id').replace('_tr', '')).length > 0) {
    //                    $(input1).css('width', getStyle($('.edit_form').find('#' + idForm + '_param_' + i), 'width'));
    //                    $(input1).css('height', getStyle($('.edit_form').find('#' + idForm + '_param_' + i), 'height'));
    //                } else {
    //                    $(input1).css('width', '100%');
    //                    $(input1).css('height', '70px');
    //                }
    //            } catch (ex) {
    //                $(input1).css('width', '100%');
    //                $(input1).css('height', '70px');
    //            }
    //            //переделать высоту
    //            //$(textarea).attr('style', 'width: 100%; height: ' + height + 'px;');
    //            $(textarea).attr('placeholder', $(this).children().first().next().next().next().find('input').val());
    //            $(textarea).attr('style', styleTexearea);
    //            $(textarea).attr('disabled', 'disabled');
    //            $(div1).append(textarea);
    //            var valid = document.createElement('span');
    //            $(valid).text($(this).children().last().prev().find('input').val());
    //            $(valid).attr('class', idForm + '_param_valid' + ' editable_element');
    //            $(valid).attr('style', styleValid);
    //            $(div1).append(valid);
    //        }
    //        $(div).append(div1);
    //    }
    //    i++;
    //});
    //var divB = document.createElement('div');
    //$(divB).attr('type', 'submit');
    //$(divB).attr('id', idForm + '_submit');
    //$(divB).attr('class', 'editable_element')
    //$(divB).attr('onclick', 'submitForm("' + idForm + '")');
    //$(divB).attr('style', $('.containerForm').children().last().attr('style'));
    //$(divB).text($('#form_elements').children().last().children('input').first().val());
    //$(div).append(divB);
    //$('.containerForm').remove();
    //$('.edit_form').append(div);
    //correctionFormSize();
    hideLitebox();
}

function showTitle(el, id) {
    //console.log('показываем или скрываем атрибут Title');
    if ($(el).attr('class') == 'fa fa-check-circle-o') {
        $(el).attr('class', 'fa fa-circle-o');
        if ($('#' + id).attr('m-title') == '' || $('#' + id).attr('m-title') == undefined) {
            $('#' + id).attr('m-title', $('#' + id).attr('title'));
        }
        $('#' + id).attr('title', '');
    } else if ($(el).attr('class') == 'fa fa-circle-o') {
        $(el).attr('class', 'fa fa-check-circle-o');
        $('#' + id).attr('title', $('#' + id).attr('m-title'));
    }
}

function selPage() {
    //console.log('выбираем страницу');
    $('#add_page').css('background', '#737373');
    $('#add_litebox').css('background', 'rgba(0,0,0,0)');
    $('#addPage_content').show();
    $('#addLitebox_content').hide();
    createPage = true;
}

function selLitebox() {
    //console.log('выбираем всплываеющее окно');
    $('#add_litebox').css('background', '#737373');
    $('#add_page').css('background', 'rgba(0,0,0,0)');
    $('#addPage_content').hide();
    $('#addLitebox_content').show();
    $('#edit_background_color_ll>div>div').css('background-color', 'rgba(0,0,0,0.5)');
    createPage = false;
}


function btnNextWidthL() {
    if ($('#width_l_val').next().text() == '%') {
        if (parseFloat($('#width_l_val').val()) < 100) {
            $('#width_l_val').val(parseFloat($('#width_l_val').val()) + 0.1);
        }
    } else {
        $('#width_l_val').val(parseInt($('#width_l_val').val()) + 1);
    }
}

function btnPrevWidthL() {
    if ($('#width_l_val').next().text() == '%') {
        if (parseFloat($('#width_l_val').val()) > 0) {
            $('#width_l_val').val(parseFloat($('#width_l_val').val()) - 0.1);
        }
    } else {
        if (parseInt($('#width_l_val').val()) > 0) {
            $('#width_l_val').val(parseInt($('#width_l_val').val()) - 1);
        }
    }
}

function btnNextHeightL() {
    if ($('#height_l_val').next().text() == '%') {
        if (parseFloat($('#height_l_val').val()) < 100) {
            $('#height_l_val').val(parseFloat($('#height_l_val').val()) + 0.1);
        }
    } else {
        $('#height_l_val').val(parseInt($('#height_l_val').val()) + 1);
    }
}

function btnPrevHeightL() {
    if ($('#height_l_val').next().text() == '%') {
        if (parseFloat($('#height_l_val').val()) > 0) {
            $('#height_l_val').val(parseFloat($('#height_l_val').val()) - 0.1);
        }
    } else {
        if (parseInt($('#height_l_val').val()) > 0) {
            $('#height_l_val').val(parseInt($('#height_l_val').val()) - 1);
        }
    }
}

//function unselectEl() {
//console.log('снять выбор с элемента');
//$('#sel_element').remove();
//$('.woc').hide();
//var el = document.createElement('div');
//var top = $('#woc_column_2').height() / 2 - 7;
//$(el).attr('style', 'width: 100%; text-align: center; font-size: 14px; height: 15px; position: relative; top: ' + top + 'px;');
//$(el).text('Выберите элемент');
//$(el).attr('id', 'sel_element');
//$('#woc_column_2').append(el);
//}

function editFormElement(id) {
    //console.log('открываем окно редактирования элемента формы');
    $('#form_elements').html('');
    $('#form_buttons>.yes').attr('onclick', 'saveForm("' + id + '")')
    var table = document.createElement('table');
    $(table).attr('style', 'width: 100%;');
    var trH = document.createElement('tr');

    var thH1 = document.createElement('th');
    $(thH1).text('Название');
    var thH2 = document.createElement('th');
    $(thH2).text('Подсказка');
    var thH3 = document.createElement('th');
    $(thH3).text('Текст ошибки');

    var thH4 = document.createElement('th');
    $(thH4).text('Название поля в сервисе');

    $(trH).append(thH1);
    $(trH).append(thH2);
    $(trH).append(thH3);
    $(trH).append(thH4);
    $(table).append(trH);
    var tr = document.createElement('tr');

    var td1 = document.createElement('td');
    $(td1).attr('style', 'width: 25%;');
    var input = document.createElement('input');
    $(input).attr('placeholder', 'Название');
    $(input).attr('style', 'width: 95%;');
    $(input).val($('#' + id).prev().text());
    $(td1).append(input);
    $(tr).append(td1);

    var td2 = document.createElement('td');
    $(td2).attr('style', 'width: 25%;');
    var input3 = document.createElement('input');
    $(input3).attr('style', 'width: 95%;');
    $(input3).attr('placeholder', 'Подсказка');
    $(input3).val($('#' + id).attr('placeholder'));
    $(td2).append(input3);

    var td3 = document.createElement('td');
    $(td3).attr('style', 'width: 25%; height: 60px; position: relative;');
    var input5 = document.createElement('input');
    $(input5).attr('style', 'width: 95%; border: 1px solid grey;');
    $(input5).val($('#' + id).next().text());
    $(td3).append(input5);
    $(input3).attr('placeholder', 'Текст ошибки');

    var td4 = document.createElement('td');
    $(td4).attr('style', 'width: 25%;');
    var inputntegration = document.createElement('input');
    $(inputntegration).attr('style', 'width: 95%;');
    $(inputntegration).attr('placeholder', 'Поле интеграции');
    $(inputntegration).val($('#' + id).attr('integrationfield'));
    $(td4).append(inputntegration);

    $(tr).append(td1);
    $(tr).append(td2);
    $(tr).append(td3);
    $(tr).append(td4);
    $(table).append(tr);

    var div = document.createElement('div');
    $(div).append(table);
    $('#form_elements').append(div);

    showLitebox('form_option_window');
}

//function editForm() {
//    //console.log('открываем окно редактирования формы');
//    $('#form_elements').html('');

//    var title = document.createElement('div');
//    $(title).attr('style', 'width: 100%; height: 60px; padding: 0 10px;');
//    var p = document.createElement('p');
//    $(p).text('Название формы:');
//    $(p).attr('style', 'font-size: 14px; font-weight: bold;');
//    $(title).append(p);
//    var input1 = document.createElement('input');
//    $(input1).val($('.containerForm>label').text());
//    $(title).append(input1);
//    $('#form_elements').append(title);


//    var div = document.createElement('div');
//    $(div).attr('style', 'width: 100%; padding: 0 10px;');
//    var pF = document.createElement('p');
//    $(pF).attr('style', 'font-size: 14px; font-weight: bold;');
//    $(pF).text('Поля:');
//    $(div).append(pF);

//    var table = document.createElement('table');
//    $(table).css('width', '100%');
//    var trH = document.createElement('tr');
//    $(trH).css('font-size', '12px');
//    var thH1 = document.createElement('th');
//    $(thH1).text('№');
//    var thH2 = document.createElement('th');
//    $(thH2).text('Название');
//    var thH3 = document.createElement('th');
//    $(thH3).text('Тип поля');
//    var thH4 = document.createElement('th');
//    $(thH4).text('Подсказка');
//    var thH5 = document.createElement('th');
//    var thH6 = document.createElement('th');
//    $(thH5).text('Текст отшибки');
//    $(trH).append(thH1);
//    $(trH).append(thH2);
//    $(trH).append(thH3);
//    $(trH).append(thH4);
//    $(trH).append(thH5);
//    $(trH).append(thH6);
//    $(table).append(trH);

//    var c = 1;

//    $('.containerForm').children('div').each(function () {
//        if ($(this).attr('type') != 'submit') {
//            var tr = document.createElement('tr');
//            $(tr).attr('id', $(this).children().first().next().attr('id') + '_tr');
//            var td1 = document.createElement('td');
//            $(td1).attr('style', 'width: 5%; height: 60px;');
//            $(td1).text(c);
//            c++;

//            var td2 = document.createElement('td');
//            $(td2).attr('style', 'width: 25%; height: 60px;');
//            var input = document.createElement('input');
//            $(input).attr('placeholder', 'Название');
//            $(input).attr('style', 'width: 95%;');
//            $(input).val($(this).find('label').text());
//            $(td2).append(input);
//            $(tr).append(td2);

//            var td3 = document.createElement('td');
//            $(td3).attr('style', 'width: 20%; height: 60px;');

//            var select = document.createElement('select');
//            var option = document.createElement('option');
//            $(option).attr('value', 'text');
//            $(option).text('текст');
//            var option11 = document.createElement('option');
//            $(option11).attr('value', 'textarea');
//            $(option11).text('текстовое поле');
//            var option1 = document.createElement('option');
//            $(option1).attr('value', 'email');
//            $(option1).text('почта');
//            var option2 = document.createElement('option');
//            $(option2).attr('value', 'tel');
//            $(option2).text('телефон');
//            var option3 = document.createElement('option');
//            $(option3).attr('value', 'url');
//            $(option3).text('ссылка');
//            var option4 = document.createElement('option');
//            $(option4).attr('value', 'file');
//            $(option4).text('файл');
//            $(select).append(option);
//            $(select).append(option11);
//            $(select).append(option1);
//            $(select).append(option2);
//            $(select).append(option3);
//            $(select).append(option4);
//            $(td3).append(select);
//            if ($(this).children().last().prev().is('textarea')) {
//                $(select).val('textarea');
//            } else {
//                $(select).val($(this).find('input').attr('type'));
//            }

//            var td4 = document.createElement('td');
//            $(td4).attr('style', 'width: 25%; height: 60px;');
//            var input3 = document.createElement('input');
//            $(input3).attr('style', 'width: 95%;');
//            $(input3).attr('placeholder', 'Название');
//            $(input3).val($(this).children().last().prev().attr('placeholder'));
//            $(td4).append(input3);

//            var td5 = document.createElement('td');
//            $(td5).attr('style', 'width: 20%; height: 60px; position: relative;');
//            var input5 = document.createElement('input');
//            $(input5).attr('style', 'width: 95%; border: 1px solid grey;');
//            $(input5).attr('id', $(this).children().last().attr('id') + 'valid');
//            $(input5).val($(this).children().last().text());
//            $(td5).append(input5);

//            var td6 = document.createElement('td');
//            $(td6).attr('style', 'width: 5%; position: relative; font-size: 16px; text-align: center; color: red; cursor: pointer;');
//            $(td6).html('<i class="fa fa-trash-o btn btn-red" onclick="delField(this)"></i>');

//            $(tr).append(td1);
//            $(tr).append(td2);
//            $(tr).append(td3);
//            $(tr).append(td4);
//            $(tr).append(td5);
//            $(tr).append(td6);
//            $(table).append(tr);
//        }
//    });

//    $(div).append(table);
//    $('#form_elements').append(div);

//    var button = document.createElement('div');
//    $(button).attr('style', 'width: 100%; height: 60px; padding: 0 10px;')
//    var pB = document.createElement('p');
//    $(pB).attr('style', 'font-size: 14px; font-weight: bold;');
//    $(pB).text('Текст кнопки:');
//    var inputB = document.createElement('input');
//    $(inputB).val($('.containerForm').children().last().text());
//    $(button).append(pB);
//    $(button).append(inputB);
//    $('#form_elements').append(button);

//    $('#form_option_window').show();
//    $('#form_option_window').css('left', ($(window).width() - $('#map_option_window').width()) / 2);
//    var topF = ($(window).height() - $('#map_option_window').height()) / 2;
//    $('#background_modal').css('top', '0px');
//    $('#background_modal').css('width', $(window).width());
//    $('#background_modal').css('height', $(window).height());
//    $('#form_option_window').animate({
//        top: topF
//    }, 200);



//}

function selectFormElement(el) {
    //console.log('выбираем редактируемый элемент формы');

    if ((el.is('input') || el.is('textarea')) && el.attr('class').indexOf('editable_element') >= 0) {
        $('#positionElement').hide();
        $('#rotate').hide();
        $('#max_width').hide();
    }

    if ((el.is('label') || el.is('span')) && el.attr('class').indexOf('editable_element') >= 0) {
        //$('#woc_text').show();
    }

    if (el.parent().attr('type') == 'submit') {
        $('#positionElement').hide();
        $('#rotate').hide();
        $('#max_width').hide();
        el = el.parent();
    }

    $('#formpar_active').remove();
    $('#move_form_element').hide();
    $('#edit_form_element').remove();
    $('#remove_form_element').remove();
    var elClass = el.attr('class');
    if (elClass == undefined) elClass = '';
    if (elClass.indexOf('containerForm') >= 0) {
        $('#positionElement').hide();
        $('#rotate').hide();
        $('#max_width').hide();
    } else {
        var parActiv = document.createElement('div');
        $(parActiv).attr('id', 'formpar_active');
        var wra = el.parent().width();
        var hra = el.parent().height();
        var tra = el.parent().position().top;
        var lra = el.parent().position().left + $('.edit_form').offset().left - ($('#left_panel').width() + parseInt($('#left_panel').css('border-left')) + parseInt($('#left_panel').css('border-right')));
        $(parActiv).attr('style', 'width:' + wra + 'px; height:' + hra + 'px; border: 1px dotted #428bca; position: absolute; top:' + tra + 'px; left:' + lra + 'px;');
        if (el.parent().attr('type') != 'title') {
            $('#content_standard').append(parActiv);
        }

        var eLeft = lra + wra + 2;
        var eTop = tra + 16;
        var editBlock = document.createElement('div');
        $(editBlock).attr('id', 'edit_form_element');
        $(editBlock).attr('class', 'fa fa-edit');
        var inputId = '';
        if ($(el).parent().attr('class') != undefined) {
            if ($(el).parent().attr('class').indexOf('containerInput') >= 0) {
                inputId = $(el).parent().children().first().next().attr('id');
            }
        }
        $(editBlock).attr('onclick', 'editFormElement("' + inputId + '")');
        $(editBlock).attr('style', 'cursor: pointer; position: absolute; z-index: 1000000; width: 14px; height: 14px; left:' + eLeft + 'px; top:' + eTop + 'px;');
        $('#content_standard').append(editBlock);

        if (el.attr('type') == 'submit') {
            var bLeft = el.position().left + $('.edit_form').offset().left - ($('#left_panel').width() + parseInt($('#left_panel').css('border-left')) + parseInt($('#left_panel').css('border-right'))) + el.width() + 2;
            var bTop = el.position().top;
            $('#move_form_element').attr('style', 'cursor: move; position: absolute; z-index: 1000000; width: 14px; height: 14px; left:' + bLeft + 'px; top:' + bTop + 'px;');
            $('#edit_form_element').attr('style', 'cursor: pointer; position: absolute; z-index: 1000000; width: 14px; height: 14px; left:' + bLeft + 'px; top:' + (bTop + 16) + 'px;');
            $('#edit_form_element').attr('onclick', 'editFormButton()');
        } else if (el.parent().attr('type') == 'title') {
            var lLeft = el.parent().position().left + $('.edit_form').offset().left - ($('#left_panel').width() + parseInt($('#left_panel').css('border-left')) + parseInt($('#left_panel').css('border-right'))) + el.parent().width() + 2;
            var lTop = el.parent().position().top;
            $('#move_form_element').attr('style', 'cursor: move; position: absolute; z-index: 1000000; width: 14px; height: 14px; left:' + lLeft + 'px; top:' + lTop + 'px;');
            $('#edit_form_element').attr('style', 'cursor: pointer; position: absolute; z-index: 1000000; width: 14px; height: 14px; left:' + lLeft + 'px; top:' + (lTop + 16) + 'px;');
            $('#edit_form_element').attr('onclick', 'editFormTitle()');
        } else {
            var mLeft = lra + wra + 2;
            var removeBlock = document.createElement('div');
            $(removeBlock).attr('id', 'remove_form_element');
            var mTop = tra + 32;
            $(removeBlock).attr('class', 'ui-draggable ui-widget-content fa fa-trash-o');
            $(removeBlock).attr('onclick', 'deleteEl("' + inputId + '")');
            $('#move_form_element').attr('style', 'cursor: move; position: absolute; z-index: 1000000; width: 14px; height: 14px; left:' + mLeft + 'px; top:' + tra + 'px;');
            $(removeBlock).attr('style', 'cursor: pointer; background: rgba(0,0,0,0); position: absolute; z-index: 1000000; width: 14px; height: 14px; left:' + mLeft + 'px; top:' + mTop + 'px;');
            $('#content_standard').append(removeBlock);
        }
        $('#move_form_element').show();
    }

    $('#active_form').remove();

    hideTextarea();
    $('.active').removeClass('active');
    var div = document.createElement('div');
    $(div).attr('id', 'active_form');
    var width = parseInt(el.css('width'));
    var height = parseInt(el.css('height'));
    var top = el.offset().top + $('#content').scrollTop() - 105;
    var left = el.offset().left - ($('#left_panel').width() + parseInt($('#left_panel').css('border-left')) + parseInt($('#left_panel').css('border-right')));
    $(div).attr('style', 'position: absolute; z-index: 100; width:' + width + 'px; height:' + height + 'px; top:' + top + 'px; left:' + left + 'px; border: 2px solid #428bca;');
    $('#content_standard').append(div);
    el.addClass('active');
}

function getStyle(el, css) {
    var val = '';
    var styl = '';
    var style;
    try {
        style = el.find('a').attr('style').replace(';', '; ');
        val = style.replace(' ' + css + ':', '$').split('$')[1].split(';')[0].replace(' ', '');
    } catch (ex) {
    }
    try {
        style = el.attr('style').replace(';', '; ');
        var st = style.replace(' ' + css + ':', '$').split('$')[1].split(';')[0].replace(' ', '');
        if (st != val) {
            if (val == '' || val == undefined) {
                val = val + style.replace(' ' + css + ':', '$').split('$')[1].split(';')[0].replace(' ', '');
            } else {
                val = style.replace(' ' + css + ':', '$').split('$')[1].split(';')[0].replace(' ', '');
            }
        }

        if (css == 'height' || css == 'width') {
            val = style.replace(' ' + css + ':', '$').split('$')[1].split(';')[0].replace(' ', '');
        }
    } catch (ex) {
        try {
            style = el.attr('style').replace(';', '; ');
            var st = style.replace('' + css + ':', '$').split('$')[1].split(';')[0].replace(' ', '');
            if (st != val) {
                if (val == '' || val == undefined) {
                    val = val + style.replace('' + css + ':', '$').split('$')[1].split(';')[0].replace(' ', '');
                } else {
                    val = style.replace('' + css + ':', '$').split('$')[1].split(';')[0].replace(' ', '');
                }
            }

            if (css == 'height' || css == 'width') {
                val = style.replace('' + css + ':', '$').split('$')[1].split(';')[0].replace(' ', '');
            }
        } catch (exq) {

            val = el.css(css);
        }
    }

    if (val == undefined) {
        switch (css) {
            case 'background-color':
                val = 'rgba(255,255,255,0)';
                break;
            case 'background-position':
                val = '0% 0%';
                break;
            case 'background-attachment':
                val = 'scroll';
                break;
            case 'background-repeat':
                val = 'no-repeat';
                break;
            case 'font-style':
                val = 'Arial';
                break;
            case 'font-weight':
                val = 'normal';
                break;
            case 'font-variant':
                val = 'normal';
                break;
            case 'font-stretch':
                val = 'normal';
                break;
            case 'text-indent':
                val = '0';
                break;
            case 'text-align':
                val = 'left';
                break;
            case 'text-decoration':
                val = 'none';
                break;
            case 'text-shadow':
                val = 'none';
                break;
            case 'letter-spacing':
                val = '0';
                break;
            case 'word-spacing':
                val = 'normal';
                break;
            case 'text-transform':
                val = 'none';
                break;
            case 'line-height':
                val = 'normal';
                break;
            case 'white-space':
                val = 'normal';
                break;
            case 'margin-top':
                val = '0';
                break;
            case 'margin-right':
                val = '0';
                break;
            case 'margin-bottom':
                val = '0';
                break;
            case 'margin-left':
                val = '0';
                break;
            case 'margin':
                val = '0';
                break;
            case 'padding-top':
                val = '0';
                break;
            case 'padding-right':
                val = '0';
                break;
            case 'padding-bottom':
                val = '0';
                break;
            case 'padding-left':
                val = '0';
                break;
            case 'padding':
                val = '0';
                break;
            case 'border-top-width':
                val = 'medium';
                break;
            case 'border-right-width':
                val = 'medium';
                break;
            case 'border-bottom-width':
                val = 'medium';
                break;
            case 'border-left-width':
                val = 'medium';
                break;
            case 'border-top-style':
                val = 'none';
                break;
            case 'border-right-style':
                val = 'none';
                break;
            case 'border-bottom-style':
                val = 'none';
                break;
            case 'border-left-style':
                val = 'none';
                break;
            case 'position':
                val = 'static';
                break;
            case 'top':
                val = '0';
                break;
            case 'right':
                val = 'auto';
                break;
            case 'bottom':
                val = 'auto';
                break;
            case 'left':
                val = '0';
                break;
            case 'float':
                val = 'none';
                break;
            case 'clear':
                val = 'none';
                break;
            case 'z-index':
                val = 'auto';
                break;
            case 'display':
                val = 'inline';
                break;
            case 'overflow':
                val = 'visible';
                break;
            case 'clip':
                val = 'auto';
                break;
            case 'visibility':
                val = 'visible';
                break;
            case 'cursor':
                val = 'auto';
                break;
            case 'clip':
                val = 'auto';
                break;
            case 'transform':
                val = 'none';
                break;
            default: {
                val = '';
            }
        }
    }
    return val;
}

function groupProperty(prop, val) {
    if ($('.active').attr('class').indexOf('form') >= 0 && $('.form_option_parametr input').first().prop('checked')) {
        var clas = $('.active').attr('class').split(' ')[0];
        $('.edit_form .' + clas).each(function () {
            $(this).css(prop, val);
            if (prop == 'width') {
                $(this).parent().css(prop, val);
            }
            if ($(this).attr('style').indexOf('"') > 0) {
                $(this).attr('style', $(this).attr('style').replace(/"/g, '\''));
            }
        });
    }
}

function correctPosition() {
    //console.log('корректируем позицию активного блока');
    var width = (parseInt($('.active').css('width')) / parseInt($('.active').parent().css('width'))) * 100;
    var height = (parseInt($('.active').css('height')) / parseInt($('.active').parent().css('height'))) * 100;
    var top = (parseInt($('.active').css('top')) / $('.active').parent().height()) * 100;
    var left = (parseInt($('.active').css('left')) / $('.active').parent().width()) * 100;
    var isSection = false;
    try {
        if ($('.active').attr('class').indexOf('section') >= 0) {
            isSection = true;
        }
    } catch (ex2) {

    }
    if (left + width > 100 && !isSection) {
        $('.active').css('left', (100 - width) + '%');
    }
    try {
        var rotate = getStyle($('.active'), 'transform');
        $('.active').css('transform', 'none');
        $('#active').css('transform', 'none');
        var activeClosest = $('.active').offset().left;
        $('#active').width($('.active').width());
        $('#active').height($('.active').height());

        $('#move_block').css('left', activeClosest - ($('#left_panel').width() + parseInt($('#left_panel').css('border-left')) + parseInt($('#left_panel').css('border-right'))));
        $('#move_block').css('top', $('.active').closest('.section').position().top + parseInt($('.active').css('top')));
        $('#move_text').css('left', $('#move-text').offset().left - $('#workspace').offset().left);
        $('#move_text').css('top', $('#move-text').offset().top - $('#workspace').offset().top - 15);
        $('#name_block').css('top', -($('#name_block').height()));
        $('#name_block').css('top', -($('#name_block').height()));
        $('#active').css('left', $('#move_block').css('left'));
        $('#resize').css('top', 0);

        $('#move_block').css('width', parseFloat($('.active').css('width')) - 6);
        $('#move_block').css('height', parseFloat($('.active').css('height')) - 6);
        $('#resize').css('width', parseInt($('.active').css('width')));
        $('#resize').css('height', parseInt($('.active').css('height')));
        $('.active').css('transform', rotate);
        $('#active').css('transform', rotate);
        $('#move_block').css('transform', rotate);
        $('#move_text').css('transform', rotate);
    } catch (ex) { }
    setPositionLeftToElementButtons();

    if ($('.edit_form').length) {
        $('#formpar_active').width($('.active').parent().width());
        $('#formpar_active').height($('.active').parent().height());
        if ($('.active').attr('type') != 'submit') {
            $('#active_form').css('top', $('.active').parent().offset().top - 105 + $('.active').position().top + $('#content').scrollTop());
            $('#active_form').css('left', $('.edit_form').offset().left - ($('#left_panel').width() + parseInt($('#left_panel').css('border-left')) + parseInt($('#left_panel').css('border-right'))) + $('.active').parent().position().left);
            $('#active_form').css('width', $('.active').width() + parseInt(getStyle($('.active'), 'border-left')) + parseInt(getStyle($('.active'), 'border-right')) + parseInt(getStyle($('.active'), 'padding-left')) + parseInt(getStyle($('.active'), 'padding-right')));
            $('#active_form').css('height', $('.active').height() + parseInt(getStyle($('.active'), 'border-top')) + parseInt(getStyle($('.active'), 'border-bottom')) + parseInt(getStyle($('.active'), 'padding-top')) + parseInt(getStyle($('.active'), 'padding-bottom')));
            $('#move_form_element').css('top', $('.active').parent().css('top'));
            $('#move_form_element').css('left', $('.edit_form').offset().left - ($('#left_panel').width() + parseInt($('#left_panel').css('border-left')) + parseInt($('#left_panel').css('border-right'))) + parseInt($('.active').parent().css('left')) + parseInt($('.active').css('padding-left')) + parseInt($('.active').css('padding-right')) + $('.active').parent().width() + 2);
        } else {
            $('#active_form').css('top', $('.active').offset().top - 105 + $('#content').scrollTop());
            $('#active_form').css('left', $('.edit_form').offset().left - ($('#left_panel').width() + parseInt($('#left_panel').css('border-left')) + parseInt($('#left_panel').css('border-right'))) + $('.active').position().left);
            $('#active_form').css('width', $('.active').width() + parseInt(getStyle($('.active'), 'border-left')) + parseInt(getStyle($('.active'), 'border-right')) + parseInt(getStyle($('.active'), 'padding-left')) + parseInt(getStyle($('.active'), 'padding-right')));
            $('#active_form').css('height', $('.active').height() + parseInt(getStyle($('.active'), 'border-top')) + parseInt(getStyle($('.active'), 'border-bottom')) + parseInt(getStyle($('.active'), 'padding-top')) + parseInt(getStyle($('.active'), 'padding-bottom')));
            $('#move_form_element').css('top', $('.active').css('top'));
            $('#move_form_element').css('left', $('.edit_form').offset().left - ($('#left_panel').width() + parseInt($('#left_panel').css('border-left')) + parseInt($('#left_panel').css('border-right'))) + parseInt($('.active').css('left')) + parseInt($('.active').css('padding-left')) + parseInt($('.active').css('padding-right')) + $('.active').width() + 2);
            $('#formpar_active').remove();
        }
        $('#edit_form_element').css('top', parseInt($('#move_form_element').css('top')) + 16);
        $('#edit_form_element').css('left', $('#move_form_element').css('left'));
        $('#remove_form_element').css('top', parseInt($('#edit_form_element').css('top')) + 16);
        $('#remove_form_element').css('left', $('#edit_form_element').css('left'));
    }
}

function correctionFormSize() {
    if ($('#content_standard').find('.edit_form').length > 0) {
        var heightForm = '';
        var genLabelHeight = parseInt(getStyle($('.edit_form').find('label').first(), 'height')) + parseInt(getStyle($('.edit_form').find('label').first(), 'margin-top')) + parseInt(getStyle($('.edit_form').find('label').first(), 'margin-bottom'));
        //коррекция высоты влодженных блоков
        var divHeight = genLabelHeight;
        $('.containerForm').children('div').each(function () {
            var childHeight = 0;
            if ($(this).attr('type') != 'submit') {
                $(this).children().each(function () {
                    childHeight += parseInt(getStyle($(this), 'height'));
                });
                $(this).css('height', childHeight + 'px')
            } else {
                childHeight += parseInt(getStyle($(this), 'height')) + parseInt(getStyle($(this), 'margin-top')) + parseInt(getStyle($(this), 'margin-bottom'));
            }
            divHeight += childHeight;
        });

        divHeight = divHeight + parseInt(getStyle($('.containerForm'), 'padding-top')) + parseInt(getStyle($('.containerForm'), 'padding-bottom'));
        $('.containerForm').css('height', divHeight + 'px');
        $('.edit_form').css('height', divHeight + 'px');
    }
}

function saveChangesForm() {
    var id = $('.tab_active').parent().attr('rel');
    var top = getStyle($('#' + id), 'top');
    var left = getStyle($('#' + id), 'left');
    $('#' + id).html($('.containerForm').html());
    $('#' + id).attr('style', $('.containerForm').attr('style'));
    $('#' + id).css('top', top);
    $('#' + id).css('left', left);
    $('#' + id).css('width', getStyle($('.edit_form'), 'width'));
    $('#' + id).css('height', getStyle($('.edit_form'), 'height'));
    $('.editable_element').removeClass('editable_element');
    var clas = $('#' + id).find('span').first().attr('class');
    $('.' + clas).hide();
    $('.edit_form').remove();
    $(".active_form").removeClass("active_form");
    $('#active_form').remove();
    $('#formpar_active').removeClass('formpar_active');
    $('#move_form_element').hide();
    $('#remove_form_element').remove();
    $('#edit_form_element').remove();
    setTimeout({}, 1000);
}

function delField(el) {
    $(el).parent().parent().remove();
    var num = 0;
    $('#form_elements tbody').children().each(function () {
        if (num > 0) {
            $(this).children().first().text(num);
        }
        num++;
    });
}

function removeFormElement(id) {
    //console.log('удалили элемент формы');
    if (id != undefined && id != '') {
        $('#' + id).parent().remove();
        $('#active_form').remove();
        $('#formpar_active').remove();
        $('#move_form_element').hide();
        $('#edit_form_element').remove();
        $('#remove_form_element').remove();
        var formId = $('.tab_active').parent().attr('rel');
        var curentId = parseInt(id.replace(formId + '_param_', ''));
        var int = curentId + 1;
        var b = true;
        while (b) {
            if ($('#' + formId + '_param_' + int).length) {
                $('#' + formId + '_param_' + int).attr('id', formId + '_param_' + (int - 1));
                int++;
            } else {
                b = false;
            }
        }
    }
    hideLitebox();
}



function editFormButton() {
    $('#edit_text').val($('.edit_form').find('[type="submit"]').text());
    $('#buttons_ETE>.yes').attr('onclick', 'save_ETE("submit")');
    showLitebox('editTextElement');
}

function editFormTitle() {
    $('#edit_text').val($('.edit_form').find('[type="title"]').children().first().text());
    $('#buttons_ETE>.yes').attr('onclick', 'save_ETE("title")');
    showLitebox('editTextElement');
}


function clearUrl() {
    $('.active>a').attr('href', '#');
    $('.active>a').removeAttr('type');
    $('#web_url').val(' ');
    $('#list_pages').val('');
    $('#list_anchor').val('');
    $('#list_liteboxes').val('');
    $('#woc-url').find('[type="radio"]').each(function () {
        $(this).prop('checked', false);
    });
}

function createAnchor(x, y, element, rand) {
    if ($('#anchor_name').val() != '') {
        //console.log('добавили якорь');
        var idEl = "anchor_" + rand;
        var div = document.createElement('div');
        div.id = idEl;
        div.className = "ui-widget-content anchor";
        var a = document.createElement('a');
        $(a).attr('class', 'fa fa-anchor');
        $(a).attr('name', $('#anchor_name').val());
        div.appendChild(a);
        $('#' + element).append(div);
        var top = (((parseInt(y) - $('#' + element).offset().top) / $('#' + element).height()) * 100).toFixed(1);
        var left = (((parseInt(x) - $('#' + element).offset().left) / $('#' + element).width()) * 100).toFixed(1);
        var width = ((15 / $('#' + element).width()) * 100).toFixed(1);
        var height = ((15 / $('#' + element).height()) * 100).toFixed(1);
        $('#' + idEl).attr('style', 'background: rgba(0,0,0,0); border: none; border-radius: 0px 0px 0px 0px; line-height: 1; font-family: Arial; font-size: 16px; color: #000000; position: absolute; width: ' + width + '%; height: ' + height + '%; top:' + top + '%; left: ' + left + '%; z-index:' + counter);
        $('#' + idEl).attr('type', 'anchor');
        $('#' + idEl).attr('m-title', $('#anchor_name').val());
        reload_right_panel();
        counter++;
        saveStep();
        hideLitebox();
    } else {
        $("#attention_anchor_name").animate({
            opacity: 1
        }, 200, function () {
            $("#attention_anchor_name").animate({
                opacity: 0
            }, 200, function () {
                $("#attention_anchor_name").animate({
                    opacity: 1
                }, 200, function () {
                    $("#attention_anchor_name").animate({
                        opacity: 0
                    }, 200, function () {
                        $("#attention_anchor_name").animate({
                            opacity: 1
                        }, 200, function () {
                            $("#attention_anchor_name").animate({
                                opacity: 0
                            }, 2500);
                        });
                    });
                });
            });
        });
    }
}

function ow_addFontFamily() {
    showLitebox('add_font_family_window');
    $('#url_font').val('');
    $('#url_font').focus();
}

function addFontFamily() {
    //console.log('добавение шрифта');
    var urlFont = $('#url_font').val();

    if (urlFont != '') {
        hideLitebox();
        $('#added_font_family_window span').remove();
        $('#added_font_family_window>.dial_content>p').text('Загруженно шрифтов - 0:');

        //var top = ($(window).height() - $('#added_font_family_window').height()) / 2;
        //var left = ($(window).width() - $('#added_font_family_window').width()) / 2;
        var loader = document.createElement('img');
        $(loader).attr('src', '/Areas/Cabinet/Images/load.gif');
        $(loader).attr('style', 'height: 60px;');
        $('#added_font_family_window>.dial_content').append(loader);
        showLitebox('added_font_family_window');

        $.ajax({
            url: urlFont,
            dataType: "text",
            async: true,
            done: (function () {
                //alert("success");
            }),
            fail: (function () {
                //alert("error");
            }),
            always: (function (msg) {
                test = msg;
                //alert('Содержимое файла: ' + test);                
            })
        });

        var myVar = 0;
        // Создали промежуточный блок div#temp
        var tempDiv = $('body').append($('<div/>').attr('id', 'temp')).find('#temp');
        // Загрузили данные в этот блок
        tempDiv.load(urlFont);
        // Записали ответ в переменную
        myVar = tempDiv.text();

        var tempText = '';

        setTimeout(function () {
            tempText = $('#temp').text();
            if (tempText != '' && tempText != undefined) {
                var text = tempText.replace(/ /g, '');
                if (text.indexOf('font-family:') >= 0) {
                    //пропарсенный style
                    var fontsArray = text.replace(/$/g, '').replace(/font-family:/g, '$').split('$');
                    //шрифты
                    var fonts = new Array();
                    for (var g = 1; g < fontsArray.length; g++) {
                        $('#added_font_family_window>.dial_content>img').remove();
                        //текущий шрифт
                        var boolFonts = true;
                        fontsArray[g - 1] = fontsArray[g].split(';')[0].replace(/'/g, '').replace(/"/g, '');
                        for (var p = 0; p < g - 1; p++) {
                            if (fontsArray[p] == fontsArray[g - 1]) {
                                boolFonts = false;
                            }
                        }
                        if (boolFonts) {
                            fonts.push(fontsArray[g - 1]);
                            var span = document.createElement('span');
                            $(span).text(fontsArray[g - 1]);
                            $('#added_font_family_window>.dial_content>p').text('Загруженно шрифтов - ' + fonts.length + ':');
                            $('#added_font_family_window>.dial_content').append(span);
                        }
                    }

                    var link = document.createElement('link');
                    $(link).attr('href', urlFont);
                    $(link).attr('class', 'added_fonts');
                    $(link).attr('rel', 'stylesheet');
                    $(link).attr('name', fonts);
                    $('head').append(link);

                    var url = '/Cabinet/Editor/Fonts';
                    $.ajax({
                        url: url,
                        dataType: 'html',
                        //data: { ajax: true },
                        type: 'GET',
                        success: function (html) {
                            $('#font_family').html(html);
                        }
                    });
                }

            } else {
                hideLitebox();
                $('#added_font_family_window>.dial_content>p').text('Загруженно шрифтов - 0:');
                showLitebox('add_font_family_window');
                $('#url_font').next().text('* ссылка ошибочна или не существует');
                $("#url_font").next().animate({
                    opacity: 1
                }, 200, function () {
                    $("#url_font").next().animate({
                        opacity: 0
                    }, 200, function () {
                        $("#url_font").next().animate({
                            opacity: 1
                        }, 200, function () {
                            $("#url_font").next().animate({
                                opacity: 0
                            }, 200, function () {
                                $("#url_font").next().animate({
                                    opacity: 1
                                }, 200, function () {
                                    $("#url_font").next().animate({
                                        opacity: 0
                                    }, 2500);
                                });
                            });
                        });
                    });
                });
            }

            // Удалили промежуточный блок
            tempDiv.remove();
        }, 3000);

        //repeal();
    } else {
        //console.log('ссылка пуста');
        $('#url_font').next().text('* поле не может быть пустым');
        $("#url_font").next().animate({
            opacity: 1
        }, 200, function () {
            $("#url_font").next().animate({
                opacity: 0
            }, 200, function () {
                $("#url_font").next().animate({
                    opacity: 1
                }, 200, function () {
                    $("#url_font").next().animate({
                        opacity: 0
                    }, 200, function () {
                        $("#url_font").next().animate({
                            opacity: 1
                        }, 200, function () {
                            $("#url_font").next().animate({
                                opacity: 0
                            }, 2500);
                        });
                    });
                });
            });
        });
    }
}


function checkPageLoader() {
    var check = $('#page_loader').attr('option');
    if (check == '' || check == undefined || check == 'unchecked') {
        $('#page_loader').animate({ backgroundColor: '#428bca' }, 200);
        $('#page_loader>img').animate({ left: '7px' }, 200);
        $('#col_load_line').animate({ height: '30px' }, 200);
        $('#col_load_background').animate({ height: '30px' }, 200);
        $('#page_loader').attr('option', 'checked');
    } else {
        $('#page_loader').animate({ backgroundColor: '#555555' }, 200);
        $('#page_loader>img').animate({ left: '-7px' }, 200);
        $('#col_load_line').animate({ height: '0px' }, 200);
        $('#col_load_background').animate({ height: '0px' }, 200);
        $('#page_loader').attr('option', 'unchecked');
    }
}

function editAllElements(element) {
    if ($(element).prop('checked')) {
        $('.form_option_parametr input').prop('checked', true);
    } else {
        $('.form_option_parametr input').prop('checked', false);
    }
}

function openWindowSetParemetrs() {
    showLitebox('page_parametrs');
}

//function viewWindow(win) {
//    var top = ($(window).height() - $(win).height()) / 2;
//    var left = ($(window).width() - $(win).width()) / 2;
//    $(win).css('left', left);
//    $(win).animate({ top: top }, 200);
//    $(win).css('display', 'block');
//    $('#background_modal').css('top', '0');
//    $('#background_modal').width($(window).width());
//    $('#background_modal').height($(window).height());
//}

function count_column_prev() {
    var val = parseInt($('#page_content_column_val').val());
    if (val > 1) {
        $('#page_content_column_val').val(val - 1);
    }
}

function count_column_next() {
    var val = parseInt($('#page_content_column_val').val());
    if (val < 4) {
        $('#page_content_column_val').val(val + 1);
    }
}

function setPageParametrs() {
    $('#content_standard').find('.section').remove();
    addSection($('#page_content_column_val').val());
    $('#content').find('.section').each(function () {
        if ($(this).attr('type') == 'section') {
            $(this).css('width', $('#page_content_width_val').val() + $('#page_content_width_val').next().text());
        }
    });
    savePages();

    //console.log('скрываем окно');
    landingBackground = false;

    hideLitebox();

    reload_right_panel();
}

function selNetworkUrlVideo() {
    $('#vy').css('background-color', '#737373');
    $('#uv').css('background-color', 'rgba(0,0,0,0)');

    $('#user_url_video').hide();
    $('#network_url_video').show();
}

function selUserUrlVideo() {
    $('#uv').css('background-color', '#737373');
    $('#vy').css('background-color', 'rgba(0,0,0,0)');
    $('#user_url_video').show();
    $('#network_url_video').hide();
}

function viewInfo(el, parametr) {
    $('.info_element').hide();
    $('.sel').removeClass('sel');
    $(el).addClass('sel');
    $('#' + parametr + '_info').show();
}


function viewSubMenu(parametr) {
    $('.zoom').remove();
    $('#sub_element_sub_menu').animate({
        width: '0px'
    }, 200);
    $('.element_menus').hide();
    $('.element_catalog').css('background-color', 'rgba(0, 0, 0, 0)');
    var position;
    switch (parametr) {
        case 'sections':
            if ($('#sub_element_menu').width() > 0) {
                $('#sections').css('background-color', 'rgba(0, 0, 0, 0)');
                $('#menu_sections').hide();
            } else {
                $('#sections').css('background-color', '#666');
                $('#menu_sections').show();
            }            
            position = $('#sections').offset();
            break;
        case 'timers':
            if ($('#sub_element_menu').width() > 0) {
                $('#timers').css('background-color', 'rgba(0, 0, 0, 0)');
                $('#menu_timers').hide();
            } else {
                $('#timers').css('background-color', '#666');
                $('#menu_timers').show();
            }            
            position = $('#timers').offset();
            break;
        case 'widgets':
            if ($('#sub_element_menu').width() > 0) {
                $('#widgets').css('background-color', 'rgba(0, 0, 0, 0)');
                $('#menu_widgets').hide();
            } else {
                $('#widgets').css('background-color', '#666');
                $('#menu_widgets').show();
            }           
            position = $('#widgets').offset();
            break;
    }
    $('#sub_element_menu').css('top', position.top - $('#general_panel').height() - $('#option_panel').height());
    if (parseInt($('#sub_element_menu').css('top')) + $('#sub_element_menu').height() > $('#content_standard').height()) {
        if ($('#sub_element_menu').height() >= $('#content_standard').height()) {
            $('#sub_element_menu').css('top', 0);
        } else {
            $('#sub_element_menu').css('top', $('#content_standard').height() - $('#sub_element_menu').height());
        }
    }

    if ($('#sub_element_menu').width() > 0) {
        $('#sub_element_menu').animate({
            width: '0px'
        }, 200);
    } else {
        $('#sub_element_menu').animate({
            width: '160px'
        }, 200);
    }
}
function viewSubElements(element) {
    $('.zoom').remove();
    var id = $(element).attr('rel');
    $('.sellect_sections').hide();
    $('.section-categiry-' + id).show();
    $('#sub_element_sub_menu').css('left', (parseInt($('#sub_element_menu').css('left')) + $('#sub_element_menu').width()) + 'px');
    $('#sub_element_sub_menu').css('top', $('#sub_element_menu').css('top'));
    $('.category-menu').css('background-color', '#444');
    $(element).css('background-color', '#666');
    $('#sub_element_sub_menu').animate({
        width: '160px'
    }, 200);

}

function searchTotalsIdInChilds(el) {
    var ids = $(el).attr('id');
    el.children().each(function () {
        if ($(this).attr('id') != undefined && $(this).attr('id') != '') {
            ids += ',' + $(this).attr('id');
        }
        if ($(this).children().length > 0) {
            ids += ',' + searchTotalsIdInChilds($(this));
        }
    });
    return ids;
}

//function downimg(el) {
//    var img;
//    html2canvas(el, {
//        onrendered: function (canvas) {
//            img = canvas.toDataURL('image/png').replace("image/png", "image/octet-stream");
//            //window.location.href = img;
//            //canvas.toBlob(function (blob) {
//            //    saveAs(blob, "blabla.png");
//            //}, "image/png");

//        }
//    });
//    return img;
//}

function openWindowSaveSection() {
    if ($('.active').length && $('.active').attr('type') == 'section') {
        $('#title_section').val('');
        showLitebox('save_section_litebox');
    }
}


function saveActiveSection() {
    if ($('#title_section').val() != '') {
        var categoryKey = $('#category_section').val();
        var title = $('#title_section').val();
        var tempToSaveBlock = document.createElement('div');
        $(tempToSaveBlock).append($('.active'));
        var width = $(tempToSaveBlock).find('.section').css('width');
        $(tempToSaveBlock).find('.section').css('width', 'auto');
        var contentSection = htmlEntities($(tempToSaveBlock).html());
        $(tempToSaveBlock).find('.section').css('width', width);
        $('#content_standard').append($(tempToSaveBlock).children().first());
        var styles = "";
        var totalId = searchTotalsIdInChilds($('.active'));
        var arrayId = totalId.split(',');
        $('#styles').children().each(function () {
            for (var i = 0; i <= arrayId.length - 1; i++) {
                if ($(this).attr('id') == arrayId[i]) {
                    styles = styles + '$' + $(this).text();
                }
            }
        });
        var img;
        html2canvas($('.active'), {
            onrendered: function (canvas) {                
                img = canvas.toDataURL('image/png').replace("image/png", "image/octet-stream");
                $.ajax({
                    url: "/Cabinet/Editor/SaveSection",
                    data: {
                        title: title,
                        styles: styles,
                        content: contentSection,
                        img: img,
                        categoryKey: categoryKey
                    },
                    //async: true,
                    typedata: "json",
                    method: "post",
                    success: function (data) {
                        if (data === 'ok!') {
                            $.ajax({
                                url: "/Cabinet/Editor/ReadySections",
                                //async: true,
                                typedata: "json",
                                success: function (data) {
                                    $('#sub_menu_sections').html(data);
                                }
                            });
                            hideLitebox();
                        } else {
                            $("#title_section_error").text('- секция с таким названием уже существует');
                            $("#attention_title_section").animate({
                                opacity: 1
                            }, 200, function () {
                                $("#attention_title_section").animate({
                                    opacity: 0
                                }, 200, function () {
                                    $("#attention_title_section").animate({
                                        opacity: 1
                                    }, 200, function () {
                                        $("#attention_title_section").animate({
                                            opacity: 0
                                        }, 200, function () {
                                            $("#attention_title_section").animate({
                                                opacity: 1
                                            }, 200, function () {
                                                $("#attention_title_section").animate({
                                                    opacity: 0
                                                }, 2500);
                                            });
                                        });
                                    });
                                });
                            });
                            $("#title_section_error").animate({
                                opacity: 1
                            }, 200, function () {
                                $("#title_section_error").animate({
                                    opacity: 0
                                }, 200, function () {
                                    $("#title_section_error").animate({
                                        opacity: 1
                                    }, 200, function () {
                                        $("#title_section_error").animate({
                                            opacity: 0
                                        }, 200, function () {
                                            $("#title_section_error").animate({
                                                opacity: 1
                                            }, 200, function () {
                                                $("#title_section_error").animate({
                                                    opacity: 0
                                                }, 5000);
                                            });
                                        });
                                    });
                                });
                            });
                        }
                    },
                    error: function (data) {
                        $("#title_section_error").text('- ошибка сохранения секции');
                        $("#title_section_error").animate({
                            opacity: 1
                        }, 200, function () {
                            $("#title_section_error").animate({
                                opacity: 0
                            }, 200, function () {
                                $("#title_section_error").animate({
                                    opacity: 1
                                }, 200, function () {
                                    $("#title_section_error").animate({
                                        opacity: 0
                                    }, 200, function () {
                                        $("#title_section_error").animate({
                                            opacity: 1
                                        }, 200, function () {
                                            $("#title_section_error").animate({
                                                opacity: 0
                                            }, 5000);
                                        });
                                    });
                                });
                            });
                        });
                    },
                    type: "POST"
                });
            }
        });

    } else {
        $("#title_section_error").text('- название не может быть пустым');
        $("#attention_title_section").animate({
            opacity: 1
        }, 200, function () {
            $("#attention_title_section").animate({
                opacity: 0
            }, 200, function () {
                $("#attention_title_section").animate({
                    opacity: 1
                }, 200, function () {
                    $("#attention_title_section").animate({
                        opacity: 0
                    }, 200, function () {
                        $("#attention_title_section").animate({
                            opacity: 1
                        }, 200, function () {
                            $("#attention_title_section").animate({
                                opacity: 0
                            }, 2500);
                        });
                    });
                });
            });
        });
        $("#title_section_error").animate({
            opacity: 1
        }, 200, function () {
            $("#title_section_error").animate({
                opacity: 0
            }, 200, function () {
                $("#title_section_error").animate({
                    opacity: 1
                }, 200, function () {
                    $("#title_section_error").animate({
                        opacity: 0
                    }, 200, function () {
                        $("#title_section_error").animate({
                            opacity: 1
                        }, 200, function () {
                            $("#title_section_error").animate({
                                opacity: 0
                            }, 5000);
                        });
                    });
                });
            });
        });
        noRepeatID();
    }
}

function noRepeatID() {
    var totalsID = '';
    $('#content_standard').children('.section').each(function () {
        totalsID += $(this).attr('id') + findTotalID($(this));
    });
    $('#pages').find('.section').each(function () {
        if ($(this).attr('type') == 'section') {
            totalsID += ',' + $(this).attr('id') + findTotalID($(this));
        }
    });
    findEqualsIDs(totalsID);
}

function findTotalID(element) {
    var ids = '';
    $(element).children().each(function () {
        if ($(this).attr('id') != undefined && $(this).attr('id') != '') {
            ids += ',' + $(this).attr('id');
        }
        ids += ',' + findTotalID($(this));
    });
    return ids;
}

function findEqualsIDs(totalsID) {
    //удаление возможной копии страницы
    var pageActive = $('.tab_active').parent().attr('rel');
    $('#pages>#' + pageActive + '>.page_content_standard').html('');
    //поиск одинаковых id
    var arrayIDs = totalsID.split(',');
    for (var i = 0; i < arrayIDs.length; i++) {
        for (var j = 0; j < arrayIDs.length; j++) {
            if (i != j && arrayIDs[i] == arrayIDs[j] && arrayIDs[i] != '') {
                setNewID(arrayIDs[i], "");
                i = arrayIDs.length;
                j = arrayIDs.length;
            }
        }
    }
}

function setNewID(id, type) {
    var rand = Math.round((Math.random() * 100000));
    var element = $('#content_standard').find('#' + id).first();
    if (type == 'inactive') {
        $('#content_standard').find('#' + id).each(function () {            
            try{
                if ($(this).attr('class').indexOf('active') = -1) {
                    element = $(this);
                }
            }catch(ex){
                console.log('id = ' + id);
            }
                     
        });
    }
    console.log('Найдено повторение id, что недопустимо! В старом ID=' + id + ' заменен индекс на ' + rand);
    switch (element.attr('type')) {
        case 'text':
            var firtsPartId = id.split('_')[0];
            if (id.indexOf('form_') >= 0) {
                element.attr('id', 'form_' + rand);
                element.find('input').each(function () {
                    var inputIDPart = $(this).attr('id').split('_');
                    $(this).prev().attr('class', 'form_' + rand + '_label');
                    $(this).attr('id', 'form_' + rand + '_param_' + inputIDPart[inputIDPart.length - 1]);
                    $(this).attr('class', 'form_' + rand + '_param');
                    $(this).next().attr('id', 'form_' + rand + '_param_' + inputIDPart[inputIDPart.length - 1] + '_valid');
                    $(this).next().attr('class', 'form_' + rand + '_param_valid');
                });
                element.find('textarea').each(function () {
                    var inputIDPart = $(this).attr('id').split('_');
                    $(this).prev().attr('class', 'form_' + rand + '_label');
                    $(this).attr('id', 'form_' + rand + '_param_' + inputIDPart[inputIDPart.length - 1]);
                    $(this).attr('class', 'form_' + rand + '_param');
                    $(this).next().attr('id', 'form_' + rand + '_param_' + inputIDPart[inputIDPart.length - 1] + '_valid');
                    $(this).next().attr('class', 'form_' + rand + '_param_valid');
                });
                element.children('[type="submit"]').first().attr('id', 'form_' + rand + '_submit');
                element.children('[type="submit"]').first().attr('onclick', "submitForm('form_" + rand + "')");
                noRepeatID();
            } else {
                element.attr('id', firtsPartId + '_' + rand);
                noRepeatID();
            }
            break;
        case 'timer':
            element.attr('id', 'timer_' + rand);
            element.find('.timeTo').first().attr('id', 'countdown-' + rand);
            noRepeatID();
            break;
        case 'section':
            element.attr('id', 'section_' + rand);
            element.children().first().attr('id', 'content_section_' + rand);
            element.children().first().attr('id', 'content_section_' + rand);
            noRepeatID();
            break;
        case 'form':
            element.attr('id', 'form_' + rand);
            element.find('input').each(function () {
                var inputIDPart = $(this).attr('id').split('_');
                $(this).prev().attr('class', 'form_' + rand + '_label');
                $(this).attr('id', 'form_' + rand + '_param_' + inputIDPart[inputIDPart.length - 1]);
                $(this).attr('class', 'form_' + rand + '_param');
                $(this).next().attr('id', 'form_' + rand + '_param_' + inputIDPart[inputIDPart.length - 1] + '_valid');
                $(this).next().attr('class', 'form_' + rand + '_param_valid');
            });
            element.find('textarea').each(function () {
                var inputIDPart = $(this).attr('id').split('_');
                $(this).prev().attr('class', 'form_' + rand + '_label');
                $(this).attr('id', 'form_' + rand + '_param_' + inputIDPart[inputIDPart.length - 1]);
                $(this).attr('class', 'form_' + rand + '_param');
                $(this).next().attr('id', 'form_' + rand + '_param_' + inputIDPart[inputIDPart.length - 1] + '_valid');
                $(this).next().attr('class', 'form_' + rand + '_param_valid');
            });
            element.children('[type="submit"]').first().attr('id', 'form_' + rand + '_submit');
            element.children('[type="submit"]').first().attr('onclick', "submitForm('form_" + rand + "')");
            noRepeatID();
            break;
        default: {
            var elemClass = '';
            if (element.attr('class') != undefined) {
                elemClass = element.attr('class');
            }
            if (id.indexOf('_') < 0 && elemClass.indexOf('column') >= 0) {
                element.attr('id', 'c' + rand);
                noRepeatID();
            } else {
                var firtsPartId = id.split('_')[0];
                element.attr('id', firtsPartId + '_' + rand);
                noRepeatID();
            }

        }
    }
}

function viewLandingOptions() {
    if ($('#parameters_con').css('display') == 'none') {
        $('#edit_background_color>div>div>div').css('background-color', $('#content').css('background-color'))
        $('#edit_background_color_content>div>div>div').css('background-color', $('#content_standard').css('background-color'))

        var elem = $('#content_standard .section').first();
        var style = $(elem).attr('style');
        var width = style.replace('width:', '$').split('$')[1].split(';')[0].replace(' ', '');
        var element = $('#content_standard .section .container_section').first();
        style = $(element).attr('style');
        var widthC = style.replace('width:', '$').split('$')[1].split(';')[0].replace(' ', '');
        var widthW = '0px';
        if (width.replace(' ', '') == '100%') {
            widthW = widthC;
        }
        if (widthC.replace(' ', '') == '100%') {
            widthW = width;
        }
        if (widthW.indexOf('%') >= 0) {
            $('#content_width_val').next().text('%');
        } else {
            $('#content_width_val').next().text('px');
        }
        $('#content_width_val').val(parseFloat(widthW));
        $('#parameters_con').show();
    } else {
        $('#parameters_con').hide();
    }
}

function fontTab(param) {
    if (param == 'additional') {
        $('.tab').css('background-color', 'rgba(0,0,0,0)');
        $('#font_additional').css('background-color', '#333');
        $('#font-additional').show();
        $('#font-param').hide();
        $('#padding-apart').show();
        $('#padding-all').hide();
    } else {
        $('.tab').css('background-color', 'rgba(0,0,0,0)');
        $('#font_param').css('background-color', '#333');
        $('#font-additional').hide();
        $('#font-param').show();
    }
}


function backgroundTab(param) {
    if (param == 'picture') {
        $('.tab').css('background-color', 'rgba(0,0,0,0)');
        $('#background_picture').css('background-color', '#333');
        $('#background-picture').show();
        $('#background-palette').hide();
    } else {
        $('.tab').css('background-color', 'rgba(0,0,0,0)');
        $('#background_palette').css('background-color', '#333');
        $('#background-picture').hide();
        $('#background-palette').show();
    }
}

function borderTab(param) {
    if (param == 'angle') {
        $('.tab').css('background-color', 'rgba(0,0,0,0)');
        $('#border_angle').css('background-color', '#333');
        $('#border-angle').show();
        $('#border-line').hide();
    } else {
        $('.tab').css('background-color', 'rgba(0,0,0,0)');
        $('#border_line').css('background-color', '#333');
        $('#border-angle').hide();
        $('#border-line').show();
    }
}


function effectTab(param) {
    if (param == 'block') {
        $('.tab').css('background-color', 'rgba(0,0,0,0)');
        $('#shadow-block').css('background-color', '#333');
        $('#shadow_block').show();
        $('#shadow_text').hide();
        $('#lighting_block').hide();
    } else if (param == 'lighting') {
        $('.tab').css('background-color', 'rgba(0,0,0,0)');
        $('#lighting-block').css('background-color', '#333');
        $('#shadow_block').hide();
        $('#shadow_text').hide();
        $('#lighting_block').show();
    } else {
        $('.tab').css('background-color', 'rgba(0,0,0,0)');
        $('#shadow-text').css('background-color', '#333');
        $('#shadow_block').hide();
        $('#shadow_text').show();
        $('#lighting_block').hide();
    }
}

function showOrHideWoc(id, id1) {
    if ($('.option_el').first().css('position') == 'relative') {
        if ($('#' + id + '>.woc_content').css('display') == 'none') {
            $('#' + id + '>.woc_content').css('display', 'block');
            $('#' + id + '>.woc_panel>i').attr('class', 'fa fa-angle-double-down');
            if (id == 'woc-layer') {
                reload_right_panel();
            }
        } else {
            $('#' + id + '>.woc_content').css('display', 'none');
            $('#' + id + '>.woc_panel>i').attr('class', 'fa fa-angle-double-right');
        }
    } else {
        $('#' + id).hide();
        $('#' + id1).css('background', 'none');
    }
}

function setTextOption() {
    $('#woc_text').removeClass('disabled');
    $('#text-options').show();
    $('#inactive-text').css('display', 'none');
    $('#padding-all').hide();
    $('#padding-apart').show();
    var font_style = getStyle($('.active'), 'font-size');
    if (font_style.indexOf('px') >= 0) {
        $('#font_s').val(parseInt(font_style));
        $('#font_s').next().text('px');
        $('#panel_font_s').val(parseInt(font_style));
        $('#panel_font_s').next().text('px');
    } else {
        $('#font_s').val(parseFloat(font_style));
        $('#font_s').next().text('%');
        $('#panel_font_s').val(parseFloat(font_style));
        $('#panel_font_s').next().text('%');
    }

    var style = getStyle($('.active'), 'font-family').replace(/'/g, '');
    if (style.indexOf(',') >= 0 || style.indexOf('"') >= 0) {
        style = style.split(',')[0].replace(/"/g, '');
    }
    if ($('.added_fonts').length) {
        var url = '/Cabinet/Editor/Fonts';
        $.ajax({
            url: url,
            dataType: 'html',
            type: 'GET',
            success: function (html) {
                $('#font_family').html(html);
                $('.country_id option').removeAttr('selected');
                $('.country_id').find('[value="' + style + '"]').attr('selected', 'selected');
                $('.country_id').val(style);
            }
        });
    } else {
        $('.country_id option').removeAttr('selected');
        $('.country_id').find('[value="' + style + '"]').attr('selected', 'selected');
        $('.country_id').val(style);
    }

    $('.font').css('background-color', 'rgb(45, 45, 45)');
    if (getStyle($('.active'), 'font-weight') == 'bold') {
        $('#b').css('background-color', 'rgb(81, 81, 81)');
        $('#bb').css('background-color', 'rgb(81, 81, 81)');
    } else {
        $('#b').css('background-color', 'rgb(45, 45, 45)');
        $('#bb').css('background-color', 'rgb(45, 45, 45)');
    }
    if (getStyle($('.active'), 'text-decoration') == 'underline') {
        $('#c').css('background-color', 'rgb(81, 81, 81)');
        $('#cc').css('background-color', 'rgb(81, 81, 81)');
    } else {
        $('#c').css('background-color', 'rgb(45, 45, 45)');
        $('#cc').css('background-color', 'rgb(45, 45, 45)');
    }
    if (getStyle($('.active'), 'font-style') == 'italic') {
        $('#i').css('background-color', 'rgb(81, 81, 81)');
        $('#ii').css('background-color', 'rgb(81, 81, 81)');
    } else {
        $('#i').css('background-color', 'rgb(45, 45, 45)');
        $('#ii').css('background-color', 'rgb(45, 45, 45)');
    }

    var tAlign = 'l';
    var varticalAlign = 't';
    switch (getStyle($('.active'), 'text-align')) {
        case 'left':
            tAlign = 'l';
            $('#textarea_text_element').css('text-align', 'left')
            break;
        case 'center':
            tAlign = 'c';
            $('#textarea_text_element').css('text-align', 'center')
            break;
        case 'right':
            tAlign = 'r';
            $('#textarea_text_element').css('text-align', 'right')
            break;
        case 'justify':
            tAlign = 'j';
            $('#textarea_text_element').css('text-align', 'justify')
            break;
    }

    var valign = $('.active').children().first().css('vertical-align');
    switch (valign) {
        case 'top':
            varticalAlign = 't';
            $('#textarea_text_element').css('vertical-align', 'top')
            break;
        case 'middle':
            varticalAlign = 'c';
            $('#textarea_text_element').css('vertical-align', 'middle')
            break;
        case 'bottom':
            varticalAlign = 'b';
            $('#textarea_text_element').css('vertical-align', 'bottom')
            break;
        default: {
            varticalAlign = 't';
            $('#textarea_text_element').css('vertical-align', 'top')
        }
    }
    if (tAlign == 'j') {
        $('#rfj').css('background-color', 'rgb(81, 81, 81)');
        $('#fj').css('background-color', 'rgb(81, 81, 81)');
        $('#textarea_text_element').css('text-align', 'justify')
        $('#textarea_text_element').css('vertical-align', 'top')
    } else {
        $('#rf' + tAlign + varticalAlign).css('background-color', 'rgb(81, 81, 81)');
        $('#f' + tAlign + varticalAlign).css('background-color', 'rgb(81, 81, 81)');
    }
    $('#panel_lt').attr('class', $('#f' + tAlign + varticalAlign).children().first().attr('class'));
    $('#panel_lt').attr('style', $('#f' + tAlign + varticalAlign).children().first().attr('style'));



    //if (valign == undefined || valign == '' || valign == 'baseline') {
    //    varticalAlign = 't';
    //} else {
    //    $('.font-vert').css('background-color', 'rgb(45, 45, 45)');
    //    $('#v_' + valign).css('background-color', 'rgb(81, 81, 81)');
    //    $('#panel_v_' + valign).css('background-color', 'rgb(81, 81, 81)');
    //}

    $('#letter_s').val(parseFloat(getStyle($('.active'), 'letter-spacing')));
    var line_height = getStyle($('.active'), 'line-height');
    if (line_height.indexOf('px') >= 0) {
        $('#line_h').val(parseInt(line_height));
        $('#line_h').next().text('px');
    } else if (line_height.indexOf('%') >= 0) {
        $('#line_h').val(parseFloat(line_height));
        $('#line_h').next().text('%');
    } else {
        var fs = parseInt($('.active').css('font-size'));
        $('#line_h').val(fs);
        $('#line_h').next().text('px');
        try {
            $('.active').children().first().css('line-height', fs + 'px');
        } catch (ex2) {
            $('.active').css('line-height', fs + 'px');
        }
    }
    var text_indent = getStyle($('.active'), 'text-indent');
    if (text_indent.indexOf('px') >= 0) {
        $('#text_i').val(parseInt(text_indent));
        $('#text_i').next().text('px');
    } else {
        $('#text_i').val(parseFloat(text_indent));
        $('#text_i').next().text('%');
    }
    var padding_left = getStyle($('.active'), 'padding-left');
    var padding_right = getStyle($('.active'), 'padding-right');
    var padding_top = getStyle($('.active'), 'padding-top');
    var padding_bottom = getStyle($('.active'), 'padding-bottom');
    if ($('.active').css('padding-left') == $('.active').css('padding-right')
        && $('.active').css('padding-top') == $('.active').css('padding-right')
        && $('.active').css('padding-top') == $('.active').css('padding-bottom')) {
        if (padding_left.indexOf('%') >= 0) {
            $('#padding_a').next().text('%');
            $('#padding_a').val(Math.abs(parseFloat(padding_left)));
        } else {
            $('#padding_a').next().text('px');
            $('#padding_a').val(Math.abs(parseInt(padding_left)));
        }
        if (padding_top.indexOf('%') >= 0) {
            $('#padding_t').val(parseFloat(padding_top));
            $('#padding_t').next().text('%');
        } else {
            $('#padding_t').val(parseInt(padding_top));
            $('#padding_t').next().text('px');
        }
        if (padding_right.indexOf('%') >= 0) {
            $('#padding_r').val(parseFloat(padding_right));
            $('#padding_r').next().text('%');
        } else {
            $('#padding_r').val(parseInt(padding_right));
            $('#padding_r').next().text('px');
        }
        if (padding_bottom.indexOf('%') >= 0) {
            $('#padding_b').val(parseFloat(padding_bottom));
            $('#padding_b').next().text('%');
        } else {
            $('#padding_b').val(parseInt(padding_bottom));
            $('#padding_b').next().text('px');
        }
        if (padding_left.indexOf('%') >= 0) {
            $('#padding_l').val(parseFloat(padding_left));
            $('#padding_l').next().text('%');
        } else {
            $('#padding_l').val(parseInt(padding_left));
            $('#padding_l').next().text('px');
        }
    } else {
        if (padding_top.indexOf('%') >= 0) {
            $('#padding_t').val(parseFloat(padding_top));
            $('#padding_t').next().text('%');
        } else {
            $('#padding_t').val(parseInt(padding_top));
            $('#padding_t').next().text('px');
        }
        if (padding_right.indexOf('%') >= 0) {
            $('#padding_r').val(parseFloat(padding_right));
            $('#padding_r').next().text('%');
        } else {
            $('#padding_r').val(parseInt(padding_right));
            $('#padding_r').next().text('px');
        }
        if (padding_bottom.indexOf('%') >= 0) {
            $('#padding_b').val(parseFloat(padding_bottom));
            $('#padding_b').next().text('%');
        } else {
            $('#padding_b').val(parseInt(padding_bottom));
            $('#padding_b').next().text('px');
        }
        if (padding_left.indexOf('%') >= 0) {
            $('#padding_l').val(parseFloat(padding_left));
            $('#padding_l').next().text('%');
        } else {
            $('#padding_l').val(parseInt(padding_left));
            $('#padding_l').next().text('px');
        }
    }

    var clr = $('.active').css('color');
    var clr2 = $('.active').children().first().css('color');
    if (clr != undefined && clr != '') {
        $('#edit_color_text>div>div>div').css('background-color', clr);
        $('#panel_edit_color_text>div>div>div').css('background-color', clr);
    }
    if (clr2 != undefined && clr2 != '') {
        $('#edit_color_text>div>div>div').css('background-color', clr2);
        $('#panel_edit_color_text>div>div>div').css('background-color', clr2);
    }

    if ($('.active').attr('type') == 'timer') {
        var clr3 = $('.active>div>div').first().css('color');
        $('#edit_color_text>div>div>div').css('background-color', clr3);
        $('#panel_edit_color_text>div>div>div').css('background-color', clr3);
        $('#g_align').hide();
        $('#line_height_And_letter_spacing').hide();
        $('#line_height').hide();
        $('#padding').hide();
        $('#text_indent').hide();

    } else if ($('.active').attr('type') == 'icon') {
        $('#line_height_And_letter_spacing').hide();
        $('#line_height').hide();
        $('#text_indent').hide();
        $('#padding').hide();
        $('#tracing').hide();

    } else {
        $('#g_align').show();
        $('#line_height_And_letter_spacing').show();
        $('#line_height').show();
        $('#text_indent').show();
        $('#padding').show();
    }
    try {
        if ($('.active').attr('class').indexOf('editable_element') >= 0 && $('.active').parent().attr('class') == 'containerInput') {
            $('.form_option_parametr').show();
        }
    } catch (ex12) {

    }
}

function setBackgroundOption() {
    $('#woc_background').removeClass('disabled');
    $('#background-option').show();
    $('#inactive-background').css('display', 'none');
    try {
        if ($('.active').attr('class').indexOf('section') >= 0) {
            $('#position-background').show();
            if ($('.active').css('background-attachment') == 'fixed') {
                $('#is_fixed input').prop("checked", true);
            } else {
                $('#is_fixed input').prop("checked", false);
            }
        } else {
            $('#position-background').hide();
        }
    } catch (ex) {
        $('#position-background').hide();
    }


    $('.img_align').css('border', '0.5px solid rgba(0,0,0,0)');
    $('.img_align').css('background', 'rgba(0,0,0,0)');
    $('#img_lt').attr('src', '/Areas/Cabinet/Content/images/lt.png');
    $('#img_t').attr('src', '/Areas/Cabinet/Content/images/t.png');
    $('#img_rt').attr('src', '/Areas/Cabinet/Content/images/rt.png');
    $('#img_l').attr('src', '/Areas/Cabinet/Content/images/l.png');
    $('#img_c').attr('src', '/Areas/Cabinet/Content/images/c.png');
    $('#img_r').attr('src', '/Areas/Cabinet/Content/images/r.png');
    $('#img_lb').attr('src', '/Areas/Cabinet/Content/images/lb.png');
    $('#img_b').attr('src', '/Areas/Cabinet/Content/images/b.png');
    $('#img_rb').attr('src', '/Areas/Cabinet/Content/images/rb.png');

    $('#panel_img_lt').attr('src', '/Areas/Cabinet/Content/images/lt.png');
    $('#panel_img_t').attr('src', '/Areas/Cabinet/Content/images/t.png');
    $('#panel_img_rt').attr('src', '/Areas/Cabinet/Content/images/rt.png');
    $('#panel_img_l').attr('src', '/Areas/Cabinet/Content/images/l.png');
    $('#panel_img_c').attr('src', '/Areas/Cabinet/Content/images/c.png');
    $('#panel_img_r').attr('src', '/Areas/Cabinet/Content/images/r.png');
    $('#panel_img_lb').attr('src', '/Areas/Cabinet/Content/images/lb.png');
    $('#panel_img_b').attr('src', '/Areas/Cabinet/Content/images/b.png');
    $('#panel_img_rb').attr('src', '/Areas/Cabinet/Content/images/rb.png');
    switch ($('.active').css('background-position')) {
        case '0% 0%':
            $('#img_lt').parent().css('border', '0.5px solid #428bca');
            $('#img_lt').parent().css('background', '#2d2d2d');
            $('#img_lt').attr('src', '/Areas/Cabinet/Content/images/lth.png');
            $('#panel_img_lt').parent().css('border', '0.5px solid #428bca');
            $('#panel_img_lt').parent().css('background', '#2d2d2d');
            $('#panel_img_lt').attr('src', '/Areas/Cabinet/Content/images/lth.png');
            break;
        case '0% 50%':
            $('#img_t').parent().css('border', '0.5px solid #428bca');
            $('#img_t').parent().css('background', '#2d2d2d');
            $('#img_t').attr('src', '/Areas/Cabinet/Content/images/th.png');
            $('#panel_img_t').parent().css('border', '0.5px solid #428bca');
            $('#panel_img_t').parent().css('background', '#2d2d2d');
            $('#panel_img_t').attr('src', '/Areas/Cabinet/Content/images/th.png');
            break;
        case '0% 100%':
            $('#img_rt').parent().css('border', '0.5px solid #428bca');
            $('#img_rt').parent().css('background', '#2d2d2d');
            $('#img_rt').attr('src', '/Areas/Cabinet/Content/images/rth.png');
            $('#panel_img_rt').parent().css('border', '0.5px solid #428bca');
            $('#panel_img_rt').parent().css('background', '#2d2d2d');
            $('#panel_img_rt').attr('src', '/Areas/Cabinet/Content/images/rth.png');
            break;
        case '50% 0%':
            $('#img_l').parent().css('border', '0.5px solid #428bca');
            $('#img_l').parent().css('background', '#2d2d2d');
            $('#img_l').attr('src', '/Areas/Cabinet/Content/images/lh.png');
            $('#panel_img_l').parent().css('border', '0.5px solid #428bca');
            $('#panel_img_l').parent().css('background', '#2d2d2d');
            $('#panel_img_l').attr('src', '/Areas/Cabinet/Content/images/lh.png');
            break;
        case '50% 50%':
            $('#img_c').parent().css('border', '0.5px solid #428bca');
            $('#img_c').parent().css('background', '#2d2d2d');
            $('#img_c').attr('src', '/Areas/Cabinet/Content/images/ch.png');
            $('#panel_img_c').parent().css('border', '0.5px solid #428bca');
            $('#panel_img_c').parent().css('background', '#2d2d2d');
            $('#panel_img_c').attr('src', '/Areas/Cabinet/Content/images/ch.png');
            break;
        case '50% 100%':
            $('#img_r').parent().css('border', '0.5px solid #428bca');
            $('#img_r').parent().css('background', '#2d2d2d');
            $('#img_r').attr('src', '/Areas/Cabinet/Content/images/rh.png');
            $('#panel_img_r').parent().css('border', '0.5px solid #428bca');
            $('#panel_img_r').parent().css('background', '#2d2d2d');
            $('#panel_img_r').attr('src', '/Areas/Cabinet/Content/images/rh.png');
            break;
        case '100% 0%':
            $('#img_lb').parent().css('border', '0.5px solid #428bca');
            $('#img_lb').parent().css('background', '#2d2d2d');
            $('#img_lb').attr('src', '/Areas/Cabinet/Content/images/lbh.png');
            $('#panel_img_lb').parent().css('border', '0.5px solid #428bca');
            $('#panel_img_lb').parent().css('background', '#2d2d2d');
            $('#panel_img_lb').attr('src', '/Areas/Cabinet/Content/images/lbh.png');
            break;
        case '100% 50%':
            $('#img_b').parent().css('border', '0.5px solid #428bca');
            $('#img_b').parent().css('background', '#2d2d2d');
            $('#img_b').attr('src', '/Areas/Cabinet/Content/images/bh.png');
            $('#panel_img_b').parent().css('border', '0.5px solid #428bca');
            $('#panel_img_b').parent().css('background', '#2d2d2d');
            $('#panel_img_b').attr('src', '/Areas/Cabinet/Content/images/bh.png');
            break;
        case '100% 100%':
            $('#img_rb').parent().css('border', '0.5px solid #428bca');
            $('#img_rb').parent().css('background', '#2d2d2d');
            $('#img_rb').attr('src', '/Areas/Cabinet/Content/images/rbh.png');
            $('#panel_img_rb').parent().css('border', '0.5px solid #428bca');
            $('#panel_img_rb').parent().css('background', '#2d2d2d');
            $('#panel_img_rb').attr('src', '/Areas/Cabinet/Content/images/rbh.png');
            break;
    }
    var repeat = $('.active').css('background-repeat');
    var text = '';
    $('#background_repeat').children().each(function () {
        $(this).removeAttr('selected');
        if ($(this).val() == repeat) {
            $(this).attr('selected', 'selected');
            text = $(this).text();
        }
    });
    $('#background_repeat').next().find('.sbSelector').text(text);
    var size = $('.active').css('background-size');
    var txt = '';
    $('#background_option').children().each(function () {
        $(this).removeAttr('selected');
        if ($(this).val() == size) {
            $(this).attr('selected', 'selected');
            txt = $(this).text();
        }
    });
    $('#background_option').next().find('.sbSelector').text(txt);
    $('#background_position_x_val').val(parseInt($('.active').css('background-position-x')));
    $('#background_position_y_val').val(parseInt($('.active').css('background-position-y')));
    backgroundTypeDefinition($('.active'));
}

function setBorderOption() {
    $('#woc_border').removeClass('disabled');
    $('#border-option').show();
    $('#inactive-borders').css('display', 'none');
    var thicknessTop = parseInt($('.active').css('border-top-width').replace('px', ''));
    var thicknessLeft = parseInt($('.active').css('border-left-width').replace('px', ''));
    var thicknessRight = parseInt($('.active').css('border-right-width').replace('px', ''));
    var thicknessBottom = parseInt($('.active').css('border-bottom-width').replace('px', ''));
    var typeBorderTop = $('.active').css('border-top-style');
    var borderColorTop = $('.active').css('border-top-color');

    if (thicknessTop == thicknessLeft && thicknessLeft == thicknessRight && thicknessRight == thicknessBottom) {
        $('#thickness_border').val(thicknessTop);
        $('#b_top').val(thicknessTop);
        $('#b_left').val(thicknessTop);
        $('#b_right').val(thicknessTop);
        $('#b_bottom').val(thicknessTop);
    } else {
        $('#b_top').val(thicknessTop);
        $('#b_left').val(thicknessLeft);
        $('#b_right').val(thicknessRight);
        $('#b_bottom').val(thicknessBottom);               
    }
    $('#border_color>div>div>div').css('background-color', borderColorTop);
    $('#country_id_2 option:selected').removeAttr('selected');
    $('#country_id_2 option[value=' + typeBorderTop + ']').attr('selected', 'selected');
    $('#country_id_2').val(typeBorderTop);
    
    $('#border_apart').hide();
    $('#border_chain').show();
    if ($('.active').css('border-radius') != undefined) {
        var borderRadius,br;
        br = getStyle($('.active'), 'border-radius');
        if (br == '') {
            br = '0 0 0 0';
        } 
        borderRadius = br.split(' ');
        var b1 = 0, b2 = 0, b3 = 0, b4 = 0;
        if (!$.isNumeric(parseInt(borderRadius[3]))) {
            if (!$.isNumeric(parseInt(borderRadius[2]))) {
                if (!$.isNumeric(parseInt(borderRadius[1]))) {
                    b1 = parseInt(borderRadius[0]);
                    b2 = b1;
                    b3 = b1;
                    b4 = b1;
                } else {
                    b1 = parseInt(borderRadius[0]);
                    b2 = parseInt(borderRadius[1]);
                    b3 = b1;
                    b4 = b2;
                }
            } else {
                b1 = parseInt(borderRadius[0]);
                b2 = parseInt(borderRadius[1]);
                b3 = parseInt(borderRadius[2]);
                b4 = b2;
            }
        } else {
            b1 = parseInt(borderRadius[0]);
            b2 = parseInt(borderRadius[1]);
            b3 = parseInt(borderRadius[2]);
            b4 = parseInt(borderRadius[3]);
        }

        if (b1 == b2 && b2 == b3 && b3 == b4) {
            $('#border_radiusi').val(b1);
            $('#br_t_l').val(b1);
            $('#br_t_r').val(b1);
            $('#br_b_l').val(b1);
            $('#br_b_r').val(b1);
        } else {
            $('#br_t_l').val(b1);
            $('#br_t_r').val(b2);
            $('#br_b_l').val(b4);
            $('#br_b_r').val(b3);
        }
        $('#borders_radius_chain').hide();
        $('#borders_radius_apart').show();
    }
    try {
        if ($('.active').attr('class').indexOf('editable_element') >= 0 && $('.active').parent().attr('class') == 'containerInput') {
            $('.form_option_parametr').show();
        }
    } catch (ex12) {

    }
}

function setEffectOption() {
    $('#woc_effect').removeClass('disabled');
    $('#effect-option').show();
    $('#inactive-shadow').css('display', 'none');
    if ($('.active').attr('type') === 'text' || $('.active').attr('type') === 'button' || $('.active').attr('type') === 'icon' || $('.active').attr('type') === 'timer') {
        $('#shadow_text').show();
        var textShadow = $('.active').children().first().css('text-shadow');
        if (textShadow != 'none') {
            //получение свойства тени текста
            var color = textShadow.split('')[0] + ')';
            if (textShadow.indexOf('rgb') > -1) {
                color = 'rgb' + textShadow.replace('rgb', '$').split('$')[1].split(')')[0] + ')';
                textShadow = textShadow.replace(' ' + color, '').replace(color + ' ', '');
            } else {
                color = 'rgba(255,255,255,0)';
                textShadow = textShadow.replace('transparent ', '').replace(' transparent', '');
            }

            var biasX = parseInt(textShadow.split(' ')[0].replace('px', ''));
            var biasY = parseInt(textShadow.split(' ')[1].replace('px', ''));
            var degradation = parseInt(textShadow.split(' ')[2].replace('px', ''));

            if (isNaN(biasX) || biasX == undefined) {
                biasX = 0;
            }
            if (isNaN(biasY) || biasY == undefined) {
                biasY = 0;
            }
            if (isNaN(degradation) || degradation == undefined) {
                degradation = 0;
            }
            $('.active').children().first().css('text-shadow', color + ' ' + biasX + 'px ' + biasY + 'px ' + degradation + 'px');
            $('#blurrines').val(degradation);
            $('#x').val(biasX);
            $('#y').val(biasY);
            $('#color_shadow>div>div>div').css('background-color', color);

            $('#blurrines_lighting_text').val(degradation);
            $('#text_lightings>div>div>div').css('background-color', color);
        } else {
            $('.active').children().first().css('text-shadow', '0px 0px 0px rgba(255,255,255,0)');
            $('#blurrines').val(0);
            $('#x').val(0);
            $('#y').val(0);
            $('#color_shadow>div>div>div').css('background-color', 'rgba(255, 255, 255, 0)');
        }
    } else {
        $('#shadow_text').hide();
    }
    var boxShadow = $('.active').css('box-shadow');
    if (boxShadow != 'none') {
        var boxColor = boxShadow.split(')')[0] + ')';
        if (boxShadow.indexOf('rgb') > -1) {
            boxColor = 'rgb' + boxShadow.replace('rgb', '$').split('$')[1].split(')')[0] + ')';
            boxShadow = boxShadow.replace(' ' + boxColor, '').replace(boxColor + ' ', '');
        } else {
            boxColor = 'rgba(255,255,255,0)';
            boxShadow = boxShadow.replace('transparent ', '').replace(' transparent', '');
        }

        var xShadow = parseInt(boxShadow.split(' ')[0].replace('px', ''));
        var yShadow = parseInt(boxShadow.split(' ')[1].replace('px', ''));
        var boxDegradation = parseInt(boxShadow.split(' ')[2].replace('px', ''));

        var lighting = '';
        if (boxShadow.indexOf('inset') >= 0) {
            lighting = parseInt(boxShadow.split(')')[1].split(' ')[5]);
        }

        if (isNaN(xShadow) || xShadow == undefined) {
            xShadow = 0;
        }
        if (isNaN(yShadow) || yShadow == undefined) {
            yShadow = 0;
        }
        if (isNaN(boxDegradation) || boxDegradation == undefined) {
            boxDegradation = 0;
        }
        $('.active').css('text-shadow', boxColor + ' ' + xShadow + 'px ' + yShadow + 'px ' + boxDegradation + 'px');
        $('#block_shadows>div>div>div').css('background-color', boxColor);
        $('#blurrines_shadow').val(boxDegradation);
        $('#x_shadow').val(xShadow);
        $('#y_shadow').val(yShadow);
        $('#block_lightings>div>div>div').css('background-color', boxColor);
        $('#blurrines_lighting').val(boxDegradation);
        $('#lighting').val(lighting);
    } else {
        $('.active').css('box-shadow', '0px 0px 0px rgba(255, 255,255,0)');
        $('#block_shadows>div>div>div').css('background-color', 'rgba(255, 255,255,0)');
        $('#blurrines_shadow').val(0);
        $('#x_shadow').val(0);
        $('#y_shadow').val(0);
        $('#block_lightings>div>div>div').css('background-color', 'rgba(255, 255,255,0)');
        $('#blurrines_lighting').val(0);
        $('#lighting').val("");
    }
}

function setUrlOption() {
    $('#woc_url').removeClass('disabled');
    $('#url-option').show();
    $('#inactive-url').css('display', 'none');
    $('#pages').children().each(function () {
        $('#list_pages').html('');
        $('#list_pages').next().find('.sbOptions').html('');
        var optionF = document.createElement('option');
        $(optionF).val('');
        $(optionF).text('-  Выберите  -');
        $('#list_pages').append(optionF);
        if ($(this).attr('type') != 'layout' && $(this).attr('type') != 'litebox') {
            var option = document.createElement('option');
            $(option).val($(this).attr('id'));
            $(option).text($(this).attr('name'));
            $('#list_pages').append(option);
        }

        $('#list_liteboxes').html('');
        $('#list_liteboxes').next().find('.sbOptions').html('');
        var optionL = document.createElement('option');
        $(optionL).val('');
        $(optionL).text('-  Выберите  -');
        $('#list_liteboxes').append(optionL);
        if ($(this).attr('type') == 'litebox') {
            var option1 = document.createElement('option');
            $(option1).val($(this).attr('id').replace("_pag", ''));
            $(option1).text($(this).attr('name'));
            $('#list_liteboxes').append(option1);
        }
    });
    var itemPage = $('.tab_active').parent().attr('rel');

    // очистка старого списка
    $('#list_anchor').html('');

    //создание пустого поля "Выберите"
    var optionF = document.createElement('option');
    $(optionF).val('');
    $(optionF).text('-  Выберите  -');
    $('#list_anchor').append(optionF);

    //поиск якорей и добавление в список
    $('#pages').children().each(function () {
        if ($(this).attr('id') != itemPage && $(this).attr('id') != 'Layout') {
            $(this).find('[type = anchor]').each(function () {
                var option = document.createElement('option');
                $(option).val($(this).children().attr('id'));
                $(option).text($(this).children().attr('name'));
                $('#list_anchor').append(option);
            });
        }
    });

    $('#content_standard').find('[type = anchor]').each(function () {
        var option = document.createElement('option');
        var name = $(this).children().attr('name');
        var id = $(this).attr('id');
        if (id != undefined && name != undefined) {
            $(option).val(id);
            $(option).text(name);
            $('#list_anchor').append(option);
        }
    });

    switch ($('.active>a').attr('type')) {
        case 'web':
            $('#web_url').val($('.active>a').attr('href'));
            if ($('.active>a').attr('href') == '#') {
                $('#web_url').val('');
            }
            $('#web_url').prev().children().first().click();
            break;
        case 'page':
            $('#list_pages').children().each(function () {
                if ($('.active>a').attr('href').replace('#', '') == $(this).val()) {
                    $(this).attr('selected', 'selected');
                }
            });
            $('#list_pages').prev().children().first().click();
            break;
        case 'anchor':
            $('#list_anchor').children().each(function () {
                if ($('.active>a').attr('href').replace('#', '') == $(this).val()) {
                    $(this).attr('selected', 'selected');
                }
            });
            $('#list_anchor').prev().children().first().click();
            break;
        case 'litebox':
            $('#list_liteboxes').children().each(function () {
                if ($('.active>a').attr('href').replace('#', '') == $(this).val()) {
                    $(this).attr('selected', 'selected');
                }
            });
            $('#list_liteboxes').prev().children().first().click();
            break;
        default:

    }
}

function btn_next_rotation() {
    var deg = parseInt($('#rotation_val').val());
    if (deg == 359) {
        $('#rotation_val').val(0);
        deg = 0;
    } else {
        deg += 1;
        $('#rotation_val').val(deg);
    }
    $('#active').width($('.active').width());
    $('#active').height($('.active').height());
    $('.active').css('transform', 'rotate(' + deg + 'deg)');
    $('#active').css('transform', 'rotate(' + deg + 'deg)');
    $('#move_block').css('transform', 'rotate(' + deg + 'deg)');
    $('#move_text').css('transform', 'rotate(' + deg + 'deg)');

    $('#move_text').css('left', $('#move-text').offset().left - $('#workspace').offset().left);
    $('#move_text').css('top', $('#move-text').offset().top - $('#workspace').offset().top - 15);
    //correctPosition();
}

function btn_prev_rotation() {
    var deg = parseInt($('#rotation_val').val());
    if (deg == 0) {
        $('#rotation_val').val(359);
        deg = 359;
    } else {
        deg -= 1;
        $('#rotation_val').val(deg);
    }
    $('#active').width($('.active').width());
    $('#active').height($('.active').height());
    $('.active').css('transform', 'rotate(' + deg + 'deg)');
    $('#active').css('transform', 'rotate(' + deg + 'deg)');
    $('#move_block').css('transform', 'rotate(' + deg + 'deg)');
    $('#move_text').css('transform', 'rotate(' + deg + 'deg)');
    //correctPosition();
}

function checkPositionCheckbox() {
    $('.positionSide>input').prop('checked', false);
    if (parseInt($('.active').css('left')) == 0) {
        $('#positionSideLeft>input').prop('checked', true);
    }
    var right = parseInt(getStyle($('.active'), 'right'));
    if (right == 0) {
        $('#positionSideRight>input').prop('checked', true);
    }
    var posLeft = getStyle($('.active'), 'left');
    var posWight = getStyle($('.active'), 'width');
    if (posLeft.indexOf('%') > -1 && posWight.indexOf('%') > -1 && parseInt(posLeft) + parseInt(posWight) == 100) {
        $('#positionSideRight>input').prop('checked', true);
    }
    correctPosition();
}
function setSizeOption() {
    $('#woc_param').removeClass('disabled');
    $('#size-option').show();
    $('#inactive-size').css('display', 'none');
    $('#positionElement').hide();
    $('#rotate').hide();

    if ($('.active').attr('type') == 'sectionLitebox') {
        $('#max_width').hide();
    } else {
        $('#max_width').show();
    }

    var width = getStyle($('.active'), 'width');
    if ($('.active').attr('class').indexOf('editable_element') >= 0) {
        if ($('.active').attr('type') == 'submit' || $('.active').attr('type') == 'title') {
            width = getStyle($('.active'), 'width');
        } else {
            width = getStyle($('.active').parent(), 'width');
        }
    }
    var height = 0;
    try {
        height = getStyle($('.active'), 'height');
    } catch (ex) {
        height = $('.active').closest('column').css('height');
    }

    if ($('.active').attr('type') == 'btn' || $('.active').attr('type') == 'sectionLitebox') {
        width = $('.active').width() + 'px';
        height = $('.active').height() + 'px';
    }

    var top = '';
    var left = '';
    if ($('.active').attr('type') == 'btn') {
        top = parseInt($('.active').attr('top')) + 'px';
        $('.active').attr('top', top);
        left = parseInt($('.active').attr('left')) + 'px';
        $('.active').attr('left', left);

    } else {
        top = getStyle($('.active'), 'top');
        left = getStyle($('.active'), 'left');
    }
    if ($('.active').attr('class').indexOf('section') < 0) {
        $('#positionSide').show();
        if (width.indexOf('%') >= 0) {
            $('#section_width_val').next().text('%');
            $('#panel_section_width_val').next().text('%');
        } else {
            $('#section_width_val').next().text('px');
            $('#panel_section_width_val').next().text('px');
        }
        $('#section_width_val').val(parseFloat(width));
        $('#panel_section_width_val').val(parseFloat(width));

        if (height.indexOf('%') >= 0) {
            $('#section_height_val').next().text('%');
            $('#panel_section_height_val').next().text('%');
        } else {
            $('#section_height_val').next().text('px');
            $('#panel_section_height_val').next().text('px');
        }

        $('#section_height_val').val(parseFloat(height));
        $('#panel_section_height_val').val(parseFloat(height));

        if (top.indexOf('%') >= 0) {
            $('#section_top_val').next().text('%');
        } else {
            $('#section_top_val').next().text('px');
        }
        $('#section_top_val').val(parseFloat(top));

        if (left.indexOf('%') >= 0) {
            $('#section_left_val').next().text('%');
        } else {
            $('#section_left_val').next().text('px');
        }
        $('#section_left_val').val(parseFloat(left));
        checkPositionCheckbox();
    } else {
        $('#positionSide').hide();
        var temp = '';
        var widthA = 0;
        var widthC = 0;
        try {
            widthA = parseFloat(getStyle($('.active'), 'width'));

            if ($('.active').attr('type') == 'sectionLitebox') {
                if (temp.indexOf('%') >= 0) {
                    $('#section_width_val').next().text('%');
                    $('#panel_section_width_val').next().text('%');
                } else {
                    $('#section_width_val').next().text('px');
                    $('#panel_section_width_val').next().text('px');
                }
            }

            widthC = parseFloat(getStyle($('.active>div'), 'width'));

            if (widthA > widthC) {
                $('#section_width_val').val(widthC);
                $('#panel_section_width_val').val(widthC);
                $('#max_width input').prop("checked", true);
            } else {
                $('#section_width_val').val(widthA);
                $('#panel_section_width_val').val(widthA);
                $('#max_width input').prop("checked", false);
            }

        } catch (ex) {
            if ((parseFloat($('.active').css('width')) / parseFloat($('.active>div').css('width'))) * 100 > 100) {
                $('#section_width_val').val(parseFloat($('.active>div').css('width')) / (parseFloat($('.active').css('width'))) * 100);
                $('#panel_section_width_val').val(parseFloat($('.active>div').css('width')) / (parseFloat($('.active').css('width'))) * 100);
            } else {
                $('#section_width_val').val((parseFloat($('.active').css('width')) / parseFloat($('.active>div').css('width'))) * 100);
                $('#panel_section_width_val').val((parseFloat($('.active').css('width')) / parseFloat($('.active>div').css('width'))) * 100);
            }
        }
        $('#section_height_val').val(parseFloat($('.active').css('height')));
        $('#panel_section_height_val').val(parseFloat($('.active').css('height')));

    }

    $('#section_width').removeClass('inactive');
    $('#section_width').children().each(function () {
        $(this).removeClass('inactive');
    });
    $('#panel_section_width').removeClass('inactive');
    $('#panel_section_width').children().each(function () {
        $(this).removeClass('inactive');
    });

    if ($('.active').attr('class').indexOf('section') < 0) {
        $('#section_width_val').removeAttr('disabled');
        $('#section_width').removeClass('inactive');
        $('#section_width').children().each(function () {
            $(this).removeClass('inactive');
        });
        $('#panel_section_width_val').removeAttr('disabled');
        $('#panel_section_width').removeClass('inactive');
        $('#panel_section_width').children().each(function () {
            $(this).removeClass('inactive');
        });

        if ((($('.active').is('input') || $('.active').is('textarea')) && $('.active').attr('class').indexOf('editable_element') >= 0) || ($('.active').attr('class').indexOf('containerForm') >= 0)) {
            if ($('.active').attr('class').indexOf('containerForm') >= 0) {
                $('#section_width_val').val($('.active').parent().width());
                $('#section_width_val').next().text('px');
                $('#section_height_val').val($('.active').parent().height());
                $('#section_height_val').next().text('px');
                $('#panel_section_width_val').val($('.active').parent().width());
                $('#panel_section_width_val').next().text('px');
                $('#panel_section_height_val').val($('.active').parent().height());
                $('#panel_section_height_val').next().text('px');
            }
            $('#positionElement').hide();
            $('#rotate').hide();
            $('#max_width').hide();
        } else {
            $('#positionElement').show();
            $('#rotate').show();
            var transform = getStyle($('.active'), 'transform');
            var rotate = 0;
            if (transform != undefined && transform != 'none') {
                rotate = parseInt(transform.replace('rotate(', '').replace('deg)', ''));
            }
            $('#rotation_val').val(rotate);
        }
    } else {
        $('#section_width_val').attr('disabled', 'disabled');
        $('#section_width').addClass('inactive');
        $('#section_width').children().each(function () {
            $(this).addClass('inactive');
        });
        $('#panel_section_width_val').attr('disabled', 'disabled');
        $('#panel_section_width').addClass('inactive');
        $('#panel_section_width').children().each(function () {
            $(this).addClass('inactive');
        });
        $('#positionElement').hide();
        $('#rotate').hide();
    }
    try {
        if ($('.active').attr('class').indexOf('editable_element') >= 0 && $('.active').parent().attr('class') == 'containerInput') {
            $('.form_option_parametr').show();
        }
    } catch (ex12) {

    }
}

function setAnimationOption() {
    $('#woc_animate').removeClass('disabled');
    $('#animation-option').show();
    $('#inactive-animate').css('display', 'none');
    $('#delay-animate').hide();
    $('#duration-animate').hide();
    $('#appearance-animate').hide();

    var animate = $('.active').attr('animate');
    if (animate != '' && animate != undefined) {
        var arrayAnimate = animate.split(' ');
        if (arrayAnimate.lenght == 3) {
            if (arrayAnimate[1] == 'glade' || arrayAnimate[1] == 'fade'
                || arrayAnimate[1] == 'space' || arrayAnimate[1] == 'bounce' || arrayAnimate[1] == 'bar') {
                $('#delay-animate').show();
                $('#delay_animate').children().each(function () {
                    if ($(this).val() != arrayAnimate[2]) {
                        $(this).prop('selected', false);
                    } else {
                        $(this).prop('selected', true);
                    }
                });
            } else if (arrayAnimate[1] == 'flash' || arrayAnimate[1] == 'swing'
                || arrayAnimate[1] == 'pulse' || arrayAnimate[1] == 'rotate') {
                $('#duration-animate').show();
                $('#duration_animate').children().each(function () {
                    if ($(this).val() != arrayAnimate[2]) {
                        $(this).prop('selected', false);
                    } else {
                        $(this).prop('selected', true);
                    }
                });
            } else {
                $('#appearance-animate').show();
                $('#appearance_animate').children().each(function () {
                    if (arrayAnimate[1].indexOf($(this).val()) < 0) {
                        $(this).prop('selected', false);
                    } else {
                        $(this).prop('selected', true);
                    }
                });
            }
        }
        $('#select_animate').children().each(function () {
            if ($(this).val() != arrayAnimate[1]) {
                $(this).prop('selected', false);
            } else {
                $(this).prop('selected', true);
            }
        });
    } else {
        $('#select_animate').children().each(function () {
            if ($(this).val() == '') {
                $(this).prop('selected', true);
            } else {
                $(this).prop('selected', false);
            }
        });
    }
    if ($('.active').attr('type') == 'sectionLitebox') {
        $('#select-animate').hide();
        $('#delay-animate').hide();
        $('#duration-animate').hide();
        $('#appearance-animate').hide();
        $('#preview-animate').hide();
        $('#litebox-animate').show();
        var aminValue = $('.active').attr('w-animate');
        $('#litebox_animate').val('');
        $('#litebox_animate').next().find('.sbSelector').text(' - без анимации - ');
        if (aminValue == 'showslow' || aminValue == 'slidedownslow' || aminValue == 'fadeinslow') {
            $('#litebox_animate').children().each(function () {
                if ($(this).attr('value') == aminValue) {
                    $(this).attr('selected', 'selected');
                    $('#litebox_animate').next().find('.sbSelector').text($(this).text());
                } else {
                    $(this).removeAttr('selected');
                }
            });
        }

    }
}

function setButtonOption() {
    $('#woc_button').removeClass('disabled');
    $('#button-option').show();
    $('#inactive-button').css('display', 'none');
    $('.button_op').css('background', 'rgba(0,0,0,0)');
    $('#button_standard').hide();
    $('#button_hover').hide();
    $('#button_active').hide();
    $('#tranform_time').hide();
    $('#image_hover').hide();
    if ($('.active').attr('type') != 'image') {
        $('#button_standard').show();
        $('#button_hover').show();
        $('#button_active').show();
        $('#tranform_time').show();
        var option = $('.active').attr('option');
        if (option == undefined) {
            $('.active').attr('option', 'standard')
            $('#button_standard').css('background', 'rgba(182, 160, 243, 0.1)');
        } else {
            $('#button_' + option).css('background', 'rgba(182, 160, 243, 0.1)');
        }

        var idButton = $('.active').attr('id');
        if ($('.edit_form').length && $('.active').parent().attr('type') == 'submit') {
            idButton = $('.active').parent().attr('id');
            option = $('.active').parent().attr('option');

            hideTextarea();
            $('.active').removeClass('active');
            $('#' + idButton).addClass('active');
            setButtonStyles(idButton, option);
        } else {
            setButtonStyles(idButton, option);
        }

        //$('#woc-button').show();
        useButtonOption();
    } else {
        $('#opacity_normal_val').val((parseInt($('.active').css('opacity')) * 100).toFixed(0));
        readCssImage();
        $('#image_hover').show();
        $('#tranform_time').show();
    }
}

function textAlign(parametr, el) {
    console.log('выравниваем');
    $('.font').css('background-color', '#2d2d2d');
    $('.active').css('display', 'table');
    if ($('.active').is('label') && $('.active').parent().attr('type') == 'title') {
        $('.active').parent().css('display', 'table');
        $('.active').css('display', 'table-cell');
    } else {
        $('.active').children().first().css('display', 'table-cell');
    }

    $('#panel_lt').attr('class', $(el).children().first().attr('class'));
    $('#panel_lt').attr('style', $(el).children().first().attr('style'));

    var gorisontal = '';
    var vertical = '';
    switch (parametr) {
        case 'flt':
            gorisontal = 'left';
            vertical = 'top';
            break;
        case 'fct':
            gorisontal = 'center';
            vertical = 'top';
            break;
        case 'frt':
            gorisontal = 'right';
            vertical = 'top';
            break;
        case 'flc':
            gorisontal = 'left';
            vertical = 'middle';
            break;
        case 'fcc':
            gorisontal = 'center';
            vertical = 'middle';
            break;
        case 'frc':
            gorisontal = 'right';
            vertical = 'middle';
            break;
        case 'flb':
            gorisontal = 'left';
            vertical = 'bottom';
            break;
        case 'fcb':
            gorisontal = 'center';
            vertical = 'bottom';
            break;
        case 'fbb':
            gorisontal = 'right';
            vertical = 'bottom';
            break;
        case 'fj':
            gorisontal = 'justify';
            vertical = 'top';
            $('#panel_lt').attr('style', 'padding-top:5px');
            break;
    }
    console.log('vertical=' + vertical);
    $('#' + parametr).css('background-color', '#515151');
    $('#r' + parametr).css('background-color', '#515151');
    if ($('.active').is('label') && $('.active').parent().attr('type') == 'title') {
        $('.active').css('text-align', gorisontal);
        $('.active').css('vertical-align', vertical);
    } else {
        $('.active').css('text-align', gorisontal);
        $('.active').css('vertical-align', vertical);
        $('.active').children().css('text-align', gorisontal);
        $('.active').children().css('vertical-align', vertical);
    }


    $('#textarea_text_element').css('text-align', gorisontal);
    $('#textarea_text_element').css('vertical-align', vertical);
    groupProperty('text-align', gorisontal);
    groupProperty('vertical-align', vertical);
    saveStep();

}

function positionSide(position) {
    $('.active').css('position', 'absolute');
    var thisParentId = $('.active').parent().attr('id');
    if (position == 'left') {
        var needParentId = $('.active').parent().parent().children().first().attr('id');
        if (thisParentId != needParentId && needParentId != undefined) {
            var active = $('.active');
            $('.active').remove();
            $('#' + needParentId).append(active);
        }
        $('.active').css('left', 0);
        $('.active').css('right', '');
        $('#section_left_val').val(0);
    } else {
        var needParentId = $('.active').parent().parent().children().last().attr('id');
        if (thisParentId != needParentId && needParentId != undefined) {
            var active = $('.active');
            $('.active').remove();
            $('#' + needParentId).append(active);
        }
        $('.active').css('right', 0);
        $('.active').css('left', '');
        $('#section_left_val').val('-');
    }
    correctPosition();
}

function correctPositionMoveText() {
    var left = parseInt($('#active').css('left')) + $('.active').width() + parseInt($('.active').css('border-left')) + parseInt($('.active').css('border-right'));
    var top = parseInt($('#active').css('top'));
    var transform = getStyle($('.active'), 'transform');

    $('#move_text').css('left', $('#move-text').offset().left - $('#workspace').offset().left);
    $('#move_text').css('top', $('#move-text').offset().top - $('#workspace').offset().top - 15);
}

function saveCircularTimerOption() {
    var time = $('#circular_data_time').val();
    var arrayDate = time.split(' ')[0].split('-');
    var arrayTime = "";
    try {
        arrayTime = time.split(' ')[1].split(':');
    } catch (ex) {
        var thisTime = new Date((new Date())).getHours() + ':' + new Date((new Date())).getMinutes() + ':' + new Date((new Date())).getSeconds();
        arrayTime = thisTime.split(':');
    }

    $('.active>div>div').attr('data-date', arrayDate[2] + '-' + arrayDate[1] + '-' + arrayDate[0] + ' ' + arrayTime[0] + ':' + arrayTime[1] + ':' + arrayTime[2]);
    //$('#data_time').val(data.getDate() + '-' + (data.getMonth() + 1) + '-' + data.getFullYear() + ' ' + data.getHours() + ':' + data.getMinutes());

    var animation = $('#circle_animation').val();
    var bgWidth = $('#circles_width_background_val').val();
    if (bgWidth == '') {
        bgWidth = 1;
    }
    //изменение размеров кругов
    //$('.active .time_circles>div').css('width', 70 * bgWidth + 'px');
    //$('.active .time_circles>div').css('height', 70 * bgWidth + 'px');
    //$('.active').css('width', (4 * 70 * bgWidth + 3 * 35 * bgWidth) + 'px');
    //$('.active').css('height', (70 * bgWidth + 30) + 'px');
    var index = 0;
    $('.active .time_circles>div').each(function () {
        $(this).css('left', (index * (35 * bgWidth + 70 * bgWidth)));
        index++;
    });

    var fgWidth = $('#circles_width_foreground_val').val();
    if (fgWidth == '') {
        fgWidth = 0.06;
    }
    $('.active .time_circles>div').css('border-width', 116 * fgWidth + 'px');
    var circleBgColor = $('#circular_timer_background_color>div>div>div').css('background-color');
    var textDays = $('#days_text').val();
    var textDaysCircleBgColor = $('#circular_timer_background_color_day>div>div>div').css('background-color');
    var textHours = $('#hours_text').val();
    var textHoursCircleBgColor = $('#circular_timer_background_color_hour>div>div>div').css('background-color');
    var textMinutes = $('#minutes_text').val();
    var textMinutesCircleBgColor = $('#circular_timer_background_color_minute>div>div>div').css('background-color');
    var textSeconds = $('#seconds_text').val();
    var textSecondsCircleBgColor = $('#circular_timer_background_color_second>div>div>div').css('background-color');
    var id = $('.active').attr('id');
    var countDownId = $('.active>div>div').attr('id');
    var script = '/*' + id + ', lastload*/ try{$("#' +
        countDownId +
        '").TimeCircles({"animation": "' +
        animation +
        '", "bg_width": ' +
        bgWidth +
        ', "fg_width": ' +
        fgWidth +
        ', "circle_bg_color": "' +
        circleBgColor +
        '", "time": {"Days": {"text": "' +
        textDays +
        '","color": "' +
        textDaysCircleBgColor +
        '","show": true}, ' +
        '"Hours": {"text": "' +
        textHours +
        '", "color": "' +
        textHoursCircleBgColor +
        '", "show": true}, ' +
        '"Minutes": { "text": "' +
        textMinutes +
        '", "color": "' +
        textMinutesCircleBgColor +
        '", "show": true}, ' +
        '"Seconds": { "text": "' +
        textSeconds +
        '", "color": "' +
        textSecondsCircleBgColor +
        '", "show": true}}});}catch(ex){console.log(ex)}';
    $('#scripts').find('[rel = "' + id + '"]').text(script);
    saveStep();
    hideLitebox();
}

var replceStringWidget = "";
function saveWidgetOption() {
    //widget тут событие при нажатии кнопки Сохранить
    replceStringWidget = "";
    var idElm = $('#widget_option_content').attr('data-id');

    $("#widget_option_content div").each(function (index) {
        if ($(this).attr('type') === "info") {
            replceStringWidget += takeInfoParam($(this));
        }
        if ($(this).attr('type') === "bool") {
            replceStringWidget += takeBoolParam($(this));
        }
        if ($(this).attr('type') === "builtin") {
            replceStringWidget += takeBuiltinParam($(this));
        }
        if ($(this).attr('type') === "list") {
            replceStringWidget += takeListParam($(this));
        }
        if ($(this).attr('type') === "url") {
            replceStringWidget += takeUrlParam($(this));
        }
    });

    $.post('/Cabinet/Widget/GetCodeWidget', { id: idElm, text: replceStringWidget }, function (data) {
        $('#scripts div').html(data.script);
        $('#widget_' + idElm).css('display', 'block');
        $('#widget_' + idElm + ' .widget_title').html($('<div>' + data.html + '</div>'));
        $('#widget_option_content').html('');
        hideLitebox();
    });
}

function takeListParam(elm) {
    var nameParam = elm.attr('name');
    var text = "";
    var e = document.getElementById("ddlViewBy");
    var text = $(e.options[e.selectedIndex]).attr('val');
    var currentString = "list" + "|" + nameParam + "|" + text + "||";
    return currentString;
}
function takeBuiltinParam(elm) {
    var nameParam = elm.attr('name');
    var text = "";
    text = elm.find('input').val();
    var currentString = "builtin" + "|" + nameParam + "|" + text + "||";
    return currentString;
}
function takeUrlParam(elm) {
    var nameParam = elm.attr('name');
    var text = "";
    var url = "";

    text = elm.find('.urlText').val();
    url = elm.find('.urlValue').val();

    var currentString = "url" + "|" + nameParam + "|" + text + "|" + url + "||";
    return currentString;
}
function takeBoolParam(elm) {
    var nameParam = elm.attr('name');
    var text = "";
    if (elm.find('input').prop('checked')) {
        text = elm.find('input').attr('valueTrue');
    }
    else {
        text = elm.find('input').attr('valueFalse');
    }
    var currentString = "bool" + "|" + nameParam + "|" + text + "||";
    return currentString;
}
function takeInfoParam(elm) {
    var nameParam = elm.attr('name');
    var text = elm.find('.boolLabel').val();
    var currentString = "info" + "|" + nameParam + "|" + text + "||";
    return currentString;
}

function btn_prev_opacity_hover() {
    var val = parseInt($('#opacity_hover_val').val());
    if (val > 0) {
        val = val * 1 - 1;
        $('#opacity_hover_val').val(val);
        saveCssImage();
    }
}

function btn_next_opacity_hover() {
    var val = parseInt($('#opacity_hover_val').val());
    if (val < 100) {
        val = val * 1 + 1;
        $('#opacity_hover_val').val(val);
        saveCssImage();
    }
}

function btn_prev_opacity_normal() {
    var val = parseInt($('#opacity_normal_val').val());
    if (val > 0) {
        val = val * 1 - 1;
        $('#opacity_normal_val').val(val);
        $('.active').css('opacity', val / 100);
    }
}

function btn_next_opacity_normal() {
    var val = parseInt($('#opacity_normal_val').val());
    if (val < 100) {
        val = val * 1 + 1;
        $('#opacity_normal_val').val(val);
        $('.active').css('opacity', val / 100);
    }
}

function btn_prev_zoom() {
    var val = parseInt($('#zoom_val').val());
    if (val > 0) {
        val = val * 1 - 1;
        $('#zoom_val').val(val);
        saveCssImage();
    }
}

function btn_next_zoom() {
    var val = parseInt($('#zoom_val').val());
    if (val < 200) {
        val = val * 1 + 1;
        $('#zoom_val').val(val);
        saveCssImage();
    }
}

function saveCssImage() {
    var id = $('.active').attr('id');
    var zoom = parseInt($('#zoom_val').val());
    var opacity = parseInt($('#opacity_hover_val').val());
    var transform = $('#tranform_val').val();
    if ($('#styles').find('p[rel = ' + id + ']').attr('rel') != undefined) {
        $('#styles').find('p[rel = ' + id + ']').text('#' + id + ' img:hover{ max-width:' + zoom + '%; opacity:' + opacity / 100 + '; transition:' + transform + 's; -webkit-transition:' + transform + 's; -moz-transition:' + transform + 's; -o-transition:' + transform + 's;}#' + id + ' img{width: 100%; max-width:' + zoom + '%; transition:' + transform + 's; -webkit-transition:' + transform + 's; -moz-transition:' + transform + 's; -o-transition:' + transform + 's;}');
    } else {
        var p = document.createElement('p');
        $(p).attr('rel', id);
        $(p).text('#' + id + ' img:hover{ max-width:' + zoom + '%; opacity:' + opacity / 100 + '; transition:' + transform + 's; -webkit-transition:' + transform + 's; -moz-transition:' + transform + 's; -o-transition:' + transform + 's;}#' + id + ' img{width: 100%; max-width:' + zoom + '%; transition:' + transform + 's; -webkit-transition:' + transform + 's; -moz-transition:' + transform + 's; -o-transition:' + transform + 's;}');
        $('#styles').append(p);
    }
}

function readCssImage() {
    var id = $('.active').attr('id');
    var styles = $('#styles').find('p[rel = ' + id + ']').text();
    var zoom = '';
    var opacity = '';
    var transform = '';

    if (styles != undefined && styles != '') {
        try {
            var tStyle = styles.split('{')[1].split('}')[0].split(';');
            for (var i = 0; i < tStyle.length - 1; i++) {
                var par = tStyle[i].split(':')[0];
                var val = tStyle[i].split(':')[1];
                switch (par.replace(' ', '')) {
                    case 'opacity':
                        opacity = parseFloat(val.replace(' ', '')).toFixed(1) * 100;
                        break;
                    case 'transition':
                        transform = parseFloat(val.replace(' ', '')).toFixed(1);
                        break;
                    case ' max-width':
                        zoom = ((parseInt(val) / $('.active').width()) * 100).toFixed(0);
                        break;
                    case 'height':
                        zoom = ((parseInt(val) / $('.active').height()) * 100).toFixed(0);
                        break;
                }
            }
        } catch (ex) {
            console.log(ex);
            zoom = '100';
            opacity = '100';
            transform = '0';
        }
    } else {
        zoom = '100';
        opacity = '100';
        transform = '0';
    }
    $('#zoom_val').val(zoom);
    $('#opacity_hover_val').val(opacity);
    $('#tranform_val').val(transform);
}

function hideTextarea() {
    if ($(".active").attr('type') == 'text') {
        $('#textarea_text_element').hide();
        $('.active>a').css('opacity', 1);
    }
}
function correctSizeTextElement() {
    var height = getStyle($('.active'), 'height');
    var width = getStyle($('.active'), 'width');
    
    $('#textarea_text_element').css('color', $('.active').css('color'));
    $('#textarea_text_element').css('font-family', $('.active').css('font-family'));
    $('#textarea_text_element').css('font-size', $('.active').css('font-size'));
    $('#textarea_text_element').css('background', 'rgba(0,0,0,0)');
    $('#textarea_text_element').css('border', 'none');
    $('#textarea_text_element').css('margin-left', $('.active').css('border-left-width'));
    $('#textarea_text_element').css('margin-top', $('.active').css('border-top-width'));
    $('#textarea_text_element').css('margin-right', $('.active').css('border-right-width'));
    $('#textarea_text_element').css('margin-bottom', $('.active').css('border-bottom-width'));
    $('#textarea_text_element').css('padding', $('.active').css('padding'));
    $('#textarea_text_element').css('line-height', $('.active').css('line-height'));
    $('#textarea_text_element').width($('.active>a').width() + 1);
    $('#textarea_text_element').height($('.active>a').height());
        
    correctEditText();
}

function correctEditText() {
    var height = getStyle($('.active'), 'height');
    if (height.indexOf('%') > -1) {
        $('.active').css('height', ((($('#textarea_text_element').height() + parseInt($('.active').css('border-top-width')) + parseInt($('.active').css('border-bottom-width')) + parseInt($('.active').css('padding-top')) + parseInt($('.active').css('padding-bottom'))) / $('.active').parent().height()) * 100) + '%');
    } else {
        $('.active').css('height', ($('#textarea_text_element').height() + parseInt($('.active').css('border-bottom-width')) + parseInt($('.active').css('border-top-width')) + parseInt($('.active').css('padding-top')) + parseInt($('.active').css('padding-bottom'))) + 'px');
    }
    var width = getStyle($('.active'), 'width');
    if (width.indexOf('%') > -1) {
        $('.active').css('width', ((($('#textarea_text_element').width() + parseInt($('.active').css('border-left-width')) + parseInt($('.active').css('border-right-width')) + parseInt($('.active').css('padding-left')) + parseInt($('.active').css('padding-right'))) / $('.active').parent().width()) * 100) + '%');
    } else {
        $('.active').css('width', ($('#textarea_text_element').width() + parseInt($('.active').css('border-left-width')) + parseInt($('.active').css('border-right-width')) + parseInt($('.active').css('padding-left')) + parseInt($('.active').css('padding-right'))) + 'px');
    }

    $('#resize').height($('.active').height() + parseInt($('.active').css('border-top-width')) + parseInt($('.active').css('border-bottom-width')) + parseInt($('.active').css('padding-top')) + parseInt($('.active').css('padding-bottom')));
    $('#resize').width($('.active').width() + parseInt($('.active').css('border-left-width')) + parseInt($('.active').css('border-right-width')) + parseInt($('.active').css('padding-left')) + parseInt($('.active').css('padding-right')));
    $('#move_text').css('left', $('#move-text').offset().left - $('#workspace').offset().left);
    $('#move_text').css('top', $('#move-text').offset().top - $('#workspace').offset().top - 15);
    $('#option_element').css('left', $('#resize').width() + 3);
    $('#remove_element').css('left', $('#resize').width() + 3);
}

function createCopy() {
    console.log('создаём копию элемента');
    // создаём копию элемента
    var id = $('.active').attr('id');
    var elementParentId = $('.active').parent().attr('id');
    var element = $('.active').clone().removeClass('active').appendTo("#" + elementParentId);
    var top = getStyle($(element), 'top');
    var left = getStyle($(element), 'left');
    var zindex = getStyle($(element), 'z-index');
    $(element).css('z-index', parseInt(zindex) + 1);

    if (top.indexOf('%') > -1) {
        $(element).css('top', (parseInt(top) * 1 + ((15 / (parseInt(getStyle($(element).parent(), 'height')))) * 100)).toFixed(0) + '%');
    } else {
        $(element).css('top', (parseInt(top) * 1 + 15) + 'px');        
    }

    if (left.indexOf('%') > -1) {
        $(element).css('left', (parseInt(left) * 1 + ((15 / (parseInt(getStyle($(element).parent(), 'width')))) * 100)).toFixed(0) + '%');
    } else {
        $(element).css('left', (parseInt(left) * 1 + 15) + 'px');
    }

    setNewID(id, 'inactive')
    $('#contextMenu').hide();
}

function zindexUp() {
    var zindex = parseInt($('.active').css('z-index'));
    $('.active').parent().children().each(function () {
        if (parseInt($(this).css('z-index')) == zindex * 1 + 1) {
            $(this).css('z-index', zindex);
        }
    });
    $('.active').css('z-index', zindex * 1 + 1);
}

function zindexDown() {    
    if (parseInt($('.active').css('z-index')) > 1) {
        var zindex = parseInt($('.active').css('z-index'));
        $('.active').parent().children().each(function () {
            if (parseInt($(this).css('z-index')) == zindex * 1 - 1) {
                $(this).css('z-index', zindex);
            }
        });
        $('.active').css('z-index', zindex * 1 - 1);
    }    
}

function zindexUpEnd() {
    var zindex = parseInt($('.active').css('z-index'));
    var countElement = $('.active').parent().children().length;
    $('.active').parent().children().each(function () {
        if (parseInt($(this).css('z-index')) > zindex && parseInt($(this).css('z-index')) <= countElement) {
            $(this).css('z-index', parseInt($(this).css('z-index'))*1-1);
        }
    });    
    $('.active').css('z-index', countElement+1);
}

function zindexDownEnd() {
    var zindex = parseInt($('.active').css('z-index'));
    $('.active').parent().children().each(function () {
        if (parseInt($(this).css('z-index')) >= 1 && parseInt($(this).css('z-index')) < zindex) {
            $(this).css('z-index', parseInt($(this).css('z-index')) * 1 + 1);
        }
    });
    $('.active').css('z-index', 1);
}