﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using DomainModel.Content;
using Interfaces.Content;
using WebSite.Areas.Cabinet.Models;

namespace WebSite.Areas.Cabinet.Controllers
{
    [Authorize]
    public class HomeController : CabinetBaseController
    {
        private readonly ILandingService _landingService;
        public HomeController(ILandingService landingService)
        {
            _landingService = landingService;
        }
        public ActionResult Index()
        {
            //var viewModel = "Пакет: ";
            //var userId = User.Identity.GetUserId();
            //var paidInvoice = _invoiceService.GetByUserPaid(userId);
            //if (paidInvoice.Any())
            //{
            //    var lastPayedInvoice = paidInvoice.OrderByDescending(_=>_.Id).First();
            //    viewModel = viewModel + lastPayedInvoice.Tariff.Name + " до " + lastPayedInvoice.Debit.Date.Value.AddMonths(paidInvoice.Last().Tariff.Period).ToString(new CultureInfo("ru-RU"));
            //}
            //else
            //{
            //    viewModel = viewModel + "ТЕСТОВЫЙ";
            //}'
            
            var models = new List<ListLandingViewModel>();
            var user = Membership.GetUser();
            if (user != null)
            {
                var userId = user.ProviderUserKey;
                if (userId != null)
                {
                    var pages = _landingService.GetByUser(Int32.Parse(userId.ToString()));
                    var groupPages = pages.GroupBy(_ => _.Url);
                    foreach (var item in groupPages)
                    {
                        var model = new ListLandingViewModel();
                        model.Url = item.Key;
                        model.Landings = pages.Select(LandingViewModel.FromDomainModel).Where(_=>_.Url == item.Key).ToList();
                        model.IsTemplate = pages.FirstOrDefault(_ => _.Url == item.Key).IsTemplate;
                        models.Add(model);
                    }
                    //model = pages.Select(LandingViewModel.FromDomainModel).ToList();
                }
            }
            return View(models);
        }

        [HttpPost]
        public bool LandingIsExist(string url)
        {
            var user = Membership.GetUser();
            if (user != null && user.ProviderUserKey != null)
            {
                    var landing = _landingService.GetByUrlAndUserId(url, Int32.Parse(user.ProviderUserKey.ToString()));
                    if (landing != null && landing.Count > 0)
                    {
                        return true;
                    }
            }
            return false;
        }

        [Route("~/Account")]
        public ViewResult Account()
        {
            return View();
        }

        [Route("~/Partners")]
        public ViewResult Partners()
        {
            return View();
        }

        [Route("~/Tariffs")]
        public ActionResult Tariffs()
        {
            var tariff = "Пакет: ";
            //var userId = User.Identity.GetUserId();
            //var paidInvoice = _invoiceService.GetByUserPaid(userId);
            //if (paidInvoice.Any())
            //{
            //    tariff = tariff + paidInvoice.Last().Tariff.Name + " до " + paidInvoice.Last().Debit.Date.Value.AddMonths(paidInvoice.Last().Tariff.Period).ToString(new CultureInfo("ru-RU"));
            //}
            //else
            //{
            //    tariff = tariff + "ТЕСТОВЫЙ";
            //}

            var viewModel = new TariffsViewModel
            {
                BreadcrumbsH2Text = tariff
            };

            //var invoice = _invoiceService.GetByUserDraft(userId).OrderByDescending(_=>_.Id).FirstOrDefault(_ => _.DueDate > DateTime.UtcNow);

            //if (invoice == null)
            //{
            //    return View(viewModel);
            //}

            //TempData["InvoiceId"] = invoice.Id;

            //return RedirectToAction("RenderInvoice", "Payment");
            return RedirectToAction("Index");
        }

        [ValidateInput(false)]
        [HttpPost]
        public bool DeleteLanding(string url)
        {
            var result = _landingService.DeleteLanding(url);
            return result;
        }
    }
}