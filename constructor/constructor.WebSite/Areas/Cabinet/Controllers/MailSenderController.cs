﻿using DomainModel.Content;
using Interfaces.Account;
using Interfaces.Content;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using Spoofi.AmoCrmIntegration.Dtos.Request;
using Spoofi.AmoCrmIntegration.Interface;
using Spoofi.AmoCrmIntegration.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebSite.Areas.Admin.Models;
using WebSite.Areas.Admin.Models.AmoCrm;
using WebSite.Areas.Admin.Service;
using WebSite.Areas.Cabinet.Framework;
using WebSite.Areas.Cabinet.Models;
using WebSite.Areas.Cabinet.Models.LandingIntegration;
using WebSite.Areas.Cabinet.Service;
using WebSite.MailChimp.Domain.Lists;
using WebSite.MailChimp.Enum;
using WebSite.MailChimp.Services.Lists;

namespace WebSite.Areas.Cabinet.Controllers
{
    public class MailSenderController : Controller
    {
        private readonly IAmoCrmService _service = new AmoCrmService(TestCrmConfig.Get());
        private readonly ILandingIntegrationService _landingIntegrationService;
        private readonly ILandingSettingsService _settingService;
        private readonly ILandingService _landing;
        private readonly IUserService _userService;
        private const string CookieName = "__AUTH_COOKIE";
        private const string CookieName2 = "__AUTH_COOKIE2";

        const string client_id = "";
        const string redirect_uri = "/Cabinet/MailSender/Bitrix";
        const string client_secret = "";
        string domain_name = "";

        public MailSenderController(ILandingIntegrationService landingIntegrationService, ILandingService landing, ILandingSettingsService settingService, IUserService userService)
        {
            _landingIntegrationService = landingIntegrationService;
            _landing = landing;
            _settingService = settingService;
            _userService = userService;
        }

        public ActionResult Index()
        {
            return View();
        }

        private class RequestModel
        {
            public bool IsRedirect { get; set; }
            public string AlertMessage { get; set; }
            public string Url { get; set; }
            public string ScriptCode { get; set; }
        }

        public void SendEmail(FormViewModel form)
        {
            if (_userService.GetByName(User.Identity.Name).IsNotify)
            {
                var result = _landingIntegrationService.GetByFormId(form.FormId);
                var emailModel = new EmailModel();
                emailModel.Subject = "Отправлена форма (cообщение от 2clicksale.com)";
                emailModel.To = _landing.GetById(result.LandingId).Email;
                emailModel.Fields = form.FieldsEmail;
                emailModel.Body = "Отправлена форма с сайта: " + _landing.GetById(result.LandingId).Name + ".";                                

                new EmailController().SendEmail(emailModel).Deliver();
            }
        }
        
        [HttpPost]
        public ActionResult SendForm(FormViewModel form)
        {
            if (form.TypeIntegration == 1)
            {
                SendToAmoCrm(form);
                var result = _landingIntegrationService.GetByFormId(form.FormId);
                var requestModel = new RequestModel();
                SendEmail(form);
                if (result!=null)
                {
                    requestModel.IsRedirect = result.IsRedirect;
                    if (result.IsRedirect)
                        requestModel.Url = result.RedirectUrl != null ? result.RedirectUrl : "";
                    else
                        requestModel.AlertMessage = result.AlertMessage != null ? result.AlertMessage : "";
                    var settingsModel = JsonConvert.DeserializeObject<MailChimpModel>(result.SettingsJson);
                    if (!string.IsNullOrEmpty(settingsModel.ScriptCode))
                    {
                        requestModel.ScriptCode = settingsModel.ScriptCode;
                    }
                }
                return Json(requestModel, JsonRequestBehavior.AllowGet);
            }            
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SendFormBitrix(FormViewModel form)
        {
            SendEmail(form);
            var paramString = "";
            foreach(var field in form.Fields)
            {
                paramString += field.Key + ":" + field.Value + "|";
            }
            var result = _landingIntegrationService.GetByFormId(form.FormId);          
            var settingsModel = JsonConvert.DeserializeObject<Bitrix24Model>(result.SettingsJson);
            paramString = paramString.Remove(paramString.Length - 1, 1);
            CreateCookie(paramString, true);
            CreateCookie2(form.FormId, true);
            return Redirect("https://landingi.bitrix24.ru/oauth/authorize/?client_id=" + settingsModel.ClientId + "&response_type=code&redirect_uri=" + domain_name + redirect_uri);                        
        }

        [HttpPost]
        public ActionResult SendPulse(FormViewModel form)
        {
            SendToPulse(form);
            var result = _landingIntegrationService.GetByFormId(form.FormId);
            SendEmail(form);
            var setting = _settingService.GetByLandingId(result.LandingId);
            if (setting != null)
            {
                var settingsModel = JsonConvert.DeserializeObject<LandingSettingsViewModel>(setting.SettingsJson);
                if (settingsModel.IsMailchimp)
                {
                    SendToMailChimpAll(form, settingsModel);
                }
                if (settingsModel.IsAmocrm)
                {
                    //SendToAmoCrmAll(form, settingsModel);
                }
                if (settingsModel.IsSmsintel)
                {
                    SendToSmsIntelAll(form, settingsModel);
                }          
            }

            var requestModel = new RequestModel();
            if (result != null)
            {
                requestModel.IsRedirect = result.IsRedirect;
                if (result.IsRedirect)
                    requestModel.Url = result.RedirectUrl != null ? result.RedirectUrl : "";
                else
                    requestModel.AlertMessage = result.AlertMessage != null ? result.AlertMessage : "";
                var settingsModel = JsonConvert.DeserializeObject<MailChimpModel>(result.SettingsJson);
                if (!string.IsNullOrEmpty(settingsModel.ScriptCode))
                {
                    requestModel.ScriptCode = settingsModel.ScriptCode;
                }
            }
            return Json(requestModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SendGetResponse(FormViewModel form)
        {
            SendToGetResponse(form);
            var result = _landingIntegrationService.GetByFormId(form.FormId);
            SendEmail(form);
            var setting = _settingService.GetByLandingId(result.LandingId);
            if (setting != null)
            {
                var settingsModel = JsonConvert.DeserializeObject<LandingSettingsViewModel>(setting.SettingsJson);
                //if (settingsModel.IsMailchimp)
                //{
                //    SendToMailChimpAll(form, settingsModel);
                //}
                //if (settingsModel.IsAmocrm)
                //{
                //    SendToAmoCrmAll(form, settingsModel);
                //}
                //if (settingsModel.IsSmsintel)
                //{
                //    SendToSmsIntelAll(form, settingsModel);
                //}
            }

            var requestModel = new RequestModel();
            if (result != null)
            {
                requestModel.IsRedirect = result.IsRedirect;
                if (result.IsRedirect)
                    requestModel.Url = result.RedirectUrl != null ? result.RedirectUrl : "";
                else
                    requestModel.AlertMessage = result.AlertMessage != null ? result.AlertMessage : "";
                var settingsModel = JsonConvert.DeserializeObject<MailChimpModel>(result.SettingsJson);
                if (!string.IsNullOrEmpty(settingsModel.ScriptCode))
                {
                    requestModel.ScriptCode = settingsModel.ScriptCode;
                }
            }
            return Json(requestModel, JsonRequestBehavior.AllowGet);
        }

        public void SendToGetResponse(FormViewModel form)
        {
            var result = _landingIntegrationService.GetByFormId(form.FormId);
            if (result != null)
            {
                var settingsModel = JsonConvert.DeserializeObject<GetResponse360>(result.SettingsJson);
                Synopsis synopsis = new Synopsis(settingsModel.Api_key, settingsModel.CampaignId);
                synopsis.Main(form.Fields);
            }
        }

        public void SendToPulse(FormViewModel form)
        {
            var result = _landingIntegrationService.GetByFormId(form.FormId);
            if (result!=null)
            {                
                var settingsModel = JsonConvert.DeserializeObject<SendPulseModel>(result.SettingsJson);
                Sendpulse sp = new Sendpulse(settingsModel.PulseId, settingsModel.PulseSecret);
                addEmailsToAddressBook(sp, settingsModel.BookId.Value, form.Fields, settingsModel);
            }            
        }

        public void addEmailsToAddressBook(Sendpulse sp, int BookId, IDictionary<string, string> fields, SendPulseModel model)
        {
            JObject email = new JObject();
            JObject variables = new JObject();
            foreach (var field in fields)
            {
                if(field.Key.ToLower().Contains("почта") || field.Key.ToLower().Contains("почты") || field.Key.ToLower().Contains("email") || field.Key.ToLower().Contains("e-mail"))
                {
                    email.Add("email", field.Value);
                }
                if (field.Key != "UTM_SOURCE" && field.Key != "UTM_MEDIUM" && field.Key != "UTM_CAMPAI" && field.Key != "UTM_CONTEN" && field.Key != "UTM_TERM")
                    variables.Add(field.Key, field.Value);
            }

            if (model.SendUtm)
            {
                try
                {
                    variables.Add("UTM_SOURCE", fields["UTM_SOURCE"] != "undefined" ? fields["UTM_SOURCE"] : "не задано");

                    variables.Add("UTM_MEDIUM", fields["UTM_MEDIUM"] != "undefined" ? fields["UTM_MEDIUM"] : "не задано");

                    variables.Add("UTM_CAMPAI", fields["UTM_CAMPAI"] != "undefined" ? fields["UTM_CAMPAI"] : "не задано");

                    variables.Add("UTM_CONTEN", fields["UTM_CONTEN"] != "undefined" ? fields["UTM_CONTEN"] : "не задано");

                    variables.Add("UTM_TERM", fields["UTM_TERM"] != "undefined" ? fields["UTM_TERM"] : "не задано");
                }
                catch { }
            }
            else
            {
                try
                {
                    variables.Add("UTM_SOURCE", "не задано");
                    variables.Add("UTM_MEDIUM", "не задано");
                    variables.Add("UTM_CAMPAI", "не задано");
                    variables.Add("UTM_CONTEN", "не задано");
                    variables.Add("UTM_TERM", "не задано");
                }
                catch { }
            }

            email.Add("variables", variables);
            JArray emails = new JArray();
            emails.Add(email);
            Dictionary<string, object> result = sp.addEmails(BookId, JsonConvert.SerializeObject(emails));
        }

        public void SendToAmoCrm(FormViewModel form)
        {
            var model = new AmoContact() { };
            model.Name = form.Fields["Имя"] + " " + form.Fields["Фамилия"];
            var accountInfo = _service.GetAccountInfo();
            var customFieldsList = accountInfo.CustomFields.Contacts;
            foreach (var field in form.Fields.ToList())
            {
                string keyField = field.Key;
                var fieldInAmocrm = customFieldsList.FirstOrDefault(_ => _.Name == keyField);
                if (fieldInAmocrm != null)
                {
                    model.CustomFields.Add(new CrmCustomFieldModel()
                    {
                        Id = fieldInAmocrm.Id,                        
                        Values = new List<CrmCustomFieldValueModel>()
                        {
                          new CrmCustomFieldValueModel() { Value = field.Value != "undefined"? field.Value : "не задано" }
                        }
                    });
                }
            }
            var jsonSettingsModel = _landingIntegrationService.GetByFormId(form.FormId);
            var amocrmSettingsModel = JsonConvert.DeserializeObject<AmoCrmModel>(jsonSettingsModel.SettingsJson);
            AmoCrmServiceIntegration amoService = new AmoCrmServiceIntegration(amocrmSettingsModel.SubDomain, amocrmSettingsModel.UserLogin, amocrmSettingsModel.UserHash);
            amoService.Send(model);
        }

        [HttpPost]
        public ActionResult SendFormSmsintel(FormViewModel form)
        {
            SendToSmsIntel(form);
            SendEmail(form);
            var result = _landingIntegrationService.GetByFormId(form.FormId);
            var requestModel = new RequestModel();
            if (result != null)
            {
                requestModel.IsRedirect = result.IsRedirect;
                if (result.IsRedirect)
                    requestModel.Url = result.RedirectUrl != null ? result.RedirectUrl : "";
                else
                    requestModel.AlertMessage = result.AlertMessage != null ? result.AlertMessage : "";
                var settingsModel = JsonConvert.DeserializeObject<MailChimpModel>(result.SettingsJson);
                if (!string.IsNullOrEmpty(settingsModel.ScriptCode))
                {
                    requestModel.ScriptCode = settingsModel.ScriptCode;
                }
            }
            return Json(requestModel, JsonRequestBehavior.AllowGet);
        }

        private void SendToSmsIntel(FormViewModel form)
        {
            var jsonSettingsModel = _landingIntegrationService.GetByFormId(form.FormId);
            var amocrmSettingsModel = JsonConvert.DeserializeObject<Smsintel>(jsonSettingsModel.SettingsJson);

            var client = new RestClient("https://lcab.smsintel.ru");
            var request = new RestRequest("lcabApi/sendSms.php", RestSharp.Method.POST);
            request.AddParameter("login", amocrmSettingsModel.UserLogin);
            request.AddParameter("password", amocrmSettingsModel.UserPassword);
            request.AddParameter("to", amocrmSettingsModel.GroupId);

            var content = "";
            var message = ";";

            foreach (var field in form.Fields)
            {
                if (field.Key.ToLower().Contains("messag") || field.Key.ToLower().Contains("сообщ") || field.Key.ToLower().Contains("текст"))
                {
                    content = field.Value;                    
                }
                else
                {
                    message += field.Value + "; ";
                }
                
            }
            request.AddParameter("txt", content + message);
            var response = client.Execute<SmsIntelResult>(request);          
            
        }

        public class SmsIntelResult
        {
            public string code { get; set; }
            public string descr { get; set; }
            public string colsmsOfSending { get; set; }
            public string priceOfSending { get; set; }
        }


        [HttpGet]
        public ActionResult Bitrix()
        {
            List<string> fields = new List<string>();
            string dataFields = "";
            var authCookie = HttpContext.Request.Cookies.Get(CookieName);
            if (authCookie != null && !string.IsNullOrEmpty(authCookie.Value))
            {
                var ticket = FormsAuthentication.Decrypt(authCookie.Value);
                dataFields = ticket.Name;
            }
            var formId0 = "";
            var authCookie20 = HttpContext.Request.Cookies.Get(CookieName2);           
            var ticket2 = FormsAuthentication.Decrypt(authCookie20.Value);
            formId0 = ticket2.Name;
            var result2 = _landingIntegrationService.GetByFormId(formId0);
            var settingsModel = JsonConvert.DeserializeObject<Bitrix24Model>(result2.SettingsJson);

            var t = Request.Url.OriginalString;
            var code = t.Split('&')[0].Split('=')[1];
            var client = new RestClient("https://landingi.bitrix24.ru");
            var request = new RestRequest("oauth/token/?client_id=" + settingsModel.ClientId + "&grant_type=authorization_code&client_secret=" + settingsModel.ClientSecret + "&redirect_uri=http://localhost:23556/Home/Token&code=" + code + "&scope=crm", RestSharp.Method.GET);

            var response2 = client.Execute<AuthData>(request);
            var token = response2.Data.access_token;
            client = new RestClient("https://landingi.bitrix24.ru") { Encoding = Encoding.UTF8 };
            request = new RestRequest("rest/crm.contact.list.json", RestSharp.Method.POST);
            request.AddParameter("auth", token);
            var response22 = client.Execute<RootObject>(request);
            var token23 = response22.Data.result;

            client = new RestClient("https://landingi.bitrix24.ru") { Encoding = Encoding.UTF8 };
            request = new RestRequest("rest/crm.contact.add.json", RestSharp.Method.POST);
            request.AddParameter("auth", token);
  
            foreach (var item in dataFields.Split('|'))
            {
                var nameField = item.Split(':')[0];
                var valField = item.Split(':')[1];
                if (nameField.ToLower().Contains("имя"))
                    request.AddParameter("fields[NAME]", valField);
                if (nameField.ToLower().Contains("фамилия"))
                    request.AddParameter("fields[LAST_NAME]", valField);
                if (nameField.ToLower().Contains("отчество"))
                    request.AddParameter("fields[SECOND_NAME]", valField);
                if (nameField.ToLower().Contains("email") || nameField.ToLower().Contains("e-mail") || nameField.ToLower().Contains("почта") || nameField.ToLower().Contains("почты"))
                {
                    request.AddParameter("fields[EMAIL][0][VALUE]", valField);
                    request.AddParameter("fields[EMAIL][0][VALUE_TYPE]", "WORK");
                }
                if (nameField.ToLower().Contains("телефон") || nameField.ToLower().Contains("phone"))
                {
                    request.AddParameter("fields[PHONE][0][VALUE]", valField);
                    request.AddParameter("fields[PHONE][0][VALUE_TYPE]", "WORK");

                    //custom field, need know name in api POST https://landingi.bitrix24.ru/rest/crm.contact.fields.json
                    //request.AddParameter("fields[UF_CRM_1477330916]", valField);
                }
            }
            
            IRestResponse response = client.Execute(request);
            var content = response.Content;

            var formId = "";
            var authCookie2 = HttpContext.Request.Cookies.Get(CookieName2);
            var landingId = -1;
            if (authCookie2 != null && !string.IsNullOrEmpty(authCookie2.Value))
            {
                var ticket = FormsAuthentication.Decrypt(authCookie2.Value);
                formId = ticket.Name;
                var result = _landingIntegrationService.GetByFormId(formId);
                landingId = result.LandingId;
                var dataString = result.IsRedirect + "|";
                if (result.IsRedirect)
                    dataString += result.RedirectUrl;
                else
                    dataString += result.AlertMessage;

                var setModel = JsonConvert.DeserializeObject<MailChimpModel>(result.SettingsJson);
                if (!string.IsNullOrEmpty(setModel.ScriptCode))
                {
                    dataString += "|" + setModel.ScriptCode;
                }
                else
                {
                    dataString += "|none";
                }

                TempData["BitrixAfterSubscribe"] = dataString;
            }
            var landKey = _landing.GetById(landingId);
            return RedirectToAction("Index", "Preview", new { area = "Cabinet", url = landKey.Url });
        }
                
        private void CreateCookie(string userName, bool isPersistent = false)
        {
            var ticket = new FormsAuthenticationTicket(
                  1,
                  userName,
                  DateTime.Now,
                  DateTime.Now.Add(FormsAuthentication.Timeout),
                  isPersistent,
                  string.Empty,
                  FormsAuthentication.FormsCookiePath);

            // Encrypt the ticket.
            var encTicket = FormsAuthentication.Encrypt(ticket);

            // Create the cookie.
            var AuthCookie = new System.Web.HttpCookie(CookieName)
            {
                Value = encTicket,
                Expires = DateTime.Now.AddDays(3)
            };
            HttpContext.Response.Cookies.Set(AuthCookie);
        }
        private void CreateCookie2(string userName, bool isPersistent = false)
        {
            var ticket = new FormsAuthenticationTicket(
                  1,
                  userName,
                  DateTime.Now,
                  DateTime.Now.Add(FormsAuthentication.Timeout),
                  isPersistent,
                  string.Empty,
                  FormsAuthentication.FormsCookiePath);

            // Encrypt the ticket.
            var encTicket = FormsAuthentication.Encrypt(ticket);

            // Create the cookie.
            var AuthCookie = new System.Web.HttpCookie(CookieName2)
            {
                Value = encTicket,
                Expires = DateTime.Now.AddDays(3)
            };
            HttpContext.Response.Cookies.Set(AuthCookie);
        }

        public class AuthData
        {
            public string access_token { get; set; }
        }

        public class Result
        {
            public string ID { get; set; }
            public string POST { get; set; }
            public string COMMENTS { get; set; }
            public string HONORIFIC { get; set; }
            public string NAME { get; set; }
            public string SECOND_NAME { get; set; }
            public string LAST_NAME { get; set; }
            public object PHOTO { get; set; }
            public object LEAD_ID { get; set; }
            public string TYPE_ID { get; set; }
            public string SOURCE_ID { get; set; }
            public string SOURCE_DESCRIPTION { get; set; }
            public object COMPANY_ID { get; set; }
            public string BIRTHDATE { get; set; }
            public string EXPORT { get; set; }
            public string HAS_PHONE { get; set; }
            public string HAS_EMAIL { get; set; }
            public string DATE_CREATE { get; set; }
            public string DATE_MODIFY { get; set; }
            public string ASSIGNED_BY_ID { get; set; }
            public string CREATED_BY_ID { get; set; }
            public string MODIFY_BY_ID { get; set; }
            public string OPENED { get; set; }
            public object ORIGINATOR_ID { get; set; }
            public object ORIGIN_ID { get; set; }
            public object ORIGIN_VERSION { get; set; }
            public string ADDRESS { get; set; }
            public object ADDRESS_2 { get; set; }
            public string ADDRESS_CITY { get; set; }
            public object ADDRESS_POSTAL_CODE { get; set; }
            public string ADDRESS_REGION { get; set; }
            public string ADDRESS_PROVINCE { get; set; }
            public object ADDRESS_COUNTRY { get; set; }
            public object ADDRESS_COUNTRY_CODE { get; set; }
        }

        public class RootObject
        {
            public List<Result> result { get; set; }
            public int total { get; set; }
        }

        public ActionResult Token(string data)
        {
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SendMailChimp(FormViewModel form)
        {
            var result = _landingIntegrationService.GetByFormId(form.FormId);
            var requestModel = new RequestModel();
            if (result != null)
            {
                var settingsModel = JsonConvert.DeserializeObject<MailChimpModel>(result.SettingsJson);
                SendToMailChimp(form, result, settingsModel);
                SendEmail(form);
                var setting = _settingService.GetByLandingId(result.LandingId);
                if (setting != null)
                {
                    var settingsModelAll = JsonConvert.DeserializeObject<LandingSettingsViewModel>(setting.SettingsJson);
                    if (settingsModelAll.IsSendpulse)
                    {
                        SendToPulseAll(form, settingsModelAll);
                    }
                    if (settingsModelAll.IsSmsintel)
                    {
                        SendToSmsIntelAll(form, settingsModelAll);
                    }
                    if (settingsModelAll.IsAmocrm)
                    {
                        //SendToAmoCrmAll(form, settingsModel);
                    }
                }

                requestModel.IsRedirect = result.IsRedirect;
                if (result.IsRedirect)
                    requestModel.Url = result.RedirectUrl != null ? result.RedirectUrl : "";
                else
                    requestModel.AlertMessage = result.AlertMessage != null ? result.AlertMessage : "";                
                if (!string.IsNullOrEmpty(settingsModel.ScriptCode))
                {
                    requestModel.ScriptCode = settingsModel.ScriptCode;
                }
            }
            return Json(requestModel, JsonRequestBehavior.AllowGet);
        }

        private void SendToMailChimp(FormViewModel form, LandingIntegration landing, MailChimpModel model)
        {
            var settingsModel = JsonConvert.DeserializeObject<MailChimpModel>(landing.SettingsJson);
            StaticSettingsMailChimp.MailChimpApiKey = settingsModel.MailChimpApiKey;
            MailChimpList lists = new MailChimpList();
            MCMember member = new MCMember();
            member.email_type = "html";
            member.language = "Russian";
            member.status = SubscriberStatus.subscribed.ToString();
            member.merge_fields = new Dictionary<string, object>();

            foreach (var field in form.Fields.ToList())
            {
                bool alreadyAdded = false;
                string keyField = field.Key;
                if (keyField == "EMAIL")
                {
                    member.email_address = String.Format(field.Value);
                    alreadyAdded = true;
                }
                else
                {
                    if (field.Key == "UTM_SOURCE" && field.Value == "undefined")
                    {
                        member.merge_fields.Add("UTM_SOURCE", "не задано");
                        alreadyAdded = true;
                    }
                    if (field.Key == "UTM_MEDIUM" && field.Value == "undefined")
                    {
                        member.merge_fields.Add("UTM_MEDIUM", "не задано");
                        alreadyAdded = true;
                    }
                    if (field.Key == "UTM_CAMPAI" && field.Value == "undefined")
                    {
                        member.merge_fields.Add("UTM_CAMPAI", "не задано");
                        alreadyAdded = true;
                    }
                    if (field.Key == "UTM_CONTEN" && field.Value == "undefined")
                    {
                        member.merge_fields.Add("UTM_CONTEN", "не задано");
                        alreadyAdded = true;
                    }
                    if (field.Key == "UTM_TERM" && field.Value == "undefined")
                    {
                        member.merge_fields.Add("UTM_TERM", "не задано");
                        alreadyAdded = true;
                    }
                }
                if (!alreadyAdded)
                {
                    member.merge_fields.Add(field.Key, field.Value);
                }
            }
                        
            var x = lists.AddMember(member, settingsModel.ListId).Result;
        }

        public void SendToPulseAll(FormViewModel form, LandingSettingsViewModel settings)
        {
            Sendpulse sp = new Sendpulse(settings.SendPulse.PulseId, settings.SendPulse.PulseSecret);
            addEmailsToAddressBook(sp, settings.SendPulse.BookId.Value, form.Fields, new SendPulseModel() { SendUtm = false });
        }

        private void SendToMailChimpAll(FormViewModel form, LandingSettingsViewModel model)
        {
            var settingsModel = model.MailChimp;
            StaticSettingsMailChimp.MailChimpApiKey = settingsModel.MailChimpApiKey;
            MailChimpList lists = new MailChimpList();
            MCMember member = new MCMember();
            member.email_type = "html";
            member.language = "Russian";
            member.status = SubscriberStatus.subscribed.ToString();
            member.merge_fields = new Dictionary<string, object>();

            foreach (var field in form.Fields.ToList())
            {
                bool alreadyAdded = false;
                string keyField = field.Key;
                if (keyField == "EMAIL")
                {
                    member.email_address = String.Format(field.Value);
                    alreadyAdded = true;
                }
                else
                {
                    if (field.Key == "UTM_SOURCE" && field.Value == "undefined")
                    {
                        member.merge_fields.Add("UTM_SOURCE", "не задано");
                        alreadyAdded = true;
                    }
                    if (field.Key == "UTM_MEDIUM" && field.Value == "undefined")
                    {
                        member.merge_fields.Add("UTM_MEDIUM", "не задано");
                        alreadyAdded = true;
                    }
                    if (field.Key == "UTM_CAMPAI" && field.Value == "undefined")
                    {
                        member.merge_fields.Add("UTM_CAMPAI", "не задано");
                        alreadyAdded = true;
                    }
                    if (field.Key == "UTM_CONTEN" && field.Value == "undefined")
                    {
                        member.merge_fields.Add("UTM_CONTEN", "не задано");
                        alreadyAdded = true;
                    }
                    if (field.Key == "UTM_TERM" && field.Value == "undefined")
                    {
                        member.merge_fields.Add("UTM_TERM", "не задано");
                        alreadyAdded = true;
                    }
                }
                if (!alreadyAdded)
                {
                    member.merge_fields.Add(field.Key, field.Value);
                }
            }

            var x = lists.AddMember(member, settingsModel.ListId).Result;
        }

        private void SendToAmoCrmAll(FormViewModel form, LandingSettingsViewModel modelSett)
        {
            var model = new AmoContact() { };
            model.Name = form.Fields["FNAME"] + " " + form.Fields["LNAME"];
            var accountInfo = _service.GetAccountInfo();
            var customFieldsList = accountInfo.CustomFields.Contacts;
            foreach (var field in form.Fields.ToList())
            {
                string keyField = field.Key;
                var fieldInAmocrm = customFieldsList.FirstOrDefault(_ => _.Name == keyField);
                if (fieldInAmocrm != null)
                {
                    model.CustomFields.Add(new CrmCustomFieldModel()
                    {
                        Id = fieldInAmocrm.Id,
                        Values = new List<CrmCustomFieldValueModel>()
                        {
                          new CrmCustomFieldValueModel() { Value = field.Value != "undefined"? field.Value : "не задано" }
                        }
                    });
                }
            }
            var settingsModel = modelSett.AmoCrm;            
            AmoCrmServiceIntegration amoService = new AmoCrmServiceIntegration(settingsModel.SubDomain, settingsModel.UserLogin, settingsModel.UserHash);
            amoService.Send(model);
        }

        private void SendToSmsIntelAll(FormViewModel form, LandingSettingsViewModel modelSett)
        {
            var client = new RestClient("https://lcab.smsintel.ru");
            var request = new RestRequest("lcabApi/addContact.php", RestSharp.Method.POST);
            request.AddParameter("login", modelSett.Smsintel.UserLogin);
            request.AddParameter("password", modelSett.Smsintel.UserPassword);
            request.AddParameter("idGroup", modelSett.Smsintel.GroupId);

            foreach (var field in form.Fields)
            {
                if (field.Key.ToLower().Contains("телефон") || field.Key.ToLower().Contains("номер") || field.Key.ToLower().Contains("phone"))
                {
                    request.AddParameter("phone", field.Value);
                }
                if (field.Key.ToLower().Contains("email") || field.Key.ToLower().Contains("почт") || field.Key.ToLower().Contains("e-mail"))
                {
                    request.AddParameter("email", field.Value);
                }
                if (field.Key.ToLower().Contains("фамили") || field.Key.ToLower().Contains("lastn") || field.Key.ToLower().Contains("lname"))
                {
                    request.AddParameter("f", field.Value);
                }
                if (field.Key.ToLower().Contains("имя") || field.Key.ToLower().Contains("firstn") || field.Key.ToLower().Contains("fname"))
                {
                    request.AddParameter("i", field.Value);
                }
                if (field.Key.ToLower().Contains("отчеств") || field.Key.ToLower().Contains("sname"))
                {
                    request.AddParameter("o", field.Value);
                }
            }
            // execute the request
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
        }
    }
}