﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DomainModel.Content;
using Interfaces.Content;
using WebSite.Areas.Cabinet.Framework;
using WebSite.Areas.Cabinet.Models;
using WebSite.Areas.Cabinet.Models.Account;

namespace WebSite.Areas.Cabinet.Controllers
{
    [Authorize]
    public class PicturesController : CabinetBaseController
    {
        public ActionResult Index()
        {
            var model = new UserInfoViewModel();
            model.UserName = "TestUser";
            return View(model);
        }

        [HttpPost]
        public ActionResult SavePictures(HttpPostedFileBase[] files)
        {
            var userName = "TestUser";
            for (var i = 0; i < files.Length; i++)
            {
                HttpPostedFileBase file = files[i];
                var path = Server.MapPath(WorkContext.UserImagesPath) + userName + "/";
                file.SaveAs(path+file.FileName);
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult LoadImages()
        {
            var user = "TestUser";
            if (!Directory.Exists(Server.MapPath(WorkContext.UserImagesPath + user)))
            {
                Directory.CreateDirectory(Server.MapPath(WorkContext.UserImagesPath + user));
            }
            var dir = new DirectoryInfo(Server.MapPath(WorkContext.UserImagesPath) + user);// папка с файлами 
            var files = dir.GetFiles().Select(file => WorkContext.UserImagesPath + user + "/" + file.Name).ToArray(); // список имен файлов 
            return Json(new { res = files, state = "ok" });
        }
    }
}