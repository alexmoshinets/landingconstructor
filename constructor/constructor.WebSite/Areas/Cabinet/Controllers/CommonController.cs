﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebSite.Areas.Cabinet.Models;

namespace WebSite.Areas.Cabinet.Controllers
{
    public class CommonController : Controller
    {
        // GET: Cabinet/Common
        public ActionResult Menu(string stringKey = "")
        {
            var currentController =
                ControllerContext.ParentActionViewContext.Controller.ValueProvider.GetValue("controller")
                    .RawValue.ToString();
            var currentAction =
                ControllerContext.ParentActionViewContext.Controller.ValueProvider.GetValue("action")
                    .RawValue.ToString();
            var model = BuildMenu(stringKey);
            foreach (var item in model)
            {
                //if (item.Controller != null &&
                //    item.Controller.Equals(currentController, StringComparison.InvariantCultureIgnoreCase)
                //    ||
                //    item.Childs.Any(
                //        _ =>
                //            _.Controller != null &&
                //            _.Controller.Equals(currentController, StringComparison.InvariantCultureIgnoreCase)))
                //{
                //    item.IsActive = true;
                //}
                if (item.Controller != null &&
                            item.Controller.Equals(currentController, StringComparison.InvariantCultureIgnoreCase)
                            && item.Action.Equals(currentAction, StringComparison.InvariantCultureIgnoreCase))
                {
                    item.IsActive = true;                   
                }

                if (item.Childs.Count > 0)
                {
                    foreach (var child in item.Childs)
                    {
                        if (child.Controller != null &&
                            child.Controller.Equals(currentController, StringComparison.InvariantCultureIgnoreCase)
                            && child.Action.Equals(currentAction, StringComparison.InvariantCultureIgnoreCase))
                        {
                            child.IsActive = true;
                            model.FirstOrDefault(_ => _.Id == child.ParentId).IsActive = true;
                        }
                    }
                }
            }           
            return PartialView(model);
        }

        private List<MenuItemViewModel> BuildMenu(string stringKey)
        {
            var menu = new List<MenuItemViewModel>()
            {
                new MenuItemViewModel()
                {
                    Controller = "Settings",
                    Action = "Index",
                    Name = "Главная",
                    Id = 1,
                    Childs = new List<MenuItemViewModel>()
                    {
                        new MenuItemViewModel()
                        {
                            Controller = "Settings",
                            Action = "Setting",
                            Name = "Общие настройки",
                            Id = 3,
                            ParentId = 1,
                            StringKey = stringKey
                        },
                        new MenuItemViewModel()
                        {
                            Controller = "Settings",
                            Action = "Integration",
                            Name = "Настройки интеграции",
                            Id = 4,
                            ParentId = 1,
                            StringKey = stringKey
                        }
                    }
                },
                new MenuItemViewModel()
                {
                    Controller = "Settings",
                    Action = "MailSettings",
                    Name = "Настройки уведомлений",
                    Id = 2,
                    StringKey = stringKey
                }
            };
            return menu;
        }
    }
}