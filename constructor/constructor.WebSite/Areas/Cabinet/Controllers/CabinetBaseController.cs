﻿using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using DomainModel.Content;
using Interfaces.Content;
using WebSite.Areas.Cabinet.Models;
using WebSite.Framework;

//using Microsoft.AspNet.Identity;

namespace WebSite.Areas.Cabinet.Controllers
{
    [Authorize(Roles = "editor")]
    public class CabinetBaseController : Controller
    {
        protected void SuccessMessage(string message)
        {
            TempData[Constants.NotificationsSuccess] = message;
        }

        protected void ErrorMessage(string message)
        {
            TempData[Constants.NotificationsError] = message;
        }
    }
}