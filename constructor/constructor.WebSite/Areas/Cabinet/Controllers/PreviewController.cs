﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DomainModel.Content;
using Interfaces.Common;
using Interfaces.Content;
using WebSite.Areas.Cabinet.Models;
using WebSite.Areas.Cabinet.Framework;
using System.Text;
using System.Diagnostics;

namespace WebSite.Areas.Cabinet.Controllers
{
    [Authorize]
    public class PreviewController : CabinetBaseController
    {
        private readonly ILandingService _landingService;
        private readonly IMessageService _messageService;
        private readonly ISettingService _settingService;
        public PreviewController(ILandingService landingService, IMessageService messageService, ISettingService settingService)
        {
            _landingService = landingService;
            _messageService = messageService;
            _settingService = settingService;
        }

        public MembershipUser user;
        //public ActionResult Index(string url, string page = "")
        //{
        //    var model = new LandingViewModel();
        //    user = Membership.GetUser();
        //    if (user != null)
        //    {
                
        //        var userId = user.ProviderUserKey;
        //        if (userId != null)
        //        {
        //            if (page == "") {
        //                page = "Home";
        //            }
        //            var pag = _landingService.GetByUser(Int32.Parse(userId.ToString())).FirstOrDefault(_ => (_.Url == url && _.StringKey == page));//страница текушего пользователя
        //            if (pag != null) {
        //                model = LandingViewModel.FromDomainModel(pag);
        //            }                    
        //        }
        //    }
            
        //    return View(model);
        //}

        //[HttpGet]
        public ActionResult Index(string url, string page = "")
        {
            var model = new List<LandingViewModel>();
            user = Membership.GetUser();
            if (user != null)
            {

                var userId = user.ProviderUserKey;
                if (userId != null)
                {
                    var pages = _landingService.GetByUser(Int32.Parse(userId.ToString()));//все страницы текушего пользователя
                    model.Add(LandingViewModel.FromDomainModel(pages.FirstOrDefault(_ => _.Url == url && _.Type == "layout")));// добавляем layout
                    if (page != "")
                    {
                        var thisPage = pages.FirstOrDefault(_ => _.Url == url && _.StringKey == page);
                        model.Add(page != null
                            ? LandingViewModel.FromDomainModel(thisPage)//если такая страница есть, то она передается
                            : LandingViewModel.FromDomainModel(
                                pages.FirstOrDefault(_ => _.Url == url && _.Type == "home")));//иначе передается главная
                    }
                    else//если страница не указана, то загружается главная
                    {
                        model.Add(LandingViewModel.FromDomainModel(pages.FirstOrDefault(_ => _.Url == url && _.Type == "home")));
                    }
                    var landingViewModel = model.FirstOrDefault(_ => _.Type != "layout");
                    if (landingViewModel != null)
                    {
                        var liteboxes = pages.Select(LandingViewModel.FromDomainModel).Where(_ => _.Url == url && _.Type == "litebox");
                        landingViewModel.Scripts = "$(document).ready(function() {";
                        foreach (var item in liteboxes)
                        {
                            landingViewModel.Litebox += item.SiteContent;
                            landingViewModel.Scripts += "$(&quot;#" + item.StringKey + "&quot;).fancybox({'titlePosition':'inside','transitionIn':'none','transitionOut':'none'});";
                        }
                        landingViewModel.Scripts += "});";
                    }
                }
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult SendForm(string param)
        {
            var temp = param.Replace("&", "$$$");
            var count = temp.Replace("]}{[", "&").Split('&').Count();
            var array = temp.Replace("]}{[", "&").Replace("{[", "").Replace("]}", "").Split('&');
            var fields = new string[count,2];
            var i = 0;
            foreach (var item in array)
            {
                fields[i, 0] = item.Replace("],[", "&").Split('&')[0].Replace("$$$", "&");
                fields[i, 1] = item.Replace("],[", "&").Split('&')[1].Replace("$$$", "&");
                i++;
            }

            var body = "<table width='100%' border='0' cellspacing='1' cellpadding='1'>";
            for (var j = 0; j<i; j++)
            {
                body += "<tr>" +
                       "<td><b>" + fields[j,0] +"</b><br>"+ fields[j,1]+"</td>" +
                       "</tr>";
            }
            body += "</table>";
            var cEmail = _settingService.GetByKey("Email");
            _messageService.Send(cEmail.Value, user.Email, body);

            return RedirectToAction("Index");
        }

         [HttpPost]
        public ActionResult SendForm2(List<string> parametrs, List<HttpPostedFileBase> files, string namesFieldsOfParametrs, string namesFieldsOfFiles)
         {
             var body = "";
            if (parametrs != null && parametrs.Count != 0)
            {
                var arrayParametrs = namesFieldsOfParametrs.Replace("$&", "~").Split('~');
                for (var i=0; i<parametrs.Count; i++)
                {
                    body = "<table width='100%' border='0' cellspacing='1' cellpadding='1'>";
                    for (var j = 0; j < i; j++)
                    {
                        body += "<tr>" +
                               "<td><b>" + parametrs[i] + "</b><br>" + arrayParametrs[i] + "</td>" +
                               "</tr>";
                    }
                    body += "</table>";
                }
            }
            user = Membership.GetUser();
            var smtpClient = new SmtpClient();

            var mailMessage = new MailMessage();
            if (!smtpClient.Host.Equals("smtp.gmail.com"))
            {
                mailMessage.From = new MailAddress(user.Email, "");
            }
            mailMessage.To.Add(_settingService.GetByKey("Email").Value);
            //mailMessage.Subject = subject;
            mailMessage.Body = body;
            mailMessage.IsBodyHtml = true;

            if (files != null && files.Count != 0)
            {
                foreach (var item in files)
                {
                    if ((item != null) && (item.InputStream != Stream.Null))
                    {
                        Attachment attach = new Attachment(item.InputStream, item.FileName, item.ContentType);
                        mailMessage.Attachments.Add(attach);
                    }
                }
            }
             
            try
            {
                smtpClient.Send(mailMessage);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                //_loggerService.Error(string.Format("Проблема отправки сообщения. To: {0} Ex: {1}; InEx: {2} ", to,
                //    ex.Message, ex.InnerException == null ? "" : ex.InnerException.Message));
                return RedirectToAction("Index");
            }
        }

         [ValidateInput(false)]
         [HttpPost]
         public string SaveHtmlFile(string url, string page, string pageContent)
         {
             var user = Membership.GetUser();
             if (user != null)
             {
                 var landing = _landingService.GetByUrlAndUserId(url, Int32.Parse(user.ProviderUserKey.ToString())).FirstOrDefault(_ => _.StringKey == page);
                 if (landing != null)
                 {
                     _landingService.Update(landing.Id, landings =>
                     {
                         landings.FullSiteContent = pageContent;
                     });
                     return "ok!";
                 }



                 return "landing is undefined";
             }
             else
             {
                 return "user is undefined";
             }
         }

    }

}