﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using DomainModel.Content;
using Interfaces.Content;
using WebSite.Areas.Cabinet.Models;
//using Microsoft.AspNet.Identity;

namespace WebSite.Areas.Cabinet.Controllers
{
    [Authorize]
    public class PaymentController : CabinetBaseController
    {
        // private readonly ITariffService _tariffService;
        // private readonly IInvoiceService _invoiceService;
        // private readonly IYandexService _yandexService;
        // public PaymentController(ITariffService tariffService, IInvoiceService invoiceService, IYandexService yandexService)
        //{
        //    _tariffService = tariffService;
        //    _invoiceService = invoiceService;
        //     _yandexService = yandexService;
        //}
        // GET: Payment/CreateInvoice
        public ActionResult CreateInvoice()
        {
            return RedirectToAction("Index", "Home");
        }

        // POST: Payment/CreateInvoice
        [HttpPost]
        public async Task<ActionResult> CreateInvoice(TariffsViewModel model)
        {
            // TODO. Check if period is in 1, 3, 6, 12 months.

            //if (!ModelState.IsValid)
            //{
            //    return RedirectToAction("Tariffs", "Home");
            //}

            // TODO Create invoice and return confirmation form with invoice details.

            //var userId = User.Identity.GetUserId();

            //if (userId == null)
            //{
            //    return RedirectToAction("Tariffs", "Home");
            //}
            //var tariff = _tariffService.GetByName(model.Tariff).FirstOrDefault(_ => _.Period == model.PeriodInMonths);
            //var invoice = new Invoice
            //{
            //    UserId = userId,
            //    Date = DateTime.UtcNow,
            //    DueDate = DateTime.UtcNow.AddDays(7),
            //    Status = InvoiceStatus.Draft,
            //    TariffId = tariff.Id,
            //    Amount = tariff.Price,
            //    PaymentMethod = model.PaymentMethod
            //};
            //_invoiceService.Add(invoice);

            //TempData["InvoiceId"] = invoice.Id;
            return RedirectToAction("RenderInvoice");
        }

        // GET: Payment/RenderInvoice
        public ActionResult RenderInvoice()
        {
            if (TempData["InvoiceId"] == null)
            {
                return RedirectToAction("Index", "Home");
            }

            var userName = User.Identity.Name;

            var invoiceId = (int) TempData["InvoiceId"];
            var viewModel = new CreateInvoiceViewModel();
            //var invoice = _invoiceService.GetById(invoiceId);

            //var viewModel = new CreateInvoiceViewModel
            //{
            //    InvoiceId = invoice.Id,
            //    Amount = invoice.Amount.ToString(CultureInfo.InvariantCulture),
            //    PaymentMethod = invoice.PaymentMethod.ToString(),
            //    PeriodInMonths = invoice.Tariff.Period == 1 ? "1 месяц" : invoice.Tariff.Period == 3 ? "3 месяца" : invoice.Tariff.Period + " месяцев",
            //    TariffName = invoice.Tariff.Name,
            //    Date = invoice.Date.ToString("d", new CultureInfo("ru-RU")),
            //    DueDate = invoice.DueDate.ToString("d", new CultureInfo("ru-RU")) + " г. " + invoice.DueDate.ToString("t", new CultureInfo("ru-RU")),
            //    YandexMoneyApiViewModel = new YandexMoneyApiViewModel
            //    {
            //        Receiver = Helpers.Constants.YandexWallet_Irina,
            //        FormComment = "2ClickSale.com - пополнение счёта " + userName,
            //        ShortDest = "2ClickSale.com - тариф " + invoice.Tariff.Name + " на период " + invoice.Tariff.Period + " мес.",
            //        Targets = "2ClickSale.com - пополнение счёта " + userName + " (тариф " + invoice.Tariff.Name + " на период " + invoice.Tariff.Period + " мес.)",
            //        Label = invoice.Id.ToString(),
            //        QuickpayForm = "shop",
            //        Sum = invoice.Amount,
            //        Comment = "",
            //        NeedFio = "false",
            //        NeedEmail = "false",
            //        NeedPhone = "false",
            //        NeedAddress = "false"
            //    }
            //};

            //switch (invoice.PaymentMethod)
            //{
            //    case PaymentMethod.YandexMoney:
            //        viewModel.YandexMoneyApiViewModel.PaymentType = "PC";
            //        break;
            //    case PaymentMethod.CreditCard:
            //        viewModel.YandexMoneyApiViewModel.PaymentType = "AC";
            //        viewModel.PaymentMethod = "Карта Visa/MasterCard";
            //        break;
            //}

            return View(viewModel);
        }

        // GET: Payment/DeleteInvoice
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteInvoice(int id)
        {
            //var userId = User.Identity.GetUserId();

            //var invoice = _invoiceService.GetById(id);

            //if (invoice != null&&invoice.UserId == userId)
            //{
            //    invoice.Status = InvoiceStatus.Deleted;
            //    _invoiceService.Update(id, inv =>
            //    {
            //        inv.Status = invoice.Status;
            //    });
            //}

            return RedirectToAction("Tariffs", "Home");
        }

        // TODO. Read all web response and save it to PaymentLog.txt. Create transaction record. Create record in table YandexPaymentSystem. Change in table Invoice field Status to Payed add PayedDateTime.

        //[AllowAnonymous]
        //[HttpPost]
        //public async Task YandexPaymentResult([Bind(Exclude = "Id")] YandexMoneyResponse model)
        //{
        //    if (YandexMoneyApiGetHash(model) == model.Sha1_Hash)
        //    {
        //        model.Date = DateTime.UtcNow;

        //        _yandexService.Add(model);

        //        var invoiceId = 0;

        //        if (model.Label != null)
        //        {
        //            invoiceId = Convert.ToInt32(model.Label);
        //        }

        //        var invoice = _invoiceService.GetById(invoiceId);
        //        if (invoice != null)
        //        {
        //            invoice.Status = InvoiceStatus.Paid;
        //            invoice.Credit = new Credit
        //            {
        //                Amount = model.Withdraw_Amount,
        //                Date = DateTime.UtcNow
        //            };
        //            switch (model.Notification_Type)
        //            {
        //                case "p2p-incoming":
        //                    invoice.Credit.PaymentMethod = PaymentMethod.YandexMoney;
        //                    break;
        //                case "card-incoming":
        //                    invoice.Credit.PaymentMethod = PaymentMethod.CreditCard;
        //                    break;
        //            }
        //            invoice.Debit = new Debit
        //            {
        //                Amount = model.Withdraw_Amount,
        //                Date = DateTime.UtcNow
        //            };
        //            switch (model.Notification_Type)
        //            {
        //                case "p2p-incoming":
        //                    invoice.Debit.PaymentMethod = PaymentMethod.YandexMoney;
        //                    break;
        //                case "card-incoming":
        //                    invoice.Debit.PaymentMethod = PaymentMethod.CreditCard;
        //                    break;
        //            }
        //        }

        //    }
        //}

        #region Helpers

        // Yandex money api hash.
        //private string YandexMoneyApiGetHash([Bind(Exclude = "Id")] YandexMoneyResponse model)
        //{
        //    var parameters = string.Empty;

        //    parameters += model.Notification_Type + "&";
        //    parameters += model.Operation_Id + "&";
        //    parameters += model.Amount.ToString(CultureInfo.InvariantCulture) + "&";
        //    parameters += model.Currency + "&";
        //    parameters += string.Format("{0:s}", model.Datetime) + "Z" + "&";
        //    parameters += model.Sender + "&";
        //    parameters += model.Codepro.ToString().ToLower() + "&";
        //    parameters += Helpers.Constants.YandexWalletSecret_Irina + "&";
        //    parameters += model.Label;

        //    using (var sha1 = new SHA1Managed())
        //    {
        //        var bytes = Encoding.Default.GetBytes(parameters);

        //        var hashBytes = sha1.ComputeHash(bytes);

        //        return BitConverter.ToString(hashBytes).Replace("-", string.Empty).ToLower();
        //    }
        //}

        #endregion
    }
}