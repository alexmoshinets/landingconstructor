﻿using Interfaces.Account;
using Interfaces.Content;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebSite.Areas.Cabinet.Models;
using WebSite.Areas.Cabinet.Models.LandingIntegration;

namespace WebSite.Areas.Cabinet.Controllers
{
    public class SettingsController : CabinetBaseController
    {
        private readonly ILandingService _landingService;
        private readonly ILandingSettingsService _settingService;
        private readonly IUserService _userService;
        public SettingsController(ILandingService landingService, ILandingSettingsService settingService, IUserService userService)
        {
            _landingService = landingService;
            _settingService = settingService;
            _userService = userService;
        }

        // GET: Cabinet/Settings
        public ActionResult Index(string url = "")
        {
            var landing = _landingService.GetByUrl(url);
            if (landing == null)
                return HttpNotFound();
            var landingSettings = new LandingSettingsViewModel();
            landingSettings.Id = landing.Id;
            landingSettings.Url = url;           
            return View(landingSettings);
        }

        public ActionResult Setting(string url = "")
        {
            var landing = _landingService.GetByUrl(url);
            if (landing == null)
                return HttpNotFound();
            var landingSettings = new LandingSettingsViewModel();
            landingSettings.Id = landing.Id;
            landingSettings.Url = url;
            landingSettings.Email = _userService.GetByName(User.Identity.Name).Email;
            landingSettings.IsNotify = _userService.GetByName(User.Identity.Name).IsNotify;
            landingSettings.Phone = _userService.GetByName(User.Identity.Name).Phone;
            ViewBag.LandingUrl = landingSettings.Url;            
            return View(landingSettings);
        }

        public ActionResult Integration(string url = "")
        {
            var landing = _landingService.GetByUrl(url);
            if (landing == null)
                return HttpNotFound();
            var landingSettings = new LandingSettingsViewModel();
            landingSettings.LandingId = landing.Id;
            landingSettings.Url = url;

            var setting = _settingService.GetByLandingId(landing.Id);
            if (setting!=null)
            {
                var settingsModel = JsonConvert.DeserializeObject<LandingSettingsViewModel>(setting.SettingsJson);
                landingSettings = settingsModel;
                landingSettings.Id = setting.Id;
            }
            else
            {
                landingSettings.LandingId = landing.Id;
                landingSettings.Url = landing.Url;
                var jsonString = JsonConvert.SerializeObject(landingSettings);
                var newSetting = _settingService.Add(new DomainModel.Content.LandingSetting()
                {
                    LandingId = landing.Id,
                    SettingsJson = jsonString
                });
                landingSettings.Id = newSetting.Id;
            }
            
            ViewBag.LandingUrl = landingSettings.Url;
            return View(landingSettings);
        }

        public ActionResult MailSettings(string url = "")
        {
            var landing = _landingService.GetByUrl(url);
            if (landing == null)
                return HttpNotFound();
            var landingSettings = new LandingSettingsViewModel();
            landingSettings.Id = landing.Id;
            landingSettings.Url = url;
            landingSettings.Email = landing.Email;
            landingSettings.IsNotify = _userService.GetByName(User.Identity.Name).IsNotify;
            ViewBag.LandingUrl = landingSettings.Url;
            return View(landingSettings);
        }

        [HttpPost]
        public ActionResult MailSettings(string Email, string Url, int LandingId)
        {
            _landingService.SetEmail(Email, LandingId);
            return RedirectToAction("MailSettings", new { url = Url });
        }

        [HttpPost]
        public ActionResult UpdateNotify(bool value)
        {                     
                _userService.UpdateNotify(User.Identity.Name, value); 
                                
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult IntegrationBool(LandingSettingsViewModel model)
        {

            var setting = _settingService.GetById(model.Id);
            if (setting != null)
            {
                var settingsModel = JsonConvert.DeserializeObject<LandingSettingsViewModel>(setting.SettingsJson);
                settingsModel.IsAmocrm = model.IsAmocrm;
                settingsModel.IsSendpulse = model.IsSendpulse;
                settingsModel.IsMailchimp = model.IsMailchimp;
                settingsModel.IsSmsintel = model.IsSmsintel;
                var jsonString = JsonConvert.SerializeObject(settingsModel);
                _settingService.Update(new DomainModel.Content.LandingSetting()
                {
                    Id = model.Id,
                    SettingsJson = jsonString
                });
            }
            return RedirectToAction("Integration", new { url = model.Url });
        }

        [HttpPost]
        public ActionResult IntegrationAmocrm(LandingSettingsViewModel model)
        {

            var setting = _settingService.GetById(model.Id);
            if (setting != null)
            {
                var settingsModel = JsonConvert.DeserializeObject<LandingSettingsViewModel>(setting.SettingsJson);
                settingsModel.AmoCrm = new AmoCrmModel()
                {
                    SubDomain = model.AmoCrm.SubDomain,
                    UserHash = model.AmoCrm.UserHash,
                    UserLogin = model.AmoCrm.UserLogin
                };                
                var jsonString = JsonConvert.SerializeObject(settingsModel);
                _settingService.Update(new DomainModel.Content.LandingSetting()
                {
                    Id = model.Id,
                    SettingsJson = jsonString
                });
            }
            return RedirectToAction("Integration", new { url = model.Url });
        }

        [HttpPost]
        public ActionResult IntegrationGetResponse(LandingSettingsViewModel model)
        {
            var setting = _settingService.GetById(model.Id);
            if (setting != null)
            {
                var settingsModel = JsonConvert.DeserializeObject<LandingSettingsViewModel>(setting.SettingsJson);
                settingsModel.GetResponse = new GetResponse360()
                {
                    Api_key = model.GetResponse.Api_key,
                    Api_url = model.GetResponse.Api_url,
                    CampaignId = model.GetResponse.CampaignId
                };
                var jsonString = JsonConvert.SerializeObject(settingsModel);
                _settingService.Update(new DomainModel.Content.LandingSetting()
                {
                    Id = model.Id,
                    SettingsJson = jsonString
                });
            }
            return RedirectToAction("Integration", new { url = model.Url });
        }

        [HttpPost]
        public ActionResult IntegrationMailchimp(LandingSettingsViewModel model)
        {

            var setting = _settingService.GetById(model.Id);
            if (setting != null)
            {
                var settingsModel = JsonConvert.DeserializeObject<LandingSettingsViewModel>(setting.SettingsJson);
                settingsModel.MailChimp = new MailChimpModel()
                {
                    MailChimpApiKey = model.MailChimp.MailChimpApiKey,
                    ListId = model.MailChimp.ListId
                };
                var jsonString = JsonConvert.SerializeObject(settingsModel);
                _settingService.Update(new DomainModel.Content.LandingSetting()
                {
                    Id = model.Id,
                    SettingsJson = jsonString
                });
            }
            return RedirectToAction("Integration", new { url = model.Url });
        }

        [HttpPost]
        public ActionResult IntegrationSendpulse(LandingSettingsViewModel model)
        {

            var setting = _settingService.GetById(model.Id);
            if (setting != null)
            {
                var settingsModel = JsonConvert.DeserializeObject<LandingSettingsViewModel>(setting.SettingsJson);
                settingsModel.SendPulse = new SendPulseModel()
                {
                    PulseSecret = model.SendPulse.PulseSecret,
                    PulseId = model.SendPulse.PulseId,
                    BookId = model.SendPulse.BookId
                };
                var jsonString = JsonConvert.SerializeObject(settingsModel);
                _settingService.Update(new DomainModel.Content.LandingSetting()
                {
                    Id = model.Id,
                    SettingsJson = jsonString
                });
            }
            return RedirectToAction("Integration", new { url = model.Url });
        }

        [HttpPost]
        public ActionResult IntegrationBitrix(LandingSettingsViewModel model)
        {

            var setting = _settingService.GetById(model.Id);
            if (setting != null)
            {
                var settingsModel = JsonConvert.DeserializeObject<LandingSettingsViewModel>(setting.SettingsJson);
                settingsModel.Bitrix = new Bitrix24Model()
                {
                    ClientId = model.Bitrix.ClientId,
                    ClientSecret = model.Bitrix.ClientSecret                    
                };
                var jsonString = JsonConvert.SerializeObject(settingsModel);
                _settingService.Update(new DomainModel.Content.LandingSetting()
                {
                    Id = model.Id,
                    SettingsJson = jsonString
                });
            }
            return RedirectToAction("Integration", new { url = model.Url });
        }

        public ActionResult IntegrationSmsintel(LandingSettingsViewModel model)
        {
            var setting = _settingService.GetById(model.Id);
            if (setting != null)
            {
                var settingsModel = JsonConvert.DeserializeObject<LandingSettingsViewModel>(setting.SettingsJson);
                settingsModel.Smsintel = new Smsintel()
                {
                    UserLogin = model.Smsintel.UserLogin,
                    UserPassword = model.Smsintel.UserPassword,
                    GroupId = model.Smsintel.GroupId
                };
                var jsonString = JsonConvert.SerializeObject(settingsModel);
                _settingService.Update(new DomainModel.Content.LandingSetting()
                {
                    Id = model.Id,
                    SettingsJson = jsonString
                });
            }
            return RedirectToAction("Integration", new { url = model.Url });
        }

    }
}