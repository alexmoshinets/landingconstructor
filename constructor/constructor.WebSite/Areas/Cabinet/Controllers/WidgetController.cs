﻿using Interfaces.Account;
using Interfaces.Content;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using WebSite.Areas.Cabinet.Models;
using WebSite.Areas.Cabinet.Service;

namespace WebSite.Areas.Cabinet.Controllers
{
    public class WidgetController : CabinetBaseController
    {
        private readonly IWidgetService _widgetService;
        private readonly IUserService _userService;
        public WidgetController(IWidgetService widgetService, IUserService userService)
        {
            _widgetService = widgetService;
            _userService = userService;
        }

        // GET: Cabinet/Widget
        public ActionResult Index()
        {
            var result = _widgetService.GetWidgetsUser(_userService.GetByName(User.Identity.Name).Id);
            var model = result.Select(WidgetViewModel.FromDomainModel).ToList();
            return View(model);
        }

        public ActionResult Add()
        {
            var widget = new WidgetViewModel();
            if (TempData["File"] != null)
            {
                widget.Script = TempData["File"].ToString();
            }
            return View(widget);
        }

        public ActionResult Edit(int id)
        {
            var widget = _widgetService.GetById(id);
            var model = WidgetViewModel.FromDomainModel(widget);            
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            var widget = _widgetService.Delete(id);            
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditWidget(WidgetViewModel model)
        {
            var widget = _widgetService.GetById(model.Id);
            widget.Description = model.Description;
            widget.Title = model.Title;
            widget.Script = model.Script;
            _widgetService.Update(widget);
            var modelWidget = WidgetViewModel.FromDomainModel(widget);
            return RedirectToAction("Edit", new { id = model.Id });
        }

        [HttpPost]
        public ActionResult ImportWidget(HttpPostedFileBase file)
        {
            if (file != null)
            {
                BinaryReader b = new BinaryReader(file.InputStream);
                byte[] binData = b.ReadBytes((int)file.InputStream.Length);                
                string result = System.Text.Encoding.UTF8.GetString(binData);
                //XmlReader reader = XmlReader.Create(file.FileName);
                TempData["File"] = result;
            }
            return RedirectToAction("Add");
        }
                
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Save(WidgetViewModel model)
        {
            //XmlParser parser = new XmlParser();
            //var result = parser.ParseFile(model.Script);
            var widget = _widgetService.Add(new DomainModel.Content.Widget()
            {
                Description = model.Description,
                CreateDate = DateTime.Now,
                Script = model.Script,
                Title = model.Title,
                UserId = _userService.GetByName(User.Identity.Name).Id
            });
            return Json(widget, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]        
        public ActionResult GetWidget(int id)
        {
            var widget = _widgetService.GetById(id);           
            var modelWidget = WidgetViewModel.FromDomainModel(widget);
            XmlParser parser = new XmlParser();
            var result = parser.ParseFile(widget.Script);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetCodeWidget(int id, string text)
        {
            string[] stringSeparators = new string[] { "||" };
            string[] list;
            var widget = _widgetService.GetById(id);
            XmlParser parser = new XmlParser();
            var result = parser.ParseFile(widget.Script);
            var invertResult = result.BodyEndHTML;

            if (!string.IsNullOrEmpty(text))
            {
                list = text.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                foreach (var item in list)
                {
                    var itemList = item.Split('|');
                    if (itemList[0] == "info")
                    {
                        invertResult = invertResult.Replace("{" + itemList[1] + "}", itemList[2]);
                    }
                    if (itemList[0] == "list")
                    {
                        invertResult = invertResult.Replace("{" + itemList[1] + "}", itemList[2]);
                    }
                    if (itemList[0] == "bool")
                    {
                        invertResult = invertResult.Replace("{" + itemList[1] + "}", itemList[2]);
                    }
                    if (itemList[0] == "builtin")
                    {
                        invertResult = invertResult.Replace("{" + itemList[1] + "}", itemList[2]);
                    }
                    if (itemList[0] == "url")
                    {
                        var name = itemList[1] + ".";
                        invertResult = invertResult.Replace("{" + name + "label" + "}", itemList[2]);
                        invertResult = invertResult.Replace("{" + name + "url" + "}", itemList[3]);
                    }
                }
            }
                                   
            //удаляем теги скрипта
            invertResult = invertResult.Replace("<script>", "try { ");
            invertResult = invertResult.Replace("<script type=\"text/javascript\">", "try { ");
            invertResult = invertResult.Replace("</script>", " } catch(ex) {console.log(ex);}");
            return Json(new { script = invertResult, html = result.PageItemHTML }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]  
        public ActionResult Widgets()
        {
            var widgets = _widgetService.GetAll();
            var model = widgets.Select(WidgetViewModel.FromDomainModel).ToList();
            return View(model);
        }
    }
}