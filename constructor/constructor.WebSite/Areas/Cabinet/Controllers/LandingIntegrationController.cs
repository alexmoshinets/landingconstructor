﻿using DomainModel.Content;
using Interfaces.Content;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebSite.Areas.Cabinet.Models.LandingIntegration;

namespace WebSite.Areas.Cabinet.Controllers
{
    public class LandingIntegrationController : CabinetBaseController
    {
        private readonly ILandingIntegrationService _landingIntegrationService;
        private readonly ILandingService _landingService;
        public LandingIntegrationController(ILandingIntegrationService landingIntegrationService, ILandingService landingService)
        {
            _landingIntegrationService = landingIntegrationService;
            _landingService = landingService;

        }

        // GET: Cabinet/LandingIntegration
        public ActionResult Index(string id, string url)
        {
            var model = new IntegrationModel();
            model.FormId = id;
            model.url = url;
            model.LandingId = _landingService.GetByUrl(url).Id;
            var jsonSettingsModel = _landingIntegrationService.GetByLandingId(id);
            if (jsonSettingsModel!=null)
            {                
                switch (jsonSettingsModel.TypeService)
                {
                    case (int)TypeServiceIntegrationEnum.Amocrm:
                        {
                            var amocrmSettingsModel = JsonConvert.DeserializeObject<AmoCrmModel>(jsonSettingsModel.SettingsJson);
                            model.TypeIntegration = TypeServiceIntegrationEnum.Amocrm;                            
                            model.AmoCrmModel = amocrmSettingsModel;
                            break;
                        }
                    case (int)TypeServiceIntegrationEnum.Bitrix24:
                        {
                            var bitrixSettingsModel = JsonConvert.DeserializeObject<Bitrix24Model>(jsonSettingsModel.SettingsJson);
                            model.TypeIntegration = TypeServiceIntegrationEnum.Bitrix24;                            
                            model.Bitrix24Model = bitrixSettingsModel;
                            break;
                        }
                    case (int)TypeServiceIntegrationEnum.MailChimp:
                        {
                            var mailChimpSettingsModel = JsonConvert.DeserializeObject<MailChimpModel>(jsonSettingsModel.SettingsJson);
                            model.TypeIntegration = TypeServiceIntegrationEnum.MailChimp;                            
                            model.MailChimpModel = mailChimpSettingsModel;
                            break;
                        }
                    default:
                        break;
                }
                
            }                        
            return View(model);
        }

        
        [HttpPost]
        public ActionResult Index(IntegrationModel model)
        {
            switch(model.TypeIntegration)
            {
                case TypeServiceIntegrationEnum.Amocrm:
                    {
                        AddAmocrmIntegration(model.AmoCrmModel, model.FormId, model.LandingId.Value);
                        break;
                    }
                case TypeServiceIntegrationEnum.Bitrix24:
                    {
                        AddBitrixIntegration(model.Bitrix24Model, model.FormId);
                        break;
                    }
                case TypeServiceIntegrationEnum.MailChimp:
                    {
                        AddMailChimpIntegration(model.MailChimpModel, model.FormId);
                        break;
                    }
                default:
                    break;
            }

            return RedirectToAction("Index", "LandingIntegration", new { id = model.FormId, url = model.url });
        }
        
        public void AddAmocrmIntegration(AmoCrmModel model, string formId, int LandingId)
        {
            var result = _landingIntegrationService.GetByLandingId(formId);

            var amocrmModel = new AmoCrmModel()
            {
                SubDomain = model.SubDomain,
                UserHash = model.UserHash,
                UserLogin = model.UserLogin,
                utm_campaign = model.utm_campaign,
                utm_content = model.utm_content,
                UtmString = model.UtmString,
                utm_medium = model.utm_medium,
                utm_source = model.utm_source,
                utm_term = model.utm_term,
                ScriptCode = model.ScriptCode
            };
            var jsonString = JsonConvert.SerializeObject(amocrmModel);
            var landingModel = new LandingIntegration()
            {
                FormId = formId,
                LandingId = LandingId,
                TypeService = (int)TypeServiceIntegrationEnum.Amocrm,
                SettingsJson = jsonString
            };

            if (result != null)
            {
                _landingIntegrationService.Update(landingModel);
            }
            else
            {
                _landingIntegrationService.Add(landingModel);
            }
        }

        public void AddBitrixIntegration(Bitrix24Model model, string formId)
        {
            var result = _landingIntegrationService.GetByLandingId(formId);

            var bitrixModel = new Bitrix24Model()
            {
                ClientId = model.ClientId,
                ClientSecret = model.ClientSecret,
                utm_campaign = model.utm_campaign,
                utm_content = model.utm_content,
                UtmString = model.UtmString,
                utm_medium = model.utm_medium,
                utm_source = model.utm_source,
                utm_term = model.utm_term,
                ScriptCode = model.ScriptCode
            };
            var jsonString = JsonConvert.SerializeObject(bitrixModel);
            var landingModel = new LandingIntegration()
            {
                FormId = formId,
                LandingId = result.LandingId,
                TypeService = (int)TypeServiceIntegrationEnum.Bitrix24,
                SettingsJson = jsonString
            };

            if (result != null)
            {
                _landingIntegrationService.Update(landingModel);
            }
            else
            {
                _landingIntegrationService.Add(landingModel);
            }
        }

        public void AddMailChimpIntegration(MailChimpModel model, string formId)
        {
            var result = _landingIntegrationService.GetByLandingId(formId);

            var mailchimpModel = new MailChimpModel()
            {
                ListId = model.ListId,
                MailChimpApiKey = model.MailChimpApiKey,
                utm_campaign = model.utm_campaign,
                utm_content = model.utm_content,
                UtmString = model.UtmString,
                utm_medium = model.utm_medium,
                utm_source = model.utm_source,
                utm_term = model.utm_term,
                ScriptCode = model.ScriptCode
            };
            var jsonString = JsonConvert.SerializeObject(mailchimpModel);
            var landingModel = new LandingIntegration()
            {
                FormId = formId,
                LandingId = result.LandingId,
                TypeService = (int)TypeServiceIntegrationEnum.MailChimp,
                SettingsJson = jsonString
            };

            if (result != null)
            {
                _landingIntegrationService.Update(landingModel);
            }
            else
            {
                _landingIntegrationService.Add(landingModel);
            }
        }

        [HttpPost]
        public ActionResult GetAmocrm(string formId, int LandingId)
        {
            var landing = _landingService.GetById(LandingId);
            if (landing != null)
            {
                var result = _landingIntegrationService.GetByLandingId(formId);
                if (result != null && result.TypeService == 0)
                {
                    var amocrmSettingsModel = JsonConvert.DeserializeObject<AmoCrmModel>(result.SettingsJson);
                    try
                    {
                        var amocrmModel = new AmoCrmModel()
                        {
                            SubDomain = amocrmSettingsModel.SubDomain,
                            UserHash = amocrmSettingsModel.UserHash,
                            UserLogin = amocrmSettingsModel.UserLogin,
                            IsRedirect = result.IsRedirect,
                            AlertMessage = result.AlertMessage,
                            RedirectUrl = result.RedirectUrl,
                            SendUtm = amocrmSettingsModel.SendUtm,
                            ScriptCode = amocrmSettingsModel.ScriptCode
                        };
                        return Json(amocrmModel, JsonRequestBehavior.AllowGet);
                    }
                    catch
                    {
                        return Json("505", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json("505", JsonRequestBehavior.AllowGet);
            }
            return Json("404", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSmsintel(string formId, int LandingId)
        {
            var landing = _landingService.GetById(LandingId);
            if (landing != null)
            {
                var result = _landingIntegrationService.GetByLandingId(formId);
                if (result != null && result.TypeService == 4)
                {
                    var amocrmSettingsModel = JsonConvert.DeserializeObject<Smsintel>(result.SettingsJson);
                    try
                    {
                        var amocrmModel = new Smsintel()
                        {
                            UserPassword = amocrmSettingsModel.UserPassword,
                            UserLogin = amocrmSettingsModel.UserLogin,
                            GroupId = amocrmSettingsModel.GroupId,
                            IsRedirect = result.IsRedirect,
                            AlertMessage = result.AlertMessage,
                            RedirectUrl = result.RedirectUrl,
                            SendUtm = amocrmSettingsModel.SendUtm,
                            ScriptCode = amocrmSettingsModel.ScriptCode
                        };
                        return Json(amocrmModel, JsonRequestBehavior.AllowGet);
                    }
                    catch
                    {
                        return Json("505", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json("505", JsonRequestBehavior.AllowGet);
            }
            return Json("404", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddSmsintel(Smsintel model, string formId, int LandingId, bool isRedirect, string stringIntegration)
        {
            var result = _landingIntegrationService.GetByLandingId(formId);

            var amocrmModel = new Smsintel()
            {
                UserPassword = model.UserPassword,
                UserLogin = model.UserLogin,
                GroupId = model.GroupId,
                SendUtm = model.SendUtm,
                ScriptCode = model.ScriptCode
            };
            var jsonString = JsonConvert.SerializeObject(amocrmModel);
            var landingModel = new LandingIntegration()
            {
                FormId = formId,
                LandingId = LandingId,
                TypeService = (int)TypeServiceIntegrationEnum.Smsintel,
                SettingsJson = jsonString,
                IsRedirect = isRedirect
            };
            if (landingModel.IsRedirect)
            {
                landingModel.RedirectUrl = stringIntegration;
            }
            else
            {
                landingModel.AlertMessage = stringIntegration;
            }

            if (result != null)
            {
                _landingIntegrationService.Update(landingModel);
            }
            else
            {
                _landingIntegrationService.Add(landingModel);
            }
            return Json("ok", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetFormType(string formId)
        {
            var result = _landingIntegrationService.GetByLandingId(formId);
            if (result != null)
            {
                return Json(result.TypeService, JsonRequestBehavior.AllowGet);
            }
            return Json(-1, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSendUtm(string formId)
        {
            var result = _landingIntegrationService.GetByLandingId(formId);
            if (result != null)
            {
                if (result.TypeService == 0)
                {
                    var amocrmSettingsModel = JsonConvert.DeserializeObject<AmoCrmModel>(result.SettingsJson);
                    return Json(amocrmSettingsModel.SendUtm, JsonRequestBehavior.AllowGet);
                }
                if (result.TypeService == 1)
                {
                    var amocrmSettingsModel = JsonConvert.DeserializeObject<Bitrix24Model>(result.SettingsJson);
                    return Json(amocrmSettingsModel.SendUtm, JsonRequestBehavior.AllowGet);
                }
                if (result.TypeService == 2)
                {
                    var amocrmSettingsModel = JsonConvert.DeserializeObject<MailChimpModel>(result.SettingsJson);
                    return Json(amocrmSettingsModel.SendUtm, JsonRequestBehavior.AllowGet);
                }
                if (result.TypeService == 3)
                {
                    var amocrmSettingsModel = JsonConvert.DeserializeObject<SendPulseModel>(result.SettingsJson);
                    return Json(amocrmSettingsModel.SendUtm, JsonRequestBehavior.AllowGet);
                }
                if (result.TypeService == 5)
                {
                    var amocrmSettingsModel = JsonConvert.DeserializeObject<GetResponse360>(result.SettingsJson);
                    return Json(amocrmSettingsModel.SendUtm, JsonRequestBehavior.AllowGet);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddAmocrm(AmoCrmModel model, string formId, int LandingId, bool isRedirect, string stringIntegration)
        {
            var result = _landingIntegrationService.GetByLandingId(formId);

            var amocrmModel = new AmoCrmModel()
            {
                SubDomain = model.SubDomain,
                UserHash = model.UserHash,
                UserLogin = model.UserLogin,
                SendUtm = model.SendUtm,
                ScriptCode = model.ScriptCode
            };
            var jsonString = JsonConvert.SerializeObject(amocrmModel);
            var landingModel = new LandingIntegration()
            {
                FormId = formId,
                LandingId = LandingId,
                TypeService = (int)TypeServiceIntegrationEnum.Amocrm,
                SettingsJson = jsonString,
                IsRedirect = isRedirect                
            };
            if(landingModel.IsRedirect)
            {
                landingModel.RedirectUrl = stringIntegration;
            }
            else
            {
                landingModel.AlertMessage = stringIntegration;
            }

            if (result != null)
            {
                _landingIntegrationService.Update(landingModel);
            }
            else
            {
                _landingIntegrationService.Add(landingModel);
            }
            return Json("ok", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetBitrix(string formId, int LandingId)
        {
            var landing = _landingService.GetById(LandingId);
            if (landing != null)
            {
                var result = _landingIntegrationService.GetByLandingId(formId);
                if (result != null && result.TypeService == 1)
                {
                    var amocrmSettingsModel = JsonConvert.DeserializeObject<Bitrix24Model>(result.SettingsJson);
                    try
                    {
                        var amocrmModel = new Bitrix24Model()
                        {
                            ClientId = amocrmSettingsModel.ClientId,
                            ClientSecret = amocrmSettingsModel.ClientSecret,
                            IsRedirect = result.IsRedirect,
                            AlertMessage = result.AlertMessage,
                            RedirectUrl = result.RedirectUrl,
                            SendUtm = amocrmSettingsModel.SendUtm,
                            ScriptCode = amocrmSettingsModel.ScriptCode
                        };
                        return Json(amocrmModel, JsonRequestBehavior.AllowGet);
                    }
                    catch
                    {
                        return Json("505", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json("505", JsonRequestBehavior.AllowGet);
            }
            return Json("404", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddBitrix(Bitrix24Model model, string formId, int LandingId, bool isRedirect, string stringIntegration)
        {
            var result = _landingIntegrationService.GetByLandingId(formId);

            var amocrmModel = new Bitrix24Model()
            {
                ClientId = model.ClientId,
                ClientSecret = model.ClientSecret,
                SendUtm = model.SendUtm,
                ScriptCode = model.ScriptCode
            };
            var jsonString = JsonConvert.SerializeObject(amocrmModel);
            var landingModel = new LandingIntegration()
            {
                FormId = formId,
                LandingId = LandingId,
                TypeService = (int)TypeServiceIntegrationEnum.Bitrix24,
                SettingsJson = jsonString,
                IsRedirect = isRedirect
            };
            if (landingModel.IsRedirect)
            {
                landingModel.RedirectUrl = stringIntegration;
            }
            else
            {
                landingModel.AlertMessage = stringIntegration;
            }

            if (result != null)
            {
                _landingIntegrationService.Update(landingModel);
            }
            else
            {
                _landingIntegrationService.Add(landingModel);
            }
            return Json("ok", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSendPulse(string formId, int LandingId)
        {
            var landing = _landingService.GetById(LandingId);
            if (landing != null)
            {
                var result = _landingIntegrationService.GetByLandingId(formId);
                if (result != null && result.TypeService == 3)
                {
                    var amocrmSettingsModel = JsonConvert.DeserializeObject<SendPulseModel>(result.SettingsJson);
                    try
                    {
                        var amocrmModel = new SendPulseModel()
                        {
                            PulseId = amocrmSettingsModel.PulseId,
                            PulseSecret = amocrmSettingsModel.PulseSecret,
                            IsRedirect = result.IsRedirect,
                            BookId = amocrmSettingsModel.BookId,
                            AlertMessage = result.AlertMessage,
                            RedirectUrl = result.RedirectUrl,
                            SendUtm = amocrmSettingsModel.SendUtm,
                            ScriptCode = amocrmSettingsModel.ScriptCode
                        };
                        return Json(amocrmModel, JsonRequestBehavior.AllowGet);
                    }
                    catch
                    {
                        return Json("505", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json("505", JsonRequestBehavior.AllowGet);
            }
            return Json("404", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddSendPulse(SendPulseModel model, string formId, int LandingId, bool isRedirect, string stringIntegration)
        {
            var result = _landingIntegrationService.GetByLandingId(formId);

            var amocrmModel = new SendPulseModel()
            {
                PulseId = model.PulseId,
                PulseSecret = model.PulseSecret,
                BookId = model.BookId,
                SendUtm = model.SendUtm,
                ScriptCode = model.ScriptCode
            };

            var jsonString = JsonConvert.SerializeObject(amocrmModel);
            var landingModel = new LandingIntegration()
            {
                FormId = formId,
                LandingId = LandingId,
                TypeService = (int)TypeServiceIntegrationEnum.SendPulse,
                SettingsJson = jsonString,
                IsRedirect = isRedirect
            };
            if (landingModel.IsRedirect)
            {
                landingModel.RedirectUrl = stringIntegration;
            }
            else
            {
                landingModel.AlertMessage = stringIntegration;
            }

            if (result != null)
            {
                _landingIntegrationService.Update(landingModel);
            }
            else
            {
                _landingIntegrationService.Add(landingModel);
            }
            return Json("ok", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetMailChimp(string formId, int LandingId)
        {
            var landing = _landingService.GetById(LandingId);
            if (landing != null)
            {
                var result = _landingIntegrationService.GetByLandingId(formId);
                if (result != null && result.TypeService == 2)
                {
                    var amocrmSettingsModel = JsonConvert.DeserializeObject<MailChimpModel>(result.SettingsJson);
                    try
                    {
                        var amocrmModel = new MailChimpModel()
                        {
                            ListId = amocrmSettingsModel.ListId,
                            MailChimpApiKey = amocrmSettingsModel.MailChimpApiKey,
                            IsRedirect = result.IsRedirect,
                            AlertMessage = result.AlertMessage,
                            RedirectUrl = result.RedirectUrl,
                            SendUtm = amocrmSettingsModel.SendUtm,
                            ScriptCode = amocrmSettingsModel.ScriptCode
                        };
                        return Json(amocrmModel, JsonRequestBehavior.AllowGet);
                    }
                    catch
                    {
                        return Json("505", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json("505", JsonRequestBehavior.AllowGet);
            }
            return Json("404", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddGetResponse(GetResponse360 model, string formId, int LandingId, bool isRedirect, string stringIntegration)
        {
            var result = _landingIntegrationService.GetByLandingId(formId);

            var amocrmModel = new GetResponse360()
            {
                Api_key = model.Api_key,
                Api_url = model.Api_url, 
                CampaignId = model.CampaignId,               
                SendUtm = model.SendUtm,
                ScriptCode = model.ScriptCode
            };

            var jsonString = JsonConvert.SerializeObject(amocrmModel);
            var landingModel = new LandingIntegration()
            {
                FormId = formId,
                LandingId = LandingId,
                TypeService = (int)TypeServiceIntegrationEnum.GetResponse360,
                SettingsJson = jsonString,
                IsRedirect = isRedirect
            };
            if (landingModel.IsRedirect)
            {
                landingModel.RedirectUrl = stringIntegration;
            }
            else
            {
                landingModel.AlertMessage = stringIntegration;
            }

            if (result != null)
            {
                _landingIntegrationService.Update(landingModel);
            }
            else
            {
                _landingIntegrationService.Add(landingModel);
            }
            return Json("ok", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetGetResponse(string formId, int LandingId)
        {
            var landing = _landingService.GetById(LandingId);
            if (landing != null)
            {
                var result = _landingIntegrationService.GetByLandingId(formId);
                if (result != null && result.TypeService == 5)
                {
                    var amocrmSettingsModel = JsonConvert.DeserializeObject<GetResponse360>(result.SettingsJson);
                    try
                    {
                        var amocrmModel = new GetResponse360()
                        {
                            Api_key = amocrmSettingsModel.Api_key,
                            Api_url = amocrmSettingsModel.Api_url,
                            CampaignId = amocrmSettingsModel.CampaignId,
                            IsRedirect = result.IsRedirect,
                            AlertMessage = result.AlertMessage,
                            RedirectUrl = result.RedirectUrl,
                            SendUtm = amocrmSettingsModel.SendUtm,
                            ScriptCode = amocrmSettingsModel.ScriptCode
                        };
                        return Json(amocrmModel, JsonRequestBehavior.AllowGet);
                    }
                    catch
                    {
                        return Json("505", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json("505", JsonRequestBehavior.AllowGet);
            }
            return Json("404", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddMailChimp(MailChimpModel model, string formId, int LandingId, bool isRedirect, string stringIntegration)
        {
            var result = _landingIntegrationService.GetByLandingId(formId);

            var amocrmModel = new MailChimpModel()
            {
                MailChimpApiKey = model.MailChimpApiKey,
                ListId = model.ListId,
                SendUtm = model.SendUtm,
                ScriptCode = model.ScriptCode
            };

            var jsonString = JsonConvert.SerializeObject(amocrmModel);
            var landingModel = new LandingIntegration()
            {
                FormId = formId,
                LandingId = LandingId,
                TypeService = (int)TypeServiceIntegrationEnum.MailChimp,
                SettingsJson = jsonString,
                IsRedirect = isRedirect
            };
            if (landingModel.IsRedirect)
            {
                landingModel.RedirectUrl = stringIntegration;
            }
            else
            {
                landingModel.AlertMessage = stringIntegration;
            }

            if (result != null)
            {
                _landingIntegrationService.Update(landingModel);
            }
            else
            {
                _landingIntegrationService.Add(landingModel);
            }
            return Json("ok", JsonRequestBehavior.AllowGet);
        }

    }
}