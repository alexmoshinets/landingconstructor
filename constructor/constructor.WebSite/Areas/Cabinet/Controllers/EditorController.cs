﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebSite.Areas.Cabinet.Framework;
using DomainModel.Content;
using Interfaces.Content;
using WebSite.Areas.Cabinet.Models;
using Encoder = System.Text.Encoder;
using Interfaces.Account;



namespace WebSite.Areas.Cabinet.Controllers
{
    public class EditorController : CabinetBaseController
    {
        private readonly ILandingService _landingService;
        private readonly IRoleService _roleService;
        private readonly ISectionsService _sectionsService;
        private readonly ISectionsCategoryService _sectionsCategoryService;
        private readonly IUserService _userService;
        public EditorController(ILandingService landingService, IRoleService roleService, ISectionsService sectionsService,
            ISectionsCategoryService sectionsCategoryService, IUserService userService)
        {
            _landingService = landingService;
            _roleService = roleService;
            _sectionsService = sectionsService;
            _sectionsCategoryService = sectionsCategoryService;
            _userService = userService;
        }

        public ActionResult Index(string url = "", string name = "", string template = "")
        {


            var landingsCurr = new List<Landing>();
            Session["Steps"] = new List<StepsViewModel>();
            var ListSes = new List<StepsViewModel>();
            var Ses = new StepsViewModel();
            var landings = new List<LandingViewModel>();
            var model = new EditorInfoViewModel();
            var membershipUser = Membership.GetUser();
            var userRole = membershipUser != null && _roleService.IsUserInRole(membershipUser.UserName, "sadmin");
            ViewBag.UserRole = userRole ? "sadmin" : "editor";
            if (membershipUser != null)
            {
                var userId = membershipUser.ProviderUserKey;
                if (userId != null)
                {

                    if (string.IsNullOrEmpty(url))
                    {
                        model.Url = "";
                    }
                    else
                    {
                        var findlanding = _landingService.GetByUrlAndUserId(url, Int32.Parse(userId.ToString()));
                        if (url != "" && name != "" && template != "" && findlanding.Count == 0)
                        {
                            var templateLanding = _landingService.GetAll().Where(_ => _.IsTemplate).Where(_ => _.Url == url);
                            foreach (var item in templateLanding)
                            {
                                item.IsTemplate = false;
                                item.Url = url;
                                item.Name = name;
                                item.UserId = Int32.Parse(userId.ToString());
                                _landingService.Add(item);
                            }

                        }

                        var pages = _landingService.GetByUrlAndUserId(url, Int32.Parse(userId.ToString()));
                        if (pages == null || pages.Count == 0)
                        {
                            var layout = new Landing
                            {
                                Title = "Шаблон",
                                StringKey = "Layout",
                                Type = "layout",
                                MetaDescription = "",
                                MetaKeyWords = "",
                                SiteContent = "&lt;div id=&quot;section_1&quot; class=&quot;section active&quot; style=&quot;width: 95%; height: 300px; position:relative; margin: 0 auto; background: rgb(255, 255, 255);&quot; type=&quot;section&quot; m-title=&quot;section_1&quot;&gt;&lt;div id=&quot;content_section_1&quot; class=&quot;container_section&quot; style=&quot;width: 100%; text-align: left; margin: 0 auto;&quot;&gt;&lt;div class=&quot;column1 column&quot; id=&quot;c20479&quot; style=&quot;height: 300px; position: relative; display: inline-block;&quot;&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;sectionContent section&quot; id=&quot;sectionContent&quot;&gt;&lt;table style=&quot;width: 100%; height: 100%;&quot;&gt;&lt;tbody&gt;&lt;tr&gt;&lt;td align=&quot;center&quot; valign=&quot;center&quot; style=&quot;font-size: 30px; color: #aaa;&quot;&gt;Содержание страниц&lt;/td&gt;&lt;/tr&gt;&lt;/tbody&gt;&lt;/table&gt;&lt;/div&gt;",
                                Name = name,
                                Url = url,
                                Styles = "",
                                Links = "",
                                UserId = Int32.Parse(userId.ToString()),
                                PageLoader = "",
                                Created = DateTime.Now
                            };
                            pages.Add(layout);
                            _landingService.AddOrUpdate(url, layout, landing =>
                            {
                                landing.Title = layout.Title;
                                landing.StringKey = layout.StringKey;
                                landing.Type = layout.Type;
                                landing.MetaDescription = layout.MetaDescription;
                                landing.MetaKeyWords = layout.MetaKeyWords;
                                landing.SiteContent = layout.SiteContent;
                                landing.Name = layout.Name;
                                landing.Url = layout.Url;
                                landing.Styles = layout.Styles;
                                landing.Links = layout.Links;
                                landing.PageLoader = layout.PageLoader;
                                landing.UserId = layout.UserId;
                                landing.Created = layout.Created;
                            });
                            var home = new Landing
                            {
                                Title = "Главная",
                                StringKey = "Home",
                                Type = "home",
                                MetaDescription = "",
                                MetaKeyWords = "",
                                SiteContent = "&lt;div id=&quot;section_2&quot; class=&quot;section&quot; style=&quot;width: 95%; height: 300px; position:relative; margin: 0 auto; background: rgb(255, 255, 255);&quot; type=&quot;section&quot; m-title=&quot;section_2&quot;&gt;&lt;div id=&quot;content_section_2&quot; class=&quot;container_section&quot; style=&quot;width: 100%; text-align: left; margin: 0 auto;&quot;&gt;&lt;div class=&quot;column2 column&quot; id=&quot;c55317&quot; style=&quot;height: 300px; float: left; position: relative; display: inline-block;&quot;&gt;&lt;/div&gt;&lt;div class=&quot;column2 column&quot; id=&quot;c45789&quot; style=&quot;height: 300px; float: left; position: relative; display: inline-block;&quot;&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;",
                                Name = name,
                                Url = url,
                                Styles = "",
                                Links = "",
                                UserId = Int32.Parse(userId.ToString()),
                                PageLoader = "",
                                Created = DateTime.Now
                            };
                            pages.Add(home);
                            _landingService.AddOrUpdate(url, home, landing =>
                            {
                                landing.Title = home.Title;
                                landing.StringKey = home.StringKey;
                                landing.Type = home.Type;
                                landing.MetaDescription = home.MetaDescription;
                                landing.MetaKeyWords = home.MetaKeyWords;
                                landing.SiteContent = home.SiteContent;
                                landing.Name = home.Name;
                                landing.Url = home.Url;
                                landing.Styles = home.Styles;
                                landing.Links = home.Links;
                                landing.PageLoader = home.PageLoader;
                                landing.UserId = home.UserId;
                                landing.Created = home.Created;
                            });
                            var l = _landingService.GetByUrlAndUserId(url, Int32.Parse(userId.ToString())).FirstOrDefault(_ => _.StringKey == "Layout");
                            landingsCurr.Add(l);
                        }
                        else
                        {
                            landingsCurr.Add(pages[0]);
                        }
                        foreach (var item in pages)
                        {
                            var mod = new LandingViewModel
                            {
                                Title = item.Title,
                                StringKey = item.StringKey,
                                Type = item.Type,
                                MetaDescription = item.MetaDescription,
                                MetaKeyWords = item.MetaKeyWords,
                                SiteContent = item.SiteContent,
                                Name = item.Name,
                                Url = item.Url,
                                Styles = item.Styles,
                                Links = item.Links,
                                UserId = item.UserId,
                                PageLoader = item.PageLoader,
                                Created = item.Created
                            };
                            landings.Add(mod);
                        }
                        model.Url = pages.Count > 0 ? url : "";

                    }
                    Ses.Landings = new List<LandingViewModel>();
                    Ses.Landings.AddRange(landings);
                    Ses.CurentTab = "Home";
                    Ses.CurrentNumber = 1;
                    Ses.Time = DateTime.Now;
                    var landingViewModel = landings.FirstOrDefault();
                    if (landingViewModel != null) Ses.Url = landingViewModel.Url;
                    ListSes.Add(Ses);
                    Session["Steps"] = ListSes;
                }
            }
            else
            {
                model.Url = "";
            }
            var step = new List<StepsViewModel>();
            step = Session["Steps"] as List<StepsViewModel>;
            try
            {
                ViewBag.LandingId = landingsCurr[0].Id;
            }
            catch
            {
                ViewBag.LandingId = -1;
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult Fonts()
        {
            var fonts = FontFamily.Families.Where(font => font != null).Select(font => font.Name).ToList();
            return View(fonts);
        }

        [HttpPost]
        public ActionResult LoadImages(string patch, string type)
        {
            var userRole = false;
            var user = Membership.GetUser();
            var patchCurrent = "";
            var currentPatch = "";
            if (user != null)
            {
                userRole = _roleService.IsUserInRole(user.UserName, "sadmin");
            }
            if (!Directory.Exists(Server.MapPath(WorkContext.UserImagesPath)))
            {
                Directory.CreateDirectory(Server.MapPath(WorkContext.UserImagesPath));
            }
            if (!Directory.Exists(Server.MapPath(WorkContext.UserImagesPath) + user.UserName + "/"))
            {
                Directory.CreateDirectory(Server.MapPath(WorkContext.UserImagesPath) + user.UserName + "/");
            }
            if (!userRole && patch != "TemplatesImage" &&
                !Directory.Exists(Server.MapPath(WorkContext.UserImagesPath) + user.UserName + "/" + patch))
            {
                Directory.CreateDirectory(Server.MapPath(WorkContext.UserImagesPath) + user.UserName + "/" + patch);
                patchCurrent = Server.MapPath(WorkContext.UserImagesPath) + user.UserName + "/" + patch;
                currentPatch = WorkContext.UserImagesPath + user.UserName + "/" + patch;
            }
            else
            {
                patchCurrent = Server.MapPath(WorkContext.TemplatesImagePath);
                currentPatch = WorkContext.TemplatesImagePath;
            }
            if (user != null && type == "user")
            {
                var dir = new DirectoryInfo(patchCurrent);// папка с файлами 
                var files = dir.GetFiles().Select(file => currentPatch + "/" + file.Name).ToArray(); // список для имен файлов 
                return Json(new { res = files, state = "ok" });
            }
            else
            {
                var dir = new DirectoryInfo(patchCurrent);// папка с файлами 
                var files = dir.GetFiles().Select(file => currentPatch + "/" + file.Name).ToArray(); // список для имен файлов 
                return Json(new { res = files, state = "ok" });
            }

        }

        [HttpPost]
        public JsonResult LoadPages(string url)
        {
            var membershipUser = Membership.GetUser();
            if (membershipUser != null)
            {
                var userId = membershipUser.ProviderUserKey;
                if (userId != null)
                {
                    var pages = _landingService.GetByUrlAndUserId(url, Int32.Parse(userId.ToString()));
                    var modelS = new StepsViewModel();
                    modelS.Landings = new List<LandingViewModel>();
                    var modelH = pages.Select(LandingViewModel.FromDomainModel).ToList();
                    modelS.Landings.AddRange(modelH);
                    var landingViewModel = modelS.Landings.FirstOrDefault();
                    if (landingViewModel != null) modelS.Url = landingViewModel.Url;
                    modelS.CurentTab = "Home";
                    modelS.Time = DateTime.Now;
                    //if (Session["Steps"] == null)
                    //{
                    //    Session["Steps"] = new List<StepsViewModel>();
                    //}
                    //Session["Steps"] = modelS;
                    return Json(new { res = pages, state = "ok!" });

                }
            }
            return Json(new { res = "", state = "empty" });
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult StepNext(int currentNumber)
        {
            if (Session["Steps"] == null)
            {
                Session["Steps"] = new List<StepsViewModel>();
            }
            var steps = Session["Steps"] as List<StepsViewModel> ?? new List<StepsViewModel>(); ;
            var landing = steps.FirstOrDefault(_ => _.CurrentNumber == currentNumber + 1) ?? new StepsViewModel();
            var membershipUser = Membership.GetUser();
            if (membershipUser != null)
            {
                var userId = membershipUser.ProviderUserKey;
                if (userId != null)
                {
                    var pages = landing.Landings;
                    var tot = steps.Count;
                    var cur = currentNumber + 1;
                    var currentT = landing.CurentTab;
                    return Json(new { res = pages, total = tot, current = cur, currentTab = currentT });
                }
            }
            return Json(new { res = "", total = 0, current = 0, currentTab = "Home" });
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult StepBack(int currentNumber)
        {
            if (Session["Steps"] == null)
            {
                Session["Steps"] = new List<StepsViewModel>();
            }
            var steps = Session["Steps"] as List<StepsViewModel> ?? new List<StepsViewModel>(); ;
            var landing = steps.FirstOrDefault(_ => _.CurrentNumber == currentNumber - 1) ?? new StepsViewModel();
            var membershipUser = Membership.GetUser();
            if (membershipUser != null)
            {
                var userId = membershipUser.ProviderUserKey;
                if (userId != null)
                {
                    var pages = landing.Landings;
                    var tot = steps.Count;
                    var cur = currentNumber - 1;
                    var currentT = landing.CurentTab;
                    return Json(new { res = pages, total = tot, current = cur, currentTab = currentT });
                }
            }
            return Json(new { res = "", total = 0, current = 0, currentTab = "Home" });
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult SaveStep(Array page, string currentTab, int currentNumber, string url, string parametr)
        {
            if (Session["Steps"] == null) Session["Steps"] = new List<StepsViewModel>();
            var step = Session["Steps"] as List<StepsViewModel> ?? new List<StepsViewModel>();
            var firstOrDefault = step.FirstOrDefault();
            if (firstOrDefault != null && (step.Count > 0 && firstOrDefault.Url == null))
            {
                step.FirstOrDefault().Url = url;
            }
            var steps = step.Where(_ => _.CurrentNumber <= currentNumber).ToList();
            var p = (from object item in page select item.ToString()).ToList();
            var membershipUser = Membership.GetUser();

            if (membershipUser != null)
            {
                var userId = membershipUser.ProviderUserKey;
                if (userId != null)
                {
                    var model = p.Select(onePage => onePage.Split('<')).Select(pag => new LandingViewModel
                    {
                        Title = pag[0],
                        StringKey = pag[1],
                        Type = pag[2],
                        MetaDescription = pag[3],
                        MetaKeyWords = pag[4],
                        SiteContent = pag[5],
                        Name = pag[6],
                        Url = pag[7],
                        Styles = pag[8],
                        UserId = Int32.Parse(userId.ToString()),
                        Created = DateTime.Now
                    }).ToList();
                    var modelS = new StepsViewModel();
                    if (model.Any())
                    {
                        modelS.Landings = new List<LandingViewModel>();
                        modelS.Landings.AddRange(model);
                        var landingViewModel = model.FirstOrDefault();
                        if (landingViewModel != null) modelS.Url = landingViewModel.Url;
                        modelS.Time = DateTime.Now;
                        modelS.CurentTab = currentTab;
                        modelS.CurrentNumber = steps.Count + 1;
                        if (parametr != "first")
                        {
                            steps.Add(modelS);
                            Session["Steps"] = steps;
                        }
                        else
                        {
                            var mod = new List<StepsViewModel>();
                            modelS.CurrentNumber = 1;
                            mod.Add(modelS);
                            Session["Steps"] = mod;
                        }
                    }
                }
            }
            if (parametr != "first")
            {
                currentNumber++;
                return Json(new { total = steps.Count, current = currentNumber });
            }
            else
            {
                return Json(new { total = 1, current = 1 });
            }

        }

        [HttpPost]
        public JsonResult SaveImage(string file)
        {
            WebClient wc = new WebClient();

            wc.DownloadFile(file, @"d:\z.jpg");
            return Json("ok!");
        }

        [HttpPost]
        public JsonResult SaveImageByByteArray(string codeImage)
        {
            var user = Membership.GetUser();
            if (user != null)
            {
                var str = codeImage;
                char[] charData;
                charData = str.ToCharArray();
                byte[] array;
                array = new byte[charData.Length];
                Random rand = new Random();
                var r = rand.Next(1000000, 9999999);

                var path = Server.MapPath("/Areas/Cabinet/Gallery/UserFiles/" + user.UserName + "/" + r + ".png");


                using (FileStream file = new FileStream(path, FileMode.OpenOrCreate))
                {
                    file.Write(array, 0, array.Length);
                }
                return Json("ok!");
            }
            else
            {
                return Json("!");
            }

        }

        [ValidateInput(false)]
        [HttpPost]
        public string SavePages(Array page, string bozo)
        {
            bool isTemplate = false;
            if (bozo == "template")
            {
                isTemplate = true;
            }
            var p = (from object item in page select item.ToString()).ToList();
            var membershipUser = Membership.GetUser();
            if (membershipUser != null)
            {
                var userId = membershipUser.ProviderUserKey;
                if (userId != null)
                {
                    foreach (var onePage in p)
                    {
                        var pag = onePage.Split('<');
                        var model = new Landing()
                        {
                            Title = pag[0],
                            IsTemplate = isTemplate,
                            StringKey = pag[1],
                            Type = pag[2],
                            MetaDescription = pag[3],
                            MetaKeyWords = pag[4],
                            SiteContent = pag[5],
                            Name = pag[6],
                            Url = pag[7],
                            Styles = pag[8],
                            Links = pag[9],
                            PageLoader = pag[10],
                            Scripts = pag[11],
                            LastLoadScripts = pag[12],
                            UserId = Int32.Parse(userId.ToString()),
                            Created = DateTime.Now,
                            Updated = DateTime.Now
                        };

                        _landingService.AddOrUpdate(model.Url, model, landing =>
                        {
                            landing.Title = pag[0];
                            landing.IsTemplate = isTemplate;
                            landing.StringKey = pag[1];
                            landing.Type = pag[2];
                            landing.MetaDescription = pag[3];
                            landing.MetaKeyWords = pag[4];
                            landing.SiteContent = pag[5];
                            landing.Name = pag[6];
                            landing.Url = pag[7];
                            landing.Styles = pag[8];
                            landing.Links = pag[9];
                            landing.PageLoader = pag[10];
                            landing.Scripts = pag[11];
                            landing.LastLoadScripts = pag[12];
                            landing.UserId = Int32.Parse(userId.ToString());
                            landing.Created = DateTime.Now;
                            landing.Updated = DateTime.Now;
                        });
                    }

                }
                return "ok!";

            }
            return "not save";
        }

        [ValidateInput(false)]
        [HttpPost]
        public string DelExcessPages(Array pageKeys, string url)
        {
            var membershipUser = Membership.GetUser();
            if (membershipUser != null)
            {
                var userId = membershipUser.ProviderUserKey;
                if (userId != null)
                {
                    var p = (from object item in pageKeys select item.ToString()).ToList();
                    var listPages = _landingService.GetByUrlAndUserId(url, Int32.Parse(userId.ToString()));
                    foreach (var page in p.Select(t => listPages.FirstOrDefault(_ => _.StringKey == t)))
                    {
                        listPages.Remove(page);
                    }
                    foreach (var page in listPages)
                    {
                        _landingService.Delete(page.Id);
                    }
                    return "ok!";
                }
            }
            return "not save";
        }

        [ValidateInput(false)]
        [HttpGet]
        public ActionResult OpenLandingMenu()
        {
            var model = new List<LandingViewModel>();
            var user = Membership.GetUser();
            if (user == null) return View(model);
            var userId = user.ProviderUserKey;
            if (userId == null) return View(model);
            var landings = _landingService.GetByUser(Int32.Parse(userId.ToString()));
            model = landings.Where(_ => _.Type == "layout").Select(LandingViewModel.FromDomainModel).ToList();
            return View(model);
        }

        [ValidateInput(false)]
        [HttpPost]
        public string SaveProject(string filename, string css, Array pages)
        {
            var user = Membership.GetUser();
            if (user != null)
            {
                var listPages = (from object item in pages select item.ToString()).ToList();
                if (!Directory.Exists(Server.MapPath(WorkContext.UserPagesPath)))
                {
                    Directory.CreateDirectory(Server.MapPath(WorkContext.UserPagesPath));
                }
                if (!Directory.Exists(Server.MapPath(WorkContext.UserPagesPath) + filename))
                {
                    Directory.CreateDirectory(@"D:\WebSites\" + user.UserName);
                }
                if (!Directory.Exists(@"D:\WebSites\" + user.UserName + @"\css"))
                {
                    Directory.CreateDirectory(@"D:\WebSites\" + user.UserName + @"\css");
                }

                // Создаем новый файл
                FileInfo cssfile = new FileInfo(@"D:\WebSites\" + user.UserName + @"\css\site.css");

                FileStream fs = cssfile.Create();
                char[] charData;
                charData = css.ToCharArray();
                byte[] byData;
                byData = new byte[charData.Length];
                Encoder e = Encoding.UTF8.GetEncoder();
                e.GetBytes(charData, 0, charData.Length, byData, 0, true);
                fs.Seek(0, SeekOrigin.Begin);
                fs.Write(byData, 0, byData.Length);
                // Закрыть файловый поток
                fs.Close();

                for (var i = 0; i < listPages.Count; i = i + 2)
                {
                    FileInfo pagefile = new FileInfo(@"D:\WebSites\" + user.UserName);
                    if (listPages[i] == "Home")
                    {
                        pagefile = new FileInfo(@"D:\WebSites\" + user.UserName + @"\index.html");
                    }
                    else
                    {
                        pagefile = new FileInfo(@"D:\WebSites\" + user.UserName + @"\" + listPages[i] + @".html");
                    }

                    FileStream pf = pagefile.Create();
                    char[] charDataPage;
                    listPages[i + 1] = HttpUtility.UrlDecode(listPages[i + 1], Encoding.Default);
                    charDataPage = listPages[i + 1].ToCharArray();
                    byte[] byDataPage;
                    byDataPage = new byte[charDataPage.Length];
                    Encoder em = Encoding.UTF8.GetEncoder();
                    em.GetBytes(charDataPage, 0, charDataPage.Length, byDataPage, 0, true);
                    pf.Seek(0, SeekOrigin.Begin);
                    pf.Write(byDataPage, 0, byDataPage.Length);
                    // Закрыть файловый поток
                    pf.Close();
                }


                var cssfil = new FileInfo(@"D:\WebSites\" + user.UserName + @"\css\style.css");
                FileStream cssf = cssfil.Create();
                string pathCss = Server.MapPath(@"/Areas/Cabinet/lib/css/site.css");
                string cssByteData = System.IO.File.ReadAllText(pathCss);
                char[] cssData;
                cssData = cssByteData.ToCharArray();
                byte[] byDataCss;
                byDataCss = new byte[cssData.Length];
                Encoder emCss = Encoding.UTF8.GetEncoder();
                emCss.GetBytes(cssData, 0, cssData.Length, byDataCss, 0, true);
                cssf.Seek(0, SeekOrigin.Begin);
                cssf.Write(byDataCss, 0, byDataCss.Length);
                // Закрыть файловый поток
                cssf.Close();

                return "ok!";
            }
            else
            {
                return "user is undefined";
            }
        }


        private string GetFileExtension(string fileExtension)
        {
            switch (fileExtension)
            {
                case ".htm":
                case ".html":
                case ".log":
                    return "text/HTML";
                case ".txt":
                    return "text/plain";
                case ".doc":
                    return "application/ms-word";
                case ".tiff":
                case ".tif":
                    return "image/tiff";
                case ".asf":
                    return "video/x-ms-asf";
                case ".avi":
                    return "video/avi";
                case ".zip":
                    return "application/zip";
                case ".xls":
                case ".csv":
                    return "application/vnd.ms-excel";
                case ".gif":
                    return "image/gif";
                case ".jpg":
                case "jpeg":
                    return "image/jpeg";
                case ".bmp":
                    return "image/bmp";
                case ".wav":
                    return "audio/wav";
                case ".mp3":
                    return "audio/mpeg3";
                case ".mpg":
                case "mpeg":
                    return "video/mpeg";
                case ".rtf":
                    return "application/rtf";
                case ".asp":
                    return "text/asp";
                case ".pdf":
                    return "application/pdf";
                case ".fdf":
                    return "application/vnd.fdf";
                case ".ppt":
                    return "application/mspowerpoint";
                case ".dwg":
                    return "image/vnd.dwg";
                case ".msg":
                    return "application/msoutlook";
                case ".xml":
                case ".sdxl":
                    return "application/xml";
                case ".xdp":
                    return "application/vnd.adobe.xdp+xml";
                default:
                    return "application/octet-stream";
            }
        }

        public string GetImageBase64Data(string image)
        {
            string path = "";
            if (image.Contains("http"))
            {
                return image;
            }
            else
            {
                path = Server.MapPath(image);
            }
            byte[] imageByteData = System.IO.File.ReadAllBytes(path);
            string imageBase64Data = Convert.ToBase64String(imageByteData);
            var count = path.Split('\\').Count();
            var fileName = path.Split('\\')[count - 1];
            var counts = fileName.Split('.').Count();
            var fileExtension = fileName.Split('.')[counts - 1];

            var format = GetFileExtension("." + fileExtension);
            return "data:" + format + ";base64," + imageBase64Data;
        }

        [HttpGet]
        public ActionResult LoadImageByFile()
        {
            var userRole = false;
            var model = new LoadFileViewModel();
            model.Catalogs = new List<string>();
            model.Catalog = "";
            var userName = "TestUser";
            var user = Membership.GetUser();
            if (user != null && user.ProviderUserKey!=null)
            {
                userRole = _roleService.IsUserInRole(user.UserName, "sadmin");
                int userId = Int32.Parse(user.ProviderUserKey.ToString());
                userName = _userService.GetById(userId).UserName;
            }
            if (!Directory.Exists(Server.MapPath(WorkContext.UserImagesPath)))
            {
                Directory.CreateDirectory(Server.MapPath(WorkContext.UserImagesPath));
            }
            if (!Directory.Exists(Server.MapPath(WorkContext.UserImagesPath) + userName))
            {
                Directory.CreateDirectory(Server.MapPath(WorkContext.UserImagesPath) + userName);
            }
            DirectoryInfo directory = new DirectoryInfo(Server.MapPath(WorkContext.UserImagesPath) + userName); ;
            var dir = directory.GetDirectories().Select(item => item.Name).ToList();
            if (userRole)
            {
                if (!Directory.Exists(Server.MapPath(WorkContext.TemplatesImagePath)))
                {
                    Directory.CreateDirectory(Server.MapPath(WorkContext.TemplatesImagePath));
                }
                dir.Add("TemplatesImage");
            }
            model.Catalogs.AddRange(dir);
            return View(model);
        }
        [HttpPost]
        public ActionResult LoadImageByFile(HttpPostedFileBase FileImage, string catalog)
        {
            var userRole = false;
            var userName = "TestUser";
            var user = Membership.GetUser();
            if (user != null && user.ProviderUserKey!=null)
            {
                userRole = _roleService.IsUserInRole(user.UserName, "sadmin");
                int userId = Int32.Parse(user.ProviderUserKey.ToString());
                userName = _userService.GetById(userId).UserName;
            }
            try
            {
                var extension = Path.GetExtension(FileImage.FileName);
                var fileName = Guid.NewGuid() + extension;
                var path = Server.MapPath(WorkContext.UserImagesPath) + userName + "/" + catalog;
                if (userRole && !Directory.Exists(path))
                {
                    path = Server.MapPath(WorkContext.TemplatesImagePath);
                }
                else
                {
                    if (!Directory.Exists(Server.MapPath(WorkContext.UserImagesPath)))
                    {
                        Directory.CreateDirectory(Server.MapPath(WorkContext.UserImagesPath));
                    }
                    if (!Directory.Exists(Server.MapPath(WorkContext.UserImagesPath) + userName))
                    {
                        Directory.CreateDirectory(Server.MapPath(WorkContext.UserImagesPath) + userName);
                    }
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                }
                FileImage.SaveAs(path + "/" + fileName);
            }
            catch (Exception)
            {

            }
            return View("LoadImageByFile");
        }

        [HttpGet]
        public ActionResult LoadImageByUrl()
        {
            var userRole = false;
            var model = new LoadUrlViewModel { Catalogs = new List<SelectListItem>() };
            var userName = "TestUser";
            var user = Membership.GetUser();
            if (user != null && user.ProviderUserKey!= null)
            {
                userRole = _roleService.IsUserInRole(user.UserName, "sadmin");
                int userId = Int32.Parse(user.ProviderUserKey.ToString());
                userName = _userService.GetById(userId).UserName;
            }
            var directory = new DirectoryInfo(Server.MapPath(WorkContext.UserImagesPath) + userName);
            if (!Directory.Exists(Server.MapPath(WorkContext.UserImagesPath) + userName))
            {
                Directory.CreateDirectory(Server.MapPath(WorkContext.UserImagesPath) + userName);
            }
            var dir = directory.GetDirectories().Select(item => item.Name).ToList();
            if (userRole)
            {
                if (!Directory.Exists(Server.MapPath(WorkContext.TemplatesImagePath)))
                {
                    Directory.CreateDirectory(Server.MapPath(WorkContext.TemplatesImagePath));
                }
                dir.Add("TemplatesImage");
            }
            foreach (var item in dir)
            {
                model.Catalogs.Add(new SelectListItem() { Text = item, Value = item });
            }
            return View(model);

        }

        [HttpPost]
        public void LoadImageByUrl(string url, string catalog)
        {
            try
            {
                var userRole = false;
                var userName = "TestUser";
                var user = Membership.GetUser();
                if (user != null && user.ProviderUserKey!=null)
                {
                    userRole = _roleService.IsUserInRole(user.UserName, "sadmin");
                    int userId = Int32.Parse(user.ProviderUserKey.ToString());
                    userName = _userService.GetById(userId).UserName;
                }
                var array = url.Split('/');
                var fileName = array[array.Length - 1];
                var path = Server.MapPath(WorkContext.UserImagesPath) + userName + "/" + catalog;
                if (userRole && !Directory.Exists(path))
                {
                    path = Server.MapPath(WorkContext.TemplatesImagePath);
                }
                
                if (!Directory.Exists(Server.MapPath(WorkContext.UserImagesPath) + userName))
                {
                    Directory.CreateDirectory(Server.MapPath(WorkContext.UserImagesPath) + userName);
                }
                var webClient = new WebClient();
                webClient.DownloadFile(new Uri(url), path + "/" + fileName); //название и можно путь провести, если папки не существует то ее надо создать заранее
            }
            catch (Exception)
            {

            }
        }

        [HttpGet]
        public ActionResult PreviewAnimation()
        {
            var model = new AnimateViewModel();
            model.Animate = "";
            return View(model);
        }

        [HttpPost]
        public ActionResult PreviewAnimation(AnimateViewModel model)
        {

            return View(model);
        }

        [HttpPost]
        public ActionResult GetCatalogs(string type)
        {
            if (type == "user")
            {
                var userRole = false;
                var userName = "TestUser";
                var user = Membership.GetUser();
                if (user != null && user.ProviderUserKey != null)
                {
                    userRole = _roleService.IsUserInRole(user.UserName, "sadmin");
                    int userId = Int32.Parse(user.ProviderUserKey.ToString());
                    userName = _userService.GetById(userId).UserName;
                }
                if (!Directory.Exists(Server.MapPath(WorkContext.UserImagesPath) + userName))
                {
                    Directory.CreateDirectory(Server.MapPath(WorkContext.UserImagesPath) + userName);
                }
                DirectoryInfo directory = new DirectoryInfo(Server.MapPath(WorkContext.UserImagesPath) + userName); ;
                var dir = directory.GetDirectories().Select(item => item.Name).ToList();
                if (userRole)
                {
                    if (!Directory.Exists(Server.MapPath(WorkContext.TemplatesImagePath)))
                    {
                        Directory.CreateDirectory(Server.MapPath(WorkContext.TemplatesImagePath));
                    }
                    dir.Add("TemplatesImage");
                }
                return Json(new { res = dir, state = "ok" });
            }
            else
            {
                DirectoryInfo directory = new DirectoryInfo(Server.MapPath(WorkContext.ImagesPath)); ;
                var dir = directory.GetDirectories().Select(item => item.Name).ToList();
                return Json(new { res = dir, state = "ok" });
            }
        }

        [HttpPost]
        public void AddCatalogs(string name)
        {
            var userName = "TestUser";
            var user = Membership.GetUser();
            if (user != null && user.ProviderUserKey!=null)
            {
                int userId = Int32.Parse(user.ProviderUserKey.ToString());
                userName = _userService.GetById(userId).UserName;
            }
            var path = Server.MapPath(WorkContext.UserImagesPath) + userName + "/" + name;
            if (!Directory.Exists(Server.MapPath(WorkContext.UserImagesPath) + userName))
            {
                Directory.CreateDirectory(Server.MapPath(WorkContext.UserImagesPath) + userName);
            }
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }
        //public static void Main()
        //{
        //    // Create a request for the URL. 
        //    WebRequest request = WebRequest.Create(
        //      "https://api.getresponse.com/v3");
        //    // If required by the server, set the credentials.
        //    request.Credentials = CredentialCache.DefaultCredentials;
        //    // Get the response.
        //    WebResponse response = request.GetResponse();
        //    // Display the status.
        //    Console.WriteLine(((HttpWebResponse)response).StatusDescription);
        //    // Get the stream containing content returned by the server.
        //    Stream dataStream = response.GetResponseStream();
        //    // Open the stream using a StreamReader for easy access.
        //    StreamReader reader = new StreamReader(dataStream);
        //    // Read the content.
        //    string responseFromServer = reader.ReadToEnd();
        //    // Display the content.
        //    Console.WriteLine(responseFromServer);
        //    // Clean up the streams and the response.
        //    reader.Close();
        //    response.Close();
        //}

        public ActionResult UserVideo(string videoUrl, string posterUrl)
        {
            var model = new VideoViewModel
            {
                VideoUrl = videoUrl,
                PosterUrl = posterUrl
            };
            return View(model);
        }

        [HttpPost]
        public JsonResult SaveSection(string title, string styles, string content, string img, string categoryKey)
        {
            var titleIsExist = _sectionsService.NameIsExist(title);
            if (titleIsExist)
            {
                return Json("title is exist");
            }

            var category = _sectionsCategoryService.GetByKey(categoryKey);
            if (category == null)
            {
                return Json("error category");
            }



            var model = new Sections
            {
                Name = title,
                StringKey = GetTranslit(title),
                SectionContent = content,
                Styles = styles,
                CategoryId = category.Id,
                Image = img,
                IsPublic = true,
                Created = DateTime.Now
            };

            _sectionsService.AddOrUpdate(model.StringKey, model, section =>
            {
                section.Name = title;
                section.StringKey = GetTranslit(title);
                section.SectionContent = content;
                section.Styles = styles;
                section.CategoryId = category.Id;
                section.Image = img;
                section.IsPublic = true;
                section.Created = DateTime.Now;
                section.Updated = DateTime.Now;
            });

            return Json("ok!");
        }

        public static string GetTranslit(string sourceText)
        {
            Dictionary<string, string> transliter = new Dictionary<string, string>();
            transliter.Add("а", "a");
            transliter.Add("б", "b");
            transliter.Add("в", "v");
            transliter.Add("г", "g");
            transliter.Add("д", "d");
            transliter.Add("е", "e");
            transliter.Add("ё", "yo");
            transliter.Add("ж", "zh");
            transliter.Add("з", "z");
            transliter.Add("и", "i");
            transliter.Add("й", "j");
            transliter.Add("к", "k");
            transliter.Add("л", "l");
            transliter.Add("м", "m");
            transliter.Add("н", "n");
            transliter.Add("о", "o");
            transliter.Add("п", "p");
            transliter.Add("р", "r");
            transliter.Add("с", "s");
            transliter.Add("т", "t");
            transliter.Add("у", "u");
            transliter.Add("ф", "f");
            transliter.Add("х", "h");
            transliter.Add("ц", "c");
            transliter.Add("ч", "ch");
            transliter.Add("ш", "sh");
            transliter.Add("щ", "sch");
            transliter.Add("ъ", "j");
            transliter.Add("ы", "i");
            transliter.Add("ь", "j");
            transliter.Add("э", "e");
            transliter.Add("ю", "yu");
            transliter.Add("я", "ya");
            transliter.Add("А", "A");
            transliter.Add("Б", "B");
            transliter.Add("В", "V");
            transliter.Add("Г", "G");
            transliter.Add("Д", "D");
            transliter.Add("Е", "E");
            transliter.Add("Ё", "Yo");
            transliter.Add("Ж", "Zh");
            transliter.Add("З", "Z");
            transliter.Add("И", "I");
            transliter.Add("Й", "J");
            transliter.Add("К", "K");
            transliter.Add("Л", "L");
            transliter.Add("М", "M");
            transliter.Add("Н", "N");
            transliter.Add("О", "O");
            transliter.Add("П", "P");
            transliter.Add("Р", "R");
            transliter.Add("С", "S");
            transliter.Add("Т", "T");
            transliter.Add("У", "U");
            transliter.Add("Ф", "F");
            transliter.Add("Х", "H");
            transliter.Add("Ц", "C");
            transliter.Add("Ч", "Ch");
            transliter.Add("Ш", "Sh");
            transliter.Add("Щ", "Sch");
            transliter.Add("Ъ", "J");
            transliter.Add("Ы", "I");
            transliter.Add("Ь", "J");
            transliter.Add("Э", "E");
            transliter.Add("Ю", "Yu");
            transliter.Add("Я", "Ya");

            var ans = new StringBuilder();
            foreach (var t in sourceText)
            {
                ans.Append(transliter.ContainsKey(t.ToString()) ? transliter[t.ToString()] : t.ToString());
            }
            return ans.ToString();
        }

        [HttpGet]
        public ActionResult ReadySections()
        {
            var membershipUser = Membership.GetUser();
            var userRole = membershipUser != null && _roleService.IsUserInRole(membershipUser.UserName, "admin");
            var sections = _sectionsService.GetAll(!userRole);
            var model = sections.Select(SectionViewModel.FromDomainModel).ToList();
            foreach (var item in model)
            {

            }

            return View(model);
        }

        [HttpGet]
        public ActionResult GetSectionCategories()
        {
            var categories = _sectionsCategoryService.GetAll();
            var model = categories.Select(CategorySectionViewModel.FromDomainModel).ToList();
            return View(model);
        }
        [HttpGet]
        public ActionResult GetSectionCategory()
        {
            var categories = _sectionsCategoryService.GetAll();
            var model = categories.Select(CategorySectionViewModel.FromDomainModel).ToList();
            return View(model);
        }

        [HttpPost]
        public void SaveLandingImage(string url, string img)
        {
            var user = Membership.GetUser();
            if (user != null && user.ProviderUserKey!=null)
            {
                var userId = Int32.Parse(user.ProviderUserKey.ToString());
                List<Landing> landings = _landingService.GetByUrlAndUserId(url, userId);
                foreach (var item in landings)
                {
                    _landingService.Update(item.Id, landing =>
                    {
                        landing.Image = img;
                    });
                }
            }
        }
    }
}
