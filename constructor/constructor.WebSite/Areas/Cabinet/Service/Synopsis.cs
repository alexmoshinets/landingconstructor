﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace WebSite.Areas.Cabinet.Service
{
    class Synopsis
    {
        // your API key
        // available at http://www.getresponse.com/my_api_key.html
        public string api_key = "9922616c58b83df1f1eacace93f836c8";
        public string campaignId = "";

        // API 2.x URL
        public string api_url = "http://api2.getresponse.com";

        public Synopsis(string api_key, string campaignId, string api_url = null)
        {
            this.api_key = api_key;
            this.campaignId = campaignId;
            if (!string.IsNullOrEmpty(api_url))
                this.api_url = api_url;
        }

        public  void Main(IDictionary<string, string> fields)
        {
            JavaScriptSerializer jss = new JavaScriptSerializer();

            // get CAMPAIGN_ID of 'sample_marketing' campaign

            // new request object
            Hashtable _request = new Hashtable();

            _request["jsonrpc"] = "2.0";
            _request["id"] = 1;

            // set method name
            _request["method"] = "get_campaigns";

            // set conditions
            Hashtable operator_obj = new Hashtable();
            operator_obj["EQUALS"] = "sample_marketing";

            Hashtable name_obj = new Hashtable();
            name_obj["name"] = operator_obj;

            // set params request object
            object[] params_array = { api_key, name_obj };

            _request["params"] = params_array;

            // send headers and content in one request
            // (disable 100 Continue behavior)
            System.Net.ServicePointManager.Expect100Continue = false;

            // initialize client
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(api_url);
            request.Method = "POST";

            byte[] request_bytes = Encoding.UTF8.GetBytes(jss.Serialize(_request));

            String response_string = null;

            try
            {
                // call method 'get_messages' and get result
                Stream request_stream = request.GetRequestStream();
                request_stream.Write(request_bytes, 0, request_bytes.Length);
                request_stream.Close();

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream response_stream = response.GetResponseStream();

                StreamReader reader = new StreamReader(response_stream);
                response_string = reader.ReadToEnd();
                reader.Close();

                response_stream.Close();
                response.Close();
            }
            catch (Exception e)
            {
                //check for communication and response errors
                //implement handling if needed
                Console.WriteLine(e.Message);
                Environment.Exit(0);
            }

            // decode response to Json object
            Dictionary<string, object> jsonContent = jss.DeserializeObject(response_string) as Dictionary<string, object>;

            // get result
            Dictionary<string, object> result = jsonContent["result"] as Dictionary<string, object>;

            string campaign_id = null;

            // get campaign id
            //foreach (object key in result.Keys)
            //{
            //    campaign_id = key.ToString();
            //}

            // add contact to 'sample_marketing' campaign

            // new request object
            _request = new Hashtable();

            _request["jsonrpc"] = "2.0";
            _request["id"] = 2;

            // set method name
            _request["method"] = "add_contact";

            List<object> customs_array = new List<object>();
            Hashtable contact_params = new Hashtable();

            var fio = "";            
            foreach (var field in fields)
            {
                bool alreadyAdded = false;
                if (field.Key.ToLower().Contains("почта") || field.Key.ToLower().Contains("почты") || field.Key.ToLower().Contains("email") || field.Key.ToLower().Contains("e-mail"))
                {
                    contact_params["email"] = field.Value;
                    alreadyAdded = true;
                }
                if (field.Key.ToLower().Contains("фамили") || field.Key.ToLower().Contains("lastn") || field.Key.ToLower().Contains("lname"))
                {
                    fio += field.Value + " ";
                    alreadyAdded = true;
                }
                if (field.Key.ToLower().Contains("имя") || field.Key.ToLower().Contains("firstn") || field.Key.ToLower().Contains("fname"))
                {
                    fio += field.Value + " ";
                    alreadyAdded = true;
                }
                //if (!alreadyAdded)
                //{
                //    Hashtable custom = new Hashtable();
                //    custom["name"] = field.Key.ToLower();
                //    custom["content"] = field.Value;

                //    // contact customs array
                //    customs_array.Add(custom);

                //    contact_params[field.Key.ToLower()] = field.Value;
                //    alreadyAdded = true;
                //}
            }

            contact_params["campaign"] = this.campaignId;
            contact_params["name"] = fio;
            //contact_params["email"] = "sample@email.com";

            //contact_params["customs"] = customs_array;


            // set params request object
            object[] add_contact_params_array = { api_key, contact_params };

            _request["params"] = add_contact_params_array;

            // send headers and content in one request
            // (disable 100 Continue behavior)
            System.Net.ServicePointManager.Expect100Continue = false;

            // initialize client
            request = (HttpWebRequest)WebRequest.Create(api_url);
            request.Method = "POST";

            request_bytes = Encoding.UTF8.GetBytes(jss.Serialize(_request));

            response_string = null;

            try
            {
                // call method 'add_contact' and get result
                Stream request_stream = request.GetRequestStream();
                request_stream.Write(request_bytes, 0, request_bytes.Length);
                request_stream.Close();

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream response_stream = response.GetResponseStream();

                StreamReader reader = new StreamReader(response_stream);
                response_string = reader.ReadToEnd();
                reader.Close();

                response_stream.Close();
                response.Close();
            }
            catch (Exception e)
            {
                //check for communication and response errors
                //implement handling if needed                
            }

        }
    }
}