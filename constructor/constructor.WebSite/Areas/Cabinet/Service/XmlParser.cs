﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

namespace WebSite.Areas.Cabinet.Service
{
    public class XmlParser
    {        
        public HTMLWidget ParseFile(string inputString)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(HTMLWidget));
            using (TextReader reader = new StringReader(inputString))
            {
                HTMLWidget result = (HTMLWidget)serializer.Deserialize(reader);
                return result;
            }           
        }
    }

    [XmlRoot(ElementName = "text")]
    public class Text
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "defaultValue")]
        public string DefaultValue { get; set; }
        [XmlAttribute(AttributeName = "label")]
        public string Label { get; set; }
        [XmlAttribute(AttributeName = "toolTip")]
        public string ToolTip { get; set; }
        [XmlAttribute(AttributeName = "multiline")]
        public string Multiline { get; set; }
    }

    [XmlRoot(ElementName = "value")]
    public class Value
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "label")]
        public string Label { get; set; }
        [XmlAttribute(AttributeName = "disableOptions")]
        public string DisableOptions { get; set; }
    }

    [XmlRoot(ElementName = "list")]
    public class List
    {
        [XmlElement(ElementName = "value")]
        public List<Value> Value { get; set; }
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "defaultValue")]
        public string DefaultValue { get; set; }
        [XmlAttribute(AttributeName = "label")]
        public string Label { get; set; }
    }

    [XmlRoot(ElementName = "url")]
    public class Url
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "label")]
        public string Label { get; set; }
        [XmlAttribute(AttributeName = "defaultValue")]
        public string DefaultValue { get; set; }
        [XmlAttribute(AttributeName = "currentPageOrURL")]
        public string CurrentPageOrURL { get; set; }
    }

    [XmlRoot(ElementName = "trueVal")]
    public class TrueVal
    {
        [XmlAttribute(AttributeName = "value")]
        public string Value { get; set; }
    }

    [XmlRoot(ElementName = "builtIn")]
    public class BuiltIn
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
    }

    [XmlRoot(ElementName = "falseVal")]
    public class FalseVal
    {
        [XmlAttribute(AttributeName = "value")]
        public string Value { get; set; }
        [XmlAttribute(AttributeName = "disableOptions")]
        public string DisableOptions { get; set; }
    }

    [XmlRoot(ElementName = "bool")]
    public class Bool
    {
        [XmlElement(ElementName = "trueVal")]
        public TrueVal TrueVal { get; set; }
        [XmlElement(ElementName = "falseVal")]
        public FalseVal FalseVal { get; set; }
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "label")]
        public string Label { get; set; }
        [XmlAttribute(AttributeName = "defaultValue")]
        public string DefaultValue { get; set; }
    }

    [XmlRoot(ElementName = "number")]
    public class Number
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "min")]
        public string Min { get; set; }
        [XmlAttribute(AttributeName = "max")]
        public string Max { get; set; }
        [XmlAttribute(AttributeName = "step")]
        public string Step { get; set; }
        [XmlAttribute(AttributeName = "snap")]
        public string Snap { get; set; }
        [XmlAttribute(AttributeName = "defaultValue")]
        public string DefaultValue { get; set; }
        [XmlAttribute(AttributeName = "label")]
        public string Label { get; set; }
    }

    [XmlRoot(ElementName = "info")]
    public class Info
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "label")]
        public string Label { get; set; }
        [XmlAttribute(AttributeName = "linkURL")]
        public string LinkURL { get; set; }
    }

    [XmlRoot(ElementName = "parameters")]
    public class Parameters
    {
        [XmlElement(ElementName = "text")]
        public List<Text> Text { get; set; }
        [XmlElement(ElementName = "list")]
        public List<List> List { get; set; }
        [XmlElement(ElementName = "url")]
        public List<Url> Url { get; set; }
        [XmlElement(ElementName = "bool")]
        public List<Bool> Bool { get; set; }
        [XmlElement(ElementName = "number")]
        public List<Number> Number { get; set; }
        [XmlElement(ElementName = "info")]
        public List<Info> Info { get; set; }
        [XmlElement(ElementName = "builtIn")]
        public List<BuiltIn> BuiltIn { get; set; }
    }

    [XmlRoot(ElementName = "HTMLWidget")]
    public class HTMLWidget
    {
        [XmlElement(ElementName = "parameters")]
        public Parameters Parameters { get; set; }
        [XmlElement(ElementName = "pageItemHTML")]
        public string PageItemHTML { get; set; }
        [XmlElement(ElementName = "bodyEndHTML")]
        public string BodyEndHTML { get; set; }
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "formatNumber")]
        public string FormatNumber { get; set; }
        [XmlAttribute(AttributeName = "localization")]
        public string Localization { get; set; }
        [XmlAttribute(AttributeName = "creator")]
        public string Creator { get; set; }
    }    
}