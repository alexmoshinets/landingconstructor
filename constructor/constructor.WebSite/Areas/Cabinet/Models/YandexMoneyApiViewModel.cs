﻿namespace WebSite.Areas.Cabinet.Models
{
    public class YandexMoneyApiViewModel
    {
        // receiver. Yandex wallet number which will receive the payment. Обязательное.
        public string Receiver { get; set; }
        // formcomment. (до 50 символов) — название получателя в истории отправителя. Мы рекомендуем формировать его из названий магазина и товара. Например, «Мой магазин: валенки белые». Обязательное.
        public string FormComment { get; set; }
        // short-dest. Название платежа на странице подтверждения. Рекомендуем делать его таким же, как formcomment. Обязательное.
        public string ShortDest { get; set; }
        // targets. (до 150 символов) — назначение платежа. Обязательное.
        public string Targets { get; set; }
        // label. (до 64 символов) — метка, которую сайт или приложение присваивает конкретному переводу. Например, в качестве метки можно указывать код или идентификатор заказа. Необязательное.
        public string Label { get; set; }
        // quickpay-form. — определяет тип транзакции. Возможные значения: • shop — для универсальной формы-принимателя; • donate — для «благотворительной» формы; • small — для кнопки. Обязательное.
        public string QuickpayForm { get; set; }
        // sum. сумма перевода. Из этой суммы вычитается комиссия: • 0,5%, если перевод отправлен из электронного кошелька; • 2%, если перевод отправлен с произвольной банковской карты. Обязательное.
        public decimal Sum { get; set; }
        // comment. (до 200 символов) — поле, в котором можно передать комментарий отправителя перевода. Необязательное.
        public string Comment { get; set; }
        // need-fio. запрос ФИО отправителя. Необязательное.
        public string NeedFio { get; set; }
        // need-email. запрос электронной почты отправителя. Необязательное.
        public string NeedEmail { get; set; }
        // need-phone. запрос телефона отправителя. Необязательное.
        public string NeedPhone { get; set; }
        // need-address. запрос адреса отправителя. Необязательное.
        public string NeedAddress { get; set; }
        // paymentType. определяет средство платежа. Возможные значения: • PC – оплата со счета Яндекс.Денег • AC – оплата с банковской карты. Обязательное.
        public string PaymentType { get; set; }
    }

    public class TestYadApiResponse
    {
        public string notification_type { get; set; }
        public string operation_id { get; set; }
        public string amount { get; set; }
        public string currency { get; set; }
        public string datetime { get; set; }
        public string sender { get; set; }
        public string codepro { get; set; }
        public string label { get; set; }    
    }
}