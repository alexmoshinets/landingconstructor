﻿namespace WebSite.Areas.Cabinet.Models
{
    public class CommonModel
    {
        public string type { get; set; }
        public string _value { get; set; }
        public string lang { get; set; }

    }

    public class Attributes
    {
        public string shemalocation { get; set; }

    }
}