﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DomainModel.Content;

namespace WebSite.Areas.Cabinet.Models
{
    public class CategorySectionViewModel
    {
        public int Id { get; set; }
        public string StringKey { get; set; }
        public string Name { get; set; }
        public List<SectionViewModel> Sections { get; set; }

        public DateTime Created { get; set; }
        public DateTime? Updated { get; set; }


        public static CategorySectionViewModel FromDomainModel(SectionsCategory sectionCategory)
        {
            var result = new CategorySectionViewModel
            {
                Id = sectionCategory.Id,
                Name = sectionCategory.Name,
                StringKey = sectionCategory.StringKey,
                Created = sectionCategory.Created,
                Updated = sectionCategory.Updated
            };
            return result;
        }
    }
}