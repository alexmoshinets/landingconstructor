﻿using System;

namespace WebSite.Areas.Cabinet.Models
{
    public class VideoViewModel
    {
        public string VideoUrl { get; set; }
        public string PosterUrl { get; set; }

    }
}