﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSite.Areas.Cabinet.Models
{
    public class MenuItemViewModel
    {
        public MenuItemViewModel()
        {
            Childs = new List<MenuItemViewModel>();
        }
        public int Id { get; set; }
        public int ParentId { get; set; }
        public string Name { get; set; }

        public string Url { get; set; }   
        public string StringKey { get; set; }     

        public bool IsActive { get; set; }

        public int? PageId { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public List<MenuItemViewModel> Childs { get; set; }
        
    }
}