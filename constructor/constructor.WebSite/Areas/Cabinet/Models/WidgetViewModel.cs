﻿using DomainModel.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSite.Areas.Cabinet.Models
{
    public class WidgetViewModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Title { get; set; }
        public DateTime CreateDate { get; set; }
        //public IDictionary<string, string> Params { get; set; }
        public string Script { get; set; }
        public string Description { get; set; }

        public static WidgetViewModel FromDomainModel(Widget entity)
        {
            return new WidgetViewModel()
            {
                CreateDate = entity.CreateDate,
                Description = entity.Description,
                Id = entity.Id,
                Script = entity.Script,
                Title = entity.Title,
                UserId = entity.UserId
            };
        }
    }
}