﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebSite.Areas.Cabinet.Models.LandingIntegration
{
    public class Smsintel
    {        
        public string UserPassword { get; set; }
        public string UserLogin { get; set; }
        public string GroupId { get; set; }

        public bool IsRedirect { get; set; }
        public string AlertMessage { get; set; }
        public string RedirectUrl { get; set; }
   
        public string ScriptCode { get; set; }
        public bool SendUtm { get; set; }
    }
}