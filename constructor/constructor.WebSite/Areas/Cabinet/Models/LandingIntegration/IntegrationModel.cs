﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebSite.Areas.Cabinet.Models.LandingIntegration
{
    public class IntegrationModel
    {
        public IntegrationModel()
        {
            AmoCrmModel = new AmoCrmModel();
            Bitrix24Model = new Bitrix24Model();
            MailChimpModel = new MailChimpModel();
        }
        public TypeServiceIntegrationEnum TypeIntegration { get; set; }
        public int? LandingId { get; set; }
        public AmoCrmModel AmoCrmModel { get; set; }
        public Bitrix24Model Bitrix24Model { get; set; }
        public MailChimpModel MailChimpModel { get; set; }
        public string FormId { get; set; }
        public string url { get; set; }
        public bool IsRedirect { get; set; }
        public string AlertMessage { get; set; }
        public string RedirectUrl { get; set; }

        public string UtmString { get; set; }

        [Display(Name = "Источник трафика")]
        public string utm_source { get; set; }

        [Display(Name = "Канал кампании")]
        public string utm_medium { get; set; }

        [Display(Name = "Название кампании")]

        public string utm_campaign { get; set; }
        [Display(Name = "Объявление")]
        public string utm_content { get; set; }

        [Display(Name = "Ключевое слово")]
        public string utm_term { get; set; }
        public string ScriptCode { get; set; }
        public bool SendUtm { get; set; }
    }
}