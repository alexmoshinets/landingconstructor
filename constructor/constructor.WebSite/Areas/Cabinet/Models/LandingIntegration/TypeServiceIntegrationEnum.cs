﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSite.Areas.Cabinet.Models.LandingIntegration
{
    public enum TypeServiceIntegrationEnum
    {
        Amocrm,
        Bitrix24,
        MailChimp,
        SendPulse,
        Smsintel,
        GetResponse360
    }
}