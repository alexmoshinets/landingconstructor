﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebSite.Areas.Cabinet.Models.LandingIntegration
{
    public class MailChimpModel
    {
        public string ListId { get; set; }
        public string MailChimpApiKey { get; set; }
        public bool IsRedirect { get; set; }
        public string AlertMessage { get; set; }
        public string RedirectUrl { get; set; }

        [Display(Name = "Источник трафика")]
        public string utm_source { get; set; }

        [Display(Name = "Канал кампании")]
        public string utm_medium { get; set; }

        [Display(Name = "Название кампании")]

        public string utm_campaign { get; set; }
        [Display(Name = "Объявление")]
        public string utm_content { get; set; }

        [Display(Name = "Ключевое слово")]
        public string utm_term { get; set; }
        public string UtmString { get; set; }
        public string ScriptCode { get; set; }
        public bool SendUtm { get; set; }
    }
}