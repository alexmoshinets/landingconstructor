﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebSite.Areas.Cabinet.Models.LandingIntegration;

namespace WebSite.Areas.Cabinet.Models
{
    public class LandingSettingsViewModel
    {
        public int Id { get; set; }
        public int LandingId { get; set; }
        public string Url { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public bool IsNotify { get; set; }
        public AmoCrmModel AmoCrm { get; set; }
        public SendPulseModel SendPulse { get; set; }
        public MailChimpModel MailChimp { get; set; }
        public Bitrix24Model Bitrix { get; set; }
        public Smsintel Smsintel { get; set; }
        public GetResponse360 GetResponse { get; set; }
        public bool IsBitrix { get; set; }
        public bool IsAmocrm { get; set; }
        public bool IsMailchimp { get; set; }
        public bool IsSendpulse { get; set; }
        public bool IsSmsintel { get; set; }
    }
}