﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DomainModel.Content;

namespace WebSite.Areas.Cabinet.Models
{
    public class SectionViewModel
    {
        public string StringKey { get; set; }
        public string Name { get; set; }
        public string SectionContent { get; set; }
        public string Styles { get; set; }
        public string Image { get; set; }
        public int CategoryId { get; set; }
        public bool IsPublic { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Updated { get; set; }

        public static SectionViewModel FromDomainModel(Sections section)
        {
            var result = new SectionViewModel
            {
                Name = section.Name,
                StringKey = section.StringKey,
                SectionContent = section.SectionContent,
                Styles = section.Styles,
                Image = section.Image,
                CategoryId = section.CategoryId,
                IsPublic = section.IsPublic,
                Created = section.Created,
                Updated = section.Updated
            };
            return result;
        }
    }
}