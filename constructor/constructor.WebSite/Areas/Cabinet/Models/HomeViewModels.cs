﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DomainModel.Content;

namespace WebSite.Areas.Cabinet.Models
{
    public class TariffsViewModel
    {
        [Required]
        public int PeriodInMonths { get; set; }
        [Required]
        //public PaymentMethod PaymentMethod { get; set; }
        public string Tariff { get; set; }
        public string BreadcrumbsH2Text { get; set; }
    }

    public class CreateInvoiceViewModel
    {
        public int InvoiceId { get; set; }
        public string PeriodInMonths { get; set; }
        public string PaymentMethod { get; set; }
        public string TariffName { get; set; }
        public string Date { get; set; }
        public string DueDate { get; set; }
        public string Amount { get; set; }
        public YandexMoneyApiViewModel YandexMoneyApiViewModel { get; set; }
    }

    public class PricingViewModel
    {
        public List<Tariff> Tariffs { get; set; }
    }

    public class Tariff
    {
        public string Name { get; set; }
        public List<Price> Prices { get; set; }
    }

    public class Price
    {
        public string Period { get; set; }
        public string Summ { get; set; }
    }
}