﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DomainModel.Content;

namespace WebSite.Areas.Cabinet.Models
{
    public class LandingViewModel
    {        
        public string Url { get; set; }
        public string Name { get; set; }
        public string StringKey { get; set; }
        public string Title { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeyWords { get; set; }
        public string SiteContent { get; set; }
        public string LastLoadScripts { get; set; }
        public string Styles { get; set; }
        public string Scripts { get; set; }
        public string Type { get; set; }
        public bool IsTemplate { get; set; }
        public string Image { get; set; }
        public int? UserId { get; set; }
        public bool IsPublic { get; set; }
        public string Litebox { get; set; }
        public string Links { get; set; }
        public string PageLoader { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Updated { get; set; }
        public string Email { get; set; }

        public static LandingViewModel FromDomainModel(Landing landing)
        {
            var result = new LandingViewModel
            {
                Url = landing.Url,
                Name = landing.Name,
                StringKey = landing.StringKey,
                Title = landing.Title,
                Image = landing.Image,
                IsTemplate = landing.IsTemplate,
                MetaDescription = landing.MetaDescription,
                MetaKeyWords = landing.MetaKeyWords,
                SiteContent = landing.SiteContent,
                LastLoadScripts = landing.LastLoadScripts,
                Styles = landing.Styles,
                Scripts = landing.Scripts,
                Links = landing.Links,
                PageLoader = landing.PageLoader,
                Type = landing.Type,
                UserId = landing.UserId,
                IsPublic = landing.IsPublic,
                Created = landing.Created,
                Updated = landing.Updated,
                Email = landing.Email
            };
            return result;
        }
    }

    public class ListLandingViewModel
    {
        public string Url { get; set; }
        public List<LandingViewModel> Landings { get; set; }
        public bool IsTemplate { get; set; }
    }

    public class StepsViewModel
    {
        public string CurentTab { get; set;  }
        public DateTime Time { get; set;  }
        public string Url { get; set; }
        public List<LandingViewModel> Landings { get; set; }
        public int CurrentNumber { get; set; }
    }

    public class AnimateViewModel
    {
        public string Animate { get; set; }
    }
}