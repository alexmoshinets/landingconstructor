﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebSite.Areas.Cabinet.Models
{
    public class ConstructionViewModel
    {
        public List<string> Fonts { get; set; }
    }
}