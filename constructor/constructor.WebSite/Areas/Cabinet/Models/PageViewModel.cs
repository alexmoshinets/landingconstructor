﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSite.Areas.Cabinet.Models
{
    public class PageViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeyWords { get; set; }
        public string StringKey { get; set; }
        public string SiteContent { get; set; }
        public string TabletContent { get; set; }
        public string MobileContent { get; set; }
        public int UserId { get; set; }
        public bool IsPublic { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Updated { get; set; }

    }
}