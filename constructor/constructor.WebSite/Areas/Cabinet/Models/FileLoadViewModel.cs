﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DomainModel.Content;

namespace WebSite.Areas.Cabinet.Models
{
    public class LoadFileViewModel
    {
        public List<string> Catalogs { get; set; }

        [Display(Name = "Укажите каталог:")]
        public string Catalog { get; set; }
        public HttpPostedFileBase File { get; set; }
    }
    public class LoadUrlViewModel
    {
        public List<SelectListItem> Catalogs { get; set; }
        [Display(Name = "Укажите каталог:")]
        public string Catalog { get; set; }
        public string UrlImage { get; set; }
    }
}