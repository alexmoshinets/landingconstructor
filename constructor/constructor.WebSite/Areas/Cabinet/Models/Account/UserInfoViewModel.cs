﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSite.Areas.Cabinet.Models.Account
{
    public class UserInfoViewModel
    {
        public string UserName { get; set; }
        public bool IsNotify { get; set; }
        public string Phone { get; set; }
    }
}