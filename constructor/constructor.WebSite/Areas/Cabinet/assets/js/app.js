﻿var tariffs = function($) {
    return {
        init: function () {
            $('.period').click(function () {
                var $this = $(this),
                    period = $this.data('months');
                $('.period').removeClass('active');
                $('.fa-check').hide();
                $this.addClass('active');
                $this.children('.fa-check').show();
                $this.children('input').prop('checked', true);
                $('.monthlyPrice').children('h4').removeClass('active');
                $('.monthlyPrice').children('h4[data-months="' + period + '"]').addClass('active');
            });

            $('.panel').click(function() {
                var $this = $(this);
                $('.panel').removeClass('active');
                $this.addClass('active');
                $this.children('.panel-footer').children('input').prop('checked', true);
            });
        }
    };
}(jQuery);

$(function () {
    tariffs.init();
});