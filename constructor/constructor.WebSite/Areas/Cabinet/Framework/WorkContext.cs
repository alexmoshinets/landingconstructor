﻿using System.Collections.Generic;
using System.Web;
using System.Web.Hosting;


namespace WebSite.Areas.Cabinet.Framework
{
    public class WorkContext
    {
        private readonly HttpContextBase _httpContextBase;
        private readonly HttpRequestBase _httpRequestBase;
        private readonly HttpResponseBase _httpResponseBase;
        private readonly VirtualPathProvider _virtualPathProvider;


        public WorkContext(HttpContextBase httpContextBase, HttpRequestBase httpRequestBase,
            HttpResponseBase httpResponseBase,
            VirtualPathProvider virtualPathProvider)
        {
            _httpContextBase = httpContextBase;
            _httpRequestBase = httpRequestBase;
            _httpResponseBase = httpResponseBase;
            _virtualPathProvider = virtualPathProvider;
        }

        public static string CssPath
        {
            get { return "/Areas/Cabinet/lib/Timers/FlipClock-master/flipclock.css"; }
        }
        public string CssAbsPath
        {
            get { return _httpContextBase.Server.MapPath(CssPath); }
        }
        public static string FilesPath
        {
            get { return "/Areas/Cabinet/Gallery/"; }
        }

        public string FilesAbsPath
        {
            get { return _httpContextBase.Server.MapPath(FilesPath); }
        }

        public static string ImagesPath
        {
            get { return "/Areas/Cabinet/Gallery/Images/"; }
        }

        public string ImagesAbsPath
        {
            get { return _httpContextBase.Server.MapPath(ImagesPath); }
        }       
        
        public static string UserImagesPath
        {
            get { return "/Areas/Cabinet/Gallery/UserFiles/"; }
        }

        public string UserImagesAbsPath
        {
            get { return _httpContextBase.Server.MapPath(UserImagesPath); }
        }

        public static string UserPagesPath
        {
            get { return "/Gallery/UserPages/"; }
        }

        public string UserPagesAbsPath
        {
            get { return _httpContextBase.Server.MapPath(UserImagesPath); }
        }

        public static string TemplatesImagePath
        {
            get { return "/Gallery/TemplatesImage"; }
        }

        public string TemplatesImageAbsPath
        {
            get { return _httpContextBase.Server.MapPath(UserImagesPath); }
        }  
    }
}