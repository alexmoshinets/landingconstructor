﻿using System.Web.Mvc;
using System.Web.Routing;

namespace WebSite.Areas.Cabinet.App_Start
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapMvcAttributeRoutes();

            //routes.MapRoute("Register", "Cabinet/Register", new { controller = "Account", action = "Register" },
            //    new[] { "WebSite.Controllers" }
            //    );

            //routes.MapRoute("LogIn", "Cabinet/LogIn", new { controller = "Account", action = "LogIn" },
            //    new[] { "WebSite.Controllers" }
            //    );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { Area = "Cabinet", controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

        }
    }
}
