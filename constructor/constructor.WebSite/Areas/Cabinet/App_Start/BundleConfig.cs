﻿using System.Web.Optimization;

namespace WebSite.Areas.Cabinet.App_Start
{
    public class BundleConfig
    {
        // Addition info http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/js")
                .Include("~/assets/plugins/jQuery-2.1.3/jquery-2.1.3.min.js")
                .Include("~/assets/plugins/bootstrap-3.3.1/js/bootstrap.js")
                .Include("~/assets/js/app.js")
            );

            bundles.Add(new StyleBundle("~/assets/production/css/css")
                .Include("~/assets/production/css/bootstrap.css")
                .Include("~/assets/production/css/font-awesome.min.css")
                .Include("~/assets/production/css/app.css")
                .Include("~/assets/production/css/style.css")
                .Include("~/assets/production/css/page_log_reg_v1.css")
                .Include("~/assets/production/css/plugins.css")
                .Include("~/assets/production/css/ie8.css")
                .Include("~/assets/production/css/plugins/animate.css")
                .Include("~/assets/production/css/plugins/box-shadow.css")
                .Include("~/assets/production/css/Layout.css")
                .Include("~/assets/production/css/Payment.css")
            );

            // Addition info http://go.microsoft.com/fwlink/?LinkId=301862
            BundleTable.EnableOptimizations = false;
        }
    }
}