﻿using System.Web;
using System.Web.Optimization;

namespace WebSite.Areas.Editor.App_Start
{
    public class BundleConfig
    {
        //Дополнительные сведения об объединении см. по адресу: http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Areas/Editor/Scripts/jquery-{version}.js",
                        "~/Areas/Editor/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/angularjs").Include(
                        "~/Areas/Editor/Scripts/angular.js",
                        "~/Areas/Editor/Scripts/angular-route.js",
                        "~/Areas/Editor/Scripts/angular-cookies.js"));

            bundles.Add(new ScriptBundle("~/bundles/lodash").Include(
                        "~/Areas/Editor/Scripts/lodash.js"));

            bundles.Add(new ScriptBundle("~/bundles/mousetrap").Include(
                        "~/Areas/Editor/Scripts/mousetrap.js"));

            bundles.Add(new ScriptBundle("~/bundles/moment").Include(
                        "~/Areas/Editor/Scripts/moment.js"));

            bundles.Add(new ScriptBundle("~/bundles/appScripts").Include(
                        "~/Areas/Editor/Scripts/AppScripts/Editor.js",
                        "~/Areas/Editor/Scripts/AppScripts/ru.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Areas/Editor/Content/Css/framework.css",
                      "~/Areas/Editor/Content/Css/editor.css",
                      "~/Areas/Editor/Content/Css/jquery.fileupload-ui.css",
                      "~/Areas/Editor/Content/Css/stuff.css",
                      "~/Areas/Editor/Content/Css/style.css"));

            // Присвойте EnableOptimizations значение false для отладки. Дополнительные сведения
            // см. по адресу: http://go.microsoft.com/fwlink/?LinkId=301862
            BundleTable.EnableOptimizations = false;
        }
    }
}
