﻿using System.Web.Mvc;

namespace WebSite.Areas.Editor.Controllers
{
    public class EditorController : Controller
    {
        public JsonResult ratra555()
        {
            var fakeObj = new[]
            {
                new 
                {
                    type = "Widget",
                    lang = "ru",
                    _value = string.Empty,
                    _attributes = new 
                    {
                        _uid = "Zcc579e32ab7c2c234c287cbcbc03f3a7639f9f1"
                    },
                    _children = new[]
                    {
                        new
                        {
                            type = "Section",
                            lang = "ru",
                            _value = string.Empty,
                            _attributes = new
                            {
                                _uid = "VbAH1H7oOcTviNLA3T42vrV7SvyfEpGN",
                                blocked = false,
                                cloneable = false,
                                deletable = false,
                                draggable = false,
                                droppable = true,
                                resizable = true,
                                stackable = false
                            },
                            _children = new string[] {},
                            _parameters = new
                            {
                                Styles = new
                                {
                                    desktop = new
                                    {
                                        height = new {value = "800px"},
                                        width = new {value = "960px"},
                                        position = new {value = "relative"},
                                        overflow = new {value = "visible"},
                                        top = new {value = ""},
                                        background = new
                                        {
                                            value = new
                                            {
                                                container = false,
                                                gradient = new {enabled = false, type = "vertical" },
                                                repeat = "repeat",
                                                size = new {contain = false, cover = false, dimensions = false}
                                            }
                                        }
                                    },
                                    mobile = new
                                    {
                                        height = new {value = "800px"},
                                        width = new {value = "320px"}
                                    },
                                    tablet = new
                                    {
                                        width = new {value = "760px"}
                                    }
                                }
                            }
                        }
                    },
                    _parameters = new
                    {
                        Styles = new
                        {
                            desktop = new
                            {
                                background = new
                                {
                                    value = new
                                    {
                                        color = new {r = "255", g = "255", b = "255", a = "1"},
                                        container = false,
                                        gradient = new {enabled = false, type = "vertical"},
                                        repeat = "repeat"
                                    }
                                },
                                border = new
                                {
                                    value = new
                                    {
                                        bottom = new {enabled = false},
                                        left = new {enabled = false},
                                        right = new {enabled = false},
                                        top = new {enabled = false}
                                    }
                                }
                            },
                            tablet = new string[] {},
                            mobile = new string[] {}
                        },
                        Fonts = new
                        {
                            fonts = new[] {"arial"}
                        },
                        Viewports = new
                        {
                            desktop = true,
                            mobile = false,
                            tablet = false
                        }
                    }
                }
            };

            return Json(fakeObj, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Versions()
        {
            var fakeObj = new
            {
                meta = new { collection = new { count = 5 } },
                response = new[]
                {
                    new { version = "5", created = "2014-07-06 19:14:36" },
                    new { version = "4", created = "2014-07-05 19:14:36" },
                    new { version = "3", created = "2014-07-04 19:14:36" },
                    new { version = "2", created = "2014-07-03 19:14:36" },
                    new { version = "1", created = "2014-07-02 19:14:36" }
                }
            };

            return Json(fakeObj, JsonRequestBehavior.AllowGet);
        }
    }
}