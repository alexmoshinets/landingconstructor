﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebSite.Areas.Cabinet.Framework;
using WebSite.Controllers;
using DomainModel.Content;
using Interfaces.Content;
using Services;


namespace WebSite.Areas.Editor.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILandingService _landingService;
        public HomeController(ILandingService landingService)
        {
            _landingService = landingService;
        }

        public ActionResult Index()
        {
            var membershipUser = Membership.GetUser();
            if (membershipUser != null)
            {
                var user = membershipUser.ProviderUserKey;
                if (user != null)
                {
                }
                else
                {
                    return RedirectToAction("Editor");
                }

            }
            else
            {
                return RedirectToAction("Editor");
            }

            return View();
        }
        public ActionResult Editor()
        {
            return View();
        }

        public ActionResult Fonts()
        {
            var fonts = FontFamily.Families.Where(font => font != null).Select(font => font.Name).ToList();
            return View(fonts);
        }

        [HttpPost]
        public ActionResult LoadImages(string patch)
        {
            var dir = new DirectoryInfo(Server.MapPath(WorkContext.ImagesPath) + patch);// папка с файлами 
            var files = dir.GetFiles().Select(file => WorkContext.ImagesPath + patch + "/" + file.Name).ToArray(); // список для имен файлов 
            return Json(new { res = files, state = "ok" });
        }

        [HttpPost]
        public JsonResult SaveImage(HttpPostedFileBase file)
        {

            if (!Directory.Exists(Server.MapPath(WorkContext.ImagesPath + "TestUser")))
            {
                Directory.CreateDirectory(Server.MapPath(WorkContext.ImagesPath + "TestUser"));
            }

            if (file != null)
            {
                var path = Server.MapPath(WorkContext.ImagesPath + "TestUser" + file.FileName);
                file.SaveAs(path);
            }
            return Json(Server.MapPath(WorkContext.ImagesPath + "TestUser" + file.FileName));
        }

        [ValidateInput(false)]
        [HttpPost]
        public string SavePages(Array page)
        {
            var p = (from object item in page select item.ToString()).ToList();
            var model = new Landing()
           {
               Title = p[0],
               StringKey = p[1],
               Type = p[2],
               MetaDescription = p[3],
               MetaKeyWords = p[4],
               SiteContent = p[5],
               TabletContent = p[6],
               MobileContent = p[7],
               Created = DateTime.Now
           };

            _landingService.AddOrUpdate(model.StringKey, model, landing =>
            {
                landing.Title = p[0];
                landing.StringKey = p[1];
                landing.Type = p[2];
                landing.MetaDescription = p[3];
                landing.MetaKeyWords = p[4];
                landing.SiteContent = p[5];
                landing.TabletContent = p[6];
                landing.MobileContent = p[7];
                landing.Created = DateTime.Now;
            });

            return "ok!";
        }
    }
}
