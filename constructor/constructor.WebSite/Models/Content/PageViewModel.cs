﻿using DomainModel.Content;

namespace WebSite.Models.Content
{
    public class PageViewModel
    {
        public string Content { get; set; }
        public string BottomContent { get; set; }

        public string MetaTitle { get; set; }
        public string MetaKeywords { get; set; }
        public string MetaDescription { get; set; }

        public static PageViewModel FromDomainModel(Page page)
        {
            var result = new PageViewModel
            {
                Content = page.Content,                
                BottomContent = page.BottomContent,
                MetaTitle = page.MetaTitle,
                MetaKeywords = page.MetaKeywords,
                MetaDescription = page.MetaDescription
            };

            return result;
        }
    }
}