﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DomainModel.Navigation;

namespace WebSite.Models.Common
{
    public class MenuItemViewModel
    {
        public MenuItemViewModel()
        {
            Childs = new List<MenuItemViewModel>();
        }

        public string Url { get; set; }

        public string Title { get; set; }
        public bool IsActive { get; set; }

        public List<MenuItemViewModel> Childs { get; set; }

        public static MenuItemViewModel FromDomainModel(MenuItem menuItem, Controller controller)
        {
            var url = controller.Url;
            var currentController =
                controller.ControllerContext.ParentActionViewContext.Controller.ValueProvider.GetValue("controller")
                    .RawValue.ToString();
            //var currentAction = controller.ValueProvider.GetValue("action").RawValue.ToString();

            var result = new MenuItemViewModel();

            result.Title = menuItem.Name;
            switch (menuItem.Type)
            {
                case MenuItemType.Url:
                    result.Url = menuItem.CustomUrl;
                    break;
                case MenuItemType.Page:
                    if (menuItem.Page.IsSection)
                    {
                        result.Url = url.Action("Index", menuItem.Page.StringKey);
                        result.IsActive = currentController.Equals(menuItem.Page.StringKey);
                    }
                    else
                    {
                        result.Url = url.Action("Index", "Page", new { menuItem.Page.StringKey });
                        result.IsActive = currentController.Equals("Page") &&
                                          menuItem.Page.StringKey.Equals(controller.RouteData.Values["stringKey"]);
                    }
                    break;
                default:
                    result.Url = url.Action("Index", "Home");
                    break;
            }

            if (menuItem.Childs != null)
            {
                result.Childs = menuItem.Childs.Select(_ => FromDomainModel(_, controller)).ToList();
            }

            return result;
        }

        public void Reset()
        {
            IsActive = false;

            foreach (var subItem in Childs)
            {
                subItem.IsActive = false;
            }
        }
    }
}