﻿using DomainModel.Content;
using WebSite.Framework;

namespace WebSite.Models.Common
{
    public class SliderViewModel
    {
        public string Title { get; set; }

        public string Image { get; set; }
        public string Url { get; set; }

        public static SliderViewModel FromDomainModel(Slider slider)
        {
            return new SliderViewModel
            {
                Title = slider.Title,
                Image = slider.Image,
                Url = slider.Url
            };
        }
    }
}