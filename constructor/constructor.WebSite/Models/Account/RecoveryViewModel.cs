﻿using System.ComponentModel.DataAnnotations;

namespace WebSite.Models.Account
{
    public class RecoveryViewModel
    {
        [Required(ErrorMessage = "Поле {0} обязательно для заполнения")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "E-mail", Description = "Введите емаил")]
        [RegularExpression("^([A-Za-z0-9_\\-\\.])+\\@([A-Za-z0-9_\\-\\.])+\\.([A-Za-z]{2,4})$",
            ErrorMessage = "Неверный емаил.")]
        public string Email { get; set; }
    }
}