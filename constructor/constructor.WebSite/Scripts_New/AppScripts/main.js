var Workspace = function () {

    var _parent = window.parent,
        _preview = false,
        _mode = 'NORMAL',
        _activeUid = '',
        _sectionsMap = [],
        _viewport = 'desktop',
        _selecting = false,
        _dragging = false;

    var CaptureKey = function () {
        return {
            init: function () {
                $(window).keydown(function (e) {
                    if (_mode === 'NORMAL' && e.keyCode >= 37 && e.keyCode <= 40) {
                        _parent.notify('key:move', {
                            key: e.keyCode,
                            shift: e.shiftKey || e.metaKey
                        });

                        return false;
                    }
                });

                $(window).keyup(function (e) {
                    if (_mode === 'NORMAL' && e.keyCode >= 37 && e.keyCode <= 40) {
                        _parent.notify('key:move:stop');

                        return false;
                    }
                });
            }
        };
    };

    var captureKey = new CaptureKey();

    //   _____ _____  _____ _____
    //  / ____|  __ \|_   _|  __ \
    // | |  __| |__) | | | | |  | |
    // | | |_ |  _  /  | | | |  | |
    // | |__| | | \ \ _| |_| |__| |
    //  \_____|_|  \_\_____|_____/

    var Grid = function () {
        return {
            /**
             * Render grid
             * @param  {Boolean} enabled
             */
            render: function (enabled) {
                var $grid = $('#landingi-grid');

                if (enabled) {
                    $grid.addClass('in');
                } else {
                    $grid.removeClass('in');
                }
            }
        };
    };

    var grid = new Grid();

    // __          _______ _____   _____ ______ _______ _____
    // \ \        / /_   _|  __ \ / ____|  ____|__   __/ ____|
    //  \ \  /\  / /  | | | |  | | |  __| |__     | | | (___
    //   \ \/  \/ /   | | | |  | | | |_ |  __|    | |  \___ \
    //    \  /\  /   _| |_| |__| | |__| | |____   | |  ____) |
    //     \/  \/   |_____|_____/ \_____|______|  |_| |_____/

    var BaseWidgetView = {

        attach: function (uid) {
            this.setDraggable(uid);
        },

        setDraggable: function (uid, options) {
            var selector = '[data-uid="' + uid + '"]',
                originalPosition,
                element = $(selector),
                o = {
                    // Blokujemy przesuwanie:
                    // 1. zagnieżdżone widgety nie mogą uruchamiać przesuwania widgetu nadrzędnego
                    // 2. na uchwytach do zmiany rozmiaru
                    // 3. gdy jest zablokowany
                    // 4. gdy jest buttonem w formularzu
                    cancel: selector + ' .widget, ' + selector + '.widget-blocked, form .widget-input-button, .widget-counter .widget-text',
                    containment: element.parents('.widget-button, .widget-buy, .widget-contact').first(),
                    distance: 10,
                    snap: '.grid',
                    snapTolerance: 15,
                    start: function (event, ui) {
                        _dragging = true;

                        originalPosition = ui.helper.offset();

                        _parent.notify('drag:start', {
                            uid: uid
                        });
                    },
                    drag: function (event, ui) {
                        var position = ui.helper.offset();

                        _parent.notify('drag:move', {
                            uid: uid,
                            position: position,
                            delta: {
                                top: position.top - originalPosition.top,
                                left: position.left - originalPosition.left
                            },
                            event: event
                        });
                    },
                    stop: function (event, ui) {
                        _dragging = false;

                        _parent.notify('drag:stop', {
                            uid: uid,
                            position: ui.helper.offset(),
                            event: event
                        });
                    }
                };

            element.draggable(_.merge(o, options));
        }
    };

    var views = {
        'widget': _.clone(BaseWidgetView),
        'box': _.extend(_.clone(BaseWidgetView), {}),
        'button': _.extend(_.clone(BaseWidgetView), {
            attach: function (uid) {
                this.setDraggable(uid);

                $('[data-uid="' + uid + '"]').droppable({
                    accept: '[data-uid="' + uid + '"] > .widget',
                    greedy: true,
                    tolerance: 'pointer',
                    drop: function (event, ui) {
                        var $this = $(this),
                            crop = {
                                v: parseInt($this.css('border-top-width'), 10) +
                                    parseInt($this.css('padding-top'), 10),
                                h: parseInt($this.css('border-left-width'), 10) +
                                    parseInt($this.css('padding-left'), 10)
                            };

                        _parent.notify('drop:button', {
                            uid: ui.draggable.attr('data-uid'),
                            btnOffset: {
                                top: parseInt($this.css('top'), 10),
                                left: parseInt($this.css('left'), 10)
                            },
                            sectionUid: $this.parents('.widget-section').attr('data-uid'),
                            position: ui.draggable.offset(),
                            crop: crop
                        });

                        ui.draggable.removeAttr('style');
                    }
                });
            }
        }),
        'buy': _.extend(_.clone(BaseWidgetView), {
            attach: function (uid) {
                this.setDraggable(uid);

                $('[data-uid="' + uid + '"]').droppable({
                    accept: '[data-uid="' + uid + '"] > .widget',
                    greedy: true,
                    tolerance: 'pointer',
                    drop: function (event, ui) {
                        var $this = $(this),
                            crop = {
                                v: parseInt($this.css('border-top-width'), 10) +
                                    parseInt($this.css('padding-top'), 10),
                                h: parseInt($this.css('border-left-width'), 10) +
                                    parseInt($this.css('padding-left'), 10)
                            };

                        _parent.notify('drop:button', {
                            uid: ui.draggable.attr('data-uid'),
                            btnOffset: {
                                top: parseInt($this.css('top'), 10),
                                left: parseInt($this.css('left'), 10)
                            },
                            sectionUid: $this.parents('.widget-section').attr('data-uid'),
                            position: ui.draggable.offset(),
                            crop: crop
                        });

                        ui.draggable.removeAttr('style');
                    }
                });
            }
        }),
        'checkbox': _.extend(_.clone(BaseWidgetView), {
            attach: function () { }
        }),
        'container': _.extend(_.clone(BaseWidgetView), {
            attach: function () { }
        }),
        'counter': _.extend(_.clone(BaseWidgetView), {}),
        'form': _.extend(_.clone(BaseWidgetView), {}),
        'hidden': _.clone(BaseWidgetView),
        'html': _.extend(_.clone(BaseWidgetView), {}),
        'image': _.extend(_.clone(BaseWidgetView), {}),
        'input/text': _.extend(_.clone(BaseWidgetView), {
            attach: function () { }
        }),
        'section': _.extend(_.clone(BaseWidgetView), {
            attach: function (uid) {
                $('[data-uid="' + uid + '"]').droppable({
                    accept: '.widget',
                    greedy: true,
                    tolerance: 'pointer',
                    drop: function (event, ui) {
                        _parent.notify('drop', {
                            uid: ui.draggable.attr('data-uid'),
                            srcSectionUid: ui.draggable.parents('.widget-section').attr('data-uid'),
                            destSectionUid: this.getAttribute('data-uid'),
                            position: ui.draggable.offset()
                        });

                        ui.draggable.removeAttr('style');
                    }
                });
            }
        }),
        'select': _.extend(_.clone(BaseWidgetView), {
            attach: function () { }
        }),
        'social': _.extend(_.clone(BaseWidgetView), {}),
        'input/button': _.extend(_.clone(BaseWidgetView), {
            attach: function () { }
        }),
        'text': _.extend(_.clone(BaseWidgetView), {
            attach: function (uid) {
                if (!$('[data-uid="' + uid + '"]').hasClass('widget-label') &&
                    ($('[data-uid="' + uid + '"]').attr('data-tag') !== 'label')) {
                    this.setDraggable(uid);
                }
            }
        }),
        'textarea': _.extend(_.clone(BaseWidgetView), {
            attach: function () { }
        }),
        'video': _.extend(_.clone(BaseWidgetView), {})
    };

    //  _______ ________   _________    ______ _____ _____ _______ ____  _____
    // |__   __|  ____\ \ / /__   __|  |  ____|  __ \_   _|__   __/ __ \|  __ \
    //    | |  | |__   \ V /   | |     | |__  | |  | || |    | | | |  | | |__) |
    //    | |  |  __|   > <    | |     |  __| | |  | || |    | | | |  | |  _  /
    //    | |  | |____ / . \   | |     | |____| |__| || |_   | | | |__| | | \ \
    //    |_|  |______/_/ \_\  |_|     |______|_____/_____|  |_|  \____/|_|  \_\

    var TextEditor = function () {
        var _selection,
            lastColorChange;

        var getContainingElement = function (isStart) {
            if (document.selection) {
                range = document.selection.createRange();
                range.collapse(isStart);

                return range.parentElement();
            } else {
                sel = window.getSelection();
                if (sel.getRangeAt) {
                    if (sel.rangeCount > 0) {
                        range = sel.getRangeAt(0);
                    }
                } else {
                    // Old WebKit
                    range = document.createRange();
                    range.setStart(sel.anchorNode, sel.anchorOffset);
                    range.setEnd(sel.focusNode, sel.focusOffset);

                    // Handle the case when the selection was selected backwards (from the end to the start in the document)
                    if (range.collapsed !== sel.isCollapsed) {
                        range.setStart(sel.focusNode, sel.focusOffset);
                        range.setEnd(sel.anchorNode, sel.anchorOffset);
                    }
                }

                if (range) {
                    container = range[isStart ? 'startContainer' : 'endContainer'];

                    // Check if the container is a text node and return its parent if so
                    return container.nodeType === 3 ? container.parentNode : container;
                }
            }
        };

        return {
            /**
             * Get text editor height
             * @return {Integer}
             */
            height: function () {
                var h = 0;

                var paragraphs = $('[contenteditable="true"] p');

                if (paragraphs.length > 0) {
                    paragraphs.each(function () {
                        h += $(this).outerHeight();
                    });
                } else {
                    h = $('[contenteditable="true"] .node-content').outerHeight();
                }

                return h;
            },
            /**
             * Close text edit mode
             */
            close: function () {
                _parent.notify('closeTextEditor', {
                    uid: _activeUid,
                    text: $('[data-uid="' + _activeUid + '"]').html()
                });

                this.clearSelection();

                $('[contenteditable]').removeAttr('contenteditable').off('input');

                workspace.enableDrag();

                _mode = 'NORMAL';
            },
            stripTags: function (allowedTags) {
                var $editable = $('[contenteditable]'),
                    text = $editable.html();

                if (allowedTags === undefined) {
                    allowedTags = '<b><strong><i><em><u><br><p>';
                }

                function strip_tags(input, allowed) {
                    allowed = (((allowed || "") + "").toLowerCase().match(/<[a-z][a-z0-9]*>/g) || []).join(''); // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
                    var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
                        commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;

                    return input.replace(commentsAndPhpTags, '').replace(tags, function ($0, $1) {
                        return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
                    });
                }

                text = strip_tags(text, allowedTags);
                text = text.replace(/<p (.*?)>/ig, '<p>');

                $editable.html(text);
            },
            paste: function (e) {
                var content;

                if ((e.originalEvent || e).clipboardData) {
                    content = (e.originalEvent || e).clipboardData.getData('text/plain');
                    document.execCommand('insertText', false, content);
                } else if (window.clipboardData) {
                    content = window.clipboardData.getData('Text');
                    document.selection.createRange().pasteHTML(content);
                }
            },
            bold: function () {
                document.execCommand('bold', null, false);
            },
            italic: function () {
                document.execCommand('italic', null, false);
            },
            underline: function () {
                document.execCommand('underline', null, false);
            },
            strikethrough: function () {
                document.execCommand('strikeThrough', null, false);
            },
            link: function (o) {
                this.saveSelection();

                var link = this.getLink();
                var range;
                var insert = false;

                if (!link) {
                    link = document.createElement('a');
                    range = window.getSelection().getRangeAt(0);

                    insert = true;
                }

                link.href = o.href;
                link.target = o.targetBlank ? '_blank' : '_self';
                link.style.color = o.color;
                link.style.textDecoration = o.underline ? 'underline' : 'none';

                if (insert) {
                    link.innerHTML = this.getSelectedText();

                    range.deleteContents();
                    range.insertNode(link);
                }

                this.restoreSelection();
            },
            unlink: function () {
                this.restoreSelection();
                var link = this.getLink();

                if (link) {
                    $(link).contents().unwrap();
                }
            },
            getLink: function () {
                var element = getContainingElement(true);

                // Traverse up until you find link parent
                while (element && element.nodeName.toLowerCase() !== 'a') {
                    element = element.parentNode;
                }

                return element;
            },
            fontFamily: function (fontFamily) {
                var color = this.getStyleProperty('color') || '',
                    text = this.getSelectedText(),
                    newText = text.replace(/font-family:[a-z,\d\s\-']*;?/gi, ''),
                    _html = '<span style=\"color: ' + color + ';\">' + newText + '</span>';

                if (text.replace(/(<([^>]+)>)/ig, '') !== '') {
                    this.saveSelection();

                    document.execCommand('insertHTML', null, _html);

                    this.restoreSelection();
                }
            },
            fontSize: function (size) {
                var color = this.getStyleProperty('color') || '',
                    text = this.getSelectedText(),
                    newText = text.replace(/font-size:[\d]*(px|em)?;?/gi, ''),
                    _html = '<span style=\"color:' + color + ';\">' + newText + '</span>';

                if (text.replace(/(<([^>]+)>)/ig, '') !== '') {
                    this.saveSelection();

                    document.execCommand('insertHTML', null, _html);

                    this.restoreSelection();
                }
            },
            color: function (color) {
                var size = this.getStyleProperty('font-size') || '',
                    text = this.getSelectedText(),
                    newText = text.replace(/color:#?[a-z\s\d(),]*;/gi, ''),
                    _html = '<span style=\"color:' + color + ';\">' + newText + '</span>';

                if (text.replace(/(<([^>]+)>)/ig, '') !== '') {
                    this.saveSelection();

                    document.execCommand('styleWithCSS', null, true);
                    document.execCommand('foreColor', null, color);

                    this.restoreSelection();
                }
            },
            /**
             * Save selection
             */
            saveSelection: function () {
                var doc, win, range, preSelectionRange, start,
                    containerEl = $('[contenteditable]')[0];

                if (window.getSelection && document.createRange) {
                    doc = containerEl.ownerDocument;
                    win = doc.defaultView;
                    range = win.getSelection().getRangeAt(0);
                    preSelectionRange = range.cloneRange();

                    preSelectionRange.selectNodeContents(containerEl);
                    preSelectionRange.setEnd(range.startContainer, range.startOffset);

                    start = preSelectionRange.toString().length;

                    _selection = {
                        start: start,
                        end: start + range.toString().length
                    };

                } else if (document.selection) {
                    doc = containerEl.ownerDocument;
                    win = doc.defaultView || doc.parentWindow;
                    selectedTextRange = doc.selection.createRange();

                    var preSelectionTextRange = doc.body.createTextRange();

                    preSelectionTextRange.moveToElementText(containerEl);
                    preSelectionTextRange.setEndPoint('EndToStart', selectedTextRange);

                    start = preSelectionTextRange.text.length;

                    _selection = {
                        start: start,
                        end: start + selectedTextRange.text.length
                    };
                }
            },
            /**
             * Restore selection
             */
            restoreSelection: function () {
                if (_selection) {
                    var doc, win,
                        containerEl = $('[contenteditable]')[0];

                    if (window.getSelection && document.createRange) {
                        doc = containerEl.ownerDocument;
                        win = doc.defaultView;
                        var charIndex = 0, range = doc.createRange();
                        range.setStart(containerEl, 0);
                        range.collapse(true);
                        var nodeStack = [containerEl], node, foundStart = false, stop = false;

                        while (!stop && (node = nodeStack.pop())) {
                            if (node.nodeType === 3) {
                                var nextCharIndex = charIndex + node.length;

                                if (!foundStart && _selection.start >= charIndex && _selection.start <= nextCharIndex) {
                                    range.setStart(node, _selection.start - charIndex);
                                    foundStart = true;
                                }
                                if (foundStart && _selection.end >= charIndex && _selection.end <= nextCharIndex) {
                                    range.setEnd(node, _selection.end - charIndex);
                                    stop = true;
                                }
                                charIndex = nextCharIndex;
                            } else {
                                var i = node.childNodes.length;
                                while (i--) {
                                    nodeStack.push(node.childNodes[i]);
                                }
                            }
                        }
                        var sel = win.getSelection();
                        sel.removeAllRanges();
                        sel.addRange(range);
                    } else if (document.selection) {
                        doc = containerEl.ownerDocument;
                        win = doc.defaultView || doc.parentWindow;

                        var textRange = doc.body.createTextRange();

                        textRange.moveToElementText(containerEl);
                        textRange.collapse(true);
                        textRange.moveEnd('character', _selection.end);
                        textRange.moveStart('character', _selection.start);
                        textRange.select();
                    }
                }
            },
            /**
             * Clear selection
             * @return {void}
             */
            clearSelection: function () {
                if (window.getSelection) {
                    if (window.getSelection().empty) {
                        window.getSelection().empty();
                    } else if (window.getSelection().removeAllRanges) {
                        window.getSelection().removeAllRanges();
                    }
                } else if (document.selection) {
                    document.selection.empty();
                }
            },
            /**
             * Get selected text with formatting
             * @return {String}
             */
            getSelectedText: function () {
                var html = '';

                if (typeof window.getSelection !== 'undefined') {
                    var sel = window.getSelection();

                    if (sel.rangeCount) {
                        var container = document.createElement('div');
                        for (var i = 0, len = sel.rangeCount; i < len; ++i) {
                            var bold, italic, underline, strike,
                                contents = sel.getRangeAt(i).cloneContents();

                            if (document.queryCommandState('bold')) {
                                bold = document.createElement('b');

                                bold.appendChild(contents);
                                contents = bold;
                            }
                            if (document.queryCommandState('italic')) {
                                italic = document.createElement('i');

                                italic.appendChild(contents);
                                contents = italic;
                            }
                            if (document.queryCommandState('underline')) {
                                underline = document.createElement('u');

                                underline.appendChild(contents);
                                contents = underline;
                            }
                            if (document.queryCommandState('strikethrough')) {
                                strike = document.createElement('strike');

                                strike.appendChild(contents);
                                contents = strike;
                            }
                            container.appendChild(contents);
                        }
                        html = container.innerHTML;
                    }
                } else if (typeof document.selection !== 'undefined') {
                    if (document.selection.type === 'Text') {
                        html = document.selection.createRange().htmlText;
                    }
                }
                return html;
            },
            /**
             * Get style of selected text
             * @param  {String} propName property name
             * @return {String}          computed style
             */
            getStyleProperty: function (propName) {
                var containerEl, sel, value;

                var isSingleNodeSelection = function (range) {
                    var startNode = range.startContainer;

                    return startNode === range.endContainer && startNode.hasChildNodes() &&
                            range.endOffset === range.startOffset + 1;
                };

                if (window.getSelection) {
                    sel = window.getSelection();
                    if (sel.rangeCount) {
                        var _range = sel.getRangeAt(0);

                        if (isSingleNodeSelection(_range)) {
                            containerEl = _range.startContainer.childNodes[_range.startOffset];
                        } else if (_range.startContainer.nodeType === 3) {
                            // Make sure we have an element rather than a text node
                            containerEl = _range.startContainer.parentNode;
                        } else {
                            containerEl = _range.startContainer;
                        }
                    }
                } else if ((sel = document.selection) && sel.type !== 'Control') {
                    containerEl = sel.createRange().parentElement();
                }

                if (containerEl) {
                    // Check if containerEl is a Text node
                    if (containerEl.nodeType === 3) {
                        // Make sure we have an element rather than a text node
                        containerEl = containerEl.parentNode;
                    }

                    if (window.getComputedStyle) {
                        value = window.getComputedStyle(containerEl, null)[propName];

                        if (!value) {
                            value = window.getComputedStyle(containerEl, null).getPropertyValue(propName);
                        }

                        return value;
                    } else if (containerEl.currentStyle) {
                        return containerEl.currentStyle[propName];
                    }
                }
            },
            getFontFamily: function () {
                var fontFamily = this.getStyleProperty('font-family') || '';

                return fontFamily.toLowerCase().split(',')[0].replace(/\'/g, '');
            },
            /**
             * @param  {String}  prop
             * @return {Boolean}
             */
            isSelection: function (prop) {
                return document.queryCommandState(prop);
            },
            /**
             * Check if selected text is a link
             * @return {Boolean}
             */
            isLink: function () {
                var element = getContainingElement(true);

                // Traverse up until you find link parent
                while (element && element.nodeName.toLowerCase() !== 'a') {
                    element = element.parentNode;
                }

                return element ? element.nodeName.toLowerCase() === 'a' : false;
            },
            /**
             * Get element
             * @return {Object}
             */
            getElement: function () {
                return getContainingElement(true);
            }
        };
    };

    var textEditor = new TextEditor();

    var Selection = function () {
        var _element;

        return {
            init: function () {
                _element = $('#landingi-workspace');

                _element.selectable({
                    cancel: '.widget-container, [contenteditable="true"], input, textarea, button, select, option, p',
                    delay: 100,
                    filter: '[data-ui-selectable]:visible',
                    containment: '.widget-section',
                    start: function () {
                        _parent.notify('selection:start');
                        _selecting = true;

                        $('.editing-widget').removeClass('editing-widget');
                        _parent.notify('selection:clear');
                    },
                    selecting: function (event, ui) {
                        _parent.notify('selection:add', ui.selecting.getAttribute('data-uid'));
                    },
                    unselecting: function (event, ui) {
                        _parent.notify('selection:remove', ui.unselecting.getAttribute('data-uid'));
                    },
                    stop: function (event) {
                        _parent.notify('selection:stop');

                        /* Prevent Firefox from firing onClick on the section if the
                         * selecting cursor is moved upwards
                         */
                        setTimeout(function () {
                            _selecting = false;
                        }, 250);

                        var selected = [];

                        $('.ui-selected').each(function () {
                            selected.push(this.getAttribute('data-uid'));
                        });

                        _parent.notify('selection:set', {
                            items: selected
                        });
                    }
                });
            },
            enable: function () {
                _element.selectable('enable');
            },
            disable: function () {
                _element.selectable('disable');
            }
        };
    };

    var _selection = new Selection();

    return {
        /**
         * @type {Grid}
         */
        grid: grid,
        /**
         * Text editor object
         * @type {TextEditor}
         */
        textEditor: textEditor,
        captureKey: captureKey,
        selection: _selection,
        /**
         * Attach key shortcuts events
         */
        bindKeyShortCuts: function () {
            var Mousetrap = window.Mousetrap;

            Mousetrap.bind(['ctrl+s', 'command+s'], function () {
                _parent.notify('save');

                return false;
            });

            Mousetrap.bind(['ctrl+c', 'command+c'], function () {
                _parent.notify('copy');

                return false;
            });

            Mousetrap.bind(['ctrl+v', 'command+v'], function () {
                _parent.notify('paste');

                return false;
            });

            Mousetrap.bind(['del', 'command+backspace'], function () {
                _parent.notify('remove');

                return false;
            });

            Mousetrap.bind('f1', function () {
                _parent.notify('help');

                return false;
            });

            Mousetrap.bind('tab', function () {
                _parent.notify('switchPage');

                return false;
            });

            Mousetrap.bind('alt+1', function () {
                _parent.notify('view', 'desktop');

                return false;
            });

            Mousetrap.bind('alt+2', function () {
                _parent.notify('view', 'tablet');

                return false;
            });

            Mousetrap.bind('alt+3', function () {
                _parent.notify('view', 'mobile');

                return false;
            });

            Mousetrap.bind('backspace', function () {
                return false;
            });

            Mousetrap.bind(['ctrl+shift+d', 'command+shift+d'], function () {
                _parent.notify('clone');

                return false;
            });

            Mousetrap.bind(['ctrl+g', 'command+g'], function () {
                _parent.notify('group:create');

                return false;
            });

            Mousetrap.bind(['ctrl+shift+g', 'command+shift+g'], function () {
                _parent.notify('group:destroy');

                return false;
            });
        },
        setViewport: function (viewport) {
            _viewport = viewport;
        },
        /**
         * Post render (attach events)
         * @param  {String} uid
         */
        postRender: function (uid) {
            var that = this;

            var _postRender = function ($el, type) {
                views[type].attach($el.attr('data-uid'));

                $el.click(that.onClick);
                $el.mouseover(that.onHover);
                $el.dblclick(that.onDblClick);
                $el.on('contextmenu', that.contextMenu);

                if ($el.hasClass('ui-draggable')) {
                    $el.draggable('option', 'snap', '.landingi-grid');
                }
            };

            if (uid !== undefined) {
                var $el = $('[data-uid="' + uid + '"]');

                _postRender($el, $el.attr('data-type'));
            } else {
                $('.widget').each(function (i, el) {
                    _postRender($(el), el.getAttribute('data-type'));
                });
            }
        },
        /**
         * @param  {Array} fonts list of font's objects
         */
        renderFonts: function (fonts) {
            _.each(fonts, function (font) {
                if (font) {
                    var id = 'font-' + font.name.replace(/ /g, '-').toLowerCase();

                    if (font.link !== null && $('#' + id).length === 0) {
                        $('head').append('<link id="' + id + '" href="' + font.link + '" rel="stylesheet" type="text/css">');
                    }
                }
            });
        },
        /**
         * Preview mode on
         */
        preview: function () {
            _preview = true;

            $('#landingi-tools').css('display', 'none');

            this.disableDrag();
            this.selection.disable();
        },
        /**
         * Preview mode off
         */
        disablePreview: function () {
            _preview = false;

            $('#landingi-tools').css('display', 'block');

            this.enableDrag();
            this.selection.enable();
        },
        enableDrag: function () {
            $('.widget').each(function (i, el) {
                var $el = $(el);

                if ($el.hasClass('ui-draggable')) {
                    $el.draggable({
                        disabled: false
                    });
                }
            });
        },
        disableDrag: function () {
            $('.widget').each(function (i, el) {
                var $el = $(el);

                if ($el.hasClass('ui-draggable')) {
                    $el.draggable({
                        disabled: true
                    });
                }
            });
        },
        /**
         * @return {Boolean}
         */
        isPreview: function () {
            return _preview;
        },
        /**
         * Move widget
         * @param  {String} uid
         * @param  {Object} delta [description]
         */
        move: function (uid, delta) {
            $('[data-uid="' + uid + '"]').css({
                top: delta.top,
                left: delta.left
            });
        },
        /**
         * Resize widget
         * @param  {String} uid
         * @param  {Object} delta [description]
         */
        resize: function (uid, delta) {
            var widget = $('[data-uid="' + uid + '"]');

            if (widget.attr('data-type') === 'section') {
                widget.css({
                    top: delta.top,
                    left: delta.left,
                    height: delta.height
                });

                widget.children('.container').css('height', delta.height);
            } else {
                widget.css({
                    top: delta.top,
                    left: delta.left,
                    width: delta.width,
                    height: delta.height
                });
            }
        },
        /**
         * Add widget
         * @param {String} sectionUid
         * @param {String} html
         */
        add: function (sectionUid, html) {
            $('.editing-widget').removeClass('editing-widget');
            $('[data-uid="' + sectionUid + '"] .container').append(html);
        },
        /**
         * Add section
         * @param {Integer} index
         * @param {String} html
         */
        addSection: function (index, html) {
            $('.editing-widget').removeClass('editing-widget');

            if (index === -1) {
                $('.widget-section:last-of-type').after(html);
            } else {
                $('.widget-section:nth-child(' + (index + 1) + ')').before(html);
            }

            this.updateSectionsMap(true);
        },
        /**
         * Remove widget
         * @param  {String} uid
         */
        remove: function (uid) {
            $('[data-uid="' + uid + '"]').remove();
        },
        /**
         * Replace widget
         * @param  {String} uid
         * @param  {String} html
         */
        replaceWidget: function (uid, html) {
            $('[data-uid="' + uid + '"]').replaceWith(html);
        },
        /**
         * Block widget
         * @param  {String} uid
         */
        block: function (uid) {
            var widget = $('[data-uid="' + uid + '"]');

            if (widget.hasClass('ui-draggable')) {
                widget.attr('data-disabled-draggable', true);
                widget.draggable('disable');
            }

            widget.removeAttr('data-ui-selectable');
        },
        /**
         * Release widget
         * @param  {String} uid
         */
        release: function (uid) {
            var widget = $('[data-uid="' + uid + '"]');

            if (widget.attr('data-disabled-draggable')) {
                widget.removeAttr('data-disabled-draggable');
                widget.draggable('enable');
            }

            widget.attr('data-ui-selectable', true);
        },
        /**
         * Move widget up
         * @param  {String} uid
         */
        moveUp: function (uid) {
            $('[data-uid="' + uid + '"]').css('z-index', parseInt($('[data-uid="' + uid + '"]').css('z-index'), 10) + 1);
        },
        /**
         * Move widget down
         * @param  {String} uid
         */
        moveDown: function (uid) {
            $('[data-uid="' + uid + '"]').css('z-index', parseInt($('[data-uid="' + uid + '"]').css('z-index'), 10) - 1);
        },
        /**
         * Get widgets section uid
         * @param  {String} widgetUid
         * @return {String}
         */
        getSectionUID: function (widgetUid) {
            return $('[data-uid="' + widgetUid + '"]').parents('.widget-section').data('uid');
        },
        /**
         * Move widget to top
         * @param  {String} uid
         */
        moveToTop: function (uid) {
            var $widget = $('[data-uid="' + uid + '"]'),
                zindex = parseInt($widget.css('z-index'), 10);

            $widget.siblings().each(function (index, element) {
                var z = $(element).css('z-index');

                if (zindex < z) {
                    zindex = parseInt(z, 10);
                }
            });

            $widget.css('z-index', ++zindex);

            return zindex;
        },
        /**
         * Move widget to bottom
         * @param  {String} uid
         */
        moveToBottom: function (uid) {
            var $widget = $('[data-uid="' + uid + '"]'),
                zindex = parseInt($widget.css('z-index'), 10);

            $widget.siblings().each(function (index, element) {
                var z = $(element).css('z-index');

                if (zindex > z) {
                    zindex = parseInt(z, 10);
                }
            });

            $widget.css('z-index', --zindex);

            return zindex;
        },
        /**
         * Notify parent about scroll event
         */
        notifyScroll: function () {
            _parent.notify('scroll', {
                offset: $(window).scrollTop()
            });
        },
        scroll: function (pos) {
            window.scrollTo(0, pos);
        },
        moveScroll: function (delta) {
            this.scroll($(window).scrollTop() + delta);
        },
        /**
         * @param  {Boolean} apply [description]
         */
        updateSectionsMap: function (apply) {
            apply = apply === undefined ? false : apply;

            _sectionsMap = [];

            $('.widget-section').each(function (i, el) {
                var section = $(el);

                _sectionsMap.push({
                    uid: section.attr('data-uid'),
                    index: i,
                    top: section.offset().top,
                    height: section.height(),
                    width: section.width(),
                    containerOffset: section.find('.container').offset()
                });
            });

            _parent.notify('updateSectionsMap', {
                map: _sectionsMap,
                apply: apply
            });
        },
        renderStyle: function (styles) {
            _.each(styles, function (style, uid) {
                var styleTag = $('#style-' + uid);

                if (styleTag.length === 0) {
                    $('head').append('<style id="style-' + uid + '"></style>');
                }

                $('#style-' + uid).html(style);
            });
        },

        onClick: function (e) {
            var $this = $(this);

            if (($this.hasClass('editing-widget') && _mode === 'TEXT_EDIT') || _selecting === true) {
                _parent.notify('colorpicker:hide');

                return false;
            }

            $('.editing-widget').removeClass('editing-widget');

            if (_mode !== 'TEXT_EDIT') {
                var $parentElem = $this.parent().closest('.widget-form, .widget-counter');
                if ($parentElem.length > 0) {
                    _activeUid = $parentElem.attr('data-uid');
                    _activeElem = $parentElem;

                    $this.blur();
                } else {
                    _activeUid = this.getAttribute('data-uid');
                    _activeElem = $this;
                }

                _activeElem.addClass('editing-widget');

                _parent.notify('click', {
                    uid: _activeUid,
                    offset: _activeElem.offset(),
                    size: {
                        width: _activeElem.outerWidth(),
                        height: _activeElem.outerHeight()
                    },
                    selecting: e.ctrlKey || e.metaKey
                });

                return false;
            } else if (_mode === 'TEXT_EDIT' && $this.parents('[data-type="text"]').attr('contenteditable') === undefined && $this.attr('contenteditable') === undefined) {
                textEditor.close();

                $this.addClass('editing-widget');

                _parent.notify('click', {
                    uid: this.getAttribute('data-uid'),
                    offset: $this.offset(),
                    size: {
                        width: $this.outerWidth(),
                        height: $this.outerHeight()
                    },
                    selecting: e.ctrlKey
                });

                return false;
            } else if (_mode === 'TEXT_EDIT' && $this.attr('contenteditable') !== undefined) {
                return false;
            } else {
                return true;
            }

            return false;
        },
        onHover: function () {
            var $this = $(this);

            if (_selecting || _dragging || _mode === 'TEXT_EDIT' || $this.attr('data-ui-selectable') === undefined) {
                return false;
            }

            _parent.notify('hover', {
                uid: this.getAttribute('data-uid'),
                offset: $this.offset(),
                size: {
                    width: $this.outerWidth(),
                    height: $this.outerHeight()
                }
            });

            return false;
        },
        onDblClick: function () {
            var $this = $(this),
                $parentElem = $this.parent().closest('.widget-form, .widget-counter');

            if ($parentElem.length > 0) {
                _activeUid = $parentElem.attr('data-uid');
                _activeElem = $parentElem;

                $this.blur();
            } else {
                _activeUid = this.getAttribute('data-uid');
                _activeElem = $this;
            }

            if (false === _preview && !$this.hasClass('widget-blocked') && _mode === 'NORMAL') {

                if (_activeElem.attr('data-type') === 'text' || _activeElem.hasClass('widget-input-button')) {
                    _activeElem.attr('contenteditable', true).focus();

                    _parent.notify('setTextEditor', {
                        uid: _activeUid,
                        position: _activeElem.offset(),
                        size: {
                            height: _activeElem.outerHeight(),
                            width: _activeElem.outerWidth()
                        }
                    });

                    workspace.disableDrag();

                    _mode = 'TEXT_EDIT';
                } else {
                    _parent.notify('edit', {
                        uid: _activeUid
                    });

                    return false;
                }
            }

            return false;
        },
        contextMenu: function (e) {
            var $this = $(this),
                $parentElem = $this.parent().closest('.widget-form, .widget-counter');

            if ($parentElem.length > 0) {
                _activeUid = $parentElem.attr('data-uid');
                _activeElem = $parentElem;

                $this.blur();
            } else {
                _activeUid = this.getAttribute('data-uid');
                _activeElem = $this;
            }

            _parent.notify('click', {
                uid: _activeUid,
                offset: $this.offset(),
                size: {
                    width: _activeElem.outerWidth(),
                    height: _activeElem.outerHeight()
                }
            });

            _parent.notify('contextmenu', {
                uid: _activeUid,
                position: {
                    top: e.pageY,
                    left: e.pageX
                }
            });

            return false;
        },
        /**
         * @return {Array}
         */
        getSectionsMap: function () {
            return _sectionsMap;
        },
        getDraggableUIParams: function (uid) {
            var $widget = $('[data-uid="' + uid + '"]');

            if ($widget.length === 0) {
                return false;
            }

            return {
                offset: $widget.offset(),
                size: {
                    width: $widget.outerWidth(),
                    height: $widget.outerHeight()
                }
            };
        },
        setMode: function (mode) {
            _mode = mode;
        },
        renderSocials: function (socialJs) {
            // facebook
            if ($('#facebook-jssdk').length === 0) {
                $('body').append(socialJs.facebook);
            }

            // twitter
            try {
                twttr.widgets.load();
            } catch (e) {
                if (socialJs.twitter) {
                    $('body').append(socialJs.twitter);
                }
            }

            // googleplus
            try {
                gapi.plusone.go();
            } catch (e) {
                if (socialJs.gplus) {
                    $('body').append(socialJs.gplus);
                }
            }

            // vkontakte
            if (!window.VK) {

                VK_async = new (function () {
                    functions = [];
                    api_script_path = '//vk.com/js/api/openapi.js?105';

                    this.ready = function (f) {
                        if (typeof (VK) === 'undefined') {
                            functions.push(f);
                        } else {
                            f();
                        }
                    };

                    this.setAPIScriptPath = function (path) {
                        api_script_path = path;
                    }

                    this.init = function () {
                        jQuery(function () {
                            $.ajax({
                                url: api_script_path,
                                dataType: "script",
                                success: function () {
                                    for (var i = 0; i < functions.length; i++) {
                                        functions[i].call(window);
                                    }
                                }
                            });
                        });
                    };

                    var init = this.init;
                    jQuery(function () {
                        init();
                    });
                })();

                VK_async.ready(function () {
                    eval(socialJs.vkontakteWidgets);
                });
            }
        },
        updateSocials: function (vkontakte) {
            try {
                FB.XFBML.parse();
            } catch (e) { }

            try {
                twttr.widgets.load();
            } catch (e) { }

            try {
                gapi.plusone.go();
            } catch (e) { }

            if (vkontakte) {
                try {
                    eval(vkontakte);
                } catch (e) { }
            }
        },
        addScript: function (script) {
            $('head').append(script);
        },
        forceSave: function () {
            if (_mode === 'TEXT_EDIT') {
                this.textEditor.close();
            }
        },
        setDragging: function (enabled) {
            _dragging = enabled;
        },
        hasAnyConversionPageWidgets: function () {
            return $('.widget-buy, .widget-form, .widget-contact').length > 0;
        }
    };
};

var workspace = new Workspace();

(function () {
    workspace.bindKeyShortCuts();
    workspace.captureKey.init();
    workspace.selection.init();

    $(window).scroll(workspace.notifyScroll);

    $(window).resize(function () {
        workspace.updateSectionsMap();
    });

    $('#landingi-fold-line span').click(function () {
        $('#landingi-fold-line').toggleClass('out');
    });
})();
