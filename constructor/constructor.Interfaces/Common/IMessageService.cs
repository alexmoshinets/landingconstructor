﻿using System.Net.Mail;

namespace Interfaces.Common
{
    public interface IMessageService
    {
        bool Send(string to, string subject, string body, bool isBodyHtml = true);

        bool Send(MailAddress from, string to, string subject, string body, bool isBodyHtml = true);
    }
}