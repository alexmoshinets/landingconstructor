﻿using System.Drawing;
using System.Web;

namespace Interfaces.Common
{
    public interface IImageResizeService
    {
        Image ResizePhoto(HttpPostedFileBase inStream, int maxWidth, int quality);
    }
}