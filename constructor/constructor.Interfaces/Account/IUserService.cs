﻿using System;
using System.Collections.Generic;
using DomainModel.Account;

namespace Interfaces.Account
{
    public interface IUserService
    {
        IList<User> GetAll();

        IList<User> GetAllByRoleName(string roleName);

        IList<User> GetAllByRoleNameAndUserNameMatch(string roleName, string userNameToMatch);

        User GetById(int id);

        User GetByName(string userName);

        User GetByEmail(string email);

        User GetByPasswordRecoveryToken(Guid token);

        User Create(string userName, string password, string email, string passwordQuestion, string passwordAnswer,
            bool isApproved, DateTime createDate);

        User CreateUser(User data);
        User Update(int id, Action<User> update);

        User Update(string userName, Action<User> update);

        bool Delete(int id);

        bool Delete(string userName);
        User UpdateNotify(string username, bool value);
    }
}