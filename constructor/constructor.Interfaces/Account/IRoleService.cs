﻿using System.Collections.Generic;
using DomainModel.Account;

namespace Interfaces.Account
{
    public interface IRoleService
    {
        IList<Role> GetAll();

        IList<Role> GetAllByUserName(string userName);

        Role GetByName(string roleName);

        bool IsRoleExist(string roleName);

        bool IsUserInRole(string userName, string roleName);

        Role Create(string roleName);

        bool Delete(string roleName);

        void AddUsersToRoles(string[] userNames, string[] roleNames);

        void RemoveUsersFromRoles(string[] userNames, string[] roleNames);
    }
}