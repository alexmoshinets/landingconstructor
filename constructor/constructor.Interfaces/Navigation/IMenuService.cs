﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using DomainModel.Navigation;

namespace Interfaces.Navigation
{
    public interface IMenuService
    {
        IList<Menu> GetAll();

        IList<TResult> GetAll<TResult>(Expression<Func<Menu, TResult>> selector);
    }
}