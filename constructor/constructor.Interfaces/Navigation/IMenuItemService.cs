﻿using System;
using System.Collections.Generic;
using DomainModel.Navigation;

namespace Interfaces.Navigation
{
    public interface IMenuItemService
    {
        IList<MenuItem> GetByMenuKey(string stringKey, bool onlyActive = false);

        IList<MenuItem> GetAll(bool onlyActive = false);

        MenuItem GetById(int id);

        MenuItem Update(int id, Action<MenuItem> update);

        MenuItem Add(MenuItem entity);

        bool Delete(int id);
    }
}