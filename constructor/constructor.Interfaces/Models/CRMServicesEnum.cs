﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces.Models
{
    public enum CRMServicesEnum
    {
        Amocrm,
        Bitrix24,
        MailChimp
    }
}
