﻿using DomainModel.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces.Content
{
    public interface ILandingSettingsService
    {
        List<LandingSetting> GetAll();
        LandingSetting GetByLandingId(int id);
        LandingSetting GetById(int id);
        LandingSetting Add(LandingSetting entity);
        LandingSetting Update(LandingSetting entity);
        bool Delete(int id);
    }
}
