﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using DomainModel.Content;

namespace Interfaces.Content
{
    public interface ISectionsCategoryService
    {
        SectionsCategory GetByKey(string stringKey);


        IList<SectionsCategory> GetAll();


        SectionsCategory Update(int id, Action<SectionsCategory> update);

        SectionsCategory Update(string stringKey, Action<SectionsCategory> update);

        string AddOrUpdate(string stringKey, SectionsCategory entity, Action<SectionsCategory> update);

        SectionsCategory Add(SectionsCategory entity);

        bool Delete(string stringKey);
    }
}