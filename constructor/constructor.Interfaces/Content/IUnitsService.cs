﻿using System;
using System.Collections.Generic;
using DomainModel.Content;
using System.Linq.Expressions;

namespace Interfaces.Content
{
    public interface IUnitsService
    {
        IList<Unit> GetAll();
        IList<TResult> GetAllUnits<TResult>(Expression<Func<Unit, TResult>> selector);
    }
}
