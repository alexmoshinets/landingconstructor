﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using DomainModel.Content;

namespace Interfaces.Content
{
    public interface ICategoryService
    {
        Category GetByKey(string stringKey);

        Category GetById(int id);

        IList<Category> GetAll(bool onlyActive = false);

        IList<TResult> GetAll<TResult>(Expression<Func<Category, TResult>> selector);

        Category Update(int id, Action<Category> update);

        Category Add(Category entity);

        string Delete(int id);
    }
}