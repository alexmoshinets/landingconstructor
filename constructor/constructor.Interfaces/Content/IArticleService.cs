﻿using System;
using System.Collections.Generic;
using DomainModel.Content;

namespace Interfaces.Content
{
    public interface IArticleService
    {
        Article GetByKey(string stringKey);

        Article GetById(int id);

        IList<Article> GetAll(bool onlyActive = true);

        IList<Article> GetLast(int Count);

        IList<Article> GetAllByCategoryKey(string stringKey, bool onlyActive = true);

        IList<Article> GetLast(int count, bool onlyActive = true);

        Article Update(int id, Action<Article> update);

        Article Add(Article entity);

        bool Delete(int id);
    }
}