﻿using System;
using System.Collections.Generic;
using DomainModel.Content;

namespace Interfaces.Content
{
    public interface IServicesService
    {
        Servise GetByKey(string stringKey);

        Servise GetById(int id);
        IList<Servise> GetLatestServices(int Count);

        IList<Servise> GetAll(bool onlyActive = true);

        IList<Servise> GetLast(int Count);

        IList<Servise> GetAllByCategoryKey(string stringKey, bool onlyActive = true);

        IList<Servise> GetLast(int count, bool onlyActive = true);

        Servise Update(int id, Action<Servise> update);

        Servise Add(Servise entity);

        bool Delete(int id);
    }
}