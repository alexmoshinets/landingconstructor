﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using DomainModel.Content;

namespace Interfaces.Content
{
    public interface IProductService
    {
        Product GetById(int id);
        IList<Product> GetAll(bool onlyActive = false);
        IList<Product> GetLatestProducts(int Count);
        Product Update(int id, Action<Product> update);
        Product Add(Product entity);
        string Delete(int id);
    }
}