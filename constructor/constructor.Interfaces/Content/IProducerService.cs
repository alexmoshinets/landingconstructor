﻿using System;
using System.Collections.Generic;
using DomainModel.Content;
using System.Linq.Expressions;

namespace Interfaces.Content
{
    public interface IProducerService
    {
        Producer GetById(int id);
        Producer GetByUrl(string url);
        bool ExistUrl(string url);
        IList<Producer> GetAll(bool onlyActive = false);
        IList<Producer> GetByList(IList<Product> list);
        IList<TResult> GetAllProducer<TResult>(Expression<Func<Producer, TResult>> selector);
        Producer Update(int id, Action<Producer> update);
        Producer Add(Producer entity);
        bool Delete(int id);
        bool DeleteProductsAll(int IDProducer);
    }
}