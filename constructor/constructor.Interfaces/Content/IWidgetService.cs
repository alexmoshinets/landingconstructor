﻿using DomainModel.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces.Content
{
    public interface IWidgetService
    {
        Widget GetById(int id);        

        IList<Widget> GetAll();
        IList<Widget> GetWidgetsUser(int userId);

        Widget Update(Widget page);

        Widget Add(Widget entity);

        bool Delete(int id);
    }
}
