﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using DomainModel.Content;

namespace Interfaces.Content
{
    public interface IPhotoAlbumService
    {
        PhotoAlbum GetByKey(string stringKey);

        PhotoAlbum GetById(int id);

        IList<string> GetPagingById(int id, int count);

        IList<PhotoAlbum> GetAll(bool onlyActive = true);

        IList<TResult> GetAll<TResult>(Expression<Func<PhotoAlbum, TResult>> selector, bool onlyActive = true);

        PhotoAlbum Update(int id, Action<PhotoAlbum> update);

        PhotoAlbum Update(string stringKey, Action<PhotoAlbum> update);

        PhotoAlbum Add(PhotoAlbum entity);

        bool Delete(int id);
    }
}