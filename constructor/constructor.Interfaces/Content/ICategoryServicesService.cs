﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using DomainModel.Content;

namespace Interfaces.Content
{
    public interface ICategoryServicesService
    {
        CategoryService GetByKey(string stringKey);

        CategoryService GetById(int id);

        IList<CategoryService> GetAll();

        IList<TResult> GetAll<TResult>(Expression<Func<CategoryService, TResult>> selector);

        CategoryService Update(int id, Action<CategoryService> update);

        CategoryService Add(CategoryService entity);

        bool Delete(int id);
    }
}