﻿using System;
using System.Collections.Generic;
using DomainModel.Content;

namespace Interfaces.Content
{
    public interface IBlockService
    {
        IList<Block> GetByBlockListKey(string stringKey, bool onlyActive = false);

        IList<Block> GetAll(bool onlyActive = false);

        Block GetByKey(string stringKey);

        Block GetById(int id);

        Block Update(int id, Action<Block> update);

        Block Add(Block entity);

        bool Delete(int id);
    }
}