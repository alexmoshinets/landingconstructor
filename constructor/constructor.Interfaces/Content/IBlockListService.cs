﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using DomainModel.Content;

namespace Interfaces.Content
{
    public interface IBlockListService
    {
        IList<BlockList> GetAll();

        IList<TResult> GetAll<TResult>(Expression<Func<BlockList, TResult>> selector);
    }
}