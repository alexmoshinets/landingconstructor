﻿using System;
using System.Collections.Generic;
using DomainModel.Content;

namespace Interfaces.Content
{
    public interface IPartnersService
    {
        Partner GetById(int id);
        IList<Partner> GetAll(bool onlyActive = false);
        Partner Update(int id, Action<Partner> update);
        Partner Add(Partner entity);
        bool Delete(int id);
    }
}
