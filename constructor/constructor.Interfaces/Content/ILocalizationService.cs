﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using DomainModel.Content;

namespace Interfaces.Content
{
    public interface ILocalizationService
    {
        Localization GetByKey(string stringKey);

        Localization GetById(int id);

        IList<Localization> GetAll();

        Localization Update(int id, Action<Localization> update);

        Localization Update(string stringKey, Action<Localization> update);

        Localization Add(Localization entity);

        bool Delete(int id);
    }
}