﻿using System;
using System.Collections.Generic;
using DomainModel.Content;
using System.Linq.Expressions;

namespace Interfaces.Content
{
    public interface IPresenceService
    {
        IList<Presence> GetAll();
        IList<TResult> GetAllItems<TResult>(Expression<Func<Presence, TResult>> selector);
    }
}

