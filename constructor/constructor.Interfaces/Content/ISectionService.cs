﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using DomainModel.Content;

namespace Interfaces.Content
{
    public interface ISectionService
    {
        CategoryServices GetById(int id);
        IList<CategoryServices> GetAll(bool onlyActive = false);
        CategoryServices GetByUrl(string Url);
        IList<TResult> GetAllSection<TResult>(Expression<Func<CategoryServices, TResult>> selector);

        CategoryServices Update(int id, Action<CategoryServices> update);
        CategoryServices Add(CategoryServices entity);
        bool Delete(int id);
        bool DeleteProductsAll(int IDSection);
    }
}