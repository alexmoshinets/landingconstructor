﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using DomainModel.Content;

namespace Interfaces.Content
{
    public interface IPageService
    {
        Page GetByKey(string stringKey);

        Page GetById(int id);

        IList<Page> GetAll();

        IList<TResult> GetAll<TResult>(Expression<Func<Page, TResult>> selector);

        Page Update(int id, Action<Page> update);

        Page Update(string stringKey, Action<Page> update);

        Page Add(Page entity);

        bool Delete(int id);
    }
}