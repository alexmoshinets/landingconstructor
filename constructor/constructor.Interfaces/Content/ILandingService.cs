﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using DomainModel.Content;

namespace Interfaces.Content
{
    public interface ILandingService
    {
        Landing GetByKey(string stringKey);
        Landing GetByUrl(string url);

        Landing GetById(int id);
        List<Landing> GetByUser(int userId);

        IList<Landing> GetAll(bool onlyActive = false);

        List<Landing> GetByUrlAndUserId(string url, int userId);

        Landing Update(int id, Action<Landing> update);

        Landing Update(string stringKey, Action<Landing> update);

        string AddOrUpdate(string stringKey, Landing entity, Action<Landing> update);

        Landing Add(Landing entity);

        bool Delete(int id);

        bool DeleteLanding(string url);
        Landing SetEmail(string email, int id);
    }
}