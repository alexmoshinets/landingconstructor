﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using DomainModel.Content;

namespace Interfaces.Content
{
    public interface ISectionsService
    {
        Sections GetByKey(string stringKey);

        bool NameIsExist(string name);

        IList<Sections> GetAll(bool onlyActive = false);


        Sections Update(int id, Action<Sections> update);

        Sections Update(string stringKey, Action<Sections> update);

        string AddOrUpdate(string stringKey, Sections entity, Action<Sections> update);

        Sections Add(Sections entity);

        bool Delete(string stringKey);
    }
}