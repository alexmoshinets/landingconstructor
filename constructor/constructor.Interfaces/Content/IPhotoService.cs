﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using DomainModel.Content;

namespace Interfaces.Content
{
    public interface IPhotoService
    {
        Photo GetById(int id);

        IList<Photo> GetAll();

        IList<TResult> GetAll<TResult>(Expression<Func<Photo, TResult>> selector);

        Photo Add(Photo entity);

        bool Delete(int id);
    }
}