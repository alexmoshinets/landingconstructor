﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using DomainModel.Content;

namespace Interfaces.Content
{
    public interface ISaleTechnikService
    {
        SaleTechniks GetById(int id);
        IList<SaleTechniks> GetLast(int Count);
        IList<SaleTechniks> GetAll(bool onlyActive = false);
        SaleTechniks Update(int id, Action<SaleTechniks> update);
        SaleTechniks Add(SaleTechniks entity);
        string Delete(int id);
    }
}