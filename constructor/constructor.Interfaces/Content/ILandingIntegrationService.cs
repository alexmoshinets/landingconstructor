﻿using DomainModel.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces.Content
{
    public interface ILandingIntegrationService
    {
        List<LandingIntegration> GetAll();
        LandingIntegration GetByLandingId(string id);
        LandingIntegration GetByFormId(string id);
        LandingIntegration Add(LandingIntegration entity);
        LandingIntegration Update(LandingIntegration entity);
        bool Delete(int id);
    }
}
