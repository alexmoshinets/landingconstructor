namespace Services.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addwidget : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Widgets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        CreateDate = c.DateTime(nullable: false),
                        Script = c.String(),
                        Description = c.String(),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Account.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Widgets", "UserId", "Account.Users");
            DropIndex("dbo.Widgets", new[] { "UserId" });
            DropTable("dbo.Widgets");
        }
    }
}
