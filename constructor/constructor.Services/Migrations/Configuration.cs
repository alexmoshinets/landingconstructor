﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.Linq;
using DomainModel.Account;
using DomainModel.Content;
using DomainModel.Navigation;

namespace Services.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<DataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(DataContext context)
        {
            //InitalizeDataBase(context);
        }

        private static void InitalizeDataBase(DataContext context)
        {
            #region Settings

            context.Settings.AddOrUpdate(_ => _.StringKey,
                new Setting
                {
                    StringKey = "LogoTimestamp",
                    Value = DateTime.Now.Ticks.ToString(CultureInfo.InvariantCulture)
                },
                new Setting
                {
                    StringKey = "PhotoTimestamp",
                    Value = DateTime.Now.Ticks.ToString(CultureInfo.InvariantCulture)
                },
                new Setting
                {
                    StringKey = "SideAdsTimestamp",
                    Value = DateTime.Now.Ticks.ToString(CultureInfo.InvariantCulture)
                },
                new Setting
                {
                    StringKey = "companyName",
                    Value = "[{\"k\":\"en\",\"v\":\"C a n E x I m  T r a d i n g  L T D\"},{\"k\":\"ru\",\"v\":\"C a n E x I m  T r a d i n g  L T D\"}]"
                },
                new Setting
                {
                    StringKey = "Map",
                    Value = "<script type=\"text/javascript\" charset=\"utf-8\" src=\"https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=wGjNjyT1X9V_0KyWC52XEDfWdhjJunQK&width=100%&height=400&lang=ru_RU&sourceType=constructor\"></script>"
                },
                new Setting
                {
                    StringKey = "copyright",
                    Value = "[{\"k\":\"en\",\"v\":\"Copyright © 2016 CanExIm Trading LTD.\"},{\"k\":\"ru\",\"v\":\"Copyright © 2016 CanExIm Trading LTD.\"}]"
                },
                new Setting
                {
                    StringKey = "Email",
                    Value = "caneximtrading@gmail.com"
                },
                new Setting
                {
                    StringKey = "WebSite",
                    Value = "http://www.caneximtrading.com"
                },
                new Setting
                {
                    StringKey = "WorkTel",
                    Value = "+1 (604) 608 4711"
                },
                new Setting
                {
                    StringKey = "Tel",
                    Value = "+1 (604) 727 0017"
                },
                new Setting
                {
                    StringKey = "Address",
                    Value = "[{\"k\":\"en\",\"v\":\"7B-6128 Patterson Ave., Burnaby, B.C. V5H4P3 Canada\"},{\"k\":\"ru\",\"v\":\"Канада, Барнаби, проспект Петтерсона 7B-6128\"}]"
                }
                );
            #endregion
            #region Users

            var roleEditor = new User
            {
                UserName = "editor",
                Email = "editor@landing.com",
                Password = "123456",
                CreateDate = DateTime.UtcNow,
                IsApproved = true
            };
            context.Users.AddOrUpdate(_ => _.Email, roleEditor);

            var admin = new User
            {
                UserName = "Admin",
                Email = "admin@admin.com",
                Password = "123456",
                CreateDate = DateTime.UtcNow,
                IsApproved = true
            };
            context.Users.AddOrUpdate(_ => _.Email, admin);

            var sadmin = new User
            {
                UserName = "sadmin",
                Email = "vasyunin.alexander@gmail.com",
                Password = "6823590",
                CreateDate = DateTime.UtcNow,
                IsApproved = true
            };
            context.Users.AddOrUpdate(_ => _.Email, sadmin);

            #region Role

            context.Roles.AddOrUpdate(_ => _.Name, new Role
            {
                Name = "sadmin",
                Description = "Супер админ",
                Users = new List<User> { sadmin }
            });
            context.Roles.AddOrUpdate(_ => _.Name, new Role
            {
                Name = "admin",
                Description = "Администратор",
                Users = new List<User> { sadmin, admin }
            });

            context.Roles.AddOrUpdate(_ => _.Name, new Role
            {
                Name = "editor",
                Description = "Редактор",
                Users = new List<User> { sadmin, admin, roleEditor }
            });

            #endregion

            #endregion

            #region Page

            // разделы 
            var home = new Page
            {
                StringKey = "Home",
                Name = "[{\"k\":\"en\",\"v\":\"Home\"},{\"k\":\"ru\",\"v\":\"Главная\"}]",
                Content = "[{\"k\":\"en\",\"v\":\"&nbsp;\"},{\"k\":\"ru\",\"v\":\"&nbsp;\"}]",
                BottomContent = "[{\"k\":\"en\",\"v\":\"&nbsp;\"},{\"k\":\"ru\",\"v\":\"&nbsp;\"}]",
                MetaTitle = "[{\"k\":\"en\",\"v\":\"&nbsp;\"},{\"k\":\"ru\",\"v\":\"&nbsp;\"}]",
                MetaKeywords = "[{\"k\":\"en\",\"v\":\"&nbsp;\"},{\"k\":\"ru\",\"v\":\"&nbsp;\"}]",
                MetaDescription = "[{\"k\":\"en\",\"v\":\"&nbsp;\"},{\"k\":\"ru\",\"v\":\"&nbsp;\"}]",
                IsSection = true,
                Created = DateTime.UtcNow
            };
            var products = new Page
            {
                StringKey = "Products",
                Name = "[{\"k\":\"en\",\"v\":\"Products\"},{\"k\":\"ru\",\"v\":\"Продукция\"}]",
                Content = "[{\"k\":\"en\",\"v\":\"&nbsp;\"},{\"k\":\"ru\",\"v\":\"&nbsp;\"}]",
                BottomContent = "[{\"k\":\"en\",\"v\":\"&nbsp;\"},{\"k\":\"ru\",\"v\":\"&nbsp;\"}]",
                MetaTitle = "[{\"k\":\"en\",\"v\":\"&nbsp;\"},{\"k\":\"ru\",\"v\":\"&nbsp;\"}]",
                MetaKeywords = "[{\"k\":\"en\",\"v\":\"&nbsp;\"},{\"k\":\"ru\",\"v\":\"&nbsp;\"}]",
                MetaDescription = "[{\"k\":\"en\",\"v\":\"&nbsp;\"},{\"k\":\"ru\",\"v\":\"&nbsp;\"}]",
                IsSection = true,
                Created = DateTime.UtcNow
            };
            var price = new Page
            {
                StringKey = "Price",
                Name = "[{\"k\":\"en\",\"v\":\"Price\"},{\"k\":\"ru\",\"v\":\"Прайс\"}]",
                Content = "[{\"k\":\"en\",\"v\":\"&nbsp;\"},{\"k\":\"ru\",\"v\":\"&nbsp;\"}]",
                BottomContent = "[{\"k\":\"en\",\"v\":\"&nbsp;\"},{\"k\":\"ru\",\"v\":\"&nbsp;\"}]",
                MetaTitle = "[{\"k\":\"en\",\"v\":\"&nbsp;\"},{\"k\":\"ru\",\"v\":\"&nbsp;\"}]",
                MetaKeywords = "[{\"k\":\"en\",\"v\":\"&nbsp;\"},{\"k\":\"ru\",\"v\":\"&nbsp;\"}]",
                MetaDescription = "[{\"k\":\"en\",\"v\":\"&nbsp;\"},{\"k\":\"ru\",\"v\":\"&nbsp;\"}]",
                IsSection = true,
                Created = DateTime.UtcNow
            };
            var contacts = new Page
            {
                StringKey = "Contacts",
                Name = "[{\"k\":\"en\",\"v\":\"Contacts\"},{\"k\":\"ru\",\"v\":\"Контакты\"}]",
                Content = "[{\"k\":\"en\",\"v\":\"&nbsp;\"},{\"k\":\"ru\",\"v\":\"&nbsp;\"}]",
                BottomContent = "[{\"k\":\"en\",\"v\":\"&nbsp;\"},{\"k\":\"ru\",\"v\":\"&nbsp;\"}]",
                MetaTitle = "[{\"k\":\"en\",\"v\":\"&nbsp;\"},{\"k\":\"ru\",\"v\":\"&nbsp;\"}]",
                MetaKeywords = "[{\"k\":\"en\",\"v\":\"&nbsp;\"},{\"k\":\"ru\",\"v\":\"&nbsp;\"}]",
                MetaDescription = "[{\"k\":\"en\",\"v\":\"&nbsp;\"},{\"k\":\"ru\",\"v\":\"&nbsp;\"}]",
                IsSection = true,
                Created = DateTime.UtcNow
            };
            var about = new Page
            {
                StringKey = "About",
                Name = "[{\"k\":\"en\",\"v\":\"About\"},{\"k\":\"ru\",\"v\":\"О нас\"}]",
                Content = "[{\"k\":\"en\",\"v\":\"&nbsp;\"},{\"k\":\"ru\",\"v\":\"&nbsp;\"}]",
                BottomContent = "[{\"k\":\"en\",\"v\":\"&nbsp;\"},{\"k\":\"ru\",\"v\":\"&nbsp;\"}]",
                MetaTitle = "[{\"k\":\"en\",\"v\":\"&nbsp;\"},{\"k\":\"ru\",\"v\":\"&nbsp;\"}]",
                MetaKeywords = "[{\"k\":\"en\",\"v\":\"&nbsp;\"},{\"k\":\"ru\",\"v\":\"&nbsp;\"}]",
                MetaDescription = "[{\"k\":\"en\",\"v\":\"&nbsp;\"},{\"k\":\"ru\",\"v\":\"&nbsp;\"}]",
                IsSection = true,
                Created = DateTime.UtcNow
            };
            
            context.Pages.AddOrUpdate(_ => _.StringKey, home, products, price, contacts, about);

            #endregion

            #region Menu

            var topMenu = new Menu
            {
                StringKey = "TopMenu",
                Name = "Главное меню"
            };
            var sideMenu = new Menu
            {
                StringKey = "SideMenu",
                Name = "Боковое меню"
            };

            context.Menus.AddOrUpdate(_ => _.StringKey, topMenu, sideMenu);
            context.SaveChanges();

            #region TopMenu

            var topMenuItems = new[]
            {
                new MenuItem
                {
                    Name = "[{\"k\":\"en\",\"v\":\"Home\"},{\"k\":\"ru\",\"v\":\"Главная\"}]",
                    Type = MenuItemType.Page,
                    MenuId = topMenu.Id,
                    PageId = home.Id,
                    Created = DateTime.UtcNow
                },
                new MenuItem
                {
                    Name = "[{\"k\":\"en\",\"v\":\"Products\"},{\"k\":\"ru\",\"v\":\"Продукция\"}]",
                    Type = MenuItemType.Page,
                    MenuId = topMenu.Id,
                    PageId = products.Id,
                    Created = DateTime.UtcNow
                },
                new MenuItem
                {
                    Name = "[{\"k\":\"en\",\"v\":\"Price\"},{\"k\":\"ru\",\"v\":\"Прайс\"}]",
                    Type = MenuItemType.Page,
                    MenuId = topMenu.Id,
                    PageId = price.Id,
                    Created = DateTime.UtcNow
                },
                new MenuItem
                {
                    Name = "[{\"k\":\"en\",\"v\":\"Contacts\"},{\"k\":\"ru\",\"v\":\"Контакты\"}]",
                    Type = MenuItemType.Page,
                    MenuId = topMenu.Id,
                    PageId = contacts.Id,
                    Created = DateTime.UtcNow
                },
                new MenuItem
                {
                    Name = "[{\"k\":\"en\",\"v\":\"About\"},{\"k\":\"ru\",\"v\":\"О нас\"}]",
                    Type = MenuItemType.Page,
                    MenuId = topMenu.Id,
                    PageId = about.Id,
                    Created = DateTime.UtcNow
                }
            };

            //context.MenuItems.AddOrUpdate(_ => new { _.Name, _.MenuId }, topMenuItems);
            // todo: вынужденная мера стандартный метод валится из-за null может поправят
            AddOrUpdate(context, topMenuItems);

            #endregion

            #region SideMenu

            #endregion

            #region FooterMenu
            
            #endregion

            #endregion

            #region Block

            var leftBlockList = new BlockList
            {
                Name = "Левая панель",
                StringKey = "LeftBlockList"
            };
            //var rightBlockList = new BlockList
            //{
            //    Name = "Правая панель",
            //    StringKey = "RightBlockList"
            //};
            //context.BlockLists.AddOrUpdate(_ => _.StringKey, leftBlockList, rightBlockList);
            context.BlockLists.AddOrUpdate(_ => _.StringKey, leftBlockList);
            context.SaveChanges();


            //Producer var rightBlockListItems = new[]
            //{
            //    new Block
            //    {
            //        StringKey = "block-1",
            //        Title = "Блок 1",
            //        Description = "Я доверяю Высшей силе и верю в свои силы.",
            //        Created = DateTime.UtcNow,
            //        BlockListId = rightBlockList.Id
            //    },
            //    new Block
            //    {
            //        StringKey = "block-2",
            //        Title = "Блок 2",
            //        Description =
            //            "Сегодня <b>19.07.2013</b>:<br />IV Пин. Равновесие<br /><br /><a href=\"#\">Календарь на неделю</a><br /><br /><a href=\"#\">Календарь на 2013 год</a>",
            //        Created = DateTime.UtcNow,
            //        BlockListId = rightBlockList.Id
            //    }
            //};
            //context.Blocks.AddOrUpdate(_ => _.StringKey, rightBlockListItems);

            //var leftBlockListItems = new[]
            //{
            //    new Block
            //    {
            //        StringKey = "o_nas",
            //        Title = "О нас",
            //        Description = "",
            //        Created = DateTime.UtcNow,
            //        BlockListId = leftBlockList.Id
            //    }
            //};
            //context.Blocks.AddOrUpdate(_ => _.StringKey, leftBlockListItems);


           

            #endregion

            #region Категории

            var kuryatina = new Category
            {
                IsHidden = false,
                StringKey = "Kuryatina",
                Title = "[{\"k\":\"en\",\"v\":\"Chicken\"},{\"k\":\"ru\",\"v\":\"Курятина\"}]",
                Description =
                    "[{\"k\":\"en\",\"v\":\"<p>Chicken - this is to buy oil, it is tasty, healthy and diet. Chicken meat is used in the preparation of cold and hot appetizers, first and second courses. Most often used for cooking meat meat chicken breeds.</p>\r\n\"},{\"k\":\"ru\",\"v\":\"<p>Курятина &ndash; это самое покупаемое мясо, оно вкусное, полезное и диетическое.&nbsp;Курятина используется в приготовлении холодных и горячих закусок, первых и вторых блюд. Чаще всего для приготовления используют мясо мясных пород кур.</p>\r\n\"}]",
                Image = "e4296e51-e87f-4a99-9c32-0a25d2b254eb.png",
                Created = DateTime.Now
            };
            var pork = new Category
            {
                IsHidden = false,
                StringKey = "Pork",
                Title = "[{\"k\":\"en\",\"v\":\"Pork\"},{\"k\":\"ru\",\"v\":\"Свинина\"}]",
                Description =
                    "[{\"k\":\"en\",\"v\":\"<p>Pork - hot, tasty and largely useful product, but not without contraindications, you should be aware.</p>\r\n\"},{\"k\":\"ru\",\"v\":\"<p>Свинина &ndash; популярный, вкусный и во многом полезный продукт, но не без противопоказаний, о которых следует знать.</p>\r\n\"}]",
                Image = "ffad5e78-d7ba-438e-bd95-29b8ea6c2fe0.png",
                Created = DateTime.Now
            };
            var beef = new Category
            {
                IsHidden = false,
                StringKey = "Beef",
                Title = "[{\"k\":\"en\",\"v\":\"Beef\"},{\"k\":\"ru\",\"v\":\"Говядина\"}]",
                Description =
                    "[{\"k\":\"en\",\"v\":\"<p>This popular food product will appreciate as true gourmets and supporters of useful power.</p>\r\n\"},{\"k\":\"ru\",\"v\":\"<p>Этот популярный продукт питания оценят как настоящие гурманы, так и сторонники полезного питания.</p>\r\n\"}]",
                Image = "2c36c5b2-a567-4157-b1ef-d6018c4f8a1b.png",
                Created = DateTime.Now
            };
            var fish = new Category
            {
                IsHidden = false,
                StringKey = "Fresh frozen fish",
                Title = "[{\"k\":\"en\",\"v\":\"Fresh frozen fish\"},{\"k\":\"ru\",\"v\":\"Свежезамороженная рыба\"}]",
                Description =
                    "[{\"k\":\"en\",\"v\":\"<p>We offer frozen fish in a very rich assortment. The method of storing and processing involves long periods of storage. This product covers a very wide consumer market, the production and further processing.</p>\r\n\"},{\"k\":\"ru\",\"v\":\"<p>Наша компания предлагает мороженную рыбу в очень богатом ассортименте. Способ её хранения и переработки предполагает длительные сроки хранения. Данный продукт охватывает очень широкий рынок потребления , производство и дальнейшую переработку.</p>\r\n\"}]",
                Image = "2802acbf-e213-4f49-bb0c-2d3d2485ad9d.png",
                Created = DateTime.Now
            };
            context.Category.AddOrUpdate(_ => _.StringKey, kuryatina, pork, beef, fish);
            context.SaveChanges();

            var tushka = new Category
            {
                IsHidden = false,
                StringKey = "Celaya-tushka",
                Title = "[{\"k\":\"en\",\"v\":\"Whole bird\"},{\"k\":\"ru\",\"v\":\"Целая тушка\"}]",
                Description =
                    "[{\"k\":\"en\",\"v\":\"<p>Dressed carcass consists of an intact carcass with all parts, including the breast, thighs, drumsticks, wings, back and abdominal fat. The head and feet are removed, oil gland and tail may be removed or left at the carcass. Stomach, heart, liver and neck with or without skin (giblet bag and neck with) are applied separately.</p>\r\n\"},{\"k\":\"ru\",\"v\":\"<p>Потрошеная тушка состоит из целой тушки со всеми частями, включая грудку, бедра, голени, крылья, спинку и брюшной жир. Голова и ноги удалены, гузка и копчиковая железа могут быть удалены или оставлены при тушке. Желудок, сердце, печень и шея с кожей или без кожи (пакет с потрохами и шеей) прикладываются отдельно.</p>\r\n\"}]",
                Image = "750a75ba-87c8-459e-aae0-9900143436cb.jpg",
                CategoryId = kuryatina.Id,
                Created = DateTime.Now
            };

            var nojki = new Category
            {
                IsHidden = false,
                StringKey = "Celaya-tushka",
                Title = "[{\"k\":\"en\",\"v\":\"Legs\"},{\"k\":\"ru\",\"v\":\"Ножки\"}]",
                Description =
                    "[{\"k\":\"en\",\"v\":\"<p>The whole leg is produced by separating a leg from the back of the carcass of the femur and pelvic bone.</p>\r\n\"},{\"k\":\"ru\",\"v\":\"<p>Окорочок получают путем отделения окорочка от задней части тушки по месту соединения бедренной и тазовой костей.</p>\r\n\"}]",
                Image = "e51d17b0-7d11-4c78-bde0-d0bb792cfb7a.jpg",
                CategoryId = kuryatina.Id,
                Created = DateTime.Now
            };
            var paddle = new Category
            {
                IsHidden = false,
                StringKey = "Celaya-tushka",
                Title = "[{\"k\":\"en\",\"v\":\"Paddle number\"},{\"k\":\"ru\",\"v\":\"Лопаточный ряд\"}]",
                Description =
                    "[{\"k\":\"en\",\"v\":\"\"},{\"k\":\"ru\",\"v\":\"Цельная лопаточная часть размещается в передней части туши. Свиная щековина обычно удаляется.\"}]",
                Image = "e713d4b3-e116-4ac8-ab63-a4b7b1314044.jpg",
                CategoryId = kuryatina.Id,
                Created = DateTime.Now
            };

            context.Category.AddOrUpdate(_ => _.StringKey, tushka, nojki, paddle);
            context.SaveChanges();
                
            #endregion

            #region Товары
            context.Products.AddOrUpdate(_ => _.StringKey,
                new Product
                {
                    IsHidden = false,
                    StringKey = "Celaya-tushka",
                    Title = "[{\"k\":\"en\",\"v\":\"Whole bird\"},{\"k\":\"ru\",\"v\":\"Целая тушка\"}]",
                    Description = "[{\"k\":\"en\",\"v\":\"<p>Dressed carcass consists of an intact carcass with all parts, including the breast, thighs, drumsticks, wings, back and abdominal fat. The head and feet are removed, oil gland and tail may be removed or left at the carcass. Stomach, heart, liver and neck with or without skin (giblet bag and neck with) are applied separately.</p>\r\n\"},{\"k\":\"ru\",\"v\":\"<p>Потрошеная тушка состоит из целой тушки со всеми частями, включая грудку, бедра, голени, крылья, спинку и брюшной жир. Голова и ноги удалены, гузка и копчиковая железа могут быть удалены или оставлены при тушке. Желудок, сердце, печень и шея с кожей или без кожи (пакет с потрохами и шеей) прикладываются отдельно.</p>\r\n\"}]",
                    Image = "68fd2bb7-4123-44d6-8cfb-854e56147185.jpg",
                    Price = 25,
                    CategoryId = tushka.Id,
                    Created = DateTime.UtcNow
                }, 
                new Product
                {
                    IsHidden = false,
                    StringKey = "Polutushki",
                    Title = "[{\"k\":\"en\",\"v\":\"Pork sides\"},{\"k\":\"ru\",\"v\":\"Полутушки\"}]",
                    Description = "[{\"k\":\"en\",\"v\":\"<p>Pork sides is produced by cutting a whole bird without giblets along the back and breast bone. The tail and abdominal fat may be removed</p>\r\n\r\n<p>&nbsp;</p>\r\n\"},{\"k\":\"ru\",\"v\":\"<p>Полутушки получают в результате разделки потрошенной тушки вдоль позвоночника и киля грудной кости. Гузка и брюшной жир могут быть удалены.</p>\r\n\"}]",
                    Image = "308ce5ad-b787-44fc-8732-ff245f13d206.jpg",
                    Price = 5,
                    CategoryId = tushka.Id,
                    Created = DateTime.UtcNow
                },
                new Product
                {
                    IsHidden = false,
                    StringKey = "Perednyaya-i-zadnyaya-chasti-tushki",
                    Title = "[{\"k\":\"en\",\"v\":\"The front and back of the carcass\"},{\"k\":\"ru\",\"v\":\"Передняя и задняя части тушки\"}]",
                    Description = "[{\"k\":\"en\",\"v\":\"<p>The resulting cross-cutting a whole bird without giblets on the front and rear portions</p>\r\n\r\n<p>&nbsp;</p>\r\n\"},{\"k\":\"ru\",\"v\":\"<p>Получают в результате поперечной разделки потрошеной тушки на переднюю и заднюю части.</p>\r\n\"}]",
                    Image = "e27111de-7dc2-408a-ba34-48f154a94db2.jpg",
                    Price = 12,
                    CategoryId = tushka.Id,
                    Created = DateTime.UtcNow
                },
                new Product
                {
                    IsHidden = false,
                    StringKey = "Chetvertiny",
                    Title = "[{\"k\":\"en\",\"v\":\"QUARTER\"},{\"k\":\"ru\",\"v\":\"Четвертины\"}]",
                    Description = "[{\"k\":\"en\",\"v\":\"<p>Quarters, obtained by transverse cutting a whole bird without giblets into two front and rear quarters.</p>\r\n\"},{\"k\":\"ru\",\"v\":\"<p>Четвертины получают в результате поперечной разделки потрошеной тушки на две передние и задние четвертины.</p>\r\n\"}]",
                    Image = "184f62e4-7e53-4bc5-b418-aa503a0b9953.jpg",
                    Price = 31,
                    CategoryId = tushka.Id,
                    Created = DateTime.UtcNow
                },
                new Product
                {
                    IsHidden = false,
                    StringKey = "Razdelannaya-tushka",
                    Title = "[{\"k\":\"en\",\"v\":\"A split carcass\"},{\"k\":\"ru\",\"v\":\"Разделанная тушка\"}]",
                    Description = "[{\"k\":\"en\",\"v\":\"<p>Gutted bird cut into 6, 8, 9 or 10 pieces. The tail and abdominal fat may be removed or left at the carcass.</p>\r\n\"},{\"k\":\"ru\",\"v\":\"<p>Потрошеная тушка, разделанная на 6, 8, 9 или 10 частей. Гузка и брюшной жир могут быть удалены или оставлены при тушке.</p>\r\n\"}]",
                    Image = "00847534-1fa2-4f12-b9af-3b3dc896b287.jpg",
                    Price = 45,
                    CategoryId = tushka.Id,
                    Created = DateTime.UtcNow
                },
                new Product
                {
                    IsHidden = false,
                    StringKey = "Okorochok",
                    Title = "[{\"k\":\"en\",\"v\":\"Leg\"},{\"k\":\"ru\",\"v\":\"Окорочок\"}]",
                    Description = "[{\"k\":\"en\",\"v\":\"<p>The whole leg is produced by separating a leg from the back of the carcass of the femur and pelvic bone. The abdominal fat and back are removed. The leg consists of the thigh and lower leg.</p>\r\n\"},{\"k\":\"ru\",\"v\":\"<p>Окорочок получают путем отделения окорочка от задней части тушки по месту соединения бедренной и тазовой костей. Брюшной жир и спинка удаляются. Окорочек состоит из бедра и голени.</p>\r\n\"}]",
                    Image = "65ada48b-4cea-4a69-9e4c-c0fb527bdaef.jpg",
                    Price = 45,
                    CategoryId = nojki.Id,
                    Created = DateTime.UtcNow
                },
                new Product
                {
                    IsHidden = false,
                    StringKey = "Zadnyaya-chetvertina",
                    Title = "[{\"k\":\"en\",\"v\":\"Hindquarter\"},{\"k\":\"ru\",\"v\":\"Задняя четвертина\"}]",
                    Description = "[{\"k\":\"en\",\"v\":\"<p>Is produced by cutting the back of the carcass without tail along the center into two equal parts. Leg quarter without tail consists of an intact part that consists of the drumstick, thigh with adjoining portion of the back and abdominal fat.</p>\r\n\"},{\"k\":\"ru\",\"v\":\"<p>Получают путем разделки задней части тушки без гузки вдоль центра позвоночника на две равные части. Задняя четвертина без гузки представляет собой целую часть, которая состоит из голени, бедра с прилегающими частью спинки и брюшным жиром.</p>\r\n\"}]",
                    Image = "c57131db-fc86-4795-bc6a-945b1b456db4.jpg",
                    Price = 27,
                    CategoryId = nojki.Id,
                    Created = DateTime.UtcNow
                },
                new Product
                {
                    IsHidden = false,
                    StringKey = "Bedro",
                    Title = "[{\"k\":\"en\",\"v\":\"Hip\"},{\"k\":\"ru\",\"v\":\"Бедро\"}]",
                    Description = "[{\"k\":\"en\",\"v\":\"<p>The thigh is produced by separating a leg between the junction place of the tibia and femur. The drumstick, patella, femur bone, and nearly all visible fat are removed. Meat rear of the backrest can be removed or left.</p>\r\n\"},{\"k\":\"ru\",\"v\":\"<p>Бедро получают путем разделения окорочка по месту сочленения голени и бедра. Голень, коленная чашечка, бедренная кость и практически весь видимый жир удаляют. Мясо задней части спинки может быть удалено или оставлено.</p>\r\n\"}]",
                    Image = "a1c9ca81-a882-4dc2-a6fb-cf6e3fe9eff3.jpg",
                    Price = 32,
                    CategoryId = nojki.Id,
                    Created = DateTime.UtcNow
                },
                new Product
                {
                    IsHidden = false,
                    StringKey = "blade",
                    Title = "[{\"k\":\"en\",\"v\":\"The one piece blade section\"},{\"k\":\"ru\",\"v\":\"Цельная лопаточная часть\"}]",
                    Description = "[{\"k\":\"en\",\"v\":\"<p>The one-piece blade section is located in front of the carcass. Pork jowl usually removed.</p>\r\n\"},{\"k\":\"ru\",\"v\":\"<p>Цельная лопаточная часть размещается в передней части туши. Свиная щековина обычно удаляется.</p>\r\n\"}]",
                    Image = "74418be6-44f4-433f-96ea-64d1bda96739.jpg",
                    Price = 34,
                    CategoryId = paddle.Id,
                    Created = DateTime.UtcNow
                }
                );
            #endregion

            #region Slider

            context.Sliders.AddOrUpdate(_ => _.Image,
                new Slider
                {
                    Title = " ",
                    Image = "slide1.jpg",
                    Url = "#",
                    Created = DateTime.UtcNow
                },
                new Slider
                {
                    Title = " ",
                    Image = "slide2.jpg",
                    Url = "#",
                    Created = DateTime.UtcNow
                },
                new Slider
                {
                    Title = " ",
                    Image = "slide3.jpg",
                    Url = "#",
                    Created = DateTime.UtcNow
                }
                );

            #endregion

            #region Локализация

            context.Localizations.AddOrUpdate(_ => _.StringKey,
                new Localization
                {
                    StringKey = "Search...",
                    Name = "[{\"k\":\"en\",\"v\":\"Search...\"},{\"k\":\"ru\",\"v\":\"Поиск...\"}]"
                },
                new Localization
                {
                    StringKey = "Заказать",
                    Name = "[{\"k\":\"en\",\"v\":\"To order\"},{\"k\":\"ru\",\"v\":\"Заказать\"}]"
                },
                new Localization
                {
                    StringKey = "Перейти",
                    Name = "[{\"k\":\"en\",\"v\":\"View\"},{\"k\":\"ru\",\"v\":\"Перейти\"}]"
                },
                new Localization
                {
                    StringKey = "Замороженные...",
                    Name = "[{\"k\":\"en\",\"v\":\"Frozen meat and fish products from North and South America, Europe and Asia.\"},{\"k\":\"ru\",\"v\":\"Замороженные мясные и рыбные продукты из Северной и Южной Америки, Европы и Азии.\"}]"
                },
                new Localization
                {
                    StringKey = "Показать все",
                    Name = "[{\"k\":\"en\",\"v\":\"View all\"},{\"k\":\"ru\",\"v\":\"Показать все\"}]"
                },
                new Localization
                {
                    StringKey = "Отправить сообщение",
                    Name = "[{\"k\":\"en\",\"v\":\"Send message\"},{\"k\":\"ru\",\"v\":\"Отправить сообщение\"}]"
                },
                new Localization
                {
                    StringKey = "Имя",
                    Name = "[{\"k\":\"en\",\"v\":\"Name\"},{\"k\":\"ru\",\"v\":\"Имя\"}]"
                },
                new Localization
                {
                    StringKey = "Почта",
                    Name = "[{\"k\":\"en\",\"v\":\"Email\"},{\"k\":\"ru\",\"v\":\"Почта\"}]"
                },
                new Localization
                {
                    StringKey = "Сообщение",
                    Name = "[{\"k\":\"en\",\"v\":\"Message\"},{\"k\":\"ru\",\"v\":\"Сообщение\"}]"
                },
                new Localization
                {
                    StringKey = "ОТПРАВИТЬ",
                    Name = "[{\"k\":\"en\",\"v\":\"SEND\"},{\"k\":\"ru\",\"v\":\"ОТПРАВИТЬ\"}]"
                },
                new Localization
                {
                    StringKey = "Наши контакты",
                    Name = "[{\"k\":\"en\",\"v\":\"Contacts\"},{\"k\":\"ru\",\"v\":\"Наши контакты\"}]"
                },
                new Localization
                {
                    StringKey = "Мобильный тел.",
                    Name = "[{\"k\":\"en\",\"v\":\"Mobile Phone\"},{\"k\":\"ru\",\"v\":\"Мобильный тел.\"}]"
                },
                new Localization
                {
                    StringKey = "Рабочий тел.",
                    Name = "[{\"k\":\"en\",\"v\":\"Work Phone\"},{\"k\":\"ru\",\"v\":\"Рабочий тел.\"}]"
                },
                new Localization
                {
                    StringKey = "Сайт",
                    Name = "[{\"k\":\"en\",\"v\":\"Site\"},{\"k\":\"ru\",\"v\":\"Сайт\"}]"
                },
                new Localization
                {
                    StringKey = "E-mail",
                    Name = "[{\"k\":\"en\",\"v\":\"E-mail\"},{\"k\":\"ru\",\"v\":\"Почта\"}]"
                },
                new Localization
                {
                    StringKey = "Вам нужно...",
                    Name = "[{\"k\":\"en\",\"v\":\"You need to frozen poultry, beef, pork or frozen fish?\"},{\"k\":\"ru\",\"v\":\"Вам нужно замороженное мясо птицы, говядина, свинина или замороженная рыба?\"}]"
                },
                new Localization
                {
                    StringKey = "подробнее",
                    Name = "[{\"k\":\"en\",\"v\":\"more\"},{\"k\":\"ru\",\"v\":\"подробнее\"}]"
                },
                new Localization
                {
                    StringKey = "Прайс",
                    Name = "[{\"k\":\"en\",\"v\":\"Price\"},{\"k\":\"ru\",\"v\":\"Прайс\"}]"
                }
                );

            #endregion

            #region Units

            context.Units.AddOrUpdate(_ => _.Title,
                new Unit
                {
                    Title = "лист"
                },
                new Unit
                {
                    Title = "шт."
                },
                new Unit
                {
                    Title = "пог. м."
                },
                new Unit
                {
                    Title = "кв.м."
                }
                );

            #endregion

            #region Articles

            //// категории
            //var annonce = new ArticleCategory
            //{
            //    StringKey = "annonce",
            //    Name = "Анонсы",
            //    MetaTitle = "Анонсы",
            //    Created = DateTime.UtcNow
            //};


            //context.ArticleCategories.AddOrUpdate(_ => _.StringKey,
            //    annonce
            //    );

            //context.SaveChanges();

            // статьи

            

            #endregion

        }

        private static void AddOrUpdate(DataContext context, params MenuItem[] topMenuItems)
        {
            foreach (var item in topMenuItems)
            {
                var isNew = false;
                var menuItem =
                    context.MenuItems.FirstOrDefault(
                        _ => _.MenuId == item.MenuId && _.ParentId == item.ParentId && _.Name.Equals(item.Name));
                if (menuItem == null)
                {
                    menuItem = item;
                    isNew = true;
                }

                menuItem.Name = item.Name;
                menuItem.Type = item.Type;
                menuItem.MenuId = item.MenuId;
                menuItem.CustomUrl = item.CustomUrl;
                menuItem.PageId = item.PageId;
                menuItem.Created = item.Created;
                menuItem.ParentId = item.ParentId;
                if (isNew)
                {
                    context.MenuItems.Add(menuItem);
                    context.SaveChanges();
                }

                item.Id = menuItem.Id;
            }
        }
    }
}