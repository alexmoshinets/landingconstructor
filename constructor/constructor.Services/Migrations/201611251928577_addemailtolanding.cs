namespace Services.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addemailtolanding : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Landings", "Email", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Landings", "Email");
        }
    }
}
