namespace Services.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class settinglanding : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LandingSettings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SettingsJson = c.String(),
                        LandingId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Landings", t => t.LandingId, cascadeDelete: true)
                .Index(t => t.LandingId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LandingSettings", "LandingId", "dbo.Landings");
            DropIndex("dbo.LandingSettings", new[] { "LandingId" });
            DropTable("dbo.LandingSettings");
        }
    }
}
