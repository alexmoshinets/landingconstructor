namespace Services.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addIsNotify : DbMigration
    {
        public override void Up()
        {
            AddColumn("Account.Users", "IsNotify", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("Account.Users", "IsNotify");
        }
    }
}
