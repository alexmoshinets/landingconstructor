namespace Services.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class userphone : DbMigration
    {
        public override void Up()
        {
            AddColumn("Account.Users", "Phone", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("Account.Users", "Phone");
        }
    }
}
