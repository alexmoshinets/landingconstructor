﻿using System;
using System.IO;
using Interfaces.Common;
using NLog;
using NLog.Config;

namespace Services.Common
{
    public class LoggerService : ILoggerService
    {
        #region Fields

        private readonly Logger _logger;

        #endregion

        #region Constructor

        public LoggerService()
        {
            var file = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "NLog.config");
            if (!File.Exists(file))
            {
                file = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "bin", "NLog.config");
            }
            LogManager.Configuration = new XmlLoggingConfiguration(file);
            _logger = LogManager.GetCurrentClassLogger();
        }

        #endregion

        public void Info(string message)
        {
            _logger.Info(message);
        }

        public void Info(string message, Exception exception)
        {
            _logger.InfoException(message, exception);
        }

        public void Warn(string message)
        {
            _logger.Warn(message);
        }

        public void Warn(string message, Exception exception)
        {
            _logger.WarnException(message, exception);
        }

        public void Error(string message)
        {
            _logger.Error(message);
        }

        public void Error(string message, Exception exception)
        {
            _logger.ErrorException(message, exception);
        }

        public void Fatal(string message)
        {
            _logger.Fatal(message);
        }

        public void Fatal(string message, Exception exception)
        {
            _logger.FatalException(message, exception);
        }
    }
}