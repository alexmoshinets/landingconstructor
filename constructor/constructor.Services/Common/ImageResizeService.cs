﻿using System.Drawing;
using System.Web;
using ImageResizer;
using Interfaces.Common;

namespace Services.Common
{
    public class ImageResizeService : IImageResizeService
    {
        public Image ResizePhoto(HttpPostedFileBase inStream, int maxWidth, int quality)
        {
            var settings = new ResizeSettings
            {
                MaxWidth = maxWidth,
                Quality = quality,
            };

            var newImage = ImageBuilder.Current.Build(inStream, settings);
            return newImage;
        }
    }
}