﻿using System;
using System.Net.Mail;
using Interfaces.Common;

namespace Services.Common
{
    public class MessageService : IMessageService
    {
        private readonly ILoggerService _loggerService;

        public MessageService(ILoggerService loggerService)
        {
            _loggerService = loggerService;
        }

        public bool Send(string to, string subject, string body, bool isBodyHtml = true)
        {
            var mailMessage = new MailMessage();

            return Send(mailMessage.From, to, subject, body, isBodyHtml);
        }

        public bool Send(MailAddress from, string to, string subject, string body, bool isBodyHtml = true)
        {
            var result = false;

            var smtpClient = new SmtpClient();

            var mailMessage = new MailMessage();
            if (!smtpClient.Host.Equals("smtp.gmail.com") || !smtpClient.Host.Equals("smtp.yandex.ru"))
            {
                mailMessage.From = from;
            }
            mailMessage.To.Add(to);
            mailMessage.Subject = subject;
            mailMessage.Body = body;
            mailMessage.IsBodyHtml = isBodyHtml;

            try
            {
                smtpClient.Send(mailMessage);
                result = true;
            }
            catch (Exception ex)
            {
                _loggerService.Error(string.Format("Проблема отправки сообщения. To: {0} Ex: {1}; InEx: {2} ", to,
                    ex.Message, ex.InnerException == null ? "" : ex.InnerException.Message));
            }

            return result;
        }
    }
}