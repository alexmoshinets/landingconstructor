﻿using System;
using System.Collections.Generic;
using System.Linq;
using DomainModel.Content;
using Interfaces.Content;

namespace Services.Content
{
    public class SliderService : ISliderService
    {
        public Slider GetById(int id)
        {
            using (var db = new DataContext())
            {
                var result = db.Sliders.FirstOrDefault(_ => _.Id == id);
                return result;
            }
        }

        public IList<Slider> GetAll(bool onlyActive = false)
        {
            using (var db = new DataContext())
            {
                var result = db.Sliders.AsQueryable();
                if (onlyActive)
                {
                    result = result.Where(_ => !_.IsHidden);
                }
                return result.ToList();
            }
        }

        public Slider Update(int id, Action<Slider> update)
        {
            using (var db = new DataContext())
            {
                var old = db.Sliders.FirstOrDefault(_ => _.Id == id);
                if (old == null)
                {
                    return null;
                }
                update(old);
                db.SaveChanges();
                return old;
            }
        }

        public Slider Add(Slider entity)
        {
            using (var db = new DataContext())
            {
                db.Sliders.Add(entity);
                db.SaveChanges();
                return entity;
            }
        }

        public bool Delete(int id)
        {
            using (var db = new DataContext())
            {
                var entity = db.Sliders.FirstOrDefault(_ => _.Id == id);
                if (entity == null)
                {
                    return false;
                }

                db.Sliders.Remove(entity);
                db.SaveChanges();
                return true;
            }
        }
    }
}