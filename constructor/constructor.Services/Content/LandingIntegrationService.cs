﻿using Interfaces.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel.Content;

namespace Services.Content
{
    public class LandingIntegrationService : ILandingIntegrationService
    {
        public LandingIntegration Add(LandingIntegration entity)
        {
            using (var db = new DataContext())
            {
                var result = db.LandingIntegrations.Add(entity);
                db.SaveChanges();
                return result;
            }
        }

        public bool Delete(int id)
        {
            using (var db = new DataContext())
            {
                var result = db.LandingIntegrations.FirstOrDefault(_ => _.LandingId == id);
                if (result != null)
                {
                    db.LandingIntegrations.Remove(result);
                    return true;
                }
                return false;
            }
        }

        public List<LandingIntegration> GetAll()
        {
            using (var db = new DataContext())
            {
                var result = db.LandingIntegrations.ToList();
                return result;
            }
        }

        public LandingIntegration GetByLandingId(string id)
        {
            using (var db = new DataContext())
            {
                var result = db.LandingIntegrations.FirstOrDefault(_ => _.FormId == id);
                return result;
            }
        }

        public LandingIntegration GetByFormId(string id)
        {
            using (var db = new DataContext())
            {
                var result = db.LandingIntegrations.FirstOrDefault(_ => _.FormId == id);
                return result;
            }
        }

        public LandingIntegration Update(LandingIntegration entity)
        {
            using (var db = new DataContext())
            {
                var old = db.LandingIntegrations.FirstOrDefault(_ => _.FormId == entity.FormId);
                old.SettingsJson = entity.SettingsJson;
                old.TypeService = entity.TypeService;
                old.IsRedirect = entity.IsRedirect;
                old.RedirectUrl = entity.RedirectUrl;
                old.AlertMessage = entity.AlertMessage;
                db.SaveChanges();
                return old;
            }
        }
    }
}
