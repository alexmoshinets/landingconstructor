﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DomainModel.Content;
using Interfaces.Content;

namespace Services.Content
{
    public class SettingService : ISettingService
    {
        public Setting GetByKey(string stringKey)
        {
            using (var db = new DataContext())
            {
                var result = db.Settings.FirstOrDefault(_ => _.StringKey.Equals(stringKey));
                return result;
            }
        }

        public Setting GetById(int id)
        {
            using (var db = new DataContext())
            {
                var result = db.Settings.FirstOrDefault(_ => _.Id == id);
                return result;
            }
        }

        public IList<Setting> GetAll()
        {
            using (var db = new DataContext())
            {
                var result = db.Settings;
                return result.ToList();
            }
        }

        public Setting Update(int id, Action<Setting> update)
        {
            using (var db = new DataContext())
            {
                var old = db.Settings.FirstOrDefault(_ => _.Id == id);
                if (old == null)
                {
                    return null;
                }
                update(old);
                db.SaveChanges();
                return old;
            }
        }

        public Setting Update(string stringKey, Action<Setting> update)
        {
            using (var db = new DataContext())
            {
                var old = db.Settings.FirstOrDefault(_ => _.StringKey.Equals(stringKey));
                if (old == null)
                {
                    return null;
                }
                update(old);
                db.SaveChanges();
                return old;
            }
        }

        public Setting Add(Setting entity)
        {
            using (var db = new DataContext())
            {
                db.Settings.Add(entity);
                db.SaveChanges();
                return entity;
            }
        }

        public bool Delete(int id)
        {
            using (var db = new DataContext())
            {
                var entity = db.Settings.FirstOrDefault(_ => _.Id == id);
                if (entity == null)
                {
                    return false;
                }

                db.Settings.Remove(entity);
                db.SaveChanges();
                return true;
            }
        }
    }
}