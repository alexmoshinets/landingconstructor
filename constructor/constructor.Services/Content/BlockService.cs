﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DomainModel.Content;
using Interfaces.Content;

namespace Services.Content
{
    public class BlockService : IBlockService
    {
        public IList<Block> GetByBlockListKey(string stringKey, bool onlyActive = false)
        {
            using (var db = new DataContext())
            {
                var result = db.Blocks.Where(_ => _.BlockList.StringKey.Equals(stringKey));
                if (onlyActive)
                {
                    result = result.Where(_ => !_.IsHidden);
                }
                return result.ToList();
            }
        }

        public IList<Block> GetAll(bool onlyActive = false)
        {
            using (var db = new DataContext())
            {
                var result = db.Blocks.Include(_ => _.BlockList);
                if (onlyActive)
                {
                    result = result.Where(_ => !_.IsHidden);
                }
                return result.ToList();
            }
        }

        public Block GetByKey(string stringKey)
        {
            using (var db = new DataContext())
            {
                return db.Blocks.FirstOrDefault(_ => _.StringKey.Equals(stringKey));
            }
        }

        public Block GetById(int id)
        {
            using (var db = new DataContext())
            {
                var result = db.Blocks.FirstOrDefault(_ => _.Id == id);
                return result;
            }
        }

        public Block Update(int id, Action<Block> update)
        {
            using (var db = new DataContext())
            {
                var old = db.Blocks.FirstOrDefault(_ => _.Id == id);
                if (old == null) { return null; }
                update(old);
                db.SaveChanges();
                return old;
            }
        }

        public Block Add(Block entity)
        {
            using (var db = new DataContext())
            {
                db.Blocks.Add(entity);
                db.SaveChanges();
                return entity;
            }
        }

        public bool Delete(int id)
        {
            using (var db = new DataContext())
            {
                var entity = db.Blocks.FirstOrDefault(_ => _.Id == id);
                if (entity == null)
                {
                    return false;
                }

                db.Blocks.Remove(entity);
                db.SaveChanges();
                return true;
            }
        }

        public Block GetBlock(string stringKey)
        {
            using (var db = new DataContext())
            {
                return db.Blocks.FirstOrDefault(_ => _.StringKey.Equals(stringKey));
            }
        }
    }
}