﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using DomainModel.Content;
using Interfaces.Content;

namespace Services.Content
{
    public class PhotoAlbumService : IPhotoAlbumService
    {
        public PhotoAlbum GetByKey(string stringKey)
        {
            using (var db = new DataContext())
            {
                var result =
                    db.PhotoAlbums.Include(_ => _.Photos).FirstOrDefault(_ => _.StringKey.Equals(stringKey));
                return result;
            }
        }

        public PhotoAlbum GetById(int id)
        {
            using (var db = new DataContext())
            {
                var result = db.PhotoAlbums.Include(_ => _.Photos).FirstOrDefault(_ => _.Id == id);
                return result;
            }
        }

        public IList<string> GetPagingById(int id, int count)
        {
            using (var db = new DataContext())
            {
                var result =
                    db.PhotoAlbums.Where(_ => !_.IsHidden)
                        .OrderBy(_ => _.Id)
                        .Where(_ => _.Id > id)
                        .Take(count)
                        .Union(db.PhotoAlbums.OrderByDescending(_ => _.Id).Where(_ => _.Id < id).Take(count));
                return result.Select(_ => _.StringKey).ToList();
            }
        }

        public IList<PhotoAlbum> GetAll(bool onlyActive = true)
        {
            using (var db = new DataContext())
            {
                var result = db.PhotoAlbums.Include(_ => _.Photos);
                if (onlyActive)
                {
                    result = result.Where(_ => !_.IsHidden);
                }
                return result.ToList();
            }
        }

        public IList<TResult> GetAll<TResult>(Expression<Func<PhotoAlbum, TResult>> selector, bool onlyActive = true)
        {
            using (var db = new DataContext())
            {
                var result = db.PhotoAlbums.Include(_ => _.Photos);
                if (onlyActive)
                {
                    result = result.Where(_ => !_.IsHidden);
                }
                return result.Select(selector).ToList();
            }
        }

        public PhotoAlbum Update(int id, Action<PhotoAlbum> update)
        {
            using (var db = new DataContext())
            {
                var old = db.PhotoAlbums.FirstOrDefault(_ => _.Id == id);
                if (old == null)
                {
                    return null;
                }
                update(old);
                db.SaveChanges();
                return old;
            }
        }

        public PhotoAlbum Update(string stringKey, Action<PhotoAlbum> update)
        {
            using (var db = new DataContext())
            {
                var old = db.PhotoAlbums.FirstOrDefault(_ => _.StringKey.Equals(stringKey));
                if (old == null)
                {
                    return null;
                }
                update(old);
                db.SaveChanges();
                return old;
            }
        }

        public PhotoAlbum Add(PhotoAlbum entity)
        {
            using (var db = new DataContext())
            {
                db.PhotoAlbums.Add(entity);
                db.SaveChanges();
                return entity;
            }
        }

        public bool Delete(int id)
        {
            using (var db = new DataContext())
            {
                var entity = db.PhotoAlbums.FirstOrDefault(_ => _.Id == id);
                if (entity == null)
                {
                    return false;
                }

                db.PhotoAlbums.Remove(entity);
                db.SaveChanges();
                return true;
            }
        }
    }
}