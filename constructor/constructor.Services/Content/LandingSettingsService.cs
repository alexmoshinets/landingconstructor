﻿using Interfaces.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel.Content;

namespace Services.Content
{
    public class LandingSettingsService : ILandingSettingsService
    {
        public LandingSetting Add(LandingSetting entity)
        {
            using (var db = new DataContext())
            {
                var result = db.LandingSettings.Add(entity);
                db.SaveChanges();
                return result;
            }
        }

        public bool Delete(int id)
        {
            using (var db = new DataContext())
            {
                var result = db.LandingSettings.FirstOrDefault(_ => _.LandingId == id);
                if (result != null)
                {
                    db.LandingSettings.Remove(result);
                    return true;
                }
                return false;
            }
        }

        public List<LandingSetting> GetAll()
        {
            using (var db = new DataContext())
            {
                var result = db.LandingSettings.ToList();
                return result;
            }
        }

        public LandingSetting GetByLandingId(int id)
        {
            using (var db = new DataContext())
            {
                var result = db.LandingSettings.FirstOrDefault(_ => _.LandingId == id);
                return result;
            }
        }

        public LandingSetting GetById(int id)
        {
            using (var db = new DataContext())
            {
                var result = db.LandingSettings.FirstOrDefault(_ => _.Id == id);
                return result;
            }
        }

        public LandingSetting Update(LandingSetting entity)
        {
            using (var db = new DataContext())
            {
                var old = db.LandingSettings.FirstOrDefault(_ => _.Id == entity.Id);
                old.SettingsJson = entity.SettingsJson;
                db.SaveChanges();
                return old;
            }
        }
    }
}
