﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using DomainModel.Content;
using Interfaces.Content;

namespace Services.Content
{
    public class ArticleCategoryService : IArticleCategoryService
    {
        public ArticleCategory GetByKey(string stringKey)
        {
            using (var db = new DataContext())
            {
                var result = db.ArticleCategories.FirstOrDefault(_ => _.StringKey.Equals(stringKey));
                return result;
            }
        }

        public ArticleCategory GetById(int id)
        {
            using (var db = new DataContext())
            {
                var result = db.ArticleCategories.Include(_ => _.Articles).FirstOrDefault(_ => _.Id == id);
                return result;
            }
        }

        public IList<ArticleCategory> GetAll()
        {
            using (var db = new DataContext())
            {
                var result = db.ArticleCategories.Include(_ => _.Articles);
                return result.ToList();
            }
        }

        public IList<TResult> GetAll<TResult>(Expression<Func<ArticleCategory, TResult>> selector)
        {
            using (var db = new DataContext())
            {
                var result = db.ArticleCategories.Include(_ => _.Articles).Select(selector);
                return result.ToList();
            }
        }

        public ArticleCategory Update(int id, Action<ArticleCategory> update)
        {
            using (var db = new DataContext())
            {
                var old = db.ArticleCategories.FirstOrDefault(_ => _.Id == id);
                if (old == null)
                {
                    return null;
                }
                update(old);
                db.SaveChanges();
                return old;
            }
        }

        public ArticleCategory Add(ArticleCategory entity)
        {
            using (var db = new DataContext())
            {
                db.ArticleCategories.Add(entity);
                db.SaveChanges();
                return entity;
            }
        }

        public bool Delete(int id)
        {
            using (var db = new DataContext())
            {
                var entity = db.ArticleCategories.FirstOrDefault(_ => _.Id == id);
                if (entity == null)
                {
                    return false;
                }

                db.ArticleCategories.Remove(entity);
                db.SaveChanges();
                return true;
            }
        }
    }
}