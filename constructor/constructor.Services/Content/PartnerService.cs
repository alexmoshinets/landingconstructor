﻿using System;
using System.Collections.Generic;
using System.Linq;
using DomainModel.Content;
using Interfaces.Content;

namespace Services.Content
{
    public class PartnerService : IPartnersService
    {
        public Partner GetById(int id)
        {
            using (var db = new DataContext())
            {
                var result = db.Partners.FirstOrDefault(_ => _.Id == id);
                return result;
            }
        }

        public IList<Partner> GetAll(bool onlyActive = false)
        {
            using (var db = new DataContext())
            {
                var result = db.Partners.AsQueryable();
                if (onlyActive)
                {
                    result = result.Where(_ => !_.IsHidden);
                }
                return result.ToList();
            }
        }

        public Partner Update(int id, Action<Partner> update)
        {
            using (var db = new DataContext())
            {
                var old = db.Partners.FirstOrDefault(_ => _.Id == id);
                if (old == null)
                {
                    return null;
                }
                update(old);
                db.SaveChanges();
                return old;
            }
        }

        public Partner Add(Partner entity)
        {
            using (var db = new DataContext())
            {
                db.Partners.Add(entity);
                db.SaveChanges();
                return entity;
            }
        }

        public bool Delete(int id)
        {
            using (var db = new DataContext())
            {
                var entity = db.Partners.FirstOrDefault(_ => _.Id == id);
                if (entity == null)
                {
                    return false;
                }

                db.Partners.Remove(entity);
                db.SaveChanges();
                return true;
            }
        }
    }
}
