﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using DomainModel.Content;
using Interfaces.Content;

namespace Services.Content
{
    public class SectionsService : ISectionsService
    {
        public Sections GetByKey(string stringKey)
        {
            using (var db = new DataContext())
            {
                var result = db.Sections.FirstOrDefault(_ => _.StringKey.Equals(stringKey));
                return result;
            }
        }
        public bool NameIsExist(string name)
        {
            using (var db = new DataContext())
            {
                var result = db.Sections.Where(_ => _.Name.ToLower() == name.ToLower());
                if (result.Count() > 0) {
                    return true;
                }
                return false;
            }
        }

        public IList<Sections> GetAll(bool onlyActive = false)
        {
            using (var db = new DataContext())
            {
                IQueryable<Sections> result = db.Sections;
                if (onlyActive)
                {
                    result = result.Where(_ => !_.IsPublic);
                }
                return result.ToList();
            }
        }

        public Sections Update(int id, Action<Sections> update)
        {
            using (var db = new DataContext())
            {
                var old = db.Sections.FirstOrDefault(_ => _.Id == id);
                if (old == null)
                {
                    return null;
                }
                update(old);
                db.SaveChanges();
                return old;
            }
        }

        public Sections Update(string stringKey, Action<Sections> update)
        {
            using (var db = new DataContext())
            {
                var old = db.Sections.FirstOrDefault(_ => _.StringKey == stringKey);
                if (old == null)
                {
                    return null;
                }
                update(old);
                db.SaveChanges();
                return old;
            }
        }

        public string AddOrUpdate(string stringKey, Sections entity, Action<Sections> update)
        {
            using (var db = new DataContext())
            {
                var old = db.Sections.FirstOrDefault(_ => _.StringKey == stringKey);
                if (old == null)
                {
                    db.Sections.Add(entity);
                    db.SaveChanges();
                    return "add";
                }
                update(old);
                db.SaveChanges();
                return "update";
            }
        }

        public Sections Add(Sections entity)
        {
            using (var db = new DataContext())
            {
                db.Sections.Add(entity);
                db.SaveChanges();
                return entity;
            }
        }     

        public bool Delete(string stringKey)
        {
            using (var db = new DataContext())
            {
                var entities = db.Sections.Where(_ => _.StringKey == stringKey);
                if (!entities.Any())
                {
                    return false;
                }
                foreach (var item in entities)
                {
                    db.Sections.Remove(item);
                    db.SaveChanges();
                }
                return true;
            }
        }
    }
}