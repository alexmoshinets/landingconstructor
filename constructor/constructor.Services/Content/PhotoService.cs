﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using DomainModel.Content;
using Interfaces.Content;

namespace Services.Content
{
    public class PhotoService : IPhotoService
    {
        public Photo GetById(int id)
        {
            using (var db = new DataContext())
            {
                var result = db.Photos.FirstOrDefault(_ => _.Id == id);
                return result;
            }
        }

        public IList<Photo> GetAll()
        {
            using (var db = new DataContext())
            {
                var result = db.Photos;
                return result.ToList();
            }
        }

        public IList<TResult> GetAll<TResult>(Expression<Func<Photo, TResult>> selector)
        {
            using (var db = new DataContext())
            {
                var result = db.Photos.Select(selector);
                return result.ToList();
            }
        }

        public Photo Add(Photo entity)
        {
            using (var db = new DataContext())
            {
                db.Photos.Add(entity);
                db.SaveChanges();
                return entity;
            }
        }

        public bool Delete(int id)
        {
            using (var db = new DataContext())
            {
                var entity = db.Photos.FirstOrDefault(_ => _.Id == id);
                if (entity == null)
                {
                    return false;
                }

                db.Photos.Remove(entity);
                db.SaveChanges();
                return true;
            }
        }
    }
}