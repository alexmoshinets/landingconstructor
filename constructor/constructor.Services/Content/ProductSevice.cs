﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using DomainModel.Content;
using Interfaces.Content;

namespace Services.Content
{
    public class ProductService : IProductService
    {
        public Product GetById(int id)
        {
            using (var db = new DataContext())
            {
                var result = db.Products.Include(_=>_.Category).FirstOrDefault(_ => _.Id == id);
                return result;
            }
        }



        public IList<Product> GetAll(bool onlyActive = false)
        {
            using (var db = new DataContext())
            {
                var res = db.Products.Include(_ => _.Category).ToList();
                if (onlyActive)
                {
                    var result = res.Where(_ => !_.IsHidden);
                    return result.ToList();
                }
                return res.ToList();
            }
        }

        public IList<Product> GetLatestProducts(int Count)
        {
            using (var db = new DataContext())
            {
                var result =
                    db.Products.Include(_ => _.Category).OrderByDescending(_ => _.Id).AsQueryable().Take(Count);
                return result.ToList();
            }
        }

        public Product Update(int id, Action<Product> update)
        {
            using (var db = new DataContext())
            {
                var old = db.Products.FirstOrDefault(_ => _.Id == id);
                if (old == null)
                {
                    return null;
                }
                update(old);
                db.SaveChanges();
                return old;
            }
        }

        public Product Add(Product entity)
        {
            using (var db = new DataContext())
            {
                db.Products.Add(entity);
                db.SaveChanges();
                return entity;
            }
        }

        public string Delete(int id)
        {
            using (var db = new DataContext())
            {
                var entity = db.Products.FirstOrDefault(_ => _.Id == id);
                if (entity == null)
                {
                    return null;
                }

                db.Products.Remove(entity);
                db.SaveChanges();
                return entity.Image;
            }
        }
    }
}

