﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using DomainModel.Content;
using Interfaces.Content;

namespace Services.Content
{
    public class LocalizationService : ILocalizationService
    {
        public Localization GetByKey(string stringKey)
        {
            using (var db = new DataContext())
            {
                var result = db.Localizations.FirstOrDefault(_ => _.StringKey.Equals(stringKey));
                return result;
            }
        }

        public Localization GetById(int id)
        {
            using (var db = new DataContext())
            {
                var result = db.Localizations.FirstOrDefault(_ => _.LocalizationId == id);
                return result;
            }
        }

        public IList<Localization> GetAll()
        {
            using (var db = new DataContext())
            {
                var result = db.Localizations;
                return result.ToList();
            }
        }

        public Localization Update(int id, Action<Localization> update)
        {
            using (var db = new DataContext())
            {
                var old = db.Localizations.FirstOrDefault(_ => _.LocalizationId == id);
                if (old == null)
                {
                    return null;
                }
                update(old);
                db.SaveChanges();
                return old;
            }
        }

        public Localization Update(string stringKey, Action<Localization> update)
        {
            using (var db = new DataContext())
            {
                var old = db.Localizations.FirstOrDefault(_ => _.StringKey.Equals(stringKey));
                if (old == null)
                {
                    return null;
                }
                update(old);
                db.SaveChanges();
                return old;
            }
        }

        public Localization Add(Localization entity)
        {
            using (var db = new DataContext())
            {
                db.Localizations.Add(entity);
                db.SaveChanges();
                return entity;
            }
        }

        public bool Delete(int id)
        {
            using (var db = new DataContext())
            {
                var entity = db.Localizations.FirstOrDefault(_ => _.LocalizationId == id);
                if (entity == null)
                {
                    return false;
                }

                db.Localizations.Remove(entity);
                db.SaveChanges();
                return true;
            }
        }
    }
}