﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using DomainModel.Content;
using Interfaces.Content;

namespace Services.Content
{
    public class SectionsCategoryService : ISectionsCategoryService
    {
        public SectionsCategory GetByKey(string stringKey)
        {
            using (var db = new DataContext())
            {
                var result = db.SectionsCategory.FirstOrDefault(_ => _.StringKey.Equals(stringKey));
                return result;
            }
        }

        public IList<SectionsCategory> GetAll()
        {
            using (var db = new DataContext())
            {
                IQueryable<SectionsCategory> result = db.SectionsCategory;                
                return result.ToList();
            }
        }

        public SectionsCategory Update(int id, Action<SectionsCategory> update)
        {
            using (var db = new DataContext())
            {
                var old = db.SectionsCategory.FirstOrDefault(_ => _.Id == id);
                if (old == null)
                {
                    return null;
                }
                update(old);
                db.SaveChanges();
                return old;
            }
        }

        public SectionsCategory Update(string stringKey, Action<SectionsCategory> update)
        {
            using (var db = new DataContext())
            {
                var old = db.SectionsCategory.FirstOrDefault(_ => _.StringKey == stringKey);
                if (old == null)
                {
                    return null;
                }
                update(old);
                db.SaveChanges();
                return old;
            }
        }

        public string AddOrUpdate(string stringKey, SectionsCategory entity, Action<SectionsCategory> update)
        {
            using (var db = new DataContext())
            {
                var old = db.SectionsCategory.FirstOrDefault(_ => _.StringKey == stringKey);
                if (old == null)
                {
                    db.SectionsCategory.Add(entity);
                    db.SaveChanges();
                    return "add";
                }
                update(old);
                db.SaveChanges();
                return "update";
            }
        }

        public SectionsCategory Add(SectionsCategory entity)
        {
            using (var db = new DataContext())
            {
                db.SectionsCategory.Add(entity);
                db.SaveChanges();
                return entity;
            }
        }     

        public bool Delete(string stringKey)
        {
            using (var db = new DataContext())
            {
                var entities = db.SectionsCategory.Where(_ => _.StringKey == stringKey);
                if (!entities.Any())
                {
                    return false;
                }
                foreach (var item in entities)
                {
                    db.SectionsCategory.Remove(item);
                    db.SaveChanges();
                }
                return true;
            }
        }
    }
}