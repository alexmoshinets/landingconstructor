﻿using System;
using System.Collections.Generic;
using System.Linq;
using DomainModel.Content;
using Interfaces.Content;
using System.Linq.Expressions;
using System.Data.Entity;

namespace Services.Content
{
    public class UnitsService: IUnitsService
    {
        public IList<Unit> GetAll()
        {
            using (var db = new DataContext())
            {
                var result = db.Units.AsQueryable();
                return result.ToList();
            }
        }

        public IList<TResult> GetAllUnits<TResult>(Expression<Func<Unit, TResult>> selector)
        {
            using (var db = new DataContext())
            {
                var result = db.Units.Include(_=>_.Id).Select(selector);
                return result.ToList();
            }
        }
    }
}
