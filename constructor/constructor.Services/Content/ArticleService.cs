﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DomainModel.Content;
using Interfaces.Content;

namespace Services.Content
{
    public class ArticleService : IArticleService
    {
        public Article GetByKey(string stringKey)
        {
            using (var db = new DataContext())
            {
                var result = db.Articles.Include(_ => _.Category).FirstOrDefault(_ => _.StringKey.Equals(stringKey));
                return result;
            }
        }

        public Article GetById(int id)
        {
            using (var db = new DataContext())
            {
                var result = db.Articles.Include(_ => _.Category).FirstOrDefault(_ => _.Id == id);
                return result;
            }
        }

        public IList<Article> GetAll(bool onlyActive = true)
        {
            using (var db = new DataContext())
            {
                var result =
                    db.Articles.Include(_ => _.Category).OrderByDescending(_ => _.Date).AsQueryable();
                if (onlyActive)
                {
                    result = result.Where(_ => !_.IsHidden);
                }
                return result.ToList();
            }
        }

        public IList<Article> GetLast(int Count)
        {
            using (var db = new DataContext())
            {
                var result =
                    db.Articles.OrderByDescending(_ => _.Date).AsQueryable().Take(Count);
                return result.ToList();
            }
        }

        public IList<Article> GetAllByCategoryKey(string stringKey, bool onlyActive = true)
        {
            using (var db = new DataContext())
            {
                var result =
                    db.Articles.Include(_ => _.Category)
                        .Where(_ => _.Category.StringKey.Equals(stringKey))
                        .OrderByDescending(_ => _.Date)
                        .AsQueryable();
                if (onlyActive)
                {
                    result = result.Where(_ => !_.IsHidden);
                }
                return result.ToList();
            }
        }

        public IList<Article> GetLast(int count, bool onlyActive = true)
        {
            using (var db = new DataContext())
            {
                var result =
                    db.Articles.Include(_ => _.Category).OrderByDescending(_ => _.Date).AsQueryable();
                if (onlyActive)
                {
                    result = result.Where(_ => !_.IsHidden);
                }
                result = result.Take(count);
                return result.ToList();
            }
        }

        public Article Update(int id, Action<Article> update)
        {
            using (var db = new DataContext())
            {
                var old = db.Articles.FirstOrDefault(_ => _.Id == id);
                if (old == null)
                {
                    return null;
                }
                update(old);
                db.SaveChanges();
                return old;
            }
        }

        public Article Add(Article entity)
        {
            using (var db = new DataContext())
            {
                db.Articles.Add(entity);
                db.SaveChanges();
                return entity;
            }
        }

        public bool Delete(int id)
        {
            using (var db = new DataContext())
            {
                var entity = db.Articles.FirstOrDefault(_ => _.Id == id);
                if (entity == null)
                {
                    return false;
                }

                db.Articles.Remove(entity);
                db.SaveChanges();
                return true;
            }
        }
    }
}