﻿using Interfaces.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel.Content;
using System.Data.Entity;

namespace Services.Content
{
    public class WidgetService : IWidgetService
    {        

        public Widget Add(Widget entity)
        {
            using (var db = new DataContext())
            {
                var result = db.Widgets.FirstOrDefault(_ => _.Title == entity.Title);
                if (result == null)
                {
                    db.Widgets.Add(entity);
                    db.SaveChanges();
                }
                return result;
            }
        }

        public bool Delete(int id)
        {
            using (var db = new DataContext())
            {
                var result = db.Widgets.FirstOrDefault(_ => _.Id == id);
                if (result != null)
                {
                    db.Widgets.Remove(result);
                    db.SaveChanges();
                    return true;
                }
                return false;
            }
        }

        public IList<Widget> GetAll()
        {
            using (var db = new DataContext())
            {
                var result = db.Widgets;
                return result.ToList();
            }
        }

        public Widget GetById(int id)
        {
            using (var db = new DataContext())
            {
                var result = db.Widgets.Include(_=>_.User).FirstOrDefault(_ => _.Id == id);
                return result;
            }
        }

        public IList<Widget> GetWidgetsUser(int userId)
        {
            using (var db = new DataContext())
            {
                var result = db.Widgets.Include(_ => _.User).Where(_ => _.UserId == userId);
                return result.ToList();
            }
        }

        public Widget Update(Widget widget)
        {
            using (var db = new DataContext())
            {
                var result = db.Widgets.FirstOrDefault(_ => _.Id == widget.Id);
                if (result != null)
                {
                    result.Title = widget.Title;
                    result.Script = widget.Script;
                    result.Description = widget.Description;                    
                    db.SaveChanges();
                    return result;
                }
                return widget;
            }
        }
       
    }
}
