﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using DomainModel.Content;
using Interfaces.Content;

namespace Services.Content
{
    public class BlockListService : IBlockListService
    {
        public IList<BlockList> GetAll()
        {
            using (var db = new DataContext())
            {
                var result = db.BlockLists.Include(_ => _.Blocks);
                return result.ToList();
            }
        }

        public IList<TResult> GetAll<TResult>(Expression<Func<BlockList, TResult>> selector)
        {
            using (var db = new DataContext())
            {
                var result = db.BlockLists.Include(_ => _.Blocks).Select(selector);
                return result.ToList();
            }
        }
    }
}