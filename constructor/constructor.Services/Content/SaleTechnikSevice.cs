﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using DomainModel.Content;
using Interfaces.Content;

namespace Services.Content
{
    public class SaleTechnikService : ISaleTechnikService
    {
        public SaleTechniks GetById(int id)
        {
            using (var db = new DataContext())
            {
                var result = db.SaleTechniks.FirstOrDefault(_ => _.Id == id);
                return result;
            }
        }

        public IList<SaleTechniks> GetLast(int Count)
        {
            using (var db = new DataContext())
            {
                var result =
                    db.SaleTechniks.OrderByDescending(_ => _.Id).AsQueryable().Take(Count);
                return result.ToList();
            }
        }

        public IList<SaleTechniks> GetAll(bool onlyActive = false)
        {
            using (var db = new DataContext())
            {
                var result = db.SaleTechniks.AsQueryable().OrderByDescending(_ => _.Id);
                return result.ToList();
            }
        }

        public SaleTechniks Update(int id, Action<SaleTechniks> update)
        {
            using (var db = new DataContext())
            {
                var old = db.SaleTechniks.FirstOrDefault(_ => _.Id == id);
                if (old == null)
                {
                    return null;
                }
                update(old);
                db.SaveChanges();
                return old;
            }
        }

        public SaleTechniks Add(SaleTechniks entity)
        {
            using (var db = new DataContext())
            {
                db.SaleTechniks.Add(entity);
                db.SaveChanges();
                return entity;
            }
        }

        public string Delete(int id)
        {
            using (var db = new DataContext())
            {
                var entity = db.SaleTechniks.FirstOrDefault(_ => _.Id == id);
                if (entity == null)
                {
                    return null;
                }

                db.SaleTechniks.Remove(entity);
                db.SaveChanges();
                return entity.Image;
            }
        }
    }
}

