﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DomainModel.Content;
using Interfaces.Content;

namespace Services.Content
{
    public class ServicesService : IServicesService
    {
        public Servise GetByKey(string stringKey)
        {
            using (var db = new DataContext())
            {
                var result = db.Servises.Include(_ => _.Category).FirstOrDefault(_ => _.Title.Equals(stringKey));
                return result;
            }
        }

        public IList<Servise> GetLatestServices(int Count)
        {
            using (var db = new DataContext())
            {
                var result =
                    db.Servises.OrderByDescending(_ => _.Id).AsQueryable().Take(Count);
                return result.ToList();
            }
        }

        public Servise GetById(int id)
        {
            using (var db = new DataContext())
            {
                var result = db.Servises.Include(_ => _.Category).FirstOrDefault(_ => _.Id == id);
                return result;
            }
        }

        public IList<Servise> GetAll(bool onlyActive = true)
        {
            using (var db = new DataContext())
            {
                var result =
                    db.Servises.Include(_ => _.Category).OrderByDescending(_ => _.Created).AsQueryable();
                if (onlyActive)
                {
                    result = result.Where(_ => !_.IsHidden);
                }
                return result.ToList();
            }
        }

        public IList<Servise> GetLast(int Count)
        {
            using (var db = new DataContext())
            {
                var result =
                    db.Servises.OrderByDescending(_ => _.Created).AsQueryable().Take(Count);
                return result.ToList();
            }
        }

        public IList<Servise> GetAllByCategoryKey(string stringKey, bool onlyActive = true)
        {
            using (var db = new DataContext())
            {
                var result =
                    db.Servises.Include(_ => _.Category)
                        .Where(_ => _.Category.StringKey.Equals(stringKey))
                        .OrderByDescending(_ => _.Created)
                        .AsQueryable();
                if (onlyActive)
                {
                    result = result.Where(_ => !_.IsHidden);
                }
                return result.ToList();
            }
        }

        public IList<Servise> GetLast(int count, bool onlyActive = true)
        {
            using (var db = new DataContext())
            {
                var result =
                    db.Servises.Include(_ => _.Category).OrderByDescending(_ => _.Created).AsQueryable();
                if (onlyActive)
                {
                    result = result.Where(_ => !_.IsHidden);
                }
                result = result.Take(count);
                return result.ToList();
            }
        }

        public Servise Update(int id, Action<Servise> update)
        {
            using (var db = new DataContext())
            {
                var old = db.Servises.FirstOrDefault(_ => _.Id == id);
                if (old == null)
                {
                    return null;
                }
                update(old);
                db.SaveChanges();
                return old;
            }
        }

        public Servise Add(Servise entity)
        {
            using (var db = new DataContext())
            {
                db.Servises.Add(entity);
                db.SaveChanges();
                return entity;
            }
        }

        public bool Delete(int id)
        {
            using (var db = new DataContext())
            {
                var entity = db.Servises.FirstOrDefault(_ => _.Id == id);
                if (entity == null)
                {
                    return false;
                }

                db.Servises.Remove(entity);
                db.SaveChanges();
                return true;
            }
        }
    }
}