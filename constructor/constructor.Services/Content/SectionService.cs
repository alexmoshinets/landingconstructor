﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DomainModel.Content;
using Interfaces.Content;
using System.Linq.Expressions;

namespace Services.Content
{
    public class SectionService : ISectionService
    {
        public CategoryServices GetById(int id)
        {
            using (var db = new DataContext())
            {
                var result = db.Sections.FirstOrDefault(_ => _.Id == id);
                return result;
            }
        }

        public CategoryServices GetByUrl(string Url)
        {
            using (var db = new DataContext())
            {
                var result = db.Sections.FirstOrDefault(_ => _.Url == Url);
                return result;
            }
        }

        public IList<CategoryServices> GetAll(bool onlyActive = false)
        {
            using (var db = new DataContext())
            {
                var result = db.Sections.AsQueryable();
                if (onlyActive)
                {
                    result = result.Where(_ => !_.IsHidden);
                }
                return result.ToList();
            }
        }

        public CategoryServices Update(int id, Action<CategoryServices> update)
        {
            using (var db = new DataContext())
            {
                var old = db.Sections.FirstOrDefault(_ => _.Id == id);
                if (old == null)
                {
                    return null;
                }
                update(old);
                db.SaveChanges();
                return old;
            }
        }

        public CategoryServices Add(CategoryServices entity)
        {
            using (var db = new DataContext())
            {
                db.Sections.Add(entity);
                db.SaveChanges();
                return entity;
            }
        }

        public bool Delete(int id)
        {
            using (var db = new DataContext())
            {
                var entity = db.Sections.FirstOrDefault(_ => _.Id == id);
                if (entity == null)
                {
                    return false;
                }

                db.Sections.Remove(entity);
                db.SaveChanges();
                return true;
            }
        }

        public bool DeleteProductsAll(int IDSection)
        {
            using (var db = new DataContext())
            {
                var entity = db.Products.Where(_ => _.Section == IDSection).ToList();
                if (entity == null)
                {
                    return false;
                }
                db.Products.RemoveRange(entity);
                db.SaveChanges();
                return true;
            }
        }

        public IList<TResult> GetAllSection<TResult>(Expression<Func<CategoryServices, TResult>> selector)
        {
            using (var db = new DataContext())
            {
                var result = db.Sections.Include(_ => _.Id).Select(selector);
                return result.ToList();
            }
        }

        //public IList<Producer> GetAllProducer(bool onlyActive = false)
        //{
        //    using (var db = new DataContext())
        //    {
        //        var result = db.Producers.AsQueryable();
        //        return result.ToList();
        //    }
        //}


    }
}