﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using DomainModel.Content;
using Interfaces.Content;

namespace Services.Content
{
    public class LandingService : ILandingService
    {
        public Landing GetByKey(string stringKey)
        {
            using (var db = new DataContext())
            {
                var result = db.Landings.FirstOrDefault(_ => _.StringKey.Equals(stringKey));
                return result;
            }
        }

        public Landing GetByUrl(string url)
        {
            using (var db = new DataContext())
            {
                var result = db.Landings.FirstOrDefault(_ => _.Url.Equals(url));
                return result;
            }
        }

        public Landing SetEmail(string email, int id)
        {
            using (var db = new DataContext())
            {
                var result = db.Landings.FirstOrDefault(_ => _.Id == id);
                if (result!=null)
                {
                    result.Email = email;
                    db.SaveChanges();
                }
                return result;
            }
        }

        public List<Landing> GetByUser(int userId)
        {
            using (var db = new DataContext())
            {
                var result = db.Landings.Where(_ => _.UserId==userId).ToList();
                return result;
            }
        }

        public Landing GetById(int id)
        {
            using (var db = new DataContext())
            {
                var result = db.Landings.FirstOrDefault(_ => _.Id == id);
                return result;
            }
        }
        public List<Landing> GetByUrlAndUserId(string url, int userId)
        {
            using (var db = new DataContext())
            {
                var result = db.Landings.Where(_ => _.Url == url&&_.UserId == userId).ToList();
                return result;
            }
        }

        public IList<Landing> GetAll(bool onlyActive = false)
        {
            using (var db = new DataContext())
            {
                IQueryable<Landing> result = db.Landings;
                if (onlyActive)
                {
                    result = result.Where(_ => !_.IsPublic);
                }
                return result.ToList();
            }
        }

        public Landing Update(int id, Action<Landing> update)
        {
            using (var db = new DataContext())
            {
                var old = db.Landings.FirstOrDefault(_ => _.Id == id);
                if (old == null)
                {
                    return null;
                }
                update(old);
                db.SaveChanges();
                return old;
            }
        }

        public Landing Update(string stringKey, Action<Landing> update)
        {
            using (var db = new DataContext())
            {
                var old = db.Landings.FirstOrDefault(_ => _.StringKey == stringKey);
                if (old == null)
                {
                    return null;
                }
                update(old);
                db.SaveChanges();
                return old;
            }
        }

        public string AddOrUpdate(string url, Landing entity, Action<Landing> update)
        {
            using (var db = new DataContext())
            {
                var old = db.Landings.FirstOrDefault(_ => _.Url == url&&_.UserId == entity.UserId&&_.StringKey == entity.StringKey);
                if (old == null)
                {
                    db.Landings.Add(entity);
                    db.SaveChanges();
                    return "add";
                }
                update(old);
                db.SaveChanges();
                return "update";
            }
        }

        public Landing Add(Landing entity)
        {
            using (var db = new DataContext())
            {
                db.Landings.Add(entity);
                db.SaveChanges();
                return entity;
            }
        }

        public bool Delete(int id)
        {
            using (var db = new DataContext())
            {
                var entity = db.Landings.FirstOrDefault(_ => _.Id == id);
                if (entity == null)
                {
                    return false;
                }

                db.Landings.Remove(entity);
                db.SaveChanges();
                return true;
            }
        }

        public bool DeleteLanding(string url)
        {
            using (var db = new DataContext())
            {
                var entities = db.Landings.Where(_ => _.Url ==url);
                if (!entities.Any())
                {
                    return false;
                }
                foreach (var item in entities)
                {
                    db.Landings.Remove(item);
                    db.SaveChanges();
                }
                return true;
            }
        }
    }
}