﻿using System;
using System.Collections.Generic;
using System.Linq;
using DomainModel.Content;
using Interfaces.Content;
using System.Linq.Expressions;
using System.Data.Entity;

namespace Services.Content
{
    public class PresenceService : IPresenceService
    {
        public IList<Presence> GetAll()
        {
            using (var db = new DataContext())
            {
                var result = db.Presences.AsQueryable();
                return result.ToList();
            }
        }

        public IList<TResult> GetAllItems<TResult>(Expression<Func<Presence, TResult>> selector)
        {
            using (var db = new DataContext())
            {
                var result = db.Presences.Include(_ => _.Id).Select(selector);
                return result.ToList();
            }
        }
    }
}
