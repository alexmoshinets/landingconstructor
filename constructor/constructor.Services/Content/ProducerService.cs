﻿using System;
using System.Collections.Generic;
using System.Linq;
using DomainModel.Content;
using Interfaces.Content;
using System.Linq.Expressions;
using System.Data.Entity;

namespace Services.Content
{
    public class ProducerService : IProducerService
    {
        public Producer GetById(int id)
        {
            using (var db = new DataContext())
            {
                var result = db.Producers.FirstOrDefault(_ => _.Id == id);
                return result;
            }
        }

        public Producer GetByUrl(string url)
        {
            using (var db = new DataContext())
            {
                var result = db.Producers.FirstOrDefault(_ => _.Url == url);
                return result;
            }
        }

        public bool ExistUrl(string url)
        {
            using (var db = new DataContext())
            {
                var result = db.Producers.FirstOrDefault(_ => _.Url == url);
                if (result == null)
                    return false;
                else
                    return true;
            }
        }

        public IList<Producer> GetByList(IList<Product> list)
        {
            using (var db = new DataContext())
            {

                var listProduct = (from cust in list
                           select cust.Producer).Distinct();

                var result = new List<Producer>();

                for (int i = 0; i < listProduct.ToList().Count; i++)
                {
                    result.Add(GetById(listProduct.ToList()[i]));
                }
                
                return result.ToList();
            }
        }

        public IList<Producer> GetAll(bool onlyActive = false)
        {
            using (var db = new DataContext())
            {
                var result = db.Producers.AsQueryable();
                return result.ToList();
            }
        }

        public IList<TResult> GetAllProducer<TResult>(Expression<Func<Producer, TResult>> selector)
        {
            using (var db = new DataContext())
            {
                var result = db.Producers.Include(_ => _.Id).Select(selector);
                return result.ToList();
            }
        }

        public Producer Update(int id, Action<Producer> update)
        {
            using (var db = new DataContext())
            {
                var old = db.Producers.FirstOrDefault(_ => _.Id == id);
                if (old == null)
                {
                    return null;
                }
                update(old);
                db.SaveChanges();
                return old;
            }
        }

        public Producer Add(Producer entity)
        {
            using (var db = new DataContext())
            {
                db.Producers.Add(entity);
                db.SaveChanges();
                return entity;
            }
        }

        public bool Delete(int id)
        {
            using (var db = new DataContext())
            {
                var entity = db.Producers.FirstOrDefault(_ => _.Id == id);
                if (entity == null)
                {
                    return false;
                }

                db.Producers.Remove(entity);
                db.SaveChanges();
                return true;
            }
        }

        public bool DeleteProductsAll(int IDProducer)
        {
            using (var db = new DataContext())
            {
                var entity = db.Products.Where(_ => _.Producer == IDProducer).ToList();
                if (entity == null)
                {
                    return false;
                }
                db.Products.RemoveRange(entity);
                db.SaveChanges();
                return true;
            }

        }
    }

    internal class from
    {
    }
}