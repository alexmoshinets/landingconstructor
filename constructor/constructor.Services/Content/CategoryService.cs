﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using DomainModel.Content;
using Interfaces.Content;

namespace Services.Content
{
    public class CategoryService : ICategoryService
    {
        public Category GetByKey(string stringKey)
        {
            using (var db = new DataContext())
            {
                var result = db.Category.FirstOrDefault(_ => _.StringKey.Equals(stringKey));
                return result;
            }
        }

        public Category GetById(int id)
        {
            using (var db = new DataContext())
            {
                var result = db.Category.Include(_=>_.Parent).Include(_ => _.Products).FirstOrDefault(_ => _.Id == id);
                return result;
            }
        }

        public IList<Category> GetAll(bool onlyActive = false)
        {
            using (var db = new DataContext())
            {
                var result = db.Category.Include(_ => _.Parent).Include(_ => _.Products);
                if (onlyActive)
                {
                    result = result.Where(_ => !_.IsHidden);
                }
                return result.ToList();
            }
        }

        public IList<TResult> GetAll<TResult>(Expression<Func<Category, TResult>> selector)
        {
            using (var db = new DataContext())
            {
                var result = db.Category.Include(_ => _.Parent).Include(_ => _.Products).Select(selector);
                return result.ToList();
            }
        }

        public Category Update(int id, Action<Category> update)
        {
            using (var db = new DataContext())
            {
                var old = db.Category.FirstOrDefault(_ => _.Id == id);
                if (old == null)
                {
                    return null;
                }
                update(old);
                db.SaveChanges();
                return old;
            }
        }

        public Category Add(Category entity)
        {
            using (var db = new DataContext())
            {
                db.Category.Add(entity);
                db.SaveChanges();
                return entity;
            }
        }

        public string Delete(int id)
        {
            var file = "";
            using (var db = new DataContext())
            {
                var entity = db.Category.FirstOrDefault(_ => _.Id == id);
                file = entity.Image;
                if (entity == null)
                {
                    return "";
                }

                db.Category.Remove(entity);
                db.SaveChanges();
                return file;
            }
        }
    }
}