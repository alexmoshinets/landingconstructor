﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using DomainModel.Content;
using Interfaces.Content;

namespace Services.Content
{
    public class PageService : IPageService
    {
        public Page GetByKey(string stringKey)
        {
            using (var db = new DataContext())
            {
                var result = db.Pages.FirstOrDefault(_ => _.StringKey.Equals(stringKey));
                return result;
            }
        }

        public Page GetById(int id)
        {
            using (var db = new DataContext())
            {
                var result = db.Pages.FirstOrDefault(_ => _.Id == id);
                return result;
            }
        }

        public IList<Page> GetAll()
        {
            using (var db = new DataContext())
            {
                var result = db.Pages;
                return result.ToList();
            }
        }

        public IList<TResult> GetAll<TResult>(Expression<Func<Page, TResult>> selector)
        {
            using (var db = new DataContext())
            {
                var result = db.Pages.Select(selector);
                return result.ToList();
            }
        }

        public Page Update(int id, Action<Page> update)
        {
            using (var db = new DataContext())
            {
                var old = db.Pages.FirstOrDefault(_ => _.Id == id);
                if (old == null)
                {
                    return null;
                }
                update(old);
                db.SaveChanges();
                return old;
            }
        }

        public Page Update(string stringKey, Action<Page> update)
        {
            using (var db = new DataContext())
            {
                var old = db.Pages.FirstOrDefault(_ => _.StringKey.Equals(stringKey));
                if (old == null)
                {
                    return null;
                }
                update(old);
                db.SaveChanges();
                return old;
            }
        }

        public Page Add(Page entity)
        {
            using (var db = new DataContext())
            {
                db.Pages.Add(entity);
                db.SaveChanges();
                return entity;
            }
        }

        public bool Delete(int id)
        {
            using (var db = new DataContext())
            {
                var entity = db.Pages.FirstOrDefault(_ => _.Id == id);
                if (entity == null)
                {
                    return false;
                }

                db.Pages.Remove(entity);
                db.SaveChanges();
                return true;
            }
        }
    }
}