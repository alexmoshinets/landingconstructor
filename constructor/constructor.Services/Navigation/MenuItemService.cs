﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DomainModel.Navigation;
using Interfaces.Navigation;

namespace Services.Navigation
{
    public class MenuItemService : IMenuItemService
    {
        public IList<MenuItem> GetByMenuKey(string stringKey, bool onlyActive = false)
        {
            using (var db = new DataContext())
            {
                var result = db.MenuItems.Include(_ => _.Parent).Include(_ => _.Childs).Include(_ => _.Childs.Select(child => child.Page)).Include(_ => _.Page).Where(_ => _.Menu.StringKey.Equals(stringKey));
                if (onlyActive)
                {
                    result = result.Where(_ => !_.IsHidden);
                }
                return result.ToList();
            }
        }

        public IList<MenuItem> GetAll(bool onlyActive = false)
        {
            using (var db = new DataContext())
            {
                var result = db.MenuItems.Include(_ => _.Menu).Include(_ => _.Parent).Include(_ => _.Childs).Include(_ => _.Page);
                if (onlyActive)
                {
                    result = result.Where(_ => !_.IsHidden);
                }
                return result.ToList();
            }
        }

        public MenuItem GetById(int id)
        {
            using (var db = new DataContext())
            {
                var result = db.MenuItems.Include(_ => _.Childs).Include(_ => _.Childs.Select(child => child.Page)).FirstOrDefault(_ => _.Id == id);
                return result;
            }
        }

        public MenuItem Update(int id, Action<MenuItem> update)
        {
            using (var db = new DataContext())
            {
                var old = db.MenuItems.FirstOrDefault(_ => _.Id == id);
                if (old == null) { return null; }
                update(old);
                db.SaveChanges();
                return old;
            }
        }

        public MenuItem Add(MenuItem entity)
        {
            using (var db = new DataContext())
            {
                db.MenuItems.Add(entity);
                db.SaveChanges();
                return entity;
            }
        }

        public bool Delete(int id)
        {
            using (var db = new DataContext())
            {
                var entity = db.MenuItems.FirstOrDefault(_ => _.Id == id);
                if (entity == null)
                {
                    return false;
                }

                db.MenuItems.Remove(entity);
                db.SaveChanges();
                return true;
            }
        }
    }
}