﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using DomainModel.Navigation;
using Interfaces.Navigation;

namespace Services.Navigation
{
    public class MenuService : IMenuService
    {
        public IList<Menu> GetAll()
        {
            using (var db = new DataContext())
            {
                var result =
                    db.Menus.Include(_ => _.Items.Select(i => i.Page)).Include(_ => _.Items.Select(i => i.Childs));
                return result.ToList();
            }
        }

        public IList<TResult> GetAll<TResult>(Expression<Func<Menu, TResult>> selector)
        {
            using (var db = new DataContext())
            {
                var result =
                    db.Menus.Include(_ => _.Items.Select(i => i.Page))
                        .Include(_ => _.Items.Select(i => i.Childs))
                        .Select(selector);
                return result.ToList();
            }
        }
    }
}