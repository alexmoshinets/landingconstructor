﻿using System.Data.Entity;
using DomainModel;
using DomainModel.Account;
using DomainModel.Content;
using DomainModel.Navigation;

namespace Services
{
    [DbConfigurationType(typeof(DefaultConfiguration))]
    public class DataContext : DbContext
    {
        public DataContext()
            : base("DbConnection")
        {
            Configuration.LazyLoadingEnabled = false;
        }

        #region Account

        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }

        #endregion

        #region Content

        public DbSet<Setting> Settings { get; set; }
        public DbSet<Page> Pages { get; set; }

        public DbSet<Article> Articles { get; set; }
        public DbSet<ArticleCategory> ArticleCategories { get; set; }

        public DbSet<Block> Blocks { get; set; }
        public DbSet<BlockList> BlockLists { get; set; }

        public DbSet<Slider> Sliders { get; set; }

        public DbSet<Unit> Units { get; set; }

        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Category { get; set; }
        public DbSet<Localization> Localizations { get; set; }
        public DbSet<Landing> Landings { get; set; }
        public DbSet<LandingIntegration> LandingIntegrations { get; set; }
        public DbSet<Sections> Sections { get; set; }
        public DbSet<SectionsCategory> SectionsCategory { get; set; }
        public DbSet<LandingSetting> LandingSettings { get; set; }
        public DbSet<Widget> Widgets { get; set; }

        #endregion

        #region Navigation

        public DbSet<Menu> Menus { get; set; }
        public DbSet<MenuItem> MenuItems { get; set; }

        #endregion

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            #region ClassesToTables

            #region Account

            modelBuilder.Entity<User>().ToTable("Users", "Account");
            modelBuilder.Entity<Role>().ToTable("Roles", "Account");

            #endregion

            #region Content

            modelBuilder.Entity<Setting>().ToTable("Settings", "Content");
            modelBuilder.Entity<Page>().ToTable("Pages", "Content");

            modelBuilder.Entity<ArticleCategory>().ToTable("ArticleCategories", "Content");
            modelBuilder.Entity<Article>().ToTable("Articles", "Content");

            modelBuilder.Entity<BlockList>().ToTable("BlockLists", "Content");
            modelBuilder.Entity<Block>().ToTable("Blocks", "Content");

            modelBuilder.Entity<Slider>().ToTable("Sliders", "Content");

            modelBuilder.Entity<Product>().ToTable("Products", "Content");
            modelBuilder.Entity<Category>().ToTable("Category", "Content");
            modelBuilder.Entity<Sections>().ToTable("Sections", "Content");
            modelBuilder.Entity<SectionsCategory>().ToTable("SectionsCategory", "Content");

            #endregion

            #region Navigation

            modelBuilder.Entity<Menu>().ToTable("Menus", "Navigation");
            modelBuilder.Entity<MenuItem>().ToTable("MenuItems", "Navigation");

            #endregion

            #endregion

            #region PropertiesAndKeys

            #region Account

            #region User

            modelBuilder.Entity<User>()
                .HasKey(c => c.Id);

            modelBuilder.Entity<User>()
                .Property(_ => _.UserName)
                .IsRequired()
                .HasMaxLength(BaseEntity.NameLength)
                .IsUnicode();

            modelBuilder.Entity<User>()
                .Property(_ => _.Email)
                .IsRequired()
                .HasMaxLength(User.EmailLength)
                .IsUnicode();

            modelBuilder.Entity<User>()
                .Property(_ => _.Password)
                .IsRequired()
                .HasMaxLength(User.PasswordLength)
                .IsUnicode();

            #endregion

            #region Role

            modelBuilder.Entity<Role>()
                .HasKey(c => c.Id);

            modelBuilder.Entity<Role>()
                .Property(_ => _.Name)
                .IsRequired()
                .HasMaxLength(BaseEntity.NameLength)
                .IsUnicode();

            modelBuilder.Entity<Role>()
                .HasMany(_ => _.Users)
                .WithMany(_ => _.Roles)
                .Map(_ => _.ToTable("RoleUsers", "Account")
                    .MapLeftKey("RoleId")
                    .MapRightKey("UserId"));

            #endregion

            #endregion

            #region Content

            #region Setting

            modelBuilder.Entity<Setting>()
                .HasKey(c => c.Id);

            modelBuilder.Entity<Setting>()
                .Property(_ => _.StringKey)
                .IsRequired()
                .IsUnicode();

            #endregion

            #region Page

            modelBuilder.Entity<Page>()
                .HasKey(c => c.Id);

            modelBuilder.Entity<Page>()
                .Property(_ => _.StringKey)
                .IsRequired()
                .IsUnicode();

            modelBuilder.Entity<Page>()
                .Property(_ => _.Name)
                .IsRequired()
                .HasMaxLength(BaseEntity.NameLength)
                .IsUnicode();

            modelBuilder.Entity<Page>()
                .Property(_ => _.Created)
                .IsRequired();

            #endregion

            #region Article

            modelBuilder.Entity<Article>()
                .HasKey(c => c.Id);

            modelBuilder.Entity<Article>()
                .Property(_ => _.Title)
                .IsRequired()
                .HasMaxLength(BaseEntity.NameLength)
                .IsUnicode();

            modelBuilder.Entity<Article>()
                .Property(_ => _.Annonse)
                .IsRequired()
                .IsUnicode();

            modelBuilder.Entity<Article>()
                .Property(_ => _.Description)
                .IsRequired()
                .IsUnicode();

            modelBuilder.Entity<Article>()
                .Property(_ => _.Date)
                .IsRequired();

            modelBuilder.Entity<Article>()
                .Property(_ => _.Created)
                .IsRequired();

            #endregion

            #region ArticleCategory

            modelBuilder.Entity<ArticleCategory>()
                .HasKey(c => c.Id);

            modelBuilder.Entity<ArticleCategory>()
                .Property(_ => _.Name)
                .IsRequired()
                .HasMaxLength(BaseEntity.NameLength)
                .IsUnicode();

            modelBuilder.Entity<ArticleCategory>()
                .Property(_ => _.Created)
                .IsRequired();

            modelBuilder.Entity<ArticleCategory>()
                .HasMany(_ => _.Articles)
                .WithRequired(_ => _.Category)
                .HasForeignKey(_ => _.CategoryId);

            #endregion

            #region Category

            modelBuilder.Entity<Category>()
                .HasKey(c => c.Id);

            modelBuilder.Entity<Category>()
                .Property(_ => _.Title)
                .IsRequired()
                .IsUnicode();

            #endregion
            #region Block

            modelBuilder.Entity<Block>()
                .HasKey(c => c.Id);

            modelBuilder.Entity<Block>()
                .Property(_ => _.StringKey)
                .IsOptional()
                .IsUnicode();

            modelBuilder.Entity<Block>()
                .Property(_ => _.Created)
                .IsRequired();

            modelBuilder.Entity<Block>()
                .Property(_ => _.Description)
                .IsRequired()
                .IsUnicode();

            modelBuilder.Entity<Block>()
                .HasOptional(_ => _.BlockList)
                .WithMany(_ => _.Blocks)
                .HasForeignKey(_ => _.BlockListId);

            #endregion

            #endregion

            #region Navigation

            #region Menu

            modelBuilder.Entity<Menu>()
                .HasKey(c => c.Id);

            modelBuilder.Entity<Menu>()
                .Property(_ => _.StringKey)
                .IsRequired()
                .IsUnicode();

            #endregion

            #region MenuItem

            modelBuilder.Entity<MenuItem>()
                .HasKey(c => c.Id);

            modelBuilder.Entity<MenuItem>()
                .Property(_ => _.Name)
                .IsRequired()
                .IsUnicode();

            modelBuilder.Entity<MenuItem>()
                .HasMany(_ => _.Childs)
                .WithOptional(_ => _.Parent)
                .HasForeignKey(_ => _.ParentId);

            modelBuilder.Entity<MenuItem>()
                .HasOptional(_ => _.Menu)
                .WithMany(_ => _.Items)
                .HasForeignKey(_ => _.MenuId);

            modelBuilder.Entity<MenuItem>()
                .HasOptional(_ => _.Page)
                .WithMany(_ => _.MenuItems)
                .HasForeignKey(_ => _.PageId);

            #endregion

            #endregion

            #endregion
                   
        }
    }
}