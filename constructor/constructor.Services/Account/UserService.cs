﻿using System;
using System.Collections.Generic;
using System.Linq;
using DomainModel.Account;
using Interfaces.Account;

namespace Services.Account
{
    public class UserService : IUserService
    {
        public IList<User> GetAll()
        {
            using (var db = new DataContext())
            {
                return db.Users.ToList();
            }
        }

        public User UpdateNotify(string username, bool value)
        {
            User user;
            using (var db = new DataContext())
            {
                user = db.Users.FirstOrDefault(_ => _.UserName == username);
                user.IsNotify = value;
                db.SaveChanges();
            }
            return user;
        }

        public IList<User> GetAllByRoleName(string roleName)
        {
            using (var db = new DataContext())
            {
                return db.Users.Where(_ => _.Roles.Any(u => u.Name.Equals(roleName))).ToList();
            }
        }

        public IList<User> GetAllByRoleNameAndUserNameMatch(string roleName, string userNameToMatch)
        {
            using (var db = new DataContext())
            {
                return
                    db.Users.Where(
                        _ => _.Roles.Any(u => u.Name.Equals(roleName)) && _.UserName.Contains(userNameToMatch)).ToList();
            }
        }

        public User GetById(int id)
        {
            User user;
            using (var db = new DataContext())
            {
                user = db.Users.FirstOrDefault(_ => _.Id == id);
            }
            return user;
        }

        public User GetByName(string name)
        {
            User user;
            using (var db = new DataContext())
            {
                user = db.Users.FirstOrDefault(_ => _.UserName == name);
            }
            return user;
        }

        public User GetByEmail(string email)
        {
            User user;
            using (var db = new DataContext())
            {
                user = db.Users.FirstOrDefault(_ => _.Email == email);
            }
            return user;
        }

        public User GetByPasswordRecoveryToken(Guid token)
        {
            User user;
            using (var db = new DataContext())
            {
                user = db.Users.FirstOrDefault(_ => _.PasswordRecoveryToken == token);
            }
            return user;
        }

        public User Create(string username, string password, string email, string passwordQuestion,
            string passwordAnswer, bool isApproved, DateTime createDate)
        {
            User user;
            using (var db = new DataContext())
            {
                user =
                    db.Users.Add(new User
                    {
                        UserName = username,
                        Password = password,
                        Email = email,
                        PasswordQuestion = passwordQuestion,
                        PasswordAnswer = passwordAnswer,
                        IsApproved = isApproved,
                        CreateDate = createDate
                    });
                db.SaveChanges();
            }
            return user;
        }

        public User CreateUser(User data)
        {
            User user;
            using (var db = new DataContext())
            {
                user =
                    db.Users.Add(new User
                    {
                        UserName = data.UserName,
                        Password = data.Password,
                        Email = data.Email,
                        PasswordQuestion = data.PasswordQuestion,
                        PasswordAnswer = data.PasswordAnswer,
                        IsApproved = data.IsApproved,
                        CreateDate = DateTime.Now,
                        Phone = data.Phone
                    });
                db.SaveChanges();
                var role = db.Roles.FirstOrDefault(_ => _.Name == "editor");
                user.Roles.Add(role);                
                role.Users.Add(user);
                db.SaveChanges();
            }
            return user;
        }

        public User Update(int id, Action<User> update)
        {
            using (var db = new DataContext())
            {
                var old = db.Users.FirstOrDefault(_ => _.Id == id);
                if (old == null)
                {
                    return null;
                }
                update(old);
                db.SaveChanges();
                return old;
            }
        }

        public User Update(string username, Action<User> update)
        {
            using (var db = new DataContext())
            {
                var old = db.Users.FirstOrDefault(_ => _.UserName == username);
                if (old == null)
                {
                    return null;
                }
                update(old);
                db.SaveChanges();
                return old;
            }
        }

        public bool Delete(int id)
        {
            using (var db = new DataContext())
            {
                var user = db.Users.FirstOrDefault(_ => _.Id == id);
                if (user == null)
                {
                    return false;
                }

                db.Users.Remove(user);
                db.SaveChanges();
                return true;
            }
        }

        public bool Delete(string username)
        {
            using (var db = new DataContext())
            {
                var user = db.Users.FirstOrDefault(_ => _.UserName == username);
                if (user == null)
                {
                    return false;
                }

                db.Users.Remove(user);
                db.SaveChanges();
                return true;
            }
        }
    }
}