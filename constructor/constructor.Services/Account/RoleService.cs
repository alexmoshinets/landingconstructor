﻿using System.Collections.Generic;
using System.Linq;
using DomainModel.Account;
using Interfaces.Account;

namespace Services.Account
{
    public class RoleService : IRoleService
    {
        public IList<Role> GetAll()
        {
            using (var db = new DataContext())
            {
                return db.Roles.ToList();
            }
        }

        public IList<Role> GetAllByUserName(string userName)
        {
            using (var db = new DataContext())
            {
                return db.Roles.Where(_ => _.Users.Any(u => u.UserName == userName)).ToList();
            }
        }

        public Role GetByName(string roleName)
        {
            using (var db = new DataContext())
            {
                return db.Roles.FirstOrDefault(_ => _.Name.Equals(roleName));
            }
        }

        public bool IsRoleExist(string roleName)
        {
            using (var db = new DataContext())
            {
                return db.Roles.Any(_ => _.Name.Equals(roleName));
            }
        }

        public bool IsUserInRole(string userName, string roleName)
        {
            using (var db = new DataContext())
            {
                return db.Roles.Any(_ => _.Name.Equals(roleName) && _.Users.Any(u => u.UserName.Equals(userName)));
            }
        }

        public Role Create(string roleName)
        {
            Role role;
            using (var db = new DataContext())
            {
                role = db.Roles.Add(new Role { Name = roleName });
                db.SaveChanges();
            }
            return role;
        }

        public bool Delete(string roleName)
        {
            using (var db = new DataContext())
            {
                var role = db.Roles.FirstOrDefault(_ => _.Name == roleName);
                if (role == null)
                {
                    return false;
                }

                db.Roles.Remove(role);
                db.SaveChanges();
                return true;
            }
        }

        public void AddUsersToRoles(string[] userNames, string[] roleNames)
        {
            using (var db = new DataContext())
            {
                var roles = db.Roles.Where(_ => roleNames.Contains(_.Name));
                var users = db.Users.Where(_ => userNames.Contains(_.UserName));

                foreach (var user in users)
                {
                    foreach (var role in roles)
                    {
                        user.Roles.Add(role);
                    }
                }

                db.SaveChanges();
            }
        }

        public void RemoveUsersFromRoles(string[] userNames, string[] roleNames)
        {
            using (var db = new DataContext())
            {
                var roles = db.Roles.Where(_ => roleNames.Contains(_.Name));
                var users = db.Users.Where(_ => userNames.Contains(_.UserName));

                foreach (var user in users)
                {
                    foreach (var role in roles)
                    {
                        user.Roles.Remove(role);
                    }
                }

                db.SaveChanges();
            }
        }
    }
}